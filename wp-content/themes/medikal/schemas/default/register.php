<?php
/**
 * Registering the theme default schemas.
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Schemas_Default_Register
extends VTCore_Zeus_Schema_Base {

  protected function setContext() {

    $this->setSchemaURL(VTCORE_ZEUS_THEME_PATH . DIRECTORY_SEPARATOR . 'default');
    $this->context = array(
      'schema' => array(
        'id' => 'default',
        'name' => __('Medikal', 'medikal'),
        'color' => '#da432e',
        'description' => __('Default schema with white background and blue accents.', 'medikal'),
      ),

      'sections' => array(
        'global' => array(
          'title' => __('Global Styling', 'medikal'),
          'active' => 'global',
        ),
        'header' => array(
          'title' => __('Header', 'medikal'),
        ),
        'navigation' => array(
          'title' => __('Navigation', 'medikal'),
        ),
        'slicknav' => array(
          'title' => __('Slick Navigation', 'medikal'),
        ),
        'slider' => array(
          'title' => __('Slider', 'medikal'),
        ),
        'headline' => array(
          'title' => __('Headline', 'medikal'),
          'enabled' => defined('VTCORE_HEADLINE_LOADED'),
        ),
        'fotorama' => array(
          'title' => __('Fotorama', 'medikal'),
        ),
        'woocommerce' => array(
          'title' => __('WooCommerce Elements', 'medikal'),
          'enabled' => VTCORE_WOOCOMMERCE_LOADED,
        ),
        'visualcomposer' => array(
          'title' => __('Visual Composer', 'medikal'),
          'enabled' => VTCORE_VC_LOADED,
        ),
        'department' => array(
          'title' => __('Department Elements', 'medikal'),
          'enabled' => defined('VTCORE_DEPARTMENT_LOADED'),
        ),
        'services' => array(
          'title' => __('Services Elements', 'medikal'),
          'enabled' => defined('VTCORE_SERVICES_LOADED'),
        ),
        'members' => array(
          'title' => __('Members Elements', 'medikal'),
          'enabled' => defined('VTCORE_MEMBERS_LOADED'),
        ),
        'single' => array(
          'title' => __('Post Single Elements', 'medikal'),
        ),
        'content' => array(
          'title' => __('Content Elements', 'medikal'),
        ),
        'html' => array(
          'title' => __('Html Elements', 'medikal'),
        ),
        'form' => array(
          'title' => __('Form Elements', 'medikal'),
        ),
        'bootstrap' => array(
          'title' => __('Bootstrap Elements', 'medikal'),
        ),
        'widgets' => array(
          'title' => __('Widgets', 'medikal'),
        ),
        'postface' => array(
          'title' => __('Postface', 'medikal'),
        ),
        'footer' => array(
          'title' => __('Footer', 'medikal'),
        ),
        'full-footer' => array(
          'title' => __('Full Footer', 'medikal'),
        ),
      ),

      'color' => array(
        'text' => array(
          'title' => __('Links', 'medikal'),
          'description' => __('CSS options for styling the global link element' , 'medikal'),
          'parent' => 'global',
          'selectors' => array(''),
          'link-color' => '#696969',
          'link-visited' => '#494949',
          'link-hover' => '#3dc5df',
          'link-focus' => '#3dc5df',
        ),
        'heading' => array(
          'selectors' => array('h1', 'h2', 'h3', 'h4', 'h5', 'h6', '.header'),
          'title' => __('Heading', 'medikal'),
          'parent' => 'global',
          'description' => __('CSS options for styling the global heading element' , 'medikal'),
          'font-color' => '#333333',
          'font-family' => 'Raleway',
          'font-style' => '',
          'font-weight' => '600',
        ),
        'body' => array(
          'title' => __('Body Element', 'medikal'),
          'description' => __('CSS options for styling the main body element,
                               the options configured here will also acts as
                               a global css rule.', 'medikal'),
          'parent' => 'global',
          'selectors' => array('body'),
          'background-image' => '',
          'background-color' => '#3dc5df',
          'background-repeat' => '',
          'background-size' => '',
          'background-position' => '',
          'font-color' => '#696969',
          'font-family' => 'Raleway',
          'font-size' => '14px',
          'font-style' => '',
          'font-weight' => '400',
        ),


        'page' => array(
          'title' => __('Page Element', 'medikal'),
          'description' => __('CSS options for styling the page element,
                               this is useful if you want to change the page
                               element background color or add some border
                               element to the page element when in boxed mode', 'medikal'),
          'selectors' => array('#page'),
          'parent' => 'global',
          'background-image' => '',
          'background-color' => '#ffffff',
          'background-repeat' => '',
          'background-size' => '',
          'background-position' => '',
          'border-style' => '',
          'border-width' => '',
          'border-color' => '',
        ),

        'header' => array(
          'title' => __('Header Element', 'medikal'),
          'description' => __('CSS options for the main header element', 'medikal'),
          'parent' => 'header',
          'selectors' => array(
            '#header',
          ),
          'background-image' => '',
          'background-color' => '#ffffff',
          'background-repeat' => '',
          'background-size' => '',
          'background-position' => '',
          'box-shadow' => '-5px -5px 16px #eeeeee',
        ),

        'header-navigation' => array(
          'title' => __('Header Navigation Menu First Level', 'medikal'),
          'description' => __('CSS options for styling the main navigation menu first level found inside the top header element', 'medikal'),
          'parent' => 'header',
          'selectors' => array(
            '#header .dropdown-navigation ul.nav > li > a',
          ),
          'font-color' => '#696969',
          'font-weight' => '600',
          'font-size' => '14px',
          'font-family' => 'Raleway',
          'hover-font-color' => '#3dc5df',
          'focus-font-color' => '#3dc5df',
          'background-color' => 'transparent',
          'hover-background-color' => '',
          'focus-background-color' => ''
        ),

        'header-navigation-active' => array(
          'title' => __('Header Navigation Menu First Level Active State', 'medikal'),
          'description' => __('CSS options for styling the main navigation menu first level when it is on active state', 'medikal'),
          'parent' => 'header',
          'selectors' => array(
            '#header .dropdown-navigation ul.nav > li.active > a',
          ),
          'font-color' => '#3dc5df',
          'background-color' => '',
          'hover-background-color' => '',
        ),

        'header-navigation-dropdown-first' => array(
          'title' => __('Header Navigation Menu With Children Hover', 'medikal'),
          'description' => __('CSS options for styling the main navigation menu first level with children when it is on dropdown mode', 'medikal'),
          'parent' => 'header',
          'selectors' => array(
            '#header .dropdown-navigation ul.nav > li.menu-item-has-children:hover > a',
          ),
          'font-color' => '#ffffff',
          'background-color' => '#3dc5df',
        ),

        'header-navigation-dropdown' => array(
          'title' => __('Header Navigation Menu Dropdown Link', 'medikal'),
          'description' => __('CSS options for styling the main navigation menu dropdown element.', 'medikal'),
          'parent' => 'header',
          'selectors' => array(
            '#header .dropdown-navigation ul.nav > li ul.dropdown-menu',
          ),
          'font-color' => '#ffffff',
          'font-weight' => '500',
          'font-size' => '13px',
          'font-family' => '',
          'link-color' => '#ffffff',
          'link-hover-color' => '#696969',
          'link-hover-background-color' => '#ffffff',
          'link-focus-color' => '#696969',
          'link-focus-background-color' => '#ffffff',
          'link-active-color' => '#696969',
          'link-active-background-color' => '#ffffff',
          'link-border-color' => '#44bbd2',
          'background-color' => '#3dc5df',
        ),


        'slicknav-button' => array(
          'title' => __('Slick Navigation Button', 'medikal'),
          'description' => __('CSS Options for styling the slick navigation button', 'medikal'),
          'parent' => 'slicknav',
          'selectors' => array(
            '.slicknav_menu a',
          ),
          'font-color' => '#696969',
          'hover-font-color' => '#3dc5df',
          'focus-font-color' => '#3dc5df',
        ),

        'slicknav-wrapper' => array(
          'title' => __('Slick Navigation Dropdown Wrapper', 'medikal'),
          'description' => __('CSS Options for styling the slick navigation dropdown wrapper element', 'medikal'),
          'parent' => 'slicknav',
          'selectors' => array(
            '.slicknav_menu > ul',
            '.slicknav_menu > ul li',
            '.slicknav_menu ul',
          ),
          'border-color' => '#f7f7f7',
          'background-color' => '#ffffff',
        ),

        'slicknav-list' => array(
          'title' => __('Slick Navigation List', 'medikal'),
          'description' => __('CSS Options for styling the slick navigation list element', 'medikal'),
          'parent' => 'slicknav',
          'selectors' => array(
            '.slicknav_menu .slicknav_nav > li',
          ),
          'link-color' => '#696969',
        ),

        'slicknav-link' => array(
          'title' => __('Slick Navigation Link', 'medikal'),
          'description' => __('CSS Options for styling the slick navigation link element', 'medikal'),
          'parent' => 'slicknav',
          'selectors' => array(
            '.slicknav_nav li > a:focus',
            '.slicknav_nav li > a:focus a',
            '.slicknav_nav li.active > a',
          ),
          'font-color' => '#3dc5df',
        ),

        'slicknav-link-hover' => array(
          'title' => __('Slick Navigation Link Hover', 'medikal'),
          'description' => __('CSS Options for styling the slick navigation link hovered element', 'medikal'),
          'parent' => 'slicknav',
          'selectors' => array(
            '.slicknav_nav li > a:hover a',
            '.slicknav_nav li > a:hover',
          ),
          'font-color' => '#3dc5df',
        ),

        'headline-bar' => array(
          'title' => __('Headline Bar Text', 'medikal'),
          'description' => __('CSS options for styling headline text bar', 'medikal'),
          'parent' => 'headline',
          'selectors' => array(
            '#headline .headline-content:after',
          ),
          'background-color' => '#3dc5df',
        ),

        'headline-title' => array(
          'title' => __('Headline Title', 'medikal'),
          'description' => __('CSS options for styling headline title text', 'medikal'),
          'parent' => 'headline',
          'selectors' => array(
            '#headline .headline-title',
          ),
          'font-color' => '#ffffff',
          'font-weight' => '600',
          'font-size' => '34px',
          'font-family' => 'Raleway',
        ),

        'headline-subtitle' => array(
          'title' => __('Headline SubTitle', 'medikal'),
          'description' => __('CSS options for styling headline subtitle text', 'medikal'),
          'parent' => 'headline',
          'selectors' => array(
            '#headline .headline-subtitle',
          ),
          'font-color' => '#ffffff',
          'font-weight' => '500',
          'font-size' => '16px',
          'font-family' => 'Raleway',
        ),


        'sitetitle' => array(
          'title' => __('Logo Title', 'medikal'),
          'description' => __('CSS options for the logo title element', 'medikal'),
          'parent' => 'header',
          'selectors' => array('.header-group .site-title .home-link'),
          'font-color' => '#333333',
          'font-family' => '',
          'font-size' => '27px',
          'font-style' => '',
          'font-weight' => '600',
        ),

        'sitedescription' => array(
          'title' => __('Logo Description', 'medikal'),
          'description' => __('CSS options for the logo description element', 'medikal'),
          'parent' => 'header',
          'selectors' => array(
            '.header-group .site-description .home-link',
          ),
          'font-color' => '#696969',
          'font-family' => '',
          'font-size' => '16px',
          'font-style' => '',
          'font-weight' => '600',
        ),


        'post-title' => array(
          'title' => __('Post Title', 'medikal'),
          'description' => __('CSS options for the post title', 'medikal'),
          'parent' => 'content',
          'selectors' => array(
            '#page .post-title',
          ),
          'font-color' => '#333333',
          'font-family' => '',
          'font-style' => '',
          'font-weight' => '700',
          'font-size' => '19px',
        ),

        'post-byline' => array(
          'title' => __('Post By Author Line', 'medikal'),
          'description' => __('CSS options for the post by author line on teasers and single post', 'medikal'),
          'parent' => 'content',
          'selectors' => array(
            '#page .post-by',
          ),
          'font-color' => '#b1b1b1',
          'font-size' => '14px',
          'font-weight' => '500',
          'font-family' => '',
        ),

        'post-byline-author-time' => array(
          'title' => __('Post By Author User and Time', 'medikal'),
          'description' => __('CSS options for the post by author line user and time text on teasers and single post', 'medikal'),
          'parent' => 'content',
          'selectors' => array(
            '#page .post-by .accents',
          ),
          'font-color' => '#333333',
          'font-size' => '14px',
          'font-weight' => '500',
          'font-family' => '',
        ),

        'post-line' => array(
          'title' => __('Post Line', 'medikal'),
          'description' => __('CSS options for the post line', 'medikal'),
          'parent' => 'content',
          'selectors' => array(
            '#page .post-line',
          ),
          'background-color' => '#c4edf5',
        ),

        'post-comment' => array(
          'title' => __('Post Comment', 'medikal'),
          'description' => __('CSS options for styling the post comment element', 'medikal'),
          'parent' => 'single',
          'selectors' => array('#post-comment'),
          'border-color' => '#f3f1f1',
        ),

        'post-comment-count' => array(
          'title' => __('Post Comment Count', 'medikal'),
          'description' => __('CSS options for styling the post comment title number element element', 'medikal'),
          'parent' => 'single',
          'selectors' => array('#post-comment .comment-title span'),
          'font-color' => '#3dc5df',
        ),

        'post-accents' => array(
          'title' => __('Post Text Accents', 'medikal'),
          'description' => __('CSS options for the text accents', 'medikal'),
          'parent' => 'content',
          'selectors' => array(
            '.accents',
            '.members-speciality.accents a'
          ),
          'font-color' => '#3dc5df',
        ),


        'maincontent' => array(
          'title' => __('Main Content Element', 'medikal'),
          'description' => __('CSS options for the main content row which hold the content and sidebar element', 'medikal'),
          'parent' => 'content',
          'selectors' => array(
            '#maincontent',
            '#pager-row',
            '.post-content:before',
            '.department-masonry-hover-wrapper'
          ),
          'background-image' => '',
          'background-color' => '#ffffff',
          'background-repeat' => '',
          'background-size' => '',
          'background-position' => '',
          'border-color' => '#f3f3f3',
        ),


        'header-underline' => array(
          'title' => __('Header with stripe', 'medikal'),
          'description' => __('CSS options for styling the header stripe element, use <em>.header-with-marker-stripe</em> css class to use this element.', 'medikal'),
          'parent' => 'content',
          'selectors' => array(
            '.header-with-marker-stripe:before',
          ),
          'background-color' => '#c4edf5',
        ),



        'pagination' => array(
          'title' => __('Pagination Element', 'medikal'),
          'description' => __('CSS options for styling the pager element, including the in post pagination links', 'medikal'),
          'parent' => 'content',
          'selectors' => array(
            '#page .page-links a',
            '#page .pagination li a',
          ),
          'background-color' => '#f7f7f7',
          'hover-background-color' => '#3dc5df',
          'font-color' => '#333333',
          'font-family' => '',
          'font-size' => '14px',
          'font-weight' => '600',
          'hover-font-color' => '#ffffff',
        ),

        'pagination-active' => array(
          'title' => __('Pagination Active Element', 'medikal'),
          'parent' => 'content',
          'description' => __('CSS options for styling the active pager element', 'medikal'),
          'selectors' => array(
            '#page .page-links > span',
            '#page .pagination li span',
          ),
          'background-color' => '#3dc5df',
          'font-color' => '#ffffff',
        ),

        'teasers-sticky' => array(
          'title' => __('Teasers Accented', 'mans'),
          'description' => __('CSS options for the teasers that is accented with variable width', 'mans'),
          'parent' => 'teasers',
          'selectors' => array(
            '#page .blog-metro .post-teasers.sticky',
          ),
          'background-color' => 'rgba(50, 89, 105, 0.8)',
          'font-color' => '#ffffff',
          'font-family' => 'Raleway',
          'font-weight' => '400',
          'font-size' => '17px',
          'heading-color' => '#ffffff',
          'heading-family' => 'Raleway',
          'heading-style' => '',
          'heading-weight' => '600',
          'heading-size' => '34px',
          'border-color' => '#eceece',
        ),

        'teasers-normal' => array(
          'title' => __('Teasers Normal', 'mans'),
          'description' => __('CSS options for the teasers that is not accented with variable width', 'mans'),
          'parent' => 'teasers',
          'selectors' => array(
            '#page .blog-metro .post-teasers.normal',
          ),
          'background-color' => '',
          'font-color' => '#676767',
          'font-family' => 'Raleway',
          'font-weight' => '400',
          'font-size' => '14px',
          'heading-color' => '#343434',
          'heading-family' => 'Raleway',
          'heading-style' => '',
          'heading-weight' => '600',
          'heading-size' => '18px',
          'border-color' => '#1396d6',
        ),

        'teasers-date' => array(
          'title' => __('Teasers Date Element', 'mans'),
          'description' => __('CSS options for the teasers date element', 'mans'),
          'parent' => 'teasers',
          'selectors' => array(
            '#page .blog-metro .post-teasers .post-date',
          ),
          'background-color' => '#3dc5df',
          'font-color' => '#ffffff',
          'font-family' => 'Raleway',
          'font-weight' => '400',
          'font-size' => '14px',
        ),

        'teaser-button' => array(
          'title' => __('Teasers Small Button', 'medikal'),
          'description' => __('CSS options for the teasers button use <em>.teaser-button</em> css class', 'medikal'),
          'parent' => 'woocommerce',
          'selectors' => array(
            '.teaser-button',
          ),
          'font-color' => '#757575',
          'font-size' => '16px',
          'background-color' => '#f7f7f7',
          'hover-font-color' => '#ffffff',
          'hover-background-color' => '#3dc5df',
          'focus-font-color' => '#ffffff',
          'focus-background-color' => '#3dc5df',
        ),


        'fotorama-thumb-active' => array(
          'title' => __('Fotorama Thumbnail Active', 'medikal'),
          'description' => __('CSS styling for the fotorama thumbnail active.', 'medikal'),
          'parent' => 'fotorama',
          'selectors' => array(
            '#page .fotorama__thumb-border',
          ),
          'border-color' => '#3dc5df',
        ),

        'fotorama-dot' => array(
          'title' => __('Fotorama Dot', 'medikal'),
          'description' => __('CSS styling for the fotorama dot navigation.', 'medikal'),
          'parent' => 'fotorama',
          'selectors' => array(
            '#page .fotorama__dot',
          ),
          'background-color' => '#f7f7f7',
          'hover-background-color' => '#3dc5df',
          'border-color' => '#f7f7f7',
          'hover-border-color' => '#3dc5df',
        ),

        'fotorama-dot-active' => array(
          'title' => __('Fotorama Active Dot', 'medikal'),
          'description' => __('CSS styling for the fotorama dot navigation active state.', 'medikal'),
          'parent' => 'fotorama',
          'selectors' => array(
            '#page .fotorama__active .fotorama__dot',
          ),
          'background-color' => '#3dc5df',
          'border-color' => '#3dc5df',
        ),


        'fotorama-arrows' => array(
          'title' => __('Fotorama Arrows', 'medikal'),
          'description' => __('CSS styling for the fotorama arrows navigation.', 'medikal'),
          'parent' => 'fotorama',
          'selectors' => array(
            '#page .fotorama__arr',
          ),
          'background-color' => '#110e0b',
          'hover-background-color' => '#00000',
          'font-color' => '#e6e6e6',
          'hover-font-color' => '#ffffff',
        ),


        'socialbutton' => array(
          'title' => __('Social Button', 'medikal'),
          'description' => __('Global styling for social buttons.', 'medikal'),
          'parent' => 'content',
          'selectors' => array('.post-social i'),
          'background-color' => '#f7f7f7',
          'font-color' => '#757575',
        ),


        'postface' => array(
          'title' => __('Postface Element', 'medikal'),
          'description' => __('CSS options for styling the postface area.', 'medikal'),
          'parent' => 'postface',
          'selectors' => array(
            '#postface'
          ),
          'background-image' => '',
          'background-color' => '#f7f7f7',
          'background-repeat' => '',
          'background-size' => '',
          'background-position' => '',
          'font-color' => '',
          'font-family' => '',
          'font-size' => '',
          'font-weight' => '',
          'link-color' => '',
          'link-hover' => '',
          'link-focus' => '',
          'heading-color' => '',
          'heading-family' => '',
          'heading-weight' => '',
          'heading-size' => '',
          'heading-style' => '',
        ),


        'footer' => array(
          'title' => __('Footer Element', 'medikal'),
          'description' => __('CSS options for styling the footer area.', 'medikal'),
          'parent' => 'footer',
          'selectors' => array(
            '#footer'
          ),
          'background-image' => '',
          'background-color' => '#212121',
          'background-repeat' => '',
          'background-size' => '',
          'background-position' => '',
          'border-color' => '#292929',
          'font-color' => '#757575',
          'font-family' => '',
          'font-size' => '14px',
          'font-weight' => '400',
          'link-color' => '#757575',
          'link-hover' => '#3dc5df',
          'link-focus' => '#3dc5df',
          'heading-color' => '#f7f7f7',
          'heading-family' => '',
          'heading-weight' => '600',
          'heading-size' => '18px',
          'heading-style' => '',
        ),

        'footer-socialbutton' => array(
          'title' => __('Social Button', 'medikal'),
          'description' => __('Social icon in footer area.', 'medikal'),
          'parent' => 'footer',
          'selectors' => array('#footer .post-social i'),
          'background-color' => '#151515',
          'font-color' => '#757575',
        ),


        'footer-form-elements' => array(
          'title' => __('Form Elements', 'medikal'),
          'description' => __('Global styling for input, select and textarea form elements.', 'medikal'),
          'parent' => 'footer',
          'selectors' => array(
            '#footer .select2-search .select2-input',
            '#footer .select2-results',
            '#footer .select2-drop',
            '#page v.select2-container .select2-choice',
            '#page #footer select',
            '#page #footer textarea',
            '#page #footer input[type="text"]',
            '#page #footer input[type="password"]',
            '#page #footer input[type="datetime"]',
            '#page #footer input[type="datetime-local"]',
            '#page #footer input[type="date"]',
            '#page #footer input[type="month"]',
            '#page #footer input[type="time"]',
            '#page #footer input[type="week"]',
            '#page #footer input[type="number"]',
            '#page #footer input[type="email"]',
            '#page #footer input[type="url"]',
            '#page #footer input[type="search"]',
            '#page #footer input[type="tel"]',
            '#page #footer input[type="color"]',
          ),
          'background-color' => '#292929',
          'font-color' => '',
          'border-color' => '#292929',
          'focus-background-color' => '',
          'focus-font-color' => '',
          'focus-border-color' => '#696969',
        ),

        'label-elements' => array(
          'title' => __('Form Label Elements', 'medikal'),
          'description' => __('Global styling for form label elements.', 'medikal'),
          'parent' => 'footer',
          'selectors' => array(
            '#page #footer .jvFloat .placeHolder',
            '#footer label',
          ),
          'font-color' => '#696969',
        ),

        'uneditable-form-elements' => array(
          'title' => __('Uneditable & Disabled Form Elements', 'medikal'),
          'description' => __('Global styling for input and textarea form elements when it is not editable or disabled.', 'medikal'),
          'parent' => 'footer',
          'selectors' => array(
            '#footer .uneditable-input',
            '#footer .uneditable-textarea',
            '#footer input[disabled]',
            '#footer select[disabled]',
            '#footer textarea[disabled]',
            '#footer input[readonly]',
            '#footer select[readonly]',
            '#footer textarea[readonly]',
          ),
          'background-color' => '#191919',
          'font-color' => '#393939',
        ),

        'placeholder-form-elements' => array(
          'title' => __('Form Elements Placeholder', 'medikal'),
          'description' => __('Global styling for input and textarea placeholder.', 'medikal'),
          'parent' => 'footer',
          'selectors' => array(
            '#footer input:-moz-placeholder',
            '#footer textarea:-moz-placeholder',
            '#footer input:-ms-input-placeholder',
            '#footer textarea:-ms-input-placeholder',
            '#footer input::-webkit-input-placeholder',
            '#footer textarea::-webkit-input-placeholder',
          ),
          'font-color' => '#898989',
        ),

        'footer button-primary' => array(
          'title' => __('Button Primary Element', 'medikal'),
          'description' => __('CSS options for styling the button primary', 'medikal'),
          'parent' => 'footer',
          'selectors' => array(
            'html #footer .btn-primary',
            'html #footer .btn-primary.active',
            'html #footer a.btn-primary',
            'html #footer .btn-primary.disabled',
            'html #footer .active > .btn-primary',
            'html #footer .btn', 'html .btn.active',
            'html #footer .active > .btn',
            'html #footer input[type=submit]',
            'html #footer  input[type=button]',
            'html #footer #page a.btn',
            'html #footer #page #respond #submit',
            '.woocommerce #footer #respond input#submit',
            '.woocommerce #footer a.button',
            '.woocommerce #footer button.button',
            '.woocommerce #footer input.button',
            '#page #footer  .widget_shopping_cart_content .buttons .button',
            '#page #footer .widget_shopping_cart_content .buttons .button',
            '#page #footer .woocommerce .button',
            '#page .shop-pages #footer #place_order',
            '#page .shop-pages #footer .wc-proceed-to-checkout a.checkout-button',
            '#page .shop-pages #footer .shop_table .actions .button',
            '#page .shop-single #footer .product-cart button',
            '#page #footer .column .product-cart .add_to_cart_button',
          ),
          'font-color' => '#696969',
          'background-color' => 'transparent',
          'border-color' => '#f7f7f7',
          'hover-font-color' => '#ffffff',
          'focus-font-color' => '#ffffff',
          'active-font-color' => '#ffffff',
          'hover-background-color' => 'transparent',
          'focus-background-color' => 'transparent',
          'active-background-color' => 'transparent',
          'hover-border-color' => '#ffffff',
          'focus-border-color' => '#ffffff',
          'active-border-color' => '#ffffff',
        ),

        'full-footer' => array(
          'title' => __('Full Footer Element', 'medikal'),
          'description' => __('CSS options for styling the full footer area.', 'medikal'),
          'parent' => 'full-footer',
          'selectors' => array(
            '#full-footer'
          ),
          'background-image' => '',
          'background-color' => '#000000',
          'background-repeat' => '',
          'background-size' => '',
          'background-position' => '',
          'font-color' => '#757575',
          'font-family' => '',
          'font-size' => '14px',
          'font-weight' => '400',
          'link-color' => '#f7f7f7',
          'link-hover' => '#3dc5df',
          'link-focus' => '#3dc5df',
          'heading-color' => '#f7f7f7',
          'heading-family' => '',
          'heading-weight' => '600',
          'heading-size' => '18px',
          'heading-style' => '',
        ),




        'product-teaser' => array(
          'title' => __('Product Teasers', 'medikal'),
          'description' => __('CSS options for the products teasers', 'medikal'),
          'parent' => 'woocommerce',
          'selectors' => array(
            '#page .column .product',
            '#page .column .top',
            '#page .column .back',
          ),
          'background-image' => '',
          'background-color' => '#ffffff',
          'background-repeat' => '',
          'background-size' => '',
          'background-position' => '',
          'font-color' => '',
          'font-weight' => '',
          'font-style' => '',
          'font-family' => '',
          'font-size' => '14px',
          'heading-color' => '',
          'heading-weight' => '700',
          'heading-style' => '',
          'heading-family' => '',
          'heading-size' => '15px',
        ),

        'product-teaser-star' => array(
          'title' => __('Product Teasers Star Rating', 'medikal'),
          'description' => __('CSS options for the products teasers star rating', 'medikal'),
          'parent' => 'woocommerce',
          'selectors' => array(
            '#page .star-rating',
            '#page .column .product-rating .star-rating',
          ),
          'font-color' => '#3dc5df',
          'font-size' => '14px',
        ),

        'product-teaser-price' => array(
          'title' => __('Product Teasers Price', 'medikal'),
          'description' => __('CSS options for the products teasers price element', 'medikal'),
          'parent' => 'woocommerce',
          'selectors' => array(
            '#page .column .product-price .price',
          ),
          'font-color' => '#3dc5df',
          'font-size' => '18px',
          'font-weight' => '700',
          'font-style' => '',
        ),


        'product-teaser-sale' => array(
          'title' => __('Product Teasers on sale', 'medikal'),
          'description' => __('CSS options for the products teasers on sale', 'medikal'),
          'parent' => 'woocommerce',
          'selectors' => array(
            '#page .column .product .onsale',
          ),
          'font-color' => '#ffffff',
          'font-size' => '14px',
          'font-weight' => '600',
          'font-style' => '',
          'background-color' => '#e7493e',
          'border-color' => '#ffffff'
        ),

        'product-teaser-flip-button' => array(
          'title' => __('Product Teasers Button', 'medikal'),
          'description' => __('CSS options for the products teasers button when on flip mode', 'medikal'),
          'parent' => 'woocommerce',
          'selectors' => array(
            '#page .button-groups .added_to_cart.wc-forward',
            '#page .shop-catalog .column .button-groups .teaser-button',
          ),
          'font-color' => '#757575',
          'font-size' => '16px',
          'background-color' => '#f7f7f7',
          'hover-font-color' => '#ffffff',
          'hover-background-color' => '#3dc5df',
          'focus-font-color' => '#ffffff',
          'focus-background-color' => '#3dc5df',
        ),

        'product-teaser-flip-button-added' => array(
          'title' => __('Product Teasers Added Button', 'medikal'),
          'description' => __('CSS options for the products teasers button when on flip mode and added to cart state', 'medikal'),
          'parent' => 'woocommerce',
          'selectors' => array(
            '#page .button-groups .add_to_cart_button.added:after',
          ),
          'font-color' => '#ffffff',
          'font-size' => '8px',
          'background-color' => '#ff0000',
        ),

        'product-teaser-detail-box' => array(
          'title' => __('Product Teasers Grid - Teaser Box', 'medikal'),
          'description' => __('CSS options for the products teasers box when on using grid template', 'medikal'),
          'parent' => 'woocommerce',
          'selectors' => array(
            '#page .product-template-detail',
          ),
          'background-color' => '#f7f7f7',
        ),

        'product-teaser-detail-price-box' => array(
          'title' => __('Product Teasers Grid - Price Box', 'medikal'),
          'description' => __('CSS options for the products teasers price box when on using grid template', 'medikal'),
          'parent' => 'woocommerce',
          'selectors' => array(
            '#page .product-template-detail .price-row',
          ),
          'background-color' => '#f7f7f7',
        ),

        'product-teaser-detail-cart-button' => array(
          'title' => __('Product Teasers Grid - Cart Button', 'medikal'),
          'description' => __('CSS options for the products teasers cart button when on using grid template', 'medikal'),
          'parent' => 'woocommerce',
          'selectors' => array(
            '#page .shop-catalog .product-template-detail .product-cart .added_to_cart',
          ),
          'background-color' => '#028eda',
          'font-color' => '#ffffff',
        ),



        'product-single-flash' => array(
          'title' => __('Product Single Flash Banner', 'medikal'),
          'description' => __('CSS options for the products single flash banner', 'medikal'),
          'parent' => 'woocommerce',
          'selectors' => array(
            '#page .shop-single .product-flash span',
          ),
          'font-color' => '#ffffff',
          'font-size' => '12px',
          'font-weight' => '600',
          'font-style' => '',
          'background-color' => '#e7493e',
        ),

        'product-single-star' => array(
          'title' => __('Product Single Star Rating', 'medikal'),
          'description' => __('CSS options for the products single star rating', 'medikal'),
          'parent' => 'woocommerce',
          'selectors' => array(
            '#page .shop-single .product-rating .star-rating',
          ),
          'font-color' => '#b1b1b1',
          'hover-font-color' => '#3dc5df',
          'font-size' => '16px',
        ),



        'product-single-price' => array(
          'title' => __('Product Single Price', 'medikal'),
          'description' => __('CSS options for the products single price', 'medikal'),
          'parent' => 'woocommerce',
          'selectors' => array(
            '#page .shop-single .product-price',
          ),
          'font-color' => '#3dc5df',
          'font-weight' => 800,
          'font-style' => '',
          'font-family' => '',
          'font-size' => '24px',
        ),

        'product-single-price-slashed' => array(
          'title' => __('Product Single Price Slashed', 'medikal'),
          'description' => __('CSS options for the products single price when it slashed', 'medikal'),
          'parent' => 'woocommerce',
          'selectors' => array(
            '#page .shop-single .price del',
          ),
          'font-color' => '#b1b1b1',
          'font-weight' => 800,
          'font-style' => '',
          'font-family' => '',
          'font-size' => '24px',
        ),

        'product-single-price-between' => array(
          'title' => __('Product Single Price Variation', 'medikal'),
          'description' => __('CSS options for the products single price when it has variations', 'medikal'),
          'parent' => 'woocommerce',
          'selectors' => array(
            '#page .shop-single .price ins',
          ),
          'font-color' => '#3dc5df',
          'font-weight' => 800,
          'font-style' => '',
          'font-family' => '',
          'font-size' => '24px',
        ),


        'product-tabs-tab' => array(
          'title' => __('Product Tab\'s Tab', 'medikal'),
          'description' => __('CSS options for the single product tab', 'medikal'),
          'parent' => 'woocommerce',
          'selectors' => array(
            '#page #product-tabs ul.tabs li',
          ),
          'background-color' => 'transparent',
          'border-color' => '#696969',
          'font-color' => '#696969',
          'font-weight' => 700,
          'font-style' => '',
          'font-family' => '',
          'font-size' => '14px',
          'hover-background-color' => '#3dc5df',
          'hover-border-color' => '#3dc5df',
          'hover-font-color' => '#ffffff',
          'active-background-color' => '#3dc5df',
          'active-border-color' => '#3dc5df',
          'active-font-color' => '#ffffff',
        ),


        'shop-slider' => array(
          'title' => __('Price Slider - Background', 'medikal'),
          'description' => __('CSS options for the price filter slider background', 'medikal'),
          'parent' => 'woocommerce',
          'selectors' => array(
            '#page .price_slider',
          ),
          'background-color' => '#cccccc',
        ),

        'shop-slider-slide' => array(
          'title' => __('Price Slider - Slide', 'medikal'),
          'description' => __('CSS options for the price filter slider slide background', 'medikal'),
          'parent' => 'woocommerce',
          'selectors' => array(
            '#page .price_slider .ui-slider-range',
            '#page .price_slider .ui-slider-handle',
          ),
          'background-color' => '#3dc5df',
        ),


        'slickcarousel-dots' => array(
          'title' => __('Slick Carousel Dots', 'medikal'),
          'description' => __('CSS Options for styling the slick carousel dots', 'medikal'),
          'parent' => 'html',
          'selectors' => array(
            '.slick-dots li button.btn',
          ),
          'border-color' => '#e1e1e1',
        ),

        'slickcarousel-dots-active' => array(
          'title' => __('Slick Carousel Dots Active', 'medikal'),
          'description' => __('CSS Options for styling the slick carousel dots on active mode', 'medikal'),
          'parent' => 'html',
          'selectors' => array(
            '#page .slick-dots li.slick-active button.btn',
          ),
          'background-color' => '#3dc5df',
          'border-color' => '#3dc5df',
        ),


        'slickcarousel-arrows' => array(
          'title' => __('Slick Arrows', 'medikal'),
          'description' => __('CSS Options for styling the slick carousel arrow', 'medikal'),
          'parent' => 'html',
          'selectors' => array(
            '#page .slick-prev',
            '#page .slick-next',
          ),
          'background-color' => '#ffffff',
          'font-color' => '#3dc5df',
        ),


        'table' => array(
          'title' => __('HTML Table Rows Element', 'medikal'),
          'description' => __('Global styling for HTML Table Rows.', 'medikal'),
          'parent' => 'html',
          'selectors' => array(
            'table > thead > tr > th',
            'table > tbody > tr > th',
            'table > tfoot > tr > th',
            'table > thead > tr > td',
            'table > tbody > tr > td',
            'table > tfoot > tr > td',
            '.woocommerce table.shop_attributes th',
            '.woocommerce table.shop_attributes td',
          ),
          'background-color' => '',
          'font-color' => '',
          'border-color' => '#f3f3f3',
        ),

        'table-odd' => array(
          'title' => __('HTML Table Rows Zebra', 'medikal'),
          'description' => __('Global styling for HTML Table Rows Odd number.', 'medikal'),
          'parent' => 'html',
          'selectors' => array(
            '#page table > tbody > tr:nth-child(even) > td',
            '#page table > tbody > tr:nth-child(even) > th',
          ),
          'background-color' => '#fcfcfc',
          'font-color' => '',
          'border-color' => '',
        ),


        'blockquote' => array(
          'title' => __('Blockquote Element', 'medikal'),
          'description' => __('Global styling for HTML blockquote element.', 'medikal'),
          'parent' => 'html',
          'selectors' => array(
            '#page blockquote',
          ),
          'background-color' => '',
          'border-color' => '#c4edf5',
          'font-color' => '',
          'font-size' => '18px',
        ),


        'search-box' => array(
          'title' => __('Search Box Element', 'medikal'),
          'description' => __('Global styling for search box input element.', 'medikal'),
          'parent' => 'content',
          'selectors' => array(
            '#page .search-form',
          ),
          'background-color' => '#f7f7f7',
          'border-color' => '#f7f7f7',
        ),


        'well' => array(
          'title' => __('Well Element', 'medikal'),
          'description' => __('CSS options for styling bootstrap well element', 'medikal'),
          'parent' => 'bootstrap',
          'selectors' => array('#page .well'),
          'font-color' => '',
          'background-color' => '#f7f7f7',
          'heading-color' => '',
          'link-visited' => '',
          'link-color' => '',
          'link-hover' => '',
          'link-focus' => '',
        ),


        'badge' => array(
          'title' => __('Badge Element', 'medikal'),
          'description' => __('CSS options for styling bootstrap badge element', 'medikal'),
          'parent' => 'bootstrap',
          'selectors' => array('#page .badge'),
          'font-color' => '#ffffff',
          'background-color' => '#e8e8e8',
          'font-color' => '#999999',
        ),

        'panel' => array(
          'title' => __('Panel Wrapper', 'medikal'),
          'description' => __('Global styling Bootstrap panel wrapper including the list group, tabs and accordions.', 'medikal'),
          'parent' => 'bootstrap',
          'selectors' => array(
            '#page .area .panel-default',
            '#page .tab-content',
          ),
          'background-color' => '#fafafa',
          'font-color' => '#565656',
          'border-color' => '#e0e0e0',
        ),

        'panel-heading' => array(
          'title' => __('Panel Heading', 'medikal'),
          'description' => __('Global styling Bootstrap panel heading elements including the list group, tabs and accordions.', 'medikal'),
          'parent' => 'bootstrap',
          'selectors' => array(
            '#page .panel-default > .panel-heading',
            '#page .panel-default > .panel-heading > .panel-title',
            '#page .nav-tabs',
          ),
          'background-color' => '#eeeeee',
          'link-color' => '#353535',
          'link-hover' => '#353535',
          'link-focus' => '#353535',
          'link-visited' => '#353535',
          'font-color' => '#353535',
          'font-weight' => 'normal',
          'font-size' => '16px',
          'border-color' => '',
        ),


        'panel-heading-tabs' => array(
          'title' => __('Panel Heading Tabs', 'medikal'),
          'description' => __('Global styling Bootstrap panel heading tabbed elements.', 'medikal'),
          'parent' => 'bootstrap',
          'selectors' => array(
            '#page .panel-default > .panel-heading li',
          ),
          'background-color' => '',
          'link-color' => '',
          'link-hover' => '',
          'link-focus' => '',
          'link-visited' => '',
          'font-color' => '',
        ),

        'panel-heading-tabs-active' => array(
          'title' => __('Panel Heading Active State', 'medikal'),
          'description' => __('Global styling Bootstrap panel heading on active state.', 'medikal'),
          'parent' => 'bootstrap',
          'selectors' => array(
            '#page .panel-default > .panel-heading li.ui-state-active',
            '#page .panel-default > .panel-heading.ui-state-active',
            '#page .nav-tabs > li.active',
          ),
          'background-color' => '#3dc5df',
          'link-background-color' => '#3dc5df',
          'link-color' => '#ffffff',
          'link-hover' => '#ffffff',
          'link-focus' => '#ffffff',
          'link-visited' => '#ffffff',
          'font-color' => '#ffffff',
        ),


        'form-elements' => array(
          'title' => __('Form Elements', 'medikal'),
          'description' => __('Global styling for input, select and textarea form elements.', 'medikal'),
          'parent' => 'form',
          'selectors' => array(
            '.select2-search .select2-input',
            '.select2-results',
            '.select2-drop',
            '#page .select2-container .select2-choice',
            '#page select',
            '#page textarea',
            '#page input[type="text"]',
            '#page input[type="password"]',
            '#page input[type="datetime"]',
            '#page input[type="datetime-local"]',
            '#page input[type="date"]',
            '#page input[type="month"]',
            '#page input[type="time"]',
            '#page input[type="week"]',
            '#page input[type="number"]',
            '#page input[type="email"]',
            '#page input[type="url"]',
            '#page input[type="search"]',
            '#page input[type="tel"]',
            '#page input[type="color"]',
          ),
          'background-color' => '#f7f7f7',
          'font-color' => '',
          'border-color' => '#f7f7f7',
          'focus-background-color' => '',
          'focus-font-color' => '',
          'focus-border-color' => '#696969',
        ),

        'label-elements' => array(
          'title' => __('Form Label Elements', 'medikal'),
          'description' => __('Global styling for form label elements.', 'medikal'),
          'parent' => 'form',
          'selectors' => array(
            '#page .jvFloat .placeHolder',
            'label',
          ),
          'font-color' => '#696969',
        ),

        'uneditable-form-elements' => array(
          'title' => __('Uneditable & Disabled Form Elements', 'medikal'),
          'description' => __('Global styling for input and textarea form elements when it is not editable or disabled.', 'medikal'),
          'parent' => 'form',
          'selectors' => array(
            '.uneditable-input',
            '.uneditable-textarea',
            'input[disabled]',
            'select[disabled]',
            'textarea[disabled]',
            'input[readonly]',
            'select[readonly]',
            'textarea[readonly]',
          ),
          'background-color' => '#ededed',
          'font-color' => '#aaaaaa',
        ),

        'placeholder-form-elements' => array(
          'title' => __('Form Elements Placeholder', 'medikal'),
          'description' => __('Global styling for input and textarea placeholder.', 'medikal'),
          'parent' => 'form',
          'selectors' => array(
            'input:-moz-placeholder',
            'textarea:-moz-placeholder',
            'input:-ms-input-placeholder',
            'textarea:-ms-input-placeholder',
            'input::-webkit-input-placeholder',
            'textarea::-webkit-input-placeholder',
          ),
          'font-color' => '#696969',
        ),

        'button-primary' => array(
          'title' => __('Button Primary Element', 'medikal'),
          'description' => __('CSS options for styling the button primary', 'medikal'),
          'parent' => 'form',
          'selectors' => array(
            'html .btn-primary',
            'html .btn-primary.active',
            'html a.btn-primary',
            'html .btn-primary.disabled',
            'html .active > .btn-primary',
            'html .btn', 'html .btn.active',
            'html .active > .btn',
            'html input[type=submit]',
            'html input[type=button]',
            'html #page a.btn',
            'html #page #respond #submit',
            '.woocommerce #respond input#submit',
            '.woocommerce a.button',
            '.woocommerce button.button',
            '.woocommerce input.button',
            '#page #sidebar .widget_shopping_cart_content .buttons .button',
            '#page .widget_shopping_cart_content .buttons .button',
            '#page .woocommerce .button',
            '#page .shop-pages #place_order',
            '#page .shop-pages .wc-proceed-to-checkout a.checkout-button',
            '#page .shop-pages .shop_table .actions .button',
            '#page .shop-single .product-cart button',
            '#page .column .product-cart .add_to_cart_button',
          ),
          'font-color' => '#333333',
          'background-color' => 'transparent',
          'border-color' => '#333333',
          'hover-font-color' => '#3dc5df',
          'focus-font-color' => '#3dc5df',
          'active-font-color' => '#3dc5df',
          'hover-background-color' => 'transparent',
          'focus-background-color' => 'transparent',
          'active-background-color' => 'transparent',
          'hover-border-color' => '#3dc5df',
          'focus-border-color' => '#3dc5df',
          'active-border-color' => '#3dc5df',
        ),



        'widget_tagcloud' => array(
          'title' => __('Tag Cloud', 'medikal'),
          'description' => __('CSS options for configuring the style for tag cloud widget', 'medikal'),
          'parent' => 'widgets',
          'selectors' => array(
            '#page .widget_tag_cloud .tagcloud a',
            '#page #sidebar .widget_tag_cloud .tagcloud a',
            '.widget_product_tag_cloud .tagcloud a',
          ),
          'background-color' => 'transparent',
          'font-weight' => '300',
          'font-color' => '#6e6e6e',
          'border-color' => '#dedddb',
          'hover-font-color' => '#ffffff',
          'hover-background-color' => '#3dc5df',
          'focus-background-color' => '#3dc5df',
          'hover-border-color' => '#3dc5df',
          'focus-border-color' => '#3dc5df',
        ),

        'widget_calendar' => array(
          'title' => __('Calendar', 'medikal'),
          'description' => __('CSS options for configuring the calendar element', 'medikal'),
          'parent' => 'widgets',
          'selectors' => array(
            '#page .widget_calendar .calendar-wrapper',
          ),
          'background-color' => '#eeeeee',
          'font-family' => '',
          'font-color' => '',
          'link-color' => '',
          'link-focus' => '',
          'link-hover' => '',
        ),


        'widget_calendar_accent' => array(
          'title' => __('Calendar Accents', 'medikal'),
          'description' => __('CSS options for configuring the calendar today and large calendar element', 'medikal'),
          'parent' => 'widgets',
          'selectors' => array(
            '#page .widget_calendar #today',
            '.widget_calendar .calendar-large-date'
          ),
          'background-color' => '#3dc5df',
          'font-family' => '',
          'font-color' => '#ffffff',
          'link-color' => '#ffffff',
          'link-focus' => '#ffffff',
        ),


        'department-icon' => array(
          'title' => __('Department Icons', 'medikal'),
          'description' => __('CSS for styling the department icon element', 'medikal'),
          'parent' => 'department',
          'selectors' => array(
            '.department-icon i',
          ),
          'font-color' => '#333333',
        ),



        'department-slogan' => array(
          'title' => __('Slogan Box', 'medikal'),
          'description' => __('CSS for styling the department single slogan box', 'medikal'),
          'parent' => 'department',
          'selectors' => array(
            '.department-sloganbox',
          ),
          'background-color' => 'rgba(255,255,255, 0.8)',
        ),

        'department-slogan-quote' => array(
          'title' => __('Slogan Box Quotes', 'medikal'),
          'description' => __('CSS for styling the department single slogan box quotes element', 'medikal'),
          'parent' => 'department',
          'selectors' => array(
            '.department-sloganbox .department-slogan:before',
            '.department-sloganbox .department-slogan:after',
          ),
          'font-color' => '#3dc5df',
        ),


        'department-thumb-list' => array(
          'title' => __('Department Thumb List', 'medikal'),
          'description' => __('CSS for styling the department thumblist carousel template', 'medikal'),
          'parent' => 'visualcomposer',
          'selectors' => array(
            '#page .department-thumb-list',
            '#page .department-thumb-list .slick-items',
          ),
          'border-color' => '#78d7e9',
          'background-color' => '#3dc5df',
          'font-family' => 'Raleway',
          'font-size' => '18px',
          'font-weight' => 600,
          'font-color' => '#ffffff',
        ),

        'department-thumb-list-active' => array(
          'title' => __('Department Thumb List Active', 'medikal'),
          'description' => __('CSS for styling the department thumblist carousel template when active', 'medikal'),
          'parent' => 'department',
          'selectors' => array(
            '#page .department-thumb-list .slick-center',
          ),
          'background-color' => '#ffffff',
          'font-color' => '#333333',
        ),


        'department-carousel-full' => array(
          'title' => __('Department Carousel Full Heading', 'medikal'),
          'description' => __('CSS for styling the department carousel template heading element', 'medikal'),
          'parent' => 'visualcomposer',
          'selectors' => array(
            '.department-carousel-full .slick-items > .item .header-with-marker-stripe',
          ),
          'font-size' => '45px',
          'font-color' => '#333333',
          'font-weight' => 700,
          'font-family' => 'Raleway',
          'line-height' => '57px',
        ),

        'services-carousel-flip-top' => array(
          'title' => __('Services Carousel Flip - Top Part', 'medikal'),
          'description' => __('CSS for styling the service carousel flip template top part', 'medikal'),
          'parent' => 'services',
          'selectors' => array(
            '.services-flip-items .post-split.top .post-back-inner',
          ),
          'background-color' => '#f7f7f7',
          'font-color' => '',
          'heading-color' => '',
          'icon-font-color' => '#3dc5dd'
        ),


        'services-carousel-flip-bottom' => array(
          'title' => __('Services Carousel Flip - Bottom Part', 'medikal'),
          'description' => __('CSS for styling the service carousel flip template bottom part', 'medikal'),
          'parent' => 'services',
          'selectors' => array(
            '.services-flip-items .post-split.bottom .post-back-inner',
          ),
          'background-color' => '#ffffff',
          'font-color' => '',
          'heading-color' => '',
        ),

        'services-carousel-simple' => array(
          'title' => __('Services Carousel Simple', 'medikal'),
          'description' => __('CSS for styling the service carousel simple template', 'medikal'),
          'parent' => 'services',
          'selectors' => array(
            '.services-carousel-simple',
          ),
          'background-color' => '#4ad9df',
          'font-color' => '#ffffff',
        ),

        'services-carousel-simple-icon' => array(
          'title' => __('Services Carousel Simple - Icon', 'medikal'),
          'description' => __('CSS for styling the service carousel simple template icon element', 'medikal'),
          'parent' => 'services',
          'selectors' => array(
            '.services-carousel-simple .services-icon i',
          ),
          'font-color' => '#ffffff',
          'font-size' => '160px',
          'line-height' => '160px',
        ),

        'services-carousel-simple-title' => array(
          'title' => __('Services Carousel Simple - Title', 'medikal'),
          'description' => __('CSS for styling the service carousel simple template title element', 'medikal'),
          'parent' => 'services',
          'selectors' => array(
            '#page .services-carousel-simple .post-title a',
          ),
          'font-color' => '#ffffff',
          'font-size' => '18px',
          'font-weight' => 700,
          'font-family' => 'Raleway',
          'line-height' => '24px',
        ),

        'services-carousel-simple-excerpt' => array(
          'title' => __('Services Carousel Simple - Excerpt', 'medikal'),
          'description' => __('CSS for styling the service carousel simple template excerpt element', 'medikal'),
          'parent' => 'services',
          'selectors' => array(
            '#page .services-carousel-simple .post-excerpt',
          ),
          'font-color' => '#ffffff',
          'font-size' => '14px',
          'font-weight' => 500,
          'font-family' => 'Raleway',
          'line-height' => '24px',
        ),

        'services-carousel-simple-first' => array(
          'title' => __('Services Carousel Simple - 1/4', 'medikal'),
          'description' => __('CSS for styling the service carousel simple template first every four slides', 'medikal'),
          'parent' => 'services',
          'selectors' => array(
            '.services-carousel-simple .slick-items:nth-child(4n+1)',
          ),
          'background-color' => '#3dc5df',
          'font-color' => '#ffffff',
        ),

        'services-carousel-simple-second' => array(
          'title' => __('Services Carousel Simple - 2/4', 'medikal'),
          'description' => __('CSS for styling the service carousel simple template second every four slides', 'medikal'),
          'parent' => 'services',
          'selectors' => array(
            '.services-carousel-simple .slick-items:nth-child(4n+2)',
          ),
          'background-color' => '#46BDDF',
          'font-color' => '#fff',
        ),

        'services-carousel-simple-third' => array(
          'title' => __('Services Carousel Simple - 3/4', 'medikal'),
          'description' => __('CSS for styling the service carousel simple template third every four slides', 'medikal'),
          'parent' => 'services',
          'selectors' => array(
            '.services-carousel-simple .slick-items:nth-child(4n+3)',
          ),
          'background-color' => '#37b3cc',
          'font-color' => '#ffffff',
        ),

        'services-carousel-simple-fourth' => array(
          'title' => __('Services Carousel Simple - 4/4', 'medikal'),
          'description' => __('CSS for styling the service carousel simple template fourth every four slides', 'medikal'),
          'parent' => 'services',
          'selectors' => array(
            '.services-carousel-simple .slick-items:nth-child(4n+4)',
          ),
          'background-color' => '#35acc4',
          'font-color' => '#ffffff',
        ),


        'services-carousel-simple-center' => array(
          'title' => __('Services Carousel Simple - Center', 'medikal'),
          'description' => __('CSS for styling the service carousel simple template centered slide', 'medikal'),
          'parent' => 'services',
          'selectors' => array(
            '#page .services-carousel-simple .slick-center',
          ),
          'background-color' => '#ffffff',
          'font-color' => '#696969',
          'box-shadow' => '0 0 24px #aaa',
        ),


        'services-carousel-simple-center-icon' => array(
          'title' => __('Services Carousel Simple - Center Icon', 'medikal'),
          'description' => __('CSS for styling the service carousel simple template centered slide icon element', 'medikal'),
          'parent' => 'services',
          'selectors' => array(
            '#page .services-carousel-simple .slick-center .services-icon i',
          ),
          'font-color' => '#3dc5df',
        ),


        'services-carousel-simple-center-title' => array(
          'title' => __('Services Carousel Simple - Center Title', 'medikal'),
          'description' => __('CSS for styling the service carousel simple template centered slide title element', 'medikal'),
          'parent' => 'services',
          'selectors' => array(
            '#page .services-carousel-simple .slick-center .post-title a',
          ),
          'font-color' => '#333333',
        ),

        'services-carousel-simple-center-excerpt' => array(
          'title' => __('Services Carousel Simple - Center Icon', 'medikal'),
          'description' => __('CSS for styling the service carousel simple template centered slide excerpt element', 'medikal'),
          'parent' => 'services',
          'selectors' => array(
            '#page .services-carousel-simple .slick-center .post-excerpt',
          ),
          'font-color' => '#696969',
        ),

        'services-table-box' => array(
          'title' => __('Services Table - Box', 'medikal'),
          'description' => __('CSS for styling the service table main box', 'medikal'),
          'parent' => 'services',
          'selectors' => array(
            '#page .service-table-row',
          ),
          'background-color' => '',
          'border-color' => '#eeeeee',
        ),

        'services-table-description' => array(
          'title' => __('Services Table - Description Box', 'medikal'),
          'description' => __('CSS for styling the service table description box', 'medikal'),
          'parent' => 'services',
          'selectors' => array(
            '#page .service-table-listing .services-column-description',
          ),
          'font-color' => '#ffffff',
          'font-size' => '18px',
          'font-weight' => '500',
          'font-family' => 'Raleway',
          'background-color' => 'rgba(61, 197, 223, 1)',
        ),


        'services-table-time' => array(
          'title' => __('Services Table - Time Box', 'medikal'),
          'description' => __('CSS for styling the service table time box', 'medikal'),
          'parent' => 'services',
          'selectors' => array(
            '#page .service-table-listing .services-time',
          ),
          'font-color' => '#767676',
          'font-size' => '18px',
          'font-weight' => '400',
          'font-family' => 'Raleway',
          'background-color' => '#f7f7f7',
        ),

        'services-table-days' => array(
          'title' => __('Services Table - Days Element', 'medikal'),
          'description' => __('CSS for styling the service table days element box', 'medikal'),
          'parent' => 'services',
          'selectors' => array(
            '#page .service-table-listing .services-time-days',
          ),
          'font-color' => '',
          'font-size' => '14px',
          'font-weight' => '',
          'font-family' => '',
        ),

        'services-table-photo' => array(
          'title' => __('Services Table - Member Photo Element', 'medikal'),
          'description' => __('CSS for styling the service table member photo', 'medikal'),
          'parent' => 'services',
          'selectors' => array(
            '#page .service-table-listing .services-member-photo',
          ),
          'border-color' => '#eeeeee'
        ),


        'services-table-member-name' => array(
          'title' => __('Services Table - Member Name', 'medikal'),
          'description' => __('CSS for styling the service table member name element', 'medikal'),
          'parent' => 'services',
          'selectors' => array(
            '#page .service-table-listing .services-member-name',
          ),
          'font-color' => '',
          'font-size' => '16px',
          'font-weight' => '600',
          'font-family' => '',
        ),


        'members-education-table-box' => array(
          'title' => __('Members Education Table - Box', 'medikal'),
          'description' => __('CSS for styling the members education table main box', 'medikal'),
          'parent' => 'members',
          'selectors' => array(
            '#page .education-table .members-table-row',
          ),
          'background-color' => '',
          'border-color' => '#eeeeee',
        ),

        'members-education-table-title' => array(
          'title' => __('Members Education Table - Title Box', 'medikal'),
          'description' => __('CSS for styling the members education table title box', 'medikal'),
          'parent' => 'members',
          'selectors' => array(
            '#page .education-table .members-column-title',
          ),
          'font-color' => '#ffffff',
          'font-size' => '18px',
          'font-weight' => '500',
          'font-family' => 'Raleway',
          'background-color' => 'rgba(61, 197, 223, 1)',
        ),


        'members-education-table-time' => array(
          'title' => __('Members Education Table - Time Box', 'medikal'),
          'description' => __('CSS for styling the members education table time box', 'medikal'),
          'parent' => 'members',
          'selectors' => array(
            '#page .education-table .members-time',
          ),
          'font-color' => '#767676',
          'font-size' => '18px',
          'font-weight' => '400',
          'font-family' => 'Raleway',
          'background-color' => '#f7f7f7',
        ),

        'members-education-table-description' => array(
          'title' => __('Members Education Table - Description Box', 'medikal'),
          'description' => __('CSS for styling the members education table time box', 'medikal'),
          'parent' => 'members',
          'selectors' => array(
            '#page .education-table .members-description',
          ),
          'font-color' => '',
          'font-size' => '16px',
          'font-weight' => '',
          'font-family' => '',
          'background-color' => '',
        ),


        'members-work-table-box' => array(
          'title' => __('Members work Table - Box', 'medikal'),
          'description' => __('CSS for styling the members work table main box', 'medikal'),
          'parent' => 'members',
          'selectors' => array(
            '#page .work-table .members-table-row',
          ),
          'background-color' => '',
          'border-color' => '#eeeeee',
        ),

        'members-work-table-title' => array(
          'title' => __('Members work Table - Title Box', 'medikal'),
          'description' => __('CSS for styling the members work table title box', 'medikal'),
          'parent' => 'members',
          'selectors' => array(
            '#page .work-table .members-column-title',
          ),
          'font-color' => '#ffffff',
          'font-size' => '18px',
          'font-weight' => '500',
          'font-family' => 'Raleway',
          'background-color' => 'rgba(61, 197, 223, 1)',
        ),


        'members-work-table-time' => array(
          'title' => __('Members work Table - Time Box', 'medikal'),
          'description' => __('CSS for styling the members work table time box', 'medikal'),
          'parent' => 'members',
          'selectors' => array(
            '#page .work-table .members-time',
          ),
          'font-color' => '#767676',
          'font-size' => '18px',
          'font-weight' => '400',
          'font-family' => 'Raleway',
          'background-color' => '#f7f7f7',
        ),

        'members-work-table-description' => array(
          'title' => __('Members work Table - Description Box', 'medikal'),
          'description' => __('CSS for styling the members work table time box', 'medikal'),
          'parent' => 'members',
          'selectors' => array(
            '#page .work-table .members-description',
          ),
          'font-color' => '',
          'font-size' => '16px',
          'font-weight' => '',
          'font-family' => '',
          'background-color' => '',
        ),


        'members-practice-table-box' => array(
          'title' => __('Members practice Table - Box', 'medikal'),
          'description' => __('CSS for styling the members practice table main box', 'medikal'),
          'parent' => 'members',
          'selectors' => array(
            '#page .practice-table .members-table-row',
          ),
          'background-color' => '',
          'border-color' => '#eeeeee',
        ),

        'members-practice-table-title' => array(
          'title' => __('Members practice Table - Title Box', 'medikal'),
          'description' => __('CSS for styling the members practice table title box', 'medikal'),
          'parent' => 'members',
          'selectors' => array(
            '#page .practice-table .members-column-title',
          ),
          'font-color' => '#ffffff',
          'font-size' => '18px',
          'font-weight' => '500',
          'font-family' => 'Raleway',
          'background-color' => 'rgba(61, 197, 223, 1)',
        ),


        'members-practice-table-time' => array(
          'title' => __('Members practice Table - Time Box', 'medikal'),
          'description' => __('CSS for styling the members practice table time box', 'medikal'),
          'parent' => 'members',
          'selectors' => array(
            '#page .practice-table .members-time',
          ),
          'font-color' => '#767676',
          'font-size' => '18px',
          'font-weight' => '400',
          'font-family' => 'Raleway',
          'background-color' => '#f7f7f7',
        ),

        'members-practice-table-description' => array(
          'title' => __('Members practice Table - Description Box', 'medikal'),
          'description' => __('CSS for styling the members practice table time box', 'medikal'),
          'parent' => 'members',
          'selectors' => array(
            '#page .practice-table .members-description',
          ),
          'font-color' => '',
          'font-size' => '16px',
          'font-weight' => '',
          'font-family' => '',
          'background-color' => '',
        ),


        'visualcomposer-tour-tab' => array(
          'title' => __('Tour Tab', 'medikal'),
          'description' => __('CSS for styling the visualcomposer tour tabs', 'medikal'),
          'parent' => 'visualcomposer',
          'selectors' => array(
            '#page .vc_tta-style-theme .vc_tta-tabs-container li a span',
            '#page .vc_tta-style-theme .vc_tta-tabs-container',
            '#page .vc_tta-style-theme .vc_tta-panel-heading',
            '#page .vc_tta-style-theme .vc_tta-panel-heading a span'
          ),
          'border-color' => '#78d7e9',
          'background-color' => '#3dc5df',
          'font-family' => 'Raleway',
          'font-size' => '18px',
          'font-weight' => 600,
          'font-color' => '#ffffff',
        ),

        'visualcomposer-tour-tab-active' => array(
          'title' => __('Tour Tab Active', 'medikal'),
          'description' => __('CSS for styling the visualcomposer tour tabs on active state', 'medikal'),
          'parent' => 'visualcomposer',
          'selectors' => array(
            '#page .vc_tta-style-theme .vc_tta-tabs-container li.vc_active a span',
            '#page .vc_tta-style-theme .vc_tta-tabs-container li.vc_active',
            '#page .vc_tta-style-theme .vc_active .vc_tta-panel-heading',
            '#page .vc_tta-style-theme .vc_active .vc_tta-panel-heading a span'
          ),
          'background-color' => '#ffffff',
          'font-color' => '#333333',
        ),

      ),
    );

    return $this;
  }

}