<?php
/**
 * Single post template for services post type
 *
 * @author jason.xie@victheme.com
 */

get_header();
?>

  <!-- Main Content -->
  <div id="maincontent" class="area clearfix">
    <div class="container-fluid">
      <div class="row">

        <div id="content"
             class="region
			     <?php if (VTCore_Zeus_Utility::getSidebar('services')) {
               echo VTCore_Zeus_Utility::getColumnSize('content');
             } ?>
			     with-sidebar-<?php echo VTCore_Zeus_Utility::getSidebar('services'); ?>">

          <?php
          wp_reset_postdata();
          while (have_posts()) {

            // Build the single post entry using main loop
            the_post();
            get_template_part('templates/content/services');

          }

          ?>
        </div>

        <?php
        // Build sidebar.
        if (VTCore_Zeus_Utility::getSidebar('services') == 'right'
          || VTCore_Zeus_Utility::getSidebar('services') == 'left'
        ) {
          get_sidebar('sidebar');
        }
        ?>

      </div>
    </div>
  </div>


<?php get_footer(); ?>