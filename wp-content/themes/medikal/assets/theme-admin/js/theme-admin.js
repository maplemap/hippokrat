/**
 * Script for admin panel only.
 * 
 * @author jason.xie@victheme.com
 */
jQuery(document).ready(function($) {


  // Boot custom css nice text edit mode
  if ($('#custom_css').length != 0) {
  	var editor = CodeMirror.fromTextArea(document.getElementById("custom_css"), {
  		lineNumbers: true
  	});
  }
  
  
  // Parse id from url
  function parseVideoURL(url) {
    var result = url.match(/^(http|https):\/\/(?:www\.)?(youtube\.com|vimeo\.com|youtu\.be)\/(watch\?[^#]*v=(\w+)|(.+))/);
    return {
        provider : RegExp.$2,
        id : (RegExp.$2 == 'vimeo.com' ||  RegExp.$2 == 'youtu.be') ? RegExp.$3 : RegExp.$4
    }
  }
  
  // Help modal open and load video
  $('[data-target="#vtcore-zeus-help-modal"]').on('click', function(event){

    // Dont allow to follow the link
    event.preventDefault();

    var content = '', title = '';
    
    if ($(this).data('type') == 'video') {
      var video = parseVideoURL($(this).attr('href'));
      if (typeof video.provider != 'undefined') {
        
        switch (video.provider) {
          
          case 'vimeo.com' :
            content = '<iframe src="http://player.vimeo.com/video/' + video.id + '" width="800" height="600" webkitallowfullscreen mozallowfullscreen allowfullscreen frameborder="0"></iframe>';
            $('#vtcore-zeus-help-modal').find('.modal-dialog').width(840);
          break;

          case 'youtu.be' :
          case 'youtube.com' :
            content = '<iframe src="http://youtube.com/embed/' + video.id + '" width="800" height="600" frameborder="0"></iframe>';
            $('#vtcore-zeus-help-modal').find('.modal-dialog').width(840);
          break;
        }
      }
    }
    
    if ($(this).data('type') == 'screenshot') {
      content = '<img src="' + $(this).attr('href') + '" width="640" height="480" />';
      $('#vtcore-zeus-help-modal').find('.modal-dialog').width(688);
    }
    
    if ($(this).data('type') == 'link') {
      content = '<iframe src="' + $(this).attr('href') + '" width="1024" height="480" frameborder="0" />';
      $('#vtcore-zeus-help-modal').find('.modal-dialog').width(1066);
    }
    
    if (typeof $(this).data('title') != 'undefined') {
      title = $(this).data('title');
    }
    else {
      title = $(this).text();
    }
    $('#vtcore-zeus-help-modal').find('.modal-title').remove();
    $('#vtcore-zeus-help-modal').find('.modal-header').append('<h4 class="modal-title">' + title + '</h4>');
    $('#vtcore-zeus-help-modal').find('.modal-body').html(content);
  });
  
  // Help modal dismiss binding.
  $('[data-dismiss="modal"]').on('click', function(){
    $('#vtcore-zeus-help-modal').find('.modal-title').remove();
    $('#vtcore-zeus-help-modal').find('.modal-body').html('');
  });
  
  
  // Alert admin if we got more than 1000 form inputs, This is for precaution against
  // PHP Max input limit.
  // @todo expand this to utilize ajax in the future.
  var count = $('input').length + $('select').length + $('textarea').length;
  if (count > 999) {
    alert('Possibly having more than 1000 form inputs, if PHP Max Input settings set to 1000, some of the inputs might not be saved properly.');
  }


  // Speed up ajax saving, only queue form element
  // that already fetched by ajax
  $('[name="themeSaveSubmit"]')
    .on('click.trim-queue', function() {
      var queue = [];
      $('[data-panel-delta]').each(function() {
        $(this).children().length && queue.push($(this).data('panel-delta'));
      })

      $(this).data('ajax-queue', queue);
    });

});