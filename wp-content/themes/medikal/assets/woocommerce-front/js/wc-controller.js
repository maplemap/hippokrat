/**
 *
 * WooCommerce additional javascript
 *
 * @author jason.xie@victheme.com
 */
jQuery(document).ready(function ($) {

  "use strict";

  var VTWOO = {
    $page: $('#page')
  }


  if (VTWOO.$page.parent().hasClass('woocommerce-checkout')) {

    // Emulating jvFloat as woo + ajax + jvfloat = nightmare!
    VTWOO.jvFloatEmulator = {
      init: function () {
        this.parentClass = '.form-row';
        this.activeClass = 'active';
        this.$el = VTWOO.$page.find(this.parentClass).addClass('jvFloat');

        return this;
      },
      fixPlaceholder: function() {
        this.label.length && !this.self.attr('placeholder') && this.self.attr('placeholder', this.label.text());
        return this;
      },
      toggleParentClass: function() {
        this.self.val().length ? this.parent.addClass(this.activeClass) : this.parent.removeClass(this.activeClass);
        return this;
      },
      registerElement: function($el) {
        this.self = $el;
        this.parent = this.self.closest(this.parentClass);
        this.label = this.parent.children().filter('label');
        return this;
      },
      reposition: function () {
        var that = this;
        this.$el
          .children()
          .filter('input, select, textarea')
          .not(':checkbox, :radio, :submit, :reset, .jvprocessed')
          .each(function () {

            that.registerElement($(this));
            that.fixPlaceholder();
            that.toggleParentClass();

            that.self
              .addClass('jvprocessed')
              .on('keyup blur change', function () {
                that.registerElement($(this));
                that.toggleParentClass();
              });
          });

        return this;
      }
    }

    VTWOO.jvFloatEmulator.init().reposition();
  }

  /**
   * Window events
   */
  $(window)
    .on('load', function () {

      /** Woocommerce rating with jquery raty **/
      if ($('#rating-wooraty').length) {
        $('#rating-wooraty').hide().after('<div id="wooraty-placeholder"/>');
        $('#wooraty-placeholder').raty({
          target: '#rating-wooraty',
          starType: 'i',
          targetType: 'score',
          targetKeep: true
        });
      }

    });


  $(document)
    .on('ajaxComplete', function () {
      VTWOO.jvFloatEmulator && VTWOO.jvFloatEmulator.init().reposition();
    })

    // Integrate ElevateZoom to fotorama
    .on('fotorama:showend', function (e, fotorama, extra) {


      // Reset and hide all fotorama frame first
      $.each(fotorama.data, function (key, data) {
        data.$stageFrame
        && data.$stageFrame.find('.fotorama__img').data('elevateZoom')
        && data.$stageFrame.find('.fotorama__img').data('elevateZoom').zoomContainer
        && data.$stageFrame.find('.fotorama__img').data('elevateZoom').zoomContainer.remove()
        && data.$stageFrame.find('.fotorama__img').removeData('elevateZoom');
      });


      // Activate the current active fotorama frame by initializing
      // the elevateZoom
      if (typeof fotorama.activeFrame.$stageFrame.find('.fotorama__img').data('elevateZoom') == 'undefined') {

        var count = 0;
        var detect = setInterval(function () {
          if (fotorama.activeFrame.$stageFrame.find('.fotorama__img').length) {
            fotorama.activeFrame.zoomimage
            && fotorama.activeFrame.zoomconfig
            && fotorama.activeFrame.$stageFrame.find('.fotorama__img').data('zoom-image', fotorama.activeFrame.zoomimage)
            && fotorama.activeFrame.$stageFrame.find('.fotorama__img').elevateZoom(fotorama.activeFrame.zoomconfig)
            && fotorama.activeFrame.$stageFrame.on('click.elevatezoom', function () {
              fotorama.requestFullScreen();
            });

            clearInterval(detect);
          }

          count++;
          // Bail out if nothing found in 8000 ms.
          if (count > 10) {
            clearInterval(detect);
          }
        }, 800);
      }

      // ElevateZoom already activated just show the zooming container
      else {
        fotorama.activeFrame.$stageFrame.find('.fotorama__img').data('elevateZoom').zoomContainer
        && fotorama.activeFrame.$stageFrame.find('.fotorama__img').data('elevateZoom').zoomContainer.show();
      }
    });
});
