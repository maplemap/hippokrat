/**
 *
 * The main theme controller javascript.
 * This file will load and configure other
 * javascript settings related to the theme.
 *
 * @author jason.xie@victheme.com
 */
jQuery(document).ready(function ($) {

  "use strict";


  var VT = {
    $page: $('#page')
  };

  // Force comment form to use jvfloat.
  if (VT.$page.hasClass('with-jvfloat')) {
    VT.$page.find('#commentform').addClass('jvfloat');
  }

  // Booting jvfloat
  // This needs to be initialilzed as soon as possible
  if (VT.$page.hasClass('with-jvfloat') && VT.$page.find('.jvfloat').length) {
    VT.$page.addClass('jvfloat');
    VT.jvFloat = {
      init: function () {
        this.$jvFloatElements = VT.$page.find('input, textarea, select');

        this.fixWPComment();
        return this;
      },
      fixWPComment: function () {
        if (VT.$page.find('#commentform:not(.jvprocessed)').length) {
          VT.$page.find('#commentform:not(.jvprocessed)').find('label').each(function () {
            $(this).parent().addClass('form-group');
          });
        }
        return this;
      },
      reposition: function () {

        this.$jvFloatElements.not(':checkbox, :radio, :submit, :reset, [type="hidden"], .jvprocessed').each(function () {
          var self = $(this);
          var label = self.closest('.form-group').children().filter('label');

          if (label.length) {
            self.attr('placeholder', label.text());
            label.hide();
            self.jvFloat().addClass('jvprocessed');
          }
        });

        VT.$page.find('.wp-termshs').find('label').eq(0).hide();

        return this;
      }
    }


    VT.jvFloat.init().reposition();
  }

  // Booting flip content
  // This needs to be invoked before isotope
  if (VT.$page.find('.js-flip').length) {
    VT.flip = {
      init: function() {
        this.$element = VT.$page.find('.js-flip');
        return this;
      },
      reposition: function() {
        this.$element.filter(':not(.flip-processed)').each(function() {
          var options = $(this).data('flip-options'), maxHeight = $(this).find(options.front).outerHeight(true);

          if (maxHeight < $(this).find(options.back).outerHeight(true)) {
            maxHeight = $(this).find(options.back).outerHeight(true);
          }

          $(this).height(maxHeight);
          $(this).find(options.front).height(maxHeight);
          $(this).find(options.back).height(maxHeight);
          $(this).flip(options).addClass('flip-processed');
        });
      }
    }

    VT.flip.init().reposition();
  }


  // Dont initialize parallax on mobile or tablet!
  if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent)) {
    $(window).off('scroll.parallax');
  }

  /**
   * Window events
   */
  $(window)
    .on('load', function () {

      // Piggy backing to pageready event
      if (!$('#page.animsition').length) {

        setTimeout(function() {
          $(window).trigger('pageready');
        }, 10);
      }

    })

  /**
   * Page Ready will be called after animsition
   * or if animsition not enabled after window load
   * event.
   */
    .on('pageready', function () {

      // Force VC to relayout row
      if (typeof window.vc_rowBehaviour == 'function') {
        window.vc_rowBehaviour()
      }

      VT.$page.addClass('animsition-done');

      // Booting data aspect
      if (VT.$page.find('[data-aspect="true"]').length) {
        VT.$page.find('[data-aspect="true"]').VTCoreAspectRatio();
      }

      // Booting sticky header
      // Now it is capable to dynamically detect if the source menu
      // is actually small enough to fit on the container, if not
      // use the slick navigation
      if (VT.$page.hasClass('with-sticky') && VT.$page.find('#header').length) {

        VT.sticky = {
          init: function () {
            this.$sticky = VT.$page.find('#header');

            this.$sticky.hcSticky({
              top: -1,
              responsive: false,
              onStart: function () {
                VT.$page.addClass('sticky-active');
              },
              onStop: function () {
                VT.$page.removeClass('sticky-active');
                VT.sticky.reposition();
              },
              offResolutions: -640
            });

            return this;
          },
          reposition: function () {

            var that = this;
            that.stickyHeight = 0;
            this.$sticky.find('.row').each(function() {
              that.stickyHeight += $(this).outerHeight();
            });

            this.$sticky
              .parent()
              .height(this.stickyHeight);

            return this;
          },
          off: function () {
            this.$sticky.hcSticky('off');
            return this;
          },
          on: function () {
            this.$sticky.hcSticky('on');
          },
          getSize: function() {
            return this.$sticky
              .parent()
              .height();
          },
          reinit: function () {

            this.$sticky.hcSticky('reinit');
            this.reposition();

            if ($(window).width() < 640) {
              VT.$page.addClass('sticky-off');
              this.off();
            }
            else {
              VT.$page.removeClass('sticky-off');
              this.on();
            }


            return this;
          }
        }

        VT.sticky.init();
      }

      // Booting slick nav menu
      if (VT.$page.hasClass('with-slicknav')) {

        VT.slick = {
          init: function () {
            this.targetClass = '.slicknav';
            this.$slick = VT.$page.find('#header ' + this.targetClass);
            this.$slick
              .addClass('slicknav-source')
              .slicknav({
                label: '',
                prependTo: '',
                appendTo: '#header',
                open: function () {
                  VT.$page.addClass('slicknav-open');
                  ($(window).width() < 640) && VT.sticky && VT.sticky.off();
                },
                close: function () {
                  VT.$page.removeClass('slicknav-open');
                  ($(window).width() > 640) && VT.sticky && VT.sticky.on();
                }
              });

            this.processSlick();

            return this;
          },
          checkParentVisibility: function(element) {
            if (element.parent().is(':hidden')) {
              return this.checkParentVisibility(element.parent());
            }

            return element;

          },
          processSlick: function() {
            this.slickElements = {};
            var that = this;
            this.$slick.each(function() {
              var delta = $(this).data('slick-source'),
                  clone = $(this).clone(),
                  self = $(this),
                  hiddenParent = false,
                  maxwidth = 0,
                  maxheight = 0;

              if (self.parent().is(':hidden')) {
                hiddenParent = that.checkParentVisibility(self.parent());
              }

              clone
                .css({
                  position: 'absolute',
                  visibility: 'hidden',
                  display: 'table',
                  zIndex: -1,
                  width : '100%',
                  height: '',
                  maxWidth : '',
                  maxHeight: ''
                });

              $(this).parent().append(clone);

              // Don't use .css() we need the !important rules!
              hiddenParent
                && hiddenParent
                    .attr('style', 'display: table !important; visibility: hidden !important; position: absolute;');

              // Loop and count each width and height manually for
              // stable output!
              clone.children().each(function() {
                maxwidth += $(this).outerWidth();
                maxheight += $(this).outerHeight();
              });

              that.slickElements[delta] = {
                source: self,
                target: VT.$page.find('[data-slick-target="' + delta + '"]'),
                delta: delta,
                width: maxwidth,
                height: maxheight,
                container: self.parent()
              }

              hiddenParent && hiddenParent.removeAttr('style');

              clone.remove();

            })
          },
          format: function () {
            VT.$page.find('.slicknav_nav')
              .find('.dropdown, .dropdown-menu, .dropdown-toggle')
              .removeClass('dropdown dropdown-menu dropdown-toggle');

            VT.$page.find('.slicknav_nav')
              .find('.caret').remove();

            this.reposition();

            return this;
          },
          reposition: function () {
            this.topPos = (VT.$page.find('#header').outerHeight() - VT.$page.find('.slicknav_btn').outerHeight()) / 2;
            VT.$page.find('.slicknav_btn').css('top', this.topPos);
            return this;
          },
          ubermenu: function() {
            VT.$page.find('#megaUber').addClass('slicknav');
            return this;
          },
          detectVisibility: function() {

            !this.slickElements && this.processSlick();

            var that = this;

            $.each(this.slickElements, function(delta, slick) {
              // Container cannot hold the menu fire slick
              if (slick.container.innerWidth() < slick.width) {
                slick.source.hide();
                slick.target.show();
              }
              // Container is wide enough for menu remove slick
              else {
                slick.source.show();
                slick.target.hide();
              }
            });


            return this;
          }
        }

        VT.slick.ubermenu().init().format().detectVisibility().reposition();
        VT.sticky && VT.sticky.reposition();

      }

      // Booting smoothscroll
      // Warning: this is required for sane pseudo-background in content and sidebar!
      VT.smoothScroll = {
        init: function() {
          this.$el = $('a[href^="#"]').not('[data-vc-container]');

          // Bug fix pseudo-background anchor!
          this.$el
            .on('click', function(e){
              e.preventDefault();
            });


          // Only attch smooth scroll on element that
          // actually has valid target in the same page
          this.$el.each(function() {

            if ($($(this).attr('href')).length) {
              $(this).smoothScroll({
                speed: 'auto',
                autoCoefficient: 0.5,
                easing: 'easeOutBack',
                offset: VT.sticky && parseInt(VT.sticky.getSize()) + 50 * -1 || 0
              });
            }

          });


          window.location.hash && $.smoothScroll({
            offset: VT.sticky && parseInt(VT.sticky.getSize()) + 50 * -1 || 0,
            scrollTarget: window.location.hash
          });
        }
      }

      VT.smoothScroll.init();

      // Fix counter up kill vc animation
      if (VT.$page.find('.wpb_animate_when_almost_visible:not(.wpb_start_animation)').length
          && typeof jQuery.fn.waypoint3 !== 'undefined') {
        VT.$page.find('.wpb_animate_when_almost_visible:not(.wpb_start_animation)').waypoint3(function () {
          $(this).addClass('wpb_start_animation');
        }, {offset: '85%'});
      }

      // Fix counter up kill vc progress bar
      if (VT.$page.find('.vc_progress_bar').length
          && typeof jQuery.fn.waypoint3 !== 'undefined') {

        VT.$page.find('.vc_progress_bar').off('waypoint').waypoint3( function () {
          $( this ).find( '.vc_single_bar' ).each( function ( index ) {
            var $this = $( this ),
              bar = $this.find( '.vc_bar' ),
              val = bar.data( 'percentage-value' );

            setTimeout( function () {
              bar.css( { "width": val + '%' } );
            }, index * 200 );
          } );
        }, { offset: '85%' } );
      }

      // Isotope
      if (VT.$page.find('.js-isotope').length) {
        VT.isotope = {
          init: function() {
            this.$isotope = VT.$page.find('.js-isotope');
            return this;
          },
          reposition: function() {
            this.$isotope.isotope('layout');
            return this;
          }
        }

        setTimeout(function() {
          VT.isotope.init().reposition();
        }, 10);

      }
    })


    .on('resize', function () {

      // Reinit Sticky header
      VT.sticky && VT.sticky.reinit();

      // Resize the flip element
      VT.flip && VT.flip.reposition();

      // Fix isotope layout
      VT.isotope && setTimeout(function() {
        VT.isotope.reposition();
      }, 300);

      // Dynamic slick detection, fired when navigation is overflowing to second row
      VT.slick && VT.slick.detectVisibility();
    });


  $(document)
    .on('animsitionPageIn', function () {

      // Piggy backing to page ready event
      $(window).trigger('pageready');

    })

    .on('flip-content', function(event, element) {

      var isotope = $(element).closest('.js-isotope');
      if (isotope.length != 0) {
        isotope.data('isotope') && isotope.isotope('layout');
      }

    })

    .on('layoutComplete', function(object, items) {
      VT.sticky && VT.sticky.reposition();
      VT.slick && VT.slick.reposition();
      VT.flip && VT.flip.init().reposition();
    })

    .on('fotorama:ready fotorama:show', function(e, fotorama, extra)  {

      // Force isotope to recheck layout when fotorama change,
      // this is to avoid fotorama with dynamic height breaks isotope
      var ParentIsotope = fotorama.activeFrame.$stageFrame.closest('.js-isotope');
      ParentIsotope.length && ParentIsotope.data('isotope') && ParentIsotope.isotope('layout');
    })

    .on('ajaxStart', function() {
      $('.ajax-notification').show();
    })
    .on('ajaxComplete', function () {

      // Boot form eye candy
      VT.jvFloat && VT.jvFloat.init().reposition();

      if ($('.wpb_single_image.imgLiquidFill').length) {
        $('.wpb_single_image.imgLiquidFill:not(.imgLiquid_ready)')
          .data('imgLiquid-horizontal-align', 'center')
          .data('imgLiquidFill', true)
          .data('imgLiquid-verticalAlign', 'center')
          .imgLiquid();
      }

      VT.flip && VT.flip.init().reposition();

      setTimeout(function() {
        $('.ajax-notification').hide();
      }, 100);

    })
    .on('wp-loop-ajax-processed', function() {
      VT.flip && VT.flip.init().reposition();
    });

});
