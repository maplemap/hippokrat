/**
 *
 * Controlling timetable with javascript
 *
 * @author jason.xie@victheme.com
 */

jQuery(document).ready(function($) {

  "use strict";


  var TimeTable = function (element) {
    this.$el = element;
    this.$headerItems = this.$el.find('[data-swipe-type="header-items"]');
    this.$columnItems = this.$el.find('[data-swipe-type="column-items"]');

    this.$header = this.$el.find('[data-swipe-type="header"]');
    this.$sections = this.$el.find('[data-swipe-type="sections"]');
    this.$columns = this.$el.find('[data-swipe-type="columns"]');
    this.$inner = this.$el.find('[data-swipe-type="filter-target"]');

    this.activeResolution = 481;

    this.$queue = [];

    var that = this;
    this.$inner.each(function() {
      that.$queue.push($(this));
    });

    this.$columns.each(function() {
      that.$queue.push($(this));
    });

    this.$sections.each(function() {
      that.$queue.push($(this));
    });

    return this;
  }

  TimeTable.prototype = {
    reposition: function() {
      if ($(window).width() < this.activeResolution) {
        this.formatHeader().formatColumn();
      }
      else {
        this.destroy();
      }
    },
    layoutChange: function() {
      $.each(this.$queue, function(key, item) {
        $(item).data('isotope') && $(item).isotope('layout');
      });
    },
    move: function() {

      if (!this.direction || !this.$active || !this.$target) {
        return;
      }

      this.position = 0;

      switch (this.direction) {
        case 'right' :

          if (this.$active.prev().length == 0) {
            return;
          }

          this.position = this.$active.prev().position().left * -1;

          break;

        case 'left' :

          if (this.$active.next().length == 0) {
            return;
          }

          this.position = this.$active.next().position().left * -1;

          break;
      }

      this.$target.css({
        left: this.position
      });

      return this;
    },
    formatHeader: function () {
      var maxWidth = 0;
      this.$headerItems.each(function (key, item) {
        $(item).width($(item).width());
        maxWidth = maxWidth + parseFloat($(item).outerWidth());
      });

      var that = this;

      this.$header
        .width(maxWidth)
        .on('swipelinked', function() {
          that.$active = $('[data-swipe-type="header-items"][data-swipe-linked="' + that.$active.data('swipe-linked') + '"]');
          that.$target = that.$header;
          that.move();
        });

      this.$header.data('isotope') && this.$header.isotope('layout');

      this.$headerItems
        .on('swipeleft', function(e) {
          that.$active = $(e.currentTarget);
          that.direction = 'left';
          that.$target = that.$header;
          that.move();

          that.$columns.trigger('swipelinked');
        })
        .on('swiperight', function(e) {
          that.$active = $(e.currentTarget);
          that.direction = 'right';
          that.$target = that.$header;
          that.move();

          that.$columns.trigger('swipelinked');
        });

      return this;
    },
    formatColumn: function() {
      var maxWidth = 0;
      this.$columnItems.each(function (key, item) {
        $(item).width($(item).width());
        maxWidth = maxWidth + parseFloat($(item).outerWidth());
      });


      var that = this;

      this.$columns
        .width(maxWidth)
        .on('swipelinked', function() {
          that.$active = $('[data-swipe-type="column-items"][data-swipe-linked="' + that.$active.data('swipe-linked') + '"]');
          that.$target = that.$columns;
          that.move();

          that.$header.trigger('swipelinked');
        });

      this.$columnItems
        .on('swipeleft', function(e) {
          that.$active = $(e.currentTarget);
          that.direction = 'left';
          that.$target = that.$columns;
          that.move();

          that.$header.trigger('swipelinked');
        })
        .on('swiperight', function(e) {
          that.$active = $(e.currentTarget);
          that.direction = 'right';
          that.$target = that.$columns;
          that.move();

          that.$header.trigger('swipelinked');
        });

      return this;
    },
    destroy: function() {
      this.$columnItems.width('').off('swipeleft swiperight');
      this.$columns.width('').off('swipelinked');
      this.$headerItems.width('').off('swipeleft swiperight');
      this.$header.width('').off('swipelinked');
      this.$header.data('isotope') && this.$header.isotope('layout');
    }
  }

  var TimeTableFilter = function(element) {
    this.$el = element;
    this.$target = $('[data-swipe-type="filter-target"]');
    this.filterVal = (this.$el.data('sort') == '*') ? '*' : '[data-sort="' + this.$el.data('sort') + '"]';
  }

  TimeTableFilter.prototype = {
    filter: function() {
      var that = this;
      this.$target.each(function() {
        $(this).isotope({
          filter: that.filterVal
        });
      });
    }
  }

  $('.js-timetable')
    .each(function() {
      $(this).data('timetable', new TimeTable($(this)));
      $(this).data('timetable').reposition();
    })
    .on('layoutChange', function() {
      $(this).data('timetable').layoutChange();
    });

  $('.js-timesorter')
    .each(function() {
      $(this).data('timesort', new TimeTableFilter($(this)));
    })
    .on('click.timesorter', function(e) {
      e.preventDefault();
      $(this).data('timesort') && $(this).data('timesort').filter();

      $(this).closest('.js-timetable').trigger('layoutChange');

      return false;
    });

});
