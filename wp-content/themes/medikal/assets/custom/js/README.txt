CUSTOM CSS FOLDER
=================

This folder is leaved empty as intended.
You can create custom.css file in this
directory and it will be automatically
loaded by the theme.

Make sure to backup the custom CSS file
before upgrading the theme to avoid
the file got overwritten or deleted
automatically by WordPress.