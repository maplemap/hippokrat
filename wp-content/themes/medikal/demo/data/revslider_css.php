
<?php
/**
 * This is an automated class generated by Victheme Exporter Plugin
 */
class VTCore_Zeus_Demo_Data_Revslider__css
extends VTCore_Demo_Models_Data {

  public $context;
  public $primary;
  public $table;
  public $base;

  public function __construct() {
    $this->context = array (
  1 => 
  array (
    'id' => '1',
    'handle' => '.tp-caption.medium_grey',
    'settings' => NULL,
    'hover' => NULL,
    'params' => '{"position":"absolute","color":"#fff","text-shadow":"0px 2px 5px rgba(0, 0, 0, 0.5)","font-weight":"700","font-size":"20px","line-height":"20px","font-family":"Arial","padding":"2px 4px","margin":"0px","border-width":"0px","border-style":"none","background-color":"#888","white-space":"nowrap"}',
  ),
  2 => 
  array (
    'id' => '2',
    'handle' => '.tp-caption.small_text',
    'settings' => NULL,
    'hover' => NULL,
    'params' => '{"position":"absolute","color":"#fff","text-shadow":"0px 2px 5px rgba(0, 0, 0, 0.5)","font-weight":"700","font-size":"14px","line-height":"20px","font-family":"Arial","margin":"0px","border-width":"0px","border-style":"none","white-space":"nowrap"}',
  ),
  3 => 
  array (
    'id' => '3',
    'handle' => '.tp-caption.medium_text',
    'settings' => NULL,
    'hover' => NULL,
    'params' => '{"position":"absolute","color":"#fff","text-shadow":"0px 2px 5px rgba(0, 0, 0, 0.5)","font-weight":"700","font-size":"20px","line-height":"20px","font-family":"Arial","margin":"0px","border-width":"0px","border-style":"none","white-space":"nowrap"}',
  ),
  4 => 
  array (
    'id' => '4',
    'handle' => '.tp-caption.large_text',
    'settings' => NULL,
    'hover' => NULL,
    'params' => '{"position":"absolute","color":"#fff","text-shadow":"0px 2px 5px rgba(0, 0, 0, 0.5)","font-weight":"700","font-size":"40px","line-height":"40px","font-family":"Arial","margin":"0px","border-width":"0px","border-style":"none","white-space":"nowrap"}',
  ),
  5 => 
  array (
    'id' => '5',
    'handle' => '.tp-caption.very_large_text',
    'settings' => NULL,
    'hover' => NULL,
    'params' => '{"position":"absolute","color":"#fff","text-shadow":"0px 2px 5px rgba(0, 0, 0, 0.5)","font-weight":"700","font-size":"60px","line-height":"60px","font-family":"Arial","margin":"0px","border-width":"0px","border-style":"none","white-space":"nowrap","letter-spacing":"-2px"}',
  ),
  6 => 
  array (
    'id' => '6',
    'handle' => '.tp-caption.very_big_white',
    'settings' => NULL,
    'hover' => NULL,
    'params' => '{"position":"absolute","color":"#fff","text-shadow":"none","font-weight":"800","font-size":"60px","line-height":"60px","font-family":"Arial","margin":"0px","border-width":"0px","border-style":"none","white-space":"nowrap","padding":"0px 4px","padding-top":"1px","background-color":"#000"}',
  ),
  7 => 
  array (
    'id' => '7',
    'handle' => '.tp-caption.very_big_black',
    'settings' => NULL,
    'hover' => NULL,
    'params' => '{"position":"absolute","color":"#000","text-shadow":"none","font-weight":"700","font-size":"60px","line-height":"60px","font-family":"Arial","margin":"0px","border-width":"0px","border-style":"none","white-space":"nowrap","padding":"0px 4px","padding-top":"1px","background-color":"#fff"}',
  ),
  8 => 
  array (
    'id' => '8',
    'handle' => '.tp-caption.modern_medium_fat',
    'settings' => NULL,
    'hover' => NULL,
    'params' => '{"position":"absolute","color":"#000","text-shadow":"none","font-weight":"800","font-size":"24px","line-height":"20px","font-family":"\\"Open Sans\\", sans-serif","margin":"0px","border-width":"0px","border-style":"none","white-space":"nowrap"}',
  ),
  9 => 
  array (
    'id' => '9',
    'handle' => '.tp-caption.modern_medium_fat_white',
    'settings' => NULL,
    'hover' => NULL,
    'params' => '{"position":"absolute","color":"#fff","text-shadow":"none","font-weight":"800","font-size":"24px","line-height":"20px","font-family":"\\"Open Sans\\", sans-serif","margin":"0px","border-width":"0px","border-style":"none","white-space":"nowrap"}',
  ),
  10 => 
  array (
    'id' => '10',
    'handle' => '.tp-caption.modern_medium_light',
    'settings' => NULL,
    'hover' => NULL,
    'params' => '{"position":"absolute","color":"#000","text-shadow":"none","font-weight":"300","font-size":"24px","line-height":"20px","font-family":"\\"Open Sans\\", sans-serif","margin":"0px","border-width":"0px","border-style":"none","white-space":"nowrap"}',
  ),
  11 => 
  array (
    'id' => '11',
    'handle' => '.tp-caption.modern_big_bluebg',
    'settings' => NULL,
    'hover' => NULL,
    'params' => '{"position":"absolute","color":"#fff","text-shadow":"none","font-weight":"800","font-size":"30px","line-height":"36px","font-family":"\\"Open Sans\\", sans-serif","padding":"3px 10px","margin":"0px","border-width":"0px","border-style":"none","background-color":"#4e5b6c","letter-spacing":"0"}',
  ),
  12 => 
  array (
    'id' => '12',
    'handle' => '.tp-caption.modern_big_redbg',
    'settings' => NULL,
    'hover' => NULL,
    'params' => '{"position":"absolute","color":"#fff","text-shadow":"none","font-weight":"300","font-size":"30px","line-height":"36px","font-family":"\\"Open Sans\\", sans-serif","padding":"3px 10px","padding-top":"1px","margin":"0px","border-width":"0px","border-style":"none","background-color":"#de543e","letter-spacing":"0"}',
  ),
  13 => 
  array (
    'id' => '13',
    'handle' => '.tp-caption.modern_small_text_dark',
    'settings' => NULL,
    'hover' => NULL,
    'params' => '{"position":"absolute","color":"#555","text-shadow":"none","font-size":"14px","line-height":"22px","font-family":"Arial","margin":"0px","border-width":"0px","border-style":"none","white-space":"nowrap"}',
  ),
  14 => 
  array (
    'id' => '14',
    'handle' => '.tp-caption.boxshadow',
    'settings' => NULL,
    'hover' => NULL,
    'params' => '{"-moz-box-shadow":"0px 0px 20px rgba(0, 0, 0, 0.5)","-webkit-box-shadow":"0px 0px 20px rgba(0, 0, 0, 0.5)","box-shadow":"0px 0px 20px rgba(0, 0, 0, 0.5)"}',
  ),
  15 => 
  array (
    'id' => '15',
    'handle' => '.tp-caption.black',
    'settings' => NULL,
    'hover' => NULL,
    'params' => '{"color":"#000","text-shadow":"none"}',
  ),
  16 => 
  array (
    'id' => '16',
    'handle' => '.tp-caption.noshadow',
    'settings' => NULL,
    'hover' => NULL,
    'params' => '{"text-shadow":"none"}',
  ),
  17 => 
  array (
    'id' => '17',
    'handle' => '.tp-caption.thinheadline_dark',
    'settings' => NULL,
    'hover' => NULL,
    'params' => '{"position":"absolute","color":"rgba(0,0,0,0.85)","text-shadow":"none","font-weight":"300","font-size":"30px","line-height":"30px","font-family":"\\"Open Sans\\"","background-color":"transparent"}',
  ),
  18 => 
  array (
    'id' => '18',
    'handle' => '.tp-caption.thintext_dark',
    'settings' => NULL,
    'hover' => NULL,
    'params' => '{"position":"absolute","color":"rgba(0,0,0,0.85)","text-shadow":"none","font-weight":"300","font-size":"16px","line-height":"26px","font-family":"\\"Open Sans\\"","background-color":"transparent"}',
  ),
  19 => 
  array (
    'id' => '19',
    'handle' => '.tp-caption.largeblackbg',
    'settings' => NULL,
    'hover' => NULL,
    'params' => '{"position":"absolute","color":"#fff","text-shadow":"none","font-weight":"300","font-size":"50px","line-height":"70px","font-family":"\\"Open Sans\\"","background-color":"#000","padding":"0px 20px","-webkit-border-radius":"0px","-moz-border-radius":"0px","border-radius":"0px"}',
  ),
  20 => 
  array (
    'id' => '20',
    'handle' => '.tp-caption.largepinkbg',
    'settings' => NULL,
    'hover' => NULL,
    'params' => '{"position":"absolute","color":"#fff","text-shadow":"none","font-weight":"300","font-size":"50px","line-height":"70px","font-family":"\\"Open Sans\\"","background-color":"#db4360","padding":"0px 20px","-webkit-border-radius":"0px","-moz-border-radius":"0px","border-radius":"0px"}',
  ),
  21 => 
  array (
    'id' => '21',
    'handle' => '.tp-caption.largewhitebg',
    'settings' => NULL,
    'hover' => NULL,
    'params' => '{"position":"absolute","color":"#000","text-shadow":"none","font-weight":"300","font-size":"50px","line-height":"70px","font-family":"\\"Open Sans\\"","background-color":"#fff","padding":"0px 20px","-webkit-border-radius":"0px","-moz-border-radius":"0px","border-radius":"0px"}',
  ),
  22 => 
  array (
    'id' => '22',
    'handle' => '.tp-caption.largegreenbg',
    'settings' => NULL,
    'hover' => NULL,
    'params' => '{"position":"absolute","color":"#fff","text-shadow":"none","font-weight":"300","font-size":"50px","line-height":"70px","font-family":"\\"Open Sans\\"","background-color":"#67ae73","padding":"0px 20px","-webkit-border-radius":"0px","-moz-border-radius":"0px","border-radius":"0px"}',
  ),
  23 => 
  array (
    'id' => '23',
    'handle' => '.tp-caption.excerpt',
    'settings' => NULL,
    'hover' => NULL,
    'params' => '{"font-size":"36px","line-height":"36px","font-weight":"700","font-family":"Arial","color":"#ffffff","text-decoration":"none","background-color":"rgba(0, 0, 0, 1)","text-shadow":"none","margin":"0px","letter-spacing":"-1.5px","padding":"1px 4px 0px 4px","width":"150px","white-space":"normal !important","height":"auto","border-width":"0px","border-color":"rgb(255, 255, 255)","border-style":"none"}',
  ),
  24 => 
  array (
    'id' => '24',
    'handle' => '.tp-caption.large_bold_grey',
    'settings' => NULL,
    'hover' => NULL,
    'params' => '{"font-size":"60px","line-height":"60px","font-weight":"800","font-family":"\\"Open Sans\\"","color":"rgb(102, 102, 102)","text-decoration":"none","background-color":"transparent","text-shadow":"none","margin":"0px","padding":"1px 4px 0px","border-width":"0px","border-color":"rgb(255, 214, 88)","border-style":"none"}',
  ),
  25 => 
  array (
    'id' => '25',
    'handle' => '.tp-caption.medium_thin_grey',
    'settings' => NULL,
    'hover' => NULL,
    'params' => '{"font-size":"34px","line-height":"30px","font-weight":"300","font-family":"\\"Open Sans\\"","color":"rgb(102, 102, 102)","text-decoration":"none","background-color":"transparent","padding":"1px 4px 0px","text-shadow":"none","margin":"0px","border-width":"0px","border-color":"rgb(255, 214, 88)","border-style":"none"}',
  ),
  26 => 
  array (
    'id' => '26',
    'handle' => '.tp-caption.small_thin_grey',
    'settings' => NULL,
    'hover' => NULL,
    'params' => '{"font-size":"18px","line-height":"26px","font-weight":"300","font-family":"\\"Open Sans\\"","color":"rgb(117, 117, 117)","text-decoration":"none","background-color":"transparent","padding":"1px 4px 0px","text-shadow":"none","margin":"0px","border-width":"0px","border-color":"rgb(255, 214, 88)","border-style":"none"}',
  ),
  27 => 
  array (
    'id' => '27',
    'handle' => '.tp-caption.lightgrey_divider',
    'settings' => NULL,
    'hover' => NULL,
    'params' => '{"text-decoration":"none","background-color":"rgba(235, 235, 235, 1)","width":"370px","height":"3px","background-position":"initial initial","background-repeat":"initial initial","border-width":"0px","border-color":"rgb(34, 34, 34)","border-style":"none"}',
  ),
  28 => 
  array (
    'id' => '28',
    'handle' => '.tp-caption.large_bold_darkblue',
    'settings' => NULL,
    'hover' => NULL,
    'params' => '{"font-size":"58px","line-height":"60px","font-weight":"800","font-family":"\\"Open Sans\\"","color":"rgb(52, 73, 94)","text-decoration":"none","background-color":"transparent","border-width":"0px","border-color":"rgb(255, 214, 88)","border-style":"none"}',
  ),
  29 => 
  array (
    'id' => '29',
    'handle' => '.tp-caption.medium_bg_darkblue',
    'settings' => NULL,
    'hover' => NULL,
    'params' => '{"font-size":"20px","line-height":"20px","font-weight":"800","font-family":"\\"Open Sans\\"","color":"rgb(255, 255, 255)","text-decoration":"none","background-color":"rgb(52, 73, 94)","padding":"10px","border-width":"0px","border-color":"rgb(255, 214, 88)","border-style":"none"}',
  ),
  30 => 
  array (
    'id' => '30',
    'handle' => '.tp-caption.medium_bold_red',
    'settings' => NULL,
    'hover' => NULL,
    'params' => '{"font-size":"24px","line-height":"30px","font-weight":"800","font-family":"\\"Open Sans\\"","color":"rgb(227, 58, 12)","text-decoration":"none","background-color":"transparent","padding":"0px","border-width":"0px","border-color":"rgb(255, 214, 88)","border-style":"none"}',
  ),
  31 => 
  array (
    'id' => '31',
    'handle' => '.tp-caption.medium_light_red',
    'settings' => NULL,
    'hover' => NULL,
    'params' => '{"font-size":"21px","line-height":"26px","font-weight":"300","font-family":"\\"Open Sans\\"","color":"rgb(227, 58, 12)","text-decoration":"none","background-color":"transparent","padding":"0px","border-width":"0px","border-color":"rgb(255, 214, 88)","border-style":"none"}',
  ),
  32 => 
  array (
    'id' => '32',
    'handle' => '.tp-caption.medium_bg_red',
    'settings' => NULL,
    'hover' => NULL,
    'params' => '{"font-size":"20px","line-height":"20px","font-weight":"800","font-family":"\\"Open Sans\\"","color":"rgb(255, 255, 255)","text-decoration":"none","background-color":"rgb(227, 58, 12)","padding":"10px","border-width":"0px","border-color":"rgb(255, 214, 88)","border-style":"none"}',
  ),
  33 => 
  array (
    'id' => '33',
    'handle' => '.tp-caption.medium_bold_orange',
    'settings' => NULL,
    'hover' => NULL,
    'params' => '{"font-size":"24px","line-height":"30px","font-weight":"800","font-family":"\\"Open Sans\\"","color":"rgb(243, 156, 18)","text-decoration":"none","background-color":"transparent","border-width":"0px","border-color":"rgb(255, 214, 88)","border-style":"none"}',
  ),
  34 => 
  array (
    'id' => '34',
    'handle' => '.tp-caption.medium_bg_orange',
    'settings' => NULL,
    'hover' => NULL,
    'params' => '{"font-size":"20px","line-height":"20px","font-weight":"800","font-family":"\\"Open Sans\\"","color":"rgb(255, 255, 255)","text-decoration":"none","background-color":"rgb(243, 156, 18)","padding":"10px","border-width":"0px","border-color":"rgb(255, 214, 88)","border-style":"none"}',
  ),
  35 => 
  array (
    'id' => '35',
    'handle' => '.tp-caption.grassfloor',
    'settings' => NULL,
    'hover' => NULL,
    'params' => '{"text-decoration":"none","background-color":"rgba(160, 179, 151, 1)","width":"4000px","height":"150px","border-width":"0px","border-color":"rgb(34, 34, 34)","border-style":"none"}',
  ),
  36 => 
  array (
    'id' => '36',
    'handle' => '.tp-caption.large_bold_white',
    'settings' => NULL,
    'hover' => NULL,
    'params' => '{"font-size":"58px","line-height":"60px","font-weight":"800","font-family":"\\"Open Sans\\"","color":"rgb(255, 255, 255)","text-decoration":"none","background-color":"transparent","border-width":"0px","border-color":"rgb(255, 214, 88)","border-style":"none"}',
  ),
  37 => 
  array (
    'id' => '37',
    'handle' => '.tp-caption.medium_light_white',
    'settings' => NULL,
    'hover' => NULL,
    'params' => '{"font-size":"30px","line-height":"36px","font-weight":"300","font-family":"\\"Open Sans\\"","color":"rgb(255, 255, 255)","text-decoration":"none","background-color":"transparent","padding":"0px","border-width":"0px","border-color":"rgb(255, 214, 88)","border-style":"none"}',
  ),
  38 => 
  array (
    'id' => '38',
    'handle' => '.tp-caption.mediumlarge_light_white',
    'settings' => NULL,
    'hover' => NULL,
    'params' => '{"font-size":"34px","line-height":"40px","font-weight":"300","font-family":"\\"Open Sans\\"","color":"rgb(255, 255, 255)","text-decoration":"none","background-color":"transparent","padding":"0px","border-width":"0px","border-color":"rgb(255, 214, 88)","border-style":"none"}',
  ),
  39 => 
  array (
    'id' => '39',
    'handle' => '.tp-caption.mediumlarge_light_white_center',
    'settings' => NULL,
    'hover' => NULL,
    'params' => '{"font-size":"34px","line-height":"40px","font-weight":"300","font-family":"\\"Open Sans\\"","color":"#ffffff","text-decoration":"none","background-color":"transparent","padding":"0px 0px 0px 0px","text-align":"center","border-width":"0px","border-color":"rgb(255, 214, 88)","border-style":"none"}',
  ),
  40 => 
  array (
    'id' => '40',
    'handle' => '.tp-caption.medium_bg_asbestos',
    'settings' => NULL,
    'hover' => NULL,
    'params' => '{"font-size":"20px","line-height":"20px","font-weight":"800","font-family":"\\"Open Sans\\"","color":"rgb(255, 255, 255)","text-decoration":"none","background-color":"rgb(127, 140, 141)","padding":"10px","border-width":"0px","border-color":"rgb(255, 214, 88)","border-style":"none"}',
  ),
  41 => 
  array (
    'id' => '41',
    'handle' => '.tp-caption.medium_light_black',
    'settings' => NULL,
    'hover' => NULL,
    'params' => '{"font-size":"30px","line-height":"36px","font-weight":"300","font-family":"\\"Open Sans\\"","color":"rgb(0, 0, 0)","text-decoration":"none","background-color":"transparent","padding":"0px","border-width":"0px","border-color":"rgb(255, 214, 88)","border-style":"none"}',
  ),
  42 => 
  array (
    'id' => '42',
    'handle' => '.tp-caption.large_bold_black',
    'settings' => NULL,
    'hover' => NULL,
    'params' => '{"font-size":"58px","line-height":"60px","font-weight":"800","font-family":"\\"Open Sans\\"","color":"rgb(0, 0, 0)","text-decoration":"none","background-color":"transparent","border-width":"0px","border-color":"rgb(255, 214, 88)","border-style":"none"}',
  ),
  43 => 
  array (
    'id' => '43',
    'handle' => '.tp-caption.mediumlarge_light_darkblue',
    'settings' => NULL,
    'hover' => NULL,
    'params' => '{"font-size":"34px","line-height":"40px","font-weight":"300","font-family":"\\"Open Sans\\"","color":"rgb(52, 73, 94)","text-decoration":"none","background-color":"transparent","padding":"0px","border-width":"0px","border-color":"rgb(255, 214, 88)","border-style":"none"}',
  ),
  44 => 
  array (
    'id' => '44',
    'handle' => '.tp-caption.small_light_white',
    'settings' => NULL,
    'hover' => NULL,
    'params' => '{"font-size":"17px","line-height":"28px","font-weight":"300","font-family":"\\"Open Sans\\"","color":"rgb(255, 255, 255)","text-decoration":"none","background-color":"transparent","padding":"0px","border-width":"0px","border-color":"rgb(255, 214, 88)","border-style":"none"}',
  ),
  45 => 
  array (
    'id' => '45',
    'handle' => '.tp-caption.roundedimage',
    'settings' => NULL,
    'hover' => NULL,
    'params' => '{"border-width":"0px","border-color":"rgb(34, 34, 34)","border-style":"none"}',
  ),
  46 => 
  array (
    'id' => '46',
    'handle' => '.tp-caption.large_bg_black',
    'settings' => NULL,
    'hover' => NULL,
    'params' => '{"font-size":"40px","line-height":"40px","font-weight":"800","font-family":"\\"Open Sans\\"","color":"rgb(255, 255, 255)","text-decoration":"none","background-color":"rgb(0, 0, 0)","padding":"10px 20px 15px","border-width":"0px","border-color":"rgb(255, 214, 88)","border-style":"none"}',
  ),
  47 => 
  array (
    'id' => '47',
    'handle' => '.tp-caption.mediumwhitebg',
    'settings' => NULL,
    'hover' => NULL,
    'params' => '{"font-size":"30px","line-height":"30px","font-weight":"300","font-family":"\\"Open Sans\\"","color":"rgb(0, 0, 0)","text-decoration":"none","background-color":"rgb(255, 255, 255)","padding":"5px 15px 10px","text-shadow":"none","border-width":"0px","border-color":"rgb(0, 0, 0)","border-style":"none"}',
  ),
  48 => 
  array (
    'id' => '48',
    'handle' => '.tp-caption.medikal-text-center',
    'settings' => '{"hover":"false"}',
    'hover' => '""',
    'params' => '{"font-size":"18px","font-weight":"400","line-height":"24px","color":"#ffffff","text-decoration":"none","background-color":"transparent","text-shadow":"none","text-align":"center","border-width":"0px","border-color":"rgb(0, 0, 0)","border-style":"none"}',
  ),
  49 => 
  array (
    'id' => '49',
    'handle' => '.tp-caption.medikal-large-text',
    'settings' => '{"hover":"false"}',
    'hover' => '""',
    'params' => '{"font-size":"70px","font-weight":"600","line-height":"74px","text-align":"center","color":"#fff","text-shadow":"none","background-color":"transparent","text-decoration":"none","border-width":"0px","border-color":"rgb(255, 255, 255)","border-style":"none"}',
  ),
  50 => 
  array (
    'id' => '50',
    'handle' => '.tp-caption.medikal-top-text',
    'settings' => '{"hover":"false"}',
    'hover' => '""',
    'params' => '{"color":"#fff","font-size":"20px","font-weight":"500","text-align":"center","text-shadow":"none","background-color":"transparent","text-decoration":"none","border-width":"0px","border-color":"rgb(255, 255, 255)","border-style":"none"}',
  ),
); 

    $this->table = 'revslider_css';
    $this->primary = 'id';
    $this->base = 'http://medikal.victheme.com';
  }
}

