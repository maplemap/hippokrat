<?php
/**
 * Single post template for department post type
 *
 * @author jason.xie@victheme.com
 */

get_header();
?>

  <!-- Main Content -->
  <div id="maincontent" class="area clearfix">
    <div class="container-fluid">
      <div class="row">

        <div id="content"
             class="region
			     <?php if (VTCore_Zeus_Utility::getSidebar('department')) {
               echo VTCore_Zeus_Utility::getColumnSize('content');
             } ?>
			     with-sidebar-<?php echo VTCore_Zeus_Utility::getSidebar('department'); ?>">

          <?php
          wp_reset_postdata();
          while (have_posts()) {

            // Build the single post entry using main loop
            the_post();
            get_template_part('templates/content/department');

          }

          ?>
        </div>

        <?php
        // Build sidebar.
        if (VTCore_Zeus_Utility::getSidebar('department') == 'right'
          || VTCore_Zeus_Utility::getSidebar('department') == 'left'
        ) {
          get_sidebar('sidebar');
        }
        ?>

      </div>
    </div>
  </div>


<?php get_footer(); ?>