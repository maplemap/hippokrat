<?php
/**
 * Main Comment Template
 *
 * @author jason.xie@victheme.com
 */

  // Switch the show default
  $show = 'full';
  if (is_singular('page')) {
    $show = 'page';
  }

  $showComment = VTCore_Zeus_Init::getFeatures()->get('show.' . $show . '.comments');
  $showForm = VTCore_Zeus_Init::getFeatures()->get('show.' . $show . '.comment_form');

  // Disable comment entry and form on password
  // protected page
  if (post_password_required() || (!comments_open() && !have_comments())) {
    $showComment = false;
    $showForm = false;
  }

  // Don't even bother to build the markup if
  // conditional logic doesn't allow commenting
  if (!$showComment && !$showForm) {
    return;
  }

  // Load wordpress default comment javascript
  wp_enqueue_script("comment-reply");

?>

  <div id="post-comment" class="post-section clearfix">

      <?php if (post_password_required()) : ?>
        <!-- Password Protected -->
        <div id="post-comment-passworded">
          <div class="alert alert-warning">
            <?php echo esc_html__('This page is password protected. Enter the password to view comments.', 'medikal'); ?>
          </div>
        </div>
      <?php endif; ?>


      <?php if ($showComment) : ?>
        <!-- Comment Entry -->
        <section id="post-comment-entry">

          <header class="comment-title">
            <h1>
              <?php
                global $post;
                if ($post->comment_count < 10 && $post->comment_count > 0) {
                  $post->old_comment_count = $post->comment_count;
                  $post->comment_count = '0' . $post->comment_count;
                }

                comments_number(esc_html__('No Comment On This Post', 'medikal'), wp_kses_post(__('There is <span>01</span> Comment On This Post', 'medikal')), wp_kses_post(__('There are <span>%</span> Comments On This Post', 'medikal')));

                if (isset($post->old_comment_count)) {
                  $post->comment_count = $post->old_comment_count;
                }

              ?>
            </h1>
          </header>


          <?php if (have_comments()) : ?>

            <ul class="media-list">
              <?php wp_list_comments(array(
                'style' => 'ul',
                'short_ping' => TRUE,
                'avatar_size' => 72,
                'format' => 'html5',
                'callback' => array('VTCore_Zeus_Utility', 'getComment'),
              ));
              ?>
            </ul>

            <?php if (get_comment_pages_count() > 1 && get_option('page_comments')) : ?>

              <nav id="comment-nav">
                <?php previous_comments_link('<span class="btn">' . esc_html__('Older comments', 'medikal') . '</span>'); ?>
                <?php next_comments_link('<span class="btn">' . esc_html__('Newer comments', 'medikal') . '</span>'); ?>
              </nav>

            <?php endif; ?>

          <?php endif; ?>


          <?php if (!comments_open()) : ?>
            <!-- Comment Closed -->
            <h4 class="comment-closed">
              <?php echo esc_html__('Comments are closed', 'medikal'); ?>
            </h4>
          <?php endif; ?>

          <?php if (!have_comments() && comments_open()) : ?>
            <!-- No Comment found but commenting is not closed -->
            <p class="comment-empty">
              <?php echo esc_html__('No comment yet.', 'medikal'); ?>
            </p>
          <?php endif; ?>

        </section>
      <?php endif; ?>


  </div>

<?php if ($showForm && comments_open()) : ?>
  <!-- Comment Form -->
  <div id="post-comment-form" class="post-section clearfix">
    <header class="comment-title">
      <h1>
        <?php echo esc_html__('Leave A Comment', 'medikal'); ?>
      </h1>
    </header>

    <?php VTCore_Zeus_Utility::getCommentForm(array('label_submit' => esc_html__('Submit', 'medikal'))); ?>

  </div>

<?php endif; ?>