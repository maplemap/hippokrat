<?php
/**
 * Default Single Page Template
 *
 * @author jason.xie@victheme.com
 */

get_header();
?>

  <!-- Main Content -->
  <div id="maincontent" class="area clearfix">
    <div class="container-fluid">
      <div class="row">

        <div id="content"
             class="region
                <?php if (VTCore_Zeus_Utility::getSidebar('page')) echo VTCore_Zeus_Utility::getColumnSize('content');?>
                <?php echo 'with-sidebar-' . VTCore_Zeus_Utility::getSidebar('page'); ?>">

          <?php
          wp_reset_postdata();
          while (have_posts()) {

            // Build the single post entry using main loop
            the_post();
            get_template_part('templates/content/page');

          }

          ?>
        </div>

        <?php
        // Build sidebar.
        if (VTCore_Zeus_Utility::getSidebar('page') == 'right'
          || VTCore_Zeus_Utility::getSidebar('page') == 'left'
        ) {
          get_sidebar('sidebar');
        }
        ?>

      </div>
    </div>
  </div>


<?php get_footer(); ?>