<?php
/**
 * Main index.php
 * @author jason.xie@victheme.com
 */
?>
<?php get_header(); ?>

  <div id="maincontent" class="area clearfix">
    <div class="container-fluid">

      <div class="row">
        <div id="content"
             class="region
			      <?php if (VTCore_Zeus_Utility::getSidebar('teaser')) {
               echo VTCore_Zeus_Utility::getColumnSize('content');
             } ?>
			      <?php echo 'with-sidebar-' . VTCore_Zeus_Utility::getSidebar('teaser'); ?>">

          <?php if (VTCore_Zeus_Init::getFeatures()->get('options.teasers.title')) : ?>
            <header class="teasers-archive-title row">
              <h1 class="post-header-title text-center col-xs-12 col-sm-8 col-md-8 col-lg-8 col-sm-offset-2 col-sm-offset-2 col-lg-offset-2">
                <?php echo wp_kses_post(VTCore_Zeus_Init::getFeatures()->get('options.teasers.title')); ?>
              </h1>
            </header>
          <?php endif; ?>

          <?php if (VTCore_Zeus_Init::getFeatures()->get('options.teasers.description.left')
            || VTCore_Zeus_Init::getFeatures()->get('options.teasers.description.right')) : ?>
            <div class="teasers-archive-description row">
              <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-sm-offset-2 col-sm-offset-2 col-lg-offset-2">
                <?php echo wp_kses_post(VTCore_Zeus_Init::getFeatures()->get('options.teasers.description.left')); ?>
              </div>
              <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <?php echo wp_kses_post(VTCore_Zeus_Init::getFeatures()->get('options.teasers.description.right')); ?>
              </div>
            </div>
          <?php endif; ?>

          <?php

          // Build the main loop using WpLoop Object
          // @see VTCore_Wordpress_Element WpLoop
          $arguments = array(
            'id' => 'main',
            'queryMain' => TRUE,
            'ajax' => TRUE,
            'attributes' => array(
              'class' => array(
                'id' => 'zeus-main-loop',
                'clearfix' => 'clearfix',
                'template' => 'template-' . VTCore_Zeus_Init::getFeatures()->get('options.teasers.template'),
                'multiple' => 'multiple',
                'mode' => 'list',
              ),
            ),
            'template' => array(
              'items' => VTCore_Zeus_Init::getFeatures()->get('options.teasers.template') . '.php',
              'empty' => 'blog-empty.php',
            ),
            'data' => array(
              'isotope-options' => array(
                'itemSelector' => '.item',
                'layoutMode' => 'fitRows',
                'fitRows' => array(
                  'equalheight' => TRUE,
                  'gutter' => array(
                    'width' => 0,
                    'height' => 0,
                  ),
                ),
                'resizeDelay' => 300,
              ),
            ),
            'grids' => array(
              'columns' => array(
                'mobile' => 12,
                'tablet' => 12,
                'small' => 12,
                'large' => 12,
              ),
            ),
            'show' => TRUE,
          );

          if (isset($contentArgs)) {
            $arguments = VTCore_Utility::arrayMergeRecursiveDistinct($contentArgs, $arguments);
          }

          if ($arguments['show']) {
            $loopObject = new VTCore_Wordpress_Element_WpLoop($arguments);
          }


          // Building the pager elements using WpPager object
          // @see VTCore_Wordpress_Element WpPager
          $arguments = array(
            'id' => $loopObject->getContext('id'),
            'query' => $loopObject->getContext('query'),
            'ajax' => VTCore_Zeus_Init::getFeatures()
              ->get('options.teasers.pager.ajax'),
            'mini' => VTCore_Zeus_Init::getFeatures()
              ->get('options.teasers.pager.mini'),
            'infinite' => VTCore_Zeus_Init::getFeatures()
              ->get('options.teasers.pager.infinite'),
            'attributes' => array(
              'class' => array(
                'text-center',
              ),
            ),
            'show' => TRUE,
          );

          if (isset($pagerArgs)) {
            $arguments = VTCore_Utility::arrayMergeRecursiveDistinct($pagerArgs, $arguments);
          }

          if ($arguments['show']) {
            // Object will be altered via vtcore_wordpress_pager_object_alter action.
            $pagerObject = new VTCore_Wordpress_Element_WpPager($arguments);
          }

          // Rendering the objects
          if (isset($loopObject)) {
            $loopObject->render();
          }

          // Render pager object
          if (isset($pagerObject)) {
            $pagerObject->render();
          }

          ?>
        </div>


        <?php
        // Build sidebar.
        if (VTCore_Zeus_Utility::getSidebar('teaser') == 'right'
          || VTCore_Zeus_Utility::getSidebar('teaser') == 'left'
        ) {

          get_sidebar('sidebar');
        }
        ?>


      </div>
    </div>
  </div>

<?php get_footer(); ?>