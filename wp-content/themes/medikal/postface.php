<?php
/**
 * Postface Area Templates
 *
 * @author jason.xie@victheme.com
 */

?>

<?php if (VTCore_Zeus_Utility::isActiveSidebar('postfaceone')
  || VTCore_Zeus_Utility::isActiveSidebar('postfacetwo')
  || VTCore_Zeus_Utility::isActiveSidebar('postfacethree')
  || VTCore_Zeus_Utility::isActiveSidebar('postfacefour')) : ?>

  <div id="postface" class="area clearfix text-center">
    <div class="container-fluid">
      <div class="row js-isotope" data-isotope-options="<?php echo esc_attr(json_encode(array(
        'itemSelector' => '.region',
        'layoutMode' => 'fitRows',
        'fitRows' => array(
          'equalheight' => true,
          'gutter' => array(
            'width' => 0,
            'height' => 0,
          ),
        ),
        'resizeDelay' => 10,
      )));?>">


        <?php if (VTCore_Zeus_Utility::isActiveSidebar('postfaceone')) : ?>
          <div id="postface-one" class="region <?php echo VTCore_Zeus_Utility::getColumnSize('postfaceone');?>">
            <?php dynamic_sidebar('postfaceone'); ?>
          </div>
        <?php endif;?>



        <?php if (VTCore_Zeus_Utility::isActiveSidebar('postfacetwo')) : ?>
          <div id="postface-two" class="region <?php echo VTCore_Zeus_Utility::getColumnSize('postfacetwo');?>">
            <?php dynamic_sidebar('postfacetwo'); ?>
          </div>
        <?php endif;?>



        <?php if (VTCore_Zeus_Utility::isActiveSidebar('postfacethree')) : ?>
          <div id="postface-three" class="region <?php echo VTCore_Zeus_Utility::getColumnSize('postfacethree');?>">
            <?php dynamic_sidebar('postfacethree'); ?>
          </div>
        <?php endif;?>


        <?php if (VTCore_Zeus_Utility::isActiveSidebar('postfacefour')) : ?>
          <div id="postface-four" class="region <?php echo VTCore_Zeus_Utility::getColumnSize('postfacefour');?>">
            <?php dynamic_sidebar('postfacefour'); ?>
          </div>
        <?php endif;?>

      </div>
    </div>
  </div>
<?php endif;?>
