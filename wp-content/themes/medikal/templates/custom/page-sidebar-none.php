<?php
/*
 Template Name: Page Sidebar None
 Template Description: Template for displaying single page with no sidebar.
 Post Types: page
*/

  // Force use no sidebar
  VTCore_Zeus_Init::getFeatures()->add('sidebars.page', false);

  // Using the generic page.php as the base
  include_once locate_template('page.php', false);


?>