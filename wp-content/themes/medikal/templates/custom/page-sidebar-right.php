<?php
/*
 Template Name: Page Sidebar Right
 Template Description: Template for displaying single page with sidebar right.
 Post Types: page
*/

  // Force use sidebar right
  VTCore_Zeus_Init::getFeatures()->add('sidebars.page', 'right');

  // Using the generic page.php as the base
  include_once locate_template('page.php', false);


?>