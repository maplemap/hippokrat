<?php
/*
 Template Name: Page Sidebar Left
 Template Description: Template for displaying single page with sidebar left.
 Post Types: page
*/

  // Force use sidebar left
  VTCore_Zeus_Init::getFeatures()->add('sidebars.page', 'left');

  // Using the generic page.php as the base
  include_once locate_template('page.php', false);


?>