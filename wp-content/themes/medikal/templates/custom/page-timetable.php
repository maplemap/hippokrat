<?php
/**
Template Name: Services TimeTable
Template Description: Template for displaying post services in a time table format
Post Types: page
Dependencies: victheme_services, victheme_department, victheme_members
 */
?>
<?php get_header(); ?>

  <div id="maincontent" class="area clearfix">
    <div class="container-fluid">

      <div class="row">
        <div id="content"
             class="region">


          <?php get_template_part('templates/content/timetable'); ?>

        </div>

      </div>
    </div>
  </div>

<?php get_footer(); ?>