<?php
/*
 Template Name: Post Sidebar None
 Template Description: Template for displaying single page with no sidebar.
 Post Types: post
*/

  // Force use no sidebar
  VTCore_Zeus_Init::getFeatures()->add('sidebars.full', false);

  // Using the generic single.php as the base
  include_once locate_template('single.php', false);


?>