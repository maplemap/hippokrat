<?php
/*
 Template Name: Blog Listing
 Template Description: Template for displaying Post Type blog listing in grid mode with sidebars.
 Post Types: page
*/

  // Passing custom argument to index.php
  $contentArgs = array(
    'queryMain' => false,
    'query' => false,
    'queryArgs' => array(
      'post_type' => 'post',
      'posts_per_page' => 12,
    ),
  );

  // Using the generic index.php as the base
  include_once locate_template('index.php', false);

?>