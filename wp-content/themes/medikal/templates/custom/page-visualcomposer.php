<?php
/*
 Template Name: Visual Composer
 Template Description: Template for use with VisualComposer.
 Post Types: page
*/
?>
<?php get_header();?>
<div id="maincontent" class="single clearfix">
<?php while (have_posts()) : the_post(); the_content(); endwhile;?>
</div>
<?php get_footer();?>