<?php
/*
 Template Name: Post Sidebar Right
 Template Description: Template for displaying single page with sidebar right.
 Post Types: post
*/

  // Force use sidebar left
  VTCore_Zeus_Init::getFeatures()->add('sidebars.full', 'right');

  // Using the generic single.php as the base
  include_once locate_template('single.php', false);


?>