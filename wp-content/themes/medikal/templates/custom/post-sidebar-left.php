<?php
/*
 Template Name: Post Sidebar Left
 Template Description: Template for displaying single page with sidebar left.
 Post Types: post
*/

  // Force use sidebar left
  VTCore_Zeus_Init::getFeatures()->add('sidebars.full', 'left');

  // Using the generic single.php as the base
  include_once locate_template('single.php', false);


?>