<?php
/**
 * Custom separator template
 * This template will add the capability to
 * define the height and width of the line
 *
 * This is not as clean as it can be, the
 * reason is to follow the original template
 * as much as possible.
 *
 * @author jason.xie@victheme.com
 */

// Explode all the attributes
extract(shortcode_atts(array(
  'el_width' => '',
  'style' => '',
  'color' => '',
  'accent_color' => '',
  'el_class' => '',
  'border_width' => '',
  'height' => 1,
  'maxwidth' => '',
  'align' => '',
), $atts, 'vc_separator'));


// Building css classes
$class = "vc_separator";
$class .= ($el_width!='') ? ' vc_el_width_'.$el_width : ' vc_el_width_100';
$class .= ($color!= '' && 'custom' != $color) ? ' vc_sep_color_' . $color : '';
$class .= $this->getExtraClass($el_class);

if ($align == 'align_left') {
  $class .= ' pull-left';
}

if ($align == 'align_right') {
  $class .= ' pull-right';
}

// Allows other to modify the element class
$css_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $class, $this->settings['base'], $atts);


$styles = array(
  'border-top-width' => '1px',
  'border-top-style' => 'solid',
);

if (!empty($border_width)) {
  $styles['border-top-width'] = $border_width;
  $styles['height'] = $border_width;
}

if ('custom' == $color && $accent_color!='') {
  $styles['border-top-color'] = $accent_color;
}

if (!empty($style)) {
  $styles['border-top-style'] = $style;
}
if (!empty($height)) {
  $styles['border-top-width'] = $height;
  $styles['height'] = $height;
}

$wrapper = array();
if (!empty($maxwidth)) {
  $wrapper['max-width'] = $maxwidth;
}

// Build the separator
$separator = new VTCore_Html_Element(array(
  'type' => 'div',
  'text' => '',
  'attributes' => array(
    'class' => array('wpb_content_element clearfix'),
  ),
));

$separator
  ->Element(array(
    'type' => 'div',
    'text' => '',
    'attributes' => array(
      'class' => array($class),
    ),
    'styles' => $wrapper,
  ))
  ->lastChild()
  ->Element(array(
    'type' => 'span',
    'attributes' => array(
      'class' => array(
        'vc_sep_holder',
      ),
    ),
  ))
  ->lastChild()
  ->Element(array(
    'type' => 'span',
    'attributes' => array(
      'class' => array(
        'vc_sep_line',
      ),
    ),
    'styles' => $styles,
  ));

$separator->render();
?>