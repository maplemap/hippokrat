<?php
/**
 * Template for displaying the single attachment page type image
 *
 * This template must be called from inside the loop.
 *
 * @author jason.xie@victheme.com
 *
 */

  $show = VTCore_Zeus_Init::getFeatures()->get('show.full');
  $show = array_filter($show);

  list($source, $width, $height) = wp_get_attachment_image_src($post->ID, 'full');

?>

<article
  id="post-<?php the_ID(); ?>" <?php post_class('single clearfix'); ?>>

  <div class="post-attachment">
    <?php // Dont attempt to use get_post_thumbnail, it wont work with attachment page. ?>
    <img src="<?php echo esc_url($source); ?>" width="<?php echo esc_attr($width);?>" height="<?php echo esc_attr($height);?>"/>

    <?php if (!empty($post->post_excerpt)) : ?>
      <figcaption class="wp-caption"><?php echo wp_kses_post($post->post_excerpt); ?></figcaption>
    <?php endif;?>
  </div>


  <?php if (!empty($show)) : ?>
    <section
      class="post-content <?php echo esc_attr($contentGrid); ?>">


      <?php if (isset($show['title'])) : ?>
        <header>
          <h1 class="post-header-title">
            <span class="post-title">
            <?php
            echo wp_kses_post(get_the_title());
            ?>
              </span>
            <?php
            if (isset($show['tag'])) {
              $tags = wp_get_post_tags($post->ID);
              if (!empty($tags)) {
                $tag = array_shift($tags);
                $tag_link = get_tag_link($tag->term_id);
                echo '<a class="label label-tags" href="' . esc_url($tag_link) . '" title="' . esc_attr($tag->name) . '">' . wp_kses_post($tag->name) . '</a>';
              }
            }
            ?>
          </h1>

        </header>
      <?php endif; ?>

      <?php if ($show['byline']) : ?>
        <div class="post-by">
            <span class="post-by-author rounded">
              <?php echo wp_kses_post(get_avatar($post->post_author, '54')); ?>
            </span>
            <span class="post-by-line">
              <?php echo sprintf(__('by %s on %s', 'medikal'), get_the_author_link(), get_the_date('d.m.Y - H:iA')); ?>
            </span>
        </div>
      <?php endif; ?>

      <?php if ($show['content']) : ?>
        <div class="post-content">
          <?php

          // Retrieves content
          the_content();

          // Retrieves Pager
          $pager = VTCore_Zeus_Utility::getPostPager();
          if (!empty($pager)) {
            echo '<div class="page-links text-center">' . $pager . '</div>';
          }
          ?>
        </div>
      <?php endif; ?>

      <?php if ($show['share']) : ?>
        <div class="post-social post-social-tags well">
          <span class="post-social-text">
            <?php echo esc_html__('Share on :', 'medikal'); ?>
          </span>
          <?php
          $object = new VTCore_Wordpress_Element_WpSocialShare();
          $object->render();
          ?>
        </div>
      <?php endif; ?>

      <?php
      if ($show['post_nav']) {
        get_template_part('templates/content/post-nav');
      }
      ?>
    </section>
  <?php endif; ?>
</article>

<?php
// Build the comments
if ($show['comments'] || $show['comment_form']) {
  comments_template();
}
?>
