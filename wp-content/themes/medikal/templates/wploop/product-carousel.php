<?php
/**
 * Template to be used by Victheme Woocommerce
 * catalog plugin.
 *
 * We just bridge this with the default
 * content-product.php compatible with woocommerce.
 *
 * This is a special wrapper for configuring the correct
 * configuration for displaying the content-product.php
 * with wpcarousel object.
 *
 * @author jason.xie@victheme.com
 */

  $gridObject = new VTCore_Bootstrap_Grid_Column(array(
    'columns' => array(
      'mobile' => 12,
      'small' => 12,
      'medium' => 12,
      'large' => 12,
    ),
  ));

  if (isset($this) && is_a($this, 'VTCore_Wordpress_Element_WpCarousel')) {
    $this->addClass('shop-catalog');
  }

  include locate_template('woocommerce' . DIRECTORY_SEPARATOR . 'content-product.php', false);
?>
