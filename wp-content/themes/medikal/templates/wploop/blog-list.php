<?php
/**
 * Template for the post teasers
 *
 * @author jason.xie@victheme.com
 */

  VTCore_Wordpress_Utility::loadAsset('jquery-imgliquid');

  $show = VTCore_Zeus_Init::getFeatures()->get('show.teaser');
  $show = array_filter($show);

  if (empty($show)) {
    return;
  }

  $contentGrid = 'col-xs-12 col-sm-12 col-md-12 col-lg-12';
  $class = array(
    'post-teasers',
    'row',
    'column',
    'item',
    'multiple',
    'clearfix',
    $this->getContext('objects.columns')->getClass(),
    ($post->post_delta % 2) ? 'odd' : 'even',
  );

  if (has_post_thumbnail() && isset($show['image'])) {
    $class[] = 'with-thumbnail';
    $contentGrid = 'col-xs-6 col-sm-5 col-md-5 col-lg-5';
    $imageGrid = 'col-xs-6 col-sm-7 col-md-7 col-lg-7';
  }

?>
<article id="post-<?php the_ID(); ?>" <?php post_class($class); ?>>

  <?php if (has_post_thumbnail() && isset($show['image'])) : ?>
    <section class="post-image <?php echo esc_attr($imageGrid); ?>">
      <a class="post-thumbnail-link"
         href="<?php esc_url(the_permalink()); ?>"
         alt="<?php echo esc_attr(get_the_title()); ?>">

        <figure class="post-thumbnail imgLiquidFill imgLiquid"
                data-imgLiquidFill="true"
                data-imgLiquid-horizontalAlign="center"
                data-imgLiquid-verticalAlign="center">

          <?php echo get_the_post_thumbnail($post->ID, 'large'); ?>
        </figure>
      </a>
    </section>
  <?php endif; ?>

  <section class="post-content text-center <?php echo esc_attr($contentGrid); ?>">

    <?php if (isset($show['title'])) : ?>
      <header>
        <h1 class="post-title text-uppercase">
          <?php
            echo '<a class="post-title" href="' . esc_url(get_the_permalink()) . '" title="'. esc_attr(get_the_title()) . '">' . wp_kses_post(get_the_title()) . '</a>';
          ?>
        </h1>
      </header>
    <?php endif; ?>

    <?php if ($show['byline']) : ?>
      <div class="post-by">
        <span class="post-by-line">
          <?php echo sprintf(__('post by %s on %s', 'medikal'), '<span class="accents">' . get_the_author_link() . '</span>', '<span class="accents">' . get_the_date('F jS, Y') . '</span>'); ?>
        </span>
      </div>
    <?php endif; ?>

    <?php if ($show['line']) : ?>
      <div class="post-line"></div>
    <?php endif;?>

    <?php if ($show['excerpt']) : ?>
      <div class="post-excerpt">
        <?php the_excerpt(); ?>
      </div>
    <?php endif; ?>

    <?php if ($show['readmore']) : ?>
      <a class="post-readmore btn btn-primary" href="<?php echo get_the_permalink() ?>" title="<?php echo esc_attr(get_the_title()); ?>">
        <?php echo esc_html__('View More', 'medikal'); ?>
      </a>
    <?php endif; ?>

  </section>
</article>
