<?php
/**
 * Single archive content template for services post
 * This template must be used inside loop and VTCore
 * loop object.
 *
 * This template utilize the VTCore_Zeus_Config_Options
 * via VTCore_Zeus_Init::getFeatures() with key show.services.archive
 *
 * Also it uses the VTCore_Department_Entity for
 * building the services object markup.
 *
 * @author jason.xie@victheme.com
 */

  // Get the services entity
  $services = isset($post->services) ? $post->services : false;

  if (!is_a($services, 'VTCore_Services_Entity')) {
    return;
  }

  if (is_a($this, 'VTCore_Wordpress_Element_WpCarousel')) {
    $this->addClass('services-carousel carousel-grid');
  }

  // Load Assets
  VTCore_Wordpress_Utility::loadAsset('jquery-imgliquid');
  VTCore_Wordpress_Utility::loadAsset('font-awesome');

  // Configure the correct classes
  if (!isset($classes)) {
    $classes = array(
      'post-teasers',
      'row',
      'column',
      'item',
      'multiple',
      'clearfix',
      'post-services-teasers',
      'text-center',
      'services-grid-items',
      'services-items',
      'services-item-' . $post->post_delta,
      $this->getContext('objects.columns')->getClass(),
      ($post->post_delta % 2) ? 'odd' : 'even',
    );
  }

  // Define the jsflip options
  if (!isset($jsFlip)) {
    $jsFlip = esc_attr(json_encode(array(
      'axis' => 'y',
      'trigger' => 'click',
      'autosize' => TRUE,
      //'forceHeight' => true,
      'front' => '.post-split.top',
      'back' => '.post-split.bottom',
    )));
  }


?>

  <div id="services-<?php echo esc_attr($post->ID); ?>" <?php post_class($classes);?>>

      <?php if (VTCore_Zeus_Init::getFeatures()->get('show.services.archive.image')) : ?>
        <figure class="post-thumbnail imgLiquidFill imgLiquid"
                data-imgLiquidFill="true"
                data-imgLiquid-horizontalAlign="center"
                data-imgLiquid-verticalAlign="center">

          <?php echo $services->getImage('large'); ?>

          <?php if (VTCore_Zeus_Init::getFeatures()->get('show.services.archive.readmore')) : ?>
            <div class="post-readmore">
              <a href="<?php the_permalink(); ?>" alt="<?php esc_attr($post->post_title); ?>">
                <i class="fa fa-plus"></i>
              </a>
            </div>
          <?php endif; ?>

        </figure>
      <?php endif; ?>

      <div class="post-back-inner">
      <?php if (VTCore_Zeus_Init::getFeatures()->get('show.services.archive.title')) : ?>
        <h3 class="post-title clearfix clearboth text-uppercase text-tailor">
          <?php echo the_title();  ?>
        </h3>
      <?php endif; ?>

      <?php if ($services->getPrice() && VTCore_Zeus_Init::getFeatures()->get('show.services.archive.price')) : ?>
        <div class="services-price accents clearfix clearboth">
          <?php echo $services->getPrice();  ?>
        </div>
      <?php endif; ?>

      <?php if (VTCore_Zeus_Init::getFeatures()->get('show.services.archive.readmore')
                && !VTCore_Zeus_Init::getFeatures()->get('show.services.archive.image')) : ?>
        <div class="post-readmore">
          <a href="<?php the_permalink(); ?>" alt="<?php esc_attr($post->post_title); ?>">
            <?php echo esc_html__('View Service', 'medikal'); ?>
          </a>
        </div>
      <?php endif; ?>
      </div>

    </div>


