<?php
/**
 * Single archive content template for department post
 * This template must be used inside loop and VTCore
 * loop object.
 *
 * This template utilize the VTCore_Zeus_Config_Options
 * via VTCore_Zeus_Init::getFeatures() with key show.department.archive
 *
 * Also it uses the VTCore_Department_Entity for
 * building the department object markup.
 *
 * this is just a wrapper for the department-grid.php with
 * configuration suitable for wpcarousel object
 *
 * @author jason.xie@victheme.com
 */

  // Configure the correct classes
  $classes = array(
    'column',
    'item',
    'multiple',
    'clearfix',
    'text-center',
    'post-department-teasers',
    'department-items',
    'department-item-' . $post->post_delta,
    ($post->post_delta % 2) ? 'odd' : 'even',
  );

  if (isset($this) && is_a($this, 'VTCore_Wordpress_Element_WpCarousel')) {
    $this->addClass('department-carousel-full');
  }

  // Get the department entity
  $department = isset($post->department) ? $post->department : false;

  if (!is_a($department, 'VTCore_Department_Entity')) {
    return;
  }

?>

<article id="department-full-<?php echo esc_attr($post->ID); ?>" <?php post_class($classes);?>>

  <header>
    <h1 class="post-header-title">
      <?php
      $words = (array) explode(' ', get_the_title());
      $processed = array();
      foreach ($words as $word) {
        $processed[] = '<span class="pull-inline header-with-marker-stripe">' . wp_kses_post($word) . '</span>';
      }

      echo implode('<span class="pull-inline empty-element header-with-marker-stripe">&nbsp;</span>', $processed);
      ?>
    </h1>
  </header>

  <section class="post-content">
    <?php the_excerpt(); ?>
  </section>

  <div class="post-readmore">
    <a href="<?php the_permalink(); ?>" alt="<?php esc_attr($post->post_title); ?>">
      <span class="readmore-title"><?php echo esc_html__('Department Information', 'medikal'); ?></span>
      <i class="fa fa-arrow-right"></i>
    </a>
  </div>

</article>
