<?php
/**
 * Single archive content template for services post
 * This template must be used inside loop and VTCore
 * loop object.
 *
 * This template utilize the VTCore_Zeus_Config_Options
 * via VTCore_Zeus_Init::getFeatures() with key show.services.archive
 *
 * Also it uses the VTCore_Department_Entity for
 * building the services object markup.
 *
 * @author jason.xie@victheme.com
 */

  // Get the services entity
  $services = isset($post->services) ? $post->services : false;

  if (!is_a($services, 'VTCore_Services_Entity')) {
    return;
  }

  if (is_a($this, 'VTCore_Wordpress_Element_WpCarousel')) {
    $this->addClass('services-carousel carousel-flip');
  }

  // Load Assets
  VTCore_Wordpress_Utility::loadAsset('jquery-flip');
  VTCore_Wordpress_Utility::loadAsset('jquery-verticalcenter');
  VTCore_Wordpress_Utility::loadAsset('jquery-imgliquid');
  VTCore_Wordpress_Utility::loadAsset('jquery-texttailor');

  // Configure the correct classes
  if (!isset($classes)) {
    $classes = array(
      'post-teasers',
      'row',
      'column',
      'item',
      'multiple',
      'clearfix',
      'post-services-teasers',
      'text-center',
      'services-flip-items',
      'services-items',
      'services-item-' . $post->post_delta,
      $this->getContext('objects.columns')->getClass(),
      ($post->post_delta % 2) ? 'odd' : 'even',
    );
  }

  // Define the jsflip options
  if (!isset($jsFlip)) {
    $jsFlip = json_encode(array(
      'axis' => 'y',
      'trigger' => 'click',
      'autosize' => TRUE,
      //'forceHeight' => true,
      'front' => '.post-split.top',
      'back' => '.post-split.bottom',
    ));
  }


?>

  <div id="services-<?php echo esc_attr($post->ID); ?>" <?php post_class($classes);?>>

    <div class="js-flip services-teasers-wrapper" data-flip-options="<?php echo esc_attr($jsFlip); ?>">

      <div class="post-split top vertical-center" data-vertical-force="true">
        <div class="post-back-inner vertical-target vertical-no-reset">

          <?php if ($services->getIcon() && VTCore_Zeus_Init::getFeatures()->get('show.services.archive.icon')) : ?>
            <div class="services-icon clearfix clearboth">
              <?php echo $services->getIcon(); ?>
            </div>
          <?php endif; ?>

          <?php if (VTCore_Zeus_Init::getFeatures()->get('show.services.archive.title')) : ?>
            <h3 class="post-title clearfix clearboth text-uppercase text-tailor">
              <?php the_title();  ?>
            </h3>
          <?php endif; ?>

          <?php if (VTCore_Zeus_Init::getFeatures()->get('show.services.archive.excerpt')) : ?>
            <div class="post-excerpt clearboth text-tailor">
              <?php echo get_the_excerpt(); ?>
            </div>
          <?php endif; ?>

        </div>
      </div>

      <div class="post-split top bottom">
        <?php if (VTCore_Zeus_Init::getFeatures()->get('show.services.archive.image')) : ?>
          <figure class="post-thumbnail imgLiquidFill imgLiquid"
                  data-imgLiquidFill="true"
                  data-imgLiquid-horizontalAlign="center"
                  data-imgLiquid-verticalAlign="center">

            <?php echo $services->getImage('large'); ?>
          </figure>
        <?php endif; ?>

        <div class="post-back-inner">
        <?php if (VTCore_Zeus_Init::getFeatures()->get('show.services.archive.title')) : ?>
          <h3 class="post-title clearfix clearboth text-uppercase text-tailor">
            <?php echo the_title();  ?>
          </h3>
        <?php endif; ?>

        <?php if ($services->getPrice() && VTCore_Zeus_Init::getFeatures()->get('show.services.archive.price')) : ?>
          <div class="services-price accents clearfix clearboth">
            <?php echo $services->getPrice();  ?>
          </div>
        <?php endif; ?>

        <?php if (VTCore_Zeus_Init::getFeatures()->get('show.services.archive.readmore')) : ?>
          <div class="post-readmore">
            <a href="<?php the_permalink(); ?>" alt="<?php esc_attr($post->post_title); ?>">
              <?php echo esc_html__('View Service', 'medikal'); ?>
            </a>
          </div>
        <?php endif; ?>
        </div>
      </div>

    </div>

  </div>
