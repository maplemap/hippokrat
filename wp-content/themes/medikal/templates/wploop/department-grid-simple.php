<?php
/**
 * Single archive content template for department post
 * This template must be used inside loop and VTCore
 * loop object.
 *
 * This template utilize the VTCore_Zeus_Config_Options
 * via VTCore_Zeus_Init::getFeatures() with key show.department.archive
 *
 * Also it uses the VTCore_Department_Entity for
 * building the department object markup.
 *
 * @author jason.xie@victheme.com
 */

  // Get the department entity
  $department = isset($post->department) ? $post->department : false;

  if (!is_a($department, 'VTCore_Department_Entity')) {
    return;
  }

  VTCore_Wordpress_Utility::loadAsset('jquery-verticalcenter');

  // Configure the correct classes
  if (!isset($classes)) {
    $classes = array(
      'post-teasers',
      'row',
      'column',
      'item',
      'multiple',
      'clearfix',
      'text-center',
      'post-department-teasers',
      'department-items',
      'department-item-' . $post->post_delta,
      $this->getContext('objects.columns')->getClass(),
      ($post->post_delta % 2) ? 'odd' : 'even',
    );
  }

?>

  <div id="department-<?php echo esc_attr($post->ID); ?>" <?php post_class($classes);?>>

    <?php echo safecss_filter_attr($department->getTeaserStyle('#department-' . $post->ID . ' .post-back-inner')); ?>

    <div class="post-back-inner">
    <?php if ($department->getTeaserIcon() && VTCore_Zeus_Init::getFeatures()->get('show.department.archive.icon')) : ?>
      <div class="department-icon clearfix">
        <?php echo $department->getTeaserIcon(); ?>
      </div>
    <?php endif; ?>

    <?php if ($department->getTeaserTitle() && VTCore_Zeus_Init::getFeatures()->get('show.department.archive.title')) : ?>
      <h3 class="post-title clearfix text-uppercase">
        <?php echo $department->getTeaserTitle();  ?>
      </h3>
    <?php endif; ?>

      <?php if ($department->getTeaserDescription() && VTCore_Zeus_Init::getFeatures()->get('show.department.archive.description')) : ?>
        <div class="post-excerpt">
          <?php echo $department->getTeaserDescription(); ?>
        </div>
      <?php endif; ?>

      <?php if (VTCore_Zeus_Init::getFeatures()->get('show.department.archive.readmore')) : ?>
        <div class="post-readmore">
          <a href="<?php the_permalink(); ?>" alt="<?php esc_attr($post->post_title); ?>">
            <span class="readmore-title"><?php echo esc_html__('View More', 'medikal'); ?></span>
            <i class="fa fa-arrow-right"></i>
          </a>
        </div>
      <?php endif; ?>
    </div>

  </div>

