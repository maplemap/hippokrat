<?php
/**
 * Single archive content template for department post
 * This template must be used inside loop and VTCore
 * loop object.
 *
 * This template utilize the VTCore_Zeus_Config_Options
 * via VTCore_Zeus_Init::getFeatures() with key show.department.archive
 *
 * Also it uses the VTCore_Department_Entity for
 * building the department object markup.
 *
 * this is just a wrapper for the department-grid.php with
 * configuration suitable for wpcarousel object
 *
 * @author jason.xie@victheme.com
 */

  // Configure the correct classes
  $classes = array(
    'column',
    'item',
    'multiple',
    'clearfix',
    'text-center',
    'post-department-teasers',
    'department-items',
    'department-item-' . $post->post_delta,
    ($post->post_delta % 2) ? 'odd' : 'even',
  );

  if (isset($this) && is_a($this, 'VTCore_Wordpress_Element_WpCarousel')) {
    $this->addClass('department-thumb-list');
    $this->addContext('slick.vertical', true);
    $this->addContext('slick.infinite', false);
  }

  // Get the department entity
  $department = isset($post->department) ? $post->department : false;

  if (!is_a($department, 'VTCore_Department_Entity')) {
    return;
  }
?>


<div id="department-thumb-list-<?php echo esc_attr($post->ID); ?>" <?php post_class($classes);?>>
  <?php echo $department->getTeaserTitle();  ?>
</div>



