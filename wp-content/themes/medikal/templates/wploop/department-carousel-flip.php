<?php
/**
 * Single archive content template for department post
 * This template must be used inside loop and VTCore
 * loop object.
 *
 * This template utilize the VTCore_Zeus_Config_Options
 * via VTCore_Zeus_Init::getFeatures() with key show.department.archive
 *
 * Also it uses the VTCore_Department_Entity for
 * building the department object markup.
 *
 * This is just a wrapper for department-flip.php template
 * for configuring to be suitable for wpcarousel object.
 *
 * @author jason.xie@victheme.com
 */

  // Configure the correct classes
  if (!isset($classes)) {
    $classes = array(
      'post-teasers',
      'row',
      'column',
      'item',
      'multiple',
      'clearfix',
      'post-department-teasers',
      'text-center',
      'department-items',
      'department-item-' . $post->post_delta,
      ($post->post_delta % 2) ? 'odd' : 'even',
    );
  }

  if (isset($this) && is_a($this, 'VTCore_Wordpress_Element_WpCarousel')) {
    $this->addClass('department-flip');
  }

  include VTCore_Wordpress_Utility::locateTemplate('department-flip.php');

?>
