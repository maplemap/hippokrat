<?php
/**
 * Template for building the services carousel
 * best used with wpcarousel object.
 *
 * This is just a wrapper for the services-flip.php
 * template, with configuration suitable for wpcarousel
 * object
 *
 * @author jason.xie@victheme.com
 */

  // Configure the correct classes
  $classes = array(
    'post-teasers',
    'row',
    'column',
    'item',
    'multiple',
    'clearfix',
    'post-services-teasers',
    'services-flip-items',
    'text-center',
    'services-items',
    'services-item-' . $post->post_delta,
    ($post->post_delta % 2) ? 'odd' : 'even',
  );


  include VTCore_Wordpress_Utility::locateTemplate('services-flip.php');

?>

