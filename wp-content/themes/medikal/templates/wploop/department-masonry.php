<?php
/**
 * Single archive content template for department post
 * This template must be used inside loop and VTCore
 * loop object.
 *
 * This template utilize the VTCore_Zeus_Config_Options
 * via VTCore_Zeus_Init::getFeatures() with key show.department_grid
 *
 * Also it uses the VTCore_Department_Entity for
 * building the department object markup.
 *
 * @author jason.xie@victheme.com
 */

  // Get the department entity
  $department = isset($post->department) ? $post->department : false;

  if (!is_a($department, 'VTCore_Department_Entity')) {
    return;
  }

  // Process parent element to add settings that must
  // be used for this template!
  if (is_a($this, 'VTCore_Wordpress_Element_WpLoop')) {
    $this->addClass('department-masonry');
    $this->addData('isotope-options.layoutMode', 'fitRows');
    $this->addData('isotope-options.fitRows.equalheight', true);
    $this->addData('isotope-options.itemSelector', '.item');
  }

  // Build the variable delta column width
  // This depends on the VTCore Loop delta
  // This is configured for 12 posts per
  // page fetching, change the array keys
  // if you need different page per post.
  $variableColumns = array(
    '1' => array(
      'columns' => array(
        'mobile' => 12,
        'tablet' => 6,
        'small' => 6,
        'large' => 6,
      ),
    ),
    '2' => array(
      'columns' => array(
        'mobile' => 12,
        'tablet' => 6,
        'small' => 3,
        'large' => 3,
      ),
    ),
    '3' => array(
      'columns' => array(
        'mobile' => 12,
        'tablet' => 6,
        'small' => 3,
        'large' => 3,
      ),
    ),
    '4' => array(
      'columns' => array(
        'mobile' => 12,
        'tablet' => 6,
        'small' => 3,
        'large' => 3,
      ),
    ),
    '5' => array(
      'columns' => array(
        'mobile' => 12,
        'tablet' => 6,
        'small' => 3,
        'large' => 3,
      ),
    ),
    '6' => array(
      'columns' => array(
        'mobile' => 12,
        'tablet' => 6,
        'small' => 6,
        'large' => 6,
      ),
    ),
  );

  // Fetch the correct column size
  if ($post->post_delta <= 6) {
    $index = $post->post_delta;
  }
  else {
    $index = ($post->post_delta - ((ceil($post->post_delta / 6) - 1) * 6));
  }


  VTCore_Wordpress_Utility::loadAsset('jquery-imgliquid');

  // Configure the correct classes
  $classes = array(
    'post-teasers',
    'row',
    'column',
    'item',
    'multiple',
    'clearfix',
    'post-department-teasers',
    'department-items',
    'department-item-' . $post->post_delta,
    ($post->post_delta % 2) ? 'odd' : 'even',
  );

  // Dynamically modify the loop column generator object
  if (isset($index) && isset($variableColumns[$index])) {

    $this->getContext('objects.columns')->setContext($variableColumns[$index])->buildColumnAttributes();
    $classes[] = $this->getContext('objects.columns')->getClass();

    switch ($index) {
      case '1' :
        $classes[] = 'height-one';
        break;

      case '2' :
      case '3' :
        $classes[] = 'height-two';
        break;

      case '4' :
      case '5' :
        $classes[] = 'height-three';
        break;

      case '6' :
        $classes[] = 'height-four';
        break;
    }
  }


?>

  <div id="department-<?php echo esc_attr($post->ID); ?>" <?php post_class($classes);?>>
    <div class="entry-wrapper">
      <?php if ($department->getTeaserImage('large') && VTCore_Zeus_Init::getFeatures()->get('show.department.archive.image')) : ?>
        <a class="post-thumbnail-link"
           href="<?php esc_url(the_permalink()); ?>"
           alt="<?php echo esc_attr(get_the_title()); ?>">

          <figure class="post-thumbnail imgLiquidFill imgLiquid"
                  data-imgLiquidFill="true"
                  data-imgLiquid-horizontalAlign="center"
                  data-imgLiquid-verticalAlign="center">

            <?php echo $department->getTeaserImage('full'); ?>
          </figure>
        </a>
      <?php endif; ?>

    <div class="department-masonry-hover-wrapper">
      <div class="entry-wrapper">
        <?php if ($department->getTeaserTitle() && VTCore_Zeus_Init::getFeatures()->get('show.department.archive.title')) : ?>
          <h3 class="post-title clearfix">
            <?php echo $department->getTeaserTitle();  ?>
          </h3>
        <?php endif; ?>

        <?php if ($department->getTeaserSlogan() && VTCore_Zeus_Init::getFeatures()->get('show.department.archive.slogan')) : ?>
          <div class="post-excerpt clearfix">
            <?php echo wp_trim_words($department->getTeaserSlogan(), 5); ?>
          </div>
        <?php endif; ?>

        <?php if (VTCore_Zeus_Init::getFeatures()->get('show.department.archive.readmore')) : ?>
          <div class="post-readmore">
            <a href="<?php the_permalink(); ?>" alt="<?php esc_attr($post->post_title); ?>">
              <i class="fa fa-search fa-flip-horizontal"></i>
            </a>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>

  </div>

