<?php
/**
 * Single archive content template for members post
 * This template must be used inside loop and VTCore
 * loop object.
 *
 * This template utilize the VTCore_Zeus_Config_Options
 * via VTCore_Zeus_Init::getFeatures() with key show.members.archive
 *
 * Also it uses the VTCore_Department_Entity for
 * building the members object markup.
 *
 * @author jason.xie@victheme.com
 */

  // Get the members entity
  $members = isset($post->members) ? $post->members : false;

  if (!is_a($members, 'VTCore_Members_Entity')) {
    return;
  }

  VTCore_Wordpress_Utility::loadAsset('jquery-imgliquid');

  // Configure the correct classes
  if (!isset($classes)) {
    $classes = array(
      'post-teasers',
      'column',
      'item',
      'multiple',
      'clearfix',
      'post-members-teasers',
      'members-items',
      'text-center',
      'members-item-' . $post->post_delta,
      $this->getContext('objects.columns')->getClass(),
      ($post->post_delta % 2) ? 'odd' : 'even',
    );
  }

?>

  <div id="members-<?php echo esc_attr($post->ID); ?>" <?php post_class($classes);?>>

    <?php if ( VTCore_Zeus_Init::getFeatures()->get('show.members.archive.image') && has_post_thumbnail()) : ?>
      <a class="post-thumbnail-link"
         href="<?php esc_url(the_permalink()); ?>"
         alt="<?php echo esc_attr(get_the_title()); ?>">
        <figure class="post-thumbnail imgLiquidFill imgLiquid"
                data-imgLiquidFill="true"
                data-imgLiquid-horizontalAlign="center"
                data-imgLiquid-verticalAlign="center">

          <?php echo get_the_post_thumbnail((int) $post->ID, 'large'); ?>
        </figure>
      </a>
    <?php endif; ?>


    <div class="post-inner">
    <?php if (VTCore_Zeus_Init::getFeatures()->get('show.members.archive.name')) : ?>
      <h3 class="post-title clearboth clearfix">
        <a href="<?php echo get_permalink(); ?>"
           class="post-title-link"
           alt="<?php esc_attr($post->post_title); ?>">
          <?php the_title();  ?>
        </a>
      </h3>
    <?php endif; ?>


    <?php if (VTCore_Zeus_Init::getFeatures()->get('show.members.archive.speciality') && $members->getSpeciality()) : ?>
      <div class="members-speciality clearboth clearfix">
        <?php echo $members->getSpeciality(); ?>
      </div>
    <?php endif; ?>


    <?php if (VTCore_Zeus_Init::getFeatures()->get('show.members.archive.excerpt')) : ?>
      <div class="post-excerpt clearboth clearfix">
        <?php echo get_the_excerpt(); ?>
      </div>
    <?php endif; ?>

    <?php if (VTCore_Zeus_Init::getFeatures()->get('show.members.archive.social')) : ?>
      <div class="clearboth clearfix">
        <?php echo $members->getSocial(); ?>
      </div>
    <?php endif; ?>

    <?php if (VTCore_Zeus_Init::getFeatures()->get('show.members.archive.readmore')) : ?>
      <a class="post-readmore btn btn-primary clearboth text-bold"
         href="<?php the_permalink(); ?>"
         alt="<?php esc_attr($post->post_title); ?>">

        <?php echo esc_html__('View Profile', 'medikal'); ?>
      </a>
    <?php endif; ?>
    </div>


  </div>

