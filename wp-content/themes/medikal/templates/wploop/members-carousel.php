<?php
/**
 * Single archive content template for members post
 * This template must be used inside loop and VTCore
 * loop object.
 *
 * This template utilize the VTCore_Zeus_Config_Options
 * via VTCore_Zeus_Init::getFeatures() with key show.members.archive
 *
 * Also it uses the VTCore_Department_Entity for
 * building the members object markup.
 *
 * This is just a wrapper for the members-grid.php to configure
 * the template for suitable usage with wpcarousel object.
 *
 * @author jason.xie@victheme.com
 */

  // Get the members entity
  $members = isset($post->members) ? $post->members : false;

  if (!is_a($members, 'VTCore_Members_Entity')) {
    return;
  }

  VTCore_Wordpress_Utility::loadAsset('jquery-imgliquid');

  // Configure the correct classes
  $classes = array(
    'post-teasers',
    'column',
    'item',
    'multiple',
    'clearfix',
    'post-members-teasers',
    'members-items',
    'text-center',
    'members-item-' . $post->post_delta,
    ($post->post_delta % 2) ? 'odd' : 'even',
  );

  // Load the grid template
  include VTCore_Wordpress_Utility::locateTemplate('members-grid.php');
?>