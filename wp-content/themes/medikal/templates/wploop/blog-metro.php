<?php
/**
 * Template for the post teasers
 *
 * @author jason.xie@victheme.com
 */

  // Build the variable delta column width
  // This depends on the VTCore Loop delta
  // This is configured for 12 posts per
  // page fetching, change the array keys
  // if you need different page per post.
  $variableColumns = array(
    '1' => array(
      'columns' => array(
        'mobile' => 6,
        'tablet' => 6,
        'small' => 6,
        'large' => 6,
      ),
    ),
    '8' => array(
      'columns' => array(
        'mobile' => 12,
        'tablet' => 12,
        'small' => 12,
        'large' => 12,
      ),
    ),
    '11' => array(
      'columns' => array(
        'mobile' => 6,
        'tablet' => 6,
        'small' => 6,
        'large' => 6,
      ),
    ),
    '12' => array(
      'columns' => array(
        'mobile' => 12,
        'tablet' => 12,
        'small' => 12,
        'large' => 12,
      ),
    ),
  );

  // Fetch the correct column size
  if ($post->post_delta <= 12) {
    $index = $post->post_delta;
  }
  else {
    $index = ($post->post_delta - ((ceil($post->post_delta / 12) - 12) * 12));
  }

  $class = 'post-teasers';

  // Dynamically modify the loop column generator object
  if (isset($index) && isset($variableColumns[$index])) {

    $this->getContext('objects.columns')->setContext($variableColumns[$index])->buildColumnAttributes();
    $class .= ' sticky imgLiquidFill imgLiquid';

    if ($index == '12' || $index == '8') {
      $class .= ' full-width';
    }
  }

  // Fallback to the default loop object grid setting.
  else {
    $this->getContext('objects.columns')->setContext(array(
      'columns' => array(
        'mobile' => 3,
        'tablet' => 3,
        'small' => 3,
        'large' => 3,
    )))->buildColumnAttributes();
    $class .= ' normal';

    // Padding if user choose to use background
    $background = VTCore_Zeus_Init::getSchema()->getActiveSchema()->getColorValue('teasers-normal', 'background-color');
    if (!empty($background) && $background != 'transparent') {
      $class .= ' padded';
    }

    // User has nopost thumbnail give more padding
    if (!has_post_thumbnail()) {
      $class .= ' no-thumbnail';
    }
  }

  VTCore_Wordpress_Utility::loadAsset('jquery-imgliquid');


?>
<div class="column item clearfix blog-metro <?php echo esc_attr($this->getContext('objects.columns')->getClass());?>">

  <a class="post-teasers <?php echo esc_attr($class); ?>"
     href="<?php esc_url(the_permalink()); ?>"
     alt="<?php echo esc_attr(get_the_title()); ?>"
     data-imgLiquidFill="true"
     data-imgLiquid-horizontalAlign="center"
     data-imgLiquid-verticalAlign="center">

    <section id="post-<?php the_ID(); ?>" <?php post_class('multiple clearfix'); ?>>

    <?php if ( has_post_thumbnail() && VTCore_Zeus_Init::getFeatures()->get('show.teaser.image')) : ?>

      <?php if (strpos($class, 'normal') !== false) : ?>
      <figure class="post-thumbnail imgLiquidFill imgLiquid"
              data-imgLiquidFill="true"
              data-imgLiquid-horizontalAlign="center"
              data-imgLiquid-verticalAlign="center">
      <?php endif; ?>

      <?php echo get_the_post_thumbnail($post->ID, 'full'); ?>

      <?php if (strpos($class, 'normal') !== false) : ?>
      </figure>
      <?php endif;?>

  	<?php endif; ?>

	  <?php if (VTCore_Zeus_Init::getFeatures()->get('show.teaser.byline')) : ?>
      <time class="post-date">
        <?php echo get_the_date('d.m.Y');?>
      </time>
    <?php endif;?>

  	<div class="post-content">

      <?php if (VTCore_Zeus_Init::getFeatures()->get('show.teaser.title')) : ?>
        <header>
      		<h1 class="post-title">
      		    <?php the_title(); ?>
      		</h1>
      	</header>
      <?php endif;?>

      <?php if (VTCore_Zeus_Init::getFeatures()->get('show.teaser.line')) : ?>
        <div class="post-line"></div>
      <?php endif;?>

      <?php if (VTCore_Zeus_Init::getFeatures()->get('show.teaser.excerpt')) : ?>
      	<div class="post-excerpt">
      		<?php the_excerpt();?>
      	</div>
      <?php endif;?>

  	</div>

    </section>
  </a>
</div>
