<?php
/**
 * Carousel template for displaying member with circle
 * photo and centered member effects.
 *
 * Must be used inside VTCore_Wordpress_Elements_WPCarousel.
 *
 * @author jason.xie@victheme.com
 */

  // Get the members entity
  $members = isset($post->members) ? $post->members : false;

  if (!is_a($members, 'VTCore_Members_Entity')) {
    return;
  }

  VTCore_Wordpress_Utility::loadAsset('jquery-imgliquid');
  VTCore_Wordpress_Utility::loadAsset('members-front');


  // Configure the correct classes
  $classes = array(
    'post-teasers',
    'column',
    'item',
    'multiple',
    'clearfix',
    'post-members-teasers',
    'members-items',
    'members-circle-template',
    'text-center',
    'members-item-' . $post->post_delta,
    ($post->post_delta % 2) ? 'odd' : 'even',
  );

  $position = '';
  if ($members->getPosition()) {
    $position = $members->getPosition();
  }
  elseif ($members->getSpeciality()) {
    $position = $members->getSpeciality();
  }

?>

<div id="members-<?php echo esc_attr($post->ID); ?>" <?php post_class($classes); ?>>

  <?php if (has_post_thumbnail()) : ?>
    <a class="post-thumbnail-link"
       href="<?php esc_url(the_permalink()); ?>"
       alt="<?php echo esc_attr(get_the_title()); ?>">
      <figure class="post-thumbnail imgLiquidFill imgLiquid"
              data-imgLiquidFill="true"
              data-imgLiquid-horizontalAlign="center"
              data-imgLiquid-verticalAlign="center">

        <?php echo get_the_post_thumbnail((int) $post->ID, array('120', '120'), true); ?>
      </figure>
    </a>
  <?php endif; ?>

  <div class="post-inner text-center">
    <h3 class="post-title clearboth clearfix">
      <a href="<?php echo get_permalink(); ?>"
         class="post-title-link"
         alt="<?php esc_attr($post->post_title); ?>">
        <?php the_title(); ?>
      </a>
    </h3>

    <?php if (!empty($position)) : ?>
      <div class="member-position">
        <?php echo wp_kses_post($position); ?>
      </div>
    <?php endif; ?>

  </div>

</div>