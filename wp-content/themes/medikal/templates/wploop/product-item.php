<?php
/**
 * Template to be used by Victheme Woocommerce
 * catalog plugin.
 *
 * We just bridge this with the default
 * content-product.php compatible with woocommerce.
 *
 * @author jason.xie@victheme.com
 */

  include locate_template('woocommerce' . DIRECTORY_SEPARATOR . 'content-product.php', false);
?>
