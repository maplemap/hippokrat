<?php
/**
 * Template for building the services carousel
 * best used with wpcarousel object.
 *
 * This is just a wrapper for the services-flip.php
 * template, with configuration suitable for wpcarousel
 * object
 *
 * @author jason.xie@victheme.com
 */

  // Get the services entity
  $services = isset($post->services) ? $post->services : false;

  if (!is_a($services, 'VTCore_Services_Entity')) {
    return;
  }

  if (is_a($this, 'VTCore_Wordpress_Element_WpCarousel')) {
    $this->addContext('slick.adaptiveHeight', false);
  }
  // Configure the correct classes
  $classes = array(
    'column',
    'item',
    'multiple',
    'clearfix',
    'post-services-teasers',
    'text-center',
    'services-items',
    'services-item-' . $post->post_delta,
    ($post->post_delta % 2) ? 'odd' : 'even',
    'fit-container',
  );

  VTCore_Wordpress_Utility::loadAsset('jquery-texttailor');
  VTCore_Wordpress_Utility::loadAsset('services-front');

?>

  <article id="services-simple-<?php echo esc_attr($post->ID); ?>" <?php post_class($classes);?>>

    <section class="services-icon clearfix clearboth">
      <?php echo $services->getIcon(); ?>
    </section>

    <h3 class="post-title clearboth text-tailor">
      <a class="services-simple-link" href="<?php the_permalink(); ?>" alt="<?php esc_attr($post->post_title); ?>">
        <?php the_title();  ?>
      </a>
    </h3>

    <div class="post-excerpt clearboth clearfix text-tailor">
      <?php echo get_the_excerpt(); ?>
    </div>

  </article>


