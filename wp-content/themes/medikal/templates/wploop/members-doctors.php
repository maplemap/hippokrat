<?php
/**
 * Single archive content template for members post
 * This template must be used inside loop and VTCore
 * loop object.
 *
 * This template utilize the VTCore_Zeus_Config_Options
 * via VTCore_Zeus_Init::getFeatures() with key show.members.archive
 *
 * Also it uses the VTCore_Department_Entity for
 * building the members object markup.
 *
 * @author jason.xie@victheme.com
 */

  // Get the members entity
  $members = isset($post->members) ? $post->members : FALSE;

  if (!is_a($members, 'VTCore_Members_Entity')) {
    return;
  }

  VTCore_Wordpress_Utility::loadAsset('jquery-imgliquid');
  VTCore_Wordpress_Utility::loadAsset('members-front');

  // Configure the correct classes
  $classes = array(
    'post-teasers',
    'column',
    'item',
    'multiple',
    'clearfix',
    'post-members-teasers',
    'members-items',
    'members-doctors-template',
    'text-center',
    'members-item-' . $post->post_delta,
    ($post->post_delta % 2) ? 'odd' : 'even',
  );

?>
<div id="members-<?php echo esc_attr($post->ID); ?>" <?php post_class($classes); ?>>

  <div class="right-side">
    <div class="post-inner text-left">
      <h3 class="post-title clearboth clearfix">
        <a href="<?php echo get_permalink(); ?>"
           class="post-title-link"
           alt="<?php esc_attr($post->post_title); ?>">
          <?php the_title(); ?>
        </a>
      </h3>


      <?php if ($members->getSpeciality()) : ?>
      <div class="members-speciality clearboth clearfix accents">
        <?php echo $members->getSpeciality(); ?>
      </div>
      <?php endif; ?>

      <?php if ($members->getAbbr()) : ?>
      <div class="members-profiles row clearboth clearfix">
        <header
          class="member-profile-header col-xs-6 col-sm-5 col-md-5 col-lg-5">
          <h3 class="member-profile-label">
            <?php echo esc_html__('Education', 'medikal'); ?>
          </h3>
        </header>
        <div class="member-profile-value col-xs-6 col-sm-7 col-md-7 col-lg-7">
          <?php echo $members->getAbbr(); ?>
        </div>
      </div>
      <?php endif; ?>

      <?php if ($members->getExperienceLength()) : ?>
      <div class="members-profiles row clearboth clearfix">
        <header
          class="member-profile-header col-xs-6 col-sm-5 col-md-5 col-lg-5">
          <h3 class="member-profile-label">
            <?php echo esc_html__('Experience', 'medikal'); ?>
          </h3>
        </header>
        <div class="member-profile-value col-xs-6 col-sm-7 col-md-7 col-lg-7">
          <?php echo $members->getExperienceLength(); ?>
        </div>
      </div>
      <?php endif; ?>

      <?php if ($members->getOffice()) : ?>
      <div class="members-profiles row clearboth clearfix">
        <header
          class="member-profile-header col-xs-6 col-sm-5 col-md-5 col-lg-5">
          <h3 class="member-profile-label">
            <?php echo esc_html__('Office', 'medikal'); ?>
          </h3>
        </header>
        <div class="member-profile-value col-xs-6 col-sm-7 col-md-7 col-lg-7">
          <?php echo $members->getOffice(); ?>
        </div>
      </div>
      <?php endif; ?>

      <?php if ($members->getSocial()) : ?>
      <div class="clearboth clearfix">
        <?php echo $members->getSocial(); ?>
      </div>
      <?php endif; ?>

      <div class="text-center visible-xs">
        <a href="<?php the_permalink(); ?>" class="teaser-button">
          <i class="fa fa-share"></i>
        </a>
      </div>
    </div>
  </div>

  <div class="left-side">
    <?php if (has_post_thumbnail()) : ?>
      <a class="post-thumbnail-link"
         href="<?php esc_url(the_permalink()); ?>"
         alt="<?php echo esc_attr(get_the_title()); ?>">
        <figure class="post-thumbnail imgLiquidFill imgLiquid"
                data-imgLiquidFill="crop"
                data-imgLiquid-horizontalAlign="center"
                data-imgLiquid-verticalAlign="top">

          <?php echo get_the_post_thumbnail((int) $post->ID, 'full'); ?>
        </figure>
      </a>
    <?php endif; ?>
  </div>

</div>

