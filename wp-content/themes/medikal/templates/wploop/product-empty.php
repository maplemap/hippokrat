<?php
/**
 * Template for displaying empty content message when
 * no teaser is available to display.
 *
 * For other not found page will use the page404.php
 * template.
 *
 * @author jason.xie@victheme.com
 */
?>
<div class="column product-item col-xs-12 col-sm-12 col-md-12 col-lg-12">
  <div id="shop-empty" class="clearfix product">
    <div class="product-wrapper">
        <h3 class="title text-center">
          <?php echo VTCore_Zeus_Init::getFeatures()->get('options.products.archive.404.title', 'post'); ?>
        </h3>
        <div class="description text-center">
          <?php echo VTCore_Zeus_Init::getFeatures()->get('options.products.archive.404.description', 'post');?>
        </div>
    </div>
  </div>
</div>