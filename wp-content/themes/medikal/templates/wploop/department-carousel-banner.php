<?php
/**
 * Single archive content template for department post
 * This template must be used inside loop and VTCore
 * loop object.
 *
 * This template utilize the VTCore_Zeus_Config_Options
 * via VTCore_Zeus_Init::getFeatures() with key show.department.archive
 *
 * Also it uses the VTCore_Department_Entity for
 * building the department object markup.
 *
 * this is just a wrapper for the department-grid.php with
 * configuration suitable for wpcarousel object
 *
 * @author jason.xie@victheme.com
 */

  // Configure the correct classes
  $classes = array(
    'column',
    'item',
    'multiple',
    'clearfix',
    'text-center',
    'post-department-teasers',
    'department-items',
    'department-item-' . $post->post_delta,
    ($post->post_delta % 2) ? 'odd' : 'even',
  );

  if (isset($this) && is_a($this, 'VTCore_Wordpress_Element_WpCarousel')) {
    $this->addClass('department-banner');
  }

  // Get the department entity
  $department = isset($post->department) ? $post->department : false;

  if (!is_a($department, 'VTCore_Department_Entity')) {
    return;
  }

  VTCore_Wordpress_Utility::loadAsset('jquery-imgliquid');

?>


<article id="department-banner-<?php echo esc_attr($post->ID); ?>" <?php post_class($classes);?>>
  <a class="post-link"
     href="<?php esc_url(the_permalink()); ?>"
     alt="<?php echo esc_attr(get_the_title()); ?>">
    <figure class="post-thumbnail imgLiquidFill imgLiquid"
            data-imgLiquidFill="true"
            data-imgLiquid-horizontalAlign="center"
            data-imgLiquid-verticalAlign="center">

      <?php echo $department->getSingleBanner('full'); ?>
    </figure>
  </a>
</article>


