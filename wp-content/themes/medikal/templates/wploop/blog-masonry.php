<?php
/**
 * Template for displaying post in a masonry mode
 * Must be used inside the loop and / or including
 * the VTCore_Wordpress_Element_WpLoop
 *
 * This template is designed to have a "stamped"
 * element with grid column 4 and placed in the
 * top left corner!.
 *
 * @author jason.xie@victheme.com
 */

  // Process parent element to add settings that must
  // be used for this template!
  if (is_a($this, 'VTCore_Wordpress_Element_WpLoop')) {
    $this->addClass('blog-masonry');
    $this->addData('isotope-options.layoutMode', 'masonry');
    $this->addData('isotope-options.masonry.columnWidth', '.grid-sizer');
    $this->addData('isotope-options.itemSelector', '.item');
  }

  // Build the variable delta column width
  // This depends on the VTCore Loop delta
  // This is configured for 12 posts per
  // page fetching, change the array keys
  // if you need different page per post.
  $variableColumns = array(
    'first' => array(
      'columns' => array(
        'mobile' => 12,
        'tablet' => 12,
        'small' => 8,
        'large' => 8,
      ),
    ),
    '1' => array(
      'columns' => array(
        'mobile' => 12,
        'tablet' => 6,
        'small' => 4,
        'large' => 4,
      ),
    ),
    '2' => array(
      'columns' => array(
        'mobile' => 12,
        'tablet' => 6,
        'small' => 4,
        'large' => 4,
      ),
    ),
    '3' => array(
      'columns' => array(
        'mobile' => 12,
        'tablet' => 6,
        'small' => 4,
        'large' => 4,
      ),
    )
  );

  $multiple = 3;

  // Fetch the correct column size
  if ($post->post_delta <= 4) {
    $index = $post->post_delta;
    if ($post->post_delta === 1) {
      $index = 'first';
    }
    else {
      $index = $post->post_delta - 1;
    }
  }
  else {
    $delta = $post->post_delta - 1;
    $index = ($delta - ((ceil($delta / $multiple - 1) * $multiple)));
  }


  VTCore_Wordpress_Utility::loadAsset('jquery-imgliquid');

  // Configure the correct classes
  $classes = array(
    'column',
    'item',
    'multiple',
    'clearfix',
    'post-blog-teasers',
    'blog-items',
    'blog-item-' . $post->post_delta,
    ($post->post_delta % 2) ? 'odd' : 'even',
    has_post_thumbnail() ? 'with-image' : 'without-image',
  );

  // Dynamically modify the loop column generator object
  if (isset($index) && isset($variableColumns[$index])) {

    $this->getContext('objects.columns')->cleanContext()->setContext($variableColumns[$index])->buildColumnAttributes();
    $classes[] = $this->getContext('objects.columns')->getClass();

    switch ($index) {
      case 'first' :
        $classes[] = 'horizontal-height image-on-left';
        break;

      case '3' :
      case '1' :
        $classes[] = 'vertical-height image-on-top grid-sizer';
        break;

      case '2' :
        $classes[] = 'vertical-height image-on-bottom grid-sizer';
        break;
    }
  }

?>

<article id="post-masonry-<?php echo esc_attr($post->ID); ?>" <?php post_class($classes);?>>

  <section class="entry-wrapper post-image">
    <a class="post-thumbnail-link"
       href="<?php esc_url(the_permalink()); ?>"
       alt="<?php echo esc_attr(get_the_title()); ?>">

      <figure class="post-thumbnail imgLiquidFill imgLiquid"
              data-imgLiquidFill="true"
              data-imgLiquid-horizontalAlign="center"
              data-imgLiquid-verticalAlign="center">

        <?php echo get_the_post_thumbnail($post->ID, 'large'); ?>
      </figure>
    </a>
  </section>

  <section class="entry-wrapper content-section post-content text-center">
    <header>
      <h1 class="post-title text-uppercase">
        <?php
        echo '<a class="post-title" href="' . esc_url(get_the_permalink()) . '" title="'. esc_attr(get_the_title()) . '">' . wp_kses_post(get_the_title()) . '</a>';
        ?>
      </h1>
    </header>

    <div class="post-by">
      <span class="post-by-line">
        <?php echo sprintf(__('post by %s on %s', 'medikal'), '<span class="accents">' . get_the_author_link() . '</span>', '<span class="accents">' . get_the_date('F jS, Y') . '</span>'); ?>
      </span>
    </div>

    <div class="post-line"></div>

    <div class="post-excerpt">
      <?php echo wp_kses_post(wp_trim_words(get_the_excerpt(), 20)); ?>
    </div>

  </section>

</article>

