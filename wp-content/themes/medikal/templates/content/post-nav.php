<?php
/**
 * Template for building the post navigation link
 *
 * @author jason.xie@victheme.com
 */

  $prevPostObject = get_previous_post();
  $nextPostObject = get_next_post();

  // Don't bother to build markup if no link available;
  if (empty($prevPostObject) && empty($nextPostObject)) {
  return;
  }

  VTCore_Wordpress_Utility::loadAsset('jquery-isotope');
VTCore_Wordpress_Utility::loadAsset('jquery-verticalcenter');

  $isotope = array(
    'layoutMode' => 'fitRows',
    'itemSelector' => '.post-nav',
    'fitRows' => array(
      'equalheight' => true,
    ),
    'resizeDelay' => 600,
  );
?>

<div class="post-pagination js-isotope clearfix row" data-isotope-options="<?php echo esc_attr(json_encode($isotope)); ?>">

  <?php if (!empty($prevPostObject)) : ?>

    <?php
      $name = esc_html__('Prev post', 'medikal');
      if (!empty($prevPostObject->post_title)) {
        $name = $prevPostObject->post_title;
      }
    ?>

    <div class="post-nav col-xs-6 col-sm-6 col-md-6 col-lg-6 vertical-center">

      <a class="post-link"
         href="<?php echo esc_url(get_the_permalink($prevPostObject));?>"
         alt="<?php echo esc_attr($prevPostObject->post_title);?>">

        <figure
          class="post-thumbnail imgLiquidFill imgLiquid"
          data-imgLiquidFill="true"
          data-imgLiquid-horizontalAlign="center"
          data-imgLiquid-verticalAlign="center">
        <?php echo get_the_post_thumbnail($prevPostObject->ID, 'thumbnail'); ?>
        </figure>

        <span class="vertical-target text-uppercase"><i class="fa fa-arrow-circle-o-left"></i><?php echo wp_kses_post($name); ?></span>
      </a>
    </div>
  <?php endif; ?>


  <?php if (!empty($nextPostObject)) : ?>

    <?php
    $name = esc_html__('Next post', 'medikal');
    if (!empty($nextPostObject->post_title)) {
      $name = $nextPostObject->post_title;
    }

    ?>
    
    <div class="post-nav col-xs-6 col-sm-6 col-md-6 col-lg-6 text-center vertical-center">

      <a class="post-link"
         href="<?php echo esc_url(get_the_permalink($nextPostObject));?>"
         alt="<?php echo esc_attr($nextPostObject->post_title);?>">

        <figure
          class="post-thumbnail imgLiquidFill imgLiquid"
          data-imgLiquidFill="true"
          data-imgLiquid-horizontalAlign="center"
          data-imgLiquid-verticalAlign="center">
        <?php echo get_the_post_thumbnail($nextPostObject->ID, 'thumbnail'); ?>
        </figure>

        <span class="text-uppercase vertical-target"><?php echo wp_kses_post($name); ?><i class="fa fa-arrow-circle-o-right"></i></span>
      </a>

    </div>
  <?php endif; ?>

</div>