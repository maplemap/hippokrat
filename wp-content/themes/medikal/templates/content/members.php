<?php
/**
 * Template for displaying the single members post content
 *
 * This template must be called from inside the loop.
 *
 *
 * @author jason.xie@victheme.com
 *
 */

  // Get the members entity
  $members = isset($post->members) ? $post->members : false;

  if (!is_a($members, 'VTCore_Members_Entity')) {
    return;
  }

  $show = VTCore_Zeus_Init::getFeatures()->get('show.members.single');
  $show = array_filter($show);

  if (!has_post_thumbnail() && isset($show['image'])) {
    unset($show['image']);
  }

  $class = 'col-xs-12 col-sm-12 col-md-12 col-lg-12';
  if (isset($show['image'])) {
    $class = 'col-xs-7 col-sm-7 col-md-7 col-lg-7';
  }

?>
  <article id="post-<?php the_ID(); ?>" <?php post_class('post-single post-members members single clearfix'); ?>>

    <?php if (!empty($show)) : ?>

      <section class="members-header-wrapper clearboth clearfix post-section">

        <?php if (isset($show['image'])) : ?>
          <div class="post-section col-xs-5 col-sm-5 col-md-5 col-lg-5 clearfix">
            <figure class="post-thumbnail-full">
              <?php echo get_the_post_thumbnail($post->ID, 'full'); ?>
            </figure>
          </div>
        <?php endif; ?>

        <div class="post-section clearfix <?php echo esc_attr($class); ?>">

          <?php if (isset($show['name'])) : ?>
            <header>
              <h1 class="post-header-title text-uppercase">
                <?php echo wp_kses_post(get_the_title()); ?>
              </h1>
            </header>
          <?php endif; ?>

          <?php if ($show['speciality'] && $members->getSpeciality()) : ?>
            <div class="members-speciality accents clearboth clearfix">
              <?php echo $members->getSpeciality(); ?>
            </div>
          <?php endif; ?>

          <?php if (isset($show['content'])) : ?>
            <div class="post-content">
              <?php the_content(); ?>
            </div>
          <?php endif; ?>

          <?php if ($show['position'] && $members->getPosition()) : ?>
            <div class="members-profiles row clearboth clearfix">
              <header class="member-profile-header col-xs-4 col-sm-3 col-md-3 col-lg-3">
                <h3 class="member-profile-label">
                  <?php echo esc_html__('Position', 'medikal'); ?>
                </h3>
              </header>
              <div class="member-profile-value col-xs-8 col-sm-9 col-md-9 col-lg-9">
                <?php echo $members->getPosition(); ?>
              </div>
            </div>
          <?php endif; ?>

          <?php if ($show['department'] && $members->getDepartment()) : ?>
            <div class="members-profiles row clearboth clearfix">
              <header class="member-profile-header col-xs-4 col-sm-3 col-md-3 col-lg-3">
                <h3 class="member-profile-label">
                  <?php echo esc_html__('Department', 'medikal'); ?>
                </h3>
              </header>
              <div class="member-profile-value col-xs-8 col-sm-9 col-md-9 col-lg-9">
                <?php echo $members->getDepartment(); ?>
              </div>
            </div>
          <?php endif; ?>

          <?php if ($show['education'] && $members->getEducation()) : ?>
            <div class="members-profiles row clearboth clearfix">
              <header class="member-profile-header col-xs-4 col-sm-3 col-md-3 col-lg-3">
                <h3 class="member-profile-label">
                  <?php echo esc_html__('Education', 'medikal'); ?>
                </h3>
              </header>
              <div class="member-profile-value col-xs-8 col-sm-9 col-md-9 col-lg-9">
                <?php echo $members->getEducation(); ?>
              </div>
            </div>
          <?php endif; ?>


          <?php if ($show['experience'] && $members->getExperience()) : ?>
            <div class="members-profiles row clearboth clearfix">
              <header class="member-profile-header col-xs-4 col-sm-3 col-md-3 col-lg-3">
                <h3 class="member-profile-label">
                  <?php echo esc_html__('Experience', 'medikal'); ?>
                </h3>
              </header>
              <div class="member-profile-value col-xs-8 col-sm-9 col-md-9 col-lg-9">
                <?php echo $members->getExperience(); ?>
              </div>
            </div>
          <?php endif; ?>

          <?php if ($show['office'] && $members->getOffice()) : ?>
            <div class="members-profiles row clearboth clearfix">
              <header class="member-profile-header col-xs-4 col-sm-3 col-md-3 col-lg-3">
                <h3 class="member-profile-label">
                  <?php echo esc_html__('Office', 'medikal'); ?>
                </h3>
              </header>
              <div class="member-profile-value col-xs-8 col-sm-9 col-md-9 col-lg-9">
                <?php echo $members->getOffice(); ?>
              </div>
            </div>
          <?php endif; ?>

          <?php if ($show['workdays'] && $members->getWorkDays()) : ?>
            <div class="members-profiles row clearboth clearfix">
              <header class="member-profile-header col-xs-4 col-sm-3 col-md-3 col-lg-3">
                <h3 class="member-profile-label">
                  <?php echo esc_html__('Work Days', 'medikal'); ?>
                </h3>
              </header>
              <div class="member-profile-value col-xs-8 col-sm-9 col-md-9 col-lg-9">
                <?php echo $members->getWorkDays(); ?>
              </div>
            </div>
          <?php endif; ?>

          <?php if ($show['email'] && $members->getEmail()) : ?>
            <div class="members-profiles row clearboth clearfix">
              <header class="member-profile-header col-xs-4 col-sm-3 col-md-3 col-lg-3">
                <h3 class="member-profile-label">
                  <?php echo esc_html__('Email', 'medikal'); ?>
                </h3>
              </header>
              <div class="member-profile-value col-xs-8 col-sm-9 col-md-9 col-lg-9">
                <?php echo $members->getEmail(); ?>
              </div>
            </div>
          <?php endif; ?>

          <?php if ($show['services'] && $members->getServices()) : ?>
            <div class="members-profiles row clearboth clearfix">
              <header class="member-profile-header col-xs-4 col-sm-3 col-md-3 col-lg-3">
                <h3 class="member-profile-label">
                  <?php echo esc_html__('Services', 'medikal'); ?>
                </h3>
              </header>
              <div class="member-profile-value col-xs-8 col-sm-9 col-md-9 col-lg-9">
                <?php echo $members->getServices(); ?>
              </div>
            </div>
          <?php endif; ?>

          <?php if ($show['training'] && $members->getTraining()) : ?>
            <div class="members-profiles row clearboth clearfix">
              <header class="member-profile-header col-xs-4 col-sm-3 col-md-3 col-lg-3">
                <h3 class="member-profile-label">
                  <?php echo esc_html__('Training', 'medikal'); ?>
                </h3>
              </header>
              <div class="member-profile-value col-xs-8 col-sm-9 col-md-9 col-lg-9">
                <?php echo $members->getTraining(); ?>
              </div>
            </div>
          <?php endif; ?>


          <?php if ($show['social'] && $members->getSocial()) : ?>
            <div class="members-profiles row clearboth clearfix">
              <header class="member-profile-header col-xs-4 col-sm-3 col-md-3 col-lg-3">
                <h3 class="member-profile-label">
                  <?php echo esc_html__('Social info', 'medikal'); ?>
                </h3>
              </header>
              <div class="member-profile-value col-xs-8 col-sm-9 col-md-9 col-lg-9">
                <?php echo $members->getSocial(); ?>
              </div>
            </div>
          <?php endif; ?>


        </div>

      </section>

      <?php if (isset($show['education_table'])) : ?>
        <section class="post-section members-education clearfix clearboth">
          <header>
            <h1 class="post-header-title text-uppercase">
              <?php echo esc_html__('Formal Educations', 'medikal'); ?>
            </h1>
          </header>
          <?php get_template_part('templates/table/education'); ?>
        </section>
      <?php endif; ?>

      <?php if (isset($show['experience_table'])) : ?>
        <section class="post-section members-experience clearfix clearboth">
          <header>
            <h1 class="post-header-title text-uppercase">
              <?php echo esc_html__('Work Experiences', 'medikal'); ?>
            </h1>
          </header>
          <?php get_template_part('templates/table/work'); ?>
        </section>
      <?php endif; ?>

      <?php if (isset($show['service_table'])) : ?>
        <section class="post-section members-experience clearfix clearboth">
          <header>
            <h1 class="post-header-title text-uppercase">
              <?php echo esc_html__('Practice Schedules', 'medikal'); ?>
            </h1>
          </header>
          <?php get_template_part('templates/table/practice'); ?>
        </section>
      <?php endif; ?>

    <?php endif; ?>
  </article>
