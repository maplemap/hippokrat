<?php
/**
 * Template for displaying empty content message when
 * no teaser is available to display.
 *
 * For other not found page will use the page404.php
 * template.
 *
 * @author jason.xie@victheme.com
 */
?>
<article id="post-not-found">
  <header><h2 class="post-title"><?php echo esc_html__('Not Found', 'medikal');?></h2></header>
  <section class="post_content">
  	<p><?php echo esc_html__('Sorry, but the requested resource was not found on this site.', 'medikal'); ?></p>
  </section>
  <footer>
  </footer>
</article>