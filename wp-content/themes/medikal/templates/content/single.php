<?php
/**
 * Template for displaying the single post content
 *
 * This template must be called from inside the loop.
 *
 * @author jason.xie@victheme.com
 *
 */

  VTCore_Wordpress_Utility::loadAsset('jquery-imgliquid');
  $show = VTCore_Zeus_Init::getFeatures()->get('show.full');
  $show = array_filter($show);

?>
  <article id="post-<?php the_ID(); ?>" <?php post_class('post-single single clearfix'); ?>>

    <?php if (!empty($show)) : ?>

      <?php if (has_post_thumbnail() && isset($show['image'])) : ?>
        <figure class="post-thumbnail imgLiquidFill imgLiquid"
                data-imgLiquidFill="true"
                data-imgLiquid-horizontalAlign="center"
                data-imgLiquid-verticalAlign="center">

          <?php echo wp_kses_post(get_the_post_thumbnail($post->ID, 'full')); ?>

        </figure>
      <?php endif; ?>


      <?php if (isset($show['title'])) : ?>
        <header>
          <h1 class="post-header-title text-uppercase">
          <?php echo wp_kses_post(get_the_title()); ?>
          </h1>

        </header>
      <?php endif; ?>

      <?php if ($show['byline']) : ?>
        <div class="post-by">
        <span class="post-by-line">
          <?php echo sprintf(__('post by %s on %s', 'medikal'), '<span class="accents">' . get_the_author_link() . '</span>', '<span class="accents">' . get_the_date('F jS, Y') . '</span>'); ?>
        </span>
        </div>
      <?php endif; ?>


      <?php if (isset($show['content'])) : ?>
        <div class="post-content">
          <?php

          // Retrieves content
          the_content();

          // Retrieves Pager
          $pager = VTCore_Zeus_Utility::getPostPager();
          if (!empty($pager)) {
            echo '<div class="page-links text-center">' . $pager . '</div>';
          }
          ?>
        </div>
      <?php endif; ?>

      <?php if (isset($show['tag']) || isset($show['social'])) : ?>

        <div class="post-social-tags well">
          <div class="row">
          <?php if (isset($show['tag'])) : ?>
          <div class="post-tag-block col-xs-12 col-sm-6 col-md-6">
            <?php the_tags('<h3 class="pull-inline">' . esc_html__('TAGS: ' . '</h3>', 'medikal'), ', ', ''); ?>
          </div>
          <?php endif; ?>

          <?php if (isset($show['share'])) : ?>
          <div class="post-social-block col-xs-12 col-sm-6 col-md-6">
            <?php $object = new VTCore_Wordpress_Element_WpSocialShare(); ?>
            <h3 class="pull-inline"><?php echo esc_html__('SHARE:', 'medikal'); ?></h3><?php $object->render(); ?>
          </div>
          <?php endif; ?>
          </div>
        </div>

      <?php endif; ?>

      <?php if (isset($show['author'])) : ?>
        <?php get_template_part('templates/content/author'); ?>
      <?php endif; ?>


    <?php endif; ?>
  </article>

<?php

  // Build the post navigation link
  if (isset($show['post_nav'])) {
    get_template_part('templates/content/post-nav');
  }

  // Build the comments
  if (isset($show['comments']) || isset($show['comment_form'])) {
    comments_template();
  }
?>