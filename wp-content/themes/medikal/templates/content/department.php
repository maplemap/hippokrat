<?php
/**
 * Template for displaying the single department post content
 *
 * This template must be called from inside the loop.
 *
 * @author jason.xie@victheme.com
 *
 */

  // Get the department entity
  $department = isset($post->department) ? $post->department : false;

  if (!is_a($department, 'VTCore_Department_Entity')) {
    return;
  }

  $show = VTCore_Zeus_Init::getFeatures()->get('show.department.single');
  $show = array_filter($show);


?>
  <article id="post-<?php the_ID(); ?>" <?php post_class('post-single post-department single clearfix'); ?>>

    <?php if (!empty($show)) : ?>

      <section class="department-header-wrapper clearboth clearfix post-section">
        <?php if (isset($show['image']) && $department->getSingleBanner('full')) : ?>
          <figure class="post-thumbnail imgLiquidFill imgLiquid"
                  data-imgLiquidFill="true"
                  data-imgLiquid-horizontalAlign="center"
                  data-imgLiquid-verticalAlign="center">

            <?php echo $department->getSingleBanner('large'); ?>

          </figure>
        <?php endif; ?>

        <?php if ($show['slogan']) : ?>
          <div class="department-sloganbox">
            <?php if ($department->getSingleData('headers.slogan')) : ?>
              <div class="department-slogan">
                <?php echo $department->getSingleData('headers.slogan'); ?>
              </div>
            <?php endif; ?>

            <?php if ($department->getSingleData('headers.author')) : ?>
              <div class="department-author">
                <?php echo $department->getSingleData('headers.author'); ?>
              </div>
            <?php endif; ?>

            <?php if ($department->getSingleData('headers.position')) : ?>
              <div class="department-position">
                <?php echo $department->getSingleData('headers.position'); ?>
              </div>
            <?php endif; ?>
          </div>
        <?php endif; ?>
      </section>

      <section class="department-content clearboth clearfix post-section">
      <?php if (isset($show['title'])) : ?>
        <header>
          <h1 class="post-header-title text-uppercase">
            <?php echo wp_kses_post(get_the_title()); ?>
          </h1>
        </header>
      <?php endif; ?>

      <?php if (isset($show['content'])) : ?>
        <section class="post-content">
          <?php the_content(); ?>
        </section>
      <?php endif; ?>
      </section>


      <?php if (isset($show['promotional'])) : ?>
        <section class="department-promotional-box clearboth clearfix post-section">

          <?php if ($department->getSingleData('promotional.title')) : ?>
            <header>
              <h1 class="post-header-title text-uppercase">
                <?php echo $department->getSingleData('promotional.title'); ?>
              </h1>
            </header>
          <?php endif; ?>

          <?php if ($department->getSingleData('promotional.description')) : ?>
            <div class="post-content">
              <?php echo $department->getSingleData('promotional.description'); ?>
            </div>
          <?php endif; ?>

          <?php if ($department->getSingleVideo()) : ?>
            <div class="post-video col-xs-12 col-sm-8 col-md-8 col-lg-8 col-sm-offset-2 col-md-offset-2 col-lg-offset-2">
              <?php echo $department->getSingleVideo(); ?>
            </div>
          <?php endif; ?>

        </section>
      <?php endif; ?>

      <?php if (isset($show['services']) && $department->get('services')) : ?>

        <section class="department-services-box post-section clearboth clearfix">

          <?php if ($department->getSingleData('services.title')) : ?>
            <header>
              <h1 class="post-header-title text-uppercase">
                <?php echo $department->getSingleData('services.title'); ?>
              </h1>
            </header>
          <?php endif; ?>

          <?php if ($department->getSingleData('services.description')) : ?>
            <div class="post-content">
              <?php echo $department->getSingleData('services.description'); ?>
            </div>
          <?php endif; ?>

          <?php

          $object = new VTCore_Wordpress_Element_WpCarousel(array(
            'slick' => array(
              // Flip dont support cloned item
              'infinite' => false,
            ),
            'queryMain' => false,
            'queryArgs' => array(
              'post_type' => 'services',
              'post__in' => $department->get('services'),
            ),
            'template' => array(
              'items' => 'services-carousel.php',
              'empty' => 'services-empty.php',
            ),
            'content_element' => array(
              'attributes' => array(
                'class' => array(
                  'services-carousel-item',
                ),
              ),
            ),
            'grids' => array(
              'columns' => array(
                'mobile' => 4,
                'tablet' => 4,
                'small' => 4,
                'large' => 4,
              ),
            ),
          ));

          $object->render();

          ?>
        </section>

      <?php endif; ?>


    <?php endif; ?>
  </article>
