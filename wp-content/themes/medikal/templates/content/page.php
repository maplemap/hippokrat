<?php
/**
 * Template for displaying the single page content
 *
 * This template must be called from inside the loop.
 *
 * @author jason.xie@victheme.com
 *
 */


VTCore_Wordpress_Utility::loadAsset('jquery-imgliquid');

$show = VTCore_Zeus_Init::getFeatures()->get('show.page');

?>
  <article
    id="post-<?php the_ID(); ?>" <?php post_class('single without-info clearfix row'); ?>>

    <?php if (!empty($show)) : ?>
      <section
        class="post-content col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <?php if (has_post_thumbnail() && isset($show['image'])) : ?>


          <figure class="post-thumbnail imgLiquidFill imgLiquid"
                  data-imgLiquidFill="true"
                  data-imgLiquid-horizontalAlign="center"
                  data-imgLiquid-verticalAlign="center">

            <?php echo get_the_post_thumbnail($post->ID, 'large'); ?>
          </figure>


        <?php endif; ?>


        <?php if (isset($show['title'])) : ?>
          <header>
            <h1 class="post-header-title">
              <span class="post-title">
                <?php echo wp_kses_post(get_the_title()); ?>
              </span>
            </h1>
          </header>
        <?php endif; ?>

        <?php if (isset($show['byline'])) : ?>
          <div class="post-by">
            <span class="post-by-author rounded">
              <?php echo wp_kses_post(get_avatar($post->post_author, '54')); ?>
            </span>
            <span class="post-by-line">
              <?php echo sprintf(__('by %s on %s', 'medikal'), get_the_author_link(), get_the_date('d.m.Y - H:iA')); ?>
            </span>
          </div>
        <?php endif; ?>

        <?php if (isset($show['content'])) : ?>
          <div class="post-content">
            <?php

            // Retrieves content
            the_content();

            // Retrieves Pager
            $pager = VTCore_Zeus_Utility::getPostPager();
            if (!empty($pager)) {
              echo '<div class="page-links text-center">' . $pager . '</div>';
            }
            ?>
          </div>
        <?php endif; ?>

        <?php if (isset($show['share'])) : ?>
          <div class="post-social post-social-tags well">
          <h3 class="post-social-text pull-inline">
            <?php echo esc_html__('Share on :', 'medikal'); ?>
          </h3>
          <?php
            $object = new VTCore_Wordpress_Element_WpSocialShare();
            $object->render();
          ?>
          </div>
        <?php endif; ?>

        <?php
        if (isset($show['post_nav'])) {
          get_template_part('templates/content/post-nav');
        }
        ?>
      </section>
    <?php endif; ?>
  </article>

<?php
// Build the comments
if (isset($show['comments']) || isset($show['comment_form'])) {
  comments_template();
}
?>