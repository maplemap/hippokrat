<?php
/**
 * Template for displaying post services formatted as a timetable
 *
 * @author jason.xie@victheme.com
 *
 */

  // Check plugin dependencies first.
  if (!defined('VTCORE_DEPARTMENT_LOADED')
      || !defined('VTCORE_SERVICES_LOADED')
      || !defined('VTCORE_MEMBERS_LOADED')) {
    return;
  }

  // Load Asset
  VTCore_Wordpress_Utility::loadAsset('jquery-move');
  VTCore_Wordpress_Utility::loadAsset('timetable-front');

  $timetable = new VTCore_Zeus_TimeTable(VTCore_Zeus_Init::getFeatures()->get('options.timetable'));

  // Build department background styling
  foreach ($timetable->get('department') as $object) {
    echo $object->getTeaserStyle('.timetable-schedule-entry-' . $object->get('post.ID'));
  }

?>

  <article id="post-<?php the_ID(); ?>" <?php post_class('js-timetable post-single post-services-timetable single clearfix'); ?>>


    <?php if (VTCore_Zeus_Init::getFeatures()->get('show.timetable.weekdate')) : ?>
    <header class="timetable-weekdate-section timetable-row text-center">
      <h1 class="timetable-weekdate">
        <?php echo date('F jS', $timetable->get('calendar.start')) . ' - ' .  date('F jS', $timetable->get('calendar.end')) . ', '  . date('Y', $timetable->get('calendar.end')); ?>
      </h1>
    </header>
    <?php endif; ?>

    <?php if (VTCore_Zeus_Init::getFeatures()->get('show.timetable.filter')) : ?>
    <!-- Timetable sorter buttons -->
    <section class="timetable-sorter-section timetable-row">

      <ul class="timetable-sorter col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <li class="timetable-sorter-item">
          <a class="js-timesorter timetable-department-link"
             title="<?php echo esc_html__('link to display available department', 'medikal'); ?>"
             href="<?php echo esc_attr(get_post_type_archive_link('department')); ?>"
             data-sort="*">
            <?php echo esc_html__('All', 'medikal'); ?>
          </a>
        </li>

        <?php foreach ($timetable->get('department') as $object) : ?>

          <li class="timetable-sorter-item">
            <a class="js-timesorter timetable-department-link"
               title="<?php echo esc_attr($object->get('post.post_title')); ?>"
               href="<?php echo esc_attr(get_permalink((int) $object->get('post.ID'))); ?>"
               data-sort="<?php echo esc_attr('department-sort-' . $object->get('post.ID')); ?>">
              <?php echo wp_kses_post($object->get('post.post_title')); ?>
            </a>
          </li>

        <?php endforeach; ?>

      </ul>
    </section>
    <?php endif; ?>


    <!-- Timetable Header -->
    <section class="timetable-header clearfix clearboth timetable-row row"
             data-swipe-type="header">

      <div class="timetable-date col-xs-12 col-sm-2 col-md-2 col-lg-2">
        <!-- empty div as the placeholder -->
      </div>

      <div class="timetable-entries col-xs-12 col-sm-10 col-md-10 col-lg-10 clearfix">
        <div class="js-isotope timetable-row row"
             data-isotope-options="<?php echo esc_attr(json_encode(array(
             'itemSelector' => '.timetable-header-wrapper',
             'layoutMode' => 'fitRows',
             'fitRows' => array(
               'equalheight' => TRUE,
               'gutter' => array(
                 'width' => 0,
                 'height' => 0,
               ),
             ),
             'resizeDelay' => 900,
           ))); ?>">

        <?php foreach ($timetable->getShifts() as $object) : ?>

        <div class="timetable-header-wrapper clearfix text-center col-xs-4 col-sm-4 col-md-4 col-lg-4"
             data-swipe-linked="schedule-<?php echo esc_attr($object->get('id')); ?>"
             data-swipe-type="header-items">

          <div class="timetable-header-entry timetable-entry">
            <?php if ($object->get('title')) : ?>
              <h2 class="post-title text-uppercase accent">
                <?php echo wp_kses_post($object->get('title')); ?>
              </h2>
            <?php endif; ?>

            <?php if ($object->get('start') && $object->get('end')) : ?>
              <time class="timetable-shift-time">
                (<?php echo date('H:i', strtotime($object->get('start'))) . ' - ' .  date('H:i', strtotime($object->get('end')));?>)
              </time>
            <?php endif; ?>
          </div>
        </div>

        <?php endforeach; ?>
        </div>
      </div>
    </section>




    <?php foreach ($timetable->getDays() as $dayObject) : ?>

    <!-- Timetable row per days -->
    <section class="js-isotope timetable-content timetable-row row"
             data-swipe-type="sections"
             data-vertical-force="true"
             data-isotope-options="<?php echo esc_attr(json_encode(array(
               'itemSelector' => '.timetable-content-wrapper',
               'layoutMode' => 'fitRows',
               'fitRows' => array(
                 'equalheight' => TRUE,
                 'gutter' => array(
                   'width' => 0,
                   'height' => 0,
                 ),
               ),
               'resizeDelay' => 700,
             ))); ?>">

      <div class="timetable-content-wrapper vertical-center timetable-date col-xs-12 col-sm-2 col-md-2 col-lg-2"
           data-swipe-type="toggle-trigger">

        <div class="timetable-date-entry text-center vertical-target">

          <h3 class="post-title">
            <?php echo wp_kses_post($dayObject->get('label')); ?>
          </h3>

          <time class="timetable-shift-day">
            (<?php echo wp_kses_post($dayObject->get('formatted')); ?>)
          </time>

        </div>

      </div>

      <div class="timetable-content-wrapper timetable-entries col-xs-12 col-sm-10 col-md-10 col-lg-10"
           data-swipe-type="toggle-content">
        <div class="js-isotope timetable-columns timetable-row row"
             data-swipe-type="columns"
             data-isotope-options="<?php echo esc_attr(json_encode(array(
               'itemSelector' => '.timetable-content-column',
               'layoutMode' => 'fitRows',
               'fitRows' => array(
                 'equalheight' => TRUE,
                 'gutter' => array(
                   'width' => 0,
                   'height' => 0,
                 ),
               ),
               'resizeDelay' => 600,
             ))); ?>">

          <?php foreach ($timetable->getShifts() as $shiftObject) : ?>

          <div class="timetable-content-column text-center col-xs-4 col-sm-4 col-md-4 col-lg-4"
               data-swipe-linked="schedule-<?php echo esc_attr($shiftObject->get('id')); ?>"
               data-swipe-type="column-items">

            <div class="timetable-content-entry timetable-entry"
                 data-swipe-type="filter-target"
                 data-isotope-options="<?php echo esc_attr(json_encode(array(
                   'itemSelector' => '.timetable-schedule-entry',
                   'layoutMode' => 'fitRows',
                   'resizeDelay' => 0,
                 ))); ?>">

            <?php foreach ($timetable->getSchedule($dayObject, $shiftObject) as $object) : ?>

              <div class="timetable-schedule-entry timetable-schedule-entry-<?php echo esc_attr($object->get('department'));?>"
                   data-sort="department-sort-<?php echo esc_attr($object->get('department'));?>">

                <?php if ($object->get('service') && VTCore_Zeus_Init::getFeatures()->get('show.timetable.title')) : ?>
                  <h4 class="post-title text-uppercase">
                    <a class="post-title-link"
                       href="<?php echo esc_attr(get_permalink($object->get('service')));?>">
                      <?php echo wp_kses_post(get_the_title($object->get('service'))); ?>
                    </a>
                  </h4>
                <?php endif; ?>

                <?php if ($object->get('department') && VTCore_Zeus_Init::getFeatures()->get('show.timetable.department')) : ?>
                  <a class="post-title-link"
                     href="<?php echo esc_attr(get_permalink($object->get('department')));?>">
                    <?php echo wp_kses_post(get_the_title($object->get('department'))); ?>
                  </a>
                <?php endif; ?>


                <?php if ($object->get('users') && VTCore_Zeus_Init::getFeatures()->get('show.timetable.members')) : ?>
                <div class="timetable-members">
                  <?php
                    $users = array();
                    foreach ($object->get('users') as $userObject) {
                      $users[] = $userObject->get('markup')->__toString();
                    }

                    echo implode(', ', $users);
                  ?>
                </div>
                <?php endif;?>

                <?php if ($object->get('location') && VTCore_Zeus_Init::getFeatures()->get('show.timetable.location')) : ?>
                  <div class="services-location">
                    <?php echo wp_kses_post($object->get('location')) ;?>
                  </div>
                <?php endif; ?>

              </div>
            <?php endforeach; ?>
            </div>
          </div>

        <?php endforeach; ?>
        </div>
      </div>
    </section>

    <?php endforeach; ?>


  </article>

