<?php
/**
 * Template for displaying the single services post content
 *
 * This template must be called from inside the loop.
 *
 * @author jason.xie@victheme.com
 *
 */

  // Get the services entity
  $services = isset($post->services) ? $post->services : false;

  if (!is_a($services, 'VTCore_Services_Entity')) {
    return;
  }

  $show = VTCore_Zeus_Init::getFeatures()->get('show.services.single');
  $show = array_filter($show);


?>
  <article id="post-<?php the_ID(); ?>" <?php post_class('post-single post-services single clearfix'); ?>>

    <?php if (!empty($show)) : ?>

      <section class="services-header-wrapper clearboth clearfix post-section">

        <?php if (isset($show['image']) && $services->getImage('full')) : ?>
          <div class="post-section pull-right col-xs-5 col-sm-5 col-md-5 col-lg-5 clearfix">
            <figure class="post-thumbnail-full">
              <?php echo $services->getImage('full'); ?>
            </figure>
          </div>
        <?php endif; ?>

        <?php if (isset($show['title']) || isset($show['content']) || isset($show['department']) || isset($show['price'])) : ?>

          <div class="post-section col-xs-7 col-sm-7 col-md-7 col-lg-7 clearfix">

            <?php if (isset($show['title'])) : ?>
              <header>
                <h1 class="post-header-title text-uppercase">
                  <?php echo wp_kses_post(get_the_title()); ?>
                </h1>
              </header>
            <?php endif; ?>


            <?php if (isset($show['department']) && $services->get('data.department')) : ?>
              <div class="services-department accents clearfix clearboth">
                <a href="<?php get_permalink((int) $services->get('data.department')); ?>">
                  <?php echo get_the_title((int) $services->get('data.department'));  ?>
                </a>
              </div>
            <?php endif; ?>

            <?php if (isset($show['content'])) : ?>
              <div class="post-content">
                <?php the_content(); ?>
              </div>
            <?php endif; ?>

            <?php if (isset($show['price'])) : ?>
              <div class="services-price accent clearfix clearboth">
                <?php echo $services->getPrice(__('Price : ', 'medikal'));  ?>
              </div>
            <?php endif; ?>



          </div>

        <?php endif;?>

      </section>

      <?php if (isset($show['schedule'])) : ?>
        <section class="services-schedule clearboth clearfix">
          <header class="clearfix">
            <h1 class="post-header-title text-uppercase col-xs-12">
              <?php echo esc_html__('Service Schedule', 'medikal'); ?>
            </h1>
          </header>
          <?php get_template_part('templates/table/services'); ?>
        </section>
      <?php endif; ?>

    <?php endif; ?>
  </article>
