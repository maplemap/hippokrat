<?php
/**
 * Template for building the author information box.
 */
  global $post;
  $avatar = get_avatar($post->post_author, '172');
  $content = get_the_author_meta('description');

  $name = get_the_author_meta('first_name') . ' ' . get_the_author_meta('last_name');
  if (empty($name)) {
    $name = get_the_author_meta('user_nicename');
  }

  if (empty($name)) {
    $name = get_the_author_meta('nickname');
  }

  if (empty($name)) {
    $name = get_the_author_meta('user_login');
  }

  if (empty($content)) {
    $content = esc_html__('No biography entered for this author yet.', 'medikal');
  }

?>
<div class="post-author clearfix">

  <?php if (!empty($avatar)) : ?>
    <div class="post-author-avatar pull-left rounded">
      <?php echo wp_kses_post($avatar); ?>
    </div>
  <?php endif; ?>

  <div class="post-author-body">
    <h3 class="header">
      <?php echo wp_kses_post($name); ?>
    </h3>
    <div class="content">
    <?php echo wp_kses_post($content); ?>
    </div>
  </div>
</div>