<?php
/**
 * Template for formatting a single form element
 *
 * Note that the li element has no closing element. This is
 * done this way accordingly to WordPress comment standard.
 *
 * @author jason.xie@victheme.com
 */

  // CSS Class logic
  $class = "without-child";
  if (!empty($args['has_children'])) {
    $class = "with-child";
  }

?>
<li id="comment-<?php comment_ID() ?>" <?php comment_class($class) ?>>

  <div class="comment-author-image media-object pull-left">
    <?php
      echo wp_kses_post(get_avatar($comment, $args['avatar_size']));
    ?>
  </div>

  <div class="comment-body media-body">

    <?php if ($comment->comment_approved == FALSE) : ?>
      <div class="alert alert-info"><?php echo esc_html__('Your comment is awaiting moderation.', 'medikal') ?></div>
    <?php endif; ?>

    <div class="comment-top clearfix row">
      <div class="col-xs-6 col-sm-8 col-md-8 col-lg-8">
        <div class="comment-author">
          <?php print get_comment_author_link(); ?>
        </div>
        <div class="comment-date">
          <?php comment_date('F jS, Y'); ?>
        </div>
      </div>
      <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
        <div class="comment-reply reply-link">
          <?php comment_reply_link(array_merge($args, array(
            'add_below' => 'comment',
            'depth' => $depth,
            'max_depth' => $args['max_depth']
          ))) ?>
        </div>
      </div>
    </div>

    <?php comment_text() ?>

  </div>

