<?php
/**
 * Template for displaying customized members work experience table
 * this must be called inside loop
 *
 * @author jason.xie@victheme.com
 *
 */

  global $post;

  // Get the members entity
  $members = isset($post->members) ? $post->members : false;

  if (!is_a($members, 'VTCore_Members_Entity')) {
    return;
  }

  $isotope = json_encode(array(
    'itemSelector' => '.item',
    'layoutMode' => 'fitRows',
    'fitRows' => array(
      'equalheight' => TRUE,
      'gutter' => array(
        'width' => 0,
        'height' => 0,
      ),
    ),
    'resizeDelay' => 300,
  ));

?>


<div class="members-table-listing work-table container-fluid">

  <?php foreach ($members->get('data.experiences') as $delta => $member) : ?>

    <?php $object = new VTCore_Wordpress_Objects_Array($member); ?>

    <div class="row members-table-row js-isotope" data-isotope-options="<?php echo esc_attr($isotope); ?>">

      <?php
      $column = 'col-xs-12 col-sm-12 col-md-12 col-lg-12';
      if ($object->get('title')) {
        $column = 'col-xs-12 col-sm-7 col-md-8 col-lg-8';
      }

      $time = '';
      if ($object->get('date.start')) {
        $time .= '<span class="start">' . esc_html($object->get('date.start')) . '</span>';
      }

      if ($object->get('date.end')) {
        $time .= '<span class="separator"> ' . esc_html__('until', 'medikal') . ' </span>';
        $time .= '<span class="end">' . esc_html($object->get('date.end')) . '</span>';
      }
      ?>

      <?php if ($object->get('title')) : ?>
        <div class="members-column-title item col-xs-12 col-sm-5 col-md-4 col-lg-4">
          <?php echo wp_kses_post($object->get('title')); ?>
        </div>
      <?php endif; ?>

      <div class="members-column-date-description item <?php echo esc_attr($column); ?>">
        <div class="row">
          <?php if (isset($time) && !empty($time)) : ?>
            <div class="members-time">
              <?php echo wp_kses_post($time); ?>
            </div>
          <?php endif; ?>
        </div>
      </div>

      <?php if ($object->get('description')) : ?>
        <div class="members-column-date-description item members-description col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <?php echo wp_kses_post($object->get('description')); ?>
        </div>
      <?php endif; ?>
    </div>
  <?php endforeach; ?>
</div>