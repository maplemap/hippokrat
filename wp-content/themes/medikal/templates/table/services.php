<?php
/**
 * Template for displaying customized service table
 * this must be called inside loop
 *
 * @author jason.xie@victheme.com
 *
 */

  global $post;

  // Get the services entity
  $services = isset($post->services) ? $post->services : false;

  if (!is_a($services, 'VTCore_Services_Entity')) {
    return;
  }

  $isotope = json_encode(array(
    'itemSelector' => '.item',
    'layoutMode' => 'fitRows',
    'fitRows' => array(
      'equalheight' => TRUE,
      'gutter' => array(
        'width' => 0,
        'height' => 0,
      ),
    ),
    'resizeDelay' => 300,
  ));
?>


<div class="service-table-listing container-fluid">

  <?php foreach ($services->get('data.schedule') as $delta => $schedule) : ?>

    <?php $object = new VTCore_Wordpress_Objects_Array($schedule); ?>

    <div class="row service-table-row js-isotope" data-isotope-options="<?php echo esc_attr($isotope); ?>">

      <?php
      $column = 'col-xs-12 col-sm-12 col-md-12 col-lg-12';
      if ($object->get('description')) {
        $column = 'col-xs-12 col-sm-7 col-md-8 col-lg-8';
      }

      $time = '';
      if ($object->get('start')) {
        $time .= '<span class="start">' . esc_html($object->get('start')) . '</span>';
      }

      if ($object->get('end')) {
        $time .= '<span class="separator"> ' . esc_html__('until', 'medikal') . ' </span>';
        $time .= '<span class="end">' . esc_html($object->get('end')) . '</span>';
      }

      $days = '';
      if ($object->get('days')) {
        $days = $object->get('days');
        if (count($days) > 1) {
          $last = array_pop($days);
        }

        $days = implode(', ', $days);
        if (isset($last)) {
          $days .= ' ' . __('and', 'victheme_medikal') . ' ' . $last;
        }

        $days = esc_html($days);
      }
      ?>

      <?php if ($object->get('description')) : ?>
        <div class="services-column-description item col-xs-12 col-sm-5 col-md-4 col-lg-4">
          <?php echo wp_kses_post($object->get('description')); ?>
        </div>
      <?php endif; ?>

      <div class="services-column-time-user item <?php echo esc_attr($column); ?>">

        <?php if (!empty($days) || !empty($time) || $object->get('location')) : ?>
          <div class="services-time row">

          <?php if ($object->get('location')) : ?>
            <div class="services-time-location col-xs-4">
              <?php echo wp_kses_post($object->get('location')); ?>
            </div>
          <?php endif; ?>

          <?php if (!empty($time) || !empty($days)) : ?>
            <div class="services-time-date col-xs-8">
            <?php if (!empty($time)) : ?>
              <div class="services-time-hour">
                <?php echo wp_kses_post($time); ?>
              </div>
            <?php endif; ?>

            <?php if (!empty($days)) : ?>
              <div class="services-time-days">
                <?php echo wp_kses_post($days); ?>
              </div>
            <?php endif; ?>
            </div>
          <?php endif; ?>
        </div>
        <?php endif; ?>


        <?php if ($object->get('users')) : ?>
        <div class="services-users row">

          <?php foreach ($object->get('users') as $userid) : ?>

            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
              <?php
                if (defined('VTCORE_MEMBERS_LOADED')) {
                  $member = new VTCore_Members_Entity(array('post' => get_post((int) $userid)));
                  $thumbnail = get_the_post_thumbnail($userid, 'medium');
                  $office = $member->getOffice();
                }
              ?>

              <?php if (isset($thumbnail) && !empty($thumbnail)) : ?>
              <div class="services-member-photo">
                <?php echo wp_kses_post($thumbnail); ?>
              </div>
              <?php endif; ?>

              <div class="services-member-name">
                <?php echo wp_kses_post($services->getUser($userid)->get('markup')); ?>
              </div>

              <?php if (isset($office) && !empty($office)) : ?>
              <div class="services-member-office">
                <?php echo esc_html__('Office : ', 'victheme_medikal') . wp_kses_post($office); ?>
              </div>
              <?php endif; ?>

            </div>

          <?php endforeach; ?>

        </div>
      <?php endif; ?>

      </div>


    </div>

  <?php endforeach; ?>

</div>