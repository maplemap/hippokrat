<?php
/**
 * Template for displaying customized member practice schedules
 * this must be called inside loop
 *
 * @author jason.xie@victheme.com
 *
 */

  global $post;

  // Get the members entity
  $members = isset($post->members) ? $post->members : false;

  if (!is_a($members, 'VTCore_Members_Entity')) {
    return;
  }

  $isotope = json_encode(array(
    'itemSelector' => '.item',
    'layoutMode' => 'fitRows',
    'fitRows' => array(
      'equalheight' => TRUE,
      'gutter' => array(
        'width' => 0,
        'height' => 0,
      ),
    ),
    'resizeDelay' => 300,
  ));

  $schedules = array();
  foreach ($members->get('services') as $post_id) {
    $meta = new VTCore_Wordpress_Objects_Array(get_post_meta($post_id, '_services_data', true));
    foreach ($meta->get('schedule') as $delta => $schedule) {

      $object = new VTCore_Wordpress_Objects_Array($schedule);

      $member = false;
      foreach ($object->get('users') as $userID) {
        if ($userID == $members->get('post.ID')) {
          $member = true;
          break;
        }
      }

      $schedules[$delta] = $schedule;
    }
  }

  if (empty($schedules)) {
    return;
  }
?>


<div class="members-table-listing practice-table container-fluid">

  <?php foreach ($schedules as $delta => $schedule) : ?>

    <?php $object = new VTCore_Wordpress_Objects_Array($schedule); ?>

    <div class="row members-table-row js-isotope" data-isotope-options="<?php echo esc_attr($isotope); ?>">

      <?php
      $column = 'col-xs-12 col-sm-12 col-md-12 col-lg-12';
      if ($object->get('description')) {
        $column = 'col-xs-12 col-sm-7 col-md-8 col-lg-8';
      }

      $time = '';
      if ($object->get('start')) {
        $time .= '<span class="start">' . esc_html($object->get('start')) . '</span>';
      }

      if ($object->get('end')) {
        $time .= '<span class="separator"> ' . esc_html__('until', 'medikal') . ' </span>';
        $time .= '<span class="end">' . esc_html($object->get('end')) . '</span>';
      }

      $days = '';
      if ($object->get('days')) {
        $days = $object->get('days');
        if (count($days) > 1) {
          $last = array_pop($days);
        }

        $days = implode(', ', $days);
        if (isset($last)) {
          $days .= ' ' . __('and', 'victheme_medikal') . ' ' . $last;
        }

        $days = esc_html($days);
      }
      ?>

      <?php if ($object->get('description')) : ?>
        <div class="members-column-title item col-xs-12 col-sm-5 col-md-4 col-lg-4">
          <?php echo wp_kses_post($object->get('description')); ?>
        </div>
      <?php endif; ?>

      <div class="members-column-date-description item <?php echo esc_attr($column); ?>">

        <?php if ($object->get('location')) : ?>
          <div class="members-time row">
            <div class="members-time-location">
              <?php echo wp_kses_post($object->get('location')); ?>
            </div>
        </div>
        <?php endif; ?>


        <?php if (!empty($time) || !empty($days)) : ?>
        <div class="members-description row">
          <div class="members-time-date">
            <?php if (!empty($time)) : ?>
              <div class="members-time-hour">
                <?php echo wp_kses_post($time); ?>
              </div>
            <?php endif; ?>

            <?php if (!empty($days)) : ?>
              <div class="members-time-days">
                <?php echo wp_kses_post($days); ?>
              </div>
            <?php endif; ?>
            </div>
        </div>
        <?php endif; ?>
      </div>
    </div>
  <?php endforeach; ?>
</div>