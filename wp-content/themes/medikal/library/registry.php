<?php
/**
 * Simple class for integrating with TGMP script
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Library_Registry {

  private $plugins = array();
  private $config;
  private $defaultPath;



  /**
   * Automatic booting
   */
  public function registerPlugin() {

    $this->defaultPath =  VTCORE_ZEUS_PLUGIN_SOURCE_PATH . DIRECTORY_SEPARATOR;
    $this->loadPlugin();
    $this->loadConfig();
    $this->activate();

  }


  /**
   * Load the plugin configuration data
   */
  private function loadPlugin() {

    $this->plugins = array(

      // VicTheme Core
      array(
        'name'               => 'VicTheme Core',
        'slug'               => 'victheme_core',
        'source'             => $this->defaultPath . 'victheme_core.zip',
        'required'           => true,
        'version'            => '1.7.30',
        'force_activation'   => true,
        'force_deactivation' => false,
        'force_install'      => true,
      ),

      // VicTheme Department
      array(
        'name'               => 'VicTheme Department',
        'slug'               => 'victheme_department',
        'source'             => $this->defaultPath . 'victheme_department.zip',
        'required'           => false,
        'version'            => '1.0.2',
        'force_activation'   => false,
        'force_deactivation' => false,
        'force_install'      => true,
      ),


      // VicTheme Services
      array(
        'name'               => 'VicTheme Services',
        'slug'               => 'victheme_services',
        'source'             => $this->defaultPath . 'victheme_services.zip',
        'required'           => false,
        'version'            => '1.0.2',
        'force_activation'   => false,
        'force_deactivation' => false,
        'force_install'      => true,
      ),


      // VicTheme Members
      array(
        'name'               => 'VicTheme Members',
        'slug'               => 'victheme_members',
        'source'             => $this->defaultPath . 'victheme_members.zip',
        'required'           => false,
        'version'            => '1.0.2',
        'force_activation'   => false,
        'force_deactivation' => false,
        'force_install'      => true,
      ),


      // VicTheme Icons
      array(
        'name'               => 'VicTheme Icons',
        'slug'               => 'victheme_icons',
        'source'             => $this->defaultPath . 'victheme_icons.zip',
        'required'           => false,
        'version'            => '1.1.2',
        'force_activation'   => false,
        'force_deactivation' => false,
        'force_install'      => true,
      ),


      // VicTheme VisualLoop
      array(
        'name'               => 'VicTheme VisualLoop',
        'slug'               => 'victheme_visualloop',
        'source'             => $this->defaultPath . 'victheme_visualloop.zip',
        'required'           => false,
        'version'            => '1.0.1',
        'force_activation'   => false,
        'force_deactivation' => false,
        'force_install'      => true,
      ),


      // VicTheme Headline
      array(
        'name'               => 'VicTheme Headline',
        'slug'               => 'victheme_headline',
        'source'             => $this->defaultPath . 'victheme_headline.zip',
        'required'           => false,
        'version'            => '1.8.7',
        'force_activation'   => false,
        'force_deactivation' => false,
        'force_install'      => true,
      ),

      // VicTheme Demo
      array(
        'name'               => 'VicTheme Demo',
        'slug'               => 'victheme_demo',
        'source'             => $this->defaultPath . 'victheme_demo.zip',
        'required'           => false,
        'version'            => '1.3.4',
        'force_activation'   => false,
        'force_install'      => true,
        'force_deactivation' => false,
      ),


      // VicTheme Maps
      array(
        'name'               => 'VicTheme Maps',
        'slug'               => 'victheme_maps',
        'source'             => $this->defaultPath . 'victheme_maps.zip',
        'required'           => false,
        'version'            => '1.7.0',
        'force_activation'   => false,
        'force_deactivation' => false,
        'force_install'      => true,
      ),


      // VicTheme Visualplus
      array(
        'name'               => 'VicTheme VisualPlus',
        'slug'               => 'victheme_visualplus',
        'source'             => $this->defaultPath . 'victheme_visualplus.zip',
        'required'           => false,
        'version'            => '1.4.1',
        'force_activation'   => true,
        'force_deactivation' => false,
        'force_install'      => true,
      ),

      // Visual Composer
      array(
        'name'               => 'Visual Composer',
        'slug'               => 'js_composer',
        'source'             => $this->defaultPath .  'js_composer.zip',
        'required'           => false,
        'version'            => '4.7.4',
        'force_activation'   => true,
        'force_deactivation' => false,
        'force_install'      => true,
      ),


      // Layer slider
      array(
        'name'               => 'Layer Slider',
        'slug'               => 'layerslider',
        'source'             => $this->defaultPath . 'layerslider.zip',
        'required'           => false,
        'version'            => '5.6.2',
        'force_activation'   => false,
        'force_deactivation' => false,
        'force_install'      => false,
      ),

      // Layer slider
      array(
        'name'               => 'Revolution Slider',
        'slug'               => 'revslider',
        'source'             => $this->defaultPath . 'revslider.zip',
        'required'           => false,
        'version'            => '4.6.93',
        'force_activation'   => false,
        'force_deactivation' => false,
        'force_install'      => false,
      ),


      // VicTheme Slick
      array(
        'name'               => 'VicTheme Slick',
        'slug'               => 'victheme_slick',
        'source'             => $this->defaultPath . 'victheme_slick.zip',
        'required'           => false,
        'version'            => '1.2.2',
        'force_activation'   => false,
        'force_deactivation' => false,
        'force_install'      => false,
      ),

      // VicTheme WooCommerce
      array(
        'name'               => 'VicTheme WooCommerce',
        'slug'               => 'victheme_woocommerce',
        'source'             => $this->defaultPath . 'victheme_woocommerce.zip',
        'required'           => false,
        'version'            => '1.1.7',
        'force_activation'   => false,
        'force_deactivation' => false,
        'force_install'      => false,
      ),


      // WooCommerce
      array(
        'name'               => 'WooCommerce',
        'slug'               => 'woocommerce',
        'source'             => $this->defaultPath . 'woocommerce.zip',
        'version'            => '2.4.8',
        'required'           => false,
        'force_activation'   => false,
        'force_install'      => false,
      ),


      // Display Widget
      array(
        'name'               => 'Display Widgets',
        'slug'               => 'display-widgets',
        'source'             => $this->defaultPath . 'display-widgets.zip',
        'required'           => true,
        'force_activation'   => true,
        'force_install'      => true,
      ),


      // Contact Form 7
      array(
        'name'               => 'Contact Form 7',
        'slug'               => 'contact-form-7',
        'source'             => $this->defaultPath . 'contact-form-7.zip',
        'required'           => false,
        'force_activation'   => false,
        'force_install'      => false,
      ),

      // Easy Theme Upgrade
      array(
        'name'               => 'Easy Theme And Plugin Upgrade',
        'slug'               => 'easy-theme-and-plugin-upgrades',
        'source'             => $this->defaultPath . 'easy-theme-and-plugin-upgrades.zip',
        'required'           => true,
        'force_activation'   => true,
        'force_install'      => true,
      ),


      // News Letter Signup
      array(
        'name'               => 'Newsletter Signup',
        'slug'               => 'newsletter-sign-up',
        'source'             => $this->defaultPath . 'newsletter-sign-up.zip',
        'required'           => false,
        'force_activation'   => false,
        'force_install'      => false,
      ),


      // Image Widget
      array(
        'name'               => 'Image Widget',
        'slug'               => 'image-widget',
        'source'             => $this->defaultPath . 'image-widget.zip',
        'required'           => false,
        'force_activation'   => false,
        'force_install'      => false,
      ),



    );
  }




  /**
   * Load all the configuration data
   */
  private function loadConfig() {

    $this->config = array(
      'id'           => BASE_THEME,
      'default_path' => '',
      'menu'         => 'tgmpa-install-plugins',
      'has_notices'  => true,
      'dismissable'  => true,
      'dismiss_msg'  => '',
      'is_automatic' => true,
      'message'      => '',
      'strings'      => array(
        'page_title'                      => __( 'Install Required Plugins', 'medikal'),
        'menu_title'                      => __( 'Install Plugins', 'medikal'),
        'installing'                      => __( 'Installing Plugin: %s', 'medikal'), // %s = plugin name.
        'oops'                            => __( 'Something went wrong with the plugin API.', 'medikal'),
        'notice_can_install_required'     => _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.', 'medikal'), // %1$s = plugin name(s).
        'notice_can_install_recommended'  => _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.', 'medikal'), // %1$s = plugin name(s).
        'notice_cannot_install'           => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.', 'medikal'), // %1$s = plugin name(s).
        'notice_can_activate_required'    => _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.', 'medikal'), // %1$s = plugin name(s).
        'notice_can_activate_recommended' => _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.', 'medikal'), // %1$s = plugin name(s).
        'notice_cannot_activate'          => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.', 'medikal'), // %1$s = plugin name(s).
        'notice_ask_to_update'            => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.', 'medikal'), // %1$s = plugin name(s).
        'notice_cannot_update'            => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.', 'medikal'), // %1$s = plugin name(s).
        'install_link'                    => _n_noop( 'Begin installing plugin', 'Begin installing plugins', 'medikal'),
        'activate_link'                   => _n_noop( 'Begin activating plugin', 'Begin activating plugins', 'medikal'),
        'return'                          => __( 'Return to Required Plugins Installer', 'medikal'),
        'plugin_activated'                => __( 'Plugin activated successfully.', 'medikal'),
        'complete'                        => __( 'All plugins installed and activated successfully. %s', 'medikal'), // %s = dashboard link.
        'nag_type'                        => 'updated' // Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
      )
    );
  }



  /**
   * Activates the plugin dependencies
   */
  private function activate() {
    tgmpa( $this->plugins, $this->config );
  }

}