<?php
/**
 * Headline Simple Mode stemplates
 *
 * Building the custom headline that can be configured via post page.
 *
 * Themes that wishes to implement the Headline element
 * must copy this template to the theme folder and
 * call it as normal template would be eg. using get_template_part()
 * or get_sidebar().
 *
 * @author jason.xie@victheme.com
 */
?>
<?php

  // Don't Proceed any further if no VicTheme Headline plugin is enabled.
  if (!defined('VTCORE_HEADLINE_LOADED')) {
    return;
  }

  // Booting headline
  $instance = VTCore_Headline_Utility::getHeadline();

  // User hasnt set to disable yet set to enable by default
  if (!isset($instance['general']['enable'])) {
    $instance['general']['enable'] = TRUE;
  }

  if (!isset($instance['background'])) {
    $instance['background'] = FALSE;
  }

  // Non empty array will throw false true.
  if (isset($instance['background']['video'])) {
    $instance['background']['video'] = array_filter($instance['background']['video']);
  }

  // User disabled the headline, bailing out!.
  if (!VTCore_Headline_Utility::checkEnabled('general', $instance)) {
    return;
  }

  VTCore_Wordpress_Utility::loadAsset('jquery-verticalcenter');
  VTCore_Wordpress_Utility::loadAsset('headline-front');

  if (isset($instance['background']['parallax']) && $instance['background']['parallax'] != 'none') {
    VTCore_Wordpress_Utility::loadAsset('jquery-parallax');
  }

  if (!isset($instance['background']['video']['mp4'])
      && !isset($instance['background']['video']['ogv'])
      && !isset($instance['background']['video']['webm'])) {
    $instance['background']['video'] = false;
  }

?>

<header id="headline" class="vertical-center" data-vertical-force="true">

  <?php if (VTCore_Headline_Utility::checkEnabled('masking', $instance)) : ?>
    <div class="headline-mask"></div>
  <?php endif; ?>

  <?php if (isset($instance['background']['video']) && !empty($instance['background']['video'])) : ?>

    <!-- Video Background -->
    <div class="headline-background"></div>
    <div class="headline-video"
         data-mode="video-background-ng"
         data-settings="<?php echo esc_attr(json_encode($instance['background']['video'])); ?>"></div>
  <?php endif; ?>


  <?php if (!isset($instance['background']['video']) || empty($instance['background']['video'])) : ?>
    <div
      class="headline-background <?php if (isset($instance['background']['parallax']) && $instance['background']['parallax'] != 'none') {
        echo esc_attr($instance['background']['parallax']);
      } ?>"></div>
  <?php endif; ?>

  <?php if ((isset($instance['general']['title']) && !empty($instance['general']['title']))
            || (isset($instance['general']['subtitle']) && !empty($instance['general']['subtitle']))) : ?>
  <div class="area clearfix headline-content">
    <div class="container-fluid">
      <div class="row">
        <div class="region col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <?php if (isset($instance['general']['title']) && !empty($instance['general']['title'])) : ?>
            <h1 class="headline-title">
              <span class="title">
              <?php echo wp_kses_post($instance['general']['title']); ?>
              </span>
            </h1>
          <?php endif; ?>

          <?php if (isset($instance['general']['subtitle']) && !empty($instance['general']['subtitle'])) : ?>
            <div class="headline-subtitle">
              <?php echo wp_kses_post($instance['general']['subtitle']); ?>
            </div>

          <?php endif; ?>
        </div>
      </div>

    </div>
  </div>
  <?php endif; ?>
</header>