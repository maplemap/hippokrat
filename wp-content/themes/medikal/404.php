<?php
/**
 * Template for displaying the page 404.
 *
 * This is for the WordPress page 404 not page empty (eg. search not found)
 *
 * @author jason.xie@victheme.com
 */
?>
<?php get_header(); ?>

  <div id="page-not-found" class="area animated fadeInLeft">
    <div class="container-fluid">
      <div class="row">

        <div id="content"
             class="region pseudo-background
           <?php if (VTCore_Zeus_Utility::getSidebar('notfound')) {
               echo VTCore_Zeus_Utility::getColumnSize('content');
             } ?>
           with-sidebar-<?php echo VTCore_Zeus_Utility::getSidebar('notfound'); ?>"
          >

          <?php if (VTCore_Zeus_Init::getFeatures()
            ->get('show.notfound.title')
          ) : ?>

            <h1 class="title">
              <?php echo VTCore_Zeus_Init::getFeatures()
                ->get('options.notfound.title', 'post'); ?>
            </h1>

          <?php endif; ?>


          <?php if (VTCore_Zeus_Init::getFeatures()
            ->get('show.notfound.content')
          ) : ?>

            <p class="not-found">
              <?php echo VTCore_Zeus_Init::getFeatures()
                ->get('options.notfound.content', 'post'); ?>
            </p>

          <?php endif; ?>

        </div>


        <?php
        // Build sidebar right.
        if (VTCore_Zeus_Utility::getSidebar('notfound') == 'right'
          || VTCore_Zeus_Utility::getSidebar('notfound') == 'left'
        ) {
          get_sidebar('sidebar');
        }
        ?>

      </div>
    </div>
  </div>

<?php get_footer(); ?>