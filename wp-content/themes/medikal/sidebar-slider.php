<?php
/**
 * Sidebar template for slider positioned on top of content.
 * @author jason.xie@victheme.com
 */
?>
<?php if (VTCore_Zeus_Utility::isActiveSidebar('slider')) : ?>
  <div id="slider">
    <div class="container-fluid">
      <div id="slider-region" class="region row <?php echo VTCore_Zeus_Utility::getColumnSize('slider');?>">
        <?php dynamic_sidebar('slider'); ?>
      </div>
    </div>
  </div>
<?php endif;?>