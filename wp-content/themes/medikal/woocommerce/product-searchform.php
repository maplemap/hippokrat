<?php
/**
 * Template for overriding default
 * woocommerce product search markup
 *
 * @see get_product_search_form()
 * @author jason.xie@victheme.com
 */
?>
<div class="search-form">
  <form action="<?php echo esc_url(home_url( '/' )); ?>" role="search" method="get" id="searchform">
    <input type="hidden" name="post_type" value="product" />
    <input type="text" name="s" class="search-field" value="<?php esc_attr(the_search_query()); ?>" placeholder="<?php _e( 'Search for products', 'woocommerce' ); ?>" />
    <button class="search-submit" type="submit"><i class="fa fa-search"></i></button>
  </form>
</div>