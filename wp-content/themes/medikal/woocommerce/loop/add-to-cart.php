<?php
/**
 * Loop Add to Cart
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


global $product;

// Normal button
$button = esc_html( $product->add_to_cart_text() );
$qty = esc_attr( isset( $quantity ) ? $quantity : 1 );

// Flip mode want mini button
// if (VTCore_Zeus_Init::getFeatures()->get('options.products.archive.template') == 'flip') {
  $button = '<i class="fa fa-plus"></i>';
  $qty = 1;
// }


echo apply_filters( 'woocommerce_loop_add_to_cart_link',
	sprintf( '<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" data-quantity="%s" class="button teaser-button %s product_type_%s">%s</a>',
		esc_url( $product->add_to_cart_url() ),
		esc_attr( $product->id ),
		esc_attr( $product->get_sku() ),
		$qty,
		$product->is_purchasable() && $product->is_in_stock() ? 'add_to_cart_button' : '',
		esc_attr( $product->product_type ),
		$button
	),
$product );
