<?php
/**
 * Product Loop Start
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */


  $isotope = array(
    'itemSelector' => '.product-item',
    'layoutMode' => 'fitRows',
    'fitRows' => array(
      'gutter' => array(
        'width' => 0,
        'height' => 2,
      ),
      'equalheight' => true,
    ),
  );

  global $woocommerce_loop;

  VTCore_Zeus_Init::getFeatures()->mutate('options.products.archive.grids.columns.mobile', $woocommerce_loop['columns']);
  VTCore_Zeus_Init::getFeatures()->mutate('options.products.archive.grids.columns.tablet', $woocommerce_loop['columns']);
  VTCore_Zeus_Init::getFeatures()->mutate('options.products.archive.grids.columns.small',  $woocommerce_loop['columns']);
  VTCore_Zeus_Init::getFeatures()->mutate('options.products.archive.grids.columns.large',  $woocommerce_loop['columns']);

  VTCore_Wordpress_Utility::loadAsset('jquery-isotope');

?>
<div class="shop-catalog">
  <div class="shop-main-loop clearfix js-isotope template-shop-grid" data-isotope-options="<?php echo esc_attr(json_encode($isotope)); ?>">