<?php
/**
 * Display the product teaser content in normal mode
 * @author jason.xie@victheme.com
 * @see content-product.php
 */

  global $product;

?>

  <?php
  // Show flash banner
  if (isset($show['flash'])) {
    woocommerce_show_product_loop_sale_flash();
  }
  ?>

  <?php if (isset($show['image']) && has_post_thumbnail()) : ?>
    <div class="product-image post-image-hover-block">
      <a class="post-link"
         href="<?php echo esc_url(get_post_permalink($post->ID)); ?>">
        <?php echo woocommerce_template_loop_product_thumbnail(); ?>
      </a>
    </div>
  <?php endif; ?>

  <?php if (isset($show['title'])) : ?>
    <h3 class="product-title clearfix clearboth">
      <a href="<?php the_permalink(); ?>">
        <?php
        do_action('woocommerce_before_shop_loop_item_title');
        the_title();
        do_action('woocommerce_after_shop_loop_item_title');
        ?>
      </a>
    </h3>
  <?php endif; ?>

  <?php if (isset($show['price'])) : ?>
    <div class="product-price clearfix clearboth">
      <?php woocommerce_template_loop_price(); ?>
    </div>
  <?php endif; ?>

  <?php if (isset($show['rating'])) : ?>
    <div class="product-rating clearfix clearboth">
      <?php woocommerce_template_loop_rating(); ?>
    </div>
  <?php endif; ?>

  <?php if (isset($show['excerpt'])) : ?>
    <div class="product-excerpt clearfix clearboth">
      <?php woocommerce_template_single_excerpt(); ?>
    </div>
  <?php endif; ?>

  <?php if (isset($show['cart'])) : ?>
    <div class="product-cart clearfix clearboth">
      <?php woocommerce_template_loop_add_to_cart(); ?>
    </div>
  <?php endif; ?>
