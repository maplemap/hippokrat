<?php
/**
 * The Template for displaying product archives,
 * including the main shop page which is a post type archive.
 *
 *
 * @todo trim and simplify?
 *
 * @overridden
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     2.0.0
 */

  // Removing default hook from woocommerce plugin
  remove_action('woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
  remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
  remove_action('woocommerce_before_shop_loop', 'woocommerce_result_count', 20);
  remove_action('woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30);
  remove_action('woocommerce_after_shop_loop', 'woocommerce_pagination', 10);
  remove_action('woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);


?>
<?php get_header(); ?>

<div id="maincontent" class="area shop-catalog clearfix">
  <div class="container-fluid">
    <div class="row">


      <div id="content"
           class="region
              <?php if (VTCore_Zeus_Utility::getSidebar('product_archive')) echo VTCore_Zeus_Utility::getColumnSize('content'); ?>
              <?php if (VTCore_Zeus_Utility::getSidebar('product_archive')) echo 'with-sidebar-' . VTCore_Zeus_Utility::getSidebar('product_archive'); ?>">


        <?php if (VTCore_Zeus_Init::getFeatures()->get('show.product.catalog.counter')
                  || VTCore_Zeus_Init::getFeatures()->get('show.product.catalog.sorter')) : ?>
          <div class="catalog-sorter-count row">
            <?php if (VTCore_Zeus_Init::getFeatures()->get('show.product.catalog.counter')) :?>
            <div class="shop-result-count clearfix col-xs-6 col-sm-6 col-md-6 col-lg-6">
              <?php woocommerce_result_count(); ?>
            </div>
            <?php endif; ?>

            <?php if (VTCore_Zeus_Init::getFeatures()->get('show.product.catalog.counter')) : ?>
              <div class="shop-sorter clearfix col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right">
              <?php woocommerce_catalog_ordering(); ?>
              </div>
            <?php endif; ?>
          </div>
        <?php endif; ?>

        <?php

        // Removing default hook from woocommerce plugin
        remove_action('woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
        remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
        remove_action('woocommerce_before_shop_loop', 'woocommerce_result_count', 20);
        remove_action('woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30);
        remove_action('woocommerce_after_shop_loop', 'woocommerce_pagination', 10);
        remove_action('woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

        do_action('woocommerce_before_main_content');

        do_action('woocommerce_before_shop_loop');

        // Modify main query object
        woocommerce_product_subcategories();

        // Build the main loop using WpLoop Object
        // This will use the main WP_Query object, to override the query such as for
        // changing post_per_page value, use action pre_get_posts.
        // @see VTCore_Wordpress_Element WpLoop
        $arguments = array(
          'id' => 'shop-main-loop',
          'queryMain' => TRUE,

          'ajax' => VTCore_Zeus_Init::getFeatures()
            ->get('options.products.archive.ajax'),

          'attributes' => array(
            'class' => array(
              'id' => 'shop-main-loop',
              'clearfix' => 'clearfix',
              'clearboth' => 'clearboth',
              'template' => 'template-shop-grid',
            ),
          ),
          'template' => array(
            'items' => VTCORE_ZEUS_THEME_PATH . DIRECTORY_SEPARATOR . 'woocommerce' . DIRECTORY_SEPARATOR . 'content-product.php',
            'empty' => 'product-empty.php',
            'mode' => 'grid',
          ),

          'ajax-queue' => array(
            'append',
          ),
          'grids' => VTCore_Zeus_Init::getFeatures()
            ->get('options.products.archive.grids'),
          'show' => TRUE,
          'data' => array(
            'isotope-options' => array(
              'itemSelector' => '.product-item',
              'layoutMode' => 'fitRows',
              'fitRows' => array(
                'gutter' => array(
                  'width' => 0,
                  'height' => 0,
                ),
                'equalheight' => TRUE,
              ),
            ),
          ),
          'custom' => array(
            'woocommerce_update_count',
          ),
        );


        // Allow injection of custom arguments via $contentArgs
        if (isset($contentArgs)) {
          $arguments = VTCore_Utility::arrayMergeRecursiveDistinct($contentArgs, $arguments);
        }


        if ($arguments['show']) {
          $loopObject = new VTCore_Wordpress_Element_WpLoop($arguments);
        }

        // Building the pager elements using WpPager object
        // @see VTCore_Wordpress_Element WpPager
        $arguments = array(
          'id' => $loopObject->getContext('id'),
          'query' => $loopObject->getContext('query'),

          'ajax' => VTCore_Zeus_Init::getFeatures()
            ->get('options.products.archive.ajax'),

          'mini' => VTCore_Zeus_Init::getFeatures()
            ->get('options.products.archive.pager.mini'),

          'infinite' => VTCore_Zeus_Init::getFeatures()
            ->get('options.products.archive.pager.infinite'),

          'attributes' => array(
            'class' => array(
              'text-center',
            ),
          ),
          'show' => TRUE,
        );

        // Allow custom arguments injection via $pagerArgs
        if (isset($pagerArgs)) {
          $arguments = VTCore_Utility::arrayMergeRecursiveDistinct($pagerArgs, $arguments);
        }

        if ($arguments['show']) {
          // Object will be altered via vtcore_wordpress_pager_object_alter action.
          $pagerObject = new VTCore_Wordpress_Element_WpPager($arguments);
        }


        // Rendering the objects
        if (isset($loopObject)) {
          $loopObject->render();
        }

        // Rendering Pager
        if (isset($pagerObject)) {
          $pagerObject->render();
        }

        do_action('woocommerce_after_shop_loop');
        do_action('woocommerce_after_main_content');

        ?>

      </div>

      <?php
      // Build sidebar.
      if (VTCore_Zeus_Utility::getSidebar('product_archive') == 'right'
          || VTCore_Zeus_Utility::getSidebar('product_archive') == 'left') {
        get_sidebar('sidebar');
      }
      ?>

    </div>
  </div>
</div>


<?php get_footer(); ?>
