<?php
/**
 * Show error messages
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! $messages ){
	return;
}

?>
<div class="shop-message alert alert-error alert-dismissible">
  <i class="fa fa-exclamation"></i>
  <ul>
      <?php foreach ( $messages as $message ) : ?>
          <li><?php echo wp_kses_post( str_replace('button', 'btn btn-success btn-sm pull-right', $message) ); ?></li>
      <?php endforeach; ?>
  </ul>
  <button type="button" class="close pull-right" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
</div>
