<?php
/**
 * Show messages
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! $messages ){
	return;
}

?>

<?php foreach ( $messages as $message ) : ?>
  <div class="shop-message alert alert-info alert-dismissible">
    <i class="fa fa-info"></i>
    <button type="button" class="close pull-right" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <?php echo wp_kses_post( str_replace('button', 'btn btn-success btn-sm pull-right', $message) ); ?>
 </div>
<?php endforeach; ?>
