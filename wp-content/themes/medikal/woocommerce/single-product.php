<?php
/**
 * The Template for displaying all single products.
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product.php
 *
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     1.6.4
 */

  // Remove woocommerce default hooked elements
  remove_action('woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
  remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
  remove_action('woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);
  remove_action('woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

?>

<?php get_header(); ?>

  <!-- Main Content -->
  <div id="maincontent" class="area clearfix product-display">
    <div class="container-fluid">
      <div class="row">

        <div id="content"
             class="region clearfix pseudo-background
			     <?php if (VTCore_Zeus_Utility::getSidebar('product')) echo VTCore_Zeus_Utility::getColumnSize('content')?>
			     <?php if (VTCore_Zeus_Utility::getSidebar('product')) echo 'with-sidebar-' . VTCore_Zeus_Utility::getSidebar('product'); ?>">

          <?php

          do_action('woocommerce_before_main_content');

          wp_reset_postdata();
          while (have_posts()) {
            the_post();

            wc_get_template_part('content', 'single-product');

          }

          do_action('woocommerce_after_main_content');

          ?>

        </div>

        <?php
          // Build sidebar.
          if (VTCore_Zeus_Utility::getSidebar('product') == 'right'
            || VTCore_Zeus_Utility::getSidebar('product') == 'left') {
            get_sidebar('sidebar');
          }
        ?>

      </div>
    </div>
  </div>


<?php get_footer(); ?>