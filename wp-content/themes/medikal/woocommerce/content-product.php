<?php
/**
 * The template for displaying product content within loops.
 *
 * Overridden and modifed so it can be invoked under normal
 * WooCommerce Loop or using VTCore WPLoop object.
 *
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     2.4.0
 */

  global $product;

  if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
  }

  // Ensure visibility
  if (!$product || !$product->is_visible()) {
    return;
  }

  if (is_a($this, 'VTCore_Wordpress_Element_WpLoop')) {
    if ($this->getContext('template.file')) {
      $templateFile = $this->getContext('template.file');
    }
    if ($this->getContext('template.show')) {
      $show = $this->getContext('template.show');
    }
  }

  if (!isset($templateFile)) {
    $templateFile = VTCore_Zeus_Init::getFeatures()->get('options.products.archive.template');
  }

  // Extra post classes
  $classes = array(
    'product',
    'text-center',
    'product-template-' . $templateFile,
  );

  if (!isset($show)) {
    $show = VTCore_Zeus_Init::getFeatures()->get('show.product.teaser');
  }

  $show = array_filter($show);

  if (!isset($gridObject)) {
    $gridObject = new VTCore_Bootstrap_Grid_Column(VTCore_Zeus_Init::getFeatures()
        ->get('options.products.archive.grids'));
  }

  // Removing product teasers related hooks
  remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10);
  remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);
  remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);
  remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10);
  remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);

?>


<div class="column product-item clearfix <?php echo esc_attr($gridObject->getClass()); ?>">
  <div id="product-<?php echo esc_attr($post->ID); ?>" <?php post_class($classes);?>>

    <?php do_action('woocommerce_before_shop_loop_item'); ?>

    <?php
      include locate_template('woocommerce/content-product-' . $templateFile . '.php');
    ?>


    <?php do_action('woocommerce_after_shop_loop_item'); ?>

  </div>
</div>
