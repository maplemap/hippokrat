<?php
/**
 * Cross-sells
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

  if ( ! defined( 'ABSPATH' ) ) {
  	exit; // Exit if accessed directly
  }

  global $product, $woocommerce_loop;

  $crosssells = WC()->cart->get_cross_sells();

  if ( sizeof( $crosssells ) == 0 ) return;

  $meta_query = WC()->query->get_meta_query();

  $args = apply_filters( 'woocommerce_cross_sells_products_args', array(
    'post_type'           => 'product',
    'ignore_sticky_posts' => 1,
    'no_found_rows'       => 1,
    'posts_per_page'      => apply_filters( 'woocommerce_cross_sells_total', $posts_per_page ),
    'orderby'             => $orderby,
    'post__in'            => $crosssells,
    'meta_query'          => $meta_query
  ));

  $products = new WP_Query( $args );

  // Build the main loop using WpLoop Object
  // @see VTCore_Wordpress_Element WpLoop
  $arguments = array(
    'id' => 'cart-cross-sale-main',
    'queryMain' => false,
    'query' => $products,
    'ajax' => true,
    'attributes' => array(
      'class' => array(
        'id' => 'product-cross-loop',
        'clearfix' => 'clearfix',
        'clearboth' => 'clearboth',
        'template' => 'template-shop-' . VTCore_Zeus_Init::getFeatures()->get('options.products.cart.cross.layout'),
      ),
    ),
    'template' => array(
      'items' => VTCORE_ZEUS_THEME_PATH . DIRECTORY_SEPARATOR . 'woocommerce' . DIRECTORY_SEPARATOR  . 'content-product.php',
      'empty' => 'product-empty.php',
      'mode' =>  VTCore_Zeus_Init::getFeatures()->get('options.products.cart.cross.layout'),
      'file' => 'normal',
      'show' => VTCore_Zeus_Init::getFeatures()->get('options.products.cart.cross.teaser.show'),
    ),
    'custom' => VTCore_Zeus_Init::getFeatures()->get('options.products.cart.cross.teaser.show'),
    'ajax-queue' => array(
      'append',
    ),

    'grids' => VTCore_Zeus_Init::getFeatures()->get('options.products.cart.cross.teaser.grids'),
    'show' => true,
  );

  // Build masonry isotope arguments
  if (VTCore_Zeus_Init::getFeatures()->get('options.products.cart.cross.layout') == 'masonry') {
    $arguments['data']['isotope-options'] = array(
      'itemSelector' => '.product-item',
      'layoutMode' => 'masonry',
    );
  }

  // Build fitRows isotope arguments
  if (VTCore_Zeus_Init::getFeatures()->get('options.products.cart.cross.layout') == 'fitrow') {
    $arguments['data']['isotope-options'] = array(
      'itemSelector' => '.product-item',
      'layoutMode' => 'fitRows',
      'fitRows' => array(
        'gutter' => array(
          'width' => 0,
          'height' => 0,
        ),
        'equalheight' => true,
      ),
    );
  }

  // Allow injection of custom arguments via $contentArgs
  if (isset($contentArgs)) {
    $arguments = VTCore_Utility::arrayMergeRecursiveDistinct($contentArgs, $arguments);
  }

  if ($arguments['show']) {
    $loopObject = new VTCore_Wordpress_Element_WpLoop($arguments);
  }

  // Building the pager elements using WpPager object
  // @see VTCore_Wordpress_Element WpPager
  $arguments = array(
    'id' => $loopObject->getContext('id'),
    'query' => $loopObject->getContext('query'),
    'ajax' => true,
    'mini' =>  false,
    'infinite' =>  true,
    'attributes' => array(
      'class' => array(
        'text-center',
      ),
    ),
    'show' => true,
  );

  // Allow custom arguments injection via $pagerArgs
  if (isset($pagerArgs)) {
    $arguments = VTCore_Utility::arrayMergeRecursiveDistinct($pagerArgs, $arguments);
  }

  if ($arguments['show']) {
    // Object will be altered via vtcore_wordpress_pager_object_alter action.
    $pagerObject = new VTCore_Wordpress_Element_WpPager($arguments);
  }

if ( $products->have_posts() ) : ?>

	<div class="cross-sells shop-catalog">
	  <div class="products">
  		<h2 class="cart-title"><?php echo VTCore_Zeus_Init::getFeatures()->get('options.products.cart.cross.title', 'html') ?></h2>
  	  <?php
    	  $wrapper = new VTCore_Bootstrap_Element_BsElement(array(
    	    'type' => 'div',
    	    'attributes' => array(
    	      'class' => array(
    	        'shop-items-wrapper',
    	        'clearboth'
    	      ),
    	    ),
    	  ));

        // Rendering the objects
        if (isset($loopObject)) {
          $wrapper->addChildren($loopObject);
        }

        if (isset($pagerObject)) {
          $wrapper->addChildren($pagerObject);
        }

        $wrapper->render();
  	  ?>
	  </div>
	</div>

<?php endif; ?>