<?php
/**
 * Display single product reviews (comments)
 *
 * This template is completely overwritten for matching
 * the css markup and classes for the theme comment css.
 *
 * The WooCommerce parts is merged from the WC original
 * template.
 *
 * @author 		jason.xie@victheme.com
 * @package 	WooCommerce/Templates
 * @version     2.3.2
 */

  $showComment = true;
  $showForm = false;

  // Woocommerce specific settings
  if ( get_option( 'woocommerce_review_rating_verification_required' ) === 'no'
      || wc_customer_bought_product( '', get_current_user_id(), $product->id )) {
    $showForm = true;
  }

  // Disable comment entry and form on password
  // protected page
  if (post_password_required() || (!comments_open() && !have_comments())) {
    $showComment = false;
    $showForm = false;
  }

  // Don't even bother to build the markup if
  // conditional logic doesn't allow commenting
  if (!$showComment && !$showForm) {
    return;
  }

  // Load wordpress default comment javascript
  wp_enqueue_script("comment-reply");

  global $product;

?>

  <div id="post-comment" class="clearfix">
    <div class="comment-content">

      <?php if (post_password_required()) : ?>
        <!-- Password Protected -->
        <div id="post-comment-passworded"
             class="region">

          <div class="alert alert-warning">
            <?php echo esc_html__('This page is password protected. Enter the password to view comments.', 'medikal'); ?>
          </div>

        </div>
      <?php endif; ?>



      <?php if ($showComment) : ?>
        <!-- Comment Entry -->
        <section id="post-comment-entry"
                 class="<?php if ($showForm && comments_open()) : ?>with-comment-form<?php endif; ?>">

          <header class="comment-title">
            <h1>
              <?php
              if ( get_option( 'woocommerce_enable_review_rating' ) === 'yes' && ( $count = $product->get_review_count() ) )
                printf( _n( '%s review for %s', '%s reviews for %s', $count, 'woocommerce' ), '<span class="accent">' . $count . '</span>', get_the_title() );
              else
                _e( 'Reviews', 'woocommerce' );
              ?>
            </h1>
          </header>


          <?php if (have_comments()) : ?>

            <ul class="media-list">
              <?php wp_list_comments(apply_filters( 'woocommerce_product_review_list_args', array(
                'style' => 'ul',
                'short_ping' => TRUE,
                'avatar_size' => 64,
                'format' => 'html5',
                'callback' => 'woocommerce_comments',
              )));
              ?>
            </ul>

            <?php if (get_comment_pages_count() > 1 && get_option('page_comments')) : ?>

              <nav id="comment-nav">
                <?php previous_comments_link('<span class="btn">' . esc_html__('Older comments', 'medikal') . '</span>'); ?>
                <?php next_comments_link('<span class="btn">' . esc_html__('Newer comments', 'medikal') . '</span>'); ?>
              </nav>

            <?php endif; ?>

          <?php endif; ?>


          <?php if (!comments_open()) : ?>
            <!-- Comment Closed -->
            <h4 class="comment-closed">
              <?php echo esc_html__('Comments are closed', 'medikal'); ?>
            </h4>
          <?php endif; ?>

          <?php if (!have_comments() && comments_open()) : ?>
            <!-- No Comment found but commenting is not closed -->
            <p class="comment-empty">
              <?php _e( 'There are no reviews yet.', 'woocommerce' ); ?>
            </p>
          <?php endif; ?>

        </section>
      <?php endif; ?>

    </div>
  </div>

<?php if ($showForm && comments_open()) : ?>
  <!-- Comment Form -->
  <div id="post-comment-form" class="clearfix">
    <div class="comment-content">

      <header class="comment-title">
        <h1>
          <?php echo have_comments() ? esc_html__( 'Add a review', 'woocommerce' ) : esc_html__( 'Be the first to review', 'woocommerce' ) . ' &ldquo;' . get_the_title() . '&rdquo;'; ?>
        </h1>
      </header>

        <?php
        // WooCommerce Original
        $commenter = wp_get_current_commenter();

        $comment_form = array(
          'title_reply'          => '',
          'title_reply_to'       => __( 'Leave a Reply to %s', 'woocommerce' ),
          'comment_notes_before' => '',
          'comment_notes_after'  => '',
          'fields'               => array(

            'author' => '<p class="comment-form-author">
                          <label for="author">' . __( 'Name', 'woocommerce' ) . ' <span class="required">*</span></label>
                          <input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" aria-required="true" />
                        </p>',

            'email'  => '<p class="comment-form-email">
                          <label for="email">' . __( 'Email', 'woocommerce' ) . ' <span class="required">*</span></label>
                          <input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30" aria-required="true" />
                        </p>',
          ),
          'label_submit'  => __( 'Submit', 'woocommerce' ),
          'logged_in_as'  => '',
          'comment_field' => '',
        );

        if ( get_option( 'woocommerce_enable_review_rating' ) === 'yes' ) {

          // Modified version for enabling jquery raty
          VTCore_Wordpress_Utility::loadAsset('jquery-raty');

          $comment_form['comment_field'] .= '<p class="comment-form-rating"><label for="rating">' . __( 'Your Rating', 'woocommerce' ) .'</label><select name="rating" id="rating-wooraty">
							<option value="">' . __( 'Rate&hellip;', 'woocommerce' ) . '</option>
							<option value="5">' . __( 'Perfect', 'woocommerce' ) . '</option>
							<option value="4">' . __( 'Good', 'woocommerce' ) . '</option>
							<option value="3">' . __( 'Average', 'woocommerce' ) . '</option>
							<option value="2">' . __( 'Not that bad', 'woocommerce' ) . '</option>
							<option value="1">' . __( 'Very Poor', 'woocommerce' ) . '</option>
						</select></p>';
        }

        $comment_form['comment_field'] .= '
          <p class="comment-form-comment">
            <label for="comment">' . __( 'Your Review', 'woocommerce' ) . '</label>
            <textarea id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea>
          </p>
          <div class="clearfix"></div>';

        comment_form( apply_filters( 'woocommerce_product_review_comment_form_args', $comment_form ) );
        ?>
    </div>
  </div>

<?php endif; ?>