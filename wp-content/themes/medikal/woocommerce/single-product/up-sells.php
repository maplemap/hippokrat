<?php
/**
 * Single Product Up-Sells
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

  if ( ! defined( 'ABSPATH' ) ) {
  	exit; // Exit if accessed directly
  }

  global $product, $woocommerce_loop;

  $upsells = $product->get_upsells();

  if ( sizeof( $upsells ) == 0 ) {
  	return;
  }

  $meta_query = WC()->query->get_meta_query();

  $args = apply_filters( 'woocommerce_upsell_products_args', array(
    'post_type'           => 'product',
    'ignore_sticky_posts' => 1,
    'no_found_rows'       => 1,
    'posts_per_page'      => $posts_per_page,
    'orderby'             => $orderby,
    'post__in'            => $upsells,
    'post__not_in'        => array( $product->id ),
    'meta_query'          => $meta_query
  ));

  $products = new WP_Query( $args );

  // Build the main loop using WpLoop Object
  // @see VTCore_Wordpress_Element WpLoop
  $arguments = array(
    'id' => 'upsell-single-main',
    'queryMain' => false,
    'query' => $products,
    'ajax' => true,
    'attributes' => array(
      'class' => array(
        'id' => 'product-upsell-loop',
        'clearfix' => 'clearfix',
        'clearboth' => 'clearboth',
        'template' => 'template-shop-' . VTCore_Zeus_Init::getFeatures()->get('options.products.single.upsell.layout'),
      ),
    ),
    'template' => array(
      'items' => VTCORE_ZEUS_THEME_PATH . DIRECTORY_SEPARATOR . 'woocommerce' . DIRECTORY_SEPARATOR  . 'content-product.php',
      'empty' => 'product-empty.php',
      'mode' => VTCore_Zeus_Init::getFeatures()->get('options.products.single.upsell.layout'),
      'file' => 'normal',
      'show' => VTCore_Zeus_Init::getFeatures()->get('options.products.single.upsell.teaser.show'),
    ),
    'custom' => VTCore_Zeus_Init::getFeatures()->get('options.products.single.upsell.teaser.show'),
    'ajax-queue' => array(
      'append',
    ),

    'grids' => VTCore_Zeus_Init::getFeatures()->get('options.products.single.upsell.teaser.grids'),
    'show' => true,
  );

  // Build masonry isotope arguments
  if (VTCore_Zeus_Init::getFeatures()->get('options.products.single.upsell.layout') == 'masonry') {
    $arguments['data']['isotope-options'] = array(
      'itemSelector' => '.product-item',
      'layoutMode' => 'masonry',
    );
  }

  // Build fitRows isotope arguments
  if (VTCore_Zeus_Init::getFeatures()->get('options.products.single.upsell.layout') == 'fitrow') {
    $arguments['data']['isotope-options'] = array(
      'itemSelector' => '.product-item',
      'layoutMode' => 'fitRows',
      'fitRows' => array(
        'gutter' => array(
          'width' => 0,
          'height' => 0,
        ),
        'equalheight' => true,
      ),
    );
  }

  // Allow injection of custom arguments via $contentArgs
  if (isset($contentArgs)) {
    $arguments = VTCore_Utility::arrayMergeRecursiveDistinct($contentArgs, $arguments);
  }

  if ($arguments['show']) {
    $loopObject = new VTCore_Wordpress_Element_WpLoop($arguments);
  }

  // Building the pager elements using WpPager object
  // @see VTCore_Wordpress_Element WpPager
  $arguments = array(
    'id' => $loopObject->getContext('id'),
    'query' => $loopObject->getContext('query'),
    'ajax' => true,
    'mini' =>  false,
    'infinite' =>  true,
    'attributes' => array(
      'class' => array(
        'text-center',
      ),
    ),
    'show' => true,
  );

  // Allow custom arguments injection via $pagerArgs
  if (isset($pagerArgs)) {
    $arguments = VTCore_Utility::arrayMergeRecursiveDistinct($pagerArgs, $arguments);
  }

  if ($arguments['show']) {
    // Object will be altered via vtcore_wordpress_pager_object_alter action.
    $pagerObject = new VTCore_Wordpress_Element_WpPager($arguments);
  }
?>




<?php if ($products->have_posts()) : ?>

    <section id="product-upsell" class="shop-catalog clearboth">

     <header class="comment-title">
        <h1>
          <?php echo VTCore_Zeus_Init::getFeatures()->get('options.products.single.upsell.title', 'post'); ?>
        </h1>
      </header>
      <?php

        // Rendering the objects
        if (isset($loopObject)) {
          $loopObject->render();
        }

        if (isset($pagerObject)) {
          $pagerObject->render();
        }

      ?>

    </section>

<?php endif; ?>
