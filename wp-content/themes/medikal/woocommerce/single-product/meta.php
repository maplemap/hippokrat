<?php
/**
 * Single Product Meta
 *
 * Modified to wrap the meta label with span.meta-title and content with span.meta-content
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post, $product;

$cat_count = sizeof( get_the_terms( $post->ID, 'product_cat' ) );
$tag_count = sizeof( get_the_terms( $post->ID, 'product_tag' ) );

?>

  <?php do_action( 'woocommerce_product_meta_start' ); ?>

  <?php if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) : ?>

      <div class="sku_wrapper product-meta">
        <span class="meta-header header"><?php echo esc_html__( 'SKU', 'woocommerce' ); ?></span>
        <span class="sku meta-content" itemprop="sku"><?php echo ( $sku = $product->get_sku() ) ? esc_html($sku) : esc_html__( 'N/A', 'woocommerce' ); ?></span>
      </div>

  <?php endif; ?>

  <?php echo wp_kses_post($product->get_categories( ', ', '<div class="product-meta posted_in"><span class="meta-header header">' . _n( 'Category', 'Categories', $cat_count, 'woocommerce' ) . '</span><span class="meta-content">', '</span></div>' )); ?>

  <?php echo wp_kses_post($product->get_tags( ', ', '<div class="product-meta tagged_as"><span class="meta-header header">' . _n( 'Tag', 'Tags', $tag_count, 'woocommerce' ) . '</span><span class="meta-content">', '</span></div>' )); ?>

  <?php do_action( 'woocommerce_product_meta_end' ); ?>

