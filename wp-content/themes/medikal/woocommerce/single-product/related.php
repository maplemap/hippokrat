<?php
/**
 * Related Products
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

  if ( ! defined( 'ABSPATH' ) ) {
  	exit; // Exit if accessed directly
  }

  global $product, $woocommerce_loop;

  if ( empty( $product ) || ! $product->exists() ) {
  	return;
  }

  $related = $product->get_related( $posts_per_page );

  if ( sizeof( $related ) == 0 ) return;

  $args = apply_filters( 'woocommerce_related_products_args', array(
  	'post_type'            => 'product',
  	'ignore_sticky_posts'  => 1,
  	'no_found_rows'        => 1,
  	'posts_per_page'       => $posts_per_page,
  	'orderby'              => $orderby,
  	'post__in'             => $related,
  	'post__not_in'         => array( $product->id )
  ) );

  $products = new WP_Query( $args );

  // Build the main loop using WpLoop Object
  // @see VTCore_Wordpress_Element WpLoop
  $arguments = array(
    'id' => 'related-single-main',
    'queryMain' => false,
    'query' => $products,
    'ajax' => true,
    'attributes' => array(
      'class' => array(
        'id' => 'product-related-loop',
        'clearfix' => 'clearfix',
        'clearboth' => 'clearboth',
        'template' => 'template-shop-' . VTCore_Zeus_Init::getFeatures()->get('options.products.single.related.layout'),
      ),
    ),
    'template' => array(
      'items' => VTCORE_ZEUS_THEME_PATH . DIRECTORY_SEPARATOR . 'woocommerce' . DIRECTORY_SEPARATOR  . 'content-product.php',
      'empty' => 'product-empty.php',
      'mode' => VTCore_Zeus_Init::getFeatures()->get('options.products.single.related.layout'),
      'file' => 'normal',
      'show' => VTCore_Zeus_Init::getFeatures()->get('options.products.single.related.teaser.show'),
    ),
    'custom' => VTCore_Zeus_Init::getFeatures()->get('options.products.single.related.teaser.show'),
    'ajax-queue' => array(
      'append',
    ),

    'grids' => VTCore_Zeus_Init::getFeatures()->get('options.products.single.related.teaser.grids'),
    'show' => true,
  );

  // Build masonry isotope arguments
  if (VTCore_Zeus_Init::getFeatures()->get('options.products.single.related.layout') == 'masonry') {
    $arguments['data']['isotope-options'] = array(
      'itemSelector' => '.product-item',
      'layoutMode' => 'masonry',
    );
  }

  // Build fitRows isotope arguments
  if (VTCore_Zeus_Init::getFeatures()->get('options.products.single.related.layout') == 'fitrow') {
    $arguments['data']['isotope-options'] = array(
      'itemSelector' => '.product-item',
      'layoutMode' => 'fitRows',
      'fitRows' => array(
        'gutter' => array(
          'width' => 0,
          'height' => 0,
        ),
        'equalheight' => true,
      ),
    );
  }

  // Allow injection of custom arguments via $contentArgs
  if (isset($contentArgs)) {
    $arguments = VTCore_Utility::arrayMergeRecursiveDistinct($contentArgs, $arguments);
  }

  if ($arguments['show']) {
    $loopObject = new VTCore_Wordpress_Element_WpLoop($arguments);
  }

  // Building the pager elements using WpPager object
  // @see VTCore_Wordpress_Element WpPager
  $arguments = array(
    'id' => $loopObject->getContext('id'),
    'query' => $loopObject->getContext('query'),
    'ajax' => true,
    'mini' =>  false,
    'infinite' =>  true,
    'attributes' => array(
      'class' => array(
        'text-center',
      ),
    ),
    'show' => true,
  );

  // Allow custom arguments injection via $pagerArgs
  if (isset($pagerArgs)) {
    $arguments = VTCore_Utility::arrayMergeRecursiveDistinct($pagerArgs, $arguments);
  }

  if ($arguments['show']) {
    // Object will be altered via vtcore_wordpress_pager_object_alter action.
    $pagerObject = new VTCore_Wordpress_Element_WpPager($arguments);
  }
?>




<?php if ($products->have_posts()) : ?>

    <section id="product-related" class="shop-catalog clearboth">

      <header class="comment-title">
        <h1>
          <?php echo VTCore_Zeus_Init::getFeatures()->get('options.products.single.related.title', 'post'); ?>
        </h1>
      </header>
      <?php

        // Rendering the objects
        if (isset($loopObject)) {
          $loopObject->render();
        }

        if (isset($pagerObject)) {
          $pagerObject->render();
        }
      ?>

    </section>

<?php endif; ?>
