<?php
/**
 * Review Comments Template
 *
 * Closing li is left out on purpose!
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.1.0
 */

  // CSS Class logic
  $class = "without-child";
  if (!empty($args['has_children'])) {
    $class = "with-child";
  }

  $rating = intval( get_comment_meta( $comment->comment_ID, 'rating', true ) );

?>
<li id="comment-<?php comment_ID() ?>" <?php comment_class($class) ?>>

  <div class="comment-wrapper clearfix">

    <div class="comment-author-image media-object pull-left">
      <?php
      echo wp_kses_post(get_avatar($comment, $args['avatar_size']));
      ?>
    </div>

    <div class="comment-body media-body">

      <?php if ($comment->comment_approved == FALSE) : ?>
        <div class="alert alert-info"><?php echo esc_html__('Your comment is awaiting moderation.', 'medikal') ?></div>
      <?php endif; ?>

      <div class="comment-header">
      <?php if ( $rating && get_option( 'woocommerce_enable_review_rating' ) == 'yes' ) : ?>
        <span itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating" class="star-rating" title="<?php echo sprintf( esc_html__( 'Rated %d out of 5', 'woocommerce' ), $rating ) ?>">
            <span style="width:<?php echo ( $rating / 5 ) * 100; ?>%"><strong itemprop="ratingValue"><?php echo wp_kses_post($rating); ?></strong> <?php echo esc_html__( 'out of 5', 'woocommerce' ); ?></span>
        </span>
      <?php endif; ?>

      <?php if ( get_option( 'woocommerce_review_rating_verification_label' ) === 'yes' && wc_customer_bought_product( $comment->comment_author_email, $comment->user_id, $comment->comment_post_ID )) : ?>
        <span class="comment-reply">
          <?php echo esc_html__('Verified owner', 'medikal');?>
        </span>
      <?php endif;?>
      </div>

      <?php comment_text() ?>

      <div class="comment-footer clearfix">
        <span class="comment-author text-uppercase">
          <?php print get_comment_author_link(); ?>
        </span>
        <span class="comment-date">
          (<?php comment_date('d.m.Y'); ?>)
        </span>
      </div>

    </div>

  </div>

