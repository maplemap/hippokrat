<?php
/**
 * Description tab
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post;

$heading = apply_filters( 'woocommerce_product_description_heading', __( 'Product Description', 'woocommerce' ) );

// Theme Hack. Remove this to enable description
$heading = false;
?>

<?php if ( $heading ): ?>
  <h2><?php echo wp_kses_post($heading); ?></h2>
<?php endif; ?>

<?php the_content(); ?>
