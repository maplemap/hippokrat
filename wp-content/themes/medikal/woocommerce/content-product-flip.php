<?php
/**
 * Display the procut teaser content in flip mode
 * @author jason.xie@victheme.com
 * @see content-product.php
 */


  VTCore_Wordpress_Utility::loadAsset('jquery-flip');
  VTCore_Wordpress_Utility::loadAsset('jquery-verticalcenter');

  $jsFlip = json_encode(array(
    'axis' => 'y',
    'trigger' => 'click',
    'autosize' => true,
    //'forceHeight' => true,
    'front' => '.post-split.top',
    'back' => '.post-split.bottom',
  ));

  global $product;

?>

<div class="js-flip" data-flip-options="<?php echo esc_attr($jsFlip); ?>">


  <div class="post-split top front">

    <?php
    // Show flash banner
    if (isset($show['flash'])) {
      woocommerce_show_product_loop_sale_flash();
    }
    ?>

    <?php if (isset($show['image'])) : ?>
      <div class="product-image post-image-hover-block">
        <?php echo woocommerce_template_loop_product_thumbnail(); ?>
      </div>
    <?php endif; ?>

    <?php if (isset($show['title'])) : ?>
      <h3 class="product-title clearfix clearboth">
        <?php
        do_action('woocommerce_before_shop_loop_item_title');
        the_title();
        do_action('woocommerce_after_shop_loop_item_title');
        ?>
      </h3>
    <?php endif; ?>

    <?php if (isset($show['price'])) : ?>
      <div class="product-price clearfix clearboth">
        <?php woocommerce_template_loop_price(); ?>
      </div>
    <?php endif; ?>
  </div>


  <div class="post-split bottom back vertical-center" data-vertical-force="true">
    <div class="post-back-inner vertical-target">
      <?php if (isset($show['title'])) : ?>
        <h3 class="product-title clearfix clearboth">
          <a href="<?php the_permalink(); ?>">
            <?php
            do_action('woocommerce_before_shop_loop_item_title');
            the_title();
            do_action('woocommerce_after_shop_loop_item_title');
            ?>
          </a>
        </h3>
      <?php endif; ?>

      <?php if (isset($show['price'])) : ?>
        <div class="product-price clearfix clearboth">
          <?php woocommerce_template_loop_price(); ?>
        </div>
      <?php endif; ?>

      <?php if (isset($show['rating'])) : ?>
        <div class="product-rating clearfix clearboth">
          <?php woocommerce_template_loop_rating(); ?>
        </div>
      <?php endif; ?>

      <?php if (isset($show['excerpt'])) : ?>
        <div class="product-excerpt clearfix clearboth">
          <?php
            $post->oldExcerpt = $post->post_excerpt;
            $post->post_excerpt = wp_trim_words($post->post_excerpt, 14, '...');
            woocommerce_template_single_excerpt();
            $post->post_excerpt = $post->oldExcerpt;
          ?>
        </div>
      <?php endif; ?>

      <div class="product-cart clearfix clearboth button-groups">
        <?php if (isset($show['cart'])) : ?>
            <?php woocommerce_template_loop_add_to_cart(); ?>
        <?php endif; ?>
        <a href="<?php the_permalink(); ?>" class="teaser-button">
          <i class="fa fa-share"></i>
        </a>
      </div>

      </div>
  </div>
</div>