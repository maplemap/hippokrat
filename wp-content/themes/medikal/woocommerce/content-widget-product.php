<?php
/**
 * Modified version to show the product rating
 */
?>
<?php global $product; ?>
<li class="column">
	<a href="<?php echo esc_url( get_permalink( $product->id ) ); ?>" title="<?php echo esc_attr( $product->get_title() ); ?>">
		<?php echo wp_kses_post($product->get_image()); ?>
		<span class="product-title"><?php echo wp_kses_post($product->get_title()); ?></span>
	</a>
    <div class="product-price"><?php echo wp_kses_post($product->get_price_html()); ?></div>
	<div class="product-rating"><?php echo wp_kses_post($product->get_rating_html()); ?></div>
</li>