<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * Override this template by copying it to yourtheme/woocommerce/content-single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

  global $product;

  // Remove woocommerce default hooked elements
  remove_action('woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10);
  remove_action('woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20);

  remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);
  remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);
  remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
  remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
  remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);
  remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
  remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50);

  remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
  remove_action('woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15);
  remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);


  $classes = array(
    'row',
    'clearfix',
    'shop-single',
    'clearboth',
  );

  if ($product->has_child()) {
    $classes[] = 'product-has-variation';
  }

  $show = VTCore_Zeus_Init::getFeatures()->get('show.product.full');
  $show = array_filter($show);

  $imageGrids = new VTCore_Bootstrap_Grid_Column(
    VTCore_Zeus_Init::getFeatures()->get('options.products.single.image.grids')
  );

  $summaryGrids = new VTCore_Bootstrap_Grid_Column(
    VTCore_Zeus_Init::getFeatures()->get('options.products.single.summary.grids')
  );

?>

<?php

   do_action( 'woocommerce_before_single_product' );

   if ( post_password_required() ) {
      echo get_the_password_form();
      return;
   }
?>

  <section id="product-<?php the_ID(); ?>"
         <?php post_class($classes); ?>
         itemscope
         itemtype="<?php echo esc_attr(woocommerce_get_product_schema()); ?>">


    <?php if (isset($show['image']) && count($product->get_gallery_attachment_ids()) > 0) : ?>

      <div class="product-image item <?php echo esc_attr($imageGrids->getClass()); ?>">

        <?php

          // Build the product images
          $configObject = new VTCore_Zeus_Config_Options();

          $context = array(
            'data' => VTCore_Zeus_Init::getFeatures()->get('options.products.single.fotorama'),
          );

          foreach ($product->get_gallery_attachment_ids() as $id) {

            list($url, $width, $height) = wp_get_attachment_image_src($id, 'shop_single_fullscreen');

            $context['values'][] = array(
              'type' => 'image',
              'attachment_id' => $id,
              'size' => 'shop_single',
              'full' => $url,
            );

          }

          $object = new VTCore_Wordpress_Element_WpFotorama($context);

          // Add the hover zoom
          if (VTCore_Zeus_Init::getFeatures()->get('options.products.single.fotorama.hoverzoom')) {

            VTCore_Wordpress_Utility::loadAsset('jquery-elevate-zoom');
            $object->addClass('with-hoverzoom');

            foreach ($object->findChildren('type', 'img') as $image) {
              list($url, $width, $height) = wp_get_attachment_image_src($image->getContext('attachment_id'), 'shop_single_zoom');
              $image->addData('zoom-image', $url);
              $image->addData('zoom-config', VTCore_Zeus_Init::getFeatures()->get('options.products.single.elevatezoom'));
            }
          }

          // Render the markup
          $object->render();

        ?>
      </div>
    <?php endif; ?>

	<div class="product-summary item <?php echo esc_attr($summaryGrids->getClass()); ?>">

      <?php do_action( 'woocommerce_before_single_product_summary' ); ?>

      <?php if (isset($show['title'])) : ?>
        <header class="title-row">
          <div class="product-title pull-inline">
            <?php woocommerce_template_single_title(); ?>
          </div>

          <?php if (isset($show['flash']) && $product->is_on_sale()) : ?>
            <div class="product-flash pull-inline">
              <?php woocommerce_show_product_sale_flash(); ?>
            </div>
          <?php endif;?>

        </header>
      <?php endif; ?>

      <?php if (isset($show['price'])) : ?>
        <div class="product-price">
          <?php woocommerce_template_single_price(); ?>
        </div>
      <?php endif; ?>

      <?php if (isset($show['rating']) && $product->get_rating_count() > 0) : ?>
        <div class="product-rating">
          <?php woocommerce_template_single_rating(); ?>
        </div>
      <?php endif; ?>

      <?php do_action( 'woocommerce_single_product_summary' ); ?>

      <?php if (isset($show['excerpt']) && !empty($post->post_excerpt)) : ?>
        <div class="product-excerpt">
          <?php woocommerce_template_single_excerpt(); ?>
        </div>
      <?php endif; ?>

      <?php do_action( 'woocommerce_after_single_product_summary' ); ?>

      <?php if (isset($show['meta'])) : ?>
        <div class="product-metas">
          <?php woocommerce_template_single_meta(); ?>
        </div>
      <?php endif; ?>

      <?php if (isset($show['cart'])) : ?>
        <div class="product-cart">
          <?php woocommerce_template_single_add_to_cart(); ?>
        </div>
      <?php endif; ?>

      <?php if (isset($show['share'])) : ?>
        <?php do_action( 'woocommerce_share' ); ?>
      <?php endif; ?>

	</div>


	<meta itemprop="url" content="<?php the_permalink(); ?>" />

    <?php do_action( 'woocommerce_after_single_product' ); ?>
  </section>



<?php

  // Build product reviews, description and additional information
  if (isset($show['tabs'])) {
    woocommerce_output_product_data_tabs();
  }

  // Build product upsell
  if (isset($show['upsell'])) {
    woocommerce_upsell_display();
  }

  // Build product related
  if (isset($show['related'])) {
    woocommerce_output_related_products();
  }

?>