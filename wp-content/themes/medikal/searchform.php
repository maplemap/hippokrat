<?php
/**
 * Custom Search Form Templates
 *
 * @author jason.xie@victheme.com
 */
?>
<div class="search-form">
<form action="<?php echo esc_url(home_url( '/' )); ?>" method="get">
  <input type="text" name="s" class="search-field" value="<?php esc_attr(the_search_query()); ?>" placeholder="<?php echo esc_attr__('Search...', 'medikal'); ?>" />
  <button class="search-submit" type="submit"><i class="fa fa-search"></i></button>
</form>
</div>