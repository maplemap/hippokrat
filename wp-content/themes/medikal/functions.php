<?php
/**
 * Booting TGM Plugin activation
 * This has to run before anything else
 *
 * At this stage all VTCore related systems such as
 * autoloading is not present yet, thus we need to
 * load everything manually.
 *
 * @author jason.xie@victheme.com
 *
 */

  define('BASE_THEME', basename(get_template_directory()));

  define('ACTIVE_THEME', basename(get_stylesheet_directory()));

  define('VTCORE_ZEUS_THEME_PATH', get_template_directory());

  define('VTCORE_ZEUS_THEME_URL', get_template_directory_uri());

  define('VTCORE_ZEUS_HELP_URL', 'http://documentation.victheme.com/' . BASE_THEME);

  define('VTCORE_ZEUS_LIBRARY_PATH', VTCORE_ZEUS_THEME_PATH . DIRECTORY_SEPARATOR . 'library');

  define('VTCORE_ZEUS_PLUGIN_SOURCE_PATH', VTCORE_ZEUS_LIBRARY_PATH . DIRECTORY_SEPARATOR . 'plugins');

  define('VTCORE_ZEUS_DEFAULT_SCHEMA', 'default');

  define('VTCORE_PLUGIN_PATH', WP_PLUGIN_DIR . DIRECTORY_SEPARATOR . 'victheme_core' . DIRECTORY_SEPARATOR . 'victheme_core.php');

  define('VTCORE_THEME_CORE_VERSION', '1.7.0');

  define('VTCORE_WOOCOMMERCE_LOADED', class_exists('WooCommerce'));

  define('VTCORE_WPML_LOADED', defined('ICL_SITEPRESS_VERSION'));

  define('VTCORE_REVSLIDER_LOADED', class_exists('RevSliderFront'));

  define('VTCORE_WPLIKE_LOADED', function_exists('gs_lp_get_like_count'));

  define('VTCORE_POSTCOUNT_LOADED', defined('POST_VIEWS_COUNTER_PATH'));

  define('VTCORE_VC_LOADED', defined('WPB_VC_VERSION'));

  define('VTCORE_LAYERSLIDER_LOADED', defined('LS_PLUGIN_VERSION'));

  define('REV_SLIDER_AS_THEME', true);

  // Load the TGM plugin activation class manually
  require_once VTCORE_ZEUS_LIBRARY_PATH . DIRECTORY_SEPARATOR . 'class-tgm-plugin-activation.php';


  // Booting the registry class to invoke TGM plugin loading mechanism
  require_once VTCORE_ZEUS_LIBRARY_PATH . DIRECTORY_SEPARATOR . 'registry.php';

  // Add action manually to boot the registry class.
  add_action('tgmpa_register', array(new VTCore_Zeus_Library_Registry, 'registerPlugin'));


  // Hooking to wp after theme installation for
  // activating the VTCore core
  add_action('after_switch_theme', 'VTCoreZeusCheckCore');
  add_action('after_setup_theme', 'VTCoreZeusCheckCore');
  add_action('init', 'VTCoreZeusCheckCore');

  // Hook callback function
  function VTCoreZeusCheckCore($oldtheme = false) {
    // Try to activate or install VTCore if missing
    if ((defined('VTCORE_VERSION') && version_compare(VTCORE_VERSION, VTCORE_THEME_CORE_VERSION, '='))
        || ($oldtheme != false && $oldtheme == ACTIVE_THEME)) {

      return;
    }

    if (!function_exists('get_plugins')) {
      require_once ABSPATH . 'wp-admin/includes/plugin.php';
    }

    $installed_plugins = get_plugins();

    if (!isset($installed_plugins[VTCORE_PLUGIN_PATH])
        && !file_exists(VTCORE_PLUGIN_PATH)
        && is_admin()
        && current_user_can('install_plugins')) {

      require_once ABSPATH . 'wp-admin/includes/template.php';
      require_once ABSPATH . 'wp-admin/includes/misc.php';
      require_once ABSPATH . 'wp-admin/includes/file.php';
      require_once ABSPATH . 'wp-admin/includes/class-wp-upgrader.php';

      $upgrader = new WP_Upgrader();

      // Nuke messages
      $upgrader->strings['unpack_package'] = '';
      $upgrader->strings['installing_package'] = '';
      $upgrader->strings['remove_old'] = '';
      $upgrader->skin->set_upgrader($upgrader);


      // Only serves server with proper filesystem
      // installed for other method they can use
      // the TGMPlugin instead
      $access = get_filesystem_method();

      // Silently installing the core package
      if ($access == 'direct') {

        $res = $upgrader->fs_connect( array(WP_CONTENT_DIR, VTCORE_ZEUS_PLUGIN_SOURCE_PATH . DIRECTORY_SEPARATOR . 'victheme_core.zip'));
        $working_dir = $upgrader->unpack_package(VTCORE_ZEUS_PLUGIN_SOURCE_PATH . DIRECTORY_SEPARATOR . 'victheme_core.zip', false);
        $result = $upgrader->install_package(array(
          'source' => $working_dir,
          'destination' => WP_PLUGIN_DIR,
          'clear_destination' => true,
          'abort_if_destination_exists' => true,
          'clear_working' => true,
          'hook_extra' => array(),
        ));
      }
    }

    // Just activate the plugin if it is installed
    if (is_plugin_inactive(VTCORE_PLUGIN_PATH)) {
      activate_plugin(VTCORE_PLUGIN_PATH);
    }

    // Still cannot find the core, possible scenario
    // user server is not correctly configured for the
    // permission and credentials then fallback to
    // the rescue classes
    if (!(defined('VTCORE_VERSION') && version_compare(VTCORE_VERSION, VTCORE_THEME_CORE_VERSION, '=')) && !class_exists('VTCore_Init')) {

      include_once(VTCORE_ZEUS_THEME_PATH . DIRECTORY_SEPARATOR . 'rescue' . DIRECTORY_SEPARATOR . 'vtcore' . DIRECTORY_SEPARATOR . 'init.php');

      // Booting VTCore
      new VTCore_Init(array(
        'corePath' => VTCORE_ZEUS_THEME_PATH . DIRECTORY_SEPARATOR . 'rescue' . DIRECTORY_SEPARATOR . 'vtcore',
        'coreURL' => VTCORE_ZEUS_THEME_URL . '/rescue/vtcore',
      ));

      // Register the autoloader for wordpress directory
      $autoloader = new VTCore_Autoloader('VTCore_Wordpress', VTCORE_ZEUS_THEME_PATH . DIRECTORY_SEPARATOR . 'rescue' . DIRECTORY_SEPARATOR);
      $autoloader->setRemovePath('vtcore' . DIRECTORY_SEPARATOR);
      $autoloader->register();

      // Booting VTCore for WordPress
      define('VTCORE_VERSION', VTCORE_THEME_CORE_VERSION);

      new VTCore_Wordpress_Init();
    }
  }


  // First Check
  if (!(defined('VTCORE_VERSION') && version_compare(VTCORE_VERSION, VTCORE_THEME_CORE_VERSION, '='))) {
    VTCoreZeusCheckCore();
  }

  // Allows child theme to override the initialization class
  // from there on the child theme can just override everything
  // via extending the classes.
  // Booting up Zeus Theme Framework if VTCore is activated
  if ((defined('VTCORE_VERSION') && version_compare(VTCORE_VERSION, VTCORE_THEME_CORE_VERSION, '='))
      && !class_exists('VTCore_Zeus_Init')) {

    locate_template(DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'init.php', true);
    $zeus = new VTCore_Zeus_Init();

    // Workaround allowing child theme to utilize the zeus init class
    // after the class is initialized.
    do_action('vtcore_zeus_after_theme_init', $zeus);

    global $content_width;
    $content_width = VTCore_Zeus_Utility::calculateApproxContentWidth(800);

  }


  // Final check if after everything core still unavailable
  // break and die to prevent further error
  if (!(defined('VTCORE_VERSION') && version_compare(VTCORE_VERSION, VTCORE_THEME_CORE_VERSION, '='))) {

    // Add other page that will be allowed to run
    // when VTCore is not activated here.
    // For example registration page or customized wp-login page.

    if (!is_admin()) {
      $allowed = array(
        'wp-login.php',
      );

      if (!in_array($GLOBALS['pagenow'], $allowed)) {
        add_action('get_header', 'ZeusMaintenanceMode');

        // @todo expand this with template?
        function ZeusMaintenanceMode() {
          wp_die(esc_html__('Sorry, the site is under maintenance, come back later.', 'medikal'));
        }
      }
    }

    else {

      add_action('admin_notices', 'ZeusThemeNotInitialized');

      function ZeusThemeNotInitialized() {
        echo '<div class="error""><p>' . esc_html__('Theme not initialized and put to maintenance mode due to dependencies API is not high enough to run this theme version, please update plugin dependencies first', 'medikal') . '</p></div>';
      }

    }
  }
/** End of Zeus Theme Framewordk Integration, add extra code below this line **/
show_admin_bar(false);