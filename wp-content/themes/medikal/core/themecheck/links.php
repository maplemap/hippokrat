<?php
/**
 * Class VTCore_Zeus_ThemeCheck_Links
 * overriding the original theme check link
 * to skip "data" type file such as the
 * demo import data classes.
 *
 * author jason.xie@victheme.com
 */
class VTCore_Zeus_ThemeCheck_Links
extends Check_Links
implements themecheck {

  protected $error = array();
  protected $skip = array();

  function check($php_files, $css_files, $other_files) {

    $this->skip = array(
      BASE_THEME . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'demo',
      BASE_THEME . DIRECTORY_SEPARATOR . 'rescue',
    );


    // Don't Check "data" type classes!
    // The "data" type classes only used for importing feature.
    // The "rescue" type classes only used if no victheme_core found.
    foreach ($php_files as $path => $file) {
      foreach ($this->skip as $skip) {
        if (strpos($path, $skip) !== FALSE) {
          unset($php_files[$path]);
        }
      }
    }

    parent::check($php_files, $css_files, $other_files);

    return true;
  }
}