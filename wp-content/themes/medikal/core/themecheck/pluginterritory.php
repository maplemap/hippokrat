<?php
/**
 * Checks for Plugin Territory Guidelines.
 *
 * Skips the rescue folder as it is a "plugin" stored
 * to replace VicTheme_Core when it is not available
 * to avoild theme broken.
 *
 * See http://make.wordpress.org/themes/guidelines/guidelines-plugin-territory/
 */
class VTCore_Zeus_ThemeCheck_PluginTerritory
extends Plugin_Territory
implements themecheck {

  protected $error = array();
  protected $skip = array();

  function check($php_files, $css_files, $other_files) {

    $this->skip = array(
      BASE_THEME . DIRECTORY_SEPARATOR . 'rescue',
    );

    // Don't Check the Rescue Folder as it is
    // for rescuing purposes only.
    foreach ($php_files as $path => $file) {
      foreach ($this->skip as $skip) {
        if (strpos($path, $skip) !== FALSE) {
          unset($php_files[$path]);
        }
      }
    }

    return parent::check($php_files, $css_files, $other_files);
  }

}
