<?php
/**
 * Skips Rescue and Demo import folder
 */
class VTCore_Zeus_ThemeCheck_NonPrintable
extends NonPrintableCheck
implements themecheck {

  protected $error = array();
  protected $skip = array();

  function check($php_files, $css_files, $other_files) {

    $this->skip = array(
      BASE_THEME . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'visualcomposer',
      BASE_THEME . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'demo',
      BASE_THEME . DIRECTORY_SEPARATOR . 'rescue',
    );

    // Don't Check the Rescue and Demo folder as it is
    // for rescuing purposes and importing demo data only.
    foreach ($php_files as $path => $file) {
      foreach ($this->skip as $skip) {
        if (strpos($path, $skip) !== FALSE) {
          unset($php_files[$path]);
        }
      }
    }

    parent::check($php_files, $css_files, $other_files);

    return true;
  }
}
