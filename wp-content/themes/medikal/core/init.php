<?php
/**
 * Class for initializing Zeus Framework and
 * acts as the bridge between VTCore and Zeus
 * framework
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Init {

  private $autoloader;
  private static $layout;
  private static $schemas;
  private static $features;
  private static $templateSystem;
  private static $theme;
  private static $visualComposer;


  private $actions = array(
    'init',
    'widgets_init',
    'wp_before_admin_bar_render',
    'wp_enqueue_scripts',
    'admin_menu',
    'customize_register',
    'customize_preview_init',
    'customize_save',
    'vtcore_zeus_alter_features',
    'vtcore_zeus_alter_regions',
    'vtcore_zeus_alter_schema',
    'wp_ajax_color_customizer',
    'vtcore_wordpress_loop_ajax_result_alter',
    'themecheck_checks_loaded',
    'vtcore_wordpress_alter_icons_library',
  );


  private $filters = array(
    'excerpt_more',
    'get_calendar',
    'post_gallery',
    'attribute_escape',
    'cancel_comment_reply_link',
    'comment_reply_link',
    'template_include',
    'woocommerce_available_variation',
    'vtcore_services_schedule_table_header',
    'vtcore_slick_register_template_items',
    'vtcore_slick_register_template_thumb_items',
    'vtcore_slick_register_template_empty',
    'vtcore_visualloop_register_template_items',
    'vtcore_visualloop_register_template_empty',
  );



  /**
   * Constructing the main class and adding action to init.
   */
  public function __construct() {

    self::$theme = wp_get_theme();

    // Load autoloader
    $this->autoloader = new VTCore_Autoloader('VTCore_Zeus', VTCORE_ZEUS_THEME_PATH . DIRECTORY_SEPARATOR . 'core');
    $this->autoloader->setRemovePath('vtcore' . DIRECTORY_SEPARATOR . 'zeus' . DIRECTORY_SEPARATOR);
    $this->autoloader->register();


    // Registering theme supports
    add_theme_support('post-thumbnails');
    add_theme_support('automatic-feed-links').
    add_theme_support('html5', array('search-form', 'comment-form', 'comment-list'));
    add_theme_support('woocommerce');
    add_theme_support('vtcore_custom_template');
    add_theme_support('title-tag');
    // add_theme_support('bootstrap');

    // Registering default menu location
    register_nav_menu('navigation', __('Navigation Menu', 'medikal'));

    // Perform this if VisualComposer is loaded
    if (VTCORE_VC_LOADED) {
      $this->actions[] = 'vc_before_init';
      $this->actions[] = 'vc_after_mapping';
      $this->filters[] = 'vc_load_default_templates';
      $this->filters[] = 'vc_grid_item_predefined_templates';
    }

    // Perform this if headline loaded
    if (defined('VTCORE_HEADLINE_LOADED') && VTCORE_HEADLINE_LOADED) {
      $this->filters[] = 'vtcore_headline_alter';
      $this->filters[] = 'vtcore_headline_context_alter';
      $this->filters[] = 'vtcore_headline_configuration_context_alter';
      $this->filters[] = 'vtcore_headline_remove_metabox_tabs';
      $this->filters[] = 'vtcore_headline_metabox_form_alter';
      $this->actions[] = 'vtcore_headline_add_configuration_panel';
    }

    // Registering actions
    VTCore_Wordpress_Init::getFactory('actions')
      ->addPrefix('VTCore_Zeus_Actions_')
      ->addHooks($this->actions)
      ->register();


    // Registering filters
    VTCore_Wordpress_Init::getFactory('filters')
      ->addPrefix('VTCore_Zeus_Filters_')
      ->addHooks($this->filters)
      ->register();

    // Loading text domain
    load_theme_textdomain(BASE_THEME, VTCORE_ZEUS_THEME_PATH . DIRECTORY_SEPARATOR . 'languages');

    // Registering config
    self::setStaticSystem('features', new VTCore_Zeus_Config_Options());

    // Registering regions
    if (class_exists('VTCore_Wordpress_Factory_Layout', true)) {
      $areas = new VTCore_Zeus_Config_Areas();
      $regions = new VTCore_Zeus_Config_Regions();
      $layout = new VTCore_Wordpress_Factory_Layout(array(
        'areas' => $areas->extract(),
        'regions' => $regions->extract(),
      ));

      self::setStaticSystem('layout', $layout);
    }


    // Registering schemas
    self::setStaticSystem('schemas', new VTCore_Zeus_Schema_Factory());

    // Setup the VTCore asset factory
    VTCore_Wordpress_Init::getFactory('assets')
      ->add('aggregate', VTCore_Zeus_Init::getFeatures()->get('options.performance.aggregate'))
      ->add('minify', VTCore_Zeus_Init::getFeatures()->get('options.performance.minify'))
      ->get('library')
      ->detect(VTCORE_ZEUS_THEME_PATH . DIRECTORY_SEPARATOR . 'assets', get_template_directory_uri() . '/assets');


    // Registering custom templating system
    VTCore_Wordpress_Init::getFactory('template')
      ->register(VTCORE_ZEUS_THEME_PATH . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'custom', 'templates' . DIRECTORY_SEPARATOR . 'custom')
      ->register(VTCORE_ZEUS_THEME_PATH . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'wploop', 'templates' . DIRECTORY_SEPARATOR . 'wploop');

    // Booting visualComposer integration system
    if (function_exists('vc_set_as_theme')) {
      self::setStaticSystem('visualComposer', new VTCore_Zeus_VisualComposer_Init());
    }

    global $revSliderAsTheme;
    $revSliderAsTheme = true;

  }

  /**
   * Allow other class to get the layout factory object
   * @return VTCore_Zeus_Layout_Factory
   */
  public static function getLayout() {
    return self::getStaticSystem('layout');
  }



  /**
   * Allow other class to get the schema factory object
   * @return VTCore_Zeus_Schema_Factory
   */
  public static function getSchema() {
    return self::getStaticSystem('schemas');
  }



  /**
   * Allow other class to get the features system object
   * @return VTCore_Zeus_Features
   */
  public static function getFeatures() {
    return self::getStaticSystem('features');
  }


  /**
   * Retrieving theme information
   */
  public static function getThemeInfo($key) {
    return self::$theme->get($key);
  }


  /**
   * Populate the static system with content (or object)
   */
  public static function setStaticSystem($system, $content) {
    self::${$system} = $content;
  }


  /**
   * Retrieve the static systems.
   */
  public static function getStaticSystem($system) {
    return self::${$system};
  }

}