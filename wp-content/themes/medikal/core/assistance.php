<?php
/**
 * Class for registering the help pages url
 *
 * Entries should have :
 *   title    :  The link title
 *   url      :  The link url
 *   target   :  The link target
 *   type     :  The link type
 *
 *
 * @available constant :
 *     VTCORE_ZEUS_HELP_URL for the theme main help url.
 *
 * @author jason.xie@victheme.com
 * @used in theme options help button and help panels
 *
 */
class VTCore_Zeus_Assistance {

  protected $context;

  public function __construct() {

    $this->context = array(

      'documentation' => array(
        'title' => __('Read Theme Documentation', 'medikal'),
        'url' => 'http://documentation.victheme.com/' . BASE_THEME . '/index.html',
        'target' => '_blank',
        'type' => 'link',
      ),


      /** General Page */

      'logo' => array(
        'title' => __('How to add or change Logo & Slogan', 'medikal'),
        'url' => 'http://vimeo.com/108890352',
        'target' => '_blank',
        'type' => 'video',
      ),

      'favicon' => array(
        'title' => __('How to add or change favicon', 'medikal'),
        'url' => 'http://vimeo.com/108890075',
        'target' => '_blank',
        'type' => 'video',
      ),

      'header_options' => array(
        'title' => __('How to enable slick menu', 'medikal'),
        'url' => 'http://vimeo.com/98117247',
        'target' => '_blank',
        'type' => 'video',
      ),

      'header_options_parent_link' => array(
        'title' => __('How to disable parent link for menu', 'medikal'),
        'url' => 'http://vimeo.com/98125334',
        'target' => '_blank',
        'type' => 'video',
      ),

      'header_options_sticky_header' => array(
        'title' => __('How to enable header sticky menu navigation', 'medikal'),
        'url' => 'http://vimeo.com/117276483',
        'target' => '_blank',
        'type' => 'video',
      ),

      'teasers_options' => array(
        'title' => __('How to set teasers option', 'medikal'),
        'url' => 'http://vimeo.com/125385205',
        'target' => '_blank',
        'type' => 'video',
      ),

      'page_loading' => array(
        'title' => __('How to configure page loading animation', 'medikal'),
        'url' => 'http://vimeo.com/117277088',
        'target' => '_blank',
        'type' => 'video',
      ),

      'post_single_options' => array(
        'title' => __('how to configure single post element', 'medikal'),
        'url' => 'http://vimeo.com/117396109',
        'target' => '_blank',
        'type' => 'video',
      ),

      'page_single_options' => array(
        'title' => __('how to configure single page element', 'medikal'),
        'url' => 'http://vimeo.com/117400136',
        'target' => '_blank',
        'type' => 'video',
      ),

      'footer_element' => array(
        'title' => __('how to configure footer element', 'medikal'),
        'url' => 'http://vimeo.com/117389970',
        'target' => '_blank',
        'type' => 'video',
      ),

      'footer_element_copyright' => array(
        'title' => __('how to change copyright notice', 'medikal'),
        'url' => 'http://vimeo.com/117396092',
        'target' => '_blank',
        'type' => 'video',
      ),

      '404_page_element' => array(
        'title' => __('how to configure error 404 page', 'medikal'),
        'url' => 'http://vimeo.com/97198351',
        'target' => '_blank',
        'type' => 'video',
      ),

      '404_page_element_text' => array(
        'title' => __('how to configure error 404 page', 'medikal'),
        'url' => 'http://vimeo.com/97198352',
        'target' => '_blank',
        'type' => 'video',
      ),

      'performance' => array(
        'title' => __('how to enable/disable performance', 'medikal'),
        'url' => 'http://vimeo.com/126331717',
        'target' => '_blank',
        'type' => 'video',
      ),

      'customizer' => array(
        'title' => __('how to configure customizer settings', 'medikal'),
        'url' => 'http://vimeo.com/134165043',
        'target' => '_blank',
        'type' => 'video',
      ),

      'page_layout' => array(
        'title' => __('how to configure page style', 'medikal'),
        'url' => 'http://vimeo.com/97197361',
        'target' => '_blank',
        'type' => 'video',
      ),

      'page_layout_responsive' => array(
        'title' => __('how to set responsiveness', 'medikal'),
        'url' => 'http://vimeo.com/100229182',
        'target' => '_blank',
        'type' => 'video',
      ),

      'sidebar_position_blog' => array(
        'title' => __('how to change sidebar position_blog listing', 'medikal'),
        'url' => 'http://vimeo.com/117400163',
        'target' => '_blank',
        'type' => 'video',
      ),

      'sidebar_position_page_404' => array(
        'title' => __('how to change sidebar position_page 404', 'medikal'),
        'url' => 'http://vimeo.com/97197362',
        'target' => '_blank',
        'type' => 'video',
      ),

      'header_column_grid' => array(
        'title' => __('how to configure header column layout', 'medikal'),
        'url' => 'http://vimeo.com/97197650',
        'target' => '_blank',
        'type' => 'video',
      ),

      'main_column_grid' => array(
        'title' => __('how to configure main content column layout', 'medikal'),
        'url' => 'http://vimeo.com/97197652',
        'target' => '_blank',
        'type' => 'video',
      ),

      'postface_column_grid' => array(
        'title' => __('how to configure footer column grid', 'medikal'),
        'url' => 'http://vimeo.com/97197648',
        'target' => '_blank',
        'type' => 'video',
      ),

      'footer_column_grid' => array(
        'title' => __('how to configure full footer menu', 'medikal'),
        'url' => 'http://vimeo.com/97197649',
        'target' => '_blank',
        'type' => 'video',
      ),

      'color_schema' => array(
        'title' => __('how to change color schema', 'medikal'),
        'url' => 'http://vimeo.com/98295905',
        'target' => '_blank',
        'type' => 'video',
      ),

      'customize_schema' => array(
        'title' => __('how to customize color schema', 'medikal'),
        'url' => 'http://vimeo.com/97198353',
        'target' => '_blank',
        'type' => 'video',
      ),
      'product_teaser_options' => array(
        'title' => __('how to configure the pagination for the catalog grid', 'medikal'),
        'url' => 'http://vimeo.com/134272214',
        'target' => '_blank',
        'type' => 'video',
      ),
      'product_single_options' => array(
        'title' => __('how to enable or disable product zooming', 'medikal'),
        'url' => 'http://vimeo.com/134272498',
        'target' => '_blank',
        'type' => 'video',
      ),
      'cart_page_settings' => array(
        'title' => __('how to customize shop header', 'medikal'),
        'url' => 'http://vimeo.com/126728554',
        'target' => '_blank',
        'type' => 'video',
      ),
      'create_post' => array(
        'title' => __('How to create post', 'medikal'),
        'url' => 'http://youtu.be/CIKSJP3aeQ4',
        'target' => '_blank',
        'type' => 'video',
      ),
      'create_member' => array(
        'title' => __('How to create and edit members', 'medikal'),
        'url' => 'http://youtu.be/KVf5OMpJwq4',
        'target' => '_blank',
        'type' => 'video',
      ),
      'create_department' => array(
        'title' => __('How to create and edit Department', 'medikal'),
        'url' => 'http://youtu.be/wU9ryOJVjsM',
        'target' => '_blank',
        'type' => 'video',
      ),

      'create_services' => array(
        'title' => __('How to create and edit Services', 'medikal'),
        'url' => 'http://youtu.be/o9Krt0WjiNY',
        'target' => '_blank',
        'type' => 'video',
      ),
      'create_product' => array(
        'title' => __('How to add product for Shop', 'medikal'),
        'url' => 'http://youtu.be/XD20RSpHRvk',
        'target' => '_blank',
        'type' => 'video',
      ),
      'department_archive' => array(
        'title' => __('How to configure Department Archive page', 'medikal'),
        'url' => 'http://youtu.be/TrVveRwDPO0',
        'target' => '_blank',
        'type' => 'video',
      ),
      'timetable_page' => array(
        'title' => __('How to configure timetable page options', 'medikal'),
        'url' => 'http://youtu.be/P4cruZXv_qA',
        'target' => '_blank',
        'type' => 'video',
      ),
      'timetable_shift' => array(
        'title' => __('How to configure timetable shift schedule', 'medikal'),
        'url' => 'http://youtu.be/L7UjW94Qx58',
        'target' => '_blank',
        'type' => 'video',
      ),
      'import_demo' => array(
        'title' => __('How to import demo contents', 'medikal'),
        'url' => 'http://youtu.be/-0I0hEZy_YM',
        'target' => '_blank',
        'type' => 'video',
      ),
    );

  }


  public function getAssistanceItems() {
    return $this->context;
  }

  public function getAssistanceItem($type, $key) {
    return isset($this->context[$type][$key]) ? $this->context[$type][$key] : false;
  }

}