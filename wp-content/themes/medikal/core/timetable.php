<?php
/**
 * Simple Class for helping in building the
 * timetable for services + department + members
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Zeus_TimeTable
extends VTCore_Wordpress_Models_Dot {

  static protected $cache = array();

  /**
   * Overriding parent method
   * @param array $options
   */
  public function __construct($options = array()) {

    $this->options = array(
      'shifts' => array(
        'morning' => array(
          'enabled' => true,
          'title' => __('Morning Shift', 'medikal'),
          'start' => '08:00AM',
          'end' => '04:00PM',
        ),
        'afternoon' => array(
          'enabled' => true,
          'title' => __('Afternoon Shift', 'medikal'),
          'start' => '04:00PM',
          'end' => '01:00AM',
        ),
        'night' => array(
          'enabled' => true,
          'title' => __('Night Shift', 'medikal'),
          'start' => '01:00AM',
          'end' => '8:00AM'
        ),
      ),
      'calendar' => array(
        'start' => 'last monday',
        'end' => '+7 days',
        'interval' => 'P1W1D',
        'format' => 'F jS, Y',
      ),
      'days' => array(
        'monday' => __('Monday', 'medikal'),
        'tuesday' => __('Tuesday', 'medikal'),
        'wednesday' => __('Wednesday', 'medikal'),
        'thursday' => __('Thursday', 'medikal'),
        'friday' => __('Friday', 'medikal'),
        'saturday' => __('Saturday', 'medikal'),
        'sunday' => __('Sunday', 'medikal'),
      ),
      'available' => array(
        'services' => defined('VTCORE_SERVICES_LOADED'),
        'department' => defined('VTCORE_DEPARTMENT_LOADED'),
        'members' => defined('VTCORE_MEMBERS_LOADED'),
      )
    );

    // Inject the configuration form array here!
    $this->merge($options);


    // Load Available services
    if ($this->get('available.services')) {
      if (!isset(self::$cache['services'])) {
        self::$cache['services'] = get_posts(array(
          'post_type' => 'services',
          'post_status' => 'publish',
          'posts_per_page' => 1000,
        ));
      }

      foreach (self::$cache['services'] as $post) {
        $this->add('services.' . $post->ID, new VTCore_Services_Entity(array('post' => $post)));
      }
    }

    // Load available department
    if ($this->get('available.department')) {
      if (!isset(self::$cache['department'])) {
        self::$cache['department'] = get_posts(array(
          'post_type' => 'department',
          'post_status' => 'publish',
          'posts_per_page' => 1000,
        ));
      }

      foreach (self::$cache['department'] as $post) {
        $this->add('department.' . $post->ID, new VTCore_Department_Entity(array('post' => $post)));
      }
    }

    // Build calendar structure
    $begin = new DateTime($this->get('calendar.start'));
    $end = clone($begin);
    $end->modify($this->get('calendar.end'));
    $interval = new DateInterval($this->get('calendar.interval'));
    $this
      ->add('calendar.start', $begin->getTimestamp())
      ->add('calendar.end', $end->getTimestamp())
      ->add('calendar.period', new DatePeriod($begin, $interval, $end));

  }


  /**
   * Method for clearing static cache
   * Use this when data has changed or when you need
   * a fresh data taken from database.
   *
   * @return $this
   */
  public function clearCache() {
    self::$cache = array();
    return $this;
  }



  /**
   * Retrieves all available shifts
   * all shifts will be returned as an object.
   *
   * @return mixed
   */
  public function getShifts() {

    if (!isset(self::$cache['shifts'])) {
      $shifts = array();

      foreach ($this->get('shifts') as $delta => $value) {

        $object = new VTCore_Wordpress_Objects_Array($value);

        if ($object->get('enabled')) {
          $object->add('id', $delta);
          $shifts[] = $object;
        }
      }

      self::$cache['shifts'] = $shifts;
    }

    return self::$cache['shifts'];
  }



  /**
   * Retrieves all available days
   * all days will be returned as an object.
   *
   * @return mixed
   */
  public function getDays() {

    if (!isset(self::$cache['days'])) {

      $days = array();

      foreach ($this->get('calendar.period') as $time) {
        $object = new VTCore_Wordpress_Objects_Array(array(
          'id' => strtolower($time->format('l')),
          'timestamp' => $time->getTimeStamp(),
          'formatted' => $time->format($this->get('calendar.format')),
          'iso' => $time->format('Y-m-d\TH:i:sO'),
          'label' => $this->get('days.' . strtolower($time->format('l'))),
        ));

        $days[] = $object;
      }

      self::$cache['days'] = $days;
    }

    return self::$cache['days'];
  }


  /**
   * Retrieves valid schedule for x days + x shift
   * all schedules will be returned as object
   *
   * @param $day
   *  object of days, preferably returned from the getDays() method
   * @param $shift
   *  object of shifts, preferably returned from getShift() method
   * @return mixed
   */
  public function getSchedule($day, $shift) {

    if (!is_a($day, 'VTCore_Wordpress_Objects_Array')
        || !is_a($shift, 'VTCore_Wordpress_Objects_Array')
        || !$this->get('available.services')) {
      return array();
    }

    if (!isset(self::$cache['schedule'][$day->get('id')][$shift->get('start')][$shift->get('end')])) {
      $services = array();
      foreach ($this->get('services') as $id => $serviceObject) {
        foreach ($serviceObject->get('data.schedule') as $delta => $schedule) {

          $object = new VTCore_Wordpress_Objects_Array($schedule);

          // Test if we are on the right shift and right days
          $start = (int) date('Hi', strtotime($object->get('start')));
          if (in_array($day->get('id'), $object->get('days'))
              && $start >= (int) date('Hi', strtotime($shift->get('start')))) {
              // && $start -1 <= (int) date('Hi', strtotime($shift->get('end')))) {

            $object
              ->add('service', (int) $serviceObject->get('post.ID'))
              ->add('department', (int) $serviceObject->get('data.department'));

            foreach ($object->get('users') as $delta => $userid) {
              $object->add('users.' . $delta, $serviceObject->getUser($userid));
            }

            $services[$id] = $object;
          }

        }
      }

      self::$cache['schedule'][$day->get('id')][$shift->get('start')][$shift->get('end')] = $services;
    }

    return self::$cache['schedule'][$day->get('id')][$shift->get('start')][$shift->get('end')];
  }

}