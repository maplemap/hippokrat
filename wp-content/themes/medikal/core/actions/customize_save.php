<?php
/**
 * Class for filtering customize_save
 * and processing the save function
 *
 * @author jason.xie@victheme.com
 * @see VTCore_Zeus_Actions_VTCore__Zeus__Alter__Regions
 * @see VTCore_Zeus_Actions_VTCore__Zeus__Alter__Features
 * @see VTCore_Zeus_Actions_VTCore__Zeus__Alter__Schema
 *
 */
class VTCore_Zeus_Actions_Customize__Save
extends VTCore_Wordpress_Models_Hook {

  protected $weight = 99;
  private $data = array();
  private $schemas;
  private $schema = array();
  private $active;
  private $config;

  public function hook($WP_Customizer = NULL) {

    // Process the customizer data
    foreach ($WP_Customizer->settings() as $key => $settings) {

      // Get value directly from manager instead of settings
      $value = $WP_Customizer->post_value($settings, 'nosave');

      // Check if we got actual post value
      if ($value == 'nosave' || $settings->type == 'nosave') {
        $WP_Customizer->remove_setting($key);
        continue;
      }

      if (strpos($settings->id, '[background-gradient]') !== false) {
        $data = json_decode($value);

        foreach ($data as $object) {
          $val = str_replace('[gradient]', $settings->id, $object->name) . '=' . $object->value;
          $this->data[] = str_replace('[gradient]', $settings->id, $object->name) . '=' . $object->value;
        }
        $WP_Customizer->remove_setting($key);
      }

      // Process Color
      elseif (strpos($settings->id, 'schemas[color') !== false) {
        $this->data[] = $settings->id . '=' . $value;
        $WP_Customizer->remove_setting($key);
      }

      // Get active schema
      elseif ($settings->id == 'schema-active') {
        $this->active = $value;
      }
    }

    // Bug fix schema wont save if user doesnt select active schema
    if (!empty($this->data) && empty($this->active)) {
      $this->active = get_theme_mod('schema-active', VTCORE_ZEUS_DEFAULT_SCHEMA);
    }

    // Convert the post data into multidimensional array
    parse_str(implode('&', $this->data), $this->data);

    // Only proceed if we got valid data
    if (!empty($this->data) && !empty($this->active)) {

      // Retrieving default schemas
      $this->schemas = new VTCore_Zeus_Schema_Factory();

      // Merging schemas
      foreach ($this->schemas->getSchemas() as $schema) {

        // Recording the stored schemas back into database
        // so user wont ever lose configured schema data
        $this->schema[$schema->getSchema('id')]['color'] = $schema->getData('color');
      }

      foreach ($this->data['schemas'] as $type => $section) {
        foreach ($section as $sectionKey => $datas) {
          foreach ($datas as $key => $value) {
            $this->schema[$this->active][$type][$sectionKey][$key] = $value;
          }
        }
      }

      // Save schema to database
      set_theme_mod('schemas', $this->schema);

      // Clear cached schemas
      $this->schemas->clearCache();

      // Clear vtcore cache
      VTCore_Wordpress_Init::getFactory('assets')
        ->mutate('prefix', 'comp-front-')
        ->clearCache()
        ->mutate('prefix', 'comp-admin-');
    }

  }
}