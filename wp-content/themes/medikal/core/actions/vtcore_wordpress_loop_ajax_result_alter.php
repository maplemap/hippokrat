<?php
/**
 * Class for filtering vtcore_wordpress_loop_ajax_result_alter
 *
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Actions_VTCore__Wordpress__Loop__Ajax__Result__Alter
  extends VTCore_Wordpress_Models_Hook {

  protected $argument = 2;

  public function hook($render = NULL, $ajax = NULL) {
    $context = $ajax->get('context');

    if (isset($context['custom']) && is_array($context['custom'])) {
      foreach ($context['custom'] as $mode) {
        switch ($mode) {
          case 'woocommerce_update_count' :

            // Replacing temporarily global query
            // This is required for result-count.php to work properly
            global $wp_query;
            $oldQuery = $wp_query;
            $wp_query = $ajax->get('loop')->getContext('query');

            ob_start();
            woocommerce_result_count();
            $content = ob_get_clean();

            $wp_query = $oldQuery;

            $ajax->addRender('action', array(
              'mode' => 'replace',
              'target' => '.woocommerce-result-count',
              'content' => $content,
            ));
            break;
        }
      }
    }


  }
}