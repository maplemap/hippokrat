<?php
/**
 * Hooking into Wordpress widgets_init action
 * This is required for registering dynamic
 * sidebar into wordpress.
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Actions_Widgets__Init
extends VTCore_Wordpress_Models_Hook {

  public function hook() {

    // Get sidebars and register them to Wordpress
    foreach (VTCore_Zeus_Init::getLayout()->getContext('regions') as $name => $context) {
      $object = new VTCore_Wordpress_Layout_Region($context);
      if ($object->getContext('mode') == 'dynamic') {
        register_sidebar($object->getContext('arguments'));
      }
    }

  }
}