<?php
/**
 * Class for filtering customize_controls_print_scripts
 * and adding extra class to it.
 *
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Actions_Customize__Preview__Init
extends VTCore_Wordpress_Models_Hook {

  public function hook() {
    VTCore_Wordpress_Utility::loadAsset('wp-gradientpicker');
    VTCore_Wordpress_Utility::loadAsset('jquery-deparam');
    VTCore_Wordpress_Utility::loadAsset('bootstrap-grids');
    VTCore_Wordpress_Utility::loadAsset('customizer');
  }
}