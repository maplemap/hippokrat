<?php
/**
 * Hooking into VTCore Headline Plugin
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Zeus_Actions_VTCore__Headline__Add__Configuration__Panel
extends VTCore_Wordpress_Models_Hook {

  public function hook($headline = NULL) {

    // Integration to woocommerce
    if (VTCORE_WOOCOMMERCE_LOADED) {
      $headline->addPanel('product_archive');
      $headline->addPanel('shop_cart');
      $headline->addPanel('shop_account');
      $headline->addPanel('shop_checkout');
    }
  }
}