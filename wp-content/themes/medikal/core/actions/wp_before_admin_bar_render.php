<?php
/**
 * Class for filtering wp_before_admin_bar_render
 * and adding extra class to it.
 *
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Actions_Wp__Before__Admin__Bar__Render
extends VTCore_Wordpress_Models_Hook {

  public function hook() {
    global $wp_admin_bar;

    $wp_admin_bar->add_menu(array(
      'id' => 'generaloptions',
      'title' => __('General', 'medikal'),
      'href' => admin_url() . 'themes.php?page=zeus_general_options',
      'parent' => 'appearance',
    ));

    $wp_admin_bar->add_menu(array(
      'id' => 'styleoptions',
      'title' => __('Style', 'medikal'),
      'href' => admin_url() . 'themes.php?page=zeus_schema_options',
      'parent' => 'appearance',
    ));

    $wp_admin_bar->add_menu(array(
      'id' => 'layoutoptions',
      'title' => __('Layout', 'medikal'),
      'href' => admin_url() . 'themes.php?page=zeus_layout_options',
      'parent' => 'appearance',
    ));

    if (VTCORE_WOOCOMMERCE_LOADED) {
      $wp_admin_bar->add_menu(array(
        'id' => 'shopoptions',
        'title' => __('Shop', 'medikal'),
        'href' => admin_url() . 'themes.php?page=zeus_shop_options',
        'parent' => 'appearance',
      ));
    }

  }
}