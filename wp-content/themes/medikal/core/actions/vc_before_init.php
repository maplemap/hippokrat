<?php
/**
 * Class for hooking to init action
 * and adding extra class to it.
 *
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Actions_VC__Before__Init
extends VTCore_Wordpress_Models_Hook {

  public function hook() {
    VTCore_Wordpress_Utility::loadAsset('vc-front');
    VTCore_Zeus_Init::getStaticSystem('visualComposer')->registerTheme();
  }
}