<?php
/**
 * Class for adding the medikal icon to the icon library
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Actions_VTCore__Wordpress__Alter__Icons__Library
extends VTCore_Wordpress_Models_Hook {

  public function hook($object = NULL) {
    $object->add('medikal', array(
      'family' => 'medikal',
      'text' => __('Medical & Health', 'medikal'),
      'version' => '',
      'asset' => 'icon-medikal',
      'iconset' => '22,2425,2426,2427,2428,2429,2430,a8,alarm21,ambulance10,ambulance11,apple29,apple30,apple31,apple33,apple34,asterisk3,baby106,ball17,band5,band6,bar13,beaker3,bed9,bicycle9,blood3,blood4,blood5,blood6,blood7,caduceus2,calendar58,calls,cardiac,catholic,cell6,circle44,clinic,clock39,conversations,cross53,curved11,dentist4,dentist5,dentist6,dentist7,dentist8,dentist9,diet,disease,dna6,drop11,drop13,dropper7,drug1,drugs1,drugs2,drugs3,drug,dumbbell15,ear2,energy11,eye64,eye70,female126,first18,first20,flag29,flask4,fork18,frequency,glass11,hand66,handicapped,health1,health2,healthy3,healthy4,heart105,heart106,heart107,heart112,heart115,heartbeats,heartbeat,helicopter18,herbal,honeycomb,hospital5,hospital6,hospital7,hospital8,hospital9,human81,human82,immunity,increasing8,inhaler,injection1,intravenous,knife10,leaf33,lifeline4,lifeline5,liquid3,liver2,male152,male153,masculine,medical17,medical18,medical19,medical20,medical21,medical22,medical23,medical24,medical25,medical26,medical27,medical28,medical29,medical30,medical31,medical32,medical33,medical34,medical35,medicine20,medicine21,medicine22,medicine23,medicine24,medicine25,medicine26,medicine27,medicine28,medicines1,medicines2,medicines3,mortar1,mortar2,music166,natural4,no39,nurse4,oxygen1,pharmaceutical1,pharmaceutical,pharmacy4,pharmacy5,pharmacy6,plaster,plus29,plus31,pressuring,radish,red1,right37,scissor7,searching21,shield53,skull14,smile2,sparkling,speech63,sperms1,spheres,spoon8,stethoscope8,stick8,syringe12,syringe13,tablet50,tape1,telephone76,test10,test7,test8,test9,thermometer26,three70,three71,time8,time9,timer25,tooth10,tooth11,tooth12,tooth13,tooth14,tooth8,tooth9,umbrella18,vegetarian1,wall16,water31,wheelchair3,wineglass,zzz',
      'base' => 'medikal',
      'prefix' => 'medikal-',
      'element' => 'i'
    ));
  }
}