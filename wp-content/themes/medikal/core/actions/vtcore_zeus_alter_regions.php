<?php
/**
 * Class for filtering vtcore_zeus_alter_regions
 * and adding extra class to it.
 *
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Actions_VTCore__Zeus__Alter__Regions
extends VTCore_Wordpress_Models_Hook {

  public function hook($object = NULL) {

    if (current_user_can('edit_theme_options')
        && isset($_POST['wp_customize'])
        && isset($_POST['customized'])
        && isset($_POST['theme'])
        && $_POST['wp_customize'] == 'on'
        && !empty($_POST['customized'])
        && $_POST['theme'] == ACTIVE_THEME) {


      // This is somehow needed to get sane
      // json format!.
      $post = filter_input_array(INPUT_POST);
      $customized = (array) json_decode($post['customized']);


      // Use the dotted array notation
      // for modifying the config object.
      // this can only be used if the theme
      // features utilizes VTCore_Wordpress_Models_Config
      foreach ($customized as $key => $value) {
        if (strpos($key, 'regions[') !== false) {
          $objectKeys = str_replace(array('regions[', '][', '[', ']'), array('', '.', '.', ''), $key);
          $object->add($objectKeys, $value);

          $save = true;
        }
      }

      // Invoke resaving here to fix chicken and egg
      // bug when saving via customizer
      if (isset($save)) {

        // Clear the grid cache
        // Call database directly instead of using factory
        // simply because factory is not initialized yet
        delete_transient('vtcore_layout_cache');

        $object->save();
      }
    }
  }
}