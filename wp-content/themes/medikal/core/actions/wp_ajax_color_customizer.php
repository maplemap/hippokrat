<?php
/**
 * Class for filtering wp_ajax_color_customizer
 * and adding extra class to it.
 *
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Actions_Wp__Ajax__Color__Customizer
extends VTCore_Wordpress_Models_Hook {

  public function hook() {
    $schema = new VTCore_Zeus_Schema_Factory();
    die(json_encode($schema->getSchema($_POST['colorSelects'])->getData('color')));
  }
}