<?php
/**
 * Hooking to vc_after_mapping hooks
 * for injecting additional visual
 * composer maps.
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Actions_VC__After__Mapping
extends VTCore_Wordpress_Models_Hook {

  public function hook() {

    // Inject the additional parameter to vc mapping system
    if (function_exists('vc_set_as_theme')) {
      VTCore_Zeus_Init::getStaticSystem('visualComposer')->addParams();
      VTCore_Zeus_Init::getStaticSystem('visualComposer')->alterParams();
    }

  }
}