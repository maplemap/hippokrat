<?php
/**
 * Class for filtering vtcore_zeus_alter_schema
 * and inject the pre saved data to config object
 *
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Actions_VTCore__Zeus__Alter__Schema
extends VTCore_Wordpress_Models_Hook {

  public function hook($object = NULL) {

    if (current_user_can('edit_theme_options')
        && isset($_POST['wp_customize'])
        && isset($_POST['customized'])
        && $_POST['wp_customize'] == 'on'
        && !empty($_POST['customized'])) {


      // This is somehow needed to get sane
      // json format!.
      $post = filter_input_array(INPUT_POST);
      $customized = (array) json_decode($post['customized']);


      // Explode the serialized array into
      // real arrays first!.
      $arrays = array();
      foreach ($customized as $key => $value) {
        if (is_string($value) || is_bool($value)) {
          $text = $key . '=' . (string) $value;
          $array = wp_parse_args($text, array());
          $arrays = array_merge_recursive($arrays, $array);
        }
      }

      if (isset($arrays['schema-active'])) {
        $object->setActiveSchemaID($arrays['schema-active']);
      }

      // Inject the customizer temp data to features
      if (isset($arrays['schemas']['color'])) {
        foreach ($arrays['schemas']['color'] as $type => $data) {
          foreach ($data as $key => $value) {
            $object->setStyle($type, $key, $value);
          }
        }
      }
    }
  }
}