<?php
/**
 * Class for filtering vtcore_zeus_alter_features
 * and adding extra class to it.
 *
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Actions_VTCore__Zeus__Alter__Features
extends VTCore_Wordpress_Models_Hook {

  public function hook($object = NULL) {

    if (current_user_can('edit_theme_options')
        && isset($_POST['wp_customize'])
        && isset($_POST['customized'])
        && isset($_POST['theme'])
        && $_POST['wp_customize'] == 'on'
        && !empty($_POST['customized'])
        && $_POST['theme'] == ACTIVE_THEME) {


      // This is somehow needed to get sane
      // json format!.
      $post = filter_input_array(INPUT_POST);
      $customized = (array) json_decode($post['customized']);
      $save = false;

      // Use the dotted array notation
      // for modifying the config object.
      // this can only be used if the theme
      // features utilizes VTCore_Wordpress_Models_Config
      foreach ($customized as $key => $value) {
        if (strpos($key, 'features[') !== false) {
          $objectKeys = str_replace(array('[', ']'), array('.', ''), str_replace('features[', '', $key));
          $object->add($objectKeys, $value);
          $save = true;
        }
      }

      // Save schema to database
      if ($save) {

        // Not sure why but calling $object->save() doesn't work!
        set_theme_mod('features', $object->extract());

        $customizer = new VTCore_Zeus_Customizer_General();
        $customizer->clearGlobalCache();
      }
    }
  }
}