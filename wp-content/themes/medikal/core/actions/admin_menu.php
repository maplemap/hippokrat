<?php
/**
 * Link to Wordpress Action admin_menu for
 * registering Zeus Administration menus.
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Actions_Admin__Menu
extends VTCore_Wordpress_Models_Hook {

  public function hook() {

    // Theme general configuration
    add_theme_page(
      'General Options',
      'General',
      'edit_theme_options',
      'zeus_general_options',
      array(new VTCore_Zeus_Pages_General(), 'buildPage')
    );


    // Theme schema configuration
    add_theme_page(
      'Style Options',
      'Style',
      'edit_theme_options',
      'zeus_schema_options',
      array(new VTCore_Zeus_Pages_Style(), 'buildPage')
    );

    // Theme layout configuration
    add_theme_page(
      'Layout Options',
      'Layout',
      'edit_theme_options',
      'zeus_layout_options',
      array(new VTCore_Zeus_Pages_Layout(), 'buildPage')
    );

    // Theme timetable configuration
    if (defined('VTCORE_MEMBERS_LOADED')
        && defined('VTCORE_SERVICES_LOADED')
        && defined('VTCORE_DEPARTMENT_LOADED')) {
      add_theme_page(
        'Timetable Options',
        'Timetable',
        'edit_theme_options',
        'zeus_timetable_options',
        array(new VTCore_Zeus_Pages_TimeTable(), 'buildPage')
      );
    }


    if (VTCORE_WOOCOMMERCE_LOADED) {
      // Custom css configuration page
      add_theme_page(
        'Shop Options',
        'Shop',
        'edit_theme_options',
        'zeus_shop_options',
        array(new VTCore_Zeus_Pages_Shop(), 'buildPage')
      );
    }

    if (defined('VTCORE_DEPARTMENT_LOADED')) {
      // Custom css configuration page
      add_theme_page(
        'Department Options',
        'Department',
        'edit_theme_options',
        'zeus_department_options',
        array(new VTCore_Zeus_Pages_Department(), 'buildPage')
      );
    }

    if (defined('VTCORE_SERVICES_LOADED')) {
      // Custom css configuration page
      add_theme_page(
        'Services Options',
        'Services',
        'edit_theme_options',
        'zeus_services_options',
        array(new VTCore_Zeus_Pages_Services(), 'buildPage')
      );
    }

    if (defined('VTCORE_MEMBERS_LOADED')) {
      // Custom css configuration page
      add_theme_page(
        'Members Options',
        'Members',
        'edit_theme_options',
        'zeus_members_options',
        array(new VTCore_Zeus_Pages_Members(), 'buildPage')
      );
    }

    // Custom css configuration page
    add_theme_page(
      'Custom CSS',
      'Custom CSS',
      'edit_theme_options',
      'custom_css',
      array(new VTCore_Zeus_Pages_CSS(), 'buildPage')
    );

    // Workaround against theme check not enough time
    // to process bug.
    if (isset($_POST['themename'])
        && VTCore_Wordpress_Utility::checkCurrentPage(array('themes.php'))
        && isset($_GET['page'])
        && $_GET['page'] == 'themecheck') {

      set_time_limit(-1);
    }
  }
}