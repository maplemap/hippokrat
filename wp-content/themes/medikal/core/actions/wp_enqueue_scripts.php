<?php
/**
 * Class for invoking wp_enqueue_scripts action
 * in a single place.
 *
 * This is done this way so themer can easily
 * queue scripts and styles.
 *
 * The ordering of calling loadAsset or wp_enqueue_script
 * will also reorder the queueing of the particular styles
 * providing no other plugin or modules altering the queue
 * after this class.
 *
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Actions_Wp__Enqueue__Scripts
extends VTCore_Wordpress_Models_Hook {

  public function hook() {

    // Core scripts, cannot be disabled!
    $enqueueArgs = array(
      'deps' => 'jquery',
      'footer' => true,
    );

    VTCore_Wordpress_init::getFactory('assets')
      ->get('queues')
      ->add('jquery-imgliquid', $enqueueArgs)
      ->add('jquery-easing', $enqueueArgs)
      ->add('bootstrap', $enqueueArgs)
      ->add('font-awesome', array())
      ->add('jquery-totop', $enqueueArgs)
      ->add('jquery-isotope', $enqueueArgs)
      ->add('jquery-smoothscroll', $enqueueArgs)
      ->add('jquery-verticalcenter', $enqueueArgs);



    if (VTCore_Zeus_Init::getFeatures()->get('options.animsition.enable')) {
      VTCore_Wordpress_init::getFactory('assets')
        ->get('queues')
        ->add('jquery-animsition', $enqueueArgs);
    }

    // Only load sticky header assets if enabled
    if (VTCore_Zeus_Init::getFeatures()->get('options.menu.enable_sticky')) {
      VTCore_Wordpress_init::getFactory('assets')
        ->get('queues')
        ->add('jquery-hcsticky', $enqueueArgs);
    }

    // Only load slicknav assets if enabled
    if (VTCore_Zeus_Init::getFeatures()->get('options.menu.enable_slick')) {
      VTCore_Wordpress_init::getFactory('assets')
        ->get('queues')
        ->add('jquery-slicknav', $enqueueArgs);
    }

    // Only load jvfloat assets if enabled
    if (VTCore_Zeus_Init::getFeatures()->get('options.form.jvfloat')) {
      VTCore_Wordpress_init::getFactory('assets')
        ->get('queues')
        ->add('jquery-jvfloat', $enqueueArgs);
    }


    // Don't Proceed any further if no VicTheme Headline plugin is enabled.
    if (defined('VTCORE_HEADLINE_LOADED')) {

      VTCore_Wordpress_init::getFactory('assets')
        ->get('queues')
        ->add('jquery-videobg', $enqueueArgs)
        ->add('headline-front', array());

      $instance = VTCore_Headline_Utility::getHeadline();

      if (VTCore_Headline_Utility::checkEnabled('general', $instance)) {
        VTCore_Wordpress_Init::getFactory('assets')
          ->get('library')
          ->add('theme.css.style-css.inline.headline', VTCore_Headline_Utility::getMaskingCSS($instance) . VTCore_Headline_Utility::getBackgroundCSS($instance));
      }
    }

    // Load department assets if department is loaded
    if (defined('VTCORE_DEPARTMENT_LOADED')) {
      VTCore_Wordpress_init::getFactory('assets')
        ->get('queues')
        ->add('department-front', array());
    }

    // Load services assets if services is loaded
    if (defined('VTCORE_SERVICES_LOADED')) {
      VTCore_Wordpress_init::getFactory('assets')
        ->get('queues')
        ->add('services-front', array());
    }

    // Load members assets if members is loaded
    if (defined('VTCORE_MEMBERS_LOADED')) {
      VTCore_Wordpress_init::getFactory('assets')
        ->get('queues')
        ->add('members-front', array());
    }


    // Load the controller javascript and main theme css
    VTCore_Wordpress_init::getFactory('assets')
      ->get('queues')
      ->add('theme', $enqueueArgs);


    // Load visual composer additional asset
    if (VTCORE_VC_LOADED) {
      VTCore_Wordpress_init::getFactory('assets')
        ->get('queues')
        ->add('vc-front', $enqueueArgs);
    }

    // Load revolution slider additional asset
    if (VTCORE_REVSLIDER_LOADED) {
      VTCore_Wordpress_init::getFactory('assets')
        ->get('queues')
        ->add('revslider-front', $enqueueArgs);
    }

    // Load woocommerce assets
    if (VTCORE_WOOCOMMERCE_LOADED) {
      VTCore_Wordpress_init::getFactory('assets')
        ->get('queues')
        ->add('woocommerce-front', $enqueueArgs);
    }


    // Load the active schema extra css
    VTCore_Wordpress_init::getFactory('assets')
      ->get('queues')
      ->add(VTCore_Zeus_Utility::getActiveSchema(), $enqueueArgs);

    VTCore_Wordpress_Init::getFactory('assets')
      ->get('library')

      // Nuke default bootstrap styling
      ->remove('bootstrap.css.bootstrap-theme-css')

      // Note: the style name must be any previously registered css!
      // Load the dynamic style last to override other CSS.
      ->add('theme.css.style-css.inline.schema', VTCore_Zeus_Utility::getSchemaCSS())

      // Load user custom css
      ->add('theme.css.style-css.inline.custom', get_theme_mod('custom_css', ''));

    // Allow user to create custom.css or custom.js file
    // and get loaded automatically.
    VTCore_Wordpress_init::getFactory('assets')
      ->get('queues')
      ->add('custom', $enqueueArgs);

  }
}