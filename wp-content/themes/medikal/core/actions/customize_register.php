<?php
/**
 * Class for filtering customize_register
 * and adding extra class to it.
 *
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Actions_Customize__Register
extends VTCore_Wordpress_Models_Hook {

  public function hook($wp_customize = NULL) {

    $wp_customize->remove_section('colors');
    $wp_customize->remove_section('background-image');
    $wp_customize->remove_section('header-image');

    // Widget Panel
    if (VTCore_Zeus_Init::getFeatures()->get('options.customizer.widgets') == false) {
      $wp_customize->remove_panel('widgets');
    }

    // Theme options panel
    if (VTCore_Zeus_Init::getFeatures()->get('options.customizer.general')) {
      $object = new VTCore_Zeus_Customizer_General();
      $object->register();

      // Clear early to minimize memory footprint
      VTCore_Wordpress_Init::getFactory('customizer')
        ->process($wp_customize)
        ->clear();
    }

    // Schema panel
    if (VTCore_Zeus_Init::getFeatures()->get('options.customizer.style')) {
      $object = new VTCore_Zeus_Customizer_Schema();
      $object->register();

      // Clear early to minimize memory footprint
      VTCore_Wordpress_Init::getFactory('customizer')
        ->process($wp_customize)
        ->clear();
    }


    // Layout Panel
    if (VTCore_Zeus_Init::getFeatures()->get('options.customizer.layout')) {
      $object = new VTCore_Zeus_Customizer_Layout();
      $object->register();

      // Clear early to minimize memory footprint
      VTCore_Wordpress_Init::getFactory('customizer')
        ->process($wp_customize)
        ->clear();
    }


    // Shop Panel
    if (defined('VTCORE_WOOCOMMERCE_LOADED')
        && VTCORE_WOOCOMMERCE_LOADED
        && VTCore_Zeus_Init::getFeatures()->get('options.customizer.shop')) {

      $object = new VTCore_Zeus_Customizer_Shop();
      $object->register();

      // Clear early to minimize memory footprint
      VTCore_Wordpress_Init::getFactory('customizer')
        ->process($wp_customize)
        ->clear();
    }


    // Department Panel
    if (defined('VTCORE_DEPARTMENT_LOADED')
      && VTCore_Zeus_Init::getFeatures()->get('options.customizer.department')) {

      $object = new VTCore_Zeus_Customizer_Department();
      $object->register();

      // Clear early to minimize memory footprint
      VTCore_Wordpress_Init::getFactory('customizer')
        ->process($wp_customize)
        ->clear();
    }


    // Services Panel
    if (defined('VTCORE_SERVICES_LOADED')
      && VTCore_Zeus_Init::getFeatures()->get('options.customizer.services')) {

      $object = new VTCore_Zeus_Customizer_Services();
      $object->register();

      // Clear early to minimize memory footprint
      VTCore_Wordpress_Init::getFactory('customizer')
        ->process($wp_customize)
        ->clear();
    }


    // Members Panel
    if (defined('VTCORE_MEMBERS_LOADED')
      && VTCore_Zeus_Init::getFeatures()->get('options.customizer.members')) {

      $object = new VTCore_Zeus_Customizer_Members();
      $object->register();

      // Clear early to minimize memory footprint
      VTCore_Wordpress_Init::getFactory('customizer')
        ->process($wp_customize)
        ->clear();
    }


    // Timetable Panel
    if (defined('VTCORE_DEPARTMENT_LOADED')
        && defined('VTCORE_SERVICES_LOADED')
        && defined('VTCORE_MEMBERS_LOADED')
        && VTCore_Zeus_Init::getFeatures()->get('options.customizer.timetable')) {

      $object = new VTCore_Zeus_Customizer_TimeTable();
      $object->register();

      // Clear early to minimize memory footprint
      VTCore_Wordpress_Init::getFactory('customizer')
        ->process($wp_customize)
        ->clear();
    }

    // Clear memory
    $object = NULL;
    unset($object);

    $this->preprocess($wp_customize);


  }


  /**
   * Method for performing final preprocessing
   * This is prepared for different theme needs
   * different pre processing method.
   *
   * Areas, Regions and Schema altering best performed
   * in their own alter actions. That way once altered
   * will effect all factory and contexes.
   *
   * @param $wp_customize
   *
   * @see VTCore_Zeus_Actions_VTCore__Zeus__Alter__Areas
   * @see VTCore_Zeus_Actions_VTCore__Zeus__Alter__Regions
   * @see VTCore_Zeus_Actions_VTCore__Zeus__Alter__Schema
   * @see VTCore_Zeus_Actions_VTCore__Zeus__Alter__Features
   */
  protected function preprocess($wp_customize) {}
}