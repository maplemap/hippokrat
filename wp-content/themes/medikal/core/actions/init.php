<?php
/**
 * Class for hooking to init action
 * and adding extra class to it.
 *
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Actions_Init
extends VTCore_Wordpress_Models_Hook {

  protected $weight = 1000;

  public function hook() {

    $optionObject = new VTCore_Zeus_Config_Options();

    // Adding Logo image size
    add_image_size(
      'logo',
      $optionObject->get('logo.width'),
      $optionObject->get('logo.height'),
      $optionObject->get('logo.crop')
    );

    if (VTCORE_WOOCOMMERCE_LOADED) {
      add_image_size(
        'shop_single',
        $optionObject->get('options.products.single.normal.width'),
        $optionObject->get('options.products.single.normal.height'),
        $optionObject->get('options.products.single.normal.crop')
      );
      
      add_image_size(
        'shop_single_zoom',
        $optionObject->get('options.products.single.zoom.width'),
        $optionObject->get('options.products.single.zoom.height'),
        $optionObject->get('options.products.single.zoom.crop')
      );

      add_image_size(
        'shop_single_fullscreen',
        $optionObject->get('options.products.single.fullscreen.width'),
        $optionObject->get('options.products.single.fullscreen.height'),
        $optionObject->get('options.products.single.fullscreen.crop')
      );
    }

  }
}