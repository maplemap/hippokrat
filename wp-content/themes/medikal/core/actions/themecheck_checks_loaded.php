<?php
/**
 * Hooking into wordpress admin_menu action
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Zeus_Actions_ThemeCheck__Checks__Loaded
extends VTCore_Wordpress_Models_Hook {

  public function hook() {
    global $themechecks;

    foreach ($themechecks as $delta => $object) {

      if (is_a($object, 'Check_Links')) {
        $themechecks[$delta] = new VTCore_Zeus_ThemeCheck_Links();
      }

      if (is_a($object, 'Plugin_Territory')) {
        $themechecks[$delta] = new VTCore_Zeus_ThemeCheck_PluginTerritory();
      }

      if (is_a($object, 'NonPrintableCheck')) {
        $themechecks[$delta] = new VTCore_Zeus_ThemeCheck_NonPrintable();
      }

      if (is_a($object, 'Bad_Checks') || is_a($object, 'TF_Bad_Checks')) {
        $themechecks[$delta] = new VTCore_Zeus_ThemeCheck_BadChecks();
      }

      // The theme uses widget_init action via VTCore
      // and register the sidebar inside the class
      // VTCore_Zeus_Actions_Widgets__Init which is invoked
      // inside the wodget_init action and the theme_check
      // just incapable to detect it.
      if (is_a($object, 'WidgetsCheck')) {
        unset($themechecks[$delta]);
      }

      // Theme check need long timeout for it to finish properly!
      set_time_limit(-1);
      // ini_set('memory_limit', '256M');
    }
  }
}