<?php
/**
 * Panel section for configuring general options
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Panels_CSS
extends VTCore_Zeus_Panels_Model {


  /**
   * Overriding parent method
   * @see VTCore_Bootstrap_Element_BsElement::buildElement()
   */
  public function buildElement() {

    $assistance = new VTCore_Zeus_Assistance();

    // Logo Configurations
    $this->panel = new VTCore_Bootstrap_Element_BsPanel(array(
      'text' => __('Custom CSS Rule', 'medikal'),
      'id' => 'vtcore-zeus-customcss-panel',
    ));

    $this->panel
      ->setChildrenPointer('heading')
      ->BsBadge(array(
        'attributes' => array(
          'class' => array(
            'pull-right',
          ),
        ),
      ))
      ->lastChild()
      ->Hyperlink(array(
        'text' => __('help', 'medikal'),
        'attributes' => array(
          'href' => $assistance->getAssistanceItem('customcss', 'url'),
          'target' => $assistance->getAssistanceItem('customcss', 'target'),
          'data-title' => $assistance->getAssistanceItem('customcss', 'title'),
          'data-type' => $assistance->getAssistanceItem('customcss', 'type'),
          'data-toggle' => 'modal',
          'data-target' => '#vtcore-zeus-help-modal',
         ),
      ))
      ->getParent()
      ->setChildrenPointer('content')
      ->addChildren(new VTCore_Bootstrap_Form_BsDescription(array(
        'text' => __('The custom css entered here will be recorded to database and printed as inline css when page is loading.', 'medikal'),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsTextarea(array(
        'name' => 'theme[custom_css]',
        'value' => get_theme_mod('custom_css', ''),
        'raw' => true,
            'id' => 'custom_css',
      )));

    $this->processPanel();

  }

}