<?php
/**
 * Panel section for configuring general options
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Panels_TimeTable
extends VTCore_Zeus_Panels_Model {


  /**
   * Overriding parent method
   * @see VTCore_Bootstrap_Element_BsElement::buildElement()
   */
  public function buildElement() {

    $assistance = new VTCore_Zeus_Assistance();


    // Build the timetable page
    $this->panel = new VTCore_Bootstrap_Element_BsPanel(array(
      'text' => __('Timetable Page Options', 'medikal'),
      'id' => 'vtcore-zeus-timetable-options',
      'attributes' => array(
        'class' => array(
          'switch-column-two',
        ),
      ),
    ));

    $this->panel
      ->setChildrenPointer('content')
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][timetable][weekdate]',
        'text' => __('Show WeekDate', 'medikal'),
        'description' => __('Show / hide the timetable weekdate element.', 'medikal'),
        'checked' => (boolean) $this->getShow('timetable', 'weekdate'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][timetable][filter]',
        'text' => __('Show Filtering', 'medikal'),
        'description' => __('Show / hide the timetable taxonomy filtering element.', 'medikal'),
        'checked' => (boolean) $this->getShow('timetable', 'filter'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][timetable][title]',
        'text' => __('Show Title', 'medikal'),
        'description' => __('Show / hide the timetable title element.', 'medikal'),
        'checked' => (boolean) $this->getShow('timetable', 'title'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][timetable][department]',
        'text' => __('Show Department', 'medikal'),
        'description' => __('Show / hide the timetable department information element.', 'medikal'),
        'checked' => (boolean) $this->getShow('timetable', 'department'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][timetable][members]',
        'text' => __('Show Members', 'medikal'),
        'description' => __('Show / hide the timetable members information element.', 'medikal'),
        'checked' => (boolean) $this->getShow('timetable', 'members'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][timetable][location]',
        'text' => __('Show WeekDate', 'medikal'),
        'description' => __('Show / hide the timetable location information element.', 'medikal'),
        'checked' => (boolean) $this->getShow('timetable', 'location'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->getParent()
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'text' => __('Days - Monday', 'medikal'),
        'name' => 'theme[features][options][timetable][days][monday]',
        'value' => $this->getOption('timetable.days', 'monday'),
        'description' => __('Set the text for the timetable days - monday.', 'medikal'),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'text' => __('Days - Tuesday', 'medikal'),
        'name' => 'theme[features][options][timetable][days][tuesday]',
        'value' => $this->getOption('timetable.days', 'tuesday'),
        'description' => __('Set the text for the timetable days - tuesday.', 'medikal'),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'text' => __('Days - Wednesday', 'medikal'),
        'name' => 'theme[features][options][timetable][days][wednesday]',
        'value' => $this->getOption('timetable.days', 'wednesday'),
        'description' => __('Set the text for the timetable days - wednesday.', 'medikal'),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'text' => __('Days - Thursday', 'medikal'),
        'name' => 'theme[features][options][timetable][days][thursday]',
        'value' => $this->getOption('timetable.days', 'thursday'),
        'description' => __('Set the text for the timetable days - thursday.', 'medikal'),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'text' => __('Days - Friday', 'medikal'),
        'name' => 'theme[features][options][timetable][days][friday]',
        'value' => $this->getOption('timetable.days', 'friday'),
        'description' => __('Set the text for the timetable days - friday.', 'medikal'),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'text' => __('Days - Saturday', 'medikal'),
        'name' => 'theme[features][options][timetable][days][saturday]',
        'value' => $this->getOption('timetable.days', 'saturday'),
        'description' => __('Set the text for the timetable days - saturday.', 'medikal'),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'text' => __('Days - Sunday', 'medikal'),
        'name' => 'theme[features][options][timetable][days][sunday]',
        'value' => $this->getOption('timetable.days', 'sunday'),
        'description' => __('Set the text for the timetable days - sunday.', 'medikal'),
      )));


    $this->processPanel();


    // Build the timetable shifts
    $this->panel = new VTCore_Bootstrap_Element_BsPanel(array(
      'text' => __('Timetable Morning Shifts', 'medikal'),
      'id' => 'vtcore-zeus-timetable-morning-shifts-options',
      'attributes' => array(
        'class' => array(
          'switch-column-two',
        ),
      ),
    ));

    $this->panel
      ->setChildrenPointer('content')
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][options][timetable][shifts][morning][enabled]',
        'text' => __('Enable Morning Shifts', 'medikal'),
        'description' => __('Enable the morning shifts column', 'medikal'),
        'checked' => (boolean) $this->getOption('timetable.shifts.morning', 'enabled'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'text' => __('Title', 'medikal'),
        'name' => 'theme[features][options][timetable][shifts][morning][title]',
        'value' => $this->getOption('timetable.shifts.morning', 'title'),
        'description' => __('Set the text for the title for morning shifts.', 'medikal'),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 12,
            'small' => 12,
            'large' => 12,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsTime(array(
        'text' => __('Start Time', 'medikal'),
        'name' => 'theme[features][options][timetable][shifts][morning][start]',
        'value' => $this->getOption('timetable.shifts.morning', 'start'),
        'description' => __('Set start time for the morning shift', 'medikal'),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsTime(array(
        'text' => __('End Time', 'medikal'),
        'name' => 'theme[features][options][timetable][shifts][morning][end]',
        'value' => $this->getOption('timetable.shifts.morning', 'end'),
        'description' => __('Set end time for the morning shift', 'medikal'),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )));

    $this->processPanel();

    // Build the timetable shifts
    $this->panel = new VTCore_Bootstrap_Element_BsPanel(array(
      'text' => __('Timetable Afternoon Shifts', 'medikal'),
      'id' => 'vtcore-zeus-timetable-afternoon-shifts-options',
      'attributes' => array(
        'class' => array(
          'switch-column-two',
        ),
      ),
    ));

    $this->panel
      ->setChildrenPointer('content')
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][options][timetable][shifts][afternoon][enabled]',
        'text' => __('Enable afternoon Shifts', 'medikal'),
        'description' => __('Enable the afternoon shifts column', 'medikal'),
        'checked' => (boolean) $this->getOption('timetable.shifts.afternoon', 'enabled'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'text' => __('Title', 'medikal'),
        'name' => 'theme[features][options][timetable][shifts][afternoon][title]',
        'value' => $this->getOption('timetable.shifts.afternoon', 'title'),
        'description' => __('Set the text for the title for afternoon shifts.', 'medikal'),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 12,
            'small' => 12,
            'large' => 12,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsTime(array(
        'text' => __('Start Time', 'medikal'),
        'name' => 'theme[features][options][timetable][shifts][afternoon][start]',
        'value' => $this->getOption('timetable.shifts.afternoon', 'start'),
        'description' => __('Set start time for the afternoon shift', 'medikal'),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsTime(array(
        'text' => __('End Time', 'medikal'),
        'name' => 'theme[features][options][timetable][shifts][afternoon][end]',
        'value' => $this->getOption('timetable.shifts.afternoon', 'end'),
        'description' => __('Set end time for the afternoon shift', 'medikal'),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )));



    $this->processPanel();


    // Build the timetable shifts
    $this->panel = new VTCore_Bootstrap_Element_BsPanel(array(
      'text' => __('Timetable Night Shifts', 'medikal'),
      'id' => 'vtcore-zeus-timetable-night-shifts-options',
      'attributes' => array(
        'class' => array(
          'switch-column-two',
        ),
      ),
    ));

    $this->panel
      ->setChildrenPointer('content')
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][options][timetable][shifts][night][enabled]',
        'text' => __('Enable night Shifts', 'medikal'),
        'description' => __('Enable the night shifts column', 'medikal'),
        'checked' => (boolean) $this->getOption('timetable.shifts.night', 'enabled'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'text' => __('Title', 'medikal'),
        'name' => 'theme[features][options][timetable][shifts][night][title]',
        'value' => $this->getOption('timetable.shifts.night', 'title'),
        'description' => __('Set the text for the title for night shifts.', 'medikal'),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 12,
            'small' => 12,
            'large' => 12,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsTime(array(
        'text' => __('Start Time', 'medikal'),
        'name' => 'theme[features][options][timetable][shifts][night][start]',
        'value' => $this->getOption('timetable.shifts.night', 'start'),
        'description' => __('Set start time for the night shift', 'medikal'),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsTime(array(
        'text' => __('End Time', 'medikal'),
        'name' => 'theme[features][options][timetable][shifts][night][end]',
        'value' => $this->getOption('timetable.shifts.night', 'end'),
        'description' => __('Set end time for the night shift', 'medikal'),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )));


    $this->processPanel();



  }
}