<?php
/**
 * Panel section for configuring general options
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Panels_Department
extends VTCore_Zeus_Panels_Model {


  /**
   * Overriding parent method
   * @see VTCore_Bootstrap_Element_BsElement::buildElement()
   */
  public function buildElement() {

    // Build the department teaser options
    $this->panel = new VTCore_Bootstrap_Element_BsPanel(array(
      'text' => __('Department Archives', 'medikal'),
      'id' => 'vtcore-zeus-department-archives-options',
    ));

    $this->panel
      ->setChildrenPointer('content')
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][department][archive][icon]',
        'text' => __('Show Icon', 'medikal'),
        'description' => __('Show / hide the department icon.', 'medikal'),
        'checked' => (boolean) $this->getShow('department.archive', 'icon'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][department][archive][image]',
        'text' => __('Show Image', 'medikal'),
        'description' => __('Show / hide the department image.', 'medikal'),
        'checked' => (boolean) $this->getShow('department.archive', 'image'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][department][archive][title]',
        'text' => __('Show Title', 'medikal'),
        'description' => __('Show / hide the department title.', 'medikal'),
        'checked' => (boolean) $this->getShow('department.archive', 'title'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][department][archive][slogan]',
        'text' => __('Show Slogan', 'medikal'),
        'description' => __('Show / hide the department slogan.', 'medikal'),
        'checked' => (boolean) $this->getShow('department.archive', 'slogan'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][department][archive][description]',
        'text' => __('Show Description', 'medikal'),
        'description' => __('Show / hide the department description.', 'medikal'),
        'checked' => (boolean) $this->getShow('department.archive', 'description'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][department][archive][readmore]',
        'text' => __('Show Readmore', 'medikal'),
        'description' => __('Show / hide the department readmore.', 'medikal'),
        'checked' => (boolean) $this->getShow('department.archive', 'readmore'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][options][department][archive][ajax]',
        'text' => __('Enable Ajax', 'medikal'),
        'description' => __('Enable ajax for pagination.', 'medikal'),
        'checked' => (boolean) $this->getOption('department.archive', 'ajax'),
        'switch' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][options][department][archive][pager][mini]',
        'text' => __('Pager Mini', 'medikal'),
        'description' => __('Use mini pager for pagination.', 'medikal'),
        'checked' => (boolean) $this->getOption('department.archive.pager', 'mini'),
        'switch' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][options][department][archive][pager][infinite]',
        'text' => __('Pager infinite', 'medikal'),
        'description' => __('Use infinite pager for pagination.', 'medikal'),
        'checked' => (boolean) $this->getOption('department.archive.pager', 'infinite'),
        'switch' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->getParent()
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsSelect(array(
        'text' => __('Teaser Template', 'medikal'),
        'name' => 'theme[features][options][department][archive][template]',
        'value' => $this->getOption('department.archive', 'template'),
        'description' => __('Select the teaser template to use for the department archive items', 'medikal'),
        'options' => array(
          'department-grid' => __('Grid', 'medikal'),
          'department-masonry' => __('Masonry', 'medikal'),
          'department-grid-simple' => __('Grid Simple', 'medikal'),
          'department-flip' => __('Flip', 'medikal'),
        ),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsGrids(array(
        'name' => 'theme[features][options][department][archive][grids]',
        'value' => $this->getOption('department.archive', 'grids'),
        'columns' => array(
          'text' => __('Department Teaser Grid Columns', 'medikal'),
          'description' => __('Set the grid column size for the teaser items.', 'medikal'),
        ),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 12,
            'small' => 12,
            'large' => 12,
          ),
        ),
        'element_grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->getParent()
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'text' => __('Not Found Title', 'medikal'),
        'name' => 'theme[features][options][department][archive][404][title]',
        'value' => $this->getOption('department.archive.404', 'title'),
        'description' => __('Set the text to serve as the title when no department is available to display.', 'medikal'),
        'raw' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 12,
            'small' => 12,
            'large' => 12,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsTextarea(array(
        'text' => __('Not Found Description', 'medikal'),
        'name' => 'theme[features][options][department][archive][404][description]',
        'value' => $this->getOption('department.archive.404', 'description'),
        'description' => __('Set the text to serve as the description when no department is available to display.', 'medikal'),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 12,
            'small' => 12,
            'large' => 12,
          ),
        ),
      )))
      ->getParent()
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'text' => __('Page Title', 'medikal'),
        'name' => 'theme[features][options][department][archive][title]',
        'value' => $this->getOption('department.archive', 'title'),
        'description' => __('Set the text to serve as the title in the department archive page.', 'medikal'),
        'raw' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 12,
            'small' => 12,
            'large' => 12,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsTextarea(array(
        'text' => __('Page Description Left', 'medikal'),
        'name' => 'theme[features][options][department][archive][description][left]',
        'value' => $this->getOption('department.archive.description', 'left'),
        'description' => __('Set the text to serve as the description on the left box in the department archive page.', 'medikal'),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsTextarea(array(
        'text' => __('Page Description Right', 'medikal'),
        'name' => 'theme[features][options][department][archive][description][right]',
        'value' => $this->getOption('department.archive.description', 'right'),
        'description' => __('Set the text to serve as the description on the left box in the department archive page.', 'medikal'),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )));

    $this->processPanel();

    // Build the page single visibility panel
    $this->panel = new VTCore_Bootstrap_Element_BsPanel(array(
      'text' => __('Department Single', 'medikal'),
      'id' => 'vtcore-zeus-department-single-options',
    ));

    $this->panel
      ->setChildrenPointer('content')
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][department][single][image]',
        'text' => __('Show Image', 'medikal'),
        'description' => __('Show / hide the department image.', 'medikal'),
        'checked' => (boolean) $this->getShow('department.single', 'image'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][department][single][slogan]',
        'text' => __('Show Slogan', 'medikal'),
        'description' => __('Show / hide the department slogan.', 'medikal'),
        'checked' => (boolean) $this->getShow('department.single', 'slogan'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][department][single][title]',
        'text' => __('Show Title', 'medikal'),
        'description' => __('Show / hide the department title.', 'medikal'),
        'checked' => (boolean) $this->getShow('department.single', 'title'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][department][single][content]',
        'text' => __('Show Content', 'medikal'),
        'description' => __('Show / hide the department content.', 'medikal'),
        'checked' => (boolean) $this->getShow('department.single', 'content'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][department][single][services]',
        'text' => __('Show Services', 'medikal'),
        'description' => __('Show / hide the department services.', 'medikal'),
        'checked' => (boolean) $this->getShow('department.single', 'services'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][department][single][members]',
        'text' => __('Show Members', 'medikal'),
        'description' => __('Show / hide the department members.', 'medikal'),
        'checked' => (boolean) $this->getShow('department.single', 'members'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][department][single][promotional]',
        'text' => __('Show Promotional', 'medikal'),
        'description' => __('Show / hide the department promotional.', 'medikal'),
        'checked' => (boolean) $this->getShow('department.single', 'promotional'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )));


    $this->processPanel();


  }
}