<?php
/**
 * Panel section for configuring general options
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Panels_General
extends VTCore_Zeus_Panels_Model {


  /**
   * Overriding parent method
   * @see VTCore_Bootstrap_Element_BsElement::buildElement()
   */
  public function buildElement() {

    $assistance = new VTCore_Zeus_Assistance();

    // Logo Configurations
    $this->panel = new VTCore_Bootstrap_Element_BsPanel(array(
      'text' => __('Logo', 'medikal'),
      'id' => 'vtcore-zeus-logo-panel',
    ));

    $this->panel
     ->setChildrenPointer('content')
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Grid_BsColumn(array(
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 8,
            'small' => 8,
            'large' => 8,
          ),
        ),
      )))
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'text' => __('Text', 'medikal'),
        'name' => 'theme[features][options][logo][text]',
        'value' => $this->getOption('logo', 'text'),
        'description' => __('Set the text to serve as the logo or leave empty to disable it.', 'medikal'),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'text' => __('Slogan', 'medikal'),
        'name' => 'theme[features][options][logo][slogan]',
        'value' => $this->getOption('logo', 'slogan'),
        'description' => __('Set the text to serve as the slogan text underneath the logo or leave empty to disable it.', 'medikal'),
      )))
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'text' => __('Image Width', 'medikal'),
        'name' => 'theme[features][options][logo][width]',
        'value' => $this->getOption('logo', 'width'),
        'description' => __('Force the logo image to have maximum width as configured.', 'medikal'),
        'suffix' => 'px',
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 12,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'text' => __('Image Height', 'medikal'),
        'name' => 'theme[features][options][logo][height]',
        'value' => $this->getOption('logo', 'height'),
        'description' => __('Force the logo image to have maximum height as configured.', 'medikal'),
        'suffix' => 'px',
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 12,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->getParent()
      ->getParent()
      ->addChildren(new VTCore_Bootstrap_Grid_BsColumn(array(
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 4,
            'small' => 4,
            'large' => 4,
          ),
        ),
      )))
      ->lastChild()
      ->addChildren(new VTCore_Wordpress_Form_WpMedia(array(
        'text' => __('Image', 'medikal'),
        'name' => 'theme[features][options][logo][image]',
        'value' => $this->getOption('logo', 'image'),
        'description' => __('Image that will be used as the logo for normal screen', 'medikal'),
        'attributes' => array(
          'class' => array('logo-preview'),
        ),
      )));

    $this->processPanel();








    // Favicon Configurations
    $this->panel = new VTCore_Bootstrap_Element_BsPanel(array(
      'text' => __('Favicon', 'medikal'),
      'id' => 'vtcore-zeus-favicon-panel',
    ));

    $this->panel
      ->setChildrenPointer('content')
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsDescription(array(
        'text' =>  __('Upload the image that will be served as the favicon icon', 'medikal'),
        'grids' => array(
          'columns' => array(
            'mobile' => 8,
            'tablet' => 8,
            'small' => 8,
            'large' => 8,
          ),
        ),
      )))
      ->addChildren(new VTCore_Wordpress_Form_WpMedia(array(
        'name' => 'theme[features][options][favicon][image]',
        'value' => $this->getOption('favicon', 'image'),
        'attributes' => array(
        'class' => array('favicon-preview'),
        ),
        'grids' => array(
          'columns' => array(
            'mobile' => 4,
            'tablet' => 4,
            'small' => 4,
            'large' => 4,
          ),
        ),
      )));

    $this->processPanel();




    // Build the header options
    $this->panel = new VTCore_Bootstrap_Element_BsPanel(array(
      'text' => __('Header Options', 'medikal'),
      'id' => 'vtcore-zeus-header-panel',
    ));

    $this->panel
      ->setChildrenPointer('content')
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][options][menu][disable_parent]',
        'text' => __('Disable Parent Link', 'medikal'),
        'description' => __('Disabling the first level parent link for menu with children and replace it with # as the destination.', 'medikal'),
        'checked' => (boolean) $this->getOption('menu', 'disable_parent'),
        'switch' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 4,
            'small' => 4,
            'large' => 4,
          ),
        ),
      )))
      ->BsCheckbox(array(
        'name' => 'theme[features][options][menu][enable_slick]',
        'text' => __('Enable Slick Navigation', 'medikal'),
        'description' => __('Enable or disable the slick navigation when screen width is less than 768px.', 'medikal'),
        'checked' => (boolean) $this->getOption('menu', 'enable_slick'),
        'switch' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 4,
            'small' => 4,
            'large' => 4,
          ),
        ),
      ))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][options][menu][enable_sticky]',
        'text' => __('Enable Sticky Header', 'medikal'),
        'description' => __('Enable or disable the sticky header.', 'medikal'),
        'checked' => (boolean) $this->getOption('menu', 'enable_sticky'),
        'switch' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 4,
            'small' => 4,
            'large' => 4,
          ),
        ),
      )));



    $this->processPanel();



    // Initial loading animation
    $this->panel = new VTCore_Bootstrap_Element_BsPanel(array(
      'text' => __('Page Loading Animation', 'medikal'),
      'id' => 'vtcore-zeus-animsition-panel',
    ));

    $this->panel
      ->setChildrenPointer('content')
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][options][animsition][enable]',
        'text' => __('Enable page loading animation', 'medikal'),
        'description' => __('Toggle to enalbe or disable the initial page loading animation', 'medikal'),
        'checked' => (boolean) $this->getOption('animsition', 'enable'),
        'switch' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'name' => 'theme[features][options][animsition][loading_text]',
        'text' => __('Loading Text', 'medikal'),
        'description' => __('Set the text to display when page is loading.', 'medikal'),
        'value' => $this->getOption('animsition', 'loading_text'),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsSelect(array(
        'name' => 'theme[features][options][animsition][animation_in]',
        'text' => __('Animation In', 'medikal'),
        'description' => __('Type of animation used when loading in', 'medikal'),
        'value' => $this->getOption('animsition', 'animation_in'),
        'attributes' => array(
          'class' => array('clear'),
        ),
        'options' => array(
          'fade-in' => __('Fade In', 'medikal'),
          'fade-out' => __('Fade Out', 'medikal'),
          'fade-in-up' => __('Fade In Out', 'medikal'),
          'fade-out-up' => __('Fade Out Up', 'medikal'),
          'fade-in-down' => __('Fade In Down', 'medikal'),
          'fade-out-down' => __('Fade Out Down', 'medikal'),
          'fade-in-left' => __('Fade In Left', 'medikal'),
          'fade-out-left' => __('Fade Out Left', 'medikal'),
          'fade-in-right' => __('Fade In Right', 'medikal'),
          'fade-out-right' => __('Fade Out Right', 'medikal'),
          'rotate-in' => __('Rotate In', 'medikal'),
          'rotate-out' => __('Rotate Out', 'medikal'),
          'flip-in-x' => __('Flip in X', 'medikal'),
          'flip-out-x' => __('Flip out X', 'medikal'),
          'flip-in-y' => __('Flip in Y', 'medikal'),
          'flip-out-y' => __('Flip out Y', 'medikal'),
          'zoom-in' => __('Zoom In', 'medikal'),
          'zoom-out' => __('Zoom Out', 'medikal'),
        ),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'name' => 'theme[features][options][animsition][in_duration]',
        'text' => __('Animation in duration', 'medikal'),
        'description' => __('Set the animation duration when loading in', 'medikal'),
        'suffix' => __('miliseconds', 'medikal'),
        'value' => $this->getOption('animsition', 'in_duration'),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsSelect(array(
        'name' => 'theme[features][options][animsition][animation_out]',
        'text' => __('Animation Out', 'medikal'),
        'description' => __('Type of animation used when loading out', 'medikal'),
        'value' => $this->getOption('animsition', 'animation_out'),
        'attributes' => array(
          'class' => array('clear'),
        ),
        'options' => array(
          'fade-in' => __('Fade In', 'medikal'),
          'fade-out' => __('Fade Out', 'medikal'),
          'fade-in-up' => __('Fade In Out', 'medikal'),
          'fade-out-up' => __('Fade Out Up', 'medikal'),
          'fade-in-down' => __('Fade In Down', 'medikal'),
          'fade-out-down' => __('Fade Out Down', 'medikal'),
          'fade-in-left' => __('Fade In Left', 'medikal'),
          'fade-out-left' => __('Fade Out Left', 'medikal'),
          'fade-in-right' => __('Fade In Right', 'medikal'),
          'fade-out-right' => __('Fade Out Right', 'medikal'),
          'rotate-in' => __('Rotate In', 'medikal'),
          'rotate-out' => __('Rotate Out', 'medikal'),
          'flip-in-x' => __('Flip in X', 'medikal'),
          'flip-out-x' => __('Flip out X', 'medikal'),
          'flip-in-y' => __('Flip in Y', 'medikal'),
          'flip-out-y' => __('Flip out Y', 'medikal'),
          'zoom-in' => __('Zoom In', 'medikal'),
          'zoom-out' => __('Zoom Out', 'medikal'),
        ),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'name' => 'theme[features][options][animsition][out_duration]',
        'text' => __('Animation out duration', 'medikal'),
        'description' => __('Set the animation duration when loading out', 'medikal'),
        'suffix' => __('miliseconds', 'medikal'),
        'value' => $this->getOption('animsition', 'out_duration'),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )));

    $this->processPanel();


    // Form Options
    $this->panel = new VTCore_Bootstrap_Element_BsPanel(array(
      'text' => __('Form Options', 'medikal'),
      'id' => 'vtcore-zeus-form-panel',
    ));

    $this->panel
      ->setChildrenPointer('content')
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][options][form][jvfloat]',
        'text' => __('Enable JvFloat', 'medikal'),
        'description' => __('Toggle to enable or disable the jvfloat effect which will convert the form label as hidden placeholder', 'medikal'),
        'checked' => (boolean) $this->getOption('form', 'jvfloat'),
        'switch' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )));

    $this->processPanel();

    // Build the teasers
    $this->panel = new VTCore_Bootstrap_Element_BsPanel(array(
      'text' => __('Post Teasers Options', 'medikal'),
      'id' => 'vtcore-zeus-teaser-panel',
    ));

    $this->panel
      ->setChildrenPointer('content')
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][teaser][image]',
        'text' => __('Show Image', 'medikal'),
        'description' => __('Show / hide the teaser featured image element', 'medikal'),
        'checked' => (boolean) $this->getShow('teaser', 'image'),
        'switch' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][teaser][title]',
        'text' => __('Show Title', 'medikal'),
        'description' => __('Show / hide the excerpt title element', 'medikal'),
        'checked' => (boolean) $this->getShow('teaser', 'title'),
        'switch' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][teaser][byline]',
        'text' => __('Show By Line', 'medikal'),
        'description' => __('Show / hide the excerpt post by line element', 'medikal'),
        'checked' => (boolean) $this->getShow('teaser', 'byline'),
        'switch' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][teaser][line]',
        'text' => __('Show Line', 'medikal'),
        'description' => __('Show / hide the teaser under the text line element', 'medikal'),
        'checked' => (boolean) $this->getShow('teaser', 'line'),
        'switch' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][teaser][excerpt]',
        'text' => __('Show Excerpt', 'medikal'),
        'description' => __('Show / hide the teaser excerpt element', 'medikal'),
        'checked' => (boolean) $this->getShow('teaser', 'excerpt'),
        'switch' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][teaser][readmore]',
        'text' => __('Show Readmore', 'medikal'),
        'description' => __('Show / hide the readmore button.', 'medikal'),
        'checked' => (boolean) $this->getShow('teaser', 'readmore'),
        'switch' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->getParent()
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][options][teasers][pager][ajax]',
        'text' => __('Pager Ajax', 'medikal'),
        'description' => __('Enable ajax for pagination.', 'medikal'),
        'checked' => (boolean) $this->getOption('teasers.pager', 'ajax'),
        'switch' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][options][teasers][pager][mini]',
        'text' => __('Pager Mini', 'medikal'),
        'description' => __('Use mini pager for pagination.', 'medikal'),
        'checked' => (boolean) $this->getOption('teasers.pager', 'mini'),
        'switch' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][options][teasers][pager][infinite]',
        'text' => __('Pager infinite', 'medikal'),
        'description' => __('Use infinite pager for pagination.', 'medikal'),
        'checked' => (boolean) $this->getOption('teasers.pager', 'infinite'),
        'switch' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->getParent()
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsSelect(array(
        'name' => 'theme[features][options][teasers][template]',
        'text' => __('Template', 'medikal'),
        'description' => __('Template used for the post teasers.', 'medikal'),
        'value' => $this->getOption('teasers', 'template'),
        'attributes' => array(
          'class' => array('clear'),
        ),
        'options' => array(
          'blog-metro' => __('Metro Style', 'medikal'),
          'blog-list' => __('Simple List Style', 'medikal'),
          'blog-masonry' => __('Masonry Style', 'medikal'),
        ),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->getParent()
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'text' => __('Page Title', 'medikal'),
        'name' => 'theme[features][options][teasers][title]',
        'value' => $this->getOption('teasers', 'title'),
        'description' => __('Set the text to serve as the title in the post archive page.', 'medikal'),
        'raw' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 12,
            'small' => 12,
            'large' => 12,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsTextarea(array(
        'text' => __('Page Description Left', 'medikal'),
        'name' => 'theme[features][options][teasers][description][left]',
        'value' => $this->getOption('teasers.description', 'left'),
        'description' => __('Set the text to serve as the description on the left box in the post archive page.', 'medikal'),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsTextarea(array(
        'text' => __('Page Description Right', 'medikal'),
        'name' => 'theme[features][options][teasers][description][right]',
        'value' => $this->getOption('teasers.description', 'right'),
        'description' => __('Set the text to serve as the description on the left box in the post archive page.', 'medikal'),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )));




    $this->processPanel();


    // Build the post single visibility panel
    $this->panel = new VTCore_Bootstrap_Element_BsPanel(array(
      'text' => __('Post Single Options', 'medikal'),
      'id' => 'vtcore-zeus-post-single-options',
      'attributes' => array(
        'class' => array(
          'switch-column-two',
        ),
      ),
    ));

    $this->panel
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][full][image]',
        'text' => __('Show Image', 'medikal'),
        'description' => __('Show / hide the single post featured image.', 'medikal'),
        'checked' => (boolean) $this->getShow('full', 'image'),
        'switch' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][full][title]',
        'text' => __('Show Title', 'medikal'),
        'description' => __('Show / hide the single post main title.', 'medikal'),
        'checked' => (boolean) $this->getShow('full', 'title'),
        'switch' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][full][byline]',
        'text' => __('Show By Line', 'medikal'),
        'description' => __('Show / hide the post by author line', 'medikal'),
        'checked' => (boolean) $this->getShow('full', 'byline'),
        'switch' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][full][content]',
        'text' => __('Show Content', 'medikal'),
        'description' => __('Show / hide the single post main content', 'medikal'),
        'checked' => (boolean) $this->getShow('full', 'content'),
        'switch' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][full][share]',
        'text' => __('Show Share Links', 'medikal'),
        'description' => __('Show / hide the post share links element.', 'medikal'),
        'checked' => (boolean) $this->getShow('full', 'share'),
        'switch' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][full][tag]',
        'text' => __('Show Tag', 'medikal'),
        'description' => __('Show / hide the post tag on the title', 'medikal'),
        'checked' => (boolean) $this->getShow('full', 'tag'),
        'switch' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][full][post_nav]',
        'text' => __('Show Post Navigation Box', 'medikal'),
        'description' => __('Show / hide the single post navigation box.', 'medikal'),
        'checked' => (boolean) $this->getShow('full', 'post_nav'),
        'switch' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][full][comments]',
        'text' => __('Show Comments Entries', 'medikal'),
        'description' => __('Show / hide single post comment entries element.', 'medikal'),
        'checked' => (boolean) $this->getShow('full', 'comments'),
        'switch' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][full][comment_form]',
        'text' => __('Show Comment Form', 'medikal'),
        'description' => __('Show / hide the single post comment form element.', 'medikal'),
        'checked' => (boolean) $this->getShow('full', 'comment_form'),
        'switch' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )));


    $this->processPanel();


    // Build the page single visibility panel
    $this->panel = new VTCore_Bootstrap_Element_BsPanel(array(
      'text' => __('Page Single Options', 'medikal'),
      'id' => 'vtcore-zeus-page-single-options',
      'attributes' => array(
        'class' => array(
          'switch-column-two',
        ),
      ),
    ));

    $this->panel
      ->setChildrenPointer('content')
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][page][image]',
        'text' => __('Show Image', 'medikal'),
        'description' => __('Show / hide the single page featured image element.', 'medikal'),
        'checked' => (boolean) $this->getShow('page', 'image'),
        'switch' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][page][title]',
        'text' => __('Show Title', 'medikal'),
        'description' => __('Show / hide the single page title element.', 'medikal'),
        'checked' => (boolean) $this->getShow('page', 'title'),
        'switch' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][page][byline]',
        'text' => __('Show By Line', 'medikal'),
        'description' => __('Show / hide the single page author by line.', 'medikal'),
        'checked' => (boolean) $this->getShow('page', 'byline'),
        'switch' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][page][comments]',
        'text' => __('Show Comments Entries', 'medikal'),
        'description' => __('Show / hide single page comment entries element.', 'medikal'),
        'checked' => (boolean) $this->getShow('page', 'comments'),
        'switch' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][page][comment_form]',
        'text' => __('Show Comment Form', 'medikal'),
        'description' => __('Show / hide the single page comment form element.', 'medikal'),
        'checked' => (boolean) $this->getShow('page', 'comment_form'),
        'switch' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][page][share]',
        'text' => __('Show Social Share Links', 'medikal'),
        'description' => __('Show / hide the single page social sharing button links.', 'medikal'),
        'checked' => (boolean) $this->getShow('page', 'share'),
        'switch' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][page][content]',
        'text' => __('Show Content', 'medikal'),
        'description' => __('Show / hide the single page content element.', 'medikal'),
        'checked' => (boolean) $this->getShow('page', 'content'),
        'switch' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][page][post_nav]',
        'text' => __('Show Page Navigation Box', 'medikal'),
        'description' => __('Show / hide the single page navigation box.', 'medikal'),
        'checked' => (boolean) $this->getShow('page', 'post_nav'),
        'switch' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )));


    $this->processPanel();


    // Build the footer elements
    $this->panel = new VTCore_Bootstrap_Element_BsPanel(array(
        'id' => 'vtcore-zeus-footer-panel',
        'text' => __('Footer Element', 'medikal'),
      ));

    $this->panel
      ->setChildrenPointer('content')
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][footer][copyright]',
        'text' => __('Show Copyright', 'medikal'),
        'description' => __('Show / hide the copyright element.', 'medikal'),
        'checked' => (boolean) $this->getShow('footer', 'copyright'),
        'switch' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->getParent()
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'text' => __('Copyright Text', 'medikal'),
        'name' => 'theme[features][options][footer][copyrighttext]',
        'value' => $this->getOption('footer', 'copyrighttext'),
        'description' => __('Set the text to serve as copyright text in the full footer element.', 'medikal'),
      )));



    $this->processPanel();






    // Build the page not found options
    $this->panel = new VTCore_Bootstrap_Element_BsPanel(array(
        'id' => 'vtcore-zeus-notfound-panel',
        'text' => __('Page 404 Options', 'medikal'),
      ));

    $this->panel
      ->setChildrenPointer('content')
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][notfound][title]',
        'text' => __('Show Title', 'medikal'),
        'description' => __('Show / hide the 404 page title.', 'medikal'),
        'checked' => (boolean) $this->getShow('notfound', 'title'),
        'switch' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][notfound][content]',
        'text' => __('Show Content', 'medikal'),
        'description' => __('Show / hide the 404 page content.', 'medikal'),
        'checked' => (boolean) $this->getShow('notfound', 'content'),
        'switch' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->getParent()
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'text' => __('Title', 'medikal'),
        'name' => 'theme[features][options][notfound][title]',
        'value' => $this->getOption('notfound', 'title'),
        'description' => __('Set the title for the page 404.', 'medikal'),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsTextarea(array(
        'text' => __('Content', 'medikal'),
        'name' => 'theme[features][options][notfound][content]',
        'value' => $this->getOption('notfound', 'content'),
        'description' => __('Set the content for the page 404.', 'medikal'),
      )));


    $this->processPanel();


    // Build the performance options
    $this->panel = new VTCore_Bootstrap_Element_BsPanel(array(
      'text' => __('Performance', 'medikal'),
      'id' => 'vtcore-zeus-performance-panel',
    ));

    $this->panel
      ->setChildrenPointer('content')
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][options][performance][aggregate]',
        'text' => __('Aggregate Assets', 'medikal'),
        'description' => __('Enabling this will force the core to aggregate CSS and Javascript file into a single file, please disable this if you have implemented other
                      aggregation plugin such as W3Total Cache.', 'medikal'),
        'checked' => (boolean) $this->getOption('performance', 'aggregate'),
        'switch' => true,
      )));

    $this->processPanel();


    // Build the performance options
    $this->panel = new VTCore_Bootstrap_Element_BsPanel(array(
      'text' => __('Customizer', 'medikal'),
      'id' => 'vtcore-zeus-customizer-panel',
    ));

    $this->panel
      ->setChildrenPointer('content')
      ->addChildren(new VTCore_Bootstrap_Form_BsDescription(array(
        'text' => __('Enable or disable the Wordpress Customizer panel group, when host server is not powerful enough to
                             display all the panel group at once, it is suggested to disable some of the panel group to accommodate the server capabilities.', 'medikal'),
      )))
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow)
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][options][customizer][general]',
        'text' => __('General Panel', 'medikal'),
        'checked' => (boolean) $this->getOption('customizer', 'general'),
        'switch' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 4,
            'large' => 4,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][options][customizer][style]',
        'text' => __('Style Panel', 'medikal'),
        'checked' => (boolean) $this->getOption('customizer', 'style'),
        'switch' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 4,
            'large' => 4,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][options][customizer][layout]',
        'text' => __('Layout Panel', 'medikal'),
        'checked' => (boolean) $this->getOption('customizer', 'layout'),
        'switch' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 4,
            'large' => 4,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][options][customizer][widgets]',
        'text' => __('Widgets Panel', 'medikal'),
        'checked' => (boolean) $this->getOption('customizer', 'widgets'),
        'switch' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 4,
            'large' => 4,
          ),
        ),
      )));

    if (VTCORE_WOOCOMMERCE_LOADED) {
      $this->panel
        ->lastChild()
        ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
          'name' => 'theme[features][options][customizer][shop]',
          'text' => __('Shop Panel', 'medikal'),
          'checked' => (boolean) $this->getOption('customizer', 'shop'),
          'switch' => true,
          'grids' => array(
            'columns' => array(
              'mobile' => 12,
              'tablet' => 6,
              'small' => 4,
              'large' => 4,
            ),
          ),
        )));
    }

    if (defined('VTCORE_SERVICES_LOADED')) {
      $this->panel
        ->lastChild()
        ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
          'name' => 'theme[features][options][customizer][services]',
          'text' => __('Services Panel', 'medikal'),
          'checked' => (boolean) $this->getOption('customizer', 'services'),
          'switch' => true,
          'grids' => array(
            'columns' => array(
              'mobile' => 12,
              'tablet' => 6,
              'small' => 4,
              'large' => 4,
            ),
          ),
        )));
    }

    if (defined('VTCORE_DEPARTMENT_LOADED')) {
      $this->panel
        ->lastChild()
        ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
          'name' => 'theme[features][options][customizer][department]',
          'text' => __('Department Panel', 'medikal'),
          'checked' => (boolean) $this->getOption('customizer', 'department'),
          'switch' => true,
          'grids' => array(
            'columns' => array(
              'mobile' => 12,
              'tablet' => 6,
              'small' => 4,
              'large' => 4,
            ),
          ),
        )));
    }

    if (defined('VTCORE_MEMBERS_LOADED')) {
      $this->panel
        ->lastChild()
        ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
          'name' => 'theme[features][options][customizer][members]',
          'text' => __('Members Panel', 'medikal'),
          'checked' => (boolean) $this->getOption('customizer', 'members'),
          'switch' => true,
          'grids' => array(
            'columns' => array(
              'mobile' => 12,
              'tablet' => 6,
              'small' => 4,
              'large' => 4,
            ),
          ),
        )));
    }


    if (defined('VTCORE_MEMBERS_LOADED')
        && defined('VTCORE_DEPARTMENT_LOADED')
        && defined('VTCORE_SERVICES_LOADED')) {

      $this->panel
        ->lastChild()
        ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
          'name' => 'theme[features][options][customizer][timetable]',
          'text' => __('Timetable Panel', 'medikal'),
          'checked' => (boolean) $this->getOption('customizer', 'timetable'),
          'switch' => true,
          'grids' => array(
            'columns' => array(
              'mobile' => 12,
              'tablet' => 6,
              'small' => 4,
              'large' => 4,
            ),
          ),
        )));
    }

    $this->processPanel();

  }
}