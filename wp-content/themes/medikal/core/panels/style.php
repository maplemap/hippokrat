<?php
/**
 * Panel section for configuring style options
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Zeus_Panels_Style
extends VTCore_Zeus_Panels_Model {

  private $options = array();
  private $config;



  /**
   * Overriding parent method
   * @see VTCore_Bootstrap_Element_BsElement::buildElement()
   */
  public function buildElement() {

    // Load assets
    VTCore_Wordpress_Utility::loadAsset('bootstrap-colorpicker');
    VTCore_Wordpress_Utility::loadAsset('bootstrap-switch');
    VTCore_Wordpress_Utility::loadAsset('jquery-equalheight');
    VTCore_Wordpress_Utility::loadAsset('jquery-isotope');
    VTCore_Wordpress_Utility::loadAsset('jquery-table-manager');
    VTCore_Wordpress_Utility::loadAsset('wp-gradientpicker');
    VTCore_Wordpress_Utility::loadAsset('wp-ajax');
    VTCore_Wordpress_Utility::loadAsset('wp-media');

    $this->messages = new VTCore_Bootstrap_BsMessages();

    $assistance = new VTCore_Zeus_Assistance();

    // Overload colors with config object
    // allowing us to use dotted notation
    $this->config = new VTCore_Wordpress_Objects_Config();

    $this->getSchemaOptions();

    // Build schema chooser
    $this->panel = new VTCore_Bootstrap_Element_BsPanel(array(
        'id' => 'vtcore-zeus-schema-panel',
        'text' => __('Color Schemas', 'medikal'),
      ));

    $this->panel
      ->setChildrenPointer('content')
      ->addChildren(new VTCore_Bootstrap_Form_BsSelect(array(
        'text' => __('Select Style Schemas', 'medikal'),
        'name' => 'theme[schema-active]',
        'value' => $this->getContext('schema')->getActiveSchemaID(),
        'options' => $this->options,
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsSubmit(array(
        'attributes' => array(
          'name' => 'changeSchemaColor',
          'value' => __('Change Schema', 'medikal'),
          'class' => array(
            'btn', 'btn-primary'
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsSubmit(array(
        'attributes' => array(
          'name' => 'resetSchemaColor',
          'value' => __('Reset Schema', 'medikal'),
        ),
        'mode' => 'danger',
        'confirmation' => true,
        'title' => __('Are you sure? This action is inreversible.', 'medikal'),
      )));

    $this->processPanel();


    // Build color customizer
    $this->buildAccordionContext();

    $this->panel = new VTCore_Bootstrap_Element_BsPanel(array(
      'id' => 'vtcore-zeus-color-panel',
      'text' => __('Customize Schema', 'medikal'),
      'attributes' => array(
        'class' => array('panel-styles')
      ),
    ));

    $this->panel
      ->setChildrenPointer('content')
      ->addContent(new VTCore_Wordpress_Element_WpAccordion($this->config->get('colors')));


    $this->processPanel();

  }


  /**
   * Method for building schema as select element options
   */
  private function getSchemaOptions() {

    foreach ($this->getContext('schema')->getSchemas() as $key => $schema) {
      $this->options[$schema->getSchema('id')] = $schema->getSchema('name');
    }

    return $this;
  }


  /**
   * Main method for converting schema arrays into a
   * valid VTCore_Bootstrap_Element_BsAccordion context
   * array.
   *
   * @return VTCore_Zeus_Panels_Style
   */
  public function buildAccordionContext() {

    // Overload colors with config object
    // allowing us to use dotted notation
    $this->config->merge(array(
      'activeSchemaSection' => $this->getContext('schema')->getActiveSchema()->getData('sections'),
      'activeSchemaColor' => $this->getContext('schema')->getActiveSchema()->getData('color'),
      'activeSchemaID' => $this->getContext('schema')->getActiveSchemaID(),
      'colors' => array(
        'ajax' => true,
        'ajaxData' => array(
          'ajax-loading-text' => __('Retrieving schema section...', 'medikal'),
          'panelObject' => 'VTCore_Zeus_Schema_Panes',
        ),
      ),
    ));

    // Building the color selectors accordion titles
    foreach ($this->config->get('activeSchemaSection') as $parent => $parentData) {

      if (isset($parentData['enabled']) && !$parentData['enabled']) {
        continue;
      }

      $this->config->add('colors.contents.' . $parent . '.title', $parentData['title']);
    }


    // Building the accordion contents.
    foreach ($this->config->get('activeSchemaColor') as $key => $data) {

      // Something wrong with the schema skipping it!.
      if (!isset($data['parent'])
          || !isset($data['title'])
          || !isset($data['description'])
          || !isset($data['selectors'])
          || !$this->config->get('colors.contents.' . $data['parent'])) {

        continue;

      }

      $schema = $data;
      unset($schema['title']);
      unset($schema['description']);
      unset($schema['selectors']);
      unset($schema['parent']);

      $this->config->add('colors.contents.' . $data['parent'] . '.content.' . $key, array(
        'activeSchemaID' => $this->config->get('activeSchemaID'),
        'schemaKey' => $key,
        'title' => $data['title'],
        'description' => $data['description'],
        'schemas' => $schema,
      ));
    }

    return $this;
  }


  public function getConfig() {
    return $this->config;
  }



}