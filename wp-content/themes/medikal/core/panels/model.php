<?php
/**
 * Base super class for the panel content
 *
 * @author jason.xie@victheme.com
 *
 */
abstract class VTCore_Zeus_Panels_Model
extends VTCore_Bootstrap_Element_BsElement {

  protected $messages;
  protected $panel;


  /**
   * Set the proces mode
   */
  public function setMode($mode) {
    $this->mode = $mode;
  }

  /**
   * Retrieve any message from panels
   */
  public function getMessageObject() {

    if (empty($this->messages)) {
      $this->messages = new VTCore_Bootstrap_BsMessages();
    }

    return $this->messages;
  }


  public function getSchema($key, $search) {
    return VTCore_Utility::searchArrayValueByKey($this->getContext('schema')->getActiveSchema()->getData($key), $search);
  }



  public function getShow($page, $key) {
    return $this->getContext('features')->get('show.' . $page . '.' . $key);
  }



  public function getImage($page, $key) {
    return $this->getContext('features')->get('images.' . $page . '.' . $key);
  }



  public function getOption($page, $key) {
    return $this->getContext('features')->get('options.' . $page . '.' . $key);
  }


  public function getColumn($region, $size) {
    $layout = $this->getContext('layout')->findChildren('id', 'region-' . $region);
    $regionData = array_shift($layout);
    return $regionData->getContext('grids.columns.' . $size);
  }



  public function getSidebar($page) {
    return $this->getContext('features')->get('sidebars.' . $page);
  }


  /**
   * Processing form per panels, this is
   * done this way instead of doing a global
   * form processing to save memory peak usage.
   */
  protected function processPanel() {

    // Only process on save for faster performance
    if ($this->getContext('process')) {
      $form = new VTCore_Bootstrap_Form_BsInstance();
      $form->addChildren($this->panel);
      $form
        ->processForm()
        ->processError(false, false);

      $errors = $form->getErrors();

      if (!empty($errors)) {
        foreach ($errors as $error) {
          $this->getMessageObject()->setError($error);
        }
      }

      $form = null;
      unset($form);
      unset($errors);
    }

    // Last chance to alter the regions panel
    // object before it got flattened as string
    do_action('vtcore_zeus_alter_panel_config', $this->panel);

    if ($this->getContext('build') == 'object') {
      $this->addChildren($this->panel);
    }

    else {
      $this->addChildren($this->panel->__toString());
    }

    unset($this->panel);

    return $this;
  }
}