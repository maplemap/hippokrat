<?php
/**
 * Panel section for configuring layout options
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Panels_Layout
extends VTCore_Zeus_Panels_Model {


  /**
   * Panel content
   */
  public function buildElement() {

    $this->assistance = new VTCore_Zeus_Assistance();

    // Page width and style
    $this->panel = new VTCore_Bootstrap_Element_BsPanel(array(
      'id' => 'vtcore-zeus-layout-panel',
      'text' => __('Page Layout', 'medikal'),
    ));

    $this->panel
      ->setChildrenPointer('content')
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsSelect(array(
        'text' => __('Maximum Width', 'medikal'),
        'description' => __('Define the maximum page width for the theme.', 'medikal'),
        'name' => 'theme[features][options][page][maxwidth]',
        'value' => $this->getOption('page', 'maxwidth'),
        'options' => array(
          'max-960' => '960px',
          'max-1170' => '1170px',
          'max-fluid' => __('Fluid', 'medikal')
        ),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsSelect(array(
        'text' => __('Page Style', 'medikal'),
        'description' => __('Define the main styling for the theme page.', 'medikal'),
        'name' => 'theme[features][options][page][style]',
        'value' => $this->getOption('page', 'style'),
        'options' => array(
          'style-normal' => __('Normal', 'medikal'),
          'style-boxed' => __('Boxed', 'medikal')
        ),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )));


    $this->processPanel();


    // Building the sidebar configuration
    $this->panel = new VTCore_Bootstrap_Element_BsPanel(array(
      'id' => 'vtcore-zeus-sidebar-panel',
      'text' => __('Sidebar Position', 'medikal'),
    ));

    $this->panel
      ->setChildrenPointer('content')
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsSelect(array(
        'text' => __('Teaser Listing Page', 'medikal'),
        'description' => __('This settings will be applied to all teasers listing pages', 'medikal'),
        'name' => 'theme[features][sidebars][teaser]',
        'value' => $this->getSidebar('teaser'),
        'options' => array(
          'none' => __('No Sidebar', 'medikal'),
          'left' => __('Left', 'medikal'),
          'right' => __('Right', 'medikal'),
        ),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 4,
            'small' => 4,
            'large' => 4,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsSelect(array(
        'text' => __('Single Post', 'medikal'),
        'description' => __('This settings will be applied to post single page', 'medikal'),
        'name' => 'theme[features][sidebars][full]',
        'value' => $this->getSidebar('full'),
        'options' => array(
          'none' => __('No Sidebar', 'medikal'),
          'left' => __('Left', 'medikal'),
          'right' => __('Right', 'medikal'),
        ),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 4,
            'small' => 4,
            'large' => 4,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsSelect(array(
        'text' => __('Page 404', 'medikal'),
        'description' => __('This settings will be applied to page 404 only.', 'medikal'),
        'name' => 'theme[features][sidebars][notfound]',
        'value' => $this->getSidebar('notfound'),
        'options' => array(
          'none' => __('No Sidebar', 'medikal'),
          'left' => __('Left', 'medikal'),
          'right' => __('Right', 'medikal'),
        ),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 4,
            'small' => 4,
            'large' => 4,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsSelect(array(
        'text' => __('Single Page', 'medikal'),
        'description' => __('This settings will be applied to single page only.', 'medikal'),
        'name' => 'theme[features][sidebars][page]',
        'value' => $this->getSidebar('page'),
        'attributes' => array(
          'class' => array(
            'clear',
          ),
        ),
        'options' => array(
          'none' => __('No Sidebar', 'medikal'),
          'left' => __('Left', 'medikal'),
          'right' => __('Right', 'medikal'),
        ),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 4,
            'small' => 4,
            'large' => 4,
          ),
        ),
      )));


    if (VTCORE_WOOCOMMERCE_LOADED) {

      $this->panel
        ->lastChild()
        ->addChildren(new VTCore_Bootstrap_Form_BsSelect(array(
          'text' => __('Product Archive', 'medikal'),
          'description' => __('This settings will be applied to product archives.', 'medikal'),
          'name' => 'theme[features][sidebars][product_archive]',
          'value' => $this->getSidebar('product_archive'),
          'attributes' => array(
            'class' => array(
              'clearfix',
            ),
          ),
          'options' => array(
            'none' => __('No Sidebar', 'medikal'),
            'left' => __('Left', 'medikal'),
            'right' => __('Right', 'medikal'),
          ),
          'grids' => array(
            'columns' => array(
              'mobile' => 12,
              'tablet' => 4,
              'small' => 4,
              'large' => 4,
            ),
          ),
        )))
        ->addChildren(new VTCore_Bootstrap_Form_BsSelect(array(
          'text' => __('Product', 'medikal'),
          'description' => __('This settings will be applied to product single page.', 'medikal'),
          'name' => 'theme[features][sidebars][product]',
          'value' => $this->getSidebar('product'),
          'attributes' => array(
            'class' => array(
              'clearfix',
            ),
          ),
          'options' => array(
            'none' => __('No Sidebar', 'medikal'),
            'left' => __('Left', 'medikal'),
            'right' => __('Right', 'medikal'),
          ),
          'grids' => array(
            'columns' => array(
              'mobile' => 12,
              'tablet' => 4,
              'small' => 4,
              'large' => 4,
            ),
          ),
        )))
        ->addChildren(new VTCore_Bootstrap_Form_BsSelect(array(
          'text' => __('Shop Pages', 'medikal'),
          'description' => __('This settings will be applied to shop pages such as cart, checkout, my account etc.', 'medikal'),
          'name' => 'theme[features][sidebars][shop_pages]',
          'value' => $this->getSidebar('shop_pages'),
          'attributes' => array(
            'class' => array(
              'clearfix',
            ),
          ),
          'options' => array(
            'none' => __('No Sidebar', 'medikal'),
            'left' => __('Left', 'medikal'),
            'right' => __('Right', 'medikal'),
          ),
          'grids' => array(
            'columns' => array(
              'mobile' => 12,
              'tablet' => 4,
              'small' => 4,
              'large' => 4,
            ),
          ),
        )));
    }


    if (defined('VTCORE_DEPARTMENT_LOADED')) {

      $this->panel
        ->lastChild()
        ->addChildren(new VTCore_Bootstrap_Form_BsSelect(array(
          'text' => __('Department Archive', 'medikal'),
          'description' => __('This settings will be applied to department post archives.', 'medikal'),
          'name' => 'theme[features][sidebars][department_archive]',
          'value' => $this->getSidebar('department_archive'),
          'attributes' => array(
            'class' => array(
              'clearfix',
            ),
          ),
          'options' => array(
            'none' => __('No Sidebar', 'medikal'),
            'left' => __('Left', 'medikal'),
            'right' => __('Right', 'medikal'),
          ),
          'grids' => array(
            'columns' => array(
              'mobile' => 12,
              'tablet' => 4,
              'small' => 4,
              'large' => 4,
            ),
          ),
        )))
        ->addChildren(new VTCore_Bootstrap_Form_BsSelect(array(
          'text' => __('Department Single', 'medikal'),
          'description' => __('This settings will be applied to department single page.', 'medikal'),
          'name' => 'theme[features][sidebars][department]',
          'value' => $this->getSidebar('department'),
          'attributes' => array(
            'class' => array(
              'clearfix',
            ),
          ),
          'options' => array(
            'none' => __('No Sidebar', 'medikal'),
            'left' => __('Left', 'medikal'),
            'right' => __('Right', 'medikal'),
          ),
          'grids' => array(
            'columns' => array(
              'mobile' => 12,
              'tablet' => 4,
              'small' => 4,
              'large' => 4,
            ),
          ),
        )));
    }
    
    if (defined('VTCORE_SERVICES_LOADED')) {

      $this->panel
        ->lastChild()
        ->addChildren(new VTCore_Bootstrap_Form_BsSelect(array(
          'text' => __('Services Archive', 'medikal'),
          'description' => __('This settings will be applied to services post archives.', 'medikal'),
          'name' => 'theme[features][sidebars][services_archive]',
          'value' => $this->getSidebar('services_archive'),
          'attributes' => array(
            'class' => array(
              'clearfix',
              'clearboth',
            ),
          ),
          'options' => array(
            'none' => __('No Sidebar', 'medikal'),
            'left' => __('Left', 'medikal'),
            'right' => __('Right', 'medikal'),
          ),
          'grids' => array(
            'columns' => array(
              'mobile' => 12,
              'tablet' => 4,
              'small' => 4,
              'large' => 4,
            ),
          ),
        )))
        ->addChildren(new VTCore_Bootstrap_Form_BsSelect(array(
          'text' => __('Services Single', 'medikal'),
          'description' => __('This settings will be applied to services single page.', 'medikal'),
          'name' => 'theme[features][sidebars][services]',
          'value' => $this->getSidebar('services'),
          'attributes' => array(
            'class' => array(
              'clearfix',
            ),
          ),
          'options' => array(
            'none' => __('No Sidebar', 'medikal'),
            'left' => __('Left', 'medikal'),
            'right' => __('Right', 'medikal'),
          ),
          'grids' => array(
            'columns' => array(
              'mobile' => 12,
              'tablet' => 4,
              'small' => 4,
              'large' => 4,
            ),
          ),
        )));
    }

    if (defined('VTCORE_MEMBERS_LOADED')) {

      $this->panel
        ->lastChild()
        ->addChildren(new VTCore_Bootstrap_Form_BsSelect(array(
          'text' => __('Members Archive', 'medikal'),
          'description' => __('This settings will be applied to members post archives.', 'medikal'),
          'name' => 'theme[features][sidebars][members_archive]',
          'value' => $this->getSidebar('members_archive'),
          'attributes' => array(
            'class' => array(
              'clearfix',
            ),
          ),
          'options' => array(
            'none' => __('No Sidebar', 'medikal'),
            'left' => __('Left', 'medikal'),
            'right' => __('Right', 'medikal'),
          ),
          'grids' => array(
            'columns' => array(
              'mobile' => 12,
              'tablet' => 4,
              'small' => 4,
              'large' => 4,
            ),
          ),
        )))
        ->addChildren(new VTCore_Bootstrap_Form_BsSelect(array(
          'text' => __('Members Single', 'medikal'),
          'description' => __('This settings will be applied to members single page.', 'medikal'),
          'name' => 'theme[features][sidebars][members]',
          'value' => $this->getSidebar('members'),
          'attributes' => array(
            'class' => array(
              'clearfix',
            ),
          ),
          'options' => array(
            'none' => __('No Sidebar', 'medikal'),
            'left' => __('Left', 'medikal'),
            'right' => __('Right', 'medikal'),
          ),
          'grids' => array(
            'columns' => array(
              'mobile' => 12,
              'tablet' => 4,
              'small' => 4,
              'large' => 4,
            ),
          ),
        )));
    }
    

    $this->processPanel();


    // Build the grid panel
    $this->buildGridForm()->processPanel();

  }


  protected function buildGridForm() {

    $this->panel = new VTCore_Bootstrap_Element_BsElement(array(
      'id' => 'vtcore-grid-form-target',
      'type' => 'div',
      'attributes' => array(
        'id' => 'vtcore-grid-form-target',
      ),
    ));

    // Building the layout configuration per area and per region
    foreach ($this->getContext('layout')->getChildrens() as $area) {

      $regions = $area->getChildrens();
      if (empty($regions) || count($regions) < 2) {
        continue;
      }

      $this->panel->addChildren(new VTCore_Bootstrap_Element_BsPanel(array(
        'id' => 'vtcore-zeus-area-' . str_replace(array(
              ' ',
              '_'
            ), '-', $area->getContext('id')) . '-panel',
        'text' => sprintf(__('%s Column Grid', 'medikal'), $area->getContext('name')),
      )));

      $this->panel
        ->lastChild()
        ->setChildrenPointer('content')
        ->addChildren(new VTCore_Bootstrap_Grid_BsRow());

      $grid = 12 / count($regions);

      // Don't attempt to stringify the regions!
      // It will break validation.
      foreach ($regions as $region) {
        $this->panel
          ->lastChild()
          ->lastChild()
          ->addChildren(new VTCore_Bootstrap_Grid_BsColumn(array(
            'grids' => array(
              'columns' => array(
                'mobile' => 12,
                'tablet' => $grid,
                'small' => $grid,
                'large' => $grid,
              ),
            ),
          )))
          ->lastChild()
          ->addChildren(new VTCore_Bootstrap_Form_BsGrids(array(
            'name' => 'theme[regions][' . $region->getContext('arguments.id') . '][grids]',
            'columns' => array(
              'text' => sprintf(__('%s Region', 'medikal'), $region->getContext('arguments.name')),
            ),
            'value' => $region->getContext('grids'),
            'element_grids' => array(
              'columns' => array(
                'mobile' => '12',
                'tablet' => '12',
                'small' => '12',
                'large' => '12',
              ),
            ),
          )));

      }
    }

    return $this;
  }


}