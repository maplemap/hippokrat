<?php

/**
 * Panel section for configuring general options
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Panels_Shop
  extends VTCore_Zeus_Panels_Model {


  /**
   * Overriding parent method
   * @see VTCore_Bootstrap_Element_BsElement::buildElement()
   */
  public function buildElement() {

    $assistance = new VTCore_Zeus_Assistance();


    // Build the show / hide elements
    $this->panel = new VTCore_Bootstrap_Element_BsPanel(array(
      'text' => __('Product Teaser Options', 'medikal'),
      'id' => 'vtcore-zeus-shop-catalog',
      'attributes' => array(
        'class' => array(
          'switch-column-two',
        ),
      ),
    ));

    $this->panel
      ->setChildrenPointer('content')
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][product][teaser][image]',
        'text' => __('Show Image', 'medikal'),
        'description' => __('Show / hide the teaser image.', 'medikal'),
        'checked' => (boolean) $this->getShow('product.teaser', 'image'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][product][teaser][title]',
        'text' => __('Show Title', 'medikal'),
        'description' => __('Show / hide the teaser title.', 'medikal'),
        'checked' => (boolean) $this->getShow('product.teaser', 'title'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][product][teaser][excerpt]',
        'text' => __('Show Excerpt', 'medikal'),
        'description' => __('Show / hide the teaser excerpt.', 'medikal'),
        'checked' => (boolean) $this->getShow('product.teaser', 'excerpt'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][product][teaser][price]',
        'text' => __('Show Price', 'medikal'),
        'description' => __('Show / hide the teaser price.', 'medikal'),
        'checked' => (boolean) $this->getShow('product.teaser', 'price'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][product][teaser][flash]',
        'text' => __('Show Flash', 'medikal'),
        'description' => __('Show / hide the teaser flash.', 'medikal'),
        'checked' => (boolean) $this->getShow('product.teaser', 'flash'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][product][teaser][rating]',
        'text' => __('Show Rating', 'medikal'),
        'description' => __('Show / hide the teaser rating.', 'medikal'),
        'checked' => (boolean) $this->getShow('product.teaser', 'rating'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][product][teaser][cart]',
        'text' => __('Show Cart', 'medikal'),
        'description' => __('Show / hide the teaser cart.', 'medikal'),
        'checked' => (boolean) $this->getShow('product.teaser', 'cart'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][product][catalog][counter]',
        'text' => __('Show Counter', 'medikal'),
        'description' => __('Show / hide the displayed product counter element.', 'medikal'),
        'checked' => (boolean) $this->getShow('product.catalog', 'counter'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][product][catalog][sorter]',
        'text' => __('Show Product Sort', 'medikal'),
        'description' => __('Show / hide the displayed product sorter field.', 'medikal'),
        'checked' => (boolean) $this->getShow('product.catalog', 'sorter'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][options][products][archive][ajax]',
        'text' => __('Enable Ajax', 'medikal'),
        'description' => __('Use ajax for pagination and filtering', 'medikal'),
        'checked' => (boolean) $this->getOption('products.archive', 'ajax'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][options][products][archive][pager][mini]',
        'text' => __('Use Mini Pager', 'medikal'),
        'description' => __('Mini pager with only previous and next button, disable the infinite pager for this to work.', 'medikal'),
        'checked' => (boolean) $this->getOption('products.archive.pager', 'mini'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][options][products][archive][pager][infinite]',
        'text' => __('Use Infinite Pager', 'medikal'),
        'description' => __('Infinite pager will hide the pager elements and autofetch the content when user scroll.', 'medikal'),
        'checked' => (boolean) $this->getOption('products.archive.pager', 'infinite'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->getParent()
      ->addChildren(new VTCore_Bootstrap_Form_BsSelect(array(
        'text' => __('Teaser Template', 'medikal'),
        'name' => 'theme[features][options][products][archive][template]',
        'value' => $this->getOption('products.archive', 'template'),
        'description' => __('Select the teaser template to use for the product archive items', 'medikal'),
        'options' => array(
          'detail' => __('Grid', 'medikal'),
          'normal' => __('Grid Simple', 'medikal'),
          'flip' => __('Flip', 'medikal'),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsGrids(array(
        'name' => 'theme[features][options][products][archive][grids]',
        'value' => $this->getOption('products.archive', 'grids'),
        'columns' => array(
          'text' => __('Product Item Grid Column Size', 'medikal'),
          'description' => __('Use Column to define the product item width, The total grid per row is 12 as specified by Bootstrap grid system.', 'medikal'),
        ),
        'element_grids' => array(
          'columns' => array(
            'mobile' => '12',
            'tablet' => '6',
            'small' => '6',
            'large' => '6',
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'name' => 'theme[features][options][products][archive][404][title]',
        'text' => __('Not Found Title', 'medikal'),
        'description' => __('Title for the not found box', 'medikal'),
        'value' => $this->getOption('products.archive.404', 'title'),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsTextarea(array(
        'name' => 'theme[features][options][products][archive][404][description]',
        'text' => __('Not Found Description', 'medikal'),
        'description' => __('Text description for the not found box', 'medikal'),
        'value' => $this->getOption('products.archive.404', 'description'),
        'raw' => TRUE,
      )));


    $this->processPanel();


    // Product Settings
    $this->panel = new VTCore_Bootstrap_Element_BsPanel(array(
      'text' => __('Product Single Settings', 'medikal'),
      'id' => 'vtcore-zeus-product-single-settings',
    ));

    $this->panel
      ->setChildrenPointer('content')
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][product][full][flash]',
        'text' => __('Show Flash', 'medikal'),
        'description' => __('Show / hide the single product sale flash.', 'medikal'),
        'checked' => (boolean) $this->getShow('product.full', 'flash'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][product][full][image]',
        'text' => __('Show Image', 'medikal'),
        'description' => __('Show / hide the single product images.', 'medikal'),
        'checked' => (boolean) $this->getShow('product.full', 'image'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][product][full][title]',
        'text' => __('Show Title', 'medikal'),
        'description' => __('Show / hide the title.', 'medikal'),
        'checked' => (boolean) $this->getShow('product.full', 'title'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][product][full][rating]',
        'text' => __('Show Rating', 'medikal'),
        'description' => __('Show / hide the single product rating.', 'medikal'),
        'checked' => (boolean) $this->getShow('product.full', 'rating'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][product][full][price]',
        'text' => __('Show Price', 'medikal'),
        'description' => __('Show / hide the single product price.', 'medikal'),
        'checked' => (boolean) $this->getShow('product.full', 'price'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][product][full][excerpt]',
        'text' => __('Show Excerpt', 'medikal'),
        'description' => __('Show / hide the single product excerpt.', 'medikal'),
        'checked' => (boolean) $this->getShow('product.full', 'excerpt'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][product][full][cart]',
        'text' => __('Show Cart', 'medikal'),
        'description' => __('Show / hide the single product cart.', 'medikal'),
        'checked' => (boolean) $this->getShow('product.full', 'cart'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][product][full][meta]',
        'text' => __('Show Meta', 'medikal'),
        'description' => __('Show / hide the single product meta.', 'medikal'),
        'checked' => (boolean) $this->getShow('product.full', 'meta'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][product][full][share]',
        'text' => __('Show Sharing', 'medikal'),
        'description' => __('Show / hide the single product share.', 'medikal'),
        'checked' => (boolean) $this->getShow('product.full', 'share'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][product][full][tabs]',
        'text' => __('Show Tabs', 'medikal'),
        'description' => __('Show / hide the single product tabs.', 'medikal'),
        'checked' => (boolean) $this->getShow('product.full', 'tabs'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][product][full][upsell]',
        'text' => __('Show Upsell', 'medikal'),
        'description' => __('Show / hide the single product upsell.', 'medikal'),
        'checked' => (boolean) $this->getShow('product.full', 'upsell'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][product][full][related]',
        'text' => __('Show Related', 'medikal'),
        'description' => __('Show / hide the single product related.', 'medikal'),
        'checked' => (boolean) $this->getShow('product.full', 'related'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsGrids(array(
        'name' => 'theme[features][options][products][single][image][grids]',
        'value' => $this->getOption('products.single.image', 'grids'),
        'columns' => array(
          'text' => __('Image Slider Column Grid', 'medikal'),
          'description' => __('Use Column to define the image width. The total grid for a single row is 12, the image and summary element is in a single row.', 'medikal'),
        ),
        'element_grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 12,
            'small' => 12,
            'large' => 12,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsGrids(array(
        'name' => 'theme[features][options][products][single][summary][grids]',
        'text' => __('Summary Grids', 'medikal'),
        'description' => __('Use Column to define the product summary box width.', 'medikal'),
        'value' => $this->getOption('products.single.summary', 'grids'),
        'columns' => array(
          'text' => __('Product Summary Column Grid', 'medikal'),
          'description' => __('Use Column to define the product summary box width. The total grid for a single row is 12, the image and summary element is in a single row.', 'medikal'),
        ),
        'element_grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 12,
            'small' => 12,
            'large' => 12,
          ),
        ),
      )))
      ->getParent()
      ->addChildren(new VTCore_Bootstrap_Element_BsHeader(array(
        'tag' => 'h3',
        'text' => __('Image Size', 'medikal'),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsDescription(array(
        'text' => __('The image size used for the product single image, full screen display and zoomed state.', 'medikal'),
      )))
      ->addChildren(new VTCore_Wordpress_Form_WpImageSize(array(
        'name' => 'theme[features][options][products][single][normal]',
        'text' => __('Normal Size', 'medikal'),
        'description' => __('The product single image normal size', 'medikal'),
        'value' => $this->getOption('products.single', 'normal'),
      )))
      ->addChildren(new VTCore_Wordpress_Form_WpImageSize(array(
        'name' => 'theme[features][options][products][single][zoom]',
        'text' => __('Zoomed Size', 'medikal'),
        'description' => __('The product single image zoomed size, it is recommended to use at least 3x larger image from normal size for zoomed state', 'medikal'),
        'value' => $this->getOption('products.single', 'zoom'),
      )))
      ->addChildren(new VTCore_Wordpress_Form_WpImageSize(array(
        'name' => 'theme[features][options][products][single][fullscreen]',
        'text' => __('Full Screen Size', 'medikal'),
        'description' => __('The product single image fullscreen size', 'medikal'),
        'value' => $this->getOption('products.single', 'fullscreen'),
      )))
      ->addChildren(new VTCore_Bootstrap_Element_BsHeader(array(
        'tag' => 'h3',
        'text' => __('Fotorama Options', 'medikal'),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsDescription(array(
        'text' => __('Configure the fotorama gallery for displaying the product single images', 'medikal'),
      )))
      ->addChildren(new VTCore_Wordpress_Form_WpFotorama(array(
        'name' => 'theme[features][options][products][single]',
        'fotorama' => $this->getOption('products.single', 'fotorama'),
      )))
      ->addChildren(new VTCore_Bootstrap_Element_BsHeader(array(
        'tag' => 'h3',
        'text' => __('Elevate Zoom', 'medikal'),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsDescription(array(
        'text' => __('Configure the product image zooming features.', 'medikal'),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][options][products][single][fotorama][hoverzoom]',
        'text' => __('Enable Zooming', 'medikal'),
        'description' => __('Disable or enable the image zooming feature', 'medikal'),
        'checked' => (boolean) $this->getOption('products.single.fotorama', 'hoverzoom'),
        'switch' => TRUE,
      )))
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()
      ->addChildren(new VTCore_jQuery_Form_Easing(array(
        'name' => 'theme[features][options][products][single][elevatezoom][easingType]',
        'text' => __('Easing Type', 'medikal'),
        'value' => $this->getOption('products.single.elevatezoom', 'easingType'),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'name' => 'theme[features][options][products][single][elevatezoom][easingDuration]',
        'text' => __('Easing Duration', 'medikal'),
        'value' => $this->getOption('products.single.elevatezoom', 'easingDuration'),
        'suffix' => __('Miliseconds', 'medikal'),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'name' => 'theme[features][options][products][single][elevatezoom][lensSize]',
        'text' => __('Lens Size', 'medikal'),
        'value' => $this->getOption('products.single.elevatezoom', 'lensSize'),
        'suffix' => 'px',
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'name' => 'theme[features][options][products][single][elevatezoom][zoomWindowWidth]',
        'text' => __('Zoom Window Width', 'medikal'),
        'value' => $this->getOption('products.single.elevatezoom', 'zoomWindowWidth'),
        'suffix' => 'px',
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'name' => 'theme[features][options][products][single][elevatezoom][zoomWindowHeight]',
        'text' => __('Zoom Window Height', 'medikal'),
        'value' => $this->getOption('products.single.elevatezoom', 'zoomWindowHeight'),
        'suffix' => 'px',
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'name' => 'theme[features][options][products][single][elevatezoom][borderSize]',
        'text' => __('Zoom Window Border Size', 'medikal'),
        'value' => $this->getOption('products.single.elevatezoom', 'borderSize'),
        'suffix' => 'px',
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsColor(array(
        'name' => 'theme[features][options][products][single][elevatezoom][borderColour]',
        'text' => __('Border Color', 'medikal'),
        'value' => $this->getOption('products.single.elevatezoom', 'borderColour'),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'name' => 'theme[features][options][products][single][elevatezoom][lensBorder]',
        'text' => __('Lens Border', 'medikal'),
        'value' => $this->getOption('products.single.elevatezoom', 'lensBorder'),
        'suffix' => 'px',
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsSelect(array(
        'name' => 'theme[features][options][products][single][elevatezoom][lensShape]',
        'text' => __('Lens Shape', 'medikal'),
        'value' => $this->getOption('products.single.elevatezoom', 'lensShape'),
        'options' => array(
          'square' => __('Square', 'medikal'),
          'round' => __('Round', 'medikal'),
        ),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsColor(array(
        'name' => 'theme[features][options][products][single][elevatezoom][lensColour]',
        'text' => __('Lens Colour', 'medikal'),
        'value' => $this->getOption('products.single.elevatezoom', 'lensColour'),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'name' => 'theme[features][options][products][single][elevatezoom][lensOpacity]',
        'text' => __('Lens Opacity', 'medikal'),
        'value' => $this->getOption('products.single.elevatezoom', 'lensOpacity'),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))

      ->getParent()
      ->addChildren(new VTCore_Bootstrap_Element_BsHeader(array(
        'tag' => 'h3',
        'text' => __('Upsell Element', 'medikal'),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsDescription(array(
        'text' => __('The upsell element configuration for product single page.', 'medikal'),
      )))
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'name' => 'theme[features][options][products][single][upsell][title]',
        'text' => __('Title', 'medikal'),
        'value' => $this->getOption('products.single.upsell', 'title'),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsSelect(array(
        'name' => 'theme[features][options][products][single][upsell][layout]',
        'text' => __('Layout', 'medikal'),
        'value' => $this->getOption('products.single.upsell', 'layout'),
        'options' => array(
          'fitrow' => __('Fit Rows', 'medikal'),
          'masonry' => __('Masonry', 'medikal'),
        ),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->getParent()
      ->addChildren(new VTCore_Bootstrap_Element_BsHeader(array(
        'tag' => 'h3',
        'text' => __('Upsell Teaser Element', 'medikal'),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsDescription(array(
        'text' => __('The upsell teaser element configuration for product single page.', 'medikal'),
      )))
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][options][products][single][upsell][teaser][show][image]',
        'text' => __('Show Image', 'medikal'),
        'checked' => (boolean) $this->getOption('products.single.upsell.teaser.show', 'image'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][options][products][single][upsell][teaser][show][title]',
        'text' => __('Show Title', 'medikal'),
        'checked' => (boolean) $this->getOption('products.single.upsell.teaser.show', 'title'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][options][products][single][upsell][teaser][show][price]',
        'text' => __('Show Price', 'medikal'),
        'checked' => (boolean) $this->getOption('products.single.upsell.teaser.show', 'price'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][options][products][single][upsell][teaser][show][flash]',
        'text' => __('Show Flash Banner', 'medikal'),
        'checked' => (boolean) $this->getOption('products.single.upsell.teaser.show', 'flash'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][options][products][single][upsell][teaser][show][rating]',
        'text' => __('Show Rating', 'medikal'),
        'checked' => (boolean) $this->getOption('products.single.upsell.teaser.show', 'rating'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][options][products][single][upsell][teaser][show][cart]',
        'text' => __('Show Cart', 'medikal'),
        'checked' => (boolean) $this->getOption('products.single.upsell.teaser.show', 'cart'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->getParent()
      ->addChildren(new VTCore_Bootstrap_Form_BsGrids(array(
        'name' => 'theme[features][options][products][single][upsell][teaser][grids]',
        'value' => $this->getOption('products.single.upsell.teaser', 'grids'),
        'columns' => array(
          'text' => __('Columns', 'medikal'),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Element_BsHeader(array(
        'tag' => 'h3',
        'text' => __('Related Element', 'medikal'),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsDescription(array(
        'text' => __('The related element configuration for product single page.', 'medikal'),
      )))
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'name' => 'theme[features][options][products][single][related][title]',
        'text' => __('Title', 'medikal'),
        'value' => $this->getOption('products.single.related', 'title'),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsSelect(array(
        'name' => 'theme[features][options][products][single][related][layout]',
        'text' => __('Layout', 'medikal'),
        'value' => $this->getOption('products.single.related', 'layout'),
        'options' => array(
          'fitrow' => __('Fit Rows', 'medikal'),
          'masonry' => __('Masonry', 'medikal'),
        ),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->getParent()
      ->addChildren(new VTCore_Bootstrap_Element_BsHeader(array(
        'tag' => 'h3',
        'text' => __('Related Teaser Element', 'medikal'),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsDescription(array(
        'text' => __('The related teaser element configuration for product single page.', 'medikal'),
      )))
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][options][products][single][related][teaser][show][image]',
        'text' => __('Show Image', 'medikal'),
        'checked' => (boolean) $this->getOption('products.single.related.teaser.show', 'image'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][options][products][single][related][teaser][show][title]',
        'text' => __('Show Title', 'medikal'),
        'checked' => (boolean) $this->getOption('products.single.related.teaser.show', 'title'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][options][products][single][related][teaser][show][price]',
        'text' => __('Show Price', 'medikal'),
        'checked' => (boolean) $this->getOption('products.single.related.teaser.show', 'price'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][options][products][single][related][teaser][show][flash]',
        'text' => __('Show Flash Banner', 'medikal'),
        'checked' => (boolean) $this->getOption('products.single.related.teaser.show', 'flash'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][options][products][single][related][teaser][show][rating]',
        'text' => __('Show Rating', 'medikal'),
        'checked' => (boolean) $this->getOption('products.single.related.teaser.show', 'rating'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][options][products][single][related][teaser][show][cart]',
        'text' => __('Show Cart', 'medikal'),
        'checked' => (boolean) $this->getOption('products.single.related.teaser.show', 'cart'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->getParent()
      ->addChildren(new VTCore_Bootstrap_Form_BsGrids(array(
        'name' => 'theme[features][options][products][single][related][teaser][grids]',
        'value' => $this->getOption('products.single.related.teaser', 'grids'),
        'columns' => array(
          'text' => __('Columns', 'medikal'),
        ),
      )));

    $this->processPanel();

    // Cart Page Settings
    $this->panel = new VTCore_Bootstrap_Element_BsPanel(array(
      'text' => __('Cart Pages Settings', 'medikal'),
      'id' => 'vtcore-zeus-cart-pages-settings',
    ));

    $this->panel
      ->setChildrenPointer('content')
      ->addChildren(new VTCore_Bootstrap_Element_BsHeader(array(
        'tag' => 'h3',
        'text' => __('Cross Sell Element', 'medikal'),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsDescription(array(
        'text' => __('The cross sell elements in the checkout cart page.', 'medikal'),
      )))
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'name' => 'theme[features][options][products][cart][cross][title]',
        'text' => __('Title', 'medikal'),
        'value' => $this->getOption('products.cart.cross', 'title'),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsSelect(array(
        'name' => 'theme[features][options][products][cart][cross][layout]',
        'text' => __('Layout', 'medikal'),
        'value' => $this->getOption('products.cart.cross', 'layout'),
        'options' => array(
          'fitrow' => __('Fit Rows', 'medikal'),
          'masonry' => __('Masonry', 'medikal'),
        ),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->getParent()
      ->addChildren(new VTCore_Bootstrap_Element_BsHeader(array(
        'tag' => 'h3',
        'text' => __('Cross Sell Teaser Element', 'medikal'),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsDescription(array(
        'text' => __('The cross sell teaser element configuration for product cart page.', 'medikal'),
      )))
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][options][products][cart][cross][teaser][show][image]',
        'text' => __('Show Image', 'medikal'),
        'checked' => (boolean) $this->getOption('products.cart.cross.teaser.show', 'image'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][options][products][cart][cross][teaser][show][title]',
        'text' => __('Show Title', 'medikal'),
        'checked' => (boolean) $this->getOption('products.cart.cross.teaser.show', 'title'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][options][products][cart][cross][teaser][show][price]',
        'text' => __('Show Price', 'medikal'),
        'checked' => (boolean) $this->getOption('products.cart.cross.teaser.show', 'price'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][options][products][cart][cross][teaser][show][flash]',
        'text' => __('Show Flash Banner', 'medikal'),
        'checked' => (boolean) $this->getOption('products.cart.cross.teaser.show', 'flash'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][options][products][cart][cross][teaser][show][rating]',
        'text' => __('Show Rating', 'medikal'),
        'checked' => (boolean) $this->getOption('products.cart.cross.teaser.show', 'rating'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][options][products][cart][cross][teaser][show][cart]',
        'text' => __('Show Cart', 'medikal'),
        'checked' => (boolean) $this->getOption('products.cart.cross.teaser.show', 'cart'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->getParent()
      ->addChildren(new VTCore_Bootstrap_Form_BsGrids(array(
        'name' => 'theme[features][options][products][cart][cross][teaser][grids]',
        'value' => $this->getOption('products.cart.cross.teaser', 'grids'),
        'columns' => array(
          'text' => __('Columns', 'medikal'),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Element_BsHeader(array(
        'tag' => 'h3',
        'text' => __('Cart Total Element', 'medikal'),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsDescription(array(
        'text' => __('The cart totals element configuration for product cart page.', 'medikal'),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'name' => 'theme[features][options][products][cart][total][title]',
        'text' => __('Title', 'medikal'),
        'value' => $this->getOption('products.cart.total', 'title'),
      )));

    $this->processPanel();
  }

  /**
   * Overriding parent method
   * @return $this|void
   */
  protected function processPanel() {

    // Remove unsupported options when on header simple mode
    if ($this->getOption('header', 'template') == 'simple') {

      switch ($this->panel->getContext('id')) {

        case 'vtcore-zeus-offcanvas-panel' :
        case 'vtcore-zeus-header-panel' :
          $this->panel = FALSE;
          break;

        case 'vtcore-zeus-show-panel' :

          $remove = array(
            'theme[features][show][header][cart]',
            'theme[features][show][header][search]',
          );

          $objects = $this->panel->findChildren('attributes', 'name', $remove);

          if (!empty($objects)) {
            foreach ($objects as $object) {
              $id = $object->getParent()->getParent()->getMachineID();
              $object->getParent()
                ->getParent()
                ->getParent()
                ->getContent()
                ->removeChildren($id);
              break;
            }
          }

          $objects = $this->panel->findChildren('context', 'id', 'header-element');

          if (!empty($objects)) {
            foreach ($objects as $object) {
              $object->getParent()
                ->getContent()
                ->removeChildren($object->getMachineID());
            }
          }

          break;
      }
    }

    // Remove unsupported options when on header megamenu mode
    if ($this->getOption('header', 'template') == 'megamenu') {

      switch ($this->panel->getContext('id')) {

        case 'vtcore-zeus-offcanvas-panel' :
        case 'vtcore-zeus-header-panel' :
          $this->panel = FALSE;
          break;

        case 'vtcore-zeus-show-panel' :

          $remove = array(
            'theme[features][show][header][cart]',
            'theme[features][show][header][search]',
          );

          $objects = $this->panel->findChildren('attributes', 'name', $remove);

          if (!empty($objects)) {
            foreach ($objects as $object) {
              $id = $object->getParent()->getParent()->getMachineID();
              $object->getParent()
                ->getParent()
                ->getParent()
                ->getContent()
                ->removeChildren($id);
              break;
            }
          }

          $objects = $this->panel->findChildren('context', 'id', 'header-element');

          if (!empty($objects)) {
            foreach ($objects as $object) {
              $object->getParent()
                ->getContent()
                ->removeChildren($object->getMachineID());
            }
          }

          break;
      }
    }

    if (!empty($this->panel) && is_object($this->panel)) {
      parent::processPanel();
    }
  }

}