<?php
/**
 * Panel section for configuring general options
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Panels_Members
extends VTCore_Zeus_Panels_Model {


  /**
   * Overriding parent method
   * @see VTCore_Bootstrap_Element_BsElement::buildElement()
   */
  public function buildElement() {

    $assistance = new VTCore_Zeus_Assistance();


    // Build the members teaser options
    $this->panel = new VTCore_Bootstrap_Element_BsPanel(array(
      'text' => __('Members Archives', 'medikal'),
      'id' => 'vtcore-zeus-members-archives-options',
    ));

    $this->panel
      ->setChildrenPointer('content')
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][members][archive][image]',
        'text' => __('Show Image', 'medikal'),
        'description' => __('Show / hide the members image.', 'medikal'),
        'checked' => (boolean) $this->getShow('members.archive', 'image'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][members][archive][name]',
        'text' => __('Show Name', 'medikal'),
        'description' => __('Show / hide the members name.', 'medikal'),
        'checked' => (boolean) $this->getShow('members.archive', 'name'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][members][archive][excerpt]',
        'text' => __('Show Excerpt', 'medikal'),
        'description' => __('Show / hide the members excerpt.', 'medikal'),
        'checked' => (boolean) $this->getShow('members.archive', 'excerpt'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][members][archive][position]',
        'text' => __('Show Position', 'medikal'),
        'description' => __('Show / hide the members position.', 'medikal'),
        'checked' => (boolean) $this->getShow('members.archive', 'position'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][members][archive][speciality]',
        'text' => __('Show Speciality', 'medikal'),
        'description' => __('Show / hide the members speciality.', 'medikal'),
        'checked' => (boolean) $this->getShow('members.archive', 'speciality'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][members][archive][social]',
        'text' => __('Show Social', 'medikal'),
        'description' => __('Show / hide the members social.', 'medikal'),
        'checked' => (boolean) $this->getShow('members.archive', 'social'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][members][archive][readmore]',
        'text' => __('Show Readmore', 'medikal'),
        'description' => __('Show / hide the members readmore.', 'medikal'),
        'checked' => (boolean) $this->getShow('members.archive', 'readmore'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][options][members][archive][pager][mini]',
        'text' => __('Pager Mini', 'medikal'),
        'description' => __('Use mini pager for pagination.', 'medikal'),
        'checked' => (boolean) $this->getOption('members.archive.pager', 'mini'),
        'switch' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][options][members][archive][pager][infinite]',
        'text' => __('Pager infinite', 'medikal'),
        'description' => __('Use infinite pager for pagination.', 'medikal'),
        'checked' => (boolean) $this->getOption('members.archive.pager', 'infinite'),
        'switch' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->getParent()
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsGrids(array(
        'name' => 'theme[features][options][members][archive][grids]',
        'value' => $this->getOption('members.archive', 'grids'),
        'columns' => array(
          'text' => __('Members Teaser Grid Columns', 'medikal'),
          'description' => __('Set the grid column size for the teaser items.', 'medikal'),
        ),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 12,
            'small' => 12,
            'large' => 12,
          ),
        ),
        'element_grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->getParent()
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'text' => __('Not Found Title', 'medikal'),
        'name' => 'theme[features][options][members][archive][404][title]',
        'value' => $this->getOption('members.archive.404', 'title'),
        'description' => __('Set the text to serve as the title when no members is available to display.', 'medikal'),
        'raw' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 12,
            'small' => 12,
            'large' => 12,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsTextarea(array(
        'text' => __('Not Found Description', 'medikal'),
        'name' => 'theme[features][options][members][archive][404][description]',
        'value' => $this->getOption('members.archive.404', 'description'),
        'description' => __('Set the text to serve as the description when no members is available to display.', 'medikal'),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 12,
            'small' => 12,
            'large' => 12,
          ),
        ),
      )))
      ->getParent()
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'text' => __('Page Title', 'medikal'),
        'name' => 'theme[features][options][members][archive][title]',
        'value' => $this->getOption('members.archive', 'title'),
        'description' => __('Set the text to serve as the title in the members archive page.', 'medikal'),
        'raw' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 12,
            'small' => 12,
            'large' => 12,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsTextarea(array(
        'text' => __('Page Description Left', 'medikal'),
        'name' => 'theme[features][options][members][archive][description][left]',
        'value' => $this->getOption('members.archive.description', 'left'),
        'description' => __('Set the text to serve as the description on the left box in the members archive page.', 'medikal'),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsTextarea(array(
        'text' => __('Page Description Right', 'medikal'),
        'name' => 'theme[features][options][members][archive][description][right]',
        'value' => $this->getOption('members.archive.description', 'right'),
        'description' => __('Set the text to serve as the description on the left box in the members archive page.', 'medikal'),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )));

    $this->processPanel();

    // Build the page single visibility panel
    $this->panel = new VTCore_Bootstrap_Element_BsPanel(array(
      'text' => __('Members Single', 'medikal'),
      'id' => 'vtcore-zeus-members-single-options',
    ));

    $this->panel
      ->setChildrenPointer('content')
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][members][single][image]',
        'text' => __('Show Image', 'medikal'),
        'description' => __('Show / hide the members image.', 'medikal'),
        'checked' => (boolean) $this->getShow('members.single', 'image'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][members][single][name]',
        'text' => __('Show Name', 'medikal'),
        'description' => __('Show / hide the members name.', 'medikal'),
        'checked' => (boolean) $this->getShow('members.single', 'name'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][members][single][speciality]',
        'text' => __('Show Speciality', 'medikal'),
        'description' => __('Show / hide the members speciality.', 'medikal'),
        'checked' => (boolean) $this->getShow('members.single', 'speciality'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][members][single][position]',
        'text' => __('Show Position', 'medikal'),
        'description' => __('Show / hide the members position.', 'medikal'),
        'checked' => (boolean) $this->getShow('members.single', 'position'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][members][single][content]',
        'text' => __('Show Content', 'medikal'),
        'description' => __('Show / hide the members content.', 'medikal'),
        'checked' => (boolean) $this->getShow('members.single', 'content'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][members][single][department]',
        'text' => __('Show Department', 'medikal'),
        'description' => __('Show / hide the members department.', 'medikal'),
        'checked' => (boolean) $this->getShow('members.single', 'department'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][members][single][education]',
        'text' => __('Show Education', 'medikal'),
        'description' => __('Show / hide the members education.', 'medikal'),
        'checked' => (boolean) $this->getShow('members.single', 'education'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][members][single][experience]',
        'text' => __('Show Experience', 'medikal'),
        'description' => __('Show / hide the members experience.', 'medikal'),
        'checked' => (boolean) $this->getShow('members.single', 'experience'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][members][single][office]',
        'text' => __('Show Office', 'medikal'),
        'description' => __('Show / hide the members office.', 'medikal'),
        'checked' => (boolean) $this->getShow('members.single', 'office'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][members][single][workdays]',
        'text' => __('Show Workdays', 'medikal'),
        'description' => __('Show / hide the members workdays.', 'medikal'),
        'checked' => (boolean) $this->getShow('members.single', 'workdays'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][members][single][email]',
        'text' => __('Show E-Mail', 'medikal'),
        'description' => __('Show / hide the members e-mail.', 'medikal'),
        'checked' => (boolean) $this->getShow('members.single', 'email'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][members][single][services]',
        'text' => __('Show Services', 'medikal'),
        'description' => __('Show / hide the members services.', 'medikal'),
        'checked' => (boolean) $this->getShow('members.single', 'services'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][members][single][training]',
        'text' => __('Show Training', 'medikal'),
        'description' => __('Show / hide the members training.', 'medikal'),
        'checked' => (boolean) $this->getShow('members.single', 'training'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][members][single][social]',
        'text' => __('Show Social', 'medikal'),
        'description' => __('Show / hide the members social.', 'medikal'),
        'checked' => (boolean) $this->getShow('members.single', 'social'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][members][single][education_table]',
        'text' => __('Show Education Table', 'medikal'),
        'description' => __('Show / hide the members education table.', 'medikal'),
        'checked' => (boolean) $this->getShow('members.single', 'education_table'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][members][single][experience_table]',
        'text' => __('Show Experience Table', 'medikal'),
        'description' => __('Show / hide the members experience table.', 'medikal'),
        'checked' => (boolean) $this->getShow('members.single', 'experience_table'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][members][single][service_table]',
        'text' => __('Show Service Table', 'medikal'),
        'description' => __('Show / hide the members service table.', 'medikal'),
        'checked' => (boolean) $this->getShow('members.single', 'service_table'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )));


    $this->processPanel();


  }
}