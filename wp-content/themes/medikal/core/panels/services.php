<?php
/**
 * Panel section for configuring general options
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Panels_Services
extends VTCore_Zeus_Panels_Model {


  /**
   * Overriding parent method
   * @see VTCore_Bootstrap_Element_BsElement::buildElement()
   */
  public function buildElement() {

    $assistance = new VTCore_Zeus_Assistance();


    // Build the services teaser options
    $this->panel = new VTCore_Bootstrap_Element_BsPanel(array(
      'text' => __('Services Archives', 'medikal'),
      'id' => 'vtcore-zeus-services-archives-options',
    ));

    $this->panel
      ->setChildrenPointer('content')
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][services][archive][icon]',
        'text' => __('Show Icon', 'medikal'),
        'description' => __('Show / hide the services icon.', 'medikal'),
        'checked' => (boolean) $this->getShow('services.archive', 'icon'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][services][archive][image]',
        'text' => __('Show Image', 'medikal'),
        'description' => __('Show / hide the services image.', 'medikal'),
        'checked' => (boolean) $this->getShow('services.archive', 'image'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][services][archive][title]',
        'text' => __('Show Title', 'medikal'),
        'description' => __('Show / hide the services title.', 'medikal'),
        'checked' => (boolean) $this->getShow('services.archive', 'title'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][services][archive][excerpt]',
        'text' => __('Show Excerpt', 'medikal'),
        'description' => __('Show / hide the services excerpt.', 'medikal'),
        'checked' => (boolean) $this->getShow('services.archive', 'excerpt'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][services][archive][price]',
        'text' => __('Show Price', 'medikal'),
        'description' => __('Show / hide the services price.', 'medikal'),
        'checked' => (boolean) $this->getShow('services.archive', 'price'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][services][archive][readmore]',
        'text' => __('Show Readmore', 'medikal'),
        'description' => __('Show / hide the services readmore.', 'medikal'),
        'checked' => (boolean) $this->getShow('services.archive', 'readmore'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][options][services][archive][pager][mini]',
        'text' => __('Pager Mini', 'medikal'),
        'description' => __('Use mini pager for pagination.', 'medikal'),
        'checked' => (boolean) $this->getOption('services.archive.pager', 'mini'),
        'switch' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][options][services][archive][pager][infinite]',
        'text' => __('Pager infinite', 'medikal'),
        'description' => __('Use infinite pager for pagination.', 'medikal'),
        'checked' => (boolean) $this->getOption('services.archive.pager', 'infinite'),
        'switch' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->getParent()
      ->addChildren(new VTCore_Bootstrap_Form_BsSelect(array(
        'text' => __('Teaser Template', 'medikal'),
        'name' => 'theme[features][options][services][archive][template]',
        'value' => $this->getOption('services.archive', 'template'),
        'description' => __('Select the teaser template to use for the services archive items', 'medikal'),
        'options' => array(
          'services-grid' => __('Grid', 'medikal'),
          'services-flip' => __('Flip', 'medikal'),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsGrids(array(
        'name' => 'theme[features][options][services][archive][grids]',
        'value' => $this->getOption('services.archive', 'grids'),
        'columns' => array(
          'text' => __('Services Teaser Grid Columns', 'medikal'),
          'description' => __('Set the grid column size for the teaser items.', 'medikal'),
        ),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 12,
            'small' => 12,
            'large' => 12,
          ),
        ),
        'element_grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->getParent()
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'text' => __('Not Found Title', 'medikal'),
        'name' => 'theme[features][options][services][archive][404][title]',
        'value' => $this->getOption('services.archive.404', 'title'),
        'description' => __('Set the text to serve as the title when no services is available to display.', 'medikal'),
        'raw' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 12,
            'small' => 12,
            'large' => 12,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsTextarea(array(
        'text' => __('Not Found Description', 'medikal'),
        'name' => 'theme[features][options][services][archive][404][description]',
        'value' => $this->getOption('services.archive.404', 'description'),
        'description' => __('Set the text to serve as the description when no services is available to display.', 'medikal'),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 12,
            'small' => 12,
            'large' => 12,
          ),
        ),
      )))
      ->getParent()
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'text' => __('Page Title', 'medikal'),
        'name' => 'theme[features][options][services][archive][title]',
        'value' => $this->getOption('services.archive', 'title'),
        'description' => __('Set the text to serve as the title in the services archive page.', 'medikal'),
        'raw' => true,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 12,
            'small' => 12,
            'large' => 12,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsTextarea(array(
        'text' => __('Page Description Left', 'medikal'),
        'name' => 'theme[features][options][services][archive][description][left]',
        'value' => $this->getOption('services.archive.description', 'left'),
        'description' => __('Set the text to serve as the description on the left box in the services archive page.', 'medikal'),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsTextarea(array(
        'text' => __('Page Description Right', 'medikal'),
        'name' => 'theme[features][options][services][archive][description][right]',
        'value' => $this->getOption('services.archive.description', 'right'),
        'description' => __('Set the text to serve as the description on the left box in the services archive page.', 'medikal'),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )));

    $this->processPanel();

    // Build the page single visibility panel
    $this->panel = new VTCore_Bootstrap_Element_BsPanel(array(
      'text' => __('Services Single', 'medikal'),
      'id' => 'vtcore-zeus-services-single-options',
    ));

    $this->panel
      ->setChildrenPointer('content')
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][services][single][image]',
        'text' => __('Show Image', 'medikal'),
        'description' => __('Show / hide the services image.', 'medikal'),
        'checked' => (boolean) $this->getShow('services.single', 'image'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][services][single][price]',
        'text' => __('Show Price', 'medikal'),
        'description' => __('Show / hide the services price.', 'medikal'),
        'checked' => (boolean) $this->getShow('services.single', 'price'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][services][single][title]',
        'text' => __('Show Title', 'medikal'),
        'description' => __('Show / hide the services title.', 'medikal'),
        'checked' => (boolean) $this->getShow('services.single', 'title'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][services][single][content]',
        'text' => __('Show Content', 'medikal'),
        'description' => __('Show / hide the services content.', 'medikal'),
        'checked' => (boolean) $this->getShow('services.single', 'content'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][services][single][schedule]',
        'text' => __('Show Schedule', 'medikal'),
        'description' => __('Show / hide the services schedule.', 'medikal'),
        'checked' => (boolean) $this->getShow('services.single', 'schedule'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsCheckbox(array(
        'name' => 'theme[features][show][services][single][department]',
        'text' => __('Show Department', 'medikal'),
        'description' => __('Show / hide the services department.', 'medikal'),
        'checked' => (boolean) $this->getShow('services.single', 'department'),
        'switch' => TRUE,
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      )));


    $this->processPanel();

  }
}