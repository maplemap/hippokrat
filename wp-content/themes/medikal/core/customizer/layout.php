<?php
/**
 * Class for building valid VTCore_Wordpress_Factory_Customizer
 * arrays for Zeus Layout options
 *
 * @author jason.xie@victheme.com
 * @todo use model buildContext() ?
 */
class VTCore_Zeus_Customizer_Layout 
extends VTCore_Zeus_Customizer_Model {
  
  protected $panelKey = 'zeus_layout';
  protected $key = 'layout';
  
  /**
   * Method for building the schema color options
   */
  public function register() {

    if (!$this->getCache()) {

      $this->object = VTCore_Zeus_Init::getLayout();

      $this->context[$this->key]['panels'][$this->panelKey] = array(
        'title' => __('Layout Grids', 'medikal'),
        'priority' => 403,
        'capability' => 'edit_theme_options',
        'description' => __('Configure the theme bootstrap layout grids', 'medikal'),
      );


      $k = 0;
      foreach ($this->object->getChildrens() as $area) {

        $i = 0;
        if (count($area->getChildrens()) < 2) {
          continue;
        }
        foreach ($area->getChildrens() as $region) {

          $id = $region->getContext('regionId');
          $section = 'region-' . $id . '-section';

          $this->context[$this->key]['sections'][$section] = array(
            'title' => $region->getContext('arguments.name'),
            'priority' => $k++,
            'capability' => 'edit_theme_options',
            'panel' => $this->panelKey,
          );

          $settingName = 'regions[' . $id . '][grids]';
          $this->context[$this->key]['settings'][$settingName] = array(
            'default' => false,
            'type' => 'nosave',
            'capability' => 'edit_theme_options',
          );

          $text = array(
            'columns' => array(
              'text' => __('Columns', 'medikal'),
            ),
            'push' => array(
              'text' => __('Push', 'medikal'),
            ),
            'pull' => array(
              'text' => __('Pull', 'medikal'),
            ),
            'offset' => array(
              'text' => __('Offset', 'medikal'),
            ),
          );

          // Each grid type and size is own select element, customizer
          // needs each to have one settings entry
          foreach (array('columns', 'push', 'pull', 'offset') as $type) {

            if (!$region->getContext('grids.' . $type)) {
              unset($text[$type]);
              continue;
            }

            foreach (array('mobile', 'tablet', 'small', 'large') as $size) {

              $name = 'regions[' . $id . '][grids]['. $type .'][' . $size . ']';
              $this->context[$this->key]['settings'][$name] = array(
                'default' => $region->getContext('grids.' . $type . '.' . $size),
                'transport' => 'postMessage',
                'capability' => 'edit_theme_options',
              );

              $this->context[$this->key]['controls'][$name] = array(
                'object' => 'VTCore_Wordpress_Customizer_Element_Hidden',
                'section' => $section,
                'settings' => $name,
                'priority' => $i++,
              );

              $this->context[$this->key]['pointers'][$name] = array(
                'target' => '.region-' . $id,
              );
            }
          }

          $this->context[$this->key]['controls'][$settingName] = array(
            'object' => 'VTCore_Wordpress_Customizer_Element_Grid',
            'label' => $region->getContext('arguments,name'),
            'description' => $region->getContext('arguments.description'),
            'section' => $section,
            'settings' => $settingName,
            'priority' => $i++,
            'choices' => $text,
          );


        }
      }

      $this->setCache();
    }

    $this->insert();

    return $this;
  }

}