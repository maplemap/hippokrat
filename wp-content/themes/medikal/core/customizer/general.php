<?php
/**
 * Class for generating customizer array
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Zeus_Customizer_General
extends VTCore_Zeus_Customizer_Model {

  protected $panelKey = 'zeus_general';
  protected $key = 'general';

  public function register() {

    if (!$this->getCache()) {
      $this->object = new VTCore_Zeus_Panels_General(array(
        'features' => VTCore_Zeus_Init::getFeatures(),
        'process' => false,
        'build' => 'object',
      ));

      $this->context[$this->key]['panels'][$this->panelKey] = array(
        'title' => __('Theme Options', 'medikal'),
        'priority' => 403,
        'capability' => 'edit_theme_options',
        'description' => __('Configure the theme general options.', 'medikal'),
      );

      $this->buildContext();
      $this->setCache();
    }

    $this->insert();


    return $this;
  }

}
