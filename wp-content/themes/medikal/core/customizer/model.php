<?php
/**
 * Model class for generic VTCore Form to VTCore Customizer array conversion
 * @author jason.xie@victheme.com
 *
 */
abstract class VTCore_Zeus_Customizer_Model {

  protected $panelKey = false;
  protected $key = false;
  protected $object = false;
  protected $hash = false;
  protected $context = false;

  /**
   * Subclass must extend this
   * @return mixed
   */
  abstract function register();

  /**
   * Initializing object
   */
  public function __construct() {
    $this->hash = 'vtcore_zeus_customizer_context_' . ACTIVE_THEME;

    if (defined('VTCORE_CLEAR_CACHE') && VTCORE_CLEAR_CACHE) {
      $this->clearCache();
    }

    // Debug
    if (defined('WP_DEBUG') && WP_DEBUG == true) {
      $this->clearCache();
    }
  }

  /**
   * Method for clearing cache, this method
   * will only attempt to remove the related
   * object cache only.
   *
   * @return $this
   */
  public function clearCache() {

    if (isset($this->context[$this->key])) {
      unset($this->context[$this->key]);
    }

    // Remove all cache
    delete_transient($this->hash);

    // If got other object cache, reinstate the cache
    if (!empty($this->context)) {
      $this->setCache();
    }

    return $this;
  }


  /**
   * Method for removing all the cached objects
   * context from the transient at once.
   * @return $this
   */
  public function clearGlobalCache() {
    delete_transient($this->hash);
    return $this;
  }

  /**
   * Storing all cache for all child object in a
   * single transient entry.
   *
   * @return $this
   */
  public function setCache() {
    set_transient($this->hash, $this->context, 12 * HOUR_IN_SECONDS);
    return $this;
  }


  /**
   * Get a single current object cache
   * @return bool
   */
  public function getCache() {
    return isset($this->context[$this->key]) ? $this->context[$this->key] : false;
  }


  /**
   * Insert the context to the factory
   */
  public function insert() {
    if (is_object(VTCore_Wordpress_Init::getFactory('customizer'))) {
      VTCore_Wordpress_Init::getFactory('customizer')
        ->insert($this->key, $this->getCache());
    }
  }


  /**
   * Method for converting VTCore Object to a valid
   * VTCore_Wordpress_Factory_Customizer array entry.
   * @return $this
   */
  protected function buildContext() {

    $this->priority = 0;

    foreach ($this->object->getChildrens() as $object) {

      $this->sectionKey = $this->key . '-panel-' . $object->getContext('id');

      // Build Customizer Section
      $this->context[$this->key]['sections'][$this->sectionKey] = array(
        'title' => $object->getContext('text'),
        'priority' => $this->priority++,
        'capability' => 'edit_theme_options',
        'panel' => $this->panelKey,
      );

      $elements = array(
        'VTCore_Bootstrap_Form_BsText',
        'VTCore_Bootstrap_Form_BsTextarea',
        'VTCore_Bootstrap_Form_BsSelect',
        'VTCore_Bootstrap_Form_BsCheckbox',
        'VTCore_Bootstrap_Form_BsColor',
        'VTCore_Wordpress_Form_WpMedia',
        'VTCore_Bootstrap_Form_BsGrids',
      );

      // Build form elements
      foreach ($object->findChildren('objectType', $elements) as $element) {

        $classType = get_class($element);

        $this->settingName = str_replace('theme[features][', 'features[', $element->getContext('name'));

        // Build Customizer Settings
        $this->context[$this->key]['settings'][$this->settingName] = array(
          'default' => $element->getContext('value'),

          // @todo figure out how to overload these options? Should we inject in vtcore context?
          'transport' => 'refresh',
          'capability' => 'edit_theme_options',
        );

        if ($element->getContext('text')) {
          $element->addContext('text', addslashes($element->getContext('text')));
        }

        if ($element->getContext('description')) {
          $element->addContext('description', addslashes($element->getContext('description')));
        }

        switch ($classType) {
          case 'VTCore_Bootstrap_Form_BsText' :

            $this->context[$this->key]['controls'][$this->settingName] = array(
              'object' => 'VTCore_Wordpress_Customizer_Element_Text',
              'label' => $element->getContext('text'),
              'description' => $element->getContext('description'),
              'section' => $this->sectionKey,
              'settings' => $this->settingName,
              'priority' => $element->getMachineID(),
            );

            break;

          case 'VTCore_Bootstrap_Form_BsTextarea' :

            $this->context[$this->key]['controls'][$this->settingName] = array(
              'object' => 'VTCore_Wordpress_Customizer_Element_Textarea',
              'label' => $element->getContext('text'),
              'description' => $element->getContext('description'),
              'section' => $this->sectionKey,
              'settings' => $this->settingName,
              'priority' => $element->getMachineID(),
            );

            break;

          case 'VTCore_Bootstrap_Form_BsSelect' :

            $this->context[$this->key]['controls'][$this->settingName] = array(
              'object' => 'VTCore_Wordpress_Customizer_Element_Select',
              'label' => $element->getContext('text'),
              'description' => $element->getContext('description'),
              'section' => $this->sectionKey,
              'settings' => $this->settingName,
              'priority' => $element->getMachineID(),
            );

            foreach ($element->getContext('options') as $key => $value) {
              $this->context[$this->key]['controls'][$this->settingName]['choices'][$key] = addslashes($value);
            }

            break;


          case 'VTCore_Bootstrap_Form_BsCheckbox' :

            $this->context[$this->key]['settings'][$this->settingName]['object'] = 'VTCore_Wordpress_Customizer_Settings_Config';
            $this->context[$this->key]['settings'][$this->settingName]['filter'] = 'boolean';
            $this->context[$this->key]['settings'][$this->settingName]['default'] = (boolean) $element->getContext('value');

            $this->context[$this->key]['controls'][$this->settingName] = array(
              'object' => 'VTCore_Wordpress_Customizer_Element_Switch',
              'label' => $element->getContext('text'),
              'description' => $element->getContext('description'),
              'section' => $this->sectionKey,
              'settings' => $this->settingName,
              'priority' => $element->getMachineID(),
            );

            break;

          case 'VTCore_Bootstrap_Form_BsColor' :

            $this->context[$this->key]['controls'][$this->settingName] = array(
              'object' => 'VTCore_Wordpress_Customizer_Element_Color',
              'label' => $element->getContext('text'),
              'description' => $element->getContext('description'),
              'section' => $this->sectionKey,
              'settings' => $this->settingName,
              'priority' => $element->getMachineID(),
            );

            break;


          case 'VTCore_Wordpress_Form_WpMedia' :

            /**$value = $element->getContext('value');
            if (!is_numeric($value)) {
              $this->context[$this->key]['settings'][$this->settingName]['default'] = $value;
            }**/


            $this->context[$this->key]['controls'][$this->settingName] = array(
              'object' => 'WP_Customize_Image_Control',
              'label' => $element->getContext('text'),
              'description' => $element->getContext('description'),
              'section' => $this->sectionKey,
              'settings' => $this->settingName,
              'priority' => $element->getMachineID(),
            );

            /** Experimental, not yet tested! probably can make loading faster
            if ($element->getContext('data.type') == 'image') {
              $this->context[$this->key]['controls'][$this->settingName] = array(
                'object' => 'VTCore_Wordpress_Customizer_Element_Image',
                'label' => $element->getContext('text'),
                'description' => $element->getContext('description'),
                'section' => $this->sectionKey,
                'settings' => $this->settingName,
                'priority' => $element->getMachineID(),
              );
            }

            if ($element->getContext('data.type') == 'video') {
              $this->context[$this->key]['controls'][$this->settingName] = array(
                'object' => 'VTCore_Wordpress_Customizer_Element_Video',
                'label' => $element->getContext('text'),
                'description' => $element->getContext('description'),
                'section' => $this->sectionKey,
                'settings' => $this->settingName,
                'priority' => $element->getMachineID(),
              );
            }
            **/

            break;

          case 'VTCore_Bootstrap_Form_BsGrids' :

            // Don't save this dummy settings!
            $this->context[$this->key]['settings'][$this->settingName]['type'] = 'nosave';

            $sizes = array(
              'mobile',
              'tablet',
              'small',
              'large',
            );

            // Each grid type and size is own select element, customizer
            // needs each to have one settings entry
            foreach (array('columns', 'push', 'pull', 'offset') as $type) {
              if ($element->getContext($type)) {
                foreach ($sizes as $size) {
                  // Build individual settings
                  $name = $this->settingName . '[' . $type . '][' . $size . ']';
                  $this->context[$this->key]['settings'][$name] = array(
                    'default' => $element->getContext('value.' . $type . '.' . $size),
                    'transport' => 'refresh',
                    'capability' => 'edit_theme_options',
                  );

                  $this->context[$this->key]['controls'][$name] = array(
                    'object' => 'VTCore_Wordpress_Customizer_Element_Hidden',
                    'section' => $this->sectionKey,
                    'settings' => $name,
                    'priority' => $element->getMachineID(),
                  );

                }
              }
            }

            $this->context[$this->key]['controls'][$this->settingName] = array(
              'object' => 'VTCore_Wordpress_Customizer_Element_Grid',
              'label' => $element->getContext('text'),
              'description' => $element->getContext('description'),
              'section' => $this->sectionKey,
              'settings' => $this->settingName,
              'priority' => $element->getMachineID(),
              'choices' => array(
                'columns' => $element->getContext('columns'),
                'push' => $element->getContext('push'),
                'pull' => $element->getContext('pull'),
                'offset' => $element->getContext('offset'),
              ),
            );

            break;

        }
      }

      // Build Heading and subtitle
      foreach ($object->findChildren('objectType', 'VTCore_Bootstrap_Element_BsHeader') as $element) {

        $this->settingName = $this->sectionKey . '-heading-';

        if ($element->getContext('id')) {
          $this->settingName .= $element->getContext('id');
        }
        else {
          $this->settingName .= $element->getMachineID();
        }

        $classType = get_class($object);
        $this->label = $object->getContext('text');
        $this->weight = $object->getMachineID();

        // Build Customizer Settings
        $this->context[$this->key]['settings'][$this->settingName] = array(
          'type' => 'nosave',
          'capability' => 'edit_theme_options',
        );

        $this->context[$this->key]['controls'][$this->settingName] = array(
          'object' => 'VTCore_Wordpress_Customizer_Element_Subtitle',
          'label' => $element->getContext('text'),
          'description' => $element->getContext('description'),
          'section' => $this->sectionKey,
          'settings' => $this->settingName,
          'priority' => $element->getMachineID(),
        );
      }
    }


    return $this;

  }

}