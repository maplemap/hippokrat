<?php
/**
 * Class for building valid VTCore_Wordpress_Factory_Customizer
 * arrays for Zeus Schema options
 *
 * @author jason.xie@victheme.com
 * @todo use model buildContext() ?
 */
class VTCore_Zeus_Customizer_Schema
extends VTCore_Zeus_Customizer_Model {

  private $colorBuilder;

  protected $panelKey = 'zeus_schema';
  protected $key = 'schema';

  /**
   * Method for building the schema color options
   */
  public function register() {

    if (!$this->getCache()) {

      $this->object = new VTCore_Zeus_Schema_Factory();

      $i = 1;

      $this->object->setObject('colorBuilder', new VTCore_Zeus_Schema_Color($this->object->getActiveSchema()->getData('color'), false, false, true));

      $options = array();
      foreach ($this->object->getSchemas() as $key => $schema) {
        $options[$schema->getSchema('id')] = $schema->getSchema('name');
      }

      $this->context[$this->key]['panels']['styles'] = array(
        'title' => __('Color & Fonts', 'medikal'),
        'priority' => 403,
        'capability' => 'edit_theme_options',
        'description' => __('Configure the theme color and fonts', 'medikal'),
      );

      $this->context[$this->key]['sections']['schema'] = array(
        'title' => __('Change Schema', 'medikal'),
        'priority' => 0,
        'capability' => 'edit_theme_options',
        'panel' => 'styles',
      );

      $this->context[$this->key]['settings']['schema-active'] = array(
        'object' => false,
        'default' => $this->object->getActiveSchemaID(),
        'transport' => 'postMessage',
        'capability' => 'edit_theme_options',
        'priority' => 0,
      );

      $this->context[$this->key]['controls']['schema-active'] = array(
        'object' => 'VTCore_Wordpress_Customizer_Element_Select',
        'label' => __('Select Style Schemas', 'medikal'),
        'section' => 'schema',
        'settings' => 'schema-active',
        'priority' => 0,
        'type' => 'select',
        'choices' => $options,
      );

      foreach ($this->object->getActiveSchema()->getData('color') as $key => $data) {

        // Dont process broken color schema entry
        if (!isset($data['title'])
            || !isset($data['description'])
            || !isset($data['selectors'])
            || !isset($data['parent'])) {

          continue;
        }

        $section = 'color-' . $key . '-section' ;

        $this->context[$this->key]['sections'][$section] = array(
          'title' => $data['title'],
          'description' => $data['description'],
          'panel' => 'styles',
          'priority' => $i++,
        );

        foreach ($data as $elkey => $value) {

          if (in_array($elkey, array('title', 'description', 'selectors', 'parent'))) {
            continue;
          }

          $text = str_replace('-', ' ', ucfirst($elkey));
          $name = 'schemas[color][' . $key . '][' . $elkey . ']';
          $objectKey = $key . '--' . $elkey;
          $target = $this->getColorTarget($objectKey);
          $rules = $this->getColorRules($objectKey);

          $this->context[$this->key]['settings'][$name] = array(
            'default'     => $value,
            'transport'   => 'postMessage',
            'capability' => 'edit_theme_options',
            'priority' => $i++,
          );


          if (strpos($elkey, 'color') !== false
              || strpos($elkey, '-hover') !== false
              || strpos($elkey, '-focus') !== false
              || strpos($elkey, '-visited') !== false) {

            $this->context[$this->key]['controls'][$name] = array(
              'object' => 'VTCore_Wordpress_Customizer_Element_Color',
              'label' =>  $text,
              'section' => $section,
              'settings' => $name,
              'priority' => $i++,
              'type' => 'vtcore_color',
            );

          }

          // Not working yet!
          elseif (strpos($elkey, 'gradient') !== false) {

            $this->context[$this->key]['controls'][$name] = array(
              'object' => 'VTCore_Wordpress_Customizer_Element_Gradient',
              'label' =>  $text,
              'section' => $section,
              'settings' => $name,
              'priority' => $i++,
              'type' => 'vtcore_gradient',
            );


          }

          elseif (strpos($elkey, 'image') !== false) {

            $this->context[$this->key]['controls'][$name] = array(
              'object' => 'WP_Customize_Image_Control',
              'label' =>  $text,
              'section' => $section,
              'settings' => $name,
              'priority' => $i++,
            );

          }

          elseif (strpos($elkey, 'background-repeat') !== false) {

            $this->context[$this->key]['controls'][$name] = array(
              'object' => 'VTCore_Wordpress_Customizer_Element_Select',
              'label' => $text,
              'section' => $section,
              'settings' => $name,
              'priority' => $i++,
              'type' => 'vtcore_select',
              'choices' => array(
                '' => __('Not set', 'medikal'),
                'no-repeat' => __('No Repeat', 'medikal'),
                'repeat' => __('Repeat All', 'medikal'),
                'repeat-x' => __('Repeat Horizontally', 'medikal'),
                'repeat-y' => __('Repeat Vertically', 'medikal'),
              ),
            );


          }
          elseif (strpos($elkey, 'border-style') !== false) {

            $this->context[$this->key]['controls'][$name] = array(
              'object' => 'VTCore_Wordpress_Customizer_Element_Select',
              'label' => $text,
              'section' => $section,
              'settings' => $name,
              'priority' => $i++,
              'type' => 'vtcore_select',
              'choices' => array(
                '' => __('Not set', 'medikal'),
                'none' => __('None', 'medikal'),
                'inherit' => __('Inherit', 'medikal'),
                'solid' => __('Solid', 'medikal'),
                'dotted' => __('Dotted', 'medikal'),
                'dashed' => __('Dashed', 'medikal'),
                'double' => __('Double', 'medikal'),
                'ridge' => __('Ridge', 'medikal'),
                'inset' => __('Inset', 'medikal'),
                'outset' => __('Outset', 'medikal'),
                'groove' => __('Groove', 'medikal'),
              ),
            );


          }
          elseif (strpos($elkey, 'font-style') !== false
            || strpos($elkey, 'heading-style') !== false) {

            $this->context[$this->key]['controls'][$name] = array(
              'object' => 'VTCore_Wordpress_Customizer_Element_Select',
              'label' => $text,
              'section' => $section,
              'settings' => $name,
              'priority' => $i++,
              'type' => 'vtcore_select',
              'choices' => array(
                '' => __('Not set', 'medikal'),
                'normal' => __('Normal', 'medikal'),
                'oblique' => __('Oblique', 'medikal'),
                'italic' => __('Italic', 'medikal'),
                'inherit' => __('Inherit', 'medikal'),
              ),
            );

          }
          elseif (strpos($elkey, 'weight') !== false) {

            $this->context[$this->key]['controls'][$name] = array(
              'object' => 'VTCore_Wordpress_Customizer_Element_Select',
              'label' => $text,
              'section' => $section,
              'settings' => $name,
              'priority' => $i++,
              'type' => 'vtcore_select',
              'choices' => array(
                '' => __('Not set', 'medikal'),
                'inherit' => __('Inherit', 'medikal'),
                100 => __('Thin', 'medikal'),
                200 => __('Extra light', 'medikal'),
                300 => __('Light', 'medikal'),
                400 => __('Normal', 'medikal'),
                500 => __('Medium', 'medikal'),
                600 => __('Demi bold', 'medikal'),
                700 => __('Bold', 'medikal'),
                800 => __('Heavy', 'medikal'),
                900 => __('Black', 'medikal'),
              ),
            );

          }
          elseif (strpos($elkey, 'family') !== false) {

            $this->context[$this->key]['controls'][$name] = array(
              'object' => 'VTCore_Wordpress_Customizer_Element_Font',
              'label' => $text,
              'section' => $section,
              'settings' => $name,
              'priority' => $i++,
              'type' => 'vtcore_font',
            );
          }


          else {
            $this->context[$this->key]['controls'][$name] = array(
              'object' => 'VTCore_Wordpress_Customizer_Element_Text',
              'label' => $text,
              'section' => $section,
              'settings' => $name,
              'priority' => $i++,
              'type' => 'vtcore_text',
            );
          }

          $this->context[$this->key]['pointers'][$name] = array(
            'target' => implode(', ',$target),
            'rules' => array_shift($rules),
          );
        }
      }

      $this->setCache();
    }

    $this->insert();

    return $this;
  }


  public function getColorTarget($key) {
    return $this->object->getObject('colorBuilder')->getSchema($key)->getTarget();
  }



  public function getColorRules($key) {
    $rules = array();
    $objects = $this->object->getObject('colorBuilder')->getSchema($key)->getRules();
    $object = array_shift($objects);

    if (is_object($object)) {
      foreach ($object->getRules() as $rule) {
        list($key, $value) = explode(':', $rule);
        $rules[] = $key;
      }
    }

    return $rules;
  }

}