<?php
/**
 * Class for generating customizer array
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Zeus_Customizer_Department
extends VTCore_Zeus_Customizer_Model {

  protected $panelKey = 'zeus_department';
  protected $key = 'department';

  public function register() {

    if (!$this->getCache()) {
      $this->object = new VTCore_Zeus_Panels_Department(array(
        'features' => VTCore_Zeus_Init::getFeatures(),
        'process' => false,
        'build' => 'object',
      ));

      $this->context[$this->key]['panels'][$this->panelKey] = array(
        'title' => __('Department Options', 'medikal'),
        'priority' => 404,
        'capability' => 'edit_theme_options',
        'description' => __('Configure the theme department options.', 'medikal'),
      );

      $this->buildContext();
      set_transient($this->hash, $this->context, 12 * HOUR_IN_SECONDS);
    }

    $this->insert();

    return $this;
  }

}