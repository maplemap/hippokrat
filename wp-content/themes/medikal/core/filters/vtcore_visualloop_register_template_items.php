<?php
/**
 * Registering new template for slick advanced carousel
 * These registered templates can be used in shortcodes, objects
 * and visualcomposer
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Filters_VTCore__VisualLoop__Register__Template__Items
extends VTCore_Wordpress_Models_Hook {

  public function hook($templates = NULL) {

    $templates['blog-list.php'] = __('Blog List', 'medikal');
    $templates['blog-masonry.php'] = __('Blog Masonry', 'medikal');
    $templates['members-grid.php'] = __('Members Grid', 'medikal');
    $templates['department-flip.php'] = __('Department Flip', 'medikal');
    $templates['department-grid.php'] = __('Department Grid', 'medikal');
    $templates['department-masonry.php'] = __('Department Masonry', 'medikal');
    $templates['product-grid.php'] = __('Product Grid', 'medikal');
    $templates['services-flip.php'] = __('Services Flip', 'medikal');

    return $templates;
  }
}