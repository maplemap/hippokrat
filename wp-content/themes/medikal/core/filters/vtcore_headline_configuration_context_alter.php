<?php
/**
 * Hooking into VTCore Headline Plugin
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Zeus_Filters_VTCore__Headline__Configuration__Context__Alter
extends VTCore_Wordpress_Models_Hook {

  public function hook($context = NULL) {

    $context['product_archive'] = array(
      'general' => array(
        'enable' => true,
        'title' => __('Catalog', 'medikal'),
        'subtitle' => __('Browse our products', 'medikal'),
      ),
    );

    $context['product_single'] = array(
      'general' => array(
        'enable' => true,
        'title' => __('Product', 'medikal'),
        'subtitle' => __('Product Specification', 'medikal'),
      ),
    );

    $context['shop_cart'] = array(
      'general' => array(
        'enable' => true,
        'title' => __('Shopping Cart', 'medikal'),
        'subtitle' => '',
      ),
    );

    $context['shop_account'] = array(
      'general' => array(
        'enable' => true,
        'title' => __('My Account', 'medikal'),
        'subtitle' => '',
      ),
    );

    $context['shop_checkout'] = array(
      'general' => array(
        'enable' => true,
        'title' => __('Process Checkout', 'medikal'),
        'subtitle' => '',
      ),
    );

    $context['department_single'] = array(
      'general' => array(
        'enable' => true,
        'title' => __('Department', 'medikal'),
        'subtitle' => '',
      ),
    );

    $context['services_single'] = array(
      'general' => array(
        'enable' => true,
        'title' => __('Service', 'medikal'),
        'subtitle' => '',
      ),
    );

    $context['members_single'] = array(
      'general' => array(
        'enable' => true,
        'title' => __('Member', 'medikal'),
        'subtitle' => '',
      ),
    );

    $context['department'] = array(
      'general' => array(
        'enable' => true,
        'title' => __('Our Departments', 'medikal'),
        'subtitle' => __('Browse available departments', 'medikal'),
      ),
    );

    $context['services'] = array(
      'general' => array(
        'enable' => true,
        'title' => __('Our Services', 'medikal'),
        'subtitle' => __('Browse available service', 'medikal'),
      ),
    );

    $context['members'] = array(
      'general' => array(
        'enable' => true,
        'title' => __('Our Members', 'medikal'),
        'subtitle' => __('Browse available members', 'medikal'),
      ),
    );




    // Set default background and styling for headline
    foreach ($context as $position => $value) {
      $context[$position]['background']['image'] = VTCORE_ZEUS_THEME_URL . '/assets/theme/css/images/headline.jpg';
      $context[$position]['background']['color'] = '#dddddd';
      $context[$position]['background']['top'] = 'top';
      $context[$position]['background']['left'] = 'center';
      $context[$position]['background']['repeat'] = 'no-repeat';
      $context[$position]['background']['parallax'] = 'parallax-vertical';
      $context[$position]['background']['width'] = '100%';
      $context[$position]['background']['height'] = 'auto';

      $context[$position]['masking']['enable'] = true;
      $context[$position]['masking']['color'] = '#eeeeee';
      $context[$position]['masking']['opacity'] = '0.7';


    }

    return $context;

  }
}