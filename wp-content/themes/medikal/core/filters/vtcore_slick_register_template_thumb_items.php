<?php
/**
 * Registering new template for slick advanced carousel
 * These registered templates can be used in shortcodes, objects
 * and visualcomposer
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Filters_VTCore__Slick__Register__Template__Thumb__Items
extends VTCore_Wordpress_Models_Hook {

  public function hook($templates = NULL) {

    $templates['department-carousel-thumb-list.php'] = __('Department List', 'medikal');

    return $templates;
  }
}