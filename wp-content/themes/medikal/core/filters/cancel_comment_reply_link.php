<?php
/**
 * Class for filtering cancel_comment_reply_link
 * to excerpt content
 * and adding extra class to it.
 *
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Filters_Cancel__Comment__Reply__Link
extends VTCore_Wordpress_Models_Hook {

  public function hook($link = NULL) {
    $link = str_replace('<a ', '<a class="btn-icon pull-right btn-danger btn-sm" ', $link);
    $link = str_replace('">', '"><i class="fa fa-times-circle"></i>', $link);
    return $link;
  }
}