<?php
/**
 * Hooking into VTCore Headline plugin to remove
 * unsupported metabox tabs
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Filters_VTCore__Headline__Remove__Metabox__Tabs
extends VTCore_Wordpress_Models_Hook {

  public function hook($context = NULL) {

    // Theme doesn't support items and columns in the headline
    $context[] = 'items';
    $context[] = 'columns';

    return $context;
  }
}