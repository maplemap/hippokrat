<?php
/**
 * Class for filtering the woocommerce product variation output
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Filters_Woocommerce__Available__Variation
extends VTCore_Wordpress_Models_Hook {

  public function hook($variable = FALSE) {

    if (isset($variable['price_html'])
        && !empty($variable['price_html'])) {
      $variable['price_html'] = '<div class="product-meta"><span class="meta-header header">'
                                  . __('Price', 'medikal') . '</span><span class="meta-content">'
                                  . $variable['price_html'] .'</span></div>';
    }

    if (isset($variable['availability_html'])
      && !empty($variable['availability_html'])) {
      $variable['availability_html'] = '<div class="product-meta"><span class="meta-header header">'
                                        . __('Stock', 'medikal') . '</span><span class="meta-content">'
                                        .  $variable['availability_html'] . '</span></div>';
    }

    if (isset($variable['variation_description'])
      && !empty($variable['variation_description'])) {
      $variable['variation_description'] = '<div class="product-meta"><span class="meta-header header">'
                                            . __('Description', 'medikal') . '</span><span class="meta-content">'
                                            .  $variable['variation_description'] . '</span></div>';
    }


    return $variable;
  }
}