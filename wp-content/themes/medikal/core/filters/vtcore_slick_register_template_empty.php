<?php
/**
 * Registering new template for slick advanced carousel
 * These registered templates can be used in shortcodes, objects
 * and visualcomposer
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Filters_VTCore__Slick__Register__Template__Empty
extends VTCore_Wordpress_Models_Hook {

  public function hook($templates = NULL) {

    $templates['members-empty.php'] = __('Members', 'medikal');
    $templates['product-empty.php'] = __('Product', 'medikal');
    $templates['services-empty.php'] = __('Services', 'medikal');
    $templates['department-empty.php'] = __('Department', 'medikal');

    return $templates;
  }
}