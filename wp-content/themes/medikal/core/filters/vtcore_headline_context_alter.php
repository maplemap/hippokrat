<?php
/**
 * Hooking into vtcore_headline_context_alter to change
 * the default context for headline metabox
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Filters_VTCore__Headline__Context__Alter
extends VTCore_Wordpress_Models_Hook {

  protected $argument = 2;

  public function hook($context = NULL, $options = NULL) {

    // Check if user has enter any data or not
    if (empty($options)) {

      // Set default background and styling for headline
      $context['background']['image'] = VTCORE_ZEUS_THEME_URL . '/assets/theme/css/images/headline.jpg';
      $context['background']['color'] = '#dddddd';
      $context['background']['top'] = 'top';
      $context['background']['left'] = 'center';
      $context['background']['repeat'] = 'no-repeat';
      $context['background']['parallax'] = 'parallax-vertical';
      $context['background']['width'] = '100%';
      $context['background']['height'] = 'auto';

      $context['masking']['color'] = '#eeeeee';
      $context['masking']['opacity'] = '0.7';

      $context['general']['enable'] = true;
    }

    return $context;
  }
}