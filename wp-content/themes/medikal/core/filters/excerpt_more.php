<?php
/**
 * Class for filtering excerpt more
 * to excerpt content
 * and adding extra class to it.
 *
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Filters_Excerpt__More
extends VTCore_Wordpress_Models_Hook {

  public function hook($button = NULL) {
    return '...';
  }
}