<?php
/**
 * Class for filtering template
 * using template_include filter
 *
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Filters_Template__Include
extends VTCore_Wordpress_Models_Hook {

  protected $weight = 1;

  public function hook($template = NULL) {

    // Routing woocommerce special page
    if (VTCORE_WOOCOMMERCE_LOADED && is_singular('page') && (is_cart() || is_checkout() || is_account_page())) {
      $template = VTCORE_ZEUS_THEME_PATH . DIRECTORY_SEPARATOR . 'single-page-shop.php';
    }


    return $template;
  }
}