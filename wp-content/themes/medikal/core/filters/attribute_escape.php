<?php
/**
 * Class for filtering attribute_escape
 * to excerpt content
 * and adding extra class to it.
 *
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Filters_Attribute__Escape
extends VTCore_Wordpress_Models_Hook {

  protected $argument = 3;
  protected $weight = 10;

  public function hook($safe_text = NULL, $text = NULL) {
    // Comment form button
    if ($text === 'bootstrap-comment-button') {
      $safe_text = 'submit" class="btn pull-right btn-primary';
    }

    return $safe_text;
  }
}