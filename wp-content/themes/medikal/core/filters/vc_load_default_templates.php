<?php
/**
 * Registering Theme Default Layout templates
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Filters_VC__Load__Default__Templates
extends VTCore_Wordpress_Models_Hook {

  public function hook($templates = NULL) {
    VTCore_Wordpress_Utility::loadAsset('vc-admin');
    return VTCore_Zeus_Init::getStaticSystem('visualComposer')->addTemplate($templates);
  }
}