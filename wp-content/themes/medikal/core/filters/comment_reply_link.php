<?php
/**
 * Class for filtering comment_reply_link
 * to excerpt content
 * and adding extra class to it.
 *
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Filters_Comment__Reply__Link
extends VTCore_Wordpress_Models_Hook {

  public function hook($link = NULL) {
    $link = str_replace("'>", "'><i class='fa fa-mail-forward fa-flip-horizontal'></i>", $link);
    return $link;
  }
}