<?php
/**
 * Hooking into VTCore Headline Plugin
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Zeus_Filters_VTCore__Headline__Alter
extends VTCore_Wordpress_Models_Hook {

  protected $argument = 3;

  public function hook($instance = NULL, $default = NULL, $config = NULL) {

    global $template;

    if (empty($instance)) {
      // Integration to woocommerce
      if (VTCORE_WOOCOMMERCE_LOADED) {

        // Use product archive headline configuration
        if (strpos($template, 'taxonomy-product_cat.php') !== FALSE
          || strpos($template, 'taxonomy-product_tag.php') !== FALSE
          || strpos($template, 'archive-product.php') !== FALSE
        ) {
          $instance = $default['product_archive'];
        }

        if (is_singular('product')) {
          $instance = $default['product_single'];
        }

        if (strpos($template, 'single-page-shop.php') !== FALSE) {

          // Use shop cart headline configuration
          if (is_cart()) {
            $instance = $default['shop_cart'];
          }

          // Use shop checkout headline configuration
          elseif (is_checkout()) {
            $instance = $default['shop_checkout'];
          }

          // Use shop account headline configuration
          elseif (is_account_page()) {
            $instance = $default['shop_account'];
          }
        }
      }

      // Integration to victheme department
      if (defined('VTCORE_DEPARTMENT_LOADED') && VTCORE_DEPARTMENT_LOADED) {
        if (is_singular('department')) {
          $instance = $default['department_single'];
        }
      }

      // Integration to victheme_services
      if (defined('VTCORE_SERVICES_LOADED') && VTCORE_SERVICES_LOADED) {
        if (is_singular('services')) {
          $instance = $default['services_single'];
        }
      }


      // Integration to victheme_members
      if (defined('VTCORE_MEMBERS_LOADED') && VTCORE_MEMBERS_LOADED) {
        if (is_singular('members')) {
          $instance = $default['members_single'];
        }
      }
    }

    // Instance not empty but broken, need to redirect to proper config array!
    else {

      // Woocommerce category page
      if (function_exists('is_product_category')
        && is_product_category()
        && $config->get('product_archive')) {
        $instance = $config->get('product_archive');
      }
    }

    return $instance;
  }
}