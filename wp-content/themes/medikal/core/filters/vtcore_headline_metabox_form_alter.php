<?php
/**
 * Hooking into VTCore Headline plugin to alter the metabox form
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Filters_VTCore__Headline__Metabox__Form__Alter
extends VTCore_Wordpress_Models_Hook {

  protected static $unique = 0;

  public function hook($form = NULL) {

    // Altering metabox items forms
    if (is_a($form, 'VTCore_Headline_Metabox_General')) {
      foreach ($form->findChildren('objectType', 'VTCore_Bootstrap_Form_BsColor') as $object) {
        $object->getParent()->removeChildren($object->getMachineId());
      }
    }

    return $form;
  }
}