<?php
/**
 * Class for filtering get_calendar
 * and style it.
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Filters_Get__Calendar
extends VTCore_Wordpress_Models_Hook {


  public function hook($output = NULL) {

    // Gotta be a better way to handle translation?
    $days = array(
      'monday' => __('Monday', 'medikal'),
      'tuesday' => __('Tuesday', 'medikal'),
      'wednesday' => __('Wednesday', 'medikal'),
      'thursday' => __('Thursday', 'medikal'),
      'friday' => __('Friday', 'medikal'),
      'saturday' => __('Saturday', 'medikal'),
      'sunday' => __('Sunday', 'medikal'),
    );

    $append = '';
    $prepend = '';
    $prev = '';
    $next = '';

    $today = date('d--l--M');
    list($date, $day, $month) = explode('--', $today);

    if (isset($days[$day])) {
      $day = $days[$day];
    }

    preg_match('/<td colspan="3" id="prev">(.*)<\/td>/', $output, $matches);
    if (isset($matches[1])) {
      $prev = str_replace('&laquo; ', '', $matches[1]);
    }

    preg_match('/<td colspan="3" id="next">(.*)<\/td>/', $output, $matches);
    if (isset($matches[1])) {
      $next = str_replace('&raquo; ', '', $matches[1]);
    }

    // Build large Calendars
    if (isset($day) && isset($date)) {

      // Nuke original caption
      $output = preg_replace('/<caption(.*?)caption>/s', "", $output);
      $prepend .= '<div class="calendar-large-date"><span class="date">' . $date . '</span><span class="day">' . $day . '</span></div>';
    }

    $prepend .= '<div class="calendar-wrapper">';

    // Build the navigation
    if (isset($prev) && isset($month) && isset($next)) {

      // Nuke footer
      $output = preg_replace('/<tfoot(.*?)tfoot>/s', "", $output);
      $prepend .= '<div class="calendar-top-nav"><span class="prev">'
                    . $prev . '</span><span class="month">'
                    . $month . '</span><span class="next">'
                    . $next . '</span></div>';
    }


    $append .= '</div>';


    return $prepend . $output . $append;
  }
}