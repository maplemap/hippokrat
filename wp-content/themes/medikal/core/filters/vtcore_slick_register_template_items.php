<?php
/**
 * Registering new template for slick advanced carousel
 * These registered templates can be used in shortcodes, objects
 * and visualcomposer
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Filters_VTCore__Slick__Register__Template__Items
extends VTCore_Wordpress_Models_Hook {

  public function hook($templates = NULL) {

    $templates['members-founder.php'] = __('Members Founder', 'medikal');
    $templates['members-doctors.php'] = __('Members Doctors', 'medikal');
    $templates['members-carousel.php'] = __('Members Listing', 'medikal');
    $templates['members-circle.php'] = __('Members Circle', 'medikal');
    $templates['product-carousel.php'] = __('WooCommerce Products', 'medikal');
    $templates['services-carousel.php'] = __('Services Listing', 'medikal');
    $templates['services-carousel-simple.php'] = __('Services Simple', 'medikal');
    $templates['department-carousel.php'] = __('Department Listing', 'medikal');
    $templates['department-carousel-flip.php'] = __('Department Flip', 'medikal');
    $templates['department-carousel-banner.php'] = __('Department Banner', 'medikal');
    $templates['department-carousel-full.php'] = __('Department Full', 'medikal');

    return $templates;
  }
}