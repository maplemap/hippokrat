<?php
/**
 * Registering Theme default vc grid templates
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Filters_VC__Grid__Item__PreDefined__Templates
extends VTCore_Wordpress_Models_Hook {

  public function hook($templates = NULL) {
    return VTCore_Zeus_Init::getStaticSystem('visualComposer')->addGrid($templates);
  }
}