<?php
/**
 * Hooking into victheme_service plugin to change
 * the schedule table header
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Zeus_Filters_VTCore__Services__Schedule__Table__Header
extends VTCore_Wordpress_Models_Hook {

  public function hook($headers = NULL) {

    $headers['members'] = __('Doctors', 'medikal');
    return $headers;
  }

}