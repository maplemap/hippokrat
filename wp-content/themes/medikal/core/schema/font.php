<?php
/**
 * Class for parsing the schema and
 * registering fonts to VTCore Google
 * Fonts loading system.
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Schema_Font {

  /**
   * Construct and parsing schema arrays
   * then register discovered fonts to
   * VTCore Font loading system.
   *
   * @param array $schema
   */
  public function __construct($schema = array()) {
    if (!empty($schema)) {

      // Processing schema
      foreach ($schema as $key => $rules) {

        // Processing each recorded schema rules
        foreach ($rules as $rulekey => $value) {

          // Skip on empty value
          if (empty($value)) {
            continue;
          }

          // Detect if the rule is for font family
          // and compare against json library.
          if (strpos($rulekey, 'font-family') !== false) {
            $value = str_replace(' ', '+', $value);
            if (VTCore_Wordpress_Init::getFactory('fonts')->get('library.' . $value)) {
              VTCore_Wordpress_Init::getFactory('fonts')->add('registered.fonts.' . $value, $value);
            }
          }

          // Detect for the font style
          if (strpos($rulekey, 'font-style') !== false) {
            VTCore_Wordpress_Init::getFactory('fonts')->add('registered.styles.' . $value, $value);
          }


          // Detect for font weight
          if (strpos($rulekey, 'font-weight') !== false) {
            VTCore_Wordpress_Init::getFactory('fonts')->add('registered.weight.' . $value, $value);
          }
        }
      }
    }
  }
}