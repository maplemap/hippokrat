<?php
/**
 * Building a group of pane into a single panes markup
 *
 * @see VTCore_Zeus_Schema_Pane
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Schema_Panes
extends VTCore_Bootstrap_Form_BsInstance {

  protected $context = array();

  /**
   * Overriding parent method, the BsInstance construct
   * method doesn't call buildElement Method.
   * @param array $context
   */
  public function __construct($context) {
    parent::__construct($context);
    $this->buildElement();
  }

  public function buildElement() {
    foreach ($this->getContexts() as $key => $context) {
      $this->addChildren(new VTCore_Zeus_Schema_Pane($context));
    }

    $this->preprocessElement();
  }

  /**
   * Allow per theme object filtering, different
   * theme may have different method for this.
   */
  protected function preprocessElement() {}
}