<?php
/**
 * Base Class for Schema System
 *
 * All registered schema must extend this class
 * and override the setContext method to register
 * the default schema value.
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Schema_Base {

  protected $context = array();
  protected $url;
  protected $themeBase = false;

  public function __construct($context = array(), $init = true) {

    if ($init) {
      $this->themeBase = get_template_directory_uri();
      $this->setContext();

      if (!empty($context)) {
        $this->context = VTCore_Utility::arrayMergeRecursiveDistinct($context, $this->context);

        // Final touch up for schema data
        foreach ($this->context['color'] as $key => $section) {
          foreach ($section as $type => $value) {

            // @bugfix gradient cannot remove entry bug
            if ($type == 'background-gradient' && isset($value['colors'])) {
              foreach ($value['colors'] as $delta => $color) {
                if (!isset($context['color'][$key][$type]['colors'][$delta])) {
                  unset($this->context['color'][$key][$type]['colors'][$delta]);
                }
              }
            }

          }
        }
      }
    }
  }


  /**
   * Child Class must extend this and define
   * the default context for the schema.
   */
  protected function setContext() {}


  /**
   * Get array tree from schema context
   */
  public function getData($key) {
    return isset($this->context[$key]) ? $this->context[$key] : false;
  }


  /**
   * Remove data dynamically
   *
   * @param $type
   * @param $key
   * @return $this
   */
  public function removeData($type, $key) {
    if (isset($this->context[$type]) && isset($this->context[$type][$key])) {
      unset($this->context[$type][$key]);
    }

    return $this;
  }


  /**
   * Get Single color value
   */
  public function getColorValue($key, $search) {
    return isset($this->context['color'][$key][$search]) ? $this->context['color'][$key][$search] : false;
  }



  /**
   * Get schema information
   */
  public function getSchema($key) {
    return isset($this->context['schema'][$key]) ? $this->context['schema'][$key] : false;
  }


  /**
   * Set color per type and key
   */
  public function setColor($type, $key, $value) {
    $this->context['color'][$type][$key] = $value;
  }


  public function setSchemaURL($path) {
    $this->url = stripslashes(VTCORE_ZEUS_THEME_URL . str_replace('register.php', '', str_replace('\\', '/', str_replace(VTCORE_ZEUS_THEME_PATH, '', $path))));
    return $this;
  }

  public function getSchemaURL($additional = '') {
    return $this->url . $additional;
  }

}