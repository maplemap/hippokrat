<?php

/**
 * Factory class for handling the schema system.
 *
 * @author jason.xie@victheme.com
 * @todo Total revamp this, perhaps use config models?
 */
class VTCore_Zeus_Schema_Factory {

  private $schemas = array();
  private $activeSchema = VTCORE_ZEUS_DEFAULT_SCHEMA;
  private $schemaFolder = '';
  private $colorBuilder;
  private $fontBuilder;
  private $transientKey;
  private $cache;
  private $css;


  /**
   * Construct and register the schemas
   */
  public function __construct() {

    // Set Active schema
    $this->activeSchema = get_theme_mod('schema-active', VTCORE_ZEUS_DEFAULT_SCHEMA);

    if (defined('VTCORE_CLEAR_CACHE') && VTCORE_CLEAR_CACHE) {
      $this->clearCache();
    }

    $this->schemaFolder = VTCORE_ZEUS_THEME_PATH . DIRECTORY_SEPARATOR . 'schemas';

    $this->autoloader = new VTCore_Autoloader('VTCore_Zeus_Schemas', $this->schemaFolder);
    $this->autoloader->setRemovePath('vtcore' . DIRECTORY_SEPARATOR . 'zeus' . DIRECTORY_SEPARATOR . 'schemas');
    $this->autoloader->register();


    // Registering schema assets
    VTCore_Wordpress_Init::getFactory('assets')
      ->get('library')
      ->detect($this->schemaFolder, get_template_directory_uri() . '/schemas/');

    // Retrieving schemas
    $this->RetrieveSchemas();

    do_action('vtcore_zeus_alter_schema', $this);

    // Build Schema Dynamic CSS
    $this->colorBuilder = new VTCore_Zeus_Schema_Color($this->getActiveSchema()
      ->getData('color'));

    // Build fonts
    $this->fontBuilder = new VTCore_Zeus_Schema_Font($this->getActiveSchema()
      ->getData('color'));
  }


  /**
   * Retrieve schema data and set the default schema
   * @return VTCore_Zeus_Schema_Factory
   */
  private function RetrieveSchemas() {

    $this->cache = get_transient($this->getTransientKey('directory'));
    if (empty($this->cache)) {
      foreach (new DirectoryIterator($this->schemaFolder) as $directory) {
        if ($directory->isDir() && !$directory->isDot()) {
          $this->cache[$directory->getFilename()] = $directory->getPath();
        }
      }

      set_transient($this->getTransientKey('directory'), $this->cache, 12 * HOUR_IN_SECONDS);
    }

    // Do not attempt to cache the object. WP + APC will break upon
    // deserialization!.
    $schemas = get_theme_mod('schemas', array());
    foreach ($this->cache as $name => $path) {
      $className = 'VTCore_Zeus_Schemas_' . ucfirst($name) . '_Register';

      if (class_exists($className, TRUE)) {

        $context = isset($schemas[$name]) ? $schemas[$name] : array();
        $object = new $className($context);
        $object->setSchemaURL($path);
        $this->schemas[$name] = $object;

        unset($object);
      }
    }

    return $this;
  }


  /**
   * Retrieve the current active schema data
   */
  public function getActiveSchema() {
    return $this->schemas[$this->activeSchema];
  }


  /**
   * Retrieve the current active schema machine id
   */
  public function getActiveSchemaID() {
    return $this->activeSchema;
  }


  /**
   * Set Active Schema
   */
  public function setActiveSchemaID($schemaID) {

    if (isset($this->schemas[$schemaID])) {
      $this->activeSchema = $schemaID;
    }

  }


  /**
   * Manipulate active schema style value
   */
  public function setStyle($type, $key, $value) {
    $this->schemas[$this->activeSchema]->setColor($type, $key, $value);
  }


  /**
   * Retrieve stored schema data
   */
  public function getSchema($name) {
    return $this->schemas[$name];
  }


  /**
   * Retrieve all registered schemas
   */
  public function getSchemas() {
    return $this->schemas;
  }


  /**
   * Get transient key
   */
  public function getTransientKey($additional = '') {

    if (empty($this->transientKey)) {
      // Set the class transient key
      $this->transientKey = 'vtcore_zeus_schema_cache_' . ACTIVE_THEME . '_';
    }

    return $this->transientKey . $additional;
  }


  /**
   * Retrieve the active schema css string
   */
  public function getCSS() {

    $this->css = get_transient($this->getTransientKey($this->activeSchema));

    // Force to rebuild on wp debug
    if (defined('WP_DEBUG') && WP_DEBUG) {
      $this->css = FALSE;
    }

    if ($this->css === FALSE) {
      $this->css = $this->colorBuilder->getCSS();
      set_transient($this->getTransientKey($this->activeSchema), $this->css, 12 * HOUR_IN_SECONDS);
    }

    return wp_unslash(strip_tags($this->css));
  }


  public function clearCache() {
    delete_transient($this->getTransientKey($this->activeSchema));
    delete_transient($this->getTransientKey('directory'));
    delete_transient($this->getTransientKey('schemas'));
    return $this;
  }

  public function getObject($object) {
    return isset($this->{$object}) ? $this->{$object} : FALSE;
  }

  public function setObject($key, $object) {
    $this->{$key} = $object;
    return $this;
  }

}