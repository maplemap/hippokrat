<?php
/**
 * Class for storing visual composer grid item templates
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Zeus_VisualComposer_Grid {

  /**
   * Storing the theme default grid item templates
   * @see filter vc_grid_item_predefined_templates
   */
  public function themeGridTemplates($templates) {

    $templates['themes-blog-grid'] = array(
      'name' => __('@Theme - Blog Grid', 'medikal'),
      'template' => '[vc_gitem][vc_gitem_animated_block][vc_gitem_zone_a height_mode="original" link="none" featured_image="" el_class="custom-blog-grid"][vc_gitem_row position="top"][vc_gitem_col width="1/1" featured_image="" el_class="post-image-block" css=".vc_custom_1434020087086{padding-top: 0px !important;padding-right: 0px !important;padding-bottom: 0px !important;padding-left: 0px !important;}"][vc_gitem_image link="post_link" alignment="center" border_color="grey" img_size="medium" css=".vc_custom_1434011343678{margin-bottom: 12px !important;}"][/vc_gitem_col][/vc_gitem_row][vc_gitem_row position="middle"][vc_gitem_col width="1/1" featured_image="" el_class="post-icon-block" css=".vc_custom_1434022046752{margin-top: -50px !important;padding-right: 40px !important;padding-left: 40px !important;}"][vc_icon link="post_link" type="typicons" icon_fontawesome="fa fa-adjust" icon_openiconic="vc-oi vc-oi-dial" icon_typicons="typcn typcn-clipboard" icon_entypo="entypo-icon entypo-icon-note" icon_linecons="vc_li vc_li-heart" color="white" background_style="rounded" background_color="juicy_pink" size="md" align="left"][/vc_gitem_col][/vc_gitem_row][vc_gitem_row position="bottom"][vc_gitem_col width="1/1" featured_image="" css=".vc_custom_1434022017827{border-bottom-width: 4px !important;padding-top: 40px !important;padding-right: 40px !important;padding-bottom: 40px !important;padding-left: 40px !important;background-color: #ffffff !important;border-bottom-color: #d5d1ca !important;border-bottom-style: solid !important;}" el_class="post-content-block"][vc_gitem_post_title link="none" font_container="tag:div|text_align:left" use_custom_fonts="" google_fonts="font_family:Abril%20Fatface%3Aregular|font_style:400%20regular%3A400%3Anormal" font=".visualplus_1434018258700 {font-size: 16px !important;font-family: Open+Sans !important;font-weight: 400 !important;color: #393f40 !important;}"][vc_gitem_post_date link="none" font_container="tag:div|text_align:left" use_custom_fonts="" google_fonts="font_family:Abril%20Fatface%3Aregular|font_style:400%20regular%3A400%3Anormal"][vc_gitem_post_excerpt link="none" font_container="tag:div|text_align:left" use_custom_fonts="" google_fonts="font_family:Abril%20Fatface%3Aregular|font_style:400%20regular%3A400%3Anormal" font=".visualplus_1434012491260 {font-size: 14px !important;font-family: Open+Sans !important;font-weight: 300 !important;color: #747474 !important;}"][/vc_gitem_col][/vc_gitem_row][/vc_gitem_zone_a][vc_gitem_zone_b][vc_gitem_row position="top"][vc_gitem_col width="1/1"][/vc_gitem_col][/vc_gitem_row][vc_gitem_row position="middle"][vc_gitem_col width="1/2"][/vc_gitem_col][vc_gitem_col width="1/2"][/vc_gitem_col][/vc_gitem_row][vc_gitem_row position="bottom"][vc_gitem_col width="1/1"][/vc_gitem_col][/vc_gitem_row][/vc_gitem_zone_b][/vc_gitem_animated_block][/vc_gitem]',
    );

    return $templates;
  }

}