<?php
/**
 * Visual Composer addon for Zeus Framework
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Zeus_VisualComposer_Init {

  protected $grid;
  protected $template;

  /**
   * Registering theme to visual composer
   */
  public function registerTheme() {
    vc_set_as_theme(TRUE);
    vc_set_shortcodes_templates_dir(VTCORE_ZEUS_THEME_PATH . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'visualcomposer' . DIRECTORY_SEPARATOR);
  }


  /**
   * Collect all new parameters in this method
   * This is done this way because this needs to be invoked
   * on hook action init.
   */
  public function addParams() {}


  /**
   * Collect all params altering here, this is done
   * this way because this is need to be invoked after
   * hook action init.
   */
  public function alterParams() {
    vc_remove_param('vc_tta_tour', 'style');
    vc_add_param('vc_tta_tour', array(
      'type' => 'dropdown',
      'param_name' => 'style',
      'value' => array(
        __( 'Classic', 'js_composer' ) => 'classic',
        __( 'Modern', 'js_composer' ) => 'modern',
        __( 'Flat', 'js_composer' ) => 'flat',
        __( 'Outline', 'js_composer' ) => 'outline',
        __( 'Theme', 'js_composer' ) => 'theme',
      ),
      'heading' => __( 'Style', 'js_composer' ),
      'description' => __( 'Select tour display style.', 'js_composer' ),
    ));
  }


  /**
   * Adding default templates
   */
  public function addTemplate($templates) {

    // Calling the template storage and
    // inject the template strings.
    $this->templates = new VTCore_Zeus_VisualComposer_Template();
    return $this->templates->themeTemplates($templates);
  }


  /**
   * Adding custom grid
   * @return array
   */
  public function addGrid($templates) {
    $this->grid = new VTCore_Zeus_VisualComposer_Grid();
    return $this->grid->themeGridTemplates($templates);
  }

}