<?php
/**
 * Configuration for the theme regions
 *
 * @author jason.xie@victheme.com
 * @see VTCore_Wordpress_Layout_Region
 *
 */
class VTCore_Zeus_Config_Regions
extends VTCore_Wordpress_Models_Config {

  protected $database = 'regions';
  protected $filter = 'vtcore_zeus_regions_context_alter';
  protected $loadFunction = 'get_theme_mod';
  protected $saveFunction = 'set_theme_mod';
  protected $deleteFunction = 'remove_theme_mod';

  public function register(array $options) {


    // Set theme default regions
    $this->options = array(

      // Always put sidebar on top of the array
      // simply because Wordpress is not smart enough
      // to assign default widget to proper region
      // when theme is initialized at the first time.
      'sidebar' => array(
        'parent' => 'maincontent',
        'mode' => 'dynamic',
        'weight' => 1,
        'grids' => array(
          'columns' => array(
            'mobile' => '12',
            'tablet' => '3',
            'small' => '3',
            'large' => '3',
          ),
        ),
        'arguments' => array(
          'name' => __('Sidebar', 'medikal'),
          'id' => 'sidebar',
          'description' => __('This region can be repositioned from left to right using the theme configurations.', 'medikal'),
          //'before_widget' => '<div id="%1$s" class="widget col-xs-6 col-sm-6 col-md-12 col-lg-12 %2$s">',
          'before_title'  => '<h2 class="widgettitle">',
        ),
      ),


      'logo' => array(
        'parent' => 'header',
        'mode' => 'static',
        'weight' => 1,
        'grids' => array(
          'columns' => array(
            'mobile' => '8',
            'tablet' => '4',
            'small' => '4',
            'large' => '4',
          ),
        ),
        'arguments' => array(
          'name' => __('Logo', 'medikal'),
          'id' => 'logo',
          'description' => __('A Static region for building logo element in the header', 'medikal'),
        ),
      ),

      'navigation' => array(
        'parent' => 'header',
        'mode' => 'static',
        'weight' => 2,
        'grids' => array(
          'columns' => array(
            'mobile' => '0',
            'tablet' => '8',
            'small' => '8',
            'large' => '8',
          ),
        ),
        'arguments' => array(
          'name' => __('Header Navigation', 'medikal'),
          'id' => 'navigation',
          'description' => __('A Static region for building main navigation element in the header', 'medikal'),
        ),
      ),

      'slider' => array(
        'parent' => 'slider',
        'mode' => 'dynamic',
        'weight' => 0,
        'grids' => array(
          'columns' => array(
            'mobile' => '12',
            'tablet' => '12',
            'small' => '12',
            'large' => '12',
          ),
        ),
        'arguments' => array(
          'name' => __('Slider', 'medikal'),
          'id' => 'slider',
          'description' => __('Slider Region on top of the main content, suitable for slider elements.', 'medikal'),
        ),
      ),


      'content' => array(
        'parent' => 'maincontent',
        'mode' => 'static',
        'weight' => 0,
        'grids' => array(
          'columns' => array(
            'mobile' => '12',
            'tablet' => '9',
            'small' => '9',
            'large' => '9',
          ),
        ),
        'arguments' => array(
          'name' => __('Content', 'medikal'),
          'id' => 'content',
          'description' => __('A Static region for building content element in the main content', 'medikal'),
        ),
      ),

      'postfaceone' => array(
        'parent' => 'postface',
        'mode' => 'dynamic',
        'weight' => 0,
        'grids' => array(
          'columns' => array(
            'mobile' => '6',
            'tablet' => '3',
            'small' => '3',
            'large' => '3',
          ),
        ),
        'arguments' => array(
          'name' => __('PostFace One', 'medikal'),
          'id' => 'postfaceone',
          'description' => __('First postface region, located on the left side of the postface.', 'medikal'),
        ),
      ),
      'postfacetwo' => array(
        'parent' => 'postface',
        'mode' => 'dynamic',
        'weight' => 1,
        'grids' => array(
          'columns' => array(
            'mobile' => '6',
            'tablet' => '3',
            'small' => '3',
            'large' => '3',
          ),
        ),
        'arguments' => array(
          'name' => __('PostFace Two', 'medikal'),
          'id' => 'postfacetwo',
          'description' => __('Second postface region, located after the postface one region.', 'medikal'),
        ),
      ),
      'postfacethree' => array(
        'parent' => 'postface',
        'mode' => 'dynamic',
        'weight' => 2,
        'grids' => array(
          'columns' => array(
            'mobile' => '6',
            'tablet' => '3',
            'small' => '3',
            'large' => '3',
          ),
        ),
        'arguments' => array(
          'name' => __('PostFace Three', 'medikal'),
          'id' => 'postfacethree',
          'description' => __('Third postface region, located after the postface two region.', 'medikal'),
        ),
      ),
      'postfacefour' => array(
        'parent' => 'postface',
        'mode' => 'dynamic',
        'weight' => 3,
        'grids' => array(
          'columns' => array(
            'mobile' => '6',
            'tablet' => '3',
            'small' => '3',
            'large' => '3',
          ),
        ),
        'arguments' => array(
          'name' => __('PostFace Four', 'medikal'),
          'id' => 'postfacefour',
          'description' => __('Fourth postface region, located after the postface three region.', 'medikal'),
        ),
      ),

      'footerone' => array(
        'parent' => 'footer',
        'mode' => 'dynamic',
        'weight' => 0,
        'grids' => array(
          'columns' => array(
            'mobile' => '6',
            'tablet' => '3',
            'small' => '3',
            'large' => '3',
          ),
        ),
        'arguments' => array(
          'name' => __('Footer One', 'medikal'),
          'id' => 'footerone',
          'description' => __('First footer region, located on the left side of the footer.', 'medikal'),
        ),
      ),
      'footertwo' => array(
        'parent' => 'footer',
        'mode' => 'dynamic',
        'weight' => 1,
        'grids' => array(
          'columns' => array(
            'mobile' => '6',
            'tablet' => '6',
            'small' => '6',
            'large' => '6',
          ),
        ),
        'arguments' => array(
          'name' => __('Footer Two', 'medikal'),
          'id' => 'footertwo',
          'description' => __('Second footer region, located after the footer one region.', 'medikal'),
        ),
      ),
      'footerthree' => array(
        'parent' => 'footer',
        'mode' => 'dynamic',
        'weight' => 2,
        'grids' => array(
          'columns' => array(
            'mobile' => '12',
            'tablet' => '3',
            'small' => '3',
            'large' => '3',
          ),
        ),
        'arguments' => array(
          'name' => __('Footer Three', 'medikal'),
          'id' => 'footerthree',
          'description' => __('Third footer region, located after the footer two region.', 'medikal'),
        ),
      ),
      

      'copyright' => array(
        'parent' => 'full-footer',
        'mode' => 'static',
        'weight' => 1,
        'grids' => array(
          'columns' => array(
            'mobile' => '12',
            'tablet' => '12',
            'small' => '12',
            'large' => '12',
          ),
        ),
        'arguments' => array(
          'name' => __('Copyright', 'medikal'),
          'id' => 'copyright',
          'description' => __('Copyright region in full footer', 'medikal'),
        ),
      ),
      
    );


    // Merge the user supplied options
    $this->merge($options);

    // Apply the hookable filter
    $this->filter();

    // Inject from database
    $this->load();

    // Backward compatibility
    do_action('vtcore_zeus_alter_regions', $this);

    return $this;
  }
}