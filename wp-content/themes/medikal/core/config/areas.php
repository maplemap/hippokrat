<?php
/**
 * Configuration for the theme layout areas
 *
 * @author jason.xie@victheme.com
 * @see VTCore_Wordpress_Layout_Area
 *
 */
class VTCore_Zeus_Config_Areas
extends VTCore_Wordpress_Models_Config {

  protected $database = 'areas';
  protected $filter = 'vtcore_zeus_area_context_alter';
  protected $loadFunction = 'get_theme_mod';
  protected $savefunction = 'set_theme_mod';
  protected $deleteFunction = 'remove_theme_mod';

  public function register(array $options) {


    // Set theme default regions
    $this->options = array(
      'header' => array(
        'id' => 'header',
        'name' => __('Header', 'medikal'),
        'description' => __('Theme header section.', 'medikal'),
        'weight' => 1,
      ),
      'slider' => array(
        'id' => 'slider',
        'name' => __('Slider', 'medikal'),
        'description' => __('Theme main slider section.', 'medikal'),
        'weight' => 3,
      ),
      'maincontent' => array(
        'id' => 'maincontent',
        'name' => __('Main Content', 'medikal'),
        'description' => __('Theme main content section.', 'medikal'),
        'weight' => 4,
      ),
      'postface' => array(
        'id' => 'postface',
        'name' => __('Post Face', 'medikal'),
        'description' => __('Theme main content section.', 'medikal'),
        'weight' => 5,
      ),
      'footer' => array(
        'id' => 'footer',
        'name' => __('Footer', 'medikal'),
        'description' => __('Theme footer section.', 'medikal'),
        'weight' => 6,
      ),
      'full-footer' => array(
        'id' => 'full-footer',
        'name' => __('Full Footer', 'medikal'),
        'description' => __('Theme full footer section.', 'medikal'),
        'weight' => 7,
      ),
    );


    // Only build if headline is enabled
    if (defined('VTCORE_HEADLINE_LOADED')) {
      $this->options['headline'] = array(
        'id' => 'headline',
        'name' => __('Headline', 'medikal'),
        'description' => __('Theme headline section.', 'medikal'),
        'weight' => 2,
      );
    }

    // Merge the user supplied options
    $this->merge($options);

    // Apply the hookable filter
    $this->filter();

    // Inject from database
    $this->load();

    // Backward compatibility
    do_action('vtcore_zeus_alter_areas', $this);

    return $this;
  }

}