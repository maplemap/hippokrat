<?php
/**
 * Simple class for managing theme features.
 *
 * Themer must fill out the theme features variables
 * in this class for setting up the default values for
 * the theme.
 *
 * User can change the configuration via theme options
 * and save them to the theme database entry as wordpress
 * theme_mod specifies.
 *
 * Other plugin can interact with the features system by
 * using Wordpress action vtcore_zeus_alter_features
 * and use the object public methods to alter the features.
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Config_Options
extends VTCore_Wordpress_Models_Config {

  protected $database = 'features';
  protected $filter = 'vtcore_zeus_features_context_alter';
  protected $loadFunction = 'get_theme_mod';
  protected $savefunction = 'set_theme_mod';
  protected $deleteFunction = 'remove_theme_mod';

  /**
   * Centralized location for registering default
   * values for all the theme features excluding
   * the default values for schema settings.
   */
  public function register(array $options) {
    $this->options = array(
      'show' => array(
        'teaser' => array(
          'image' => true,
          'title' => true,
          'byline' => true,
          'line' => true,
          'excerpt' => true,
          'readmore' => true, 
        ), 

        'full' => array(
          'image' => true,
          'title' => true,
          'byline' => true,
          'content' => true,
          'share' => true,
          'tag' => true,
          'author' => true,
          'post_nav' => true,
          'comments' => true, 
          'comment_form' => true,
        ), 

        'notfound' => array(
          'title' => true, 
          'content' => true, 
        ), 

        'page' => array(
          'image' => true, 
          'title' => true, 
          'byline' => true, 
          'comments' => true, 
          'comment_form' => true, 
          'share' => true, 
          'content' => true, 
          'post_nav' => true, 
        ), 

        'product' => array(
          'catalog' => array(
            'counter' => true, 
            'sorter' => true, 
          ), 
          'teaser' => array(
            'image' => true, 
            'title' => true,
            'price' => true, 
            'flash' => true, 
            'rating' => true, 
            'cart' => true,
            'excerpt' => true,
          ), 
          'full' => array(
            'flash' => true, 
            'image' => true, 
            'title' => true, 
            'rating' => true, 
            'price' => true, 
            'excerpt' => true, 
            'cart' => true, 
            'meta' => true, 
            'share' => true, 
            'tabs' => true, 
            'upsell' => true, 
            'related' => true, 
          )
        ),

        'department' => array(
          'archive' => array(
            'icon' => true,
            'image' => true,
            'title' => true,
            'slogan' => true,
            'description' => true,
            'readmore' => true,
          ), 
          'single' => array(
            'image' => true,
            'slogan' => true,
            'title' => true,
            'content' => true,
            'services' => true,
            'members' => true,
            'promotional' => true,
          ), 
        ), 

        'services' => array(
          'archive' => array(
            'image' => true,
            'icon' => true,
            'title' => true,
            'excerpt' => true,
            'price' => true,
            'readmore' => true,
          ),
          'single' => array(
            'image' => true,
            'price' => true,
            'title' => true,
            'content' => true,
            'schedule' => true,
            'department' => true,
          ),
        ), 

        'members' => array(
          'archive' => array(
            'image' => true,
            'name' => true,
            'excerpt' => true,
            'position' => true,
            'speciality' => true,
            'social' => true,
            'readmore' => true,
          ),
          'single' => array(
            'image' => true,
            'name' => true,
            'speciality' => true,
            'position' => true,
            'content' => true,
            'department' => true,
            'education' => true,
            'experience' => true,
            'office' => true,
            'workdays' => true,
            'email' => true,
            'services' => true,
            'training' => true,
            'social' => true,
            'education_table' => true,
            'experience_table' => true,
            'service_table' => true,
          ),
        ), 

        'timetable' => array(
          'weekdate' => true,
          'filter' => true,
          'title' => true,
          'department' => true,
          'members' => true,
          'location' => true,
        ), 

        'footer' => array(
          'copyright' => true, 
        ), 
      ),

      'sidebars' => array(
        'teaser' => 'none',
        'full' => 'right', 
        'notfound' => 'none', 
        'page' => 'right', 
        'product_archive' => 'left',
        'product' => 'right', 
        'shop_pages' => 'none',
        'department_archive' => 'none',
        'deparment' => 'right',
        'services_archive' => 'none',
        'services' => 'none',
        'members_archive' => 'none',
        'members' => 'right',
      ), 

      'options' => array(
        'logo' => array(
          'image' => VTCORE_ZEUS_THEME_URL . '/logo.png',
          'text' => '',
          'slogan' => '',
          'width' => '132',
          'height' => '43'
        ), 
        'favicon' => array(
          'image' => VTCORE_ZEUS_THEME_URL . '/favicon.ico', 
        ),
        'products' => array(
          'archive' => array(
            'ajax' => true,
            'template' => 'detail',
            'pager' => array(
              'mini' => false,
              'infinite' => false,
            ),
            '404' => array(
              'title' => __('OOPS NOT FOUND', 'medikal'),
              'description' => __('Sorry, but the requested product was not found on this site. Browse our product category for more products.', 'medikal'),
            ),
            'grids' => array( 
              'columns' => array(
                'mobile' => 6,
                'tablet' => 6,
                'small'  => 6,
                'large'  => 6,
              ),
            ),
          ), 

          'single' => array(
            'image' => array( 
              'grids' => array(
                'columns' => array(
                  'mobile' => 6,
                  'tablet' => 5,
                  'small'  => 5,
                  'large'  => 5,
                ),
              ),
            ), 
            'summary' => array( 
              'grids' => array(
                'columns' => array(
                  'mobile' => 6,
                  'tablet' => 7,
                  'small'  => 7,
                  'large'  => 7,
                ),
              ),
            ), 
            'normal' => array( 
              'width' => 600,
              'height' => 600,
              'crop' => false,
            ), 
            'zoom' => array( 
              'width' => 1800,
              'height' => 1800,
              'crop' => false,
            ), 
            'fullscreen' => array( 
              'width' => 1200,
              'height' => 1200,
              'crop' => false,
            ), 
            'fotorama' => array( 

              // Dimensions
              'width' => '100%',
              'height' => '100%',
              'ratio' => '',
              'minwidth' => '',
              'maxwidth' => '100%',
              'minheight' => '',
              'maxheight' => '400px',
              'captions' => false,
              'margin' => '',
              'glimpse' => '',

              // Animations
              'loop' => true,
              'shuffle' => true,
              'startindex' => '',
              'autoplay' => '',
              'stopautoplayontouch' => true,
              'shadows' => true,
              'transition' => 'slide',
              'clicktransition' => 'crossfade',
              'transitionduration' => '',

              // Key operations
              'arrows' => 'false',
              'keyboard' => true,
              'click' => true,
              'swipe' => true,
              'trackpad' => true,


              //  Navigations
              'navposition' => 'bottom',
              'direction' => 'rtl',
              'nav' => 'thumbs',
              'navwidth' => '',

              //  Full screens
              'allowfullscreen' => 'native',

              //  Thumbnails
              'thumbwidth' => '80',
              'thumbheight' => '80',
              'thumbmargin' => '',
              'thumbborderwidth' => '',

              //  Image fitting
              'fit' => 'contain',
              'thumbfit' => 'cover',

              // Hover zoom
              'hoverzoom' => true,
            ), 
            'elevatezoom' => array( 
              'responsive' => true,
              'scrollZoom' => false,
              'imageCrossfade' => false,
              'loadingIcon' => false,
              'easing' => true,
              'easingType' => 'easeOutExpo',
              'easingDuration' => 2000,
              'lensSize' => 100,
              'zoomWindowWidth' => 400,
              'zoomWindowHeight' => 400,
              'zoomWindowOffsetx' => 0,
              'zoomWindowOffsety' => 0,
              'zoomWindowPosition' => 1,
              'lensFadeIn' => true,
              'lensFadeOut' => true,
              'zoomWindowFadeIn' => true,
              'zoomWindowFadeOut' => true,
              'zoomTintFadeIn' => false,
              'zoomTintFadeOut' => false,
              'borderSize' => 4,
              'zoomLens' => true,
              'borderColour' => '#888888',
              'lensBorder'  => 2,
              'lensShape' => 'square',
              'zoomType' => 'window',
              'containLensZoom' => true,
              'lensColour' => '#cccccc',
              'lensOpacity' => 0.4,
              'lenszoom' => false,
              'tint' => false,
              'tintColour' => '#333333',
              'tintOpacity' => 0.4,
              'cursor' => 'default',
            ), 
            'upsell' => array(
              'title' => __('Compare Products', 'medikal'),
              'layout' => 'fitrow', 
              'teaser' => array(
                'show' => array(
                  'image' => true, 
                  'title' => true, 
                  'price' => true, 
                  'flash' => true, 
                  'rating' => true, 
                  'cart' => true, 
                ),
                'grids' => array( 
                  'columns' => array(
                    'mobile' => 6,
                    'tablet' => 4,
                    'small'  => 4,
                    'large'  => 4,
                  ),
                ),
              ),
            ), 
            'related' => array(
              'title' => __('Related Products', 'medikal'),
              'layout' => 'fitrow', 
              'teaser' => array(
                'show' => array(
                  'image' => true, 
                  'title' => true, 
                  'price' => true, 
                  'flash' => true, 
                  'rating' => true, 
                  'cart' => true, 
                ),
                'grids' => array( 
                  'columns' => array(
                    'mobile' => 6,
                    'tablet' => 4,
                    'small'  => 4,
                    'large'  => 4,
                  ),
                ),
              ),
            ), 
          ), 

          'cart' => array(
            'cross' => array(
              'title' => __('You may be interested in', 'medikal'),
              'layout' => 'fitrow', 
              'teaser' => array(
                'show' => array(
                  'image' => true, 
                  'title' => true, 
                  'price' => true, 
                  'flash' => true, 
                  'rating' => true, 
                  'cart' => true, 
                ),
                'grids' => array( 
                  'columns' => array(
                    'mobile' => 6,
                    'tablet' => 4,
                    'small'  => 4,
                    'large'  => 4,
                  ),
                ),
              ),
            ),
            'total' => array(
              'title' => __('Cart Totals', 'medikal'),
            ),
          ), 
        ),
        'services' => array(
          'archive' => array(
            'title' => 'We are a team of young<br/> professionals passionate in our work.',
            'description' =>  array(
              'left' => 'Duis autem vel eum iriure dolor in hendrerit n vuew lputate velit esse molestie consequat, vel illum dolore eufe ugiat nulla facilisis at vero.',
              'right' => 'Duis autem vel eum iriure dolor in hendrerit n vuew lputate velit esse molestie consequat, vel illum dolore eufe ugiat nulla facilisis at vero.',
            ),
            '404' => array(
              'title' => __('OOPS NOT FOUND', 'medikal'),
              'description' => __('No Services found.', 'medikal'),
            ),
            'template' => 'services-grid',
            'pager' => array(
              'mini' => false,
              'infinite' => false,
            ),
            'grids' => array(
              'columns' => array(
                'mobile' => 6,
                'tablet' => 4,
                'small'  => 4,
                'large'  => 4,
              ),
            ),
          ),
        ),
        'members' => array(
          'archive' => array(
            'title' => 'We are a team of young<br/> professionals passionate in our work.',
            'description' =>  array(
              'left' => 'Duis autem vel eum iriure dolor in hendrerit n vuew lputate velit esse molestie consequat, vel illum dolore eufe ugiat nulla facilisis at vero.',
              'right' => 'Duis autem vel eum iriure dolor in hendrerit n vuew lputate velit esse molestie consequat, vel illum dolore eufe ugiat nulla facilisis at vero.',
            ),
            '404' => array(
              'title' => __('OOPS NOT FOUND', 'medikal'),
              'description' => __('No Members found.', 'medikal'),
            ),
            'pager' => array(
              'mini' => false,
              'infinite' => false,
            ),
            'grids' => array(
              'columns' => array(
                'mobile' => 6,
                'tablet' => 3,
                'small'  => 3,
                'large'  => 3,
              ),
            ),
          ),
        ),
        'timetable' => array(
          'shifts' => array(
            'morning' => array(
              'enabled' => true,
              'title' => __('Morning Shift', 'medikal'),
              'start' => '08:00AM',
              'end' => '04:00PM',
            ),
            'afternoon' => array(
              'enabled' => true,
              'title' => __('Afternoon Shift', 'medikal'),
              'start' => '04:00PM',
              'end' => '1:00AM',
            ),
            'night' => array(
              'enabled' => true,
              'title' => __('Night Shift', 'medikal'),
              'start' => '1:00AM',
              'end' => '8:00AM'
            ),
          ), 
          'days' => array(
            'monday' => __('Monday', 'medikal'),
            'tuesday' => __('Tuesday', 'medikal'),
            'wednesday' => __('Wednesday', 'medikal'),
            'thursday' => __('Thursday', 'medikal'),
            'friday' => __('Friday', 'medikal'),
            'saturday' => __('Saturday', 'medikal'),
            'sunday' => __('Sunday', 'medikal'),
          ), 
        ),
        'department' => array(
          'archive' => array(
            'title' => 'We are a team of young<br/> professionals passionate in our work.',
            'description' =>  array(
              'left' => 'Duis autem vel eum iriure dolor in hendrerit n vuew lputate velit esse molestie consequat, vel illum dolore eufe ugiat nulla facilisis at vero.',
              'right' => 'Duis autem vel eum iriure dolor in hendrerit n vuew lputate velit esse molestie consequat, vel illum dolore eufe ugiat nulla facilisis at vero.',
            ),
            'template' => 'department-grid', // need additional options
            'ajax' => true, 
            'pager' => array(
              'mini' => false,
              'infinite' => false,
            ), 
            '404' => array(
              'title' => __('OOPS NOT FOUND', 'medikal'),
              'description' => __('No Department found.', 'medikal'),
            ), 
            'grids' => array(
              'columns' => array(
                'mobile' => 6,
                'tablet' => 4,
                'small'  => 4,
                'large'  => 4,
              ),
            ), 
          ), 
        ),
        'teasers' => array(
          'title' => 'We are a team of young<br/> professionals passionate in our work.',
          'description' =>  array(
            'left' => 'Duis autem vel eum iriure dolor in hendrerit n vuew lputate velit esse molestie consequat, vel illum dolore eufe ugiat nulla facilisis at vero.',
            'right' => 'Duis autem vel eum iriure dolor in hendrerit n vuew lputate velit esse molestie consequat, vel illum dolore eufe ugiat nulla facilisis at vero.',
          ),
          'template' => 'blog-metro', // blog-masonry || blog-list
          'pager' => array(
            'ajax' => true, 
            'mini' => false, 
            'infinite' => false, 
          ),
        ), 

        'menu' => array(
          'disable_parent' => false, 
          'enable_sticky' => true, 
          'enable_slick' => true, 
        ),  

        'animsition' => array(
          'enable' => false,
          'loading_text' => __('Loading ..', 'medikal'),
          'animation_in' => 'zoom-in', 
          'in_duration' => 2000, 
          'animation_out' => 'zoom-out', 
          'out_duration' => 2000, 
        ), 

        'form' => array(
          'jvfloat' => true, 
        ),  

        'page' => array(
          'maxwidth' => 'max-1170', 
          'style' => 'style-normal', 
          'responsive' => 'bs-responsive', 
        ), 

        'performance' => array(
          'aggregate' => false, 
          'minify' => false, 
        ), 

        'customizer' => array(
          'general' => true, 
          'style' => true, 
          'layout' => true, 
          'widgets' => true, 
          'shop' => true,
          'services' => true,
          'members' => true,
          'department' => true,
          'timetable' => true,
        ), 

        'notfound' => array(
          'title' => __('Oops. This Page Could Not Be Found!', 'medikal'),
          'content' => __('Oops! 404', 'medikal'),
        ), 

        'footer' => array(
          'copyrighttext' => __('Medikal Premium E-Commerce Theme built by VicTheme.com', 'medikal'),
        ), 
      ), 
    );

    // Merge the user supplied options
    $this->merge($options);

    // Apply the hookable filter
    $this->filter();

    // Inject from database
    $this->load();

    // Backward compatibility
    do_action('vtcore_zeus_alter_features', $this);

    return $this;
  }
}