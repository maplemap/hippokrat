<?php
/**
 * Page callback class for building the Custom
 * CSS entry form for user to add custom css
 * easily and Zeus will print the custom css
 * using inline css via wp_enqueue_scripts action.
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Pages_CSS
extends VTCore_Zeus_Pages_Model {

  /**
   * Override parent method
   * @return VTCore_Zeus_Pages_Css
   */
  protected function config() {

    $this->icon = 'css3';
    $this->title = __('Custom CSS', 'medikal');

    $this->object = new VTCore_Zeus_Panels_CSS(array(
      'process' => !empty($_POST['theme']),
    ));

    VTCore_Wordpress_Utility::loadAsset('codemirror-css');

    return $this;
  }
}