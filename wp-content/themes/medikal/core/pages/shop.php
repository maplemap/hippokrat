<?php
/**
 * Simple plugin system for configuration panels
 *
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Pages_Shop
extends VTCore_Zeus_Pages_Model {

  /**
   * Override parent method
   * @return VTCore_Zeus_Pages_Style
   */
  protected function config() {

    $this->icon = 'shopping-cart';
    $this->title = __('Shop Options', 'medikal');

    $this->object = new VTCore_Zeus_Panels_Shop(array(
      'features' => new VTCore_Zeus_Config_Options(),
      'process' => !empty($_POST['theme']),
    ));

    return $this;
  }

}