<?php
/**
 * Simple plugin system for configuration panels
 *
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Pages_Department
extends VTCore_Zeus_Pages_Model {

  /**
   * Override parent method
   * @return VTCore_Zeus_Pages_Style
   */
  protected function config() {

    $this->icon = 'tasks';
    $this->title = __('Department Options', 'medikal');

    // Force refresh features
    VTCore_Zeus_Init::getFeatures()->register(array());

    $this->object = new VTCore_Zeus_Panels_Department(array(
      'features' => VTCore_Zeus_Init::getFeatures(),
      'process' => !empty($_POST['theme']),
    ));

    return $this;
  }

}