<?php
/**
 * Building the Layout Configuration Page
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Zeus_Pages_Layout
extends VTCore_Zeus_Pages_Model {

  /**
   * Override parent method
   * @return VTCore_Zeus_Pages_Style
   */
  protected function config() {

    $this->icon = 'dashboard';
    $this->title = __('Layout Options', 'medikal');

    // Force refresh Object
    VTCore_Zeus_Init::getFeatures()->register(array());
    VTCore_Zeus_Init::getLayout()->clearCache()->resetChildren()->buildElement();

    $this->object = new VTCore_Zeus_Panels_Layout(array(
      'features' => VTCore_Zeus_Init::getFeatures(),
      'layout' => VTCore_Zeus_Init::getLayout(),
      'process' => !empty($_POST['theme']),
    ));

    return $this;
  }


  /**
   * Ajax callback function
   *
   * @param $post
   * @return array
   */
  public function renderAjax($post) {

    $_POST = $post['data'];

    $render = array();

    switch ($post['queue']) {
      case 'change-header' :

        // change the header template temporarily
        VTCore_Zeus_Init::getFeatures()->mutate('options.header.template', $post['elval']);

        // Need to build fresh layout to invoke the filters
        // for the area and regions.
        $areas = new VTCore_Zeus_Config_Areas();
        $regions = new VTCore_Zeus_Config_Regions();

        VTCore_Zeus_Init::getLayout()
          ->clearCache()
          ->resetChildren()
          ->resetContext()
          ->setContext(array(
            'areas' => $areas->extract(),
            'regions' => $regions->extract(),
            'blocks' => array(),
          ))
          ->buildElement();


        $this->object = new VTCore_Zeus_Panels_Layout(array(
          'features' => VTCore_Zeus_Init::getFeatures(),
          'layout' => VTCore_Zeus_Init::getLayout(),
          'process' => false,
          'build' => 'object',
        ));

        $panel = $this->object->findChildren('attributes', 'id', 'vtcore-grid-form-target');

        if (!empty($panel)) {
          $panel = array_shift($panel);

          $render['action'][] = array(
            'mode' => 'replace',
            'target' => $post['target'],
            'content' => $panel->__toString(),
          );
        }

        break;
    }

    return $render;
  }

}