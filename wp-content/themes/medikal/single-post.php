<?php
/**
 * Default Single Post Template
 *
 * @author jason.xie@victheme.com
 */

get_header();
?>

  <!-- Main Content -->
  <div id="maincontent" class="area clearfix">
    <div class="container-fluid">
      <div class="row">

        <?php
        if (VTCore_Zeus_Init::getFeatures()->get('show.full.taxonomybar')) {
          get_template_part('templates/content/taxonomy');
        }
        ?>

        <div id="content"
             class="region pseudo-background
			     <?php if (VTCore_Zeus_Utility::getSidebar('full')) {
               echo VTCore_Zeus_Utility::getColumnSize('content');
             } ?>
			     with-sidebar-<?php echo VTCore_Zeus_Utility::getSidebar('full'); ?>">

          <?php
          wp_reset_postdata();
          while (have_posts()) {

            // Build the single post entry using main loop
            the_post();
            get_template_part('templates/content/single', get_post_format());

          }

          ?>
        </div>

        <?php
        // Build sidebar.
        if (VTCore_Zeus_Utility::getSidebar('full') == 'right'
          || VTCore_Zeus_Utility::getSidebar('full') == 'left'
        ) {
          get_sidebar('sidebar');
        }
        ?>

      </div>
    </div>
  </div>


<?php get_footer(); ?>