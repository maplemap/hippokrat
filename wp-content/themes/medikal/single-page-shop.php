<?php
/**
 * Template for displaying the woocommerce
 * special pages such as cart, myaccount and checkout page
 *
 * @author jason.xie@victheme.com
 * @see hook template_include for the redirection logic
 */

get_header();

?>


  <!-- Main Content -->
  <div id="maincontent" class="area clearfix shop-pages">
    <div class="container-fluid">
      <div class="row">

        <div id="content"
             class="region col-xs-12 pseudo-background
             <?php if (VTCore_Zeus_Utility::getSidebar('shop_pages')) echo VTCore_Zeus_Utility::getColumnSize('content'); ?>
             <?php echo 'with-sidebar-' . VTCore_Zeus_Utility::getSidebar('shop_pages'); ?>
             ">

          <?php
          wp_reset_postdata();
          while (have_posts()) {
            the_post();
            the_content();
          }
          ?>
        </div>

        <?php
        // Build sidebar.
        if (VTCore_Zeus_Utility::getSidebar('shop_pages')) {
          get_sidebar('sidebar');
        }
        ?>

      </div>
    </div>
  </div>


<?php get_footer(); ?>