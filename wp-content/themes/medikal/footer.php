<?php
/**
 * Footer Templates
 * @author jason.xie@victheme.com
 */


  get_template_part('postface');
?>


<?php if (VTCore_Zeus_Utility::isActiveSidebar('footerone')
          || VTCore_Zeus_Utility::isActiveSidebar('footertwo')
          || VTCore_Zeus_Utility::isActiveSidebar('footerthree')) : ?>

  <footer id="footer" class="area clearfix">
    <div class="container-fluid">
      <div class="row js-isotope" data-isotope-options="<?php echo esc_attr(json_encode(array(
        'itemSelector' => '.region',
        'layoutMode' => 'fitRows',
        'fitRows' => array(
          'equalheight' => true,
          'gutter' => array(
            'width' => 0,
            'height' => 0,
          ),
        ),
        'resizeDelay' => 10,
      )));?>">


        <?php if (VTCore_Zeus_Utility::isActiveSidebar('footerone')) : ?>
        <div id="footer-one" class="region text-left <?php echo VTCore_Zeus_Utility::getColumnSize('footerone');?>">
          <?php dynamic_sidebar('footerone'); ?>
        </div>
        <?php endif;?>



        <?php if (VTCore_Zeus_Utility::isActiveSidebar('footertwo')) : ?>
        <div id="footer-two" class="region text-center <?php echo VTCore_Zeus_Utility::getColumnSize('footertwo');?>">
          <?php dynamic_sidebar('footertwo'); ?>
        </div>
        <?php endif;?>



        <?php if (VTCore_Zeus_Utility::isActiveSidebar('footerthree')) : ?>
        <div id="footer-three" class="region text-right <?php echo VTCore_Zeus_Utility::getColumnSize('footerthree');?>">
          <?php dynamic_sidebar('footerthree'); ?>
        </div>
        <?php endif;?>

      </div>
    </div>
  </footer>
<?php endif;?>


<?php if (VTCore_Zeus_Init::getFeatures()->get('show.footer.copyright')) : ?>

  <footer id="full-footer" class="area text-center">
    <div class="container-fluid">
      <div class="row">

          <div id="copyright-region" class="region <?php echo VTCore_Zeus_Utility::getColumnSize('copyright');?>">
            <p class="copyright"><?php echo VTCore_Zeus_Utility::getFooter('copyrighttext'); ?></p>
          </div>

      </div>
    </div>
  </footer>
<?php endif;?>

  <div class="ajax-notification alert alert-info">
    <?php echo esc_html__('Loading please wait....', 'medikal'); ?>
  </div>

  <div class="modal"></div>

</div>


<?php wp_footer();?>

</body>
</html>

