<?php
/**
 * Main Header template
 * @author jason.xie@victheme.com
 */

  $class = 'without-nicescroll';
  if (VTCore_Zeus_Init::getFeatures()->get('options.nicescroll.enable')) {
    $class = 'with-nicescroll';
  }
?>
<!DOCTYPE html>

<!--[if IEMobile 7 ]> <html <?php language_attributes(); ?>class="no-js iem7 <?php echo esc_attr($class); ?>"> <![endif]-->
<!--[if lt IE 7 ]>    <html <?php language_attributes(); ?> class="no-js ie6 <?php echo esc_attr($class); ?>"> <![endif]-->
<!--[if IE 7 ]>       <html <?php language_attributes(); ?> class="no-js ie7 <?php echo esc_attr($class); ?>"> <![endif]-->
<!--[if IE 8 ]>       <html <?php language_attributes(); ?> class="no-js ie8 <?php echo esc_attr($class); ?>"> <![endif]-->
<!--[if IE 9 ]>       <html <?php language_attributes(); ?> class="no-js ie9 <?php echo esc_attr($class); ?>"> <![endif]-->
<!--[if (gt IEMobile 7)|!(IEMobile)|!(IE)]><!--><html <?php language_attributes(); ?> class="no-js <?php echo esc_attr($class); ?>"><!--<![endif]-->

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="shortcut icon" href="<?php echo VTCore_Zeus_Utility::getFavicon(); ?>" />


	<!-- media-queries.js (fallback) -->
	<!--[if lt IE 9]>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->

	<!-- html5.js -->
	<!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

  <!-- Remove the no-js quickly if javascript is enabled -->
  <script>
    document.documentElement.className = document.documentElement.className.replace("no-js","js");
    if(document.documentMode) {
      document.documentElement.className+=' ie'+document.documentMode;
    }
  </script>

  <?php

    // Adding extra classes for easier css theming
    $class = array();
    if (VTCore_Zeus_Utility::isActiveSidebar('slider')) {
      $class[] = 'with-slider';
    }
    else {
      $class[] = 'without-slider';
    }

    if (defined('VTCORE_HEADLINE_LOADED') && VTCORE_HEADLINE_LOADED) {
      $class[] = 'with-headline';
    }
    else {
      $class[] = 'without-headline';
    }

    wp_head();
  ?>

</head>
<body <?php body_class('template-' . VTCore_Zeus_Utility::getCustomTemplate() . ' ajax-page-target'); ?>>
<div <?php echo VTCore_Zeus_Utility::buildPageAttributes(implode(' ',$class)); ?>>

  <!-- Header -->
  <header id="header" class="area cleafix header-logo full-width">
    <div class="container-fluid">
      <div class="row">

        <!-- Logo -->
        <div id="logo"
             class="region vertical-center text-center <?php echo VTCore_Zeus_Utility::getColumnSize('logo'); ?>">

          <div class="vertical-target">
            <?php extract(VTCore_Zeus_Utility::getLogo()); ?>
            <?php if (!empty($image)) : ?>
              <a class="home-link" href="<?php echo esc_url(home_url('/')); ?>"
                 title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>"
                 rel="home">

                <img class="main-logo" src="<?php echo esc_url($image); ?>"
                     alt="<?php echo esc_attr(__('Logo for ', 'medikal') . get_bloginfo('name', 'display')); ?>"
                  <?php if (!empty($width)) : ?> width="<?php echo esc_attr($width); ?>"<?php endif; ?>
                  <?php if (!empty($height)) : ?> height="<?php echo esc_attr($height); ?>"<?php endif; ?>
                  />

              </a>
            <?php endif ?>

            <?php if (!empty($text)) : ?>
              <div class="branding">
                <h1 class="site-title">

                  <a class="home-link"
                     href="<?php echo esc_url(home_url('/')); ?>"
                     title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>"
                     rel="home"><?php echo wp_kses_post($text); ?></a>

                </h1>
                <?php if (!empty($slogan)) : ?>
                  <div class="site-description">
                    <a class="home-link"
                       href="<?php echo esc_url(home_url('/')); ?>"
                       title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>"
                       rel="home">

                      <?php echo wp_kses_post($slogan); ?>

                    </a>
                  </div>
                <?php endif; ?>
              </div>
            <?php endif; ?>
          </div>
        </div>

        <!-- Menu -->
        <nav id="navigation"
             class="region navigation dropdown-navigation text-center <?php echo VTCore_Zeus_Utility::getColumnSize('navigation'); ?>">
          <?php

          // Trigger slicknav support
          $slicknav = '';
          if (VTCore_Zeus_Init::getFeatures()->get('options.menu.enable_slick')) {
            $slicknav = 'slicknav';
          }

          wp_nav_menu(array(
            'theme_location' => 'navigation',
            'container' => 'div',
            'container_class' => 'menu',
            'menu_class' => 'nav nav-pills ' . $slicknav,
            'fallback_cb' => 'VTCore_Zeus_Navwalker_Dropdown::fallback',
            'walker' => new VTCore_Zeus_Navwalker_Dropdown()
          ));
          ?>
        </nav>
      </div>
    </div>
  </header>

  <?php

  // Build the slider dynamic content
  get_sidebar('slider');

  // Build the headline on position top
  if (defined('VTCORE_HEADLINE_LOADED') && VTCORE_HEADLINE_LOADED) {
    get_template_part('headline');
  }

  ?>
