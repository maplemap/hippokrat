<?php
/**
 * Sidebar template for sidebar positioned on right or left side.
 * @author jason.xie@victheme.com
 */
?>
<?php if (VTCore_Zeus_Utility::isActiveSidebar('sidebar')) : ?>
  <aside id="sidebar" class="region <?php echo VTCore_Zeus_Utility::getColumnSize('sidebar');?>">
    <?php dynamic_sidebar('sidebar'); ?>
  </aside>
<?php endif;?>