<?php
/**
 * Single Page For Post Post Type Templates
 *
 * @author jason.xie@victheme.com
 */
?>
<?php get_header(); ?>

  <div id="maincontent" class="area clearfix">
    <div class="container-fluid">
      <div class="row">

        <div id="content"
             class="region pseudo-background
			     <?php if (VTCore_Zeus_Utility::getSidebar('full')) echo VTCore_Zeus_Utility::getColumnSize('content'); ?>
			     <?php echo 'with-sidebar-' . VTCore_Zeus_Utility::getSidebar('full'); ?>"
          >

          <?php

          while (have_posts()) {

            // Build the single post entry using main loop
            the_post();
            $type = (explode('/',$post->post_mime_type));
            $type = array_shift($type);

            get_template_part('templates/attachment/' . $type);

          }
          ?>

        </div>

        <?php
        // Build sidebar.
        if (VTCore_Zeus_Utility::getSidebar('full') == 'right'
          || VTCore_Zeus_Utility::getSidebar('full') == 'left'
        ) {
          get_sidebar('sidebar');
        }
        ?>

      </div>
    </div>
  </div>

<?php get_footer(); ?>