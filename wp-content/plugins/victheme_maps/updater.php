<?php
/**
 * Updating Maps
 *
 * @see VTCore_Wordpress_Factory_Updater
 * Class VTCore_Wordpress_Models_Updater
 */
class VTCore_Maps_Updater
extends VTCore_Wordpress_Models_Updater {

  protected function update_1_6_6() {
    // Force clear cache because we remove the maps init hook class
    update_option('vtcore_clear_cache', true);
    return true;
  }
}