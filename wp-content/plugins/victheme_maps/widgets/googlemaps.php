<?php
/**
 * Main Class for building GoogleMaps widgets
 * extending WP_Widget class.
 *
 * @author jason.xie@victheme.com
 *
 * @see VTCore_Maps_Init();
 */
class VTCore_Maps_Widgets_GoogleMaps
extends WP_Widget {


  private $defaults = array(
    'title' => '',
  );

  private $instance;
  private $args;



  /**
   * Registering widget as WP_Widget requirement
   */
  public function __construct() {
    parent::__construct(
      'vtcore_maps_widgets_googlemaps',
      'Google Maps',
      array('description' => 'Displaying Maps using Google Maps Service')
    );
  }




  /**
   * Registering widget
   */
  public function registerWidget() {
    return register_widget('vtcore_maps_widgets_googlemaps');
  }





  /**
   * Extending widget
   *
   * @see WP_Widget::widget()
   */
  public function widget($args, $instance) {

    $this->args = $args;
    $this->instance = VTCore_Utility::arrayMergeRecursiveDistinct($instance, $this->defaults);

    echo $args['before_widget'];

    $title = apply_filters( 'widget_title', $this->instance['title'] );
    if (!empty($title)) {
      echo $this->args['before_title'] . $title . $this->args['after_title'];
    }

    $marker = new VTCore_Maps_Element_Google_Maps($this->instance);
    $marker->render();

    echo $args['after_widget'];
  }





  /**
   * Widget configuration form
   * @see WP_Widget::form()
   */
  public function form($instance) {

    VTCore_Wordpress_Utility::loadAsset('maps-widgets');
    VTCore_Wordpress_Utility::loadAsset('wp-bootstrap');

    $this->instance = VTCore_Utility::arrayMergeRecursiveDistinct($instance, $this->defaults);

    $this
      ->buildForm()
      ->processForm()
      ->processError(true)
      ->render();

  }




  /**
   * Function for building the form object
   */
  private function buildForm() {

    $form = new VTCore_Bootstrap_Form_BsInstance(array(
      'type' => '',
    ));

    $this->instance['name'] = str_replace('[]', '', $this->get_field_name(''));

    $form
      ->BsText(array(
        'name' => $this->get_field_name('title'),
        'text' => __('Title', 'victheme_maps'),
        'value' => $this->instance['title'],
      ))
      ->addChildren(new VTCore_Maps_Form_Google_Maps($this->instance));

    return $form;
  }



  /**
   * Widget update function.
   * @see WP_Widget::update()
   */
  public function update($new_instance, $old_instance) {

    $form = $this->buildForm()->processForm()->processError();
    $errors = $form->getErrors();

    if (empty($errors)) {

      // Clean out potential empty markers
      foreach ($new_instance['markers'] as $key => $markers) {
        $new_instance['markers'][$key]['data'] = array_filter($markers['data']);
        $new_instance['markers'][$key] = array_filter($new_instance['markers'][$key]);
      }

      $new_instance['markers'] = array_filter($new_instance['markers']);

      return $new_instance;
    }

    return false;
  }
}