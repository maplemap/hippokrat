<?php
/**
 * Main class for booting up the plugin
 * functionality and overloading some
 * of usefull function that can be called
 * later.
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Maps_Init {

  private $autoloader;
  private $template;
  private $content;

  private $actions = array(
    'widgets_init',
    'admin_menu',
    'vc_backend_editor_enqueue_js_css',
    'vc_frontend_editor_enqueue_js_css',
    'vc_load_iframe_jscss',
  );

  private $filters = array(
    'vtcore_register_shortcode',
    'vtcore_register_shortcode_prefix',
    'vtcore_wordpress_ajax_processor',
  );


  private $shortcodes = array(
    'VTCore_Maps_Addon_VisualComposer_Shortcodes_Google_Maps',
    'VTCore_Maps_Addon_VisualComposer_Shortcodes_Google_Marker',
  );


  public function __construct() {
    $this->autoloader = new VTCore_Autoloader('VTCore_Maps', dirname(__FILE__));
    $this->autoloader->setRemovePath('vtcore' . DIRECTORY_SEPARATOR . 'maps' . DIRECTORY_SEPARATOR);
    $this->autoloader->register();

    // Autoload translation for vtcore
    load_plugin_textdomain('victheme_maps', false, 'victheme_maps/languages');

    // Registering assets
    VTCore_Wordpress_Init::getFactory('assets')->get('library')->detect(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets', plugins_url('', __FILE__) . '/assets');

    // Registering config
    $this->config = new VTCore_Maps_Config();

    // Registering Filters
    VTCore_Wordpress_Init::getFactory('filters')
      ->addPrefix('VTCore_Maps_Filters_')
      ->addHooks($this->filters)
      ->register();


    // Registering Actions
    VTCore_Wordpress_Init::getFactory('actions')
      ->addPrefix('VTCore_Maps_Actions_')
      ->addHooks($this->actions)
      ->register();


    // Hooking to WPML
    if (defined('ICL_SITEPRESS_VERSION')) {
      VTCore_Wordpress_Init::getFactory('wpml')->add('vtcore_maps_config', array(
        'type' => 'plugin',
        'atid' => 'victheme_maps',
        'map' => array(
          'google' => array(
            'language' => 'en',
          ),
        ),
      ));
    }

    // Register to visual composer via VTCore Visual Composer Factory
    if (VTCore_Wordpress_Init::getFactory('visualcomposer')) {
      VTCore_Wordpress_Init::getFactory('visualcomposer')
        ->mapShortcode($this->shortcodes);
    }

  }

}