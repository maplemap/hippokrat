<?php
/**
 * Registering maps shortcode to wordpress
 * shortcodes via VTCore_Wordpress_Shortcode
 * using vtcore_register_shortcode filter
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Maps_Filters_VTCore__Register__Shortcode
extends VTCore_Wordpress_Models_Hook {

  public function hook($shortcodes = NULL) {
    $shortcodes[] = 'google_maps';
    $shortcodes[] = 'google_marker';

    return $shortcodes;
  }
}