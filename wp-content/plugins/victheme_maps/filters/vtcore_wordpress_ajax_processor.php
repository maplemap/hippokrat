<?php
/**
 * Registering maps shortcode to wordpress
 * shortcodes via VTCore_Wordpress_Shortcode
 * using vtcore_register_shortcode filter
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Maps_Filters_VTCore__Wordpress__Ajax__Processor
extends VTCore_Wordpress_Models_Hook {

  public function hook($processors = NULL) {
    $processors['googlemaps'] = 'VTCore_Maps_Ajax_Google';
    return $processors;
  }
}