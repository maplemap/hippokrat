<?php
/**
 * Class extending the Shortcodes base class
 * for building the google maps marker.
 *
 *
 * [google_marker
 *   center="true|false"
 *   address="string valid address for geocoding"
 *   latitude="fallback latitude if geocoding failed"
 *   longitude="fallback longitude if geocoding failed"
 *   title="the hover title for the icon"
 *   icon="the full url for the icon image"
 * ]
 *
 * some content for the popup content
 *
 * [/google_marker]

 *
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Maps_Shortcodes_Google_Marker
extends VTCore_Wordpress_Models_Shortcodes
implements VTCore_Wordpress_Interfaces_Shortcodes {

  protected $data = array(
    'center' => false,
    'address' => '',
    'latitude' => '',
    'longitude' => '',
    'title' => '',
    'html' => '',
    'icon' => '',
    'name' => '',
  );


  public function buildObject() {

    $this->object = new VTCore_Maps_Element_Google_Marker($this->atts);
    $this->object->addChildren(do_shortcode($this->content));

  }
}