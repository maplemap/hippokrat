<?php
/**
 * Base for building VTCore_VisualCandy_Element elements
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Maps_Element_Base
extends VTCore_Html_Base {

  protected $overloaderPrefix = array(
    'VTCore_Maps_Element_',
    'VTCore_Wordpress_Element_',
    'VTCore_Html_',
  );

  /**
   * Trigger for force data attribute in lower key
   * as HTML5 specification
   */
  protected $dataConvertLowerKey = false;


  protected $booleans = array();
  protected $int = array();

  /**
   * Overriding parent
   * @param array $context
   */
  public function buildObject($context) {

    // Convert to booleans
    if (isset($context['data']) && isset($this->booleans)) {
      foreach ($this->booleans as $key) {
        if (isset($context['data'][$key])) {
          $context['data'][$key] = filter_var($context['data'][$key], FILTER_VALIDATE_BOOLEAN);
        }
      }
    }

    // Convert to int number
    if (isset($context['data']) && isset($this->int)) {
      foreach ($this->int as $key) {
        if (isset($context['data'][$key])) {
          $context['data'][$key] = (int) $context['data'][$key];
        }
      }
    }

    // Populate $this->context
    parent::buildObject($context);
  }


  /**
   * Overriding base because some jQuery scripts
   * need special treatment regarding the data-*
   * attributes
   *
   * @see VTCore_Html_Base::buildData()
   */
  protected function buildData() {

    foreach ($this->getContext('data') as $key => $value) {

      // HTML5 specs need all lowercase key
      if ($this->dataConvertLowerKey) {
        $key = strtolower($key);
      }

      // HTML5 specs needs string "true" or "false" for
      // true booleans
      if (in_array($key,$this->booleans)) {

        if ($value === true) {
          $value = 'true';
        }

        if ($value === false) {
          $value = 'false';
        }
      }

      // HTML5 can only store string
      // Convert to json so jQuery can pick it up easily.
      if (is_array($value) || is_object($value)) {
        $value = json_encode($value);
      }


      // Dont build really "empty" value
      if (empty($value)) {
        continue;
      }

      $this->addAttribute('data-' . $key, $value);

    }

    return $this;
  }
}