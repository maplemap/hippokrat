<?php
/**
 * Class for building google maps marker instance.
 *
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Maps_Element_Google_Marker
extends VTCore_Maps_Element_Base {

  protected $context = array(
    'type' => 'span',
    'attributes' => array(
      'class' => array(
        'map-markers',
        'hidden',
      ),
    ),
    'data' => array(
      'address' => '',
      'icon' => '',
      'title' => '',
      'html' => '',
      'latitude' => '',
      'longitude' => '',
    ),

    'raw' => true,
  );


  /**
   * Overriding parent method to preprocess data
   * @see VTCore_jQuery_Element_Base::buildObject()
   */
  public function buildObject($context) {

    // Allow wordpress shortcodes.
    if (isset($context['data']['html'])) {
      $context['content'] = do_shortcode($context['data']['html']);
      unset($context['data']['html']);
    }

    $unique = new VTCore_Uid();
    $context['data']['id'] = 'marker-' . $unique->getID();

    if (isset($context['data']['icon']) && is_numeric($context['data']['icon'] )) {
      list($src, $width, $height) = wp_get_attachment_image_src($context['data']['icon'] );
      $context['data']['icon'] = $src;
    }

    parent::buildObject($context);
  }


  public function buildElement() {
    $this->addAttributes($this->getContext('attributes'));
    $this->setText(do_shortcode($this->getContext('content')));
  }
}