<?php
/**
 * Class for building google maps instance.
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Maps_Element_Google_Maps
extends VTCore_Maps_Element_Base {

  protected $context = array(
    'type' => 'div',
    'attributes' => array(
      'class' => array('googlemaps-wrapper'),
    ),
    'data' => array(
      'address' => '',
      'longitude' => '',
      'latitude' => '',
      'width' => 300,
      'height' => 300,
      'responsive' => true,
      'zoom' => 10,
      'zoom_control' => false,
      'zoom_control_position' => 'TOP_RIGHT',
      'zoom_control_style' =>  'DEFAULT',
      'pan_control' => false,
      'pan_control_position' => 'TOP_LEFT',
      'map_type_control' => false,
      'map_type_control_position' => 'TOP_CENTER',
      'map_type_control_style' => 'DROPDOWN_MENU',
      'scale_control' => false,
      'scrollwheel' => false,
      'street_view' => false,
      'maptype' => 'HYBRID',
      'draggable' => false,
      'disable_zoom' => false,
      'title' => '',
      'html' => '',
      'fitbounds' => false,
      'one_info_window' => false,
      'hide_by_click' => true,
      'start_open_info' => true,
      'enable_infowindow' => true,
      'info_maxwidth' => 640,
      'icon' => 'http://www.google.com/intl/en_us/mapfiles/ms/micons/red-dot.png',
      'styles' => false,
    ),
    'map_element' => array(
      'type' => 'div',
      'attributes' => array(
        'class' => array('googlemaps-instances'),
      ),
    ),
    'marker_element' => array(
      'type' => 'div',
      'attributes' => array(
        'class' => array('googlemaps-markers', 'hidden'),
      ),
    ),
    'markers' => array(),
    'processMarker' => true,

    // Supports wp-ajax
    'ajax' => false,
    'ajaxData' => array(
      'ajax-mode' => 'selfData',
      'ajax-object' => 'googlemaps',
      'ajax-loading-text' => 'Loading...',
      'ajax-type' => 'google-maps',
      'ajax-target' => false,
      'ajax-action' => 'vtcore_ajax_framework',
      'ajax-queue' => array(
        'replace',
      ),
    ),
  );


  protected $markers = array();


  /**
   * This is needed to play nice
   * with HTML5 data-* attributes
   */
  protected $booleans = array(
    'responsive',
    'zoom_control',
    'pan_control',
    'map_type_control',
    'scale_control',
    'scrollwheel',
    'street_view',
    'draggable',
    'disable_zoom',
    'one_info_window',
    'hide_by_click',
    'start_open_info',
    'enable_infowindow',
  );



  public function buildElement() {

    $config = new VTCore_Maps_Loader_Google_Api();
    VTCore_Wordpress_Utility::loadAsset('jquery-resizeend');
    VTCore_Wordpress_Utility::loadAsset('maps-controller', array('jquery'));
    wp_localize_script('maps-controller-maps-controller-ng.js', 'vtcore_maps', array('url' => $config->buildMapUrl()));

    if (substr($this->getData('width'), -2) != 'px') {
      $this->addData('width', $this->getData('width') . 'px');
    }

    if (substr($this->getData('height'), -2) != 'px') {
      $this->addData('height', $this->getData('height') . 'px');
    }

    if (!$this->getContext('data.styles')) {
      $this->removeContext('data.styles');
    }

    $this
      ->addAttributes($this->getContext('attributes'))
      ->addChildren(new VTCore_Html_Element($this->getContext('map_element')))
      ->lastChild()
      ->addStyle('width', $this->getData('width'))
      ->addStyle('height', $this->getData('height'));

    $this->markers = $this->addChildren(new VTCore_Html_Element($this->getContext('marker_element')))->lastChild();
    $this->setChildrenPointer('markers');


    // Add dummy marker for the default location if no marker defined
    if ($this->getContext('markers') == array() && $this->getContext('processMarker')) {
      $this->context['markers'][] = array(
        'data' => array(
          'center' => true,
          'address' => $this->getData('address'),
          'latitude' => $this->getData('longitude'),
          'longitude' => $this->getData('latitude'),
          'title' => $this->getData('title'),
          'html' => $this->getData('html'),
          'icon' => $this->getContext('data.icon'),
        ),
      );
    }

    if ($this->getContext('processMarker')) {
      foreach ($this->getContext('markers') as $marker) {
        if (!empty($marker['data']['address']) || (!empty($marker['data']['latitude']) && !empty($marker['data']['longitude']))) {
          if (empty($marker['data']['icon'])) {
            $marker['data']['icon'] = $this->getContext('data.icon');
          }
          $this->addChildren(new VTCore_Maps_Element_Google_Marker($marker));
        }
      }
    }

    // Supports for ajax mode
    if ($this->getContext('ajax')) {

      VTCore_Wordpress_Utility::loadAsset('wp-ajax');
      $this
        ->addContext('data.ajax-target', uniqid('vtcore-maps-'))
        ->addContext('data.nonce', wp_create_nonce('vtcore-ajax-nonce-admin'))
        ->addClass('btn-ajax-content');

      foreach ($this->getContext('ajaxData') as $key => $data) {
        if (!empty($data)) {
          $this->addContext('data.' . $key, $data);
        }
      }
    }

  }

}