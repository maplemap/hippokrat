/**
 * 
 * Integrating to VisualComposer
 * 
 * @author jason.xie@victheme.com
 */
(function ($) {

  $(window)
    .on('vc_ready', function() {

      // Maps Container
      window.InlineShortcodeView_google_maps = window.InlineShortcodeViewContainer.extend({

        events: {
          'click > .vc_controls .vc_element .vc_control-btn-delete': 'destroy',
          'click > .vc_controls .vc_element .vc_control-btn-edit': 'edit',
          'click > .vc_controls .vc_element .vc_control-btn-clone': 'clone',
          'click > .vc_controls .vc_element .vc_control-btn-prepend': 'prependElement',
          'click > .vc_controls .vc_element .vc_control-btn-clear': 'clearMarker'
        },
        //hold_active: true,
        addControls: function() {
          var template = $(this.controls_selector).html(),
            parent = vc.shortcodes.get(this.model.get('parent_id')),
            data = {
              name: vc.getMapped(this.model.get('shortcode')).name,
              tag: this.model.get('shortcode'),
              parent_name: vc.getMapped(parent.get('shortcode')).name,
              parent_tag: parent.get('shortcode')
            };


          this.$controls = $(_.template(template, data, vc.template_options).trim()).addClass('vc_controls');
          this.$controls.find('.vc_controls-bc').remove();
          this.$controls.find('.element-google_maps').append('<a class="vc_control-btn vc_control-btn-clear" href="#" title="Clear Map Markers"><span class="vc_btn-content"><span class="icon"></span></span></a>');

          if (this.$el.find('.element-google_maps').length == 0) {
            this.$controls.appendTo(this.$el);
          }

        },
        clearMarker: function(e) {
          e.preventDefault();
          this.$el.find('.googlemaps-markers').find('.vc_control-btn-delete').trigger('click');
          this.changed();
        },
        render: function() {

          if ($.isFunction($.fn.VTCoreBuildGoMap) !== false) {
            this.$el.find('.googlemaps-instances').VTCoreBuildGoMap();
          }

          window.InlineShortcodeViewContainer.__super__.render.call(this);
          this.content().addClass('vc_element-container');
          this.$el.addClass('vc_container');
          return this;
        },
        changed: function() {
          if ($.isFunction($.fn.VTCoreUpdateMaps) !== false) {
            this.$el.find('.googlemaps-instances').VTCoreUpdateMaps().VTCoreMapMarkerAssignClick();
          }

          // Toggle class
          (this.$el.find('.vc_google_marker').length == 0)
          || this.$el.removeClass('vc_empty').find('> .vc_empty-element').removeClass('vc_empty-element');
        }
      });


      // Maps Markers
      window.InlineShortcodeView_google_marker = window.InlineShortcodeView.extend({
        events: {
          'click > .vc_controls .vc_control-btn-delete': 'destroy',
          'click > .vc_controls .vc_control-btn-edit': 'edit',
          'click > .vc_controls .vc_control-btn-clone': 'clone',
          'click > .vc_controls .vc_control-btn-go': 'move'
        },
        addControls: function() {
          var template = $('#vc_controls-template-' + this.model.get('shortcode')).length ? $('#vc_controls-template-' + this.model.get('shortcode')).html() : this._getDefaultTemplate(),
            parent = vc.shortcodes.get(this.model.get('parent_id')),
            data = {
              name: vc.getMapped(this.model.get('shortcode')).name,
              tag: this.model.get('shortcode'),
              parent_name: parent ? vc.getMapped(parent.get('shortcode')).name : '',
              parent_tag: parent ? parent.get('shortcode') : ''
            };
          this.$controls = $(_.template(template, data, vc.template_options).trim()).addClass('vc_controls');
          this.$controls.find('.vc_control-btn-clone').remove();
          this.$controls.find('.vc_element-move').remove();
          this.$controls.find('.vc_controls-cc').prepend('<a class="vc_control-btn vc_control-btn-go" href="#" title="Go to markers"><span class="vc-btn-content"><span class="icon"></span></span></a>');
          this.$controls.appendTo(this.$el);
          this.$controls_buttons = this.$controls.find('> :first');
        },
        move: function(e) {
          e.preventDefault();
          this.$el.find('.map-markers').trigger('click');
        },
        render: function() {
          window.InlineShortcodeView_google_marker.__super__.render.call(this);
          return this;
        },
        changed: function(){
          vc.builder.notifyParent(this.model.get('parent_id'));
        }
      });
    })
    .on('vtcore-maps-ready', function() {
      // Booting uninitialized maps
      $('.googlemaps-instances:not(.vtcore-maps-initialized)').each(function() {
        $(this).addClass('vtcore-maps-initialized').VTCoreBuildGoMap();
      });
    });

 })(window.jQuery);
