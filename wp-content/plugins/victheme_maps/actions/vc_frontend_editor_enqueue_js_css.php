<?php
/**
 * Hooking into Visual Composer Front Editor
 * for loading admin script
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Maps_Actions_Vc__Frontend__Editor__Enqueue__Js__Css
  extends VTCore_Wordpress_Models_Hook {

  public function hook() {
    $config = new VTCore_Maps_Loader_Google_Api();

    VTCore_Wordpress_Utility::loadAsset('jquery-resizeend');
    VTCore_Wordpress_Utility::loadAsset('maps-controller', array('jquery'));
    wp_localize_script('maps-controller-maps-controller-ng.js', 'vtcore_maps', array('url' => $config->buildMapUrl()));
    VTCore_Wordpress_Utility::loadAsset('maps-visualcomposer');
  }
}