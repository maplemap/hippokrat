<?php
/**
 * Hooking into wordpress admin_menu action for
 * registering maps configuration page.
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Maps_Actions_Admin__Menu
extends VTCore_Wordpress_Models_Hook {

  public function hook() {
    add_menu_page(
      'Maps',
      'Maps',
      'manage_options',
      'vtcoremaps',
       array(new VTCore_Maps_Admin_Factory,
        'buildPage'),
      'dashicons-format-image');
  }
}