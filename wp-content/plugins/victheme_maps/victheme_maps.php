<?php
/*
Plugin Name: VicTheme Maps
Plugin URI: http://victheme.com
Description: Plugin for providing a maps
Author: jason.xie@victheme.com
Version: 1.7.0
Author URI: http://victheme.com
*/

define('VTCORE_MAPS_CORE_VERSION', '1.7.0');

add_action('plugins_loaded', 'bootVicthemeMaps', 11);

function bootVicthemeMaps() {

  if (!(defined('VTCORE_VERSION') && version_compare(VTCORE_VERSION, VTCORE_MAPS_CORE_VERSION, '='))) {

    add_action('admin_notices', 'MapsMissingCoreNotice');


    function MapsMissingCoreNotice() {

      if (!defined('VTCORE_VERSION')) {
        $notice = __('VicTheme Maps depends on VicTheme Core Plugin which is not activated or missing, Please enable it first before VicTheme Maps can work properly.');
      }
      elseif (version_compare(VTCORE_VERSION, VTCORE_MAPS_CORE_VERSION, '!=')) {
        $notice = sprintf(__('VicTheme Maps depends on VicTheme Core Plugin API version %s to operate properly.', 'victheme_maps'), VTCORE_MAPS_CORE_VERSION);
      }

      if (isset($notice)) {
        echo '<div class="error""><p>' . $notice . '</p></div>';
      }

    }

    return;
  }

  define('VTCORE_MAPS_LOADED', true);
  define('VTCORE_MAPS_BASE_DIR', dirname(__FILE__));

  include_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'init.php');
  new VTCore_Maps_Init();
}