<?php
/**
 * Ajax processor for google maps element
 * This class can :
 *
 * 1. Wipe all the markers
 * 2. Append the markers
 * 3. Prepend the markers
 * 4. Replace all of the markers
 *
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Maps_Ajax_Google
extends VTCore_Wordpress_Models_Ajax {

  protected $render = array();
  protected $post;


  protected $markers;
  protected $target;


  /**
   * Ajax callback function
   */
  protected function processAjax() {

    // Build the dummy container
    $this->markers = new VTCore_Html_Element();

    // Setup the correct html target for ajax results
    $this->target = '[data-ajax-target="' . $this->post['target'] . '"] > .googlemaps-markers';

    // Other plugins must add the markers
    // and implement their own content fetching logic
    do_action('vtcore_maps_ajax_markers', $this);

    switch ($this->post['queue']) {

      // Remove all the markers
      case 'wipe' :
        $this->render['action'][] = array(
          'mode' => 'empty',
          'target' => $this->target,
        );

        break;

      // Append the markers
      case 'append' :
        $this->render['action'][] = array(
        'mode' => 'append',
        'target' => $this->target,
        'content' => $this->markers->__toString(),
        );

        break;

      // Prepend the markers
      case 'prepend' :
        $this->render['action'][] = array(
          'mode' => 'prepend',
          'target' => $this->target,
          'content' => $this->markers->__toString(),
        );

        break;

      // Replace the whole markers
      case 'replace' :
        $this->render['action'][] = array(
          'mode' => 'html',
          'target' =>  $this->target,
          'content' => $this->markers->__toString(),
        );

      break;

    }

    // Force element to update maps
    $this->render['action'][] = array(
      'mode' => 'callback',
      'content' => 'jQuery(\'[data-ajax-target="' . $this->post['target'] . '"]\').data("ajax-stop", false);
                    jQuery(\'[data-ajax-target="' . $this->post['target'] . '"]\').find(".googlemaps-instances").VTCoreUpdateMaps();',
    );

    return $this->render;
  }

}