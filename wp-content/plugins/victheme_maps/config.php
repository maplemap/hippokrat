<?php
/**
 * Main Class for handling Maps default
 * values and or inject the value from the
 * database
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Maps_Config
extends VTCore_Wordpress_Models_Config {

  protected function register(array $options) {

    // Define the database name
    $this->database = 'vtcore_maps_config';

    // Set the default options
    $this->options = array(
      'google' => array(
        'language' => 'en',
        'sensor' => 'false',
        'v' => 3,
        'async' => false,
        'key' => '',
        'http' => 'http',
      ),
    );

    // Set the hookable filter name
    $this->filter = 'vtcore_maps_config_alter';

    // Apply the hookable filter
    $this->filter();

    // Inject from database
    $this->load();

    // Merge the user supplied options
    $this->merge($options);

    return $this;
  }
}