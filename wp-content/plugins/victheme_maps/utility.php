<?php
/**
 * Utility singleton class
 * This class is for all the function that can be
 * used outside any of the maps classes hiearchy
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Maps_Utility
extends VTCore_Utility {

}