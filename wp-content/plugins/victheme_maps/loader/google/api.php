<?php
/**
 * Class extending VTCore_Maps_Config for loading
 * Google Maps API
 *
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Maps_Loader_Google_Api
extends VTCore_Maps_Config {

  /**
   * Building the url for loading Google maps api url
   * @return string
   */
  public function buildMapUrl() {

    $options = $this->get('google');
    $query = http_build_query(array_filter($options));

    return $this->get('google.http') . '://maps.googleapis.com/maps/api/js?' . $query;
  }

}