<?php
/**
 * Class for building the Maps Grid configuration panel
 *
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Maps_Admin_Panels_Google
extends VTCore_Bootstrap_Element_BsPanel {

  private $config;
  private $description;
  private $languages;


  /**
   * Overriding parent method
   * @see VTCore_Bootstrap_Element_BsPanel::buildElement()
   */
  public function buildElement() {

    $this->context['heading'] = true;
    $this->context['text'] = __('Google Maps API Configuration', 'victheme_maps');
    $this->config = new VTCore_Maps_Loader_Google_Api();
    $this->LanguageOptions();

    parent::buildElement();

    $this
      ->setChildrenPointer('content')
      ->BsSelect(array(
        'text' => __('Connection Protocol', 'victheme_maps'),
        'description' => __('Select the internet protocol to use when connecting to Google API', 'victheme_maps'),
        'name' => 'maps[google][http]',
        'value' => $this->config->get('google.http'),
        'options' => array(
          'http' => 'HTTP',
          'https' => 'HTTPS',
        ),
      ))
      ->BsSelect(array(
        'text' => __('Languages', 'victheme_maps'),
        'description' => __('Set what language the maps should display', 'victheme_maps'),
        'name' => 'maps[google][language]',
        'value' => $this->config->get('google.language'),
        'options' => $this->languages,
      ))
      ->BsSelect(array(
        'text' => __('Sensor', 'victheme_maps'),
        'description' => __('Enable or disable sensor', 'victheme_maps'),
        'name' => 'maps[google][sensor]',
        'value' => $this->config->get('google.sensor'),
        'options' => array(
          'true' => __('True', 'victheme_maps'),
          'false' => __('False', 'victheme_maps'),
        ),
      ))
      ->BsSelect(array(
        'text' => __('API version', 'victheme_maps'),
        'description' => __('Set The API Version', 'victheme_maps'),
        'name' => 'maps[google][v]',
        'value' => $this->config->get('google.v'),
        'options' => array(
          '3' => __('Google Maps API 3', 'victheme_maps'),
        ),
      ))
      ->BsText(array(
        'text' => __('API Key', 'victheme_maps'),
        'description' => __('Set The API key for business level map use or leave empty for limited usage', 'victheme_maps'),
        'name' => 'maps[google][key]',
        'value' => $this->config->get('google.key'),
      ));

  }




  /**
   * Providing array suitable for select element
   * for all google maps supported languages.
   */
  private function LanguageOptions() {
    $this->languages = array(
      'ar' => __('Arabic', 'victheme_maps'),
      'bg' => __('Bulgarian', 'victheme_maps'),
      'bn' => __('Bengali', 'victheme_maps'),
      'ca' => __('Catalan', 'victheme_maps'),
      'cs' => __('Czech', 'victheme_maps'),
      'da' => __('Danish', 'victheme_maps'),
      'de' => __('German', 'victheme_maps'),
      'el' => __('Greek', 'victheme_maps'),
      'en' => __('English', 'victheme_maps'),
      'en-AU' => __('English (Australian)', 'victheme_maps'),
      'en-GB' => __('English (Great Britain)', 'victheme_maps'),
      'es' => __('Spanish', 'victheme_maps'),
      'eu' => __('Basque', 'victheme_maps'),
      'fa' => __('Farsi', 'victheme_maps'),
      'fi' => __('Finnish', 'victheme_maps'),
      'fil' => __('Filipino', 'victheme_maps'),
      'fr' => __('French', 'victheme_maps'),
      'gl' => __('Galician', 'victheme_maps'),
      'gu' => __('Gujarati', 'victheme_maps'),
      'hi' => __('Hindi', 'victheme_maps'),
      'hr' => __('Croatian', 'victheme_maps'),
      'hu' => __('Hungarian', 'victheme_maps'),
      'id' => __('Indonesian', 'victheme_maps'),
      'it' => __('Italian', 'victheme_maps'),
      'iw' => __('Hebrew', 'victheme_maps'),
      'ja' => __('Japanese', 'victheme_maps'),
      'kn' => __('Kannada', 'victheme_maps'),
      'ko' => __('Korean', 'victheme_maps'),
      'lt' => __('Lithuanian', 'victheme_maps'),
      'lv' => __('Latvian', 'victheme_maps'),
      'ml' => __('Malayalam', 'victheme_maps'),
      'mr' => __('Marathi', 'victheme_maps'),
      'nl' => __('Dutch', 'victheme_maps'),
      'nn' => __('Norwegian Nynorsk', 'victheme_maps'),
      'no' => __('Norwegian', 'victheme_maps'),
      'or' => __('Oriya', 'victheme_maps'),
      'pl' => __('Polish', 'victheme_maps'),
      'pt' => __('Portuguese', 'victheme_maps'),
      'pt-BR' => __('Portuguese (Brazil)', 'victheme_maps'),
      'pt-PT' => __('Portuguese (Portugal)', 'victheme_maps'),
      'rm' => __('Romansch', 'victheme_maps'),
      'ro' => __('Romanian', 'victheme_maps'),
      'ru' => __('Russian', 'victheme_maps'),
      'sk' => __('Slovak', 'victheme_maps'),
      'sl' => __('Slovenian', 'victheme_maps'),
      'sr' => __('Serbian', 'victheme_maps'),
      'sv' => __('Swedish', 'victheme_maps'),
      'tl' => __('Tagalog', 'victheme_maps'),
      'ta' => __('Tamil', 'victheme_maps'),
      'te' => __('Tegulu', 'victheme_maps'),
      'th' => __('Thai', 'victheme_maps'),
      'uk' => __('Ukrainian', 'victheme_maps'),
      'vi' => __('Vietnamese', 'victheme_maps'),
      'zh-CN' => __('Chinese (Simplified)', 'victheme_maps'),
      'zh-TW' => __('Chinese (Traditional)', 'victheme_maps'),
    );

    return $this;
  }

}