<?php
/**
 * Class for handling Maps administration pages
 *
 * The Form relies on VTCore Form classes for building
 * the value based on $_POST or default value from
 * database.
 *
 * The stored data is under vtcore_maps_config using
 * WordPress options table.
 *
 * It is possible for other plugin to do final alter
 * using action vtcore_maps_form_alter hook
 * and transverse the HTML form object to add or
 * remove form object.
 *
 * @author jason.xie@victheme.com
 * @wpaction vtcore_maps_form_alter
 */
class VTCore_Maps_Admin_Factory {

  private $form;
  private $header;
  private $messages;

  private $mapTypes = array(
    'google' => 'VTCore_Maps_Admin_Panels_Google',
  );


  /**
   * Page callbacks
   * @see add_submenu_page()
   * @see VTCore_Maps_Actions_Admin__Init
   */
  public function buildPage() {

    $this->messages = new VTCore_Bootstrap_BsMessages();

    wp_deregister_script('heartbeat');

    VTCore_Wordpress_Utility::loadAsset('wp-bootstrap');
    VTCore_Wordpress_Utility::loadAsset('bootstrap-confirmation');
    VTCore_Wordpress_Utility::loadAsset('maps-admin');

    if (isset($_POST['mapsResetSubmit'])) {
      $this->messages->setNotice(__('Configuration deleted from database and retrieving the default configuration value.', 'victheme_maps'));
      delete_option('vtcore_maps_config');
    }

    $this
      ->buildHeader()
      ->buildForm();

    if (!isset($_POST['mapsResetSubmit'])) {
      $this->form
        ->processForm()
        ->processError(true, true);
    }

    $errors = $this->form->getErrors();

    if (empty($errors) && isset($_POST['maps']) && isset($_POST['mapsSaveSubmit'])) {
      $this->messages->setNotice(__('Configuration saved to database', 'victheme_maps'));
      update_option('vtcore_maps_config', wp_unslash($_POST['maps']));
    }

    if (!empty($errors)) {
      foreach ($errors as $error) {
        $this->messages->setError($error);
      }
    }

    $this->form->prependChild($this->messages->render());

    add_action('vtcore_maps_form_alter', $this->form);

    echo $this->header->render() . $this->form->render();
  }


  /**
   * Build the form
   */
  public function buildForm() {
    $this->form = new VTCore_Bootstrap_Form_BsInstance(array(
      'attributes' => array(
        'id' => 'maps-configuration-form',
        'method' => 'post',
        'action' => $_SERVER['REQUEST_URI'],
        'class' => array('container-fluid')
      ),
    ));


    foreach ($this->mapTypes as $type => $class) {
      $this->form->addChildren(new $class());
    }

    $this->form
    ->Submit(array(
      'attributes' => array(
        'name' => 'mapsSaveSubmit',
        'value' => __('Save', 'victheme_maps'),
        'class' => array('btn', 'btn-primary')
      ),
    ))
    ->BsSubmit(array(
      'attributes' => array(
        'name' => 'mapsResetSubmit',
        'value' => __('Reset', 'victheme_maps'),
      ),
      'mode' => 'danger',
      'confirmation' => true,
      'title' => __('Are you sure?', 'victheme_maps'),
      'ok' => __('Yes', 'victheme_maps'),
      'cancel' => __('No', 'victheme_maps'),
    ));

    return $this->form;

  }


  /**
   * Build the page header elements
   */
  public function buildHeader() {

    $plugin_data = get_plugin_data(VTCORE_MAPS_BASE_DIR . DIRECTORY_SEPARATOR . 'victheme_maps.php');

    $this->header = new VTCore_Bootstrap_Grid_BsContainerFluid(array(
      'type' => 'div',
      'attributes' => array(
        'id' => 'maps-options-header',
      ),
    ));

    $this->header
      ->BsRow()
      ->lastChild()
      ->BsColumn(array(
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 12,
            'small' => 12,
            'large' => 6,
          )
        ),
      ))
      ->lastChild()
      ->addOverloaderPrefix('VTCore_Fontawesome_')
      ->faIcon(array(
        'icon' => 'picture-o',
        'shape' => 'circle',
        'position' => 'pull-left',
      ))
      ->BsHeader(array(
        'text' => __('Maps Configuration', 'victheme_maps'),
        'small' => sprintf(__('version %s', 'victheme_maps'), $plugin_data['Version']),
      ))
      ->getParent()
      ->BsColumn(array(
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 12,
            'small' => 12,
            'large' => 6,
          )
        ),
      ));

    return $this;
  }

}