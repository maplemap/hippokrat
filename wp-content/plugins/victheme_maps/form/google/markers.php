<?php
/**
 * Building the Google Maps Markers configuration elements
 *
 *
 * @author jason.xie@victheme.com
 * @todo need popup etc
 *
 */
class VTCore_Maps_Form_Google_Markers
extends VTCore_Bootstrap_Form_Base {

  protected $context = array(
    'name' => '',
    'data' => array(
      'center' => false,
      'address' => '',
      'latitude' => '',
      'longitude' => '',
      'title' => '',
      'html' => '',
      'icon' => '',
    ),
  );

  public function buildElement() {
    $this
      ->addOverloaderPrefix('VTCore_Wordpress_Form_')
      ->WpMedia(array(
        'name' => $this->getContext('name') . '[data][icon]',
        'text' => __('Icons', 'victheme_maps'),
        'value' => $this->getData('icon'),
        'description' => __('Markers image to serve as the icon', 'victheme_maps'),
      ))
      ->BsText(array(
        'name' => $this->getContext('name') . '[data][address]',
        'text' => __('Address', 'victheme_maps'),
        'value' => $this->getData('address'),
        'description' => __('Marker location address for geocoding', 'victheme_maps'),
      ))
      ->BsText(array(
        'name' => $this->getContext('name') . '[data][latitude]',
        'text' => __('Latitudes', 'victheme_maps'),
        'value' => $this->getData('latitude'),
        'description' => __('Fallback latitudes if geocoding fails', 'victheme_maps'),
      ))
      ->BsText(array(
        'name' => $this->getContext('name') . '[data][longitude]',
        'text' => __('Longitudes', 'victheme_maps'),
        'value' => $this->getData('longitude'),
        'description' => __('Fallback longitudes if geocoding fails', 'victheme_maps'),
      ))
      ->BsText(array(
        'name' => $this->getContext('name') . '[data][title]',
        'text' => __('Title', 'victheme_maps'),
        'value' => $this->getData('title'),
        'description' => __('Marker hover text.', 'victheme_maps'),
      ))
      ->BsTextarea(array(
        'name' => $this->getContext('name') . '[data][html]',
        'text' => __('Content', 'victheme_maps'),
        'value' => $this->getData('html'),
        'description' => __('The infobox contents. Wordpress shortcode and HTML tags is allowed.', 'victheme_maps'),
      ))
      ->BsCheckbox(array(
        'name' => $this->getContext('name') . '[data][center]',
        'text' => __('Center Map?', 'victheme_maps'),
        'description' => __('Use this marker to center the map?.', 'victheme_maps'),
        'checked' => (boolean) $this->getData('center'),
      ));

  }

}