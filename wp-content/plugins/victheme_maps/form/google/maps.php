<?php
/**
 * Building the Google Maps configuration elements
 *
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Maps_Form_Google_Maps
extends VTCore_Bootstrap_Form_Base {

  protected $context = array(
    'name' => '',
    'basic' => true,
    'advanced' => true,
    'markers' => true,
    'infobox' => true,
    'data' => array(
      'address' => '',
      'longitude' => '',
      'latitude' => '',
      'width' => '',
      'height' => '',
      'responsive' => true,
      'zoom' => false,
      'zoom_control' => false,
      'zoom_control_position' => 'TOP_RIGHT',
      'zoom_control_style' =>  'DEFAULT',
      'pan_control' => false,
      'pan_control_position' => 'TOP_LEFT',
      'map_type_control' => false,
      'map_type_control_position' => 'TOP_CENTER',
      'map_type_control_style' => 'DROPDOWN_MENU',
      'scale_control' => false,
      'scrollwheel' => false,
      'streetview' => false,
      'maptype' => 'HYBRID',
      'draggable' => false,
      'disable_zoom' => false,
      'fitbounds' => false,
      'one_info_window' => false,
      'hide_by_click' => true,
      'start_open_info' => true,
      'enable_infowindow' => true,
      'info_maxwidth' => 640,
      'icon' => 'http://www.google.com/intl/en_us/mapfiles/ms/micons/red-dot.png',
      'styles' => '',
    ),
    'markers' => array(
      array(
        'name' => '',
        'data' => array(
          'center' => false,
          'address' => '',
          'latitude' => '',
          'longitude' => '',
          'title' => '',
          'html' => '',
          'icon' => '',
        ),
      ),
    ),
  );

  protected $position;
  protected $maptype;
  protected $mapcontrol;
  protected $zoomcontrol;




  /**
   * Providing array suitable for select element
   * for valid google map type.
   */
  protected function MapTypeOptions() {
    $this->maptype = array(
      'HYBRID' => __('Hybrid', 'victheme_maps'),
      'ROADMAP' => __('Roadmap', 'victheme_maps'),
      'SATELLITE' => __('Satellite', 'victheme_maps'),
      'TERRAIN' => __('Terrain', 'victheme_maps'),
    );

    return $this;
  }








  /**
   * Providing array suitable for select element
   * for maps navigation position.
   */
  protected function NavigationPositionOptions() {
    $this->position = array(
      'TOP_CENTER' => __('Top Center', 'victheme_maps'),
      'TOP_LEFT' => __('Top Left', 'victheme_maps'),
      'TOP_RIGHT' => __('Top Right', 'victheme_maps'),
      'LEFT_TOP' => __('Left Top', 'victheme_maps'),
      'RIGHT_TOP' => __('Right Top', 'victheme_maps'),
      'LEFT_CENTER' => __('Left Center', 'victheme_maps'),
      'RIGHT_CENTER' => __('Right Center', 'victheme_maps'),
      'LEFT_BOTTOM' => __('Left Bottom', 'victheme_maps'),
      'RIGHT_BOTTOM' => __('Right Bottom', 'victheme_maps'),
      'BOTTOM_CENTER' => __('Bottom Center', 'victheme_maps'),
      'BOTTOM_LEFT' => __('Bottom Left', 'victheme_maps'),
      'BOTTOM_RIGHT' => __('Bottom Right', 'victheme_maps'),
    );

    return $this;
  }




  protected function ZoomControlStyleOptions() {
    $this->zoomcontrol = array(
  		'DEFAULT' => __('Default', 'victheme_maps'),
  		'LARGE' => __('Large', 'victheme_maps'),
  		'SMALL' => __('Small', 'victheme_maps'),
  	);

    return $this;
  }


  protected function MapControlStyleOptions() {
    $this->mapcontrol = array(
      'DEFAULT' => __('Default', 'victheme_maps'),
      'DROPDOWN_MENU' => __('Drop down menu', 'victheme_maps'),
      'HORIZONTAL_BAR' => __('Horizontal Bar', 'victheme_maps'),
    );

    return $this;
  }


  /**
   * Overiding parent method
   * @see VTCore_Html_Base::buildElement()
   */
  public function buildElement() {

    $this
      ->MapTypeOptions()
      ->NavigationPositionOptions()
      ->ZoomControlStyleOptions()
      ->MapControlStyleOptions();

    // Basic configuration tabs content
    if ($this->getContext('basic')) {
      $basic = new VTCore_Bootstrap_Form_Base();
      $basic
        ->BsCheckbox(array(
          'name' => $this->getContext('name') . '[data][scrollwheel]',
          'text' => __('Mouse wheel scroll', 'victheme_maps'),
          'description' => __('Allow mouse wheel movement to zoom the map', 'victheme_maps'),
          'checked' => (boolean) $this->getContext('data.scrollwheel'),
          'switch' => true,
        ))
        ->BsCheckbox(array(
          'name' => $this->getContext('name') . '[data][draggable]',
          'text' => __('Draggable', 'victheme_maps'),
          'description' => __('Allow click and drag to move the maps position', 'victheme_maps'),
          'checked' => (boolean) $this->getContext('data.draggable'),
          'switch' => true,
        ))
        ->BsCheckbox(array(
          'name' => $this->getContext('name') . '[data][disable_zoom]',
          'text' => __('Double Click Zoom', 'victheme_maps'),
          'description' => __('Allow double click to zoom in the map', 'victheme_maps'),
          'checked' => (boolean) $this->getContext('data.disable_zoom'),
          'switch' => true,
        ))
        ->BsCheckbox(array(
          'name' => $this->getContext('name') . '[data][responsive]',
          'text' => __('Responsive', 'victheme_maps'),
          'description' => __('Allow the maps to be resized on viewport dimension change', 'victheme_maps'),
          'checked' => (boolean) $this->getContext('data.responsive'),
          'switch' => true,
        ))
        ->BsHeader(array(
          'text' => __('Maps Location', 'victheme_maps'),
          'tag' => 'h3',
        ))
        ->BsDescription(array(
          'text' => __('Set the default map centering, address type input will be geocoded using google geocoding
            service. This option will be overridden if there is an active marker configured', 'victheme_maps'),
        ))
        ->BsText(array(
          'name' => $this->getContext('name') . '[data][address]',
          'text' => __('Address', 'victheme_maps'),
          'value' => $this->getContext('data.address'),
        ))
        ->BsHeader(array(
          'text' => __('Maps Default Coordinates', 'victheme_maps'),
          'tag' => 'h3',
        ))
        ->BsDescription(array(
          'text' => __('Set the default map latitude and longitude coordinate. This option will be overridden if
            there is an active marker configured', 'victheme_maps'),
        ))
        ->BsRow()
        ->lastChild()
        ->BsText(array(
          'name' => $this->getContext('name') . '[data][latitude]',
          'text' => __('Latitude', 'victheme_maps'),
          'value' => $this->getContext('data.latitude'),
          'grids' => array(
            'columns' => array(
              'mobile' => '6',
              'tablet' => '6',
              'small' => '6',
              'large' => '6',
            ),
          ),
        ))
        ->BsText(array(
          'name' => $this->getContext('name') . '[data][longitude]',
          'text' => __('Longitude', 'victheme_maps'),
          'value' => $this->getContext('data.longitude'),
          'grids' => array(
            'columns' => array(
              'mobile' => '6',
              'tablet' => '6',
              'small' => '6',
              'large' => '6',
            ),
          ),
        ))
        ->getParent()
        ->BsHeader(array(
          'text' => __('Maps Size', 'victheme_maps'),
          'tag' => 'h3',
        ))
        ->BsDescription(array(
          'text' => __('Set the initial map width and height in pixel, if responsive
            settings is enabled then map will auto resize when browser viewport
            dimension changes', 'victheme_maps'),
        ))
        ->BsRow()
        ->lastChild()
        ->BsNumber(array(
          'name' => $this->getContext('name') . '[data][width]',
          'text' => __('Width', 'victheme_maps'),
          'value' => $this->getContext('data.width'),
          'suffix' => 'px',
          'grids' => array(
            'columns' => array(
              'mobile' => '6',
              'tablet' => '6',
              'small' => '6',
              'large' => '6',
            ),
          ),
        ))
        ->BsNumber(array(
          'name' => $this->getContext('name') . '[data][height]',
          'text' => __('Height', 'victheme_maps'),
          'value' => $this->getContext('data.height'),
          'suffix' => 'px',
          'grids' => array(
            'columns' => array(
              'mobile' => '6',
              'tablet' => '6',
              'small' => '6',
              'large' => '6',
            ),
          ),
        ));

      $content[] = array(
        'title' => __('Basic', 'victheme_maps'),
        'content' => $basic,
      );

      unset($basic);
    }

    // Advanced configuration control
    if ($this->getContext('advanced')) {
      $advanced = new VTCore_Bootstrap_Form_Base();
      $advanced
        ->BsCheckbox(array(
          'name' => $this->getContext('name') . '[data][streetview]',
          'text' => __('Street View', 'victheme_maps'),
          'description' => __('Allow street view maps viewing mode', 'victheme_maps'),
          'checked' => (boolean) $this->getContext('data.streetview'),
          'switch' => true,
        ))
        ->BsCheckbox(array(
          'name' => $this->getContext('name') . '[data][map_type_control]',
          'text' => __('Map Type Control', 'victheme_maps'),
          'description' => __('Show the map type controller', 'victheme_maps'),
          'checked' => (boolean) $this->getContext('data.map_type_control'),
          'switch' => true,
        ))
        ->BsCheckbox(array(
          'name' => $this->getContext('name') . '[data][zoom_control]',
          'text' => __('Zoom Control', 'victheme_maps'),
          'description' => __('Show the zoom in / out controller', 'victheme_maps'),
          'checked' => (boolean) $this->getContext('data.zoom_control'),
          'switch' => true,
        ))
        ->BsCheckbox(array(
          'name' => $this->getContext('name') . '[data][pan_control]',
          'text' => __('Pan Control', 'victheme_maps'),
          'description' => __('Show the panning controller', 'victheme_maps'),
          'checked' => (boolean) $this->getContext('data.pan_control'),
          'switch' => true,
        ))
        ->BsCheckbox(array(
          'name' => $this->getContext('name') . '[data][scale_control]',
          'text' => __('Scale Control', 'victheme_maps'),
          'description' => __('Show the scale controler', 'victheme_maps'),
          'checked' => (boolean) $this->getContext('data.scale_control'),
          'switch' => true,
        ))
        ->BsCheckbox(array(
          'name' => $this->getContext('name') . '[data][hide_by_click]',
          'text' => __('Hide Info Window', 'victheme_maps'),
          'description' => __('Enable click event to show / hide the info window', 'victheme_maps'),
          'checked' => (boolean) $this->getContext('data.hide_by_click'),
          'switch' => true,
        ))
        ->BsCheckbox(array(
          'name' => $this->getContext('name') . '[data][start_open_info]',
          'text' => __('Open info window', 'victheme_maps'),
          'description' => __('Open info window on initial loading', 'victheme_maps'),
          'checked' => (boolean) $this->getContext('data.start_open_info'),
          'switch' => true,
        ))
        ->BsCheckbox(array(
          'name' => $this->getContext('name') . '[data][one_info_window]',
          'text' => __('One info window', 'victheme_maps'),
          'description' => __('Only open one info window on initial load', 'victheme_maps'),
          'checked' => (boolean) $this->getContext('data.one_info_window'),
          'switch' => true,
        ))
        ->BsRow()
        ->lastChild()
        ->addChildren(new VTCore_Wordpress_Form_WpMedia(array(
          'text' => __('Maps Icon', 'victheme_maps'),
          'description' => __('Define the default maps icon', 'victheme_maps'),
          'name' => $this->getContext('name') . '[data][icon]',
          'value' => $this->getContext('data.icon'),
          'data' => array(
            'type' => 'image',
            'title' => __('Select Image', 'victheme_maps'),
            'button' =>__('Select Image', 'victheme_maps'),
          ),
          'grids' => array(
            'columns' => array(
              'mobile' => '6',
              'tablet' => '6',
              'small' => '6',
              'large' => '6',
            ),
          ),
        )))
        ->getParent()
        ->BsRow()
        ->lastChild()
        ->BsSelect(array(
          'text' => __('Map Zoom Level', 'victheme_maps'),
          'name' => $this->getContext('name') . '[data][zoom]',
          'description' => __('Set the initial zoom level for the maps', 'victheme_maps'),
          'value' => $this->getContext('data.zoom'),
          'options' => range(0, 20, 1),
          'grids' => array(
            'columns' => array(
              'mobile' => '6',
              'tablet' => '6',
              'small' => '6',
              'large' => '6',
            ),
          ),
        ))
        ->BsSelect(array(
          'text' => __('Map Types', 'victheme_maps'),
          'description' => __('Set the initial map types to be displayed', 'victheme_maps'),
          'name' => $this->getContext('name') . '[data][maptype]',
          'value' => $this->getContext('data.maptype'),
          'options' => $this->maptype,
          'grids' => array(
            'columns' => array(
              'mobile' => '6',
              'tablet' => '6',
              'small' => '6',
              'large' => '6',
            ),
          ),
        ))
        ->BsSelect(array(
          'text' => __('Map Type Control Position', 'victheme_maps'),
          'name' => $this->getContext('name') . '[data][map_type_control_position]',
          'description' => __('Define the location for the map type controller', 'victheme_maps'),
          'value' => $this->getContext('data.map_type_control_position'),
          'options' => $this->position,
          'grids' => array(
            'columns' => array(
              'mobile' => '6',
              'tablet' => '6',
              'small' => '6',
              'large' => '6',
            ),
          ),
        ))
        ->BsSelect(array(
          'text' => __('Map Type Control Style', 'victheme_maps'),
          'name' => $this->getContext('name') . '[data][map_type_control_style]',
          'description' => __('Set the style for the map type controller', 'victheme_maps'),
          'value' => $this->getContext('data.map_type_control_style'),
          'options' => $this->mapcontrol,
          'grids' => array(
            'columns' => array(
              'mobile' => '6',
              'tablet' => '6',
              'small' => '6',
              'large' => '6',
            ),
          ),
        ))
        ->getParent()
        ->BsRow()
        ->lastChild()
        ->BsSelect(array(
          'text' => __('Pan Control Position', 'victheme_maps'),
          'name' => $this->getContext('name') . '[data][pan_control_position]',
          'description' => __('Define the location for the panning controller', 'victheme_maps'),
          'value' => $this->getContext('data.pan_control_position'),
          'options' => $this->position,
          'grids' => array(
            'columns' => array(
              'mobile' => '6',
              'tablet' => '6',
              'small' => '6',
              'large' => '6',
            ),
          ),
        ))
        ->getParent()
        ->BsRow()
        ->lastChild()
        ->BsSelect(array(
          'text' => __('Zoom Control Position', 'victheme_maps'),
          'description' => __('Define the location for the zooming controller', 'victheme_maps'),
          'name' => $this->getContext('name') . '[data][zoom_control_position]',
          'value' => $this->getContext('data.zoom_control_position'),
          'options' => $this->position,
          'grids' => array(
            'columns' => array(
              'mobile' => '6',
              'tablet' => '6',
              'small' => '6',
              'large' => '6',
            ),
          ),
        ))
        ->BsSelect(array(
          'text' => __('Zoom Control Style', 'victheme_maps'),
          'name' => $this->getContext('name') . '[data][zoom_control_style]',
          'description' => __('Set the style for the zooming controller', 'victheme_maps'),
          'value' => $this->getContext('data.zoom_control_style'),
        	'options' => $this->zoomcontrol,
          'grids' => array(
            'columns' => array(
              'mobile' => '6',
              'tablet' => '6',
              'small' => '6',
              'large' => '6',
            ),
          ),
        ))
        ->getParent()
        ->BsRow()
        ->lastChild()
        ->BsSelect(array(
          'text' => __('Fit Bounds', 'victheme_maps'),
          'name' => $this->getContext('name') . '[data][fitbounds]',
          'description' => __('Fit all the markers in a single window', 'victheme_maps'),
          'value' => $this->getContext('data.fitbounds'),
          'options' => array(
            false => __('None', 'victheme_maps'),
            'any' => __('All marker', 'victheme_maps'),
            'visible' => __('Visible marker', 'victheme_maps'),
          ),
          'grids' => array(
            'columns' => array(
              'mobile' => '6',
              'tablet' => '6',
              'small' => '6',
              'large' => '6',
            ),
          ),
        ))
        ->getParent()
        ->BsRow()
        ->lastChild()
        ->BsTextarea(array(
          'text' => __('Custom Map Styles', 'victheme_maps'),
          'description' => __('Insert valid JSON text to style the maps, you can create the string via
                              <a href="http://googlemaps.github.io/js-samples/styledmaps/wizard/index.html">Google Maps Styler Tools</a>'),
          'name' => $this->getContext('name') . '[data][styles]',
          'value' => $this->getContext('data.styles'),
          'grids' => array(
            'columns' => array(
              'mobile' => '12',
              'tablet' => '12',
              'small' => '12',
              'large' => '12',
            ),
          ),
          'raw' => true,
        ));


      $content[] = array(
        'title' => __('Advanced', 'victheme_maps'),
        'content' => $advanced,
      );

      unset($advanced);
    }

    // Info Box configuration control
    if ($this->getContext('infobox')) {
      $info = new VTCore_Bootstrap_Form_Base();
      $info
        ->BsCheckbox(array(
          'name' => $this->getContext('name') . '[data][enable_infowindow]',
          'text' => __('Enable info window', 'victheme_maps'),
          'description' => __('Enable / Disable the info window feature', 'victheme_maps'),
          'checked' => (boolean) $this->getContext('data.enable_infowindow'),
          'switch' => true,
        ))
        ->BsCheckbox(array(
          'name' => $this->getContext('name') . '[data][hide_by_click]',
          'text' => __('Hide by click', 'victheme_maps'),
          'description' => __('Show / hide the info window by clicking the markers', 'victheme_maps'),
          'checked' => (boolean) $this->getContext('data.hide_by_click'),
          'switch' => true,
        ))
        ->BsCheckbox(array(
          'name' => $this->getContext('name') . '[data][start_open_info]',
          'text' => __('Open info window', 'victheme_maps'),
          'description' => __('Open info window on initial loading', 'victheme_maps'),
          'checked' => (boolean) $this->getContext('data.start_open_info'),
          'switch' => true,
        ))
        ->BsCheckbox(array(
          'name' => $this->getContext('name') . '[data][one_info_window]',
          'text' => __('One info window', 'victheme_maps'),
          'description' => __('Only open one info window at all time', 'victheme_maps'),
          'checked' => (boolean) $this->getContext('data.one_info_window'),
          'switch' => true,
        ))
        ->BsNumber(array(
          'name' => $this->getContext('name') . '[data][info_maxwidth]',
          'text' => __('Maximum width', 'victheme_maps'),
          'value' => $this->getContext('data.info_maxwidth'),
          'suffix' => 'px',
        ));


      $content[] = array(
        'title' => __('Info Box', 'victheme_maps'),
        'content' => $info,
      );

      unset($info);
    }

    if ($this->getContext('marker')) {

      VTCore_Wordpress_Utility::loadAsset('jquery-table-manager');

      $markers = new VTCore_Bootstrap_Form_Base();
      $markers
        ->BsElement(array(
          'type' => 'div',
          'attributes' => array(
            'class' => array('table-manager'),
          ),
        ))
        ->lastChild()
        ->Table(array(
          'headers' => array(
            ' ',
            __('Markers', 'victheme_maps'),
            ' ',
          ),
          'rows' => $this->buildRows(),
          'attributes' => array('data-filter' => 2),
        ))
        ->Button(array(
          'text' => __('Add New Entry', 'victheme_maps'),
          'attributes' => array(
            'data-tablemanager-type' => 'addrow',
            'class' => array('button', 'button-large', 'button-primary'),
          ),
        ));

      $content[] = array(
        'title' => __('Markers', 'victheme_maps'),
        'content' => $markers,
      );

      unset($markers);
    }


    if (isset($content) && !empty($content)) {
      $this->BsTabs(array(
        'prefix' => 'tabs-__i__',
        'contents' => $content,
      ));

      unset($content);
    }

  }


  /**
   * Building markers table rows
   */
  protected function buildRows() {
    $rows = array();
    foreach ($this->getContext('markers') as $delta => $marker) {
      // Draggable Icon
      $rows[$delta][] = array(
        'content' => new VTCore_Bootstrap_Element_BsElement(array(
          'type' => 'span',
          'attributes' => array(
            'class' => array('drag-icon'),
          ),
        )),
        'attributes' => array(
          'class' => array('drag-element'),
        ),
      );


      $marker['name'] = $this->getContext('name') . '[markers][' . $delta . ']';
      $rows[$delta][] = new VTCore_Maps_Form_Google_Markers($marker);


      // Remove button
      $rows[$delta][] = new VTCore_Form_Button(array(
        'text' => 'X',
        'attributes' => array(
          'data-tablemanager-type' => 'removerow',
          'class' => array('button', 'button-mini', 'form-button'),
        ),
      ));
    }


    return $rows;

  }

}