<?php
/**
 * Class extending the Shortcodes base class
 * for building the google maps marker.
 *
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Maps_Addon_Visualcomposer_Shortcodes_Google_Marker
extends VTCore_Wordpress_Models_VC {


  public function registerVC() {

    if (class_exists('NewVisualComposer')) {
      VTCore_Wordpress_Utility::loadAsset('maps-visualcomposer');
    }

    $options = array(
      'name' => __('Maps Marker', 'victheme_maps'),
      'description' => __('Custom marker for Google Maps', 'victheme_maps'),
      'base' => 'google_marker',
      'icon' => 'icon-marker',
      'category' => __('Maps', 'victheme_maps'),
      'as_child' => array(
        'only' => 'google_maps',
      ),
      'params' => array(

        array(
          'type' => 'checkbox',
          'heading' => __('Center Map', 'victheme_maps'),
          'param_name' => 'center',
          'description' => __('Use this marker as the center of the map and overrides the default map centering position', 'victheme_maps'),
          'edit_field_class' => 'vc_col-xs-12',
          'value' => array(__('Enable', 'victheme_maps') => 'true'),
          'admin_label' => true,
        ),

        array(
          'type' => 'attach_image',
          'heading' => __('Marker Icon', 'victheme_maps'),
          'param_name' => 'Icon',
          'value' => '',
          'admin_label' => true,
          'description' => __('Select image from media library or leave empty to use default icon.', 'victheme_maps')
        ),

        array(
          'type' => 'textfield',
          'heading' => __('Address', 'victheme_maps'),
          'param_name' => 'address',
          'admin_label' => true,
          'edit_field_class' => 'vc_col-xs-12 clearboth',
          'description' => __('Set the marker address that will be geocoded using google geocoding service.', 'victheme_maps'),
        ),

        array(
          'type' => 'textfield',
          'heading' => __('Latitude', 'victheme_maps'),
          'param_name' => 'latitude',
          'admin_label' => true,
          'edit_field_class' => 'vc_col-xs-6 clearboth',
          'description' => __('Set the marker latitude coordinate.', 'victheme_maps'),
        ),

        array(
          'type' => 'textfield',
          'heading' => __('Longitude', 'victheme_maps'),
          'param_name' => 'longitude',
          'admin_label' => true,
          'edit_field_class' => 'vc_col-xs-6',
          'description' => __('Set the marker longitude coordinate.', 'victheme_maps'),
        ),

        array(
          'type' => 'textfield',
          'heading' => __('Title', 'victheme_maps'),
          'param_name' => 'title',
          'admin_label' => true,
          'edit_field_class' => 'vc_col-xs-12',
          'description' => __('Set the popup hover title.', 'victheme_maps'),
        ),

        array(
          'type' => 'textarea',
          'heading' => __('Content', 'victheme_maps'),
          'param_name' => 'html',
          'admin_label' => true,
          'edit_field_class' => 'vc_col-xs-12',
          'description' => __('Set the popup content text.', 'victheme_maps'),
        ),
      ),
    );

    return $options;
  }
}


class WPBakeryShortCode_Google_Marker extends WPBakeryShortCode {}