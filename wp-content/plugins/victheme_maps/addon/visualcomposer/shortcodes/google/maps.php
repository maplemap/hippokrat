<?php
/**
/**
 * Class extending the Shortcodes base class
 * for building the google maps.
 *
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Maps_Addon_Visualcomposer_Shortcodes_Google_Maps
extends VTCore_Wordpress_Models_VC {


  public function registerVC() {

    $options = array(
      'name' => __('Google Maps', 'victheme_maps'),
      'description' => __('Google Maps instance', 'victheme_maps'),
      'base' => 'google_maps',
      'icon' => 'icon-maps',
      'category' => __('Maps', 'victheme_maps'),
      'as_parent' => array(
        'only' => 'google_marker',
      ),
      'is_container' => true,
      'js_view' => 'VTCoreContainer',
      'params' => array(

        array(
          'type' => 'textfield',
          'heading' => __('Address', 'victheme_maps'),
          'param_name' => 'address',
          'edit_field_class' => 'vc_col-xs-12 clearboth',
          'admin_label' => true,
          'description' => __('Set the default map centering, address type input will be geocoded using google geocoding
                               service. This option will be overridden if there is an active marker configured', 'victheme_maps'),
        ),


        array(
          'type' => 'textfield',
          'heading' => __('Latitude', 'victheme_maps'),
          'param_name' => 'latitude',
          'edit_field_class' => 'vc_col-xs-6 clearboth',
          'admin_label' => true,
          'description' => __('Set the default map latitude coordinate.', 'victheme_maps'),
        ),

        array(
          'type' => 'textfield',
          'heading' => __('Longitude', 'victheme_maps'),
          'param_name' => 'longitude',
          'edit_field_class' => 'vc_col-xs-6',
          'admin_label' => true,
          'description' => __('Set the default map longitude coordinate.', 'victheme_maps'),
        ),


        array(
          'type' => 'textfield',
          'heading' => __('Width', 'victheme_maps'),
          'param_name' => 'width',
          'admin_label' => true,
          'edit_field_class' => 'vc_col-xs-6 clearboth',
          'description' => __('Set the initial map width in pixel', 'victheme_maps'),
        ),


        array(
          'type' => 'textfield',
          'heading' => __('Height', 'victheme_maps'),
          'param_name' => 'height',
          'edit_field_class' => 'vc_col-xs-6',
          'description' => __('Set the initial map height in pixel', 'victheme_maps'),
          'admin_label' => true,
        ),

        array(
          'type' => 'textfield',
          'heading' => __('Info window max width', 'victheme_maps'),
          'param_name' => 'info_maxwidth',
          'edit_field_class' => 'vc_col-xs-12 clearboth',
          'value' => '640',
          'description' => __('Set the maximum width for the info window', 'victheme_maps'),
          'admin_label' => true,
          'group' => __('Info Window', 'victheme_maps'),
        ),

        array(
          'type' => 'dropdown',
          'heading' => __('Scroll Wheel', 'victheme_maps'),
          'param_name' => 'scrollwheel',
          'description' => __('Allow mouse wheel movement to zoom the map', 'victheme_maps'),
          'edit_field_class' => 'vc_col-xs-4',
          'value' => array(
            __('Yes', 'victheme_maps') => 'true',
            __('No', 'victheme_maps') => 'false',
          ),
          'admin_label' => true,
          'group' => __('Advanced', 'victheme_maps'),
        ),

        array(
          'type' => 'dropdown',
          'heading' => __('Draggable', 'victheme_maps'),
          'param_name' => 'draggable',
          'description' => __('Allow click and drag to move the maps position', 'victheme_maps'),
          'edit_field_class' => 'vc_col-xs-4',
          'value' => array(
            __('Yes', 'victheme_maps') => 'true',
            __('No', 'victheme_maps') => 'false',
          ),
          'admin_label' => true,
          'group' => __('Advanced', 'victheme_maps'),
        ),

        array(
          'type' => 'dropdown',
          'heading' => __('Double Click Zoom', 'victheme_maps'),
          'param_name' => 'disable_zoom',
          'description' => __('Allow double click to zoom in the map', 'victheme_maps'),
          'edit_field_class' => 'vc_col-xs-4',
          'value' => array(
            __('Yes', 'victheme_maps') => 'true',
            __('No', 'victheme_maps') => 'false',
          ),
          'admin_label' => true,
          'group' => __('Advanced', 'victheme_maps'),
        ),

        array(
          'type' => 'dropdown',
          'heading' => __('Responsive', 'victheme_maps'),
          'param_name' => 'responsive',
          'description' => __('Allow the maps to be resized on viewport dimension change', 'victheme_maps'),
          'edit_field_class' => 'vc_col-xs-4 clearboth',
          'value' => array(
            __('Yes', 'victheme_maps') => 'true',
            __('No', 'victheme_maps') => 'false',
          ),
          'admin_label' => true,
          'group' => __('Advanced', 'victheme_maps'),
        ),


        array(
          'type' => 'dropdown',
          'heading' => __('Street View', 'victheme_maps'),
          'param_name' => 'streetview',
          'description' => __('Allow street view maps viewing mode', 'victheme_maps'),
          'edit_field_class' => 'vc_col-xs-4',
          'value' => array(
            __('Yes', 'victheme_maps') => 'true',
            __('No', 'victheme_maps') => 'false',
          ),
          'admin_label' => true,
          'group' => __('Advanced', 'victheme_maps'),
        ),


        array(
          'type' => 'dropdown',
          'heading' => __('Scale Control', 'victheme_maps'),
          'param_name' => 'scale_control',
          'description' => __('Show the scale controler', 'victheme_maps'),
          'edit_field_class' => 'vc_col-xs-4',
          'value' => array(
            __('Yes', 'victheme_maps') => 'true',
            __('No', 'victheme_maps') => 'false',
          ),
          'admin_label' => true,
        ),


        array(
          'type' => 'dropdown',
          'heading' => __('Map Type Control', 'victheme_maps'),
          'param_name' => 'map_type_control',
          'description' => __('Show the map type controller', 'victheme_maps'),
          'edit_field_class' => 'vc_col-xs-4',
          'value' => array(
            __('Yes', 'victheme_maps') => 'true',
            __('No', 'victheme_maps') => 'false',
          ),
          'admin_label' => true,
        ),

        array(
          'type' => 'dropdown',
          'heading' => __('Zoom Control', 'victheme_maps'),
          'param_name' => 'zoom_control',
          'description' => __('Show the zoom in / out controller', 'victheme_maps'),
          'edit_field_class' => 'vc_col-xs-4',
          'value' => array(
            __('Yes', 'victheme_maps') => 'true',
            __('No', 'victheme_maps') => 'false',
          ),
          'admin_label' => true,
        ),


        array(
          'type' => 'dropdown',
          'heading' => __('Pan Control', 'victheme_maps'),
          'param_name' => 'pan_control',
          'description' => __('Show the panning controller', 'victheme_maps'),
          'edit_field_class' => 'vc_col-xs-4',
          'value' => array(
            __('Yes', 'victheme_maps') => 'true',
            __('No', 'victheme_maps') => 'false',
          ),
          'admin_label' => true,
        ),


        array(
          'type' => 'dropdown',
          'heading' => __('Map Zoom Level', 'victheme_maps'),
          'param_name' => 'zoom',
          'edit_field_class' => 'vc_col-xs-4',
          'description' => __('Set the initial zoom level for the maps', 'victheme_maps'),
          'value' => range(0, 20, 1),
          'admin_label' => true,
        ),


        array(
          'type' => 'dropdown',
          'heading' => __('Map Types', 'victheme_maps'),
          'param_name' => 'maptype',
          'edit_field_class' => 'vc_col-xs-4',
          'description' => __('Set the initial map types to be displayed', 'victheme_maps'),
          'value' => array(
            __('Hybrid', 'victheme_maps') => 'HYBRID',
            __('Roadmap', 'victheme_maps') => 'ROADMAP',
            __('Satellite', 'victheme_maps') => 'SATELLITE',
            __('Terrain', 'victheme_maps') => 'TERRAIN',
          ),
          'admin_label' => true,
        ),

        array(
          'type' => 'dropdown',
          'heading' => __('Pan Control Position', 'victheme_maps'),
          'param_name' => 'pan_control_position',
          'edit_field_class' => 'vc_col-xs-4',
          'description' => __('Define the location for the panning controller', 'victheme_maps'),
          'value' => array(
            __('Top Center', 'victheme_maps') => 'TOP_CENTER',
            __('Top Left', 'victheme_maps') => 'TOP_LEFT',
            __('Top Right', 'victheme_maps') => 'TOP_RIGHT',
            __('Left Top', 'victheme_maps') => 'LEFT_TOP',
            __('Right Top', 'victheme_maps') => 'RIGHT_TOP',
            __('Left Center', 'victheme_maps') => 'LEFT_CENTER',
            __('Right Center', 'victheme_maps') => 'RIGHT_CENTER',
            __('Left Bottom', 'victheme_maps') => 'LEFT_BOTTOM',
            __('Right Bottom', 'victheme_maps') => 'RIGHT_BOTTOM',
            __('Bottom Center', 'victheme_maps') => 'BOTTOM_CENTER',
            __('Bottom Left', 'victheme_maps') => 'BOTTOM_LEFT',
            __('Bottom Right', 'victheme_maps') => 'BOTTOM_RIGHT',
          ),
          'admin_label' => true,
          'group' => __('Advanced', 'victheme_maps'),
        ),

        array(
          'type' => 'dropdown',
          'heading' => __('Zoom Control Position', 'victheme_maps'),
          'param_name' => 'zoom_control_position',
          'edit_field_class' => 'vc_col-xs-4 clearboth',
          'description' => __('Define the location for the zooming controller', 'victheme_maps'),
          'value' => array(
            __('Top Center', 'victheme_maps') => 'TOP_CENTER',
            __('Top Left', 'victheme_maps') => 'TOP_LEFT',
            __('Top Right', 'victheme_maps') => 'TOP_RIGHT',
            __('Left Top', 'victheme_maps') => 'LEFT_TOP',
            __('Right Top', 'victheme_maps') => 'RIGHT_TOP',
            __('Left Center', 'victheme_maps') => 'LEFT_CENTER',
            __('Right Center', 'victheme_maps') => 'RIGHT_CENTER',
            __('Left Bottom', 'victheme_maps') => 'LEFT_BOTTOM',
            __('Right Bottom', 'victheme_maps') => 'RIGHT_BOTTOM',
            __('Bottom Center', 'victheme_maps') => 'BOTTOM_CENTER',
            __('Bottom Left', 'victheme_maps') => 'BOTTOM_LEFT',
            __('Bottom Right', 'victheme_maps') => 'BOTTOM_RIGHT',
          ),
          'admin_label' => true,
          'group' => __('Advanced', 'victheme_maps'),
        ),


        array(
          'type' => 'dropdown',
          'heading' => __('Zoom Control Style', 'victheme_maps'),
          'param_name' => 'zoom_control_style',
          'edit_field_class' => 'vc_col-xs-4',
          'description' => __('Set the style for the zooming controller', 'victheme_maps'),
          'value' => array(
        		__('Default', 'victheme_maps') => 'DEFAULT',
        		__('Large', 'victheme_maps') => 'LARGE',
        		__('Small', 'victheme_maps') => 'SMALL',
          ),
          'admin_label' => true,
          'group' => __('Advanced', 'victheme_maps'),
        ),


        array(
          'type' => 'dropdown',
          'heading' => __('Map Type Control Position', 'victheme_maps'),
          'param_name' => 'map_type_control_position',
          'edit_field_class' => 'vc_col-xs-4',
          'description' => __('Define the location for the map type controller', 'victheme_maps'),
          'value' => array(
            __('Top Center', 'victheme_maps') => 'TOP_CENTER',
            __('Top Left', 'victheme_maps') => 'TOP_LEFT',
            __('Top Right', 'victheme_maps') => 'TOP_RIGHT',
            __('Left Top', 'victheme_maps') => 'LEFT_TOP',
            __('Right Top', 'victheme_maps') => 'RIGHT_TOP',
            __('Left Center', 'victheme_maps') => 'LEFT_CENTER',
            __('Right Center', 'victheme_maps') => 'RIGHT_CENTER',
            __('Left Bottom', 'victheme_maps') => 'LEFT_BOTTOM',
            __('Right Bottom', 'victheme_maps') => 'RIGHT_BOTTOM',
            __('Bottom Center', 'victheme_maps') => 'BOTTOM_CENTER',
            __('Bottom Left', 'victheme_maps') => 'BOTTOM_LEFT',
            __('Bottom Right', 'victheme_maps') => 'BOTTOM_RIGHT',
          ),
          'admin_label' => true,
          'group' => __('Advanced', 'victheme_maps'),
        ),


        array(
          'type' => 'dropdown',
          'heading' => __('Map Type Control Style', 'victheme_maps'),
          'param_name' => 'map_type_control_style',
          'edit_field_class' => 'vc_col-xs-4 clearboth',
          'description' => __('Set the style for the map type controller', 'victheme_maps'),
          'value' => array(
            __('Default', 'victheme_maps') => 'DEFAULT',
            __('Drop down menu', 'victheme_maps') => 'DROPDOWN_MENU',
            __('Horizontal Bar', 'victheme_maps') => 'HORIZONTAL_BAR',
          ),
          'admin_label' => true,
          'group' => __('Advanced', 'victheme_maps'),
        ),

        array(
          'type' => 'dropdown',
          'heading' => __('Fit Bounds', 'victheme_maps'),
          'param_name' => 'fitbounds',
          'edit_field_class' => 'vc_col-xs-4',
          'description' => __('Fit all the markers', 'victheme_maps'),
          'value' => array(
            __('None', 'victheme_maps') => 'false',
            __('All Markers', 'victheme_maps') => 'all',
            __('Visible Markers', 'victheme_maps') => 'visible',
          ),
          'admin_label' => true,
          'group' => __('Advanced', 'victheme_maps'),
        ),

        array(
          'type' => 'dropdown',
          'heading' => __('Enable info window', 'victheme_maps'),
          'param_name' => 'enable_infowindow',
          'edit_field_class' => 'vc_col-xs-4',
          'description' => __('Enable / Disable the info window completely', 'victheme_maps'),
          'value' => array(
            __('Enable', 'victheme_maps') => 'true',
            __('Disable', 'victheme_maps') => 'false',
          ),
          'admin_label' => true,
          'group' => __('Info Window', 'victheme_maps'),
        ),

        array(
          'type' => 'dropdown',
          'heading' => __('Hide by click', 'victheme_maps'),
          'param_name' => 'hide_by_click',
          'edit_field_class' => 'vc_col-xs-4',
          'description' => __('Show / hide the info window by clicking the markers', 'victheme_maps'),
          'value' => array(
            __('Enable', 'victheme_maps') => 'true',
            __('Disable', 'victheme_maps') => 'false',
          ),
          'admin_label' => true,
          'group' => __('Advanced', 'victheme_maps'),
        ),

        array(
          'type' => 'textarea_raw_html',
          'heading' => __('Custom Map Styles', 'victheme_maps'),
          'param_name' => 'custom_styles',
          'edit_field_class' => 'vc_col-xs-12',
          'description' => __('Insert valid JSON text to style the maps, you can create the string via
                              <a href="http://googlemaps.github.io/js-samples/styledmaps/wizard/index.html">Google Maps Styler Tools</a>'),
          'admin_label' => false,
          'group' => __('Advanced', 'victheme_maps'),
        ),


        array(
          'type' => 'dropdown',
          'heading' => __('Open all info window', 'victheme_maps'),
          'param_name' => 'start_open_info',
          'edit_field_class' => 'vc_col-xs-4',
          'description' => __('Open all info window on start', 'victheme_maps'),
          'value' => array(
            __('Enable', 'victheme_maps') => 'true',
            __('Disable', 'victheme_maps') => 'false',
          ),
          'admin_label' => true,
          'group' => __('Info Window', 'victheme_maps'),
        ),

        array(
          'type' => 'dropdown',
          'heading' => __('One info window', 'victheme_maps'),
          'param_name' => 'one_info_window',
          'edit_field_class' => 'vc_col-xs-4',
          'description' => __('Only open one info window at all time', 'victheme_maps'),
          'value' => array(
            __('Enable', 'victheme_maps') => 'true',
            __('Disable', 'victheme_maps') => 'false',
          ),
          'admin_label' => true,
          'group' => __('Info Window', 'victheme_maps'),
        ),
      ),
    );

    return $options;
  }
}


class WPBakeryShortCode_Google_Maps extends WPBakeryShortCodesContainer {}