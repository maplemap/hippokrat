<?php
/**
 * Class for building a simple mini cart
 * with a single line text and using tokenized system
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_WooCommerce_Widgets_MiniCart
extends WP_Widget {

  private $defaults;
  private $instance;
  private $args;
  private $text;


  /**
   * Registering widget as WP_Widget requirement
   */
  public function __construct() {
    parent::__construct(
      'vtcore_woocommerce_widgets_minicart',
      'WooCommerce Mini Cart',
      array('description' => 'Displaying WooCommerce mini cart with token system')
    );
  }



  /**
   * Registering widget
   */
  public function registerWidget() {
    return register_widget('vtcore_woocommerce_widgets_minicart');
  }


  /**
   * Extending widget
   *
   * @see WP_Widget::widget()
   */
  public function widget($args, $instance) {

    $this->loadInstance($args, $instance);

    $title = apply_filters( 'widget_title', $instance['title'] );
    echo $this->args['before_widget'];

    if (!empty($title)) {
      echo $this->args['before_title'] . $title . $this->args['after_title'];
    }

    // Process the token string here!
    $this->buildText();

    $element = new VTCore_Bootstrap_Element_Base(array(
      'type' => 'div',
      'attributes' => array(
        'id' => $this->getWidgetContentID(),
        'class' => array(
          'mini-cart' => 'vtcore-mini-cart'
        ),
      ),
    ));

    $element->addChildren($this->text)->render();

    echo $args['after_widget'];
  }


  /**
   * Method for loading instance to the object
   * @param $args
   * @param $instance
   * @return $this
   */
  protected function loadInstance($args, $instance) {
    $this->defaults = array(
      'text' => __('Cart %cart_total_amount <span class="total-product">%cart_total_product</span>', 'victheme_woocommerce'),
      'empty_text' => __('Cart empty <span class="total-product">0</span>', 'victheme_woocommerce'),
    );

    $this->args = $args;
    $this->instance = VTCore_Utility::arrayMergeRecursiveDistinct($instance, $this->defaults);

    return $this;
  }



  /**
   * Method for building the cart text
   * @return mixed
   */
  protected function buildText() {

    $token = apply_filters('vtcore_woocommerce_cart_token', array(
      '%cart_total_amount' => WC()->cart->get_cart_subtotal(),
      '%cart_total_product' => count(WC()->cart->get_cart()),
      '%cart_url' => WC()->cart->get_cart_url(),
      '%checkout_url' => WC()->cart->get_checkout_url(),
    ));


    // Cart has content
    if (sizeof(WC()->cart->get_cart()) > 0 ) {
      $this->text = str_replace(array_keys($token), array_values($token), $this->instance['text']);
    }

    // Cart is empty
    else {
      $this->text = str_replace(array_keys($token), array_values($token), $this->instance['empty_text']);
    }

    return $this;
  }



  /**
   * Retrieving valid markup id for the replaceable widget content
   * @return string
   */
  public function getWidgetContentID() {
    return 'vtcore-mini-cart-' . $this->args['widget_id'];
  }


  /**
   * Retrieve the converted token text
   * @param $args
   * @param $instance
   * @return mixed
   */
  public function getConvertedText($args, $instance) {
    $this->loadInstance($args, $instance);
    $this->buildText();
    return $this->text;
  }


  /**
   * Widget configuration form
   * @see WP_Widget::form()
   */
  public function form($instance) {

    VTCore_Wordpress_Utility::loadAsset('wp-bootstrap');

    $this->defaults = array(
      'title' => '',
      'text' => __('Cart %cart_total_amount <span class="total-product">%cart_total_product</span>', 'victheme_woocommerce'),
      'empty_text' => __('Cart empty <span class="total-product">0</span>', 'victheme_woocommerce'),
    );

    $this->instance = VTCore_Utility::arrayMergeRecursiveDistinct($instance, $this->defaults);

    $this
      ->buildForm()
      ->processForm()
      ->processError(true)
      ->render();
  }


  /**
   * Function for building the form object
   */
  private function buildForm() {

    $widget = new VTCore_Bootstrap_Form_BsInstance(array(
      'type' => false,
    ));

    $widget
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'text' => __('Title', 'victheme_woocommerce'),
        'name' => $this->get_field_name('title'),
        'id' => $this->get_field_id('title'),
        'value' => $this->instance['title'],
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsTextarea(array(
        'name' => $this->get_field_name('text'),
        'text' => __('Cart Text', 'victheme_woocommerce'),
        'input_elements' => array(
          'raw' => true,
        ),
        'description' => __('The text supports token system which will be replaced programmatically with dynamic value. </br>
                            Available token strings: <br />
                            <ul class="token-list">
                              <li><em><strong>%cart_total_amount</strong></em> : The total value of the cart</li>
                              <li><em><strong>%cart_total_product</strong></em> : The total product in the cart</li>
                              <li><em><strong>%cart_url</strong></em> : The cart page url</li>
                              <li><em><strong>%checkout_url</strong></em> : The checkout page url</li>
                            </ul>
                            ', 'victheme_woocommerce'),
        'value' => $this->instance['text'],
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsTextarea(array(
        'name' => $this->get_field_name('empty_text'),
        'text' => __('Cart Empty Text', 'victheme_woocommerce'),
        'input_elements' => array(
          'raw' => true,
        ),
        'description' => __('The text supports token system which will be replaced programmatically with dynamic value. </br>
                            Available token strings: <br />
                            <ul class="token-list">
                              <li><em><strong>%cart_total_amount</strong></em> : The total value of the cart</li>
                              <li><em><strong>%cart_total_product</strong></em> : The total product in the cart</li>
                              <li><em><strong>%cart_url</strong></em> : The cart page url</li>
                              <li><em><strong>%checkout_url</strong></em> : The checkout page url</li>
                            </ul>
                            ', 'victheme_woocommerce'),
        'value' => $this->instance['empty_text'],
      )));


    return $widget;
  }

}