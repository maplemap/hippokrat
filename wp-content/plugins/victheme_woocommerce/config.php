<?php
/**
 * Main Class for handling Headline default value
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_WooCommerce_Config
extends VTCore_Wordpress_Models_Config {

  protected $database = 'vtcore_woocommerce_config';
  protected $filter = 'vtcore_woocommerce_configuration_alter';

  /**
   * Child class must extend this method
   * to register their object information
   *
   * @param array $options
   * @return VTCore_Wordpress_Config_Base
   */
  protected function register(array $options) {

    // Set the default options
    $this->options = array(
      'related' => array(
        'posts_per_page' => 12,
        'fill' => true,
        'orderby' => 'rand',
        'order' => 'DESC',
      ),
      'upsell' => array(
        'posts_per_page' => 12,
        'fill' => true,
        'orderby' => 'rand',
        'order' => 'DESC',
      ),
      'cross' => array(
        'posts_per_page' => 3,
        'fill' => true,
        'orderby' => 'rand',
        'order' => 'DESC',
      ),
    );

    // Apply the hookable filter
    $this->filter();

    // Inject from database
    $this->load();

    // Merge the user supplied options
    $this->merge($options);

    return $this;
  }
}