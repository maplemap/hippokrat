<?php
/**
 * Hooking into Visual Composer Frontend iFrame
 * before it got initialized
 *
 * @author jason.xie@victheme.com
 */
class VTCore_WooCommerce_Actions_Vc__Load__IFrame__JsCss
extends VTCore_Wordpress_Models_Hook {

  public function hook() {
    VTCore_Wordpress_Utility::loadAsset('bootstrap');
    VTCore_Wordpress_Utility::loadAsset('wp-ajax');
    wp_localize_script('wp-ajax-wp-ajax.js', 'ajaxurl', admin_url('admin-ajax.php'));

    // Theme can override this.
    VTCore_Wordpress_Utility::loadAsset('woocatalog-front');

  }
}