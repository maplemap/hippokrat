<?php
/**
 * Hooking into Visual Composer Front Editor
 * for loading admin script
 *
 * @author jason.xie@victheme.com
 */
class VTCore_WooCommerce_Actions_Vc__Frontend__Editor__Enqueue__Js__Css
  extends VTCore_Wordpress_Models_Hook {

  public function hook() {
    VTCore_Wordpress_Utility::loadAsset('bootstrap');
    VTCore_Wordpress_Utility::loadAsset('wp-ajax');
    wp_localize_script('wp-ajax-wp-ajax.js', 'ajaxurl', admin_url('admin-ajax.php'));

    // Theme can override this.
    VTCore_Wordpress_Utility::loadAsset('woocatalog-front');
  }
}