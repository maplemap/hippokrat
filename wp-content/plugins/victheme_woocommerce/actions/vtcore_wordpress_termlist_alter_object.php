<?php
/**
 * Altering the VTCore term list objects
 * This is invoked after the object is built, so
 * alter the object instead of the context array.
 * 
 * @see VTCore_Wordpress_Element_WpTermList::buildElement()
 * @author jason.xie@victheme.com
 *
 */
class VTCore_WooCommerce_Actions_VTCore__Wordpress__TermList__Alter__Object
extends VTCore_Wordpress_Models_Hook {

  public function hook($listObject = NULL) {

    /**
     * Injecting WooCommerce Product Category Image to each terms
     * and append empty span item for the term-all button.
     * 
     * This will be applied globally regardless of shortcode / widgets
     * To invoke this alteration, add custom.wooimage and custom.wooimagesize to
     * the WpTermList context.
     */
    if ($listObject->getContext('custom.wooimage')) {

      $listObject->addClass('with-image');

      foreach ($listObject->findChildren('objectType', 'VTCore_Html_HyperLink') as $linkObject) {

        $object = false;
        // All Button
        if ($linkObject->getData('term-all')) {
          $object = new VTCore_Html_Element(array(
            'type' => 'span',
            'attributes' => array(
              'class' => array(
                'term-all-image',
              ),
            ),
          ));
        }

        // Term link
        else if ($linkObject->getData('term-id')) {
          $thumbnail_id = get_woocommerce_term_meta($linkObject->getData('term-id'), 'thumbnail_id', true );
          if (is_numeric($thumbnail_id) && !empty($thumbnail_id)) {
            $object = new VTCore_Html_Image(array(
              'attributes' => array(
                'src' => wp_get_attachment_url( $thumbnail_id, $listObject->getContext('custom.wooimagesize')),
                'class' => array(
                  'term-image',
                ),
              ),
            ));
          }
        }

        if ($object) {
          $text = $linkObject->getText();
          $linkObject
            ->setText('')
            ->addChildren($object)
            ->addChildren(new VTCore_Html_Element(array(
              'type' => 'span',
              'text' => $text,
              'attributes' => array(
                'class' => array(
                  'term-name',
                ),
              ),
            )));
        }
      }
    }
    
  }
}