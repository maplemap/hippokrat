<?php

/**
 * Loading additional assets
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_WooCommerce_Actions_WooCommerce__Ajax__Added__To__Cart
  extends VTCore_Wordpress_Models_Hook {

  public function hook() {

    $fragments = array();

    // Updating mini carts
    $widgets = get_option('widget_vtcore_woocommerce_widgets_minicart');

    if (!empty($widgets)) {

      foreach ($widgets as $id => $instance) {
        if (!is_numeric($id)) {
          continue;
        }
        $object = new VTCore_WooCommerce_Widgets_MiniCart();
        $markupID = 'vtcore-mini-cart-vtcore_woocommerce_widgets_minicart-' . $id;
        $fragments['div#' . $markupID] = '<div id="' . $markupID . '">' . $object->getConvertedText(array(), $instance) . '</div>';
      }
    }


    if (!empty($fragments)) {
      $data = array(
        'fragments' => $fragments,
        'cart_hash' => WC()->cart->get_cart_for_session() ? md5(json_encode(WC()->cart->get_cart_for_session())) : '', WC()->cart->get_cart_for_session(),
      );

      wp_send_json($data);
    }

  }
}