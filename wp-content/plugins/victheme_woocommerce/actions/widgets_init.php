<?php
/**
 * Hooking into wordpress widget_init action for
 * registering woocommerce custom widgets.
 *
 * @author jason.xie@victheme.com
 */
class VTCore_WooCommerce_Actions_Widgets__Init
extends VTCore_Wordpress_Models_Hook {

  private $widgets = array(
    'minicart',
  );

  public function hook() {

    // Booting widgets
    foreach ($this->widgets as $key => $name) {
      $widget = 'VTCore_WooCommerce_Widgets_' . ucfirst($name);

      if (class_exists($widget, true)) {
        $object = new $widget();
        $object->registerWidget();
      }
    }
  }
}