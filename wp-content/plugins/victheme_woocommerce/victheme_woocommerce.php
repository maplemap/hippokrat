<?php
/*
Plugin Name: VicTheme WooCommerce
Plugin URI: http://victheme.com
Description: additional extra widget, shortcodes and integration to visual composer for woocommerce
Author: jason.xie@victheme.com
Version: 1.1.7
Author URI: http://victheme.com
*/

define('VTCORE_WOOCOMMERCE_CORE_VERSION', '1.7.0');

add_action('plugins_loaded', 'bootVicthemeWoocommerce', 13);

function bootVicthemeWoocommerce() {

  if (!(defined('VTCORE_VERSION') && version_compare(VTCORE_VERSION, VTCORE_WOOCOMMERCE_CORE_VERSION, '='))) {

    add_action('admin_notices', 'WoocommerceMissingCoreNotice');

    function WoocommerceMissingCoreNotice() {

      if (!defined('VTCORE_VERSION')) {
        $notice = __('VicTheme Woocommerce depends on VicTheme Core Plugin which is not activated or missing, Please enable it first before VicTheme Woocommerce can work properly.');
      }
      elseif (version_compare(VTCORE_VERSION, VTCORE_WOOCOMMERCE_CORE_VERSION, '!=')) {
        $notice = sprintf(__('VicTheme Woocommerce depends on VicTheme Core Plugin API version %s to operate properly.', 'victheme_woocommerce'), VTCORE_WOOCOMMERCE_CORE_VERSION);
      }

      if (isset($notice)) {
        echo '<div class="error""><p>' . $notice . '</p></div>';
      }

    }

    return;
  }


  // Check if we got woocommerce installed
  if (!class_exists('WooCommerce')) {

    add_action('admin_notices', 'WoocommerceMissingWooCommerceNotice');

    function WoocommerceMissingWooCommerceNotice() {
      echo '<div class="error""><p>' . __('VicTheme Woocommerce depends on WooCommerce Plugin which is not activated or missing, Please enable it first before VicTheme Woocommerce can work properly.') . '</p></div>';
    }

    return;
  }

  define('VTCORE_WOOCOMMERCE_PLUGIN_LOADED', true);
  define('VTCORE_WOOCOMMERCE_BASE_DIR', dirname(__FILE__));

  // Booting autoloader
  $autoloader = new VTCore_Autoloader('VTCore_WooCommerce', dirname(__FILE__));
  $autoloader->setRemovePath('vtcore' . DIRECTORY_SEPARATOR . 'woocommerce' . DIRECTORY_SEPARATOR);
  $autoloader->register();

  // Autoload translation for victheme woocommerce
  load_plugin_textdomain('victheme_woocommerce', false, 'victheme_woocommerce/languages');

  // Registering assets
  VTCore_Wordpress_Init::getFactory('assets')
    ->get('library')
    ->detect(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets', plugins_url('', __FILE__) . '/assets');

  // Registering Actions
  VTCore_Wordpress_Init::getFactory('actions')
    ->addPrefix('VTCore_WooCommerce_Actions_')
    ->addHooks(array(
      'vc_backend_editor_enqueue_js_css',
      'vc_frontend_editor_enqueue_js_css',
      'vc_load_iframe_jscss',
      'widgets_init',
      'woocommerce_ajax_added_to_cart',
      'vtcore_wordpress_termlist_alter_object',
    ))
    ->register();

  // Registering Filters
  VTCore_Wordpress_Init::getFactory('filters')
    ->addPrefix('VTCore_WooCommerce_Filters_')
    ->addHooks(array(
      'vtcore_register_shortcode_prefix',
      'vtcore_register_shortcode',
      'woocommerce_related_products_args',
      'woocommerce_upsell_products_args',
      'woocommerce_cross_sells_products_args',
    ))
    ->register();


  // Register to visual composer via VTCore Visual Composer Factory
  if (VTCore_Wordpress_Init::getFactory('visualcomposer')) {
    VTCore_Wordpress_Init::getFactory('visualcomposer')
      ->mapShortcode(array(
        'VTCore_WooCommerce_Composer_VTWooCatalog',
      ));
  }


  // Register the template directory to VTCore Template factory
  VTCore_Wordpress_Init::getFactory('template')
    ->register(dirname(__FILE__) .  DIRECTORY_SEPARATOR . 'templates', 'templates' . DIRECTORY_SEPARATOR . 'woocommerce');


}