<?php
/**
 * Class extending the Shortcodes base class
 * for building the WooCommerce advanced catalog
 *
 * how to use :
 *
 * [vtwoocatalog]
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_WooCommerce_Composer_VTWooCatalog
extends VTCore_Wordpress_Models_VC {



  public function registerVC() {

    $options = array(
      'name' => __('Catalog', 'victheme_woocommerce'),
      'description' => __('Catalog Display', 'victheme_woocommerce'),
      'base' => 'vtwoocatalog',
      'icon' => 'icon-wpb-woocommerce',
      'category' => __('WooCommerce', 'victheme_woocommerce'),
      'params' => array()
    );

    $options['params'][] = array(
      'heading' => __('Show Image', 'victheme_woocommerce'),
      'type' => 'dropdown',
      'param_name' => 'custom___image',
      'edit_field_class' => 'vc_col-xs-4',
      'admin_label' => true,
      'value' => array(
        __('-- Select --', 'victheme_woocommerce') => '',
        __('Show', 'victheme_woocommerce') => 'true',
        __('Hide', 'victheme_woocommerce') => 'false',
      ),
    );

    $options['params'][] = array(
      'heading' => __('Show Title', 'victheme_woocommerce'),
      'type' => 'dropdown',
      'param_name' => 'custom___title',
      'edit_field_class' => 'vc_col-xs-4',
      'admin_label' => true,
      'value' => array(
        __('-- Select --', 'victheme_woocommerce') => '',
        __('Show', 'victheme_woocommerce') => 'true',
        __('Hide', 'victheme_woocommerce') => 'false',
      ),
    );

    $options['params'][] = array(
      'heading' => __('Show Rating', 'victheme_woocommerce'),
      'type' => 'dropdown',
      'param_name' => 'custom___rating',
      'edit_field_class' => 'vc_col-xs-4',
      'admin_label' => true,
      'value' => array(
        __('-- Select --', 'victheme_woocommerce') => '',
        __('Show', 'victheme_woocommerce') => 'true',
        __('Hide', 'victheme_woocommerce') => 'false',
      ),
    );

    $options['params'][] = array(
      'heading' => __('Show Price', 'victheme_woocommerce'),
      'type' => 'dropdown',
      'param_name' => 'custom___price',
      'edit_field_class' => 'vc_col-xs-4',
      'admin_label' => true,
      'value' => array(
        __('-- Select --', 'victheme_woocommerce') => '',
        __('Show', 'victheme_woocommerce') => 'true',
        __('Hide', 'victheme_woocommerce') => 'false',
      ),
    );

    $options['params'][] = array(
      'heading' => __('Show Cart Button', 'victheme_woocommerce'),
      'type' => 'dropdown',
      'param_name' => 'custom___cart',
      'edit_field_class' => 'vc_col-xs-4',
      'admin_label' => true,
      'default' => true,
      'value' => array(
        __('-- Select --', 'victheme_woocommerce') => '',
        __('Show', 'victheme_woocommerce') => 'true',
        __('Hide', 'victheme_woocommerce') => 'false',
      ),
    );


    $options['params'][] = array(
      'heading' => __('Show Flash Banner', 'victheme_woocommerce'),
      'type' => 'dropdown',
      'param_name' => 'custom___flash',
      'edit_field_class' => 'vc_col-xs-4',
      'admin_label' => true,
      'value' => array(
        __('-- Select --', 'victheme_woocommerce') => '',
        __('Show', 'victheme_woocommerce') => 'true',
        __('Hide', 'victheme_woocommerce') => 'false',
      ),
    );

    $options['params'][] = array(
      'heading' => __('Enable Ajax', 'victheme_woocommerce'),
      'type' => 'dropdown',
      'param_name' => 'ajax',
      'edit_field_class' => 'vc_col-xs-4',
      'admin_label' => true,
      'value' => array(
        __('-- Select --', 'victheme_woocommerce') => '',
        __('Enable', 'victheme_woocommerce') => 'true',
        __('Disable', 'victheme_woocommerce') => 'false',
      ),
    );



    $keys = array('columns', 'offset', 'push', 'pull');
    $sizes = array('mobile', 'tablet', 'small', 'large');

    $options['params'][] = array(
      'heading' => __('Ignore Sticky Post', 'victheme_woocommerce'),
      'type' => 'dropdown',
      'param_name' => 'query___ignore_sticky_post',
      'admin_label' => true,
      'edit_field_class' => 'vc_col-xs-6',
      'value' => array(
        __('-- Select --', 'victheme_woocommerce') => '',
        __('Enable', 'victheme_woocommerce') => 'true',
        __('Disable', 'victheme_woocommerce') => 'false',
      ),
      'group' => __('Query', 'victheme_woocommerce'),
    );

    $options['params'][] = array(
      'type' => 'textfield',
      'heading' => __('Taxonomy Terms', 'victheme_woocommerce'),
      'description' => __('Filter query by product category taxonomy terms, separate taxonomy terms from product category by comma.', 'victheme_woocommerce'),
      'param_name' => 'query___terms',
      'edit_field_class' => 'vc_col-xs-6',
      'admin_label' => true,
      'value' => '',
      'group' => __('Query', 'victheme_woocommerce'),
    );

    $options['params'][] = array(
      'type' => 'textfield',
      'heading' => __('Include only Product', 'victheme_woocommerce'),
      'description' => __('Filter query by including only product with these product id, separate multiple id by comma.', 'victheme_woocommerce'),
      'param_name' => 'query___post__in',
      'edit_field_class' => 'vc_col-xs-6',
      'admin_label' => true,
      'value' => '',
      'group' => __('Query', 'victheme_woocommerce'),
    );

    $options['params'][] = array(
      'type' => 'textfield',
      'heading' => __('Exclude only Product', 'victheme_woocommerce'),
      'description' => __('Filter query by excluding only product with these product id, separate multiple id by comma.', 'victheme_woocommerce'),
      'param_name' => 'query___post__not_in',
      'edit_field_class' => 'vc_col-xs-6',
      'admin_label' => true,
      'group' => __('Query', 'victheme_woocommerce'),
    );


    $options['params'][] = array(
      'type' => 'dropdown',
      'heading' => __('Product Status', 'victheme_woocommerce'),
      'description' => __('Filter query by applying specific product status only.', 'victheme_woocommerce'),
      'param_name' => 'query___post_status',
      'edit_field_class' => 'vc_col-xs-6',
      'admin_label' => true,
      'value' => array(
        __('-- Select --', 'victheme_woocommerce') => '',
        __('Published', 'victheme_woocommerce') => 'publish',
        __('Pending', 'victheme_woocommerce') => 'pending',
        __('Draft', 'victheme_woocommerce') => 'draft',
        __('Future', 'victheme_woocommerce') => 'future',
        __('Any', 'victheme_woocommerce') => 'any',
        __('Trash', 'victheme_woocommerce') => 'trash',
        __('Private', 'victheme_woocommerce') => 'private',
      ),
      'group' => __('Query', 'victheme_woocommerce'),
    );

    $options['params'][] = array(
      'type' => 'textfield',
      'heading' => __('Product per page', 'victheme_woocommerce'),
      'description' => __('Show number of product per page', 'victheme_woocommerce'),
      'param_name' => 'query___posts_per_page',
      'edit_field_class' => 'vc_col-xs-6',
      'admin_label' => true,
      'value' => '',
      'group' => __('Query', 'victheme_woocommerce'),
    );

    $options['params'][] = array(
      'type' => 'textfield',
      'heading' => __('Offset', 'victheme_woocommerce'),
      'description' => __('Offset number of product before start loop', 'victheme_woocommerce'),
      'param_name' => 'query___offset',
      'edit_field_class' => 'vc_col-xs-6',
      'admin_label' => true,
      'value' => '',
      'group' => __('Query', 'victheme_woocommerce'),
    );

    $options['params'][] = array(
      'type' => 'dropdown',
      'heading' => __('Order', 'victheme_woocommerce'),
      'description' => __('Ordering direction.', 'victheme_woocommerce'),
      'param_name' => 'query___order',
      'edit_field_class' => 'vc_col-xs-6',
      'admin_label' => true,
      'value' => array(
        __('-- Select --', 'victheme_woocommerce') => '',
        __('Ascending', 'victheme_woocommerce') => 'ASC',
        __('Descending', 'victheme_woocommerce') => 'DESC',
      ),
      'group' => __('Query', 'victheme_woocommerce'),
    );

    $options['params'][] = array(
      'type' => 'dropdown',
      'heading' => __('Order By', 'victheme_woocommerce'),
      'description' => __('Order the query by specified value.', 'victheme_woocommerce'),
      'param_name' => 'query___orderby',
      'edit_field_class' => 'vc_col-xs-6',
      'admin_label' => true,
      'value' => array(
        __('-- Select --', 'victheme_woocommerce') => '',
        __('None', 'victheme_woocommerce') => 'none',
        __('Product ID', 'victheme_woocommerce') => 'ID',
        __('Product author', 'victheme_woocommerce') =>'author',
        __('Product Title','victheme_woocommerce') => 'title',
        __('Product Creation Date', 'victheme_woocommerce') => 'date',
        __('Product Modified Date','victheme_woocommerce') => 'modified',
        __('Random Order', 'victheme_woocommerce') => 'rand',
        __('Product Review Count','victheme_woocommerce') => 'comment_count',
      ),
      'group' => __('Query', 'victheme_woocommerce'),
    );


    $options['params'][] = array(
      'heading' => __('Enable Pager', 'victheme_woocommerce'),
      'type' => 'dropdown',
      'param_name' => 'pager___enable',
      'edit_field_class' => 'vc_col-xs-3',
      'admin_label' => true,
      'value' => array(
        __('-- Select --', 'victheme_woocommerce') => '',
        __('Enable', 'victheme_woocommerce') => 'true',
        __('Disable', 'victheme_woocommerce') => 'false',
      ),
      'group' => __('Pager', 'victheme_woocommerce'),
    );

    $options['params'][] = array(
      'heading' => __('Mini Pager', 'victheme_woocommerce'),
      'type' => 'dropdown',
      'param_name' => 'pager___mini',
      'edit_field_class' => 'vc_col-xs-3',
      'admin_label' => true,
      'value' => array(
        __('-- Select --', 'victheme_woocommerce') => '',
        __('Enable', 'victheme_woocommerce') => 'true',
        __('Disable', 'victheme_woocommerce') => 'false',
      ),
      'group' => __('Pager', 'victheme_woocommerce'),
    );

    $options['params'][] = array(
      'heading' => __('Infinite Pager', 'victheme_woocommerce'),
      'type' => 'dropdown',
      'param_name' => 'pager___infinite',
      'edit_field_class' => 'vc_col-xs-3',
      'admin_label' => true,
      'value' => array(
        __('-- Select --', 'victheme_woocommerce') => '',
        __('Enable', 'victheme_woocommerce') => 'true',
        __('Disable', 'victheme_woocommerce') => 'false',
      ),
      'group' => __('Pager', 'victheme_woocommerce'),
    );

    $options['params'][] = array(
      'heading' => __('Prev and next', 'victheme_woocommerce'),
      'type' => 'dropdown',
      'param_name' => 'pager___prev_next',
      'edit_field_class' => 'vc_col-xs-3',
      'admin_label' => true,
      'value' => array(
        __('-- Select --', 'victheme_woocommerce') => '',
        __('Enable', 'victheme_woocommerce') => 'true',
        __('Disable', 'victheme_woocommerce') => 'false',
      ),
      'group' => __('Pager', 'victheme_woocommerce'),
    );


    $options['params'][] = array(
      'type' => 'textfield',
      'heading' => __('Pager End Size', 'victheme_woocommerce'),
      'description' => __('Number of pagination shown before replacing wiht ...', 'victheme_woocommerce'),
      'param_name' => 'pager___end_size',
      'edit_field_class' => 'vc_col-xs-4 clearboth',
      'admin_label' => true,
      'value' => '4',
      'group' => __('Pager', 'victheme_woocommerce'),
    );

    $options['params'][] = array(
      'type' => 'textfield',
      'heading' => __('Pager Prev Text', 'victheme_woocommerce'),
      'description' => __('Text for the prev button.', 'victheme_woocommerce'),
      'param_name' => 'pager___prev_text',
      'edit_field_class' => 'vc_col-xs-4',
      'admin_label' => true,
      'value' => '&laquo',
      'group' => __('Pager', 'victheme_woocommerce'),
    );

    $options['params'][] = array(
      'type' => 'textfield',
      'heading' => __('Pager Next Text', 'victheme_woocommerce'),
      'description' => __('Text for the next button.', 'victheme_woocommerce'),
      'param_name' => 'pager___next_text',
      'edit_field_class' => 'vc_col-xs-4',
      'admin_label' => true,
      'value' => '&raquo',
      'group' => __('Pager', 'victheme_woocommerce'),
    );

    $options['params'][] = array(
      'type' => 'dropdown',
      'heading' => __('Layout Style', 'victheme_woocommerce'),
      'param_name' => 'isotope___layoutmode',
      'edit_field_class' => 'vc_col-xs-6',
      'admin_label' => true,
      'value' => apply_filters('vtcore_woocommerce_alter_layoutmode', array(
        __('-- Select --', 'victheme_woocommerce') => '',
        __('Masonry', 'victheme_woocommerce') => 'masonry',
        __('FitRows', 'victheme_woocommerce') => 'fitRows',
      )),
      'group' => __('Layout', 'victheme_woocommerce'),
    );


    $options['params'][] = array(
      'heading' => __('Enable equalheight on fitRows layout', 'victheme_woocommerce'),
      'type' => 'dropdown',
      'param_name' => 'isotope___equalheight',
      'edit_field_class' => 'vc_col-xs-6',
      'admin_label' => true,
      'value' => array(
        __('-- Select --', 'victheme_woocommerce') => '',
        __('Enable', 'victheme_woocommerce') => 'true',
        __('Disable', 'victheme_woocommerce') => 'false',
      ),
      'group' => __('Layout', 'victheme_woocommerce'),
    );


    foreach ($keys as $key) {
      foreach ($sizes as $size) {

        $name =  'grids___' . $key . '___' . $size;

        $value = 0;
        if ($key == 'columns') {
          $value = 12;
        }

        $options['params'][] = array(
          'type' => 'vtcore',
          'param_name' => $name,
          'name' => $name,
          'group' => __('Layout', 'victheme_woocommerce'),
          'core_class' => 'VTCore_Bootstrap_Form_BsSelect',
          'edit_field_class' => 'col-md-3',
          'core_context' => array(
            'text' => ucfirst($key) . ' ' . ucfirst($size),
            'name' => $name,
            'value' => $value,
            'input_elements' => array(
              'attributes' => array(
                'class' => array('wpb_vc_param_value', 'wpb-dropdown', 'icon', 'vtcore_field')
              ),
            ),
            'options' => range(0, 12, 1),
          ),
        );
      }
    }

    $options['params'][] = array(
      'heading' => __('Enable terms filtering', 'victheme_woocommerce'),
      'description' => __('Main switch to enable or disable the product term filtering feature.', 'victheme_woocommerce'),
      'type' => 'dropdown',
      'param_name' => 'terms___enable',
      'edit_field_class' => 'vc_col-xs-4',
      'admin_label' => true,
      'value' => array(
        __('-- Select --', 'victheme_woocommerce') => '',
        __('Enable', 'victheme_woocommerce') => 'true',
        __('Disable', 'victheme_woocommerce') => 'false',
      ),
      'group' => __('Filters', 'victheme_woocommerce'),
    );

    $options['params'][] = array(
      'heading' => __('Use taxonomy terms', 'victheme_woocommerce'),
      'description' => __('The link element will use product category as the filtering source when enabled.', 'victheme_woocommerce'),
      'type' => 'dropdown',
      'param_name' => 'terms___taxonomy',
      'edit_field_class' => 'vc_col-xs-4',
      'admin_label' => true,
      'value' => array(
        __('-- Select --', 'victheme_woocommerce') => '',
        __('Enable', 'victheme_woocommerce') => 'true',
        __('Disable', 'victheme_woocommerce') => 'false',
      ),
      'group' => __('Filters', 'victheme_woocommerce'),
    );

    $options['params'][] = array(
      'heading' => __('Enable redirect on click', 'victheme_woocommerce'),
      'description' => __('The filter element will redirect to taxonomy archive page when no ajax enabled.', 'victheme_woocommerce'),
      'type' => 'dropdown',
      'param_name' => 'terms___redirect',
      'edit_field_class' => 'vc_col-xs-4',
      'admin_label' => true,
      'value' => array(
        __('-- Select --', 'victheme_woocommerce') => '',
        __('Enable', 'victheme_woocommerce') => 'true',
        __('Disable', 'victheme_woocommerce') => 'false',
      ),
      'group' => __('Filters', 'victheme_woocommerce'),
    );

    $options['params'][] = array(
      'heading' => __('Enable terms drilling', 'victheme_woocommerce'),
      'description' => __('If enabled the filter will drill down to child terms when activated', 'victheme_woocommerce'),
      'type' => 'dropdown',
      'param_name' => 'terms___drill',
      'edit_field_class' => 'vc_col-xs-4 clearboth',
      'admin_label' => true,
      'value' => array(
        __('-- Select --', 'victheme_woocommerce') => '',
        __('Enable', 'victheme_woocommerce') => 'true',
        __('Disable', 'victheme_woocommerce') => 'false',
      ),
      'group' => __('Filters', 'victheme_woocommerce'),
    );

    $options['params'][] = array(
      'heading' => __('Show parent terms', 'victheme_woocommerce'),
      'description' => __('Show the parent terms when child term is active, only applicable if drilling mode is enabled', 'victheme_woocommerce'),
      'type' => 'dropdown',
      'param_name' => 'terms___parent',
      'edit_field_class' => 'vc_col-xs-4',
      'admin_label' => true,
      'value' => array(
        __('-- Select --', 'victheme_woocommerce') => '',
        __('Enable', 'victheme_woocommerce') => 'true',
        __('Disable', 'victheme_woocommerce') => 'false',
      ),
      'group' => __('Filters', 'victheme_woocommerce'),
    );

    $options['params'][] = array(
      'type' => 'textfield',
      'heading' => __('All Button', 'victheme_woocommerce'),
      'description' => __('The text for show all button, only works under taxonomy filtering mode, leave empty to disable.', 'victheme_woocommerce'),
      'param_name' => 'terms___all',
      'edit_field_class' => 'vc_col-xs-4',
      'admin_label' => true,
      'value' => __('All', 'victheme_woocommerce'),
      'group' => __('Filters', 'victheme_woocommerce'),
    );

    $options['params'][] = array(
      'type' => 'dropdown',
      'heading' => __('Term Image', 'victheme_woocommerce'),
      'description' => __('Build the WooCommerce attached term image.', 'victheme_woocommerce'),
      'param_name' => 'terms___custom___wooimage',
      'edit_field_class' => 'vc_col-xs-4 clearboth',
      'admin_label' => true,
      'value' => array(
        __('-- Select --', 'victheme_woocommerce') => '',
        __('Enable', 'victheme_woocommerce') => 'true',
        __('Disable', 'victheme_woocommerce') => 'false',
      ),
      'group' => __('Filters', 'victheme_woocommerce'),
    );

    $imageSizes = array();
    foreach (get_intermediate_image_sizes() as $size) {
      $imageSizes[$size] = $size;
    }

    $imageSizes['full'] = 'full';

    $options['params'][] = array(
      'type' => 'dropdown',
      'heading' => __('Term Image Size', 'victheme_woocommerce'),
      'description' => __('The image size to use for the term image', 'victheme_woocommerce'),
      'param_name' => 'terms___custom___wooimagesize',
      'edit_field_class' => 'vc_col-xs-4',
      'admin_label' => true,
      'value' => $imageSizes,
      'group' => __('Filters', 'victheme_woocommerce'),
    );


    $options['params'][] = array(
      'type' => 'textfield',
      'heading' => __('Element Title', 'victheme_woocommerce'),
      'description' => __('Text for the filter element box title.', 'victheme_woocommerce'),
      'param_name' => 'terms___title',
      'edit_field_class' => 'vc_col-xs-4',
      'admin_label' => true,
      'value' => '',
      'group' => __('Filters', 'victheme_woocommerce'),
    );

    $menus = get_registered_nav_menus();
    $menu = array();
    foreach ($menus as $location => $description) {
      $menu[$description] = $location;
    }

    $options['params'][] = array(
      'type' => 'dropdown',
      'heading' => __('Menu Source', 'victheme_woocommerce'),
      'description' => __('Use Menu instead of taxonomy terms for filtering, the use taxonomy options must unchecked for this to work.', 'victheme_woocommerce'),
      'param_name' => 'terms___menu',
      'edit_field_class' => 'vc_col-xs-6  clearboth',
      'admin_label' => true,
      'value' => $menu,
      'group' => __('Filters', 'victheme_woocommerce'),
    );

    $options['params'][] = array(
      'type' => 'dropdown',
      'heading' => __('Filter Element Position', 'victheme_woocommerce'),
      'description' => __('Define the location position for the filtering element. If configured as inside, please use the grid to adjust the position by using offset and grid size', 'victheme_woocommerce'),
      'param_name' => 'terms___position',
      'edit_field_class' => 'vc_col-xs-6',
      'admin_label' => true,
      'value' => array(
        __('-- Select --', 'victheme_woocommerce') => '',
        __('Inside', 'victheme_woocommerce') => 'inside',
        __('Top', 'victheme_woocommerce') => 'top',
        __('Left', 'victheme_woocommerce') => 'left',
        __('Right', 'victheme_woocommerce') => 'right',
        __('Bottom', 'victheme_woocommerce') => 'bottom',
      ),
      'group' => __('Filters', 'victheme_woocommerce'),
    );

    foreach ($keys as $key) {
      foreach ($sizes as $size) {

        $name =  'terms___grids___' . $key . '___' . $size;

        $value = 0;
        if ($key == 'columns') {
          $value = 12;
        }

        $class = '';
        if ($size == 'mobile') {
          $class = 'clearboth';
        }

        $options['params'][] = array(
          'type' => 'vtcore',
          'param_name' => $name,
          'name' => $name,
          'group' => __('Filters', 'victheme_woocommerce'),
          'core_class' => 'VTCore_Bootstrap_Form_BsSelect',
          'edit_field_class' => 'col-md-3',
          'core_context' => array(
            'text' => ucfirst($key) . ' ' . ucfirst($size),
            'name' => $name,
            'value' => $value,
            'attributes' => array(
              'class' => array(
                $class
              ),
            ),
            'input_elements' => array(
              'attributes' => array(
                'class' => array('wpb_vc_param_value', 'wpb-dropdown', 'icon', 'vtcore_field',)
              ),
            ),
            'options' => range(0, 12, 1),
          ),
        );
      }
    }


    $options['params'][] = array(
      'type' => 'textfield',
      'heading' => __('ID', 'victheme_woocommerce'),
      'param_name' => 'attributes_id',
      'admin_label' => true,
    );


    $options['params'][] = array(
      'type' => 'textfield',
      'heading' => __('Class', 'victheme_woocommerce'),
      'param_name' => 'class',
      'admin_label' => true,
    );


    $options['params'][] = array(
      'type' => 'css_editor',
      'heading' => __('Css', 'victheme_woocommerce'),
      'param_name' => 'css',
      'group' => __('Design options', 'victheme_woocommerce')
    );

    return $options;
  }
}


class WPBakeryShortCode_VTWooCatalog extends WPBakeryShortCode {}