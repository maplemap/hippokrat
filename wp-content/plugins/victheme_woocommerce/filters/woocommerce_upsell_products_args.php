<?php
/**
 * Hooking into vtcore_register_shortcode filter
 * to register woocommerce related shortcodes
 *
 * @author jason.xie@victheme.com
 */
class VTCore_WooCommerce_Filters_WooCommerce__Upsell__Products__Args
extends VTCore_Wordpress_Models_Hook {

  protected $blank;
  protected $args;
  protected $config;
  protected $additionalArgs;
  protected $query;
  protected $rendered;
  protected $product;


  public function hook($args = NULL) {

    global $product;

    $this->product = $product;

    if (!isset($this->product->VTWC_rendered)) {
      $this->product->VTWC_rendered = array();
    }

    $this->args = $args;
    $this->config = new VTCore_WooCommerce_Config();

    $this->args['posts_per_page'] = (int) $this->config->get('upsell.posts_per_page');
    $this->args['orderby'] = $this->config->get('upsell.orderby');
    $this->args['order'] = $this->config->get('upsell.order');
    $this->args['post__not_in'] = array_merge((array) $this->product->VTWC_rendered, (array) $this->args['post__not_in']);

    $this->blank = $this->args['posts_per_page'] - count($this->args['post__in']);

    // We need to fill the blanks?
    if ($this->config->get('upsell.fill')
        && $this->blank > 0) {

      $this->fetchAdditionalPost();

    }

    $this->product->VTWC_rendered = array_merge($this->product->VTWC_rendered, $this->args['post__in']);

    return $this->args;
  }


  /**
   * Method for fetching additional posts to fill in the blanks
   * @return $this
   */
  protected function fetchAdditionalPost() {


    $this->additionalArgs = array(
      'post_type' => 'product',
      'posts_per_page' => (int) $this->blank,
      'post_status' => 'published',
      'post__not_in' => array_merge(
        $this->args['post__in'],
        $this->args['post__not_in'],
        $this->product->get_related()
      ),
      'ignore_sticky_posts' => 1,
      'orderby' => 'rand',
      'order' => 'DESC',
      'fields' => 'ids',
      'meta_query' => array(
        array(
          'key' => '_visibility',
          'value' => 'visible',
          'compare' => '=',
        ),
      ),
    );

    if (get_option( 'woocommerce_hide_out_of_stock_items' ) == 'yes') {
      $this->additionalArgs['meta_query'][] = array(
        'relation' => 'AND',
        'key' => '_stock_status',
        'value' => 'instock',
        'compare' => '='
      );
    }

    $this->query = new WP_Query($this->additionalArgs);
    foreach ($this->query->get_posts() as $id) {
      $this->args['post__in'][] = $id;
    }

    return $this;
  }
}