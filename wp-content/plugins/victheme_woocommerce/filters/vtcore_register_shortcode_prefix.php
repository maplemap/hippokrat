<?php
/**
 * Hooking into vtcore_register_shortcode_prefix filter
 * to register woocommerce related shortcodes
 *
 * @author jason.xie@victheme.com
 */
class VTCore_WooCommerce_Filters_VTCore__Register__Shortcode__Prefix
extends VTCore_Wordpress_Models_Hook {

  public function hook($prefix = NULL) {

    $prefix[] = 'VTCore_WooCommerce_Shortcodes_';

    return $prefix;
  }
}