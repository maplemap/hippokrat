<?php
/**
 * Hooking into vtcore_register_shortcode filter
 * to register woocommerce related shortcodes
 *
 * @author jason.xie@victheme.com
 */
class VTCore_WooCommerce_Filters_VTCore__Register__Shortcode
extends VTCore_Wordpress_Models_Hook {

  public function hook($shortcodes = NULL) {

    $shortcodes[] = 'vtwoocatalog';

    return $shortcodes;
  }
}