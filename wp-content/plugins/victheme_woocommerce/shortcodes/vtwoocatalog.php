<?php
/**
 * Class extending the Shortcodes base class
 * for building the Slick Carousel Container
 *
 * Note : don't use . as VC doesnt support dot
 *        in attribute name
 *
 * how to use :
 *
 * [vtwoocatalog
 *
 *   // Ajax
 *   ajax="true"
 *
 *   // CSS
 *   id="some css id"
 *   class="some css class"
 *
 *   // Grids
 *   grids___columns___mobile="X"
 *   grids___columns___tablet="X"
 *   grids___columns___small="X"
 *   grids___columns___large="X"
 *   grids___offset___mobile="X"
 *   grids___offset___tablet="X"
 *   grids___offset___small="X"
 *   grids___offset___large="X"
 *   grids___push___mobile="X"
 *   grids___push___tablet="X"
 *   grids___push___small="X"
 *   grids___push___large="X"
 *   grids___pull___mobile="X"
 *   grids___pull___tablet="X"
 *   grids___pull___small="X"
 *   grids___pull___large="X"
 *
 *   // Query
 *   // @see wp_query args
 *   query___terms="some terms separated by comma"
 *   query___post__in="some post id separated by comma"
 *   query___post__not_in="some post id separated by comma"
 *   query___post_status="some valid post status"
 *   query___posts_per_page="some number"
 *   query___offset="offsetting post number"
 *   query___ignore_sticky_post="true"
 *   query___order="ASC|DESC"
 *   query___orderby="some valid order by"
 *
 *   // Pagination
 *   pager___enable="true"
 *   pager___mini="true"
 *   pager___infinite="true"
 *   pager___prev_next="true"
 *   pager___end_size="4"
 *   pager___prev_text="&laquo"
 *   pager___next_text="&raquo"
 *
 *   // Isotope
 *   isotope___layoutmode="masonry|fitRows"
 *   isotope___equalheight="true"
 *
 *   // Terms
 *   terms___enable="true"
 *   terms___all="some text for all button or false to disable"
 *   terms___taxonomy="true"
 *   terms___menu="some menu id"
 *   terms___title => "some text for menu title"
 *   terms___redirect="true"
 *   terms___parent="true"
 *   terms___drill="false"
 *   terms___position="top|left|right|bottom|inside"
 *   terms___grids___columns___mobile="X"
 *   terms___grids___columns___tablet="X"
 *   terms___grids___columns___small="X"
 *   terms___grids___columns___large="X"
 *   terms___grids___offset___mobile="X"
 *   terms___grids___offset___tablet="X"
 *   terms___grids___offset___small="X"
 *   terms___grids___offset___large="X"
 *   terms___grids___push___mobile="X"
 *   terms___grids___push___tablet="X"
 *   terms___grids___push___small="X"
 *   terms___grids___push___large="X"
 *   terms___grids___pull___mobile="X"
 *   terms___grids___pull___tablet="X"
 *   terms___grids___pull___small="X"
 *   terms___grids___pull___large="X"
 *
 *   // Custom variable for template use
 *   custom___image="true"
 *   custom___title="true"
 *   custom___rating="true"
 *   custom___price="true"
 *   custom___cart="true"
 *   custom___flash="true"
 *
 *   // Term image
 *   terms___custom___wooimage = "true or false"
 *   terms___custom___wooimagesize = "valid image size"
 *
 * ]
 *
 * Note : if you use the shortcode directly without
 * visual composer, you can change ___ to . or #
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_WooCommerce_Shortcodes_VTWooCatalog
extends VTCore_Wordpress_Models_Shortcodes
implements VTCore_Wordpress_Interfaces_Shortcodes {

  protected $processDottedNotation = true;

  protected $booleans = array(
    'ajax',
    'custom.image',
    'custom.title',
    'custom.rating',
    'custom.price',
    'custom.cart',
    'custom.flash',
    'terms.image',
    'terms.redirect',
    'terms.parent',
    'terms.drill',
    'terms.enable',
    'terms.taxonomy',
    'pager.enable',
    'pager.mini',
    'pager.infinite',
    'pager.prev_next',
    'query.ignore_sticky_post',
    'isotope.equalheight',
  );




  /**
   * Parsing atts to valid query args for taxonomy
   * @return VTCore_WooCatalog_Shortcodes_VTWooCatalog
   */
  protected function queryArgsTaxonomy() {

    if ($this->get('query.terms')) {
      $terms = explode(',', $this->get('query.terms'));

      $query = array(
        'relation' => 'OR',
      );

      $slugs = array();
      $ids = array();

      foreach ($terms as $term) {
        $term = trim($term);
        if (is_numeric($term)) {
          $ids[] = $term;
        }
        else {
          $slugs[] = $term;
        }
      }

      if (!empty($slugs)) {
        $query[] = array(
          'taxonomy' => 'product_cat',
          'field' => 'slug',
          'terms' => $slugs,
        );
      }

      if (!empty($ids)) {
        $query[] = array(
          'taxonomy' => 'product_cat',
          'field' => 'id',
          'terms' => $ids,
        );
      }

      if (count($query) > 1) {
        $this->add('queryArgs.tax_query', $query);
      }

      $this->remove('query.terms');
    }

    return $this;
  }




  /**
   * Parsing atts and create the valid wploop
   * isotope options.
   *
   * @return VTCore_WooCatalog_Shortcodes_VTWooCatalog
   */
  protected function isotopeArgs() {

    // Masonry mode
    if ($this->get('isotope.layoutmode') == 'masonry') {
      $this->add('data.isotope-options',array(
        'itemSelector' => '.product-item',
        'layoutMode' => 'masonry'
      ));
    }

    // Menu inside build using packery
    if ($this->get('terms.enable')
        && $this->get('terms.position') == 'inside') {

       $this->add('data.isotope-options', array(
         'itemSelector' => '.product-item',
         'stamp' => '.catalog-menu',
         'layoutMode' => 'packery',
       ));
    }


    // Fitrow Mode
    if ($this->get('isotope.layoutmode') == 'fitRows') {
      $this->add('data.isotope-options', array(
        'itemSelector' => '.product-item',
        'layoutMode' => 'fitRows',
        'fitRows' => array(
          'gutter' => array(
            'width' => 0,
            'height' => 0,
          ),
          'equalheight' => $this->get('isotope.equalheight'),
        ),
      ));
    }

    $this->remove('isotope');

    return $this;

  }


  /**
   * Overriding parent method to preprocess args
   * @see VTCore_Wordpress_Models_Shortcodes::processCustomRules()
   */
  protected function processCustomRules() {

    // Build taxonomy args
    $this->queryArgsTaxonomy();

    // Build isotope args
    $this->isotopeArgs();

    $uniq = new VTCore_Uid();

    // Build loop query
    $this->add('id', 'vtwoocommerceloop-' . $uniq->getID());
    $this->add('queryArgs.post_type', 'product');
    $this->add('queryMain', false);

    foreach ($this->get('query') as $key => $value) {

      if (empty($value)) {
        continue;
      }

      if ($key == 'post__in'
          || $key == 'post__not_in'
          || $key == 'post_parent__in'
          || $key == 'post_parent__not-in') {

        $value = explode(',', $value);
      }

      $this->add('queryArgs.' . $key, $value);
    }

    $this->add('query', false);

    // Build template args
    // Theme can overide this template by using the template factory
    $this->add('template.items', VTCore_Wordpress_Init::getFactory('template')->locate('product-item.php'));
    $this->add('template.empty', VTCore_Wordpress_Init::getFactory('template')->locate('product-empty.php'));
    $this->add('template.mode', $this->get('data.isotope-options.layoutMode'));

    // Additional classes
    $this->add('attributes.class.id', $this->get('queryArgs.id'));
    $this->add('attributes.class.template', 'template-shop-' . $this->get('data.isotope-options.layoutMode'));
    $this->add('attributes.class.clearfix', 'clearfix');
    $this->add('attributes.class.clearboth', 'clearboth');

    // Force ajax mode
    if ($this->get('pager.infinite')) {
      $this->add('ajax', true);
    }

    // Ajax args
    $this->add('ajaxData.ajax-queue.0', 'append');

    // Pager args
    if ($this->get('pager.enable')) {
      $this->add('pager.ajax', $this->get('ajax'));
      $this->add('pager.id', $this->get('id'));
    }

    // Terms list args
    if ($this->get('terms.enable')) {
      $this->add('terms.ajax', $this->get('ajax'));
      $this->add('terms.id', $this->get('id'));

      if ($this->get('terms.all') == '') {
        $this->add('terms.all', false);
      }
    }

    return $this;
  }


  public function buildObject() {

    // Load the assets
    VTCore_Wordpress_Utility::loadAsset('woocommerce-front');

    // Build the main wrapper
    $this->object = new VTCore_Bootstrap_Element_BsElement(array(
      'type' => 'div',
      'attributes' => array(
        'class' => array(
          'vtwoocatalog-' . $this->get('queryArgs.id'),
          'shop-items-wrapper',
          'clearboth',
          'shop-catalog',
          'woocommerce',
        ),
      ),
    ));


    // this has to be injected early
    // so it will be carried forward on ajax
    if ($this->get('terms.enable')
        && ($this->get('terms.position') == 'left'
        || $this->get('terms.position') == 'right')) {

      $grid = new VTCore_Bootstrap_Grid_Column(array(
        'columns' => array(
          'mobile' => 12 - $this->get('terms.grids.columns.mobile'),
          'tablet' => 12 - $this->get('terms.grids.columns.tablet'),
          'small' => 12 - $this->get('terms.grids.columns.small'),
          'large' => 12 - $this->get('terms.grids.columns.large'),
        ),
      ));

      $this->add('attributes.class.grid', $grid->getClass());
    }


    // Build the loop object
    $loopObject = new VTCore_Wordpress_Element_WpLoop($this->extract());


    // Build the terms selector
    if ($this->get('terms.enable')) {

      $menuObject = new VTCore_Bootstrap_Element_BsElement(array(
        'type' => 'div',
        'attributes' => array(
          'class' => array(
            'column',
            'catalog-menu',
            'clearfix'
          ),
        ),
        'raw' => true,
        'grids' => $this->get('terms.grids'),
      ));


      $menuObject
        ->addChildren(new VTCore_Bootstrap_Element_BsElement(array(
          'type' => 'div',
          'attributes' => array(
            'class' => array(
              'product',
            ),
          ),
        )))
        ->lastChild()
        ->addChildren(new VTCore_Bootstrap_Element_BsElement(array(
          'type' => 'div',
          'attributes' => array(
            'class' => array(
              'items',
            ),
          ),
        )));

      if ($this->get('terms.title')) {
        $menuObject
          ->lastChild()
          ->lastChild()
          ->addChildren(new VTCore_Bootstrap_Element_BsElement(array(
            'type' => 'h3',
            'text' => $this->get('terms.title'),
          )));
      }

      // We dont want inner li to have grids!
      $this->remove('terms.grids');

      // Build the terms list
      if ($this->get('terms.taxonomy')) {

        $this->add('terms.taxonomy', 'product_cat');
        $this->add('terms.attributes.class.menu', 'nav menu');

        $menuObject
          ->lastChild()
          ->lastChild()
          ->addChildren(new VTCore_Wordpress_Element_WpTermList($this->get('terms')));

      }

      // Build the menu as list
      elseif ($this->get('terms.menu')) {
        $menuObject
          ->lastChild()
          ->lastChild()
          ->addChildren(wp_nav_menu(array(
            'theme_location' => $this->get('terms.menu'),
            'echo' => false,
          )));
      }


      switch ($this->get('terms.position')) {

        case 'inside' :
          $loopObject->prependChild($menuObject);
          $this->object->addChildren($loopObject);
          break;

        case 'top' :
          $menuObject->addClass('horizontal');
          $this->object->addChildren($menuObject)->addChildren($loopObject);
          break;

        case 'bottom' :
          $menuObject->addClass('horizontal');
          $this->object->addChildren($loopObject)->addChildren($menuObject);
          break;

        case 'left' :

          $loopObject
            ->removeClass('clearboth');


          $menuObject
            ->removeContext('grids.push')
            ->removeContext('grids.offset')
            ->removeContext('grids.pull')
            ->buildGrid()
            ->addClass('clearboth');

          $this->object->addChildren($menuObject)->addChildren($loopObject);

          break;

        case 'right' :

          $menuObject
            ->removeContext('grids.push')
            ->removeContext('grids.offset')
            ->removeContext('grids.pull')
            ->buildGrid();

          $this->object->addChildren($loopObject)->addChildren($menuObject);

          break;
      }

      $this->remove('terms');

    }

    // Direct inject, no need to build the terms filtering
    else {
      $this->object->addChildren($loopObject);
    }


    // Build the pager element
    if ($this->get('pager.enable')) {
      $this->add('pager.query', $loopObject->getContext('query'));
      $this->object->addChildren(new VTCore_Wordpress_Element_WpPager($this->get('pager')));
    }
  }
}