<?php
/**
 * Victheme Woocommerce Catalog default templates
 * based on woocommerce version 1.6.4.
 *
 * Themes can override this template by copying
 * this to theme/templates/ folder or any other
 * location registered via VTCore Template Factory.
 */

  global $product;

  if (!defined('ABSPATH')) {
  	exit; // Exit if accessed directly
  }

  // Ensure visibility
  if (!$product || !$product->is_visible()) {
  	return;
  }

  // Extra post classes
  $classes = array(
    'product',
  );

  $show = $this->getContext('custom');
  $mode = $this->getContext('template.mode');
  $grid = $this->getContext('grids');

  $gridObject = new VTCore_Bootstrap_Grid_Column($grid);

  // Removing product teasers related hooks
  remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10);
  remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);
  remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);
  remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10);
  remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);


?>


<div class="column product-item clearfix <?php echo $gridObject->getClass();?>">
  <div <?php post_class( $classes ); ?>>

    <?php do_action( 'woocommerce_before_shop_loop_item' ); ?>

    <?php

      // Show flash banner
      if ($show['flash']) {
        woocommerce_show_product_loop_sale_flash();
      }

    ?>

    <div class="product-wrapper">
      <?php if ($show['image']) : ?>

        <a href="<?php the_permalink(); ?>" class="product-image">
          <?php echo woocommerce_template_loop_product_thumbnail(); ?>
        </a>

      <?php endif;?>


      <div class="product-content">


    		<?php do_action( 'woocommerce_before_shop_loop_item_title' ); ?>


    		<?php if ($show['title']) : ?>
    		  <h3 class="product-title clearfix clearboth"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
    		<?php endif;?>


    		<?php do_action( 'woocommerce_after_shop_loop_item_title' ); ?>


    		<?php if ($show['rating']) : ?>
    		  <div class="product-rating clearfix clearboth"><?php woocommerce_template_loop_rating(); ?></div>
    		<?php endif;?>


    		<?php if ($show['price']) : ?>
    		  <div class="product-price clearfix clearboth"><?php woocommerce_template_loop_price(); ?></div>
    		<?php endif;?>


    		<?php if ($show['cart']) : ?>
    		  <div class="product-cart clearfix clearboth"><?php woocommerce_template_loop_add_to_cart(); ?></div>
    		<?php endif;?>

      </div>

    </div>
  	<?php do_action( 'woocommerce_after_shop_loop_item' ); ?>

  </div>
</div>
