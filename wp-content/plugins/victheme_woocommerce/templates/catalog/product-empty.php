<?php
/**
 * Template for displaying empty content message when
 * no teaser is available to display.
 *
 * For other not found page will use the page404.php
 * template.
 *
 * @author jason.xie@victheme.com
 */
?>
<div class="column product col-xs-12 col-sm-8 col-md-8 col-lg-8 col-xs-offset-0 col-sm-offset-2 col-lg-offset-2 col-lg-offset-2">
  <div id="shop-empty" class="clearfix product">

    <div class="product-wrapper">

    <?php if (VTCore_Zeus_Init::getFeatures()->get('show.shop404.title')) :  ?>
      <h3 class="title text-center"><?php echo VTCore_Zeus_Init::getFeatures()->get('options.products.archive.404.title'); ?></h3>
    <?php endif; ?>

    <?php if (VTCore_Zeus_Init::getFeatures()->get('show.shop404.description')) :  ?>
      <div class="description text-center"><?php echo VTCore_Zeus_Init::getFeatures()->get('options.products.archive.404.description');?></div>
    <?php endif; ?>

    <?php if (VTCore_Zeus_Init::getFeatures()->get('show.shop404.category')) :  ?>
      <div class="column category-wrapper catalog-menu">
        <div class="product">
          <div class="items">
            <ul class="menu">
              <?php
                wp_list_categories(array(
                  'taxonomy' => 'product_cat',
                  'title_li' => '',
                ));
              ?>
            </ul>
          </div>
        </div>
      </div>
    <?php endif;?>

    </div>
  </div>
</div>