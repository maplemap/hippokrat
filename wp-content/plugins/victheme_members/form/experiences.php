<?php
/**
 * Class for building the experiences field element
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Members_Form_Experiences
extends VTCore_Bootstrap_Form_Base {

  protected $context = array(

    'type' => 'div',
    'attributes' => array(
      'class' => array(
        'row',
      ),
    ),
    'name' => 'members',
    'value' => array(
      array(
        'title' => false,
        'description' => false,
        'date' => array(
          'start' => false,
          'end' => false,
        ),
      )
    ),
  );

  private $rows = array();
  protected $defaults = array(
    'title' => false,
    'description' => false,
    'start' => false,
    'date' => array(
      'start' => false,
      'end' => false,
    ),
  );


  /**
   * Overriding parent method
   * @see VTCore_Html_Base::buildElement()
   */
  public function buildElement() {

    parent::buildElement();

    VTCore_Wordpress_Utility::loadAsset('jquery-table-manager');

    $this->buildRows();

    $this
      ->addChildren(new VTCore_Bootstrap_Grid_BsColumn(array(
        'grids' => array(
          'columns' => array(
            'mobile' => '12',
            'tablet' => '12',
            'small' => '12',
            'large' => '12',
          ),
        ),
      )))
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Element_BsElement(array(
        'type' => 'div',
        'attributes' => array(
          'class' => array('table-manager'),
        ),
      )))
      ->lastChild()
      ->Table(array(
        'headers' => array(
          ' ',
          __('Entries', 'victheme_members'),
          ' ',
        ),
        'rows' => $this->rows,
      ))
      ->Button(array(
        'text' => __('Add New Experience', 'victheme_members'),
        'attributes' => array(
          'data-tablemanager-type' => 'addrow',
          'class' => array('button', 'button-large', 'button-primary'),
        ),
      ));
  }


  /**
   * Building the media table rows contents array
   */
  private function buildRows() {

    if (!$this->getContext('value')) {
      $this->addContext('value', array($this->defaults));
    }

    foreach ($this->getContext('value') as $key => $data) {

      $data = wp_parse_args($data, $this->defaults);

      // Draggable Icon
      $this->rows[$key][] = array(
        'content' => new VTCore_Bootstrap_Element_BsElement(array(
          'type' => 'span',
          'attributes' => array(
            'class' => array('drag-icon'),
          ),
        )),
        'attributes' => array(
          'class' => array('drag-element'),
        ),
      );

      $object = new VTCore_Bootstrap_Grid_BsRow();
      $object
        ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
          'text' => __('Title', 'victheme_members'),
          'name' => $this->getContext('name') . '[experiences][' . $key . '][title]',
          'value' => $data['title'],
          'grids' => array(
            'columns' => array(
              'mobile' => '12',
              'tablet' => '6',
              'small' => '6',
              'large' => '6',
            ),
          ),
        )))
        ->addChildren(new VTCore_Bootstrap_Form_BsDate(array(
          'text' => __('Duration', 'victheme_members'),
          'name' => $this->getContext('name') . '[experiences][' . $key . '][date]',
          'value' => $data['date'],
          'datepicker' => array(
            'mode' => 'range',
            'separator' => __('To', 'victheme_members'),
          ),
          'grids' => array(
            'columns' => array(
              'mobile' => '12',
              'tablet' => '6',
              'small' => '6',
              'large' => '6',
            ),
          ),
        )))
        ->addChildren(new VTCore_Bootstrap_Form_BsTextarea(array(
          'text' => __('Description', 'victheme_members'),
          'name' => $this->getContext('name') . '[experiences][' . $key . '][description]',
          'raw' => true,
          'value' => $data['description'],
          'grids' => array(
            'columns' => array(
              'mobile' => '12',
              'tablet' => '12',
              'small' => '12',
              'large' => '12',
            ),
          ),
        )));

      $this->rows[$key][] = $object;


      // Remove button
      $this->rows[$key][] = array(
        'content' => new VTCore_Form_Button(array(
          'text' => 'X',
          'attributes' => array(
            'data-tablemanager-type' => 'removerow',
            'class' => array('button', 'button-mini', 'form-button'),
          ),
        )),
        'attributes' => array(
          'class' => array('close-element'),
        ),
      );

    }

    return $this;
  }

}