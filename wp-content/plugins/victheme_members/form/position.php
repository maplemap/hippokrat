<?php
/**
 * Class for building the position field element
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Members_Form_Position
extends VTCore_Bootstrap_Form_Base {

  protected $context = array(

    'type' => 'div',
    'attributes' => array(
      'class' => array(
        'row',
      ),
    ),

    'name' => 'members',
    'value' => array()
  );

  private $rows = array();


  /**
   * Overriding parent method
   * @see VTCore_Html_Base::buildElement()
   */
  public function buildElement() {

    parent::buildElement();
    
    $this
      ->addChildren(new VTCore_Bootstrap_Grid_BsColumn(array(
        'grids' => array(
          'columns' => array(
            'mobile' => '12',
            'tablet' => '4',
            'small' => '4',
            'large' => '4',
          ),
        ),
      )))
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Element_BsElement(array(
        'type' => 'h4',
        'text' => __('Position', 'victheme_members'),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsDescription(array(
        'text' => __('Enter the member job position', 'victheme_members'),
      )))
      ->getParent()
      ->addChildren(new VTCore_Bootstrap_Element_BsElement(array(
        'type' => 'div',
        'grids' => array(
          'columns' => array(
            'mobile' => '12',
            'tablet' => '8',
            'small' => '8',
            'large' => '8',
          ),
        ),
      )))
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'name' => $this->getContext('name') . '[position]',
        'value' => $this->getContext('value'),
        'required' => false,
      )));
  }

}