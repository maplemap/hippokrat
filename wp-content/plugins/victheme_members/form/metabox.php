<?php
/**
 * Class for building the main metabox wrapper
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Members_Form_Metabox
extends VTCore_Bootstrap_Element_BsTabs {

  /**
   * Overriding parent method
   * @see VTCore_Html_Base::buildElement()
   */
  public function buildElement() {

    VTCore_Wordpress_Utility::loadAsset('wp-bootstrap');
    VTCore_Wordpress_Utility::loadAsset('wp-media');
    VTCore_Wordpress_Utility::loadAsset('members-metabox');

    $this->addContext('values', get_post_meta(get_the_id(), '_members_information', true));

    $object = new VTCore_Bootstrap_Form_Base(array(
      'type' => false,
    ));

    $object->addChildren(new VTCore_Members_Form_Position(array(
      'value' => $this->getContext('values.position'),
    )));

    $object->addChildren(new VTCore_Members_Form_Email(array(
      'value' => $this->getContext('values.email'),
    )));

    $object->addChildren(new VTCore_Members_Form_Office(array(
      'value' => $this->getContext('values.office'),
    )));

    $object->addChildren(new VTCore_Members_Form_Training(array(
      'value' => $this->getContext('values.training'),
    )));

    $object->addChildren(new VTCore_Members_Form_Social(array(
      'value' => $this->getContext('values.social'),
    )));


    do_action('vtcore_members_form_profile_alter', $object, $this);

    $this->addContext('contents.profiles', array(
      'title' => __('Profiles', 'victheme_members'),
      'content' => $object,
    ));


    $object = new VTCore_Bootstrap_Form_Base(array(
      'type' => false,
    ));

    $object->addChildren(new VTCore_Members_Form_Experiences(array(
      'value' => $this->getContext('values.experiences'),
    )));

    do_action('vtcore_members_form_experience_alter', $object, $this);

    $this->addContext('contents.experiences', array(
      'title' => __('Experiences', 'victheme_members'),
      'content' => $object,
    ));

    $object = new VTCore_Bootstrap_Form_Base(array(
      'type' => false,
    ));

    $object->addChildren(new VTCore_Members_Form_Education(array(
      'value' => $this->getContext('values.education'),
    )));

    do_action('vtcore_members_form_education_alter', $object, $this);

    $this->addContext('contents.education', array(
      'title' => __('Education', 'victheme_members'),
      'content' => $object,
    ));


    $this->addContext('active', 'profiles');

    parent::buildElement();

  }

}