<?php
/**
 * Class for building the Members Taxonomy.
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Members_Taxonomy
extends VTCore_Wordpress_Models_Config {

  protected $database = false;
  protected $filter = 'vtcore_members_taxonomy_alter';
  protected $loadFunction = false;
  protected $saveFunction = false;
  protected $deleteFunction = false;


  /**
   * Child class must extend this method
   * to register their object information
   *
   * @param array $options
   * @return VTCore_Wordpress_Config_Base
   */
  protected function register(array $options) {
    $this->options =  array(
      'speciality' => array(
        'public' => true,
        'show_ui' => true,
        'show_in_nav_menus' => true,
        'show_tagcloud' => true,
        'show_admin_column' => true,
        'hierarchical' => true,
        'query_vars' => 'speciality',
        'rewrite' => array(
          'slug' => 'speciality',
          'with_front' => true,
          'hierarchical' => true,
        ),
        'labels' => array(
          'name'              => _x('Speciality', 'taxonomy general name', 'victheme_members'),
          'singular_name'     => _x('Speciality', 'taxonomy singular name', 'victheme_members'),
          'search_items'      => __('Search Speciality', 'victheme_members'),
          'all_items'         => __('All Specialities', 'victheme_members'),
          'parent_item'       => __('Parent Speciality', 'victheme_members'),
          'parent_item_colon' => __('Parent Speciality :', 'victheme_members'),
          'edit_item'         => __('Edit Speciality', 'victheme_members'),
          'update_item'       => __('Update Speciality', 'victheme_members'),
          'add_new_item'      => __('Add New Speciality', 'victheme_members'),
          'new_item_name'     => __('New Speciality', 'victheme_members'),
          'menu_name'         => __('Specialities', 'victheme_members'),
        ),
      ),
    );

    $this->merge($options);
    $this->filter();

    return $this;
  }

  /**
   * Register the registered taxonomy to WordPress
   * @return $this
   */
  public function registerTaxonomy() {

    foreach ($this->extract() as $taxonomy_slug => $taxonomy) {
      register_taxonomy($taxonomy_slug, 'members', $taxonomy);
    }

    return $this;
  }

}