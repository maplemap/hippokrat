<?php
/*
Plugin Name: VicTheme Members
Plugin URI: http://victheme.com
Description: Plugin for doctor custom post types and its utility
Author: jason.xie@victheme.com
Version: 1.0.2
Author URI: http://victheme.com
*/

define('VTCORE_MEMBERS_CORE_VERSION', '1.7.0');

// Register the data mode alteration before victheme department init.
add_filter('vtcore_department_alter_data_mode', 'vtcoreMembersAlterDataMode');
add_filter('vtcore_services_alter_data_mode', 'vtcoreMembersAlterDataMode');
function vtcoreMembersAlterDataMode($mode) {
  return 'post';
}

add_action('plugins_loaded', 'bootVicthemeMembers', 12);

function bootVicthemeMembers() {

  if (!(defined('VTCORE_VERSION') && version_compare(VTCORE_VERSION, VTCORE_MEMBERS_CORE_VERSION, '='))) {

    add_action('admin_notices', 'MembersMissingCoreNotice');

    function MembersMissingCoreNotice() {

      if (!defined('VTCORE_VERSION')) {
        $notice = __('VicTheme Members depends on VicTheme Core Plugin which is not activated or missing, Please enable it first before VicTheme Members can work properly.');
      }
      elseif (version_compare(VTCORE_VERSION, VTCORE_MEMBERS_CORE_VERSION, '!=')) {
        $notice = sprintf(__('VicTheme Members depends on VicTheme Core Plugin API version %s to operate properly.', 'victheme_members'), VTCORE_MEMBERS_CORE_VERSION);
      }

      if (isset($notice)) {
        echo '<div class="error""><p>' . $notice . '</p></div>';
      }

    }

    return;
  }

  define('VTCORE_MEMBERS_LOADED', true);
  define('VTCORE_MEMBERS_BASE_DIR', dirname(__FILE__));

  include_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'init.php');
  new VTCore_Members_Init();
}
