<?php
/**
 * Hooking into vtcore_headline_configuration_context_alter filter
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Members_Filters_VTCore__Headline__Configuration__Context__Alter
extends VTCore_Wordpress_Models_Hook {

  public function hook($context = NULL) {

    $context['members'] = array(
      'general' => array(
        'enable' => true,
        'title' => __('Members', 'victheme_members'),
        'subtitle' => __('Browse our members', 'victheme_members'),
        'headline_columns' => array(
          'mobile' => '12',
          'tablet' => '6',
          'small' => '6',
          'large' => '6',
        ),
        'item_columns' => array(
          'mobile' => '0',
          'tablet' => '6',
          'small' => '6',
          'large' => '6',
        ),
      ),
    );

    global $wp_query;

    $context['members_speciality'] = array(
      'general' => array(
        'enable' => true,
        'title' => sprintf(__('Speciality : %s', 'victheme_members'), $wp_query->get('speciality')),
        'subtitle' => sprintf(__('Members with %s speciality skills', 'victheme_members'), $wp_query->get('speciality')),
        'headline_columns' => array(
          'mobile' => '12',
          'tablet' => '6',
          'small' => '6',
          'large' => '6',
        ),
        'item_columns' => array(
          'mobile' => '0',
          'tablet' => '6',
          'small' => '6',
          'large' => '6',
        ),
      ),
    );

    return $context;
  }
}