<?php
/**
 * Hooking into victheme_service plugin to define
 * the valid user is all of members post type
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Members_Filters_VTCore__Services__Valid__Users
extends VTCore_Wordpress_Models_Hook {

  public function hook($options = NULL) {

    $posts = get_posts(array(
      'post_type' => 'members',
      'post_status' => 'publish',
      'posts_per_page' => 600,
    ));

    $options = array();
    foreach ($posts as $post) {
      $options[$post->ID] = $post->post_title;
    }

    if (empty($options)) {
      $options[false] = __('No Members Available', 'victheme_members');
    }

    return $options;
  }

}