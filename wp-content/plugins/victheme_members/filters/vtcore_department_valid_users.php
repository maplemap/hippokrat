<?php
/**
 * Hooking into victheme_service plugin to define
 * the valid user is all of members post type
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Members_Filters_VTCore__Department__Valid__Users
extends VTCore_Wordpress_Models_Hook {

  public function hook($options = NULL) {
    $object = new VTCore_Members_Filters_VTCore__Services__Valid__Users();
    return $object->hook($options);
  }

}