<?php
/**
 * Class for building the Members Post Type.
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Members_PostType
  extends VTCore_Wordpress_Models_Config {

  protected $database = false;
  protected $filter = 'vtcore_members_post_type_alter';
  protected $loadFunction = false;
  protected $saveFunction = false;
  protected $deleteFunction = false;



  /**
   * Child class must extend this method
   * to register their object information
   *
   * @param array $options
   * @return VTCore_Wordpress_Config_Base
   */
  protected function register(array $options) {
    $this->options = array(
      'labels' => array(
        'name' => _x('Members', 'post type general name', 'victheme_members'),
        'singular_name' => _x('Member', 'post type singular name', 'victheme_members'),
        'add_new' => _x('Add New', 'Member', 'victheme_members'),
        'add_new_item' => __('Add New Member', 'victheme_members'),
        'edit_item' => __('Edit Member', 'victheme_members'),
        'new_item' => __('New Member', 'victheme_members'),
        'view_item' => __('View Member', 'victheme_members'),
        'search_items' => __('Search Members', 'victheme_members'),
        'not_found' =>  __('No member found', 'victheme_members'),
        'not_found_in_trash' => __('No member found in Trash', 'victheme_members'),
        'parent_item_colon' => ''
      ),
      'public' => true,
      'publicly_queryable' => true,
      'show_ui' => true,
      'query_var' => true,
      'rewrite' => array( 'slug' => 'members','with_front' => true),
      'capability_type' => 'page',
      'map_meta_cap' => true,
      'hierarchical' => false,
      'has_archive' => true,
      'menu_position' => 5,
      'supports' => array('title', 'editor', 'excerpt', 'thumbnail'),
      'exclude_from_search' => false,
      'register_meta_box_cb' => array($this, 'registerMetabox'),
      'menu_icon' => 'dashicons-businessman',
      'delete_with_user' => true,
    );

    $this->merge($options);
    $this->filter();

    return $this;
  }

  public function registerPostType() {
    register_post_type('members', $this->options);
    return $this;
  }

  public function registerMetabox() {
    add_meta_box('members-information', __('Members Information', 'victheme_members'), array(new VTCore_Members_Form_Metabox(), 'render'), 'members','normal', 'high');
    return $this;
  }

}