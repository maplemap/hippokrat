<?php
/**
 * Main initialization class for properly
 * booting the Members plugin.
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Members_Init {

  private $autoloader;

  private $actions = array(
    'save_post',
    'the_post',
    'delete_post',
    'init',
    'vtcore_headline_add_configuration_panel',
    'vtcore_services_user_object',
  );

  private $filters = array(
    'vtcore_department_valid_users',
    'vtcore_services_valid_users',
    'vtcore_headline_alter',
    'vtcore_headline_configuration_context_alter',
    'enter_title_here',
  );

  public function __construct() {

    // Booting autoloader
    $this->autoloader = new VTCore_Autoloader('VTCore_Members', dirname(__FILE__));
    $this->autoloader->setRemovePath('vtcore' . DIRECTORY_SEPARATOR . 'members' . DIRECTORY_SEPARATOR);
    $this->autoloader->register();

    // Autoload translation for vtcore
    load_plugin_textdomain('victheme_members', false, 'victheme_members/languages');

    // Registering assets
    VTCore_Wordpress_Init::getFactory('assets')
      ->get('library')
      ->detect(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets', plugins_url('', __FILE__) . '/assets');


    // Registering actions
    VTCore_Wordpress_Init::getFactory('filters')
      ->addPrefix('VTCore_Members_Filters_')
      ->addHooks($this->filters)
      ->register();


    // Registering filters
    VTCore_Wordpress_Init::getFactory('actions')
      ->addPrefix('VTCore_Members_Actions_')
      ->addHooks($this->actions)
      ->register();

  }

}