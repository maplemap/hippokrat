<?php
/**
 * Simple Class for loading all related post meta data
 * into the object and utilizing the dotted notation
 * for easy parsing.
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Members_Entity
extends VTCore_Wordpress_Models_Dot {

  protected $metaKey = '_members_information';
  protected $departmentKey = '_members_department';
  protected $serviceKey = '_department_services';
  protected static $cache = array();


  /**
   * Overriding parent method
   * @param array $options
   */
  public function __construct($options = array()) {

    $this->options = array(
      'post' => array(),
      'department' => array(),
      'services' => array(),
      'days' => array(
        'monday' => __('Monday', 'victheme_members'),
        'tuesday' => __('Tuesday', 'victheme_members'),
        'wednesday' => __('Wednesday', 'victheme_members'),
        'thursday' => __('Thursday', 'victheme_members'),
        'friday' => __('Friday', 'victheme_members'),
        'saturday' => __('Saturday', 'victheme_members'),
        'sunday' => __('Sunday', 'victheme_members'),
      ),
    );

    if (isset($options['post'])) {
      $options['post'] = (array) $options['post'];
    }

    $this->merge($options);

    if (!$this->get('post') && get_the_ID()) {
      $this->add('post', (array) get_post(get_the_ID()));
    }

    if (!$this->get('data') && $this->get('post')) {
      $this->add('data', get_post_meta($this->get('post.ID'), $this->metaKey, true));
    }


    // Load department
    if ($department = get_post_meta($this->get('post.ID'), $this->departmentKey, true)) {
      $this->add('department', (array) $department);
    }

    // Load related services
    if ($this->get('department')) {
      foreach ($this->get('department') as $post_id) {
        if (!isset(self::$cache['services']) && !isset(self::$cache['services'][$post_id])) {
          self::$cache['services'][$post_id] = $this->getMeta($this->serviceKey, (int) $post_id);
        }

        if (isset(self::$cache['services'][$post_id])) {
          $this->add('services', self::$cache['services'][$post_id]);
        }
      }
    }

    do_action('vtcore_members_entity_loading', $this);

  }




  /**
   * Method for retrieving stored wordpress meta post id
   * by defining the metakey, meta value and post status
   * @param $key
   *  string meta key
   * @param $value
   *  string meta value
   * @param string $status
   *  string post status
   * @return array
   */
  protected function getMeta($key, $value, $status = 'publish') {
    global $wpdb;
    $results = $wpdb->get_col(
      $wpdb->prepare("
        SELECT m.post_id
        FROM $wpdb->postmeta as m
        JOIN $wpdb->posts as p
        ON m.post_id = p.ID
        WHERE m.meta_key = %s
        AND m.meta_value = %s
        AND p.post_status = %s
      ", $key, $value, $status));

    foreach ($results as $key => $value) {
      $object = new VTCore_Wordpress_Objects_Array(get_post_meta((int) $value, '_services_data', true));

      $member = false;
      foreach ($object->get('schedule') as $schedule) {
        $scheduleObject = new VTCore_Wordpress_Objects_Array($schedule);
        foreach ($scheduleObject->get('users') as $userID) {
          if ($userID == $this->get('post.ID')) {
            $member = true;
          }
        }
      }

      if ($member) {
        $results[$key] = (int) $value;
      }
    }

    return $results;
  }




  /**
   * Clear object statically cached array values
   *
   * @return $this
   */
  public function clearCache() {
    self::$cache = array();
    return $this;
  }


  /**
   * Remove Member from services
   * Best hooked to action where service post is deleted
   *
   * @return $this
   */
  public function removeFromServices() {

    if (!$this->get('services')) {
      return $this;
    }

    foreach ($this->get('services') as $service_id) {

      $service_meta = get_post_meta($service_id, '_services_data', true);

      if (!is_array($service_meta)
          || !isset($service_meta['schedule'])
          || !is_array($service_meta['schedule'])) {

        continue;
      }

      foreach ($service_meta['schedule'] as $delta => $data) {

        if (!isset($data['users']) || !is_array($data['users'])) {
          continue;
        }

        foreach ($data['users'] as $userDelta => $userID) {
          if ($userID != $this->get('post.ID')) {
            continue;
          }

          unset($service_meta['schedule'][$delta]['users'][$userDelta]);

        }

      }

      update_post_meta($service_id, '_services_data', $service_meta);
    }


    return $this;

  }


  /**
   * Remove users from department
   * Best hooked to action where department is deleted
   *
   * @return $this
   */
  public function removeFromDepartment() {

    if (!$this->get('department')) {
      return $this;
    }

    foreach ($this->get('department') as $department_id) {
      $department_meta = get_post_meta('_department_information', true);

      if (!isset($department_meta['members'])
          || !is_array($department_meta['members'])) {
        continue;
      }

      foreach ($department_meta['members'] as $delta => $userID) {
        if ($userID != $this->get('post.ID')) {
          continue;
        }

        unset($department_meta['members'][$delta]);

      }

      update_post_meta($department_id, '_department_information', $department_meta);
    }

    return $this;
  }


  /**
   * Retrives and cache the markup for speciality field
   *
   * @return array|null|string|void
   */
  public function getSpeciality() {
    if (!$this->get('rendered.speciality')) {
      $terms = wp_get_post_terms($this->get('post.ID'), 'speciality', 'all');
      $output = array();
      if (!is_wp_error($terms) && is_array($terms)) {
        foreach ($terms as $term) {
          $term_link = get_term_link($term);
          if (!is_wp_error($term_link)) {
            $output[] = '<a href="' . esc_url( $term_link ) . '">' . $term->name . '</a>';
          }
        }
      }

      if (!empty($output)) {
        $this->add('rendered.speciality', wp_kses_post(implode(', ', $output)));
      }
    }
    return $this->get('rendered.speciality');
  }


  /**
   * Retrives and cache the markup for department field
   *
   * @return array|null|string|void
   */
  public function getDepartment() {
    if (!$this->get('rendered.department') && $this->get('department')) {
      $department = array();
      foreach ($this->get('department') as $post_id) {
        $object = new VTCore_Html_HyperLink(array(
          'text' => get_the_title($post_id),
          'attributes' => array(
            'href' => get_permalink($post_id),
            'alt' => get_the_title($post_id),
            'class' => array(
              'post-link'
            ),
          ),
        ));

        $department[] = $object->__toString();
      }

      $this->add('rendered.department', wp_kses_post(implode(', ', $department)));
    }
    return $this->get('rendered.department');
  }


  /**
   * Retrives and cache the markup for experience field
   *
   * @return array|null|string|void
   */
  public function getExperience() {
    if (!$this->get('rendered.experiences') && $this->get('data.experiences')) {
      $experience = array();
      foreach ($this->get('data.experiences') as $data) {
        $object = new VTCore_Wordpress_Objects_Array($data);
        if ($object->get('title')) {
          $experience[] = $object->get('title');
        }
      }

      if (!empty($experience)) {
        $this->add('rendered.experiences', wp_kses_post(implode(', ', $experience)));
      }
    }
    return $this->get('rendered.experiences');
  }


  /**
   * Retrives and cache the markup for office field
   *
   * @return array|null|string|void
   */
  public function getOffice() {
    if (!$this->get('rendered.office')) {
      $this->add('rendered.office', wp_kses_post(do_shortcode($this->get('data.office'))));
    }
    return $this->get('rendered.office');
  }


  /**
   * Retrives and cache the markup for workdays field
   *
   * @return array|null|string|void
   */
  public function getWorkDays() {
    if (!$this->get('rendered.workday') && $this->get('services')) {
      $days = array();
      foreach ($this->get('services') as $post_id) {
        $object = new VTCore_Wordpress_Objects_Array(get_post_meta($post_id, '_services_data', true));
        foreach ($object->get('schedule') as $schedule) {
          $scheduleObject = new VTCore_Wordpress_Objects_Array($schedule);
          
          if ($scheduleObject->get('days')) {
            foreach ($scheduleObject->get('days') as $day) {
              $days[$day] = $this->get('days.' . $day);
            }
          }
        }

      }

      if (!empty($days)) {
        $this->add('rendered.workday', wp_kses_post(implode(', ', $days)));
      }
    }
    return $this->get('rendered.workday');
  }


  /**
   * Retrives and cache the markup for position field
   *
   * @return array|null|string|void
   */
  public function getPosition() {
    if (!$this->get('rendered.position')) {
      $this->add('rendered.position', wp_kses_post($this->get('data.position')));
    }
    return $this->get('rendered.position');
  }

  /**
   * Retrives and cache the markup for email field
   *
   * @return array|null|string|void
   */
  public function getEmail() {
    if (!$this->get('rendered.email')) {
      $this->add('rendered.email', wp_kses_post($this->get('data.email')));
    }
    return $this->get('rendered.email');
  }


  /**
   * Retrives and cache the markup for services field
   *
   * @return array|null|string|void
   */
  public function getServices() {
    if (!$this->get('rendered.services')) {
      $services = array();
      foreach ($this->get('services') as $post_id) {
        $object = new VTCore_Html_HyperLink(array(
          'text' => get_the_title((int) $post_id),
          'attributes' => array(
            'href' => get_permalink($post_id),
            'alt' => get_the_title($post_id),
            'class' => array(
              'post-link'
            ),
          ),
        ));

        $services[] = wp_kses_post($object->__toString());
      }

      if (!empty($services)) {
        $this->add('rendered.services', wp_kses_post(implode(', ', $services)));
      }
    }
    return $this->get('rendered.services');
  }


  /**
   * Retrives and cache the markup for training field
   *
   * @return array|null|string|void
   */
  public function getTraining() {
    if (!$this->get('rendered.training')) {
      $this->add('rendered.training', wp_kses_post(do_shortcode($this->get('data.training'))));
    }
    return $this->get('rendered.training');
  }


  /**
   * Retrives and cache the markup for education field
   *
   * @return array|null|string|void
   */
  public function getEducation() {
    if (!$this->get('rendered.education') && $this->get('data.education')) {
      $education = array();
      foreach ($this->get('data.education') as $data) {
        $object = new VTCore_Wordpress_Objects_Array($data);
        if ($object->get('title')) {
          $education[] = $object->get('title');
        }
      }

      if (!empty($education)) {
        $this->add('rendered.education', wp_kses_post(implode(', ', $education)));
      }
    }
    return $this->get('rendered.education');
  }


  /**
   * Retrives and cache the markup for abbr field
   *
   * @return array|null|string|void
   */
  public function getAbbr() {
    if (!$this->get('rendered.abbr') && $this->get('data.abbr')) {
      $abbr = array();
      foreach ($this->get('data.abbr') as $data) {
        $object = new VTCore_Wordpress_Objects_Array($data);
        if ($object->get('abbr')) {
          $abbr[] = $object->get('abbr');
        }
      }

      if (!empty($abbr)) {
        $this->add('rendered.abbr', wp_kses_post(implode(', ', $abbr)));
      }
    }
    return $this->get('rendered.abbr');
  }

  /**
   * Retrieves and cached the markup for social icon field
   *
   * @return array|null|string|void
   */
  public function getSocial() {

    if (!$this->get('rendered.social')) {
      $object = new VTCore_Wordpress_Element_WpSocialIcon(array(
        'social' => $this->get('data.social'),
        'attributes' => array(
          'class' => array(
            'post-social',
          ),
        ),
      ));

      $this->add('rendered.social', wp_kses_post($object->__toString()));
    }

    return $this->get('rendered.social');
  }


  /**
   * Build, cache and render the member total experience years
   *
   * @return array|null|string|void
   */
  public function getExperienceLength() {
    if (!$this->get('rendered.experienceslength')) {
      $start = '';
      foreach ($this->get('data.experiences') as $delta => $experiences) {
        $object = new VTCore_Wordpress_Objects_Array($experiences);

        if (empty($start)) {
          $start = strtotime($object->get('date.start'));
        }

        elseif ($object->get('date.start')) {
          $startWorking = strtotime($object->get('date.start'));

          if ($startWorking < $start) {
            $start = $startWorking;
          }
        }
      }

      $this->add('rendered.experienceslength', wp_kses_post($this->dateLength($start)));
    }


    return $this->get('rendered.experienceslength');
  }


  /**
   * Calculate the working period
   * @param $time
   * @return string
   */
  public function dateLength($time) {

    $periods         = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
    $lengths         = array("60","60","24","7","4.35","12","10");

    $now             = time();
    $unix_date       = $time;
    $difference      = $now - $unix_date;

    for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
      $difference /= $lengths[$j];
    }

    $difference = round($difference);

    if($difference != 1) {
      $periods[$j].= "s";
    }

    return "$difference $periods[$j]";
  }


  /**
   * Build, cache and render the member registered service
   *
   * @return array|null|string|void
   */
  public function getServicesTable() {

    if (!$this->get('rendered.servicestable')) {

      $rows = array();
      foreach ($this->get('services') as $post_id) {
        $meta = new VTCore_Wordpress_Objects_Array(get_post_meta($post_id, '_services_data', true));

        foreach ($meta->get('schedule') as $delta => $schedule) {
          $object = new VTCore_Wordpress_Objects_Array($schedule);

          $member = false;
          foreach ($object->get('users') as $userID) {
            if ($userID == $this->get('post.ID')) {
              $member = true;
              break;
            }
          }

          // User is not member of this service schedule, continue to next schedule
          if (!$member) {
            continue;
          }

          $row = apply_filters('vtcore_member_services_table_row', array(
            'description' => $object->get('description'),
            'days' => implode('<br/>', $object->get('days')),
            'time' => '<span class="start">' . $object->get('start') . '</span><span class="separator"> - </span><span class="end">' . $object->get('end') . '</span>',
            'location' => $object->get('location'),
          ));

          foreach ($row as $key => $value) {
            $rows[$delta][$key] = array(
              'content' => $value,
              'attributes' => array(
                'class' => array(
                  'members-services-cell',
                  'members-services-' . $key,
                ),
              ),
            );
          }
        }

      }

      $headers_data = apply_filters('vtcore_members_services_table_header', array(
        'service' => __('Service', 'victheme_members'),
        'days' => __('Days', 'victheme_members'),
        'time' => __('Time', 'victheme_members'),
        'location' => __('Location', 'victheme_members'),
      ));

      $headers = array();
      foreach ($headers_data as $delta => $value) {
        $headers[$delta] = array(
          'content' => $value,
          'attributes' => array(
            'class' => array(
              'members-services-header',
              'members-services-' . $delta,
            ),
          ),
        );
      }

      $table = new VTCore_Html_Table(array(
        'headers' => $headers,
        'rows' => $rows,
        'attributes' => array(
          'class' => array(
            'members-services-table',
            'members-table',
            'table table-stripped table-bordered'
          ),
        )
      ));

      $this->add('rendered.servicestable', wp_kses_post($table->__toString()));
    }

    return $this->get('rendered.servicestable');

  }


  /**
   * Build, cache and render the member education table
   * @return array|null|string|void
   */
  public function getEducationTable() {

    if (!$this->get('rendered.educationtable')) {
      $rows = array();
      foreach ($this->get('data.education') as $delta => $education) {
        $object = new VTCore_Wordpress_Objects_Array($education);
        $row = apply_filters('vtcore_member_education_table_row', array(
          'description' => '<h2 class="post-title">' . wp_kses_post($object->get('title')) . '</h2><div class="post-content">' .wp_kses_post(do_shortcode($object->get('description'))) . '</div>',
          'duration' => '<span class="start">' . $object->get('date.start') . '</span><span class="separator"> - </span><span class="end">' . $object->get('date.end') . '</span>',
        ));

        foreach ($row as $key => $value) {
          $rows[$delta][$key] = array(
            'content' => $value,
            'attributes' => array(
              'class' => array(
                'members-education-cell',
                'members-education-' . $key,
              ),
            ),
          );
        }
      }

      $headers_data = apply_filters('vtcore_members_education_table_header', array(
        'description' => __('Description', 'victheme_members'),
        'duration' => __('Duration', 'victheme_members'),
      ));

      $headers = array();
      foreach ($headers_data as $delta => $value) {
        $headers[$delta] = array(
          'content' => $value,
          'attributes' => array(
            'class' => array(
              'members-education-header',
              'members-education-' . $delta,
            ),
          ),
        );
      }

      $table = new VTCore_Html_Table(array(
        'headers' => $headers,
        'rows' => $rows,
        'attributes' => array(
          'class' => array(
            'members-education-table',
            'members-table',
            'table table-stripped table-bordered'
          ),
        )
      ));

      $this->add('rendered.educationtable', wp_kses_post($table->__toString()));
    }


    return $this->get('rendered.educationtable');

  }


  /**
   * Build, cache and render the member experiences table
   *
   * @return array|null|string|void
   */
  public function getExperienceTable() {
    if (!$this->get('rendered.experiencestable')) {
      $rows = array();
      foreach ($this->get('data.experiences') as $delta => $experiences) {
        $object = new VTCore_Wordpress_Objects_Array($experiences);
        $row = apply_filters('vtcore_member_experiences_table_row', array(
          'description' => '<h2 class="post-title">' . wp_kses_post($object->get('title')) . '</h2><div class="post-content">' .wp_kses_post(do_shortcode($object->get('description'))) . '</div>',
          'duration' => '<span class="start">' . $object->get('date.start') . '</span><span class="separator"> - </span><span class="end">' . $object->get('date.end') . '</span>',
        ));

        foreach ($row as $key => $value) {
          $rows[$delta][$key] = array(
            'content' => $value,
            'attributes' => array(
              'class' => array(
                'members-experiences-cell',
                'members-experiences-' . $key,
              ),
            ),
          );
        }
      }

      $headers_data = apply_filters('vtcore_members_experiences_table_header', array(
        'description' => __('Description', 'victheme_members'),
        'duration' => __('Duration', 'victheme_members'),
      ));

      $headers = array();
      foreach ($headers_data as $delta => $value) {
        $headers[$delta] = array(
          'content' => $value,
          'attributes' => array(
            'class' => array(
              'members-experiences-header',
              'members-experiences-' . $delta,
            ),
          ),
        );
      }

      $table = new VTCore_Html_Table(array(
        'headers' => $headers,
        'rows' => $rows,
        'attributes' => array(
          'class' => array(
            'members-experiences-table',
            'members-table',
            'table table-stripped table-bordered'
          ),
        )
      ));

      $this->add('rendered.experiencestable', wp_kses_post($table->__toString()));
    }


    return $this->get('rendered.experiencestable');
  }


}