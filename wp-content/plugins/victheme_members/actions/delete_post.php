<?php
/**
 * Hooking into wordpress delete_post action
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Members_Actions_Delete__Post
extends VTCore_Wordpress_Models_Hook {

  protected $weight = 11;
  private $post;

  public function hook($post_id = FALSE) {

    global $wpdb;
    $this->post =  $wpdb->get_row($wpdb->prepare("SELECT * FROM $wpdb->posts WHERE ID = %d", $post_id));

    // Doctor is deleted from database, process all related entry.
    if ($this->post->post_type == 'members') {

      $object = new VTCore_Members_Entity(array('post' => $this->post));
      $object
        ->removeFromServices()
        ->removeFromDepartment()
        ->clearCache();
    }

    // Department is deleted, remove all members pointers
    if ($this->post->post_type == 'department') {
      $object = new VTCore_Department_Entity(array('post' => $this->post));
      if ($object->get('members') && is_array($this->get('members'))) {
        foreach ($object->get('members') as $memberid) {
          delete_post_meta($memberid, '_members_department');
        }
      }
    }

    // Service is deleted, just delete the entity cache, all pointer entry
    // will be handled by department and service plugin
    if ($this->post->post_type == 'services') {
      $object = new VTCore_Members_Entity(array('post' => $this->post));
      $object->clearCache();
    }
  }
}