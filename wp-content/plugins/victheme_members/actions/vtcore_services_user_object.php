<?php
/**
 * Hooking into wordpress widget_init action for
 * registering maps custom widgets.
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Members_Actions_VTCore__Services__User__Object
extends VTCore_Wordpress_Models_Hook {


  public function hook($object = NULL) {
    $object
      ->add('href', get_permalink((int) $object->get('userid')))
      ->add('name', get_the_title((int) $object->get('userid')))
      ->add('markup', new VTCore_Html_HyperLink(array(
        'text' => $object->get('name'),
        'attributes' => array(
          'href' => $object->get('href'),
          'class' => array(
            'member-link',
            'pull-inline',
          ),
        ),
      )))
      ->remove('userid');

    // Don't let VTCore clean the object!
    $object->get('markup')->setClean(false);
  }
}