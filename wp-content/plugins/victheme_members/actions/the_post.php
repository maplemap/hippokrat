<?php
/**
 * Hooking into wordpress the_post action.
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Members_Actions_The__Post
extends VTCore_Wordpress_Models_Hook {

  public function hook($post = NULL) {

    if ($post->post_type == 'members') {
      $post->members = new VTCore_Members_Entity(array('post' => $post));
    }

  }
}