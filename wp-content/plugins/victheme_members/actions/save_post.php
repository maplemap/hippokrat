<?php
/**
 * Hooking into wordpress save_post action
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Members_Actions_Save__Post
extends VTCore_Wordpress_Models_Hook {

  protected $argument = 2;
  private $post;

  public function hook($post_id = FALSE, $post = FALSE) {

    if (isset($_POST['members']) && $post->post_type == 'members') {
      $this->post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
      update_post_meta($post_id, '_members_information', $this->post['members']);
    }

    // Saving members assigned department
    if (isset($_POST['department']) && $post->post_type == 'department') {
      $this->post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
      if (isset($this->post['department']['members'])) {
        foreach ($this->post['department']['members'] as $memberid) {
          update_post_meta($memberid, '_members_department', $post_id);
        }
      }
    }

  }
}