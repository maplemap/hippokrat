<?php
/**
 * Hooking into wordpress init action.
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Members_Actions_Init
extends VTCore_Wordpress_Models_Hook {

  public function hook() {

    // Booting Members post type
    $object = new VTCore_Members_PostType();

    // Last chance to alter the post type before it get registered
    do_action('vtcore_members_registering_posttype', $object);

    // Register the post type
    $object->registerPostType();

    // Booting Members taxonomy
    $object = new VTCore_Members_Taxonomy();

    // Last chance to alter the taxonomy before it get registered
    do_action('vtcore_members_registering_taxonomy', $object);

    // Register the taxonomy
    $object->registerTaxonomy();

    // Members "home" archive template redirection
    // on custom taxonomy and members/ link
    add_rewrite_rule('members/?$', 'index.php?post_type=members', 'top');

  }
}