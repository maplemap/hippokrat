<?php
/**
 * Hooking into vtcore_headline_add_configuration_panel action for
 * registering headline panel for members.
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Members_Actions_VTCore__Headline__Add__Configuration__Panel
extends VTCore_Wordpress_Models_Hook {

  public function hook($headline = NULL) {
    $headline->addPanel('members');
    $headline->addPanel('members_speciality');
  }
}