<?php
/*
Plugin Name: VicTheme Services
Plugin URI: http://victheme.com
Description: Plugin suite developed creating services information
Author: jason.xie@victheme.com
Version: 1.0.2
Author URI: http://victheme.com
*/

define('VTCORE_SERVICES_CORE_VERSION', '1.7.0');

add_action('plugins_loaded', 'bootVicthemeServices', 12);

function bootVicthemeServices() {

  if (!(defined('VTCORE_VERSION') && version_compare(VTCORE_VERSION, VTCORE_SERVICES_CORE_VERSION, '='))) {

    add_action('admin_notices', 'ServicesMissingCoreNotice');

    function ServicesMissingCoreNotice() {

      if (!defined('VTCORE_VERSION')) {
        $notice = __('VicTheme Services depends on VicTheme Core Plugin which is not activated or missing, Please enable it first before VicTheme Services can work properly.');
      }
      elseif (version_compare(VTCORE_VERSION, VTCORE_SERVICES_CORE_VERSION, '!=')) {
        $notice = sprintf(__('VicTheme Services depends on VicTheme Core Plugin API version %s to operate properly.', 'victheme_services'), VTCORE_SERVICES_CORE_VERSION);
      }

      if (isset($notice)) {
        echo '<div class="error""><p>' . $notice . '</p></div>';
      }

    }

    return;
  }

  define('VTCORE_SERVICES_LOADED', true);
  define('VTCORE_SERVICES_BASE_DIR', dirname(__FILE__));
  define('VTCORE_SERVICES_DATA_MODE', apply_filters('vtcore_services_alter_data_mode', 'user'));


  include_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'init.php');
  new VTCore_Services_Init();
}