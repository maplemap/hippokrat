<?php
/**
 * Hooking into wordpress the_post action.
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Services_Actions_The__Post
extends VTCore_Wordpress_Models_Hook {

  public function hook($post = NULL) {

    if ($post->post_type == 'services') {
      $post->services = new VTCore_Services_Entity(array('post' => $post));
    }

  }
}