<?php
/**
 * Hooking into wordpress init action.
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Services_Actions_Init
extends VTCore_Wordpress_Models_Hook {

  public function hook() {

    // Booting Members post type
    $object = new VTCore_Services_PostType();

    // Last chance to alter the post type before it get registered
    do_action('vtcore_services_registering_posttype', $object);

    // Register the post type
    $object->registerPostType();

    // Services "home" archive template redirection
    // on custom taxonomy and services/ link
    add_rewrite_rule('services/?$', 'index.php?post_type=services', 'top');

  }
}