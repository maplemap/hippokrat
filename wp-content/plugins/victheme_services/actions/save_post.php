<?php
/**
 * Hooking into wordpress save_post action
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Services_Actions_Save__Post
extends VTCore_Wordpress_Models_Hook {

  protected $argument = 2;
  protected $weight = 20;
  private $post;

  public function hook($post_id = FALSE, $post = FALSE) {

    if (isset($_POST['services']) && $post->post_type == 'services') {
      $this->post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
      update_post_meta($post_id, '_services_data', $this->post['services']);
    }
  }
}