<?php
/**
 * Simple Class for loading all related post meta data
 * into the object and utilizing the dotted notation
 * for easy parsing.
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Services_Entity
extends VTCore_Wordpress_Models_Dot {

  protected $metaKey = '_services_data';


  /**
   * Overriding parent method
   * @param array $options
   */
  public function __construct($options = array()) {

    if (isset($options['post'])) {
      $options['post'] = (array) $options['post'];
    }

    $this->merge($options);

    if (!$this->get('post') && get_the_ID()) {
      $this->add('post', (array) get_post(get_the_ID()));
    }

    if (!$this->get('data') && $this->get('post')) {
      $this->add('data', get_post_meta($this->get('post.ID'), $this->metaKey, true));
    }

    do_action('vtcore_services_entity_loading', $this);

  }


  /**
   * Method for retrieving stored wordpress meta post id
   * by defining the metakey, meta value and post status
   * @param $key
   *  string meta key
   * @param $value
   *  string meta value
   * @param string $status
   *  string post status
   * @return array
   */
  protected function getMeta($key, $value, $status = 'publish') {
    global $wpdb;
    $results = $wpdb->get_col(
      $wpdb->prepare("
        SELECT m.post_id
        FROM $wpdb->postmeta as m
        JOIN $wpdb->posts as p
        ON m.post_id = p.ID
        WHERE m.meta_key = %s
        AND m.meta_value = %s
        AND p.post_status = %s
      ", $key, $value, $status));

    foreach ($results as $key => $value) {
      $results[$key] = (int) $value;
    }

    return $results;
  }

  /**
   * Method for reseting the stored rendered markup per type
   *
   * @param $type
   *   string the rendered type to reset
   *   valid type : teasers, single, all (reset all rendered types)
   * @return $this
   */
  public function resetRendered($type = 'all') {
    if ($type == 'all') {
      $this->remove('rendered');
    }
    elseif ($this->get('rendered.' . $type)) {
      $this->remove('rendered.' . $type);
    }

    return $this;
  }


  /**
   * Method for retrieving the object image and will
   * fall back to post featured image if image is not available
   * or empty string if nothing is available to fetch
   *
   * @param $size
   *    The image size string as in wordpress registered image size or array or width and height
   * @return string
   *    empty string or the image markup
   */
  public function getImage($size = 'medium') {

    if (!$this->get('rendered.image')) {
      $image = '';
      if ($this->get('data.display.image')) {
        $image = wp_get_attachment_image($this->get('data.display.image'), $size);
      }
      elseif ($this->get('post.ID') && has_post_thumbnail($this->get('post.ID'))) {
        $image = get_the_post_thumbnail($this->get('post.ID'), $size);
      }

      $this->add('rendered.image', wp_kses_post($image));
    }

    return $this->get('rendered.image');
  }



  /**
   * Method for retrieving the object icon image and will
   * fall back to post featured Image if teaser icon image is not available
   * or empty string if nothing is available to fetch
   *
   * @param $size
   *    The image size string as in wordpress registered image size or array or width and height
   * @return string
   *    empty string or the icon image markup
   */
  public function getIcon() {

    if (!$this->get('rendered.icon')) {
      $object = new VTCore_Wordpress_Element_WpIcon($this->get('data.display.icon'));
      $this->add('rendered.icon', wp_kses_post($object->__toString()));
    }

    return $this->get('rendered.icon');
  }



  /**
   * Method for retrieving the object price and format it for
   * valid currency markup.
   *
   * @return string
   */
  public function getPrice($prefix = '') {

    if (!$this->get('rendered.price')) {
      $price = '';
      if ($this->get('data.price.price')) {
        $object = new VTCore_Wordpress_Element_WpCurrency(array(
          'prefix' => $prefix,
          'code' => $this->get('data.price.currency'),
          'number' => $this->get('data.price.price'),
        ));

        $price = $object->__toString();
      }

      $this->add('rendered.price', wp_kses_post(do_shortcode($price)));
    }

    return $this->get('rendered.price');
  }


  public function getUser($userid) {

    if (!$this->get('rendered.users.' . $userid)) {
      $object = new VTCore_Wordpress_Objects_Array(array(
        'userid' => $userid,
      ));

      do_action('vtcore_services_user_object', $object);

      if ($object->get('userid')) {
        $meta = array_map(function ($a) {
            return $a[0];
          }, get_user_meta((int) $userid));
        $name = array();
        if (!empty($meta['first_name'])) {
          $name[] = $meta['first_name'];
        }
        if (!empty($meta['last_name'])) {
          $name[] = $meta['last_name'];
        }

        if (empty($name)) {
          $name[] = $meta['nickname'];
        }

        $object
          ->add('name', implode(' ', $name))
          ->add('markup', new VTCore_Html_Element(array(
            'attributes' => array(
              'type' => 'span',
              'text' => $object->get('name'),
              'class' => array(
                'member-link',
                'pull-inline',
              ),
            ),
          )));

      }

      $this->add('rendered.users.' . $userid, $object);
    }

    return $this->get('rendered.users.' . $userid);
  }


  public function getSchedule() {

    if (!$this->get('rendered.schedule')) {

      $rows = array();
      foreach ($this->get('data.schedule') as $delta => $schedule) {
        $object = new VTCore_Wordpress_Objects_Array($schedule);

        do_action('vtcore_services_schedule_table_members', $object);

        if ($object->get('users')) {
          foreach ($object->get('users') as $userid) {
            if ($this->getUser($userid) &&  $this->getUser($userid)->get('markup')) {
              $object->add('markup.users.' . $userid, $this->getUser($userid)->get('markup')->__toString());
            }
          }
        }

        $row = apply_filters('vtcore_services_schedule_table_row', array(
          'description' => $object->get('description'),
          'days' => implode('<br/>', $object->get('days')),
          'time' => '<span class="start">' . $object->get('start') . '</span><span class="separator"> - </span><span class="end">' . $object->get('end') . '</span>',
          'location' => $object->get('location'),
          'members' => implode('<br/>', $object->get('markup.users')),
        ));

        foreach ($row as $key => $value) {
          $rows[$delta][$key] = array(
            'content' => $value,
            'attributes' => array(
              'class' => array(
                'schedule-cell',
                'schedule-' . $key,
              ),
            ),
          );
        }

      }

      $headers_data = apply_filters('vtcore_services_schedule_table_header', array(
          'service' => __('Service', 'victheme_services'),
          'days' => __('Days', 'victheme_services'),
          'time' => __('Time', 'victheme_services'),
          'location' => __('Location', 'victheme_services'),
          'members' => __('Members', 'victheme_services'),
      ));

      $headers = array();
      foreach ($headers_data as $delta => $value) {
        $headers[$delta] = array(
          'content' => $value,
          'attributes' => array(
            'class' => array(
              'schedule-header',
              'schedule-' . $delta,
            ),
          ),
        );
      }

      $table = new VTCore_Html_Table(array(
        'headers' => $headers,
        'rows' => $rows,
        'attributes' => array(
          'class' => array(
            'schedule-table',
            'table-bordered',
          ),
        )
      ));

      $this->add('rendered.schedule', wp_kses_post($table->__toString()));
    }

    return $this->get('rendered.schedule');
  }

}