<?php
/**
 * Class for building the Services Post Type.
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Services_PostType
  extends VTCore_Wordpress_Models_Config {

  protected $database = false;
  protected $filter = 'vtcore_portfolio_post_type_alter';
  protected $loadFunction = false;
  protected $saveFunction = false;
  protected $deleteFunction = false;



  /**
   * Child class must extend this method
   * to register their object information
   *
   * @param array $options
   * @return VTCore_Wordpress_Config_Base
   */
  protected function register(array $options) {
    $this->options = array(
      'labels' => array(
        'name' => _x('Services', 'post type general name', 'victheme_services'),
        'singular_name' => _x('Service', 'post type singular name', 'victheme_services'),
        'add_new' => _x('Add New', 'Service', 'victheme_services'),
        'add_new_item' => __('Add New Service', 'victheme_services'),
        'edit_item' => __('Edit Service', 'victheme_services'),
        'new_item' => __('New Service', 'victheme_services'),
        'view_item' => __('View Service', 'victheme_services'),
        'search_items' => __('Search Services', 'victheme_services'),
        'not_found' =>  __('No Service found', 'victheme_services'),
        'not_found_in_trash' => __('No service found in Trash', 'victheme_services'),
        'parent_item_colon' => ''
      ),
      'public' => true,
      'publicly_queryable' => true,
      'show_ui' => true,
      'query_var' => true,
      'rewrite' => array( 'slug' => 'services','with_front' => true),
      'capability_type' => 'page',
      'map_meta_cap' => true,
      'hierarchical' => false,
      'has_archive' => true,
      'menu_position' => 5,
      'supports' => array('title', 'editor', 'excerpt', 'thumbnail'),
      'exclude_from_search' => false,
      'register_meta_box_cb' => array($this, 'registerMetabox'),
      'menu_icon' => 'dashicons-id-alt',
      'delete_with_user' => true,
    );

    $this->merge($options);
    $this->filter();

    return $this;
  }

  public function registerPostType() {
    register_post_type('services', $this->options);
    return $this;
  }

  public function registerMetabox() {
    add_meta_box('services-information', __('Services Information', 'victheme_services'), array(new VTCore_Services_Form_Metabox(), 'render'), 'services','normal', 'high');
  }

}