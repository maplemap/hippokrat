<?php
/**
 * Class for building the price field element
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Services_Form_Price
extends VTCore_Bootstrap_Form_Base {

  protected $context = array(

    'type' => 'div',
    'attributes' => array(
      'class' => array(
        'row',
      ),
    ),
    'name' => 'services',
    'value' => array(
      'currency' => '',
      'price' => '',
    ),

  );


  private $content;

  /**
   * Overriding parent method
   * @see VTCore_Html_Base::buildElement()
   */
  public function buildElement() {

    parent::buildElement();

    $this
      ->addChildren(new VTCore_Bootstrap_Element_BsElement(array(
        'type' => 'div',
        'grids' => array(
          'columns' => array(
            'mobile' => '12',
            'tablet' => '4',
            'small' => '4',
            'large' => '4',
          ),
        ),
      )))
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Element_BsElement(array(
        'type' => 'h4',
        'text' => __('Price', 'victheme_services'),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsDescription(array(
        'text' => __('Set the price for this service entry.', 'victheme_services'),
      )))
      ->getParent()
      ->addChildren(new VTCore_Bootstrap_Element_BsElement(array(
        'type' => 'div',
        'grids' => array(
          'columns' => array(
            'mobile' => '12',
            'tablet' => '8',
            'small' => '8',
            'large' => '8',
          ),
        ),
      )))
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()

      // Create proper currency select fields!
      ->addChildren(new VTCore_Wordpress_Form_WpCurrency(array(
        'text' => __('Currency', 'victheme_services'),
        'name' => $this->getContext('name') . '[price][currency]',
        'value' => $this->getContext('value.currency'),
        'grids' => array(
          'columns' => array(
            'mobile' => '4',
            'tablet' => '4',
            'small' => '4',
            'large' => '4',
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'text' => __('Price', 'victheme_services'),
        'name' => $this->getContext('name') . '[price][price]',
        'value' => $this->getContext('value.price'),
        'grids' => array(
          'columns' => array(
            'mobile' => '8',
            'tablet' => '8',
            'small' => '8',
            'large' => '8',
          ),
        ),
      )));
  }

}