<?php
/**
 * Class for building the video field element
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Services_Form_Display
extends VTCore_Bootstrap_Form_Base {

  protected $context = array(
    'type' => 'div',
    'attributes' => array(
      'class' => array(
        'row',
      ),
    ),
    'name' => 'services',
    'value' => array(
      'image' => false,
      'icon' => false,
    ),

  );


  private $video;

  /**
   * Overriding the parent buildElement method
   */
  public function buildElement() {

    parent::buildElement();

    $this
      ->addChildren(new VTCore_Bootstrap_Element_BsElement(array(
        'type' => 'div',
        'grids' => array(
          'columns' => array(
            'mobile' => '12',
            'tablet' => '4',
            'small' => '4',
            'large' => '4',
          ),
        ),
      )))
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Element_BsElement(array(
        'type' => 'h4',
        'text' => __('Display', 'victheme_services'),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsDescription(array(
        'text' => __('Select the icon and upload image to be used in services grid and carousel template', 'victheme_services'),
      )))
      ->getParent()
      ->addChildren(new VTCore_Bootstrap_Element_BsElement(array(
        'type' => 'div',
        'grids' => array(
          'columns' => array(
            'mobile' => '12',
            'tablet' => '8',
            'small' => '8',
            'large' => '8',
          ),
        ),
      )))
      ->lastChild()

      ->addChildren(new VTCore_Wordpress_Form_WpMedia(array(
        'text' => __('Image', 'victheme_directory'),
        'name' => $this->getContext('name') . '[display][image]',
        'value' => $this->getContext('value.image'),
        'data' => array(
          'type' => 'image',
          'title' => __('Select Image', 'victheme_services'),
          'button' =>__('Select Image', 'victheme_services'),
        ),
      )))
      // Change this to proper icon selector!
      ->addChildren(new VTCore_Wordpress_Form_WpIconSet(array(
        'name' => $this->getContext('name') . '[display][icon]',
        'value' => $this->getContext('value.icon'),
        'build' => array(
          'preview' => false,
          'picker' => true,
          'sizing' => false,
          'styling' => false,
          'border' => false,
        ),
      )));


  }

}