<?php
/**
 * Class for building the images field element
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Services_Form_Schedule
extends VTCore_Bootstrap_Form_Base {

  protected $context = array(

    'type' => 'div',
    'attributes' => array(
      'class' => array(
        'row',
      ),
    ),

    'name' => 'services',
    'value' => array(
      array(
        'location' => false,
        'start' => false,
        'end' => false,
        'days' => array(),
        'users' => array(),
      )
    ),
  );

  private $rows = array();
  protected $options = array();
  protected $defaults = array(
    'location' => false,
    'description' => false,
    'start' => false,
    'end' => false,
    'days' => array(),
    'users' => array(),
  );


  /**
   * Overriding parent method
   * @see VTCore_Html_Base::buildElement()
   */
  public function buildElement() {

    parent::buildElement();

    VTCore_Wordpress_Utility::loadAsset('jquery-table-manager');

    $this->buildRows();

    $this
      ->addChildren(new VTCore_Bootstrap_Grid_BsColumn(array(
        'grids' => array(
          'columns' => array(
            'mobile' => '12',
            'tablet' => '12',
            'small' => '12',
            'large' => '12',
          ),
        ),
      )))
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Element_BsElement(array(
        'type' => 'div',
        'attributes' => array(
          'class' => array('table-manager'),
        ),
      )))
      ->lastChild()
      ->Table(array(
        'headers' => array(
          ' ',
          __('Scheduled Services', 'victheme_services'),
          ' ',
        ),
        'rows' => $this->rows,
      ))
      ->Button(array(
        'text' => __('Add New Service', 'victheme_services'),
        'attributes' => array(
          'data-tablemanager-type' => 'addrow',
          'class' => array('button', 'button-large', 'button-primary'),
        ),
      ));
  }


  /**
   * Building the media table rows contents array
   */
  private function buildRows() {

    // When empty define the default value
    if (!$this->getContext('value')) {
      $this->addContext('value', array($this->defaults));
    }

    // Allow other plugin to define the valid user to use
    // services item
    $this->options = apply_filters('vtcore_services_valid_users', $this->options);

    // Fallback to simple user fetching
    if (empty($this->options)) {
      $users = get_users();
      $this->options = array();
      foreach ($users as $user) {
        $name = $user->get('display_name');
        if (empty($name)) {
          $name = $user->get('user_nicename');
        }
        if (empty($name)) {
          $name = $user->get('user_login');
        }
        $this->options[$user->ID] = $name;
      }
    }

    foreach ($this->getContext('value') as $key => $data) {

      $data = wp_parse_args($data, $this->defaults);

      // Draggable Icon
      $this->rows[$key][] = array(
        'content' => new VTCore_Bootstrap_Element_BsElement(array(
          'type' => 'span',
          'attributes' => array(
            'class' => array('drag-icon'),
          ),
        )),
        'attributes' => array(
          'class' => array('drag-element'),
        ),
      );

      $this->rows[$key][] = new VTCore_Bootstrap_Grid_BsRow(array(
        'children' => array(
          new VTCore_Bootstrap_Form_BsText(array(
            'text' => __('Location', 'victheme_services'),
            'name' => $this->getContext('name') . '[schedule][' . $key . '][location]',
            'value' => $data['location'],
            'grids' => array(
              'columns' => array(
                'mobile' => '12',
                'tablet' => '6',
                'small' => '6',
                'large' => '6',
              ),
            ),
          )),

          new VTCore_Bootstrap_Form_BsTime(array(
            'text' => __('Starting Time', 'victheme_services'),
            'name' => $this->getContext('name') . '[schedule][' . $key . '][start]',
            'value' => $data['start'],
            'grids' => array(
              'columns' => array(
                'mobile' => '12',
                'tablet' => '3',
                'small' => '3',
                'large' => '3',
              ),
            ),
          )),

          new VTCore_Bootstrap_Form_BsTime(array(
            'text' => __('Ending Time', 'victheme_services'),
            'name' => $this->getContext('name') . '[schedule][' . $key . '][end]',
            'value' => $data['end'],
            'grids' => array(
              'columns' => array(
                'mobile' => '12',
                'tablet' => '3',
                'small' => '3',
                'large' => '3',
              ),
            ),
          )),

          new VTCore_Bootstrap_Form_BsSelect(array(
            'text' => __('Occuring Day', 'victheme_services'),
            'description' => __('Hold ctrl+click to select multiple day.', 'victheme_services'),
            'multiple' => true,
            'name' => $this->getContext('name') . '[schedule][' . $key . '][days]',
            'data' => array(
              'toggle' => 'days',
            ),
            'attributes' => array(
              'class' => array(
                'clearfix',
                'clearboth',
                'clear',
              ),
            ),
            'value' => $data['days'],
            'options' => array(
              'monday' => __('Monday', 'victheme_services'),
              'tuesday' => __('Tuesday', 'victheme_services'),
              'wednesday' => __('Wednesday', 'victheme_services'),
              'thursday' => __('Thursday', 'victheme_services'),
              'friday' => __('Friday', 'victheme_services'),
              'saturday' => __('Saturday', 'victheme_services'),
              'sunday' => __('Sunday', 'victheme_services'),
              'holiday' => __('Holiday', 'victheme_services'),
            ),
            'grids' => array(
              'columns' => array(
                'mobile' => '12',
                'tablet' => '6',
                'small' => '6',
                'large' => '6',
              ),
            ),
          )),

          new VTCore_Bootstrap_Form_BsSelect(array(
            'text' => __('Participating Users', 'victheme_services'),
            'description' => __('Hold ctrl+click to select multiple users.', 'victheme_services'),
            'multiple' => true,
            'name' => $this->getContext('name') . '[schedule][' . $key . '][users]',
            'value' => $data['users'],
            'options' => $this->options,
            'grids' => array(
              'columns' => array(
                'mobile' => '12',
                'tablet' => '6',
                'small' => '6',
                'large' => '6',
              ),
            ),
          )),

          new VTCore_Bootstrap_Form_BsTextarea(array(
            'text' => __('Description', 'victheme_services'),
            'raw' => true,
            'name' => $this->getContext('name') . '[schedule][' . $key . '][description]',
            'value' => $data['description'],
            'grids' => array(
              'columns' => array(
                'mobile' => '12',
                'tablet' => '12',
                'small' => '12',
                'large' => '12',
              ),
            ),
          )),
        )
      ));

      // Remove button
      $this->rows[$key][] = array(
        'content' => new VTCore_Form_Button(array(
          'text' => 'X',
          'attributes' => array(
            'data-tablemanager-type' => 'removerow',
            'class' => array('button', 'button-mini', 'form-button'),
          ),
        )),
        'attributes' => array(
          'class' => array('close-element'),
        ),
      );

    }

    return $this;
  }

}