<?php
/**
 * Class for building the main metabox wrapper
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Services_Form_Metabox
extends VTCore_Bootstrap_Element_BsTabs {

  /**
   * Overriding parent method
   * @see VTCore_Html_Base::buildElement()
   */
  public function buildElement() {

    VTCore_Wordpress_Utility::loadAsset('wp-bootstrap');
    VTCore_Wordpress_Utility::loadAsset('wp-media');
    VTCore_Wordpress_Utility::loadAsset('services-metabox');

    $this->addContext('values', get_post_meta(get_the_id(), '_services_data', true));

    $object = new VTCore_Bootstrap_Form_Base(array(
      'type' => false,
    ));

    $object->addChildren(new VTCore_Services_Form_Price(array(
      'value' => $this->getContext('values.price'),
    )));

    do_action('vtcore_services_form_information_alter', $object, $this);

    if ($object->getChildrens() != array()) {
      $this->addContext('contents.information', array(
        'title' => __('Information', 'victheme_services'),
        'content' => $object,
      ));
    }



    $object = new VTCore_Bootstrap_Form_Base(array(
      'type' => false,
    ));

    $object->addChildren(new VTCore_Services_Form_Schedule(array(
      'value' => $this->getContext('values.schedule'),
    )));

    do_action('vtcore_services_form_schedules_alter', $object, $this);

    if ($object->getChildrens() != array()) {
      $this->addContext('contents.schedules', array(
        'title' => __('Schedules', 'victheme_services'),
        'content' => $object,
      ));
    }




    $object = new VTCore_Bootstrap_Form_Base(array(
      'type' => false,
    ));

    $object->addChildren(new VTCore_Services_Form_Display(array(
      'value' => $this->getContext('values.display'),
    )));

    do_action('vtcore_services_form_display_alter', $object, $this);

    if ($object->getChildrens() != array()) {
      $this->addContext('contents.display', array(
        'title' => __('Display', 'victheme_services'),
        'content' => $object,
      ));
    }

    $this->addContext('active', apply_filters('vtcore_services_default_active_tabs', 'information'));

    unset($object);
    $object = null;


    do_action('vtcore_services_form_metabox_alter', $this);

    parent::buildElement();

  }

}