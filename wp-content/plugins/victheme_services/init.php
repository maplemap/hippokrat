<?php
/**
 * Main initialization class for properly
 * booting the Services plugin.
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Services_Init {

  private $autoloader;

  private $actions = array(
    //'widgets_init',
    'save_post',
    'the_post',
    'init',
    'vtcore_headline_add_configuration_panel',
  );

  private $filters = array(
    'vtcore_headline_alter',
    'vtcore_headline_configuration_context_alter',
    'enter_title_here',
  );


  public function __construct() {

    // Booting autoloader
    $this->autoloader = new VTCore_Autoloader('VTCore_Services', dirname(__FILE__));
    $this->autoloader->setRemovePath('vtcore' . DIRECTORY_SEPARATOR . 'services' . DIRECTORY_SEPARATOR);
    $this->autoloader->register();

    // Autoload translation for vtcore
    load_plugin_textdomain('victheme_services', false, 'victheme_services/languages');

    // Registering assets
    VTCore_Wordpress_Init::getFactory('assets')
      ->get('library')
      ->detect(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets', plugins_url('', __FILE__) . '/assets');


    // Registering actions
    VTCore_Wordpress_Init::getFactory('filters')
      ->addPrefix('VTCore_Services_Filters_')
      ->addHooks($this->filters)
      ->register();


    // Registering filters
    VTCore_Wordpress_Init::getFactory('actions')
      ->addPrefix('VTCore_Services_Actions_')
      ->addHooks($this->actions)
      ->register();

  }

}