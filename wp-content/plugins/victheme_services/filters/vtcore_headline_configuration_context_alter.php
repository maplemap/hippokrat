<?php
/**
 * Hooking into vtcore_headline_configuration_context_alter filter
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Services_Filters_VTCore__Headline__Configuration__Context__Alter
extends VTCore_Wordpress_Models_Hook {

  public function hook($context = NULL) {

    $context['services'] = array(
      'general' => array(
        'enable' => true,
        'title' => __('Services', 'victheme_services'),
        'subtitle' => __('Browse our servicess', 'victheme_services'),
        'headline_columns' => array(
          'mobile' => '12',
          'tablet' => '6',
          'small' => '6',
          'large' => '6',
        ),
        'item_columns' => array(
          'mobile' => '0',
          'tablet' => '6',
          'small' => '6',
          'large' => '6',
        ),
      ),
    );

    return $context;
  }
}