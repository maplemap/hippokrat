<?php
/**
 * Hooking into enter_title_here to modify
 * the text in the post edit page.
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Services_Filters_Enter__Title__Here
extends VTCore_Wordpress_Models_Hook {

  public function hook($title = NULL) {
    $screen = get_current_screen();
    return ($screen->post_type == 'services') ? __('Enter Services Title', 'victheme_services') : $title;
  }

}