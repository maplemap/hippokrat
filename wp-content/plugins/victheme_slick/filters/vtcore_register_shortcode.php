<?php
/**
 * Hooking into vtcore_register_shortcode filter
 * to register visualcandy related shortcodes
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Slick_Filters_VTCore__Register__Shortcode
extends VTCore_Wordpress_Models_Hook {

  public function hook($shortcodes = NULL) {

    // Advanced Carousels set
    $shortcodes[] = 'slickcarousel';
    $shortcodes[] = 'slickfilter';
    $shortcodes[] = 'slickpager';
    $shortcodes[] = 'slickquery';
    $shortcodes[] = 'slickthumbnail';
    $shortcodes[] = 'slicknext';
    $shortcodes[] = 'slickprev';

    // Content Carousel set
    $shortcodes[] = 'slickwrapper';
    $shortcodes[] = 'slickslide';

    // Simple Carousel set
    $shortcodes[] = 'slicksimple';

    return $shortcodes;
  }
}