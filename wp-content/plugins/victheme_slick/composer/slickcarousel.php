<?php
/**
 * Registering slickcarousel shortcode to visualcomposer
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Slick_Composer_SlickCarousel
extends VTCore_Wordpress_Models_VC {

  
  public function registerVC() {

    $options = array(
      'name' => __('Advanced Carousel', 'victheme_slick'),
      'description' => __('Slick elements container', 'victheme_slick'),
      'base' => 'slickcarousel',
      'icon' => 'icon-slickcarousel',
      'category' => __('Slick Carousel', 'victheme_slick'),
      'deprecated' => false,
      'is_container' => true,
      'js_view' => 'VTCoreContainer',
      'as_child' => array(
        'except' => 'slickcarousel',
      ),
      'params' => array()
    );

    $options['params'][] = array(
      'type' => 'textfield',
      'heading' => __('CSS ID', 'victheme_slick'),
      'description' => __('Valid CSS ID for the main wrapper element.', 'victheme_slick'),
      'param_name' => 'id',
      'admin_label' => true,
    );

    $options['params'][] = array(
      'type' => 'textfield',
      'heading' => __('CSS Class', 'victheme_slick'),
      'description' => __('Valid CSS Class for the main wrapper element.', 'victheme_slick'),
      'param_name' => 'class',
      'admin_label' => true,
    );

    $options['params'][] = array(
      'type' => 'dropdown',
      'heading' => __( 'CSS Animation', 'victheme_slick'),
      'param_name' => 'css_animation',
      'admin_label' => true,
      'value' => array(
        __('No', 'victheme_slick') => '',
        __('Top to bottom', 'victheme_slick') => 'top-to-bottom',
        __('Bottom to top', 'victheme_slick') => 'bottom-to-top',
        __('Left to right', 'victheme_slick') => 'left-to-right',
        __('Right to left', 'victheme_slick') => 'right-to-left',
        __('Appear from center', 'victheme_slick') => "appear"
      ),
      'description' => __('Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'victheme_slick'),
    );

    $keys = array('columns', 'offset', 'push', 'pull');
    $sizes = array('mobile', 'tablet', 'small', 'large');
    foreach ($keys as $key) {
      foreach ($sizes as $size) {

        $name =  'grids___' . $key .'___' . $size;

        $value = 0;
        if ($key == 'columns') {
          $value = 12;
        }

        $options['params'][] = array(
          'type' => 'vtcore',
          'param_name' => $name,
          'name' => $name,
          'core_class' => 'VTCore_Bootstrap_Form_BsSelect',
          'edit_field_class' => 'vc_col-xs-3',
          'heading' => ucfirst($key) . ' ' . ucfirst($size),
          'core_context' => array(
            'name' => $name,
            'value' => $value,
            'input_elements' => array(
              'attributes' => array(
                'class' => array('wpb_vc_param_value', 'wpb-dropdown', 'icon', 'vtcore_field')
              ),
            ),
            'options' => range(0, 12, 1),
          ),
        );
      }
    }

    $keys = array('columns');
    $sizes = array('mobile', 'tablet', 'small', 'large');
    foreach ($keys as $key) {
      foreach ($sizes as $size) {

        $name =  'slick___grids___' . $key .'___' . $size;

        $value = 0;
        if ($key == 'columns') {
          $value = 12;
        }

        $options['params'][] = array(
          'type' => 'vtcore',
          'group' => __('Carousel', 'victheme_slick'),
          'param_name' => $name,
          'name' => $name,
          'core_class' => 'VTCore_Bootstrap_Form_BsSelect',
          'edit_field_class' => 'vc_col-xs-6',
          'heading' => __('Slides To Show - ', 'victheme_slick') . ' ' . ucfirst($size),
          'core_context' => array(
            'name' => $name,
            'value' => $value,
            'input_elements' => array(
              'attributes' => array(
                'class' => array('wpb_vc_param_value', 'wpb-dropdown', 'icon', 'vtcore_field')
              ),
            ),
            'options' => range(0, 12, 1),
          ),
        );
      }
    }


    $options['params'][] = array(
      'type' => 'textfield',
      'param_name' => 'slick___slidestoscroll',
      'heading' => __('Slide To Scroll', 'victheme_slick'),
      'description' => __('# of slides to scroll at a time', 'victheme_slick'),
      'edit_field_class' => 'vc_col-xs-4 clearboth',
      'value' => 1,
      'admin_label' => true,
      'group' => __('Carousel', 'victheme_slick'),
    );

    $options['params'][] = array(
      'type' => 'dropdown',
      'heading' => __('Center Mode', 'victheme_slick'),
      'param_name' => 'slick___centermode',
      'description' => __('Enables centered view with partial prev/next slides. Use with odd numbered slidesToShow counts.', 'victheme_slick'),
      'value' => array(
        __('Disable', 'victheme_slick') => 'false',
        __('Enable', 'victheme_slick') => 'true',
      ),
      'admin_label' => true,
      'edit_field_class' => 'vc_col-xs-4',
      'group' => __('Carousel', 'victheme_slick'),
    );

    $options['params'][] = array(
      'type' => 'textfield',
      'param_name' => 'slick___centerpadding',
      'heading' => __('Center Padding', 'victheme_slick'),
      'description' => __('Side padding when in center mode. (px or %)', 'victheme_slick'),
      'edit_field_class' => 'vc_col-xs-4',
      'value' => '50px',
      'admin_label' => true,
      'group' => __('Carousel', 'victheme_slick'),
    );

    $options['params'][] = array(
      'type' => 'dropdown',
      'param_name' => 'slick___variablewidth',
      'heading' => __('Variable Width', 'victheme_slick'),
      'description' => __('If disabled carousel will resize the slide content according to fit frame width', 'victheme_slick'),
      'edit_field_class' => 'vc_col-xs-4  clearboth',
      'value' => array(
        __('Disable', 'victheme_slick') => 'false',
        __('Enable', 'victheme_slick') => 'true',
      ),
      'admin_label' => true,
      'group' => __('Carousel', 'victheme_slick'),
    );

    $options['params'][] = array(
      'type' => 'dropdown',
      'param_name' => 'slick___autoplay',
      'heading' => __('Autoplay', 'victheme_slick'),
      'description' => __('Enables auto play of slides', 'victheme_slick'),
      'edit_field_class' => 'vc_col-xs-4',
      'value' => array(
        __('Disable', 'victheme_slick') => 'false',
        __('Enable', 'victheme_slick') => 'true',
      ),
      'admin_label' => true,
      'group' => __('Carousel', 'victheme_slick'),
    );

    $options['params'][] = array(
      'type' => 'dropdown',
      'param_name' => 'slick___pauseonhover',
      'heading' => __('Pause On Hover', 'victheme_slick'),
      'description' => __('Pauses autoplay on hover', 'victheme_slick'),
      'edit_field_class' => 'vc_col-xs-4',
      'value' => array(
        __('Disable', 'victheme_slick') => 'false',
        __('Enable', 'victheme_slick') => 'true',
      ),
      'admin_label' => true,
      'group' => __('Carousel', 'victheme_slick'),
    );

    $options['params'][] = array(
      'type' => 'textfield',
      'param_name' => 'slick___autoplayspeed',
      'heading' => __('Autoplay Speed', 'victheme_slick'),
      'description' => __('Auto play change interval in miliseconds', 'victheme_slick'),
      'edit_field_class' => 'vc_col-xs-4  clearboth',
      'value' => '1000',
      'admin_label' => true,
      'group' => __('Carousel', 'victheme_slick'),
    );

    $options['params'][] = array(
      'type' => 'dropdown',
      'param_name' => 'slick___infinite',
      'heading' => __('Infinite', 'victheme_slick'),
      'description' => __('Infinite looping, the preview for infinite mode is disabled while on front edit mode.', 'victheme_slick'),
      'edit_field_class' => 'vc_col-xs-4',
      'value' => array(
        __('Disable', 'victheme_slick') => 'false',
        __('Enable', 'victheme_slick') => 'true',
      ),
      'admin_label' => true,
      'group' => __('Carousel', 'victheme_slick'),
    );

    $options['params'][] = array(
      'type' => 'dropdown',
      'param_name' => 'slick___cssease',
      'heading' => __('CSS3 Easing', 'victheme_slick'),
      'description' => __('CSS3 easing.', 'victheme_slick'),
      'edit_field_class' => 'vc_col-xs-4 ',
      'value' => array(
        'Ease' => 'ease',
        'Linear' => 'linear',
        'Ease in' => 'ease-in',
        'Ease out' => 'ease-out',
        'Ease in out' => 'ease-in-out',
      ),
      'admin_label' => true,
      'group' => __('Carousel', 'victheme_slick'),
    );

    $options['params'][] = array(
      'type' => 'textfield',
      'param_name' => 'slick___speed',
      'heading' => __('Speed', 'victheme_slick'),
      'description' => __('Transition speed', 'victheme_slick'),
      'edit_field_class' => 'vc_col-xs-4 clearboth',
      'value' => 600,
      'admin_label' => true,
      'group' => __('Carousel', 'victheme_slick'),
    );


    // Mark advanced mode disable if problematic!
    $options['params'][] = array(
      'type' => 'dropdown',
      'param_name' => 'slick___accessibility',
      'heading' => __('Accessibility', 'victheme_slick'),
      'description' => __('Enables tabbing and arrow key navigation', 'victheme_slick'),
      'edit_field_class' => 'vc_col-xs-4',
      'value' => array(
        __('Disable', 'victheme_slick') => 'false',
        __('Enable', 'victheme_slick') => 'true',
      ),
      'admin_label' => true,
      'group' => __('Carousel', 'victheme_slick'),
    );

    $options['params'][] = array(
      'type' => 'dropdown',
      'param_name' => 'slick___draggable',
      'heading' => __('Draggable', 'victheme_slick'),
      'description' => __('Enables desktop dragging', 'victheme_slick'),
      'edit_field_class' => 'vc_col-xs-4',
      'value' => array(
        __('Disable', 'victheme_slick') => 'false',
        __('Enable', 'victheme_slick') => 'true',
      ),
      'admin_label' => true,
      'group' => __('Carousel', 'victheme_slick'),
    );

    $options['params'][] = array(
      'type' => 'dropdown',
      'param_name' => 'slick___fade',
      'heading' => __('Fade', 'victheme_slick'),
      'description' => __('Enables fade', 'victheme_slick'),
      'edit_field_class' => 'vc_col-xs-4  clearboth visible',
      'value' => array(
        __('Disable', 'victheme_slick') => 'false',
        __('Enable', 'victheme_slick') => 'true',
      ),
      'admin_label' => true,
      'group' => __('Carousel', 'victheme_slick'),
    );

    $options['params'][] = array(
      'type' => 'dropdown',
      'param_name' => 'slick___swipe',
      'heading' => __('Swipe', 'victheme_slick'),
      'description' => __('Enables touch swipe', 'victheme_slick'),
      'edit_field_class' => 'vc_col-xs-4',
      'value' => array(
        __('Disable', 'victheme_slick') => 'false',
        __('Enable', 'victheme_slick') => 'true',
      ),
      'admin_label' => true,
      'group' => __('Carousel', 'victheme_slick'),
    );

    $options['params'][] = array(
      'type' => 'dropdown',
      'param_name' => 'slick___touchmove',
      'heading' => __('Touch Move', 'victheme_slick'),
      'description' => __('Enables slide moving with touch', 'victheme_slick'),
      'edit_field_class' => 'vc_col-xs-4',
      'value' => array(
        __('Disable', 'victheme_slick') => 'false',
        __('Enable', 'victheme_slick') => 'true',
      ),
      'admin_label' => true,
      'group' => __('Carousel', 'victheme_slick'),
    );

    $options['params'][] = array(
      'type' => 'textfield',
      'param_name' => 'slick___touchthreshold',
      'heading' => __('Touch Threshold', 'victheme_slick'),
      'description' => __('Swipe distance threshold', 'victheme_slick'),
      'edit_field_class' => 'vc_col-xs-4',
      'value' => 5,
      'admin_label' => true,
      'group' => __('Carousel', 'victheme_slick'),
    );

    $options['params'][] = array(
      'type' => 'dropdown',
      'param_name' => 'slick___vertical',
      'heading' => __('Vertical', 'victheme_slick'),
      'description' => __('Vertical slide direction, This options can break the live previewer.', 'victheme_slick'),
      'edit_field_class' => 'vc_col-xs-4',
      'value' => array(
        __('Disable', 'victheme_slick') => 'false',
        __('Enable', 'victheme_slick') => 'true',
      ),
      'group' => __('Carousel', 'victheme_slick'),
    );

    $options['params'][] = array(
      'type' => 'dropdown',
      'param_name' => 'slick___equalheight',
      'heading' => __('Equalheight', 'victheme_slick'),
      'description' => __('Force all slides to have equal height', 'victheme_slick'),
      'edit_field_class' => 'vc_col-xs-4',
      'value' => array(
        __('Disable', 'victheme_slick') => 'false',
        __('Enable', 'victheme_slick') => 'true',
      ),
      'group' => __('Carousel', 'victheme_slick'),
    );

    $options['params'][] = array(
      'type' => 'dropdown',
      'param_name' => 'slick___adaptiveheight',
      'heading' => __('Adaptive Height', 'victheme_slick'),
      'description' => __('Force slides track to adjust the height according to current slide height and will force slider to show only 1 slide at the time', 'victheme_slick'),
      'edit_field_class' => 'vc_col-xs-4',
      'value' => array(
        __('Disable', 'victheme_slick') => 'false',
        __('Enable', 'victheme_slick') => 'true',
      ),
      'group' => __('Carousel', 'victheme_slick'),
    );

    // End advanced mode

    $taxonomies = get_taxonomies(array('public' => true));
    $tax = array(
      __('--- Select ---', 'victheme_slick') => '',
    );
    foreach ($taxonomies as $taxonomy) {
      $tax[$taxonomy] = $taxonomy;
    }

    $options['params'][] = array(
      'type' => 'dropdown',
      'param_name' => 'taxonomy',
      'heading' => __('Taxonomy for filter element', 'victheme_slick'),
      'description' => __('Define the taxonomy for filtering elements', 'victheme_slick'),
      'value' => $tax,
      'admin_label' => true,
      'group' => __('Query', 'victheme_slick'),
    );

    $class = '';
    if (!get_theme_support('bootstrap')) {
      $class .= 'vc_col-xs-12 no-bootstrap';
    }
    $options['params'][] = array(
      'type' => 'vt_query_form',
      'heading' => __('Query Parameter', 'victheme_slick'),
      'param_name' => 'queryargs',
      'value' => '',
      'edit_field_class' => $class,
      'description' => __('Post query parameter settings for retrieving carousel items', 'victheme_slick'),
      'group' => __('Query', 'victheme_slick'),
    );

    // CSS Editor
    $options['params'][] = array(
      'type' => 'css_editor',
      'heading' => __('Css', 'victheme_slick'),
      'param_name' => 'css',
      'group' => __('Design options', 'victheme_slick'),
    );

    return $options;
  }
}


class WPBakeryShortCode_SlickCarousel extends WPBakeryShortCodesContainer {}