<?php
/**
 * Registering slickthumbnail shortcode to visualcomposer
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Slick_Composer_SlickThumbnail
extends VTCore_Wordpress_Models_VC {

  
  public function registerVC() {

    $options = array(
      'name' => __('Slick Thumbnail', 'victheme_slick'),
      'description' => __('Slick thumbnail navigation', 'victheme_slick'),
      'base' => 'slickthumbnail',
      'icon' => 'icon-slickthumbnail',
      'category' => __('Slick Carousel', 'victheme_slick'),
      'is_container' => false,
      'as_child' => array(
        'only' => 'vc_column_inner, slickcarousel',
      ),
      'params' => array()
    );


    $templates = array_flip(apply_filters('vtcore_slick_register_template_thumb_items', array(
      'slick-thumb.php' => __('Default using featured image', 'victheme_slick'),
    )));

    $empty = array_flip(apply_filters('vtcore_slick_register_template_thumb_empty', array(
      'slick-thumb-empty.php' => __('Default empty messages', 'victheme_slick'),
    )));

    $options['params'][] = array(
      'type' => 'dropdown',
      'param_name' => 'template___items',
      'heading' => __('Thumbnail Template', 'victheme_slick'),
      'description' => __('Template to use for processing the slide items', 'victheme_slick'),
      'value' => $templates,
    );

    $options['params'][] = array(
      'type' => 'dropdown',
      'param_name' => 'template___empty',
      'heading' => __('Empty Template', 'victheme_slick'),
      'description' => __('Template to use when thumbnail found no valid slides', 'victheme_slick'),
      'value' => $empty,
    );

    $options['params'][] = array(
      'type' => 'dropdown',
      'param_name' => 'slick___vertical',
      'heading' => __('Vertical', 'victheme_slick'),
      'description' => __('Vertical slide direction, This options can break the live previewer.', 'victheme_slick'),
      'value' => array(
        __('Disable', 'victheme_slick') => 'false',
        __('Enable', 'victheme_slick') => 'true',
      ),
    );

    $keys = array('columns');
    $sizes = array('mobile', 'tablet', 'small', 'large');
    foreach ($keys as $key) {
      foreach ($sizes as $size) {

        $name =  'grids___' . $key .'___' . $size;

        $value = 0;
        if ($key == 'columns') {
          $value = 12;
        }

        $options['params'][] = array(
          'type' => 'vtcore',
          'param_name' => $name,
          'name' => $name,
          'core_class' => 'VTCore_Bootstrap_Form_BsSelect',
          'edit_field_class' => 'vc_col-xs-3',
          'heading' => __('Thumbnail ', 'victheme_slick') . ucfirst($key) . ' ' . ucfirst($size),
          'core_context' => array(
            'name' => $name,
            'value' => $value,
            'input_elements' => array(
              'attributes' => array(
                'class' => array('wpb_vc_param_value', 'wpb-dropdown', 'icon', 'vtcore_field')
              ),
            ),
            'options' => range(0, 12, 1),
          ),
        );
      }
    }

    $options['params'][] = array(
      'type' => 'textfield',
      'heading' => __('CSS ID', 'victheme_slick'),
      'param_name' => 'id',
      'admin_label' => true,
    );

    $options['params'][] = array(
      'type' => 'textfield',
      'heading' => __('CSS Class', 'victheme_slick'),
      'param_name' => 'class',
      'admin_label' => true,
    );

    $options['params'][] = array(
      'type' => 'dropdown',
      'heading' => __( 'CSS Animation', 'victheme_slick'),
      'param_name' => 'css_animation',
      'admin_label' => true,
      'value' => array(
        __('No', 'victheme_slick') => '',
        __('Top to bottom', 'victheme_slick') => 'top-to-bottom',
        __('Bottom to top', 'victheme_slick') => 'bottom-to-top',
        __('Left to right', 'victheme_slick') => 'left-to-right',
        __('Right to left', 'victheme_slick') => 'right-to-left',
        __('Appear from center', 'victheme_slick') => "appear"
      ),
      'description' => __('Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'victheme_slick'),
    );


    // CSS Editor
    $options['params'][] = array(
      'type' => 'css_editor',
      'heading' => __('Css', 'victheme_slick'),
      'param_name' => 'css',
      'group' => __('Design options', 'victheme_slick'),
    );
    
    return $options;
  }
}


class WPBakeryShortCode_SlickThumbnail extends WPBakeryShortCode {}