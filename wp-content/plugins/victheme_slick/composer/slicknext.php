<?php
/**
 * Registering slicknext shortcode to visualcomposer
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Slick_Composer_SlickNext
extends VTCore_Wordpress_Models_VC {

  
  public function registerVC() {

    $options = array(
      'name' => __('Slick Next', 'victheme_slick'),
      'description' => __('Slick elements next button', 'victheme_slick'),
      'base' => 'slicknext',
      'icon' => 'icon-slicknext',
      'category' => __('Slick Carousel', 'victheme_slick'),
      'is_container' => false,
      'as_child' => array(
        'only' => 'vc_column_inner, slickcarousel',
      ),
      'params' => array()
    );

    $options['params'][] = array(
      'type' => 'textfield',
      'heading' => __('CSS ID', 'victheme_slick'),
      'param_name' => 'id',
      'admin_label' => true,
    );

    $options['params'][] = array(
      'type' => 'textfield',
      'heading' => __('CSS Class', 'victheme_slick'),
      'param_name' => 'class',
      'admin_label' => true,
    );

    $options['params'][] = array(
      'type' => 'dropdown',
      'heading' => __( 'CSS Animation', 'victheme_slick'),
      'param_name' => 'css_animation',
      'admin_label' => true,
      'value' => array(
        __('No', 'victheme_slick') => '',
        __('Top to bottom', 'victheme_slick') => 'top-to-bottom',
        __('Bottom to top', 'victheme_slick') => 'bottom-to-top',
        __('Left to right', 'victheme_slick') => 'left-to-right',
        __('Right to left', 'victheme_slick') => 'right-to-left',
        __('Appear from center', 'victheme_slick') => "appear"
      ),
      'description' => __('Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'victheme_slick'),
    );

    // CSS Editor
    $options['params'][] = array(
      'type' => 'css_editor',
      'heading' => __('Css', 'victheme_slick'),
      'param_name' => 'css',
      'group' => __('Design options', 'victheme_slick'),
    );
    
    return $options;
  }
}


class WPBakeryShortCode_SlickNext extends WPBakeryShortCode {}