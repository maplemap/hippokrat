<?php
/**
 * Registering slickquery shortcode to visualcomposer
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Slick_Composer_SlickQuery
extends VTCore_Wordpress_Models_VC {

  
  public function registerVC() {

    $options = array(
      'name' => __('Slick Slides', 'victheme_slick'),
      'description' => __('Slick slides using query', 'victheme_slick'),
      'base' => 'slickquery',
      'icon' => 'icon-slickquery',
      'category' => __('Slick Carousel', 'victheme_slick'),
      'is_container' => false,
      'as_child' => array(
        'only' => 'vc_column_inner, slickcarousel',
      ),
//      'params' => array()
    );

    $templates = array_flip(apply_filters('vtcore_slick_register_template_items', array(
      'slick-carousel.php' => __('Default carousel', 'victheme_slick'),
    )));

    $empty = array_flip(apply_filters('vtcore_slick_register_template_empty', array(
      'slick-empty.php' => __('Default carousel', 'victheme_slick'),
    )));

    $options['params'][] = array(
      'type' => 'dropdown',
      'param_name' => 'template___items',
      'heading' => __('Slide Template', 'victheme_slick'),
      'description' => __('Template to use for processing the slide items', 'victheme_slick'),
      'value' => $templates,
    );

    $options['params'][] = array(
      'type' => 'dropdown',
      'param_name' => 'template___empty',
      'heading' => __('Empty Template', 'victheme_slick'),
      'description' => __('Template to use when query found no valid slides', 'victheme_slick'),
      'value' => $empty,
    );

    $options['params'][] = array(
      'type' => 'textfield',
      'heading' => __('CSS ID', 'victheme_slick'),
      'param_name' => 'id',
      'admin_label' => true,
    );

    $options['params'][] = array(
      'type' => 'textfield',
      'heading' => __('CSS Class', 'victheme_slick'),
      'param_name' => 'class',
      'admin_label' => true,
    );

    $options['params'][] = array(
      'type' => 'dropdown',
      'heading' => __( 'CSS Animation', 'victheme_slick'),
      'param_name' => 'css_animation',
      'admin_label' => true,
      'value' => array(
        __('No', 'victheme_slick') => '',
        __('Top to bottom', 'victheme_slick') => 'top-to-bottom',
        __('Bottom to top', 'victheme_slick') => 'bottom-to-top',
        __('Left to right', 'victheme_slick') => 'left-to-right',
        __('Right to left', 'victheme_slick') => 'right-to-left',
        __('Appear from center', 'victheme_slick') => "appear"
      ),
      'description' => __('Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'victheme_slick'),
    );

    $options['params'][] = array(
      'type' => 'hidden',
      'param_name' => 'css_animation',
      'value' => '',
    );

    // CSS Editor
    $options['params'][] = array(
      'type' => 'css_editor',
      'heading' => __('Css', 'victheme_slick'),
      'param_name' => 'css',
      'group' => __('Design options', 'victheme_slick'),
    );
    
    return $options;
  }
}


class WPBakeryShortCode_SlickQuery extends WPBakeryShortCode {}