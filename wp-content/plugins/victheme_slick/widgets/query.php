<?php
/**
 * Class for building the slick carousel widget.
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Slick_Widgets_Query
extends WP_Widget {

  private $defaults = array(
    'title' => '',
    'description' => '',
    'id' => '',
    'template' => array(
      'items' => 'slick-carousel.php',
      'empty' => 'slick-empty.php',
    ),
    'queryArgs' => array(
      'posts' => array(
        'post_type' => array(
          'post'
        ),
        'post_status' => array(
          'publish'
        ),
      ),
      'pagination' => array(
        'posts_per_page' => 10,
      ),
    ),
    'slick' => array(
      'dots' => false,
      'arrows' => false,
      'slidesToScroll' => 1,
      'centerPadding' => '50px',
      'autoplaySpeed' => '1000',
      'cssEase' => 'ease',
      'speed' => 600,
      'touchThreshold' => 5,
      'centerMode' => false,
      'variableWidth' => false,
      'autoplay' => false,
      'pauseOnHover' => false,
      'infinite' => false,
      'accessibility' => false,
      'draggable' => false,
      'fade' => false,
      'swipe' => false,
      'touchmove' => false,
      'vertical' => false,
      'adaptiveHeight' => false,
      'equalheight' => false,
      'slidesToShow' => 4,
    ),
    'grids' => array(
      'columns' => array(
        'mobile' => 1,
        'tablet' => 2,
        'small' => 4,
        'large' => 6,
      )
    ),
    'convert_grids' => false,
  );


  private $form;
  


  /**
   * Registering widget as WP_Widget requirement
   */
  public function __construct() {
    parent::__construct(
        'vtcore_slick_widgets_query',
        'Slick Carousel - Post Query',
        array('description' => 'Content carousel with queried post as the slides')
    );
  }




  /**
   * Registering widget
   */
  public function registerWidget() {
    return register_widget('vtcore_slick_widgets_query');
  }





  /**
   * Extending widget
   *
   * @see WP_Widget::widget()
   */
  public function widget($args, $instance) {

    $this->instance = VTCore_Utility::arrayMergeRecursiveDistinct($instance, $this->defaults);

    echo $args['before_widget'];

    $title = $this->instance['title'];

    if (!empty($title)) {
      echo $args['before_title'] . $title . $args['after_title'];
    }

    if (!empty($this->instance['description'])) {
      $object = new VTCore_Html_Element(array(
        'type' => 'div',
        'text' => $this->instance['description'],
        'attributes' => array(
          'class' => array(
            'vtcore-slick-description'
          ),
        ),
        'raw' => true,
      ));

      $object->render();
    }
    $carousel = new VTCore_Wordpress_Element_WpCarousel($this->instance);
    $carousel->render();

    echo $args['after_widget'];
  }





  /**
   * Widget configuration form
   * @see WP_Widget::form()
   */
  public function form($instance) {

    VTCore_Wordpress_Utility::loadAsset('wp-bootstrap');
    VTCore_Wordpress_Utility::loadAsset('wp-widget');

    $this->instance = VTCore_Utility::arrayMergeRecursiveDistinct($instance, $this->defaults);
    $this->buildForm()->processForm()->processError(true, false)->render();

  }



  /**
   * Form need to be in separated function for validation
   * purposes. Rebuild the form and get the error to validate.
   */
  private function buildForm() {

    $templates = apply_filters('vtcore_slick_register_template_items', array(
      'slick-carousel.php' => __('Default carousel', 'victheme_slick'),
    ));

    $empty = apply_filters('vtcore_slick_register_template_empty', array(
      'slick-empty.php' => __('Default carousel', 'victheme_slick'),
    ));

    if (empty($this->instance['id'])) {
      $this->instance['id'] = uniqid('vtcore-slick-query-widgets-');
    }

    $widget = new VTCore_Bootstrap_Form_BsInstance(array(
      'type' => false,
    ));

    // Title
    $widget
      ->BsText(array(
        'text' => __('Title', 'victheme_slick'),
        'name' => $this->get_field_name('title'),
        'id' => $this->get_field_id('title'),
        'value' => $this->instance['title'],
      ))
      ->BsTextarea(array(
        'text' => __('Description', 'victheme_slick'),
        'name' => $this->get_field_name('description'),
        'id' => $this->get_field_id('description'),
        'value' => $this->instance['description'],
      ))
      ->BsText(array(
        'text' => __('Query ID', 'victheme_slick'),
        'name' => $this->get_field_name('id'),
        'description' => __('Unique query id for filtering purposes when combined with other victheme filtering element.', 'victheme_slick'),
        'id' => $this->get_field_id('id'),
        'value' => $this->instance['id'],
      ))
      ->BsSelect(array(
        'text' => __('Item Template', 'victheme_slick'),
        'name' => $this->get_field_name('template][items'),
        'id' => $this->get_field_id('template][items'),
        'value' => $this->instance['template']['items'],
        'options' => $templates,
      ))
      ->BsSelect(array(
        'text' => __('Empty Template', 'victheme_slick'),
        'name' => $this->get_field_name('template][empty'),
        'id' => $this->get_field_id('template][empty'),
        'value' => $this->instance['template']['empty'],
        'options' => $empty,
      ))
      ->addChildren(new VTCore_Wordpress_Form_WpQuery(array(
        'text' => __('Query Parameter', 'victheme_slick'),
        'name' =>  $this->get_field_name('queryArgs'),
        'id' => $this->get_field_id('queryArgs'),
        'value' => $this->instance['queryArgs'],
      )))
      ->BsCheckbox(array(
        'text' => __('Center Mode', 'victheme_slick'),
        'name' => $this->get_field_name('slick][centerMode'),
        'id' => $this->get_field_id('slick][centerMode'),
        'switch' => true,
        'checked' => (boolean) $this->instance['slick']['centerMode'],
      ))
      ->BsCheckbox(array(
        'text' => __('Variable Width', 'victheme_slick'),
        'name' => $this->get_field_name('slick][variableWidth'),
        'id' => $this->get_field_id('slick][variableWidth'),
        'switch' => true,
        'checked' => (boolean) $this->instance['slick']['variableWidth'],
      ))
      ->BsCheckbox(array(
        'text' => __('Auto Play', 'victheme_slick'),
        'name' => $this->get_field_name('slick][autoplay'),
        'id' => $this->get_field_id('slick][autoplay'),
        'switch' => true,
        'checked' => (boolean) $this->instance['slick']['autoplay'],
      ))
      ->BsCheckbox(array(
        'text' => __('Pause On Hover', 'victheme_slick'),
        'name' => $this->get_field_name('slick][pauseOnHover'),
        'id' => $this->get_field_id('slick][pauseOnHover'),
        'switch' => true,
        'checked' => (boolean) $this->instance['slick']['pauseOnHover'],
      ))
      ->BsCheckbox(array(
        'text' => __('Dots', 'victheme_slick'),
        'name' => $this->get_field_name('slick][dots'),
        'id' => $this->get_field_id('slick][dots'),
        'switch' => true,
        'checked' => (boolean) $this->instance['slick']['dots'],
      ))
      ->BsCheckbox(array(
        'text' => __('Arrows', 'victheme_slick'),
        'name' => $this->get_field_name('slick][arrows'),
        'id' => $this->get_field_id('slick][arrows'),
        'switch' => true,
        'checked' => (boolean) $this->instance['slick']['arrows'],
      ))
      ->BsCheckbox(array(
        'text' => __('Infinite Scrolling', 'victheme_slick'),
        'name' => $this->get_field_name('slick][infinite'),
        'id' => $this->get_field_id('slick][infinite'),
        'switch' => true,
        'checked' => (boolean) $this->instance['slick']['infinite'],
      ))
      ->BsSelect(array(
        'text' => __('CSS Easing', 'victheme_slick'),
        'name' => $this->get_field_name('slick][cssEase'),
        'id' => $this->get_field_id('slick][cssEase'),
        'value' => $this->instance['slick']['cssEase'],
        'options' => array(
          'ease' => 'Ease',
          'linear' => 'Linear',
          'ease-in' => 'Ease In',
          'ease-out' => 'Ease Out',
          'ease-in-out' => 'Ease In Out',
        ),
      ))
      ->BsText(array(
        'text' => __('Center Padding', 'victheme_slick'),
        'name' => $this->get_field_name('slick][centerPadding'),
        'id' => $this->get_field_id('slick][centerPadding'),
        'value' => $this->instance['slick']['centerPadding'],
      ))
      ->BsText(array(
        'text' => __('Auto Play Speed', 'victheme_slick'),
        'name' => $this->get_field_name('slick][autoplaySpeed'),
        'id' => $this->get_field_id('slick][autoplaySpeed'),
        'value' => $this->instance['slick']['autoplaySpeed'],
      ))
      ->BsNumber(array(
        'text' => __('Slide To Scroll', 'victheme_slick'),
        'name' => $this->get_field_name('slick][slidesToScroll'),
        'id' => $this->get_field_id('slick][slidesToScroll'),
        'value' => $this->instance['slick']['slidesToScroll'],
      ))
      ->BsSelect(array(
        'text' => __('Mobile - Slide To Show', 'victheme_slick'),
        'description' => __('Slides to show when on mobile devices', 'victheme_slick'),
        'name' => $this->get_field_name('grids][columns][mobile'),
        'value' => $this->instance['grids']['columns']['mobile'],
        'options' => range(0, 12, 1),
      ))
      ->BsSelect(array(
        'text' => __('Tablet - Slide To Show', 'victheme_slick'),
        'description' => __('Slides to show when on tablet devices', 'victheme_slick'),
        'name' => $this->get_field_name('grids][columns][tablet'),
        'value' => $this->instance['grids']['columns']['tablet'],
        'options' => range(0, 12, 1),
      ))
      ->BsSelect(array(
        'text' => __('Small - Slide To Show', 'victheme_slick'),
        'description' => __('Slides to show when on small desktop devices', 'victheme_slick'),
        'name' => $this->get_field_name('grids][columns][small'),
        'value' => $this->instance['grids']['columns']['small'],
        'options' => range(0, 12, 1),
      ))
      ->BsSelect(array(
        'text' => __('Large - Slide To Show', 'victheme_slick'),
        'description' => __('Slides to show when on large desktop devices', 'victheme_slick'),
        'name' => $this->get_field_name('grids][columns][large'),
        'value' => $this->instance['grids']['columns']['large'],
        'options' => range(0, 12, 1),
      ));

    return $widget;
  }
  

  /**
   * Widget update function.
   * @see WP_Widget::update()
   */
  public function update($new_instance, $old_instance) {

    $this->form = $this->buildForm()->processForm()->processError();
    $errors = $this->form->getErrors();
    if (empty($errors)) {
      return wp_unslash($new_instance);
    }

    return false;
  }
}