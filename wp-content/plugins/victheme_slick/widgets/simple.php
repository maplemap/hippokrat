<?php
/**
 * Class for building the slick carousel widget.
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Slick_Widgets_Simple
extends WP_Widget {

  private $defaults = array(
    'title' => '',
    'description' => '',
    'data' => array(
      'autoplay' => false,
      'autoplaySpeed' => 3000,
      'arrows' => true,
      'dots' => true,
      'centerMode' => false,
      'centerPadding' => '50px',
      'cssEase' => 'ease',
      'easing' => 'linear',
      'infinite' => true,
      'pauseOnHover' => true,
      'slidesToShow' => 1,
      'slidesToScroll' => 1,
      'variableWidth' => false,

      // Advanced options that cannot be set via GUI
      'accessibility' => false,
      'draggable' => true,
      'fade' => false,
      'swipe' => true,
      'touchMove' => true,
      'easing' => 'easeInOut',
      'lazyLoad' => 'ondemand',
      'touchTreshold' => 5,
      'vertical' => false,
      'speed' => 600,
      'adaptiveHeight' => true,
    ),
    'entries' => array(
      array(
        'image' => '',
        'text' => '',
        'link' => '',
      ),
    ),
  );


  private $form;


  protected $int = array(
    'slidesToShow',
    'slidesToScroll',
    'speed',
    'touchTreshold',
    'autoplaySpeed',
  );

  protected $booleans = array(
    'accessibility',
    'autoplay',
    'arrows',
    'dots',
    'centerMode',
    'draggable',
    'fade',
    'infinite',
    'pauseOnHover',
    'swipe',
    'touchMove',
    'vertical',
    'variableWidth',
    'adaptiveHeight',
  );


  /**
   * Registering widget as WP_Widget requirement
   */
  public function __construct() {
    parent::__construct(
        'vtcore_slick_widgets_simple',
        'Slick Carousel - Custom Content',
        array('description' => 'Carousel with simple custom content and images')
    );
  }




  /**
   * Registering widget
   */
  public function registerWidget() {
    return register_widget('vtcore_slick_widgets_simple');
  }





  /**
   * Extending widget
   *
   * @see WP_Widget::widget()
   */
  public function widget($args, $instance) {

    $this->instance = VTCore_Utility::arrayMergeRecursiveDistinct($instance, $this->defaults);

    echo $args['before_widget'];

    $title = $this->instance['title'];

    if (!empty($title)) {
      echo $args['before_title'] . $title . $args['after_title'];
    }

    if (!empty($this->instance['description'])) {
      $object = new VTCore_Html_Element(array(
        'type' => 'div',
        'text' => $this->instance['description'],
        'attributes' => array(
          'class' => array(
            'vtcore-slick-description'
          ),
        ),
        'raw' => true,
      ));

      $object->render();
    }

    foreach ($this->booleans as $key) {
      $this->instance['data'][$key] = (boolean) $this->instance['data'][$key];
    }

    foreach ($this->int as $key) {
      $this->instance['data'][$key] = (int) $this->instance['data'][$key];
    }


    $object = new VTCore_Html_Element(array(
      'type' => 'div',
      'data' => array(
        'settings' => $this->instance['data'],
      ),
      'attributes' => array(
        'class' => array(
          'vtcore-slick-carousel-instance',
          'slick-carousel',
        ),
      ),
    ));

    if ($instance['data']['dots']) {
      $object->addClass('with-dots', 'dots');
    }

    if ($instance['data']['arrows']) {
      $object->addClass('with-arrows', 'arrows');
    }

    foreach ($this->instance['entries'] as $entry) {
      if (isset($entry['image']) && !empty($entry['image']) && is_numeric($entry['image'])) {
        $image = new VTCore_Wordpress_Element_WpImage(array(
          'attachment_id' => $entry['image'],
          'size' => 'medium',
          'attributes' => array(
            'class' => array(
              'vtcore-slick-images',
            ),
          ),
        ));
      }

      if (isset($entry['text']) && !empty($entry['text'])) {
        $text = new VTCore_Html_Element(array(
          'type' => 'div',
          'text' => $entry['text'],
          'attributes' => array(
            'class' => array(
              'vtcore-slick-text',
            ),
          ),
        ));
      }

      if (isset($image) || isset($text)) {

        $wrapper = new VTCore_Html_Element(array(
          'type' => 'div',
          'attributes' => array(
            'class' => array(
              'vtcore-slick-content',
            ),
          ),
        ));

        if (isset($entry['link']) && !empty($entry['link'])) {
          $link = new VTCore_Html_Hyperlink(array(
            'attributes' => array(
              'href' => $entry['link'],
              'class' => array(
                'vtcore-slick-link',
              ),
            ),
          ));

          $wrapper->addChildren($link);

          if (isset($image)) {
            $wrapper->lastChild()->addChildren($image);
          }

          if (isset($text)) {
            $wrapper->lastChild()->addChildren($text);
          }

          unset($link);
        }

        else {
          if (isset($image)) {
            $wrapper->addChildren($image);
          }

          if (isset($text)) {
            $wrapper->addChildren($text);
          }
        }

        $object->addChildren($wrapper);

        unset($image);
        unset($text);
        unset($wrapper);

      }
    }

    if ($object->hasChildren()) {

      VTCore_Wordpress_Utility::loadAsset('jquery-slick');
      VTCore_Wordpress_Utility::loadAsset('slick-front');

      $object->render();
    }

    unset($object);

    echo $args['after_widget'];
  }





  /**
   * Widget configuration form
   * @see WP_Widget::form()
   */
  public function form($instance) {

    VTCore_Wordpress_Utility::loadAsset('wp-bootstrap');
    VTCore_Wordpress_Utility::loadAsset('jquery-table-manager');
    VTCore_Wordpress_Utility::loadAsset('wp-widget');

    $this->instance = VTCore_Utility::arrayMergeRecursiveDistinct($instance, $this->defaults);
    $this->buildForm()->processForm()->processError(true, false)->render();

  }



  /**
   * Form need to be in separated function for validation
   * purposes. Rebuild the form and get the error to validate.
   */
  private function buildForm() {


    $widget = new VTCore_Bootstrap_Form_BsInstance(array(
      'type' => false,
    ));

    // Title
    $widget
      ->BsText(array(
        'text' => __('Title', 'victheme_slick'),
        'name' => $this->get_field_name('title'),
        'id' => $this->get_field_id('title'),
        'value' => $this->instance['title'],
      ))
      ->BsTextarea(array(
        'text' => __('Description', 'victheme_slick'),
        'name' => $this->get_field_name('description'),
        'id' => $this->get_field_id('description'),
        'value' => $this->instance['description'],
      ))
      ->BsCheckbox(array(
        'text' => __('Center Mode', 'victheme_slick'),
        'name' => $this->get_field_name('data][centerMode'),
        'id' => $this->get_field_id('data][centerMode'),
        'switch' => true,
        'checked' => (boolean) $this->instance['data']['centerMode'],
      ))
      ->BsCheckbox(array(
        'text' => __('Variable Width', 'victheme_slick'),
        'name' => $this->get_field_name('data][variableWidth'),
        'id' => $this->get_field_id('data][variableWidth'),
        'switch' => true,
        'checked' => (boolean) $this->instance['data']['variableWidth'],
      ))
      ->BsCheckbox(array(
        'text' => __('Auto Play', 'victheme_slick'),
        'name' => $this->get_field_name('data][autoplay'),
        'id' => $this->get_field_id('data][autoplay'),
        'switch' => true,
        'checked' => (boolean) $this->instance['data']['autoplay'],
      ))
      ->BsCheckbox(array(
        'text' => __('Pause On Hover', 'victheme_slick'),
        'name' => $this->get_field_name('data][pauseOnHover'),
        'id' => $this->get_field_id('data][pauseOnHover'),
        'switch' => true,
        'checked' => (boolean) $this->instance['data']['pauseOnHover'],
      ))
      ->BsCheckbox(array(
        'text' => __('Dots', 'victheme_slick'),
        'name' => $this->get_field_name('data][dots'),
        'id' => $this->get_field_id('data][dots'),
        'switch' => true,
        'checked' => (boolean) $this->instance['data']['dots'],
      ))
      ->BsCheckbox(array(
        'text' => __('Arrows', 'victheme_slick'),
        'name' => $this->get_field_name('data][arrows'),
        'id' => $this->get_field_id('data][arrows'),
        'switch' => true,
        'checked' => (boolean) $this->instance['data']['arrows'],
      ))
      ->BsCheckbox(array(
        'text' => __('Infinite Scrolling', 'victheme_slick'),
        'name' => $this->get_field_name('data][infinite'),
        'id' => $this->get_field_id('data][infinite'),
        'switch' => true,
        'checked' => (boolean) $this->instance['data']['infinite'],
      ))
      ->BsSelect(array(
        'text' => __('CSS Easing', 'victheme_slick'),
        'name' => $this->get_field_name('data][cssEase'),
        'id' => $this->get_field_id('data][cssEase'),
        'value' => $this->instance['data']['cssEase'],
        'options' => array(
          'ease' => 'Ease',
          'linear' => 'Linear',
          'ease-in' => 'Ease In',
          'ease-out' => 'Ease Out',
          'ease-in-out' => 'Ease In Out',
        ),
      ))
      ->BsText(array(
        'text' => __('Center Padding', 'victheme_slick'),
        'name' => $this->get_field_name('data][centerPadding'),
        'id' => $this->get_field_id('data][centerPadding'),
        'value' => $this->instance['data']['centerPadding'],
      ))
      ->BsText(array(
        'text' => __('Auto Play Speed', 'victheme_slick'),
        'name' => $this->get_field_name('data][autoplaySpeed'),
        'id' => $this->get_field_id('data][autoplaySpeed'),
        'value' => $this->instance['data']['autoplaySpeed'],
      ))
      ->BsNumber(array(
        'text' => __('Slide To Show', 'victheme_slick'),
        'name' => $this->get_field_name('data][slidesToShow'),
        'id' => $this->get_field_id('data][slidesToShow'),
        'value' => $this->instance['data']['slidesToShow'],
      ))
      ->BsNumber(array(
        'text' => __('Slide To Scroll', 'victheme_slick'),
        'name' => $this->get_field_name('data][slidesToScroll'),
        'id' => $this->get_field_id('data][slidesToScroll'),
        'value' => $this->instance['data']['slidesToScroll'],
      ))
      ->BsElement(array(
        'type' => 'div',
        'attributes' => array(
          'class' => array('table-manager'),
        ),
      ))
      ->lastChild()
      ->Table(array(
        'headers' => array(
          ' ',
          __('Description', 'victheme_slick'),
          ' ',
        ),
        'rows' => $this->buildRows(),
        'attributes' => array(
          'data-filter' => 2,
          'class' => array(
            'form-table',
            'vtcore-widget-table'
          ),
        ),
      ))
      ->Button(array(
        'text' => __('Add New Entry', 'victheme_slick'),
        'attributes' => array(
          'data-tablemanager-type' => 'addrow',
          'class' => array('button', 'button-large', 'button-primary'),
        ),
      ));

    return $widget;
  }


  /**
   * Building the table manager rows
   */
  private function buildRows() {
    $rows = array();
    foreach ($this->instance['entries'] as $key => $link) {

      // Draggable Icon
      $rows[$key][] = array(
        'content' => new VTCore_Bootstrap_Element_BsElement(array(
          'type' => 'span',
          'attributes' => array(
            'class' => array('drag-icon'),
          ),
        )),
        'attributes' => array(
          'class' => array('drag-element'),
        ),
      );


      // Icon selector
      $rows[$key]['content'] = new VTCore_Bootstrap_Element_Base();
      $rows[$key]['content']
      ->addChildren(new VTCore_Wordpress_Form_WpMedia(array(
        'label' => __('Image', 'victheme_slick'),
        'value' => $link['image'],
        'name' => $this->get_field_name('entries][' . $key . '][image')
      )))
      ->BsText(array(
        'text' => __('Link URL', 'victheme_slick'),
        'name' => $this->get_field_name('entries][' . $key . '][link'),
        'value' => $link['link'],
      ))
      ->BsTextarea(array(
        'text' => __('Description', 'victheme_slick'),
        'name' => $this->get_field_name('entries][' . $key . '][text'),
        'value' => $link['text'],
      ));


      // Remove button
      $rows[$key][] = array(
        'content' => new VTCore_Form_Button(array(
          'text' => 'X',
          'attributes' => array(
            'data-tablemanager-type' => 'removerow',
            'class' => array('button', 'button-mini', 'form-button'),
          ),
        )),
        'attributes' => array(
          'class' => array('remove-button')
        ),
      );
    }

    return $rows;
  }

  /**
   * Widget update function.
   * @see WP_Widget::update()
   */
  public function update($new_instance, $old_instance) {

    $this->form = $this->buildForm()->processForm()->processError();
    $errors = $this->form->getErrors();
    if (empty($errors)) {
      return wp_unslash($new_instance);
    }

    return false;
  }
}