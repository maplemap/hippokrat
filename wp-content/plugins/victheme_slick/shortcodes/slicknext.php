<?php
/**
 * Shortcode for building the slick next arrow wrapper
 * and act as the javascript append target point
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Slick_Shortcodes_SlickNext
extends VTCore_Slick_Models_Shortcodes
implements VTCore_Wordpress_Interfaces_Shortcodes {

  protected $processDottedNotation = true;

  protected function processCustomRules() {

    // Convert the bootstrap classes into vc compatible one
    $this->convertVCGrid = !get_theme_support('bootstrap');

    $object = new VTCore_Wordpress_Objects_Array($this->atts);

    $object
      ->add('type', 'div')
      ->add('attributes.class.target', 'slick-next-target')
      ->add('data.slick-carousel-arrow-next', $this->accessQuery()->get('vtcore_queryid'));

    do_action('vtcore_slick_alter_next_button', $object, $this);

    $this->atts = $object->extract();
  }


  public function buildObject() {
    $this->object = new VTCore_Bootstrap_Element_BsElement($this->atts);
  }
}