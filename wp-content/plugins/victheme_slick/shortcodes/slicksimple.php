<?php
/**
 * Shortcode for building the slick carousel in simple
 * mode with slides taken from query.
 *
 * This shortcode will not process any child shortcodes.
 *
 *
 * [slicksimple
 *   queryargs="{json of valid WP_Query arguments}"
 *   filter___taxonomy="taxonomy name for the filter elements"
 *   slick___accessibility="true|false" #
 *   slick___autoplay="true|false" #
 *   slick___centermode="true|false" #
 *   slick___draggable="true|false" #
 *   slick___fade="true|false" #
 *   slick___pauseonhover="true|false" #
 *   slick___infinite="true|false" #
 *   slick___swipe="true|false" #
 *   slick___touchmove="true|false" #
 *   slick___vertical="true|false"  #
 *   slick___cssease="ease|ease-in|ease-out|ease-in-out" #
 *   slick___easing="swing|easeInQuad|easeOutQuad|easeInOutQuad|easeInCubic|easeOutCubic|easeInOutCubic|..." #
 *   slick___autoplayspeed="Number in miliseconds" #
 *   slick___centerpadding="xxpx" #
 *   slick___slidestoshow="number" #
 *   slick___slidestoscroll="number" #
 *   slick___speed="number in miliseconds" #
 *   slick___touchthreshold="number" #
 *   slick___variablewidth="true|false" #
 *   slick___equalheight="true|false"
 *   slick___dots="true|false"
 *   slick___arrows="true|false"
 *   slick___grids___columns___mobile="X"
 *   slick___grids___columns___tablet="X"
 *   slick___grids___columns___small="X"
 *   slick___grids___columns___large="X"
 *   template___item="the template name for the slick items"
 *   template___empty="the template name where no slick item found"
 *   ]
 *
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Slick_Shortcodes_SlickSimple
extends VTCore_Slick_Models_Shortcodes
implements VTCore_Wordpress_Interfaces_Shortcodes {

  protected $processDottedNotation = true;

  /**
   * VC 4.7 somehow wont record
   * the shortcode if it is on the first select options
   * define the defaults for those kind of select here!
   * @var array
   */
  protected $defaults = array(
    'template' => array(
      'items' => 'slick-carousel.php',
      'empty' => 'slick-empty.php',
    ),
    'slick' => array(
      'dots' => false,
      'arrows' => false,
      'slidesToScroll' => 1,
      'centerPadding' => '50px',
      'autoplaySpeed' => '1000',
      'cssEase' => 'ease',
      'speed' => 600,
      'touchThreshold' => 5,
      'centerMode' => false,
      'variableWidth' => false,
      'autoplay' => false,
      'pauseOnHover' => false,
      'infinite' => false,
      'accessibility' => false,
      'draggable' => false,
      'fade' => false,
      'swipe' => false,
      'touchmove' => false,
      'vertical' => false,
      'adaptiveHeight' => false,
      'equalheight' => false,
    ),
  );

  /**
   * Extending parent method.
   * @see VTCore_Wordpress_Models_Shortcodes::processCustomRules()
   */
  protected function processCustomRules() {

    // Convert the bootstrap classes into vc compatible one
    $this->convertVCGrid = !get_theme_support('bootstrap');
    $this->buildQuery();
    $object = new VTCore_Wordpress_Objects_Array($this->defaults);

    $object
      ->merge($this->atts)
      ->merge(array('slick' => $this->accessAttributes()->get('slick')))
      ->add('grids', $this->accessAttributes()->get('slick.grids'))
      ->add('convert_grids', false)
      ->add('attributes.class.slick', 'slick-simple-carousel')
      ->add('query', $this->accessQuery())
      ->add('process.filter', false);

    do_action('vtcore_slick_alter_simple', $object, $this);

    $this->atts = $object->extract();
  }



  public function buildObject() {

    VTCore_Wordpress_Utility::loadAsset('jquery-slick');
    VTCore_Wordpress_Utility::loadAsset('slick-front');

    $this->object = new VTCore_Wordpress_Element_WpCarousel($this->atts);
  }
}