<?php
/**
 * Shortcode for building the slick filtering element
 * the filtering element must use taxonomy as the filters
 *
 *   [slickfilter
 *      all="text for all button"
 *      ajax___ajax-loading-text="some text for ajax notice"
 *   ]
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Slick_Shortcodes_SlickFilter
extends VTCore_Slick_Models_Shortcodes
implements VTCore_Wordpress_Interfaces_Shortcodes {

  protected $processDottedNotation = true;

  /**
   * Extending parent method.
   * @see VTCore_Wordpress_Models_Shortcodes::processCustomRules()
   */
  protected function processCustomRules() {

    // Convert the bootstrap classes into vc compatible one
    $this->convertVCGrid = !get_theme_support('bootstrap');

    $object = new VTCore_Wordpress_Objects_Array(array(
      'all' => __('All', 'victheme_slick'),
      'ajax' => array(
        'ajax-loading-text' => __('Processing...', 'victheme_slick'),
      ),
    ));
    $object
      ->merge($this->atts)
      ->add('data.ajax-type', 'ajax-carousel-filter')
      ->add('id', $this->accessQuery()->get('vtcore_queryid'))
      ->add('ajax.ajax-group', '.js-carousel-filter .term-items')
      ->add('link_elements.attributes.class.btn', 'btn btn-primary')
      ->add('attributes.class.slick', 'slick-filter')
      ->add('taxonomy', $this->accessAttributes()->get('taxonomy'));

    if ($object->get('all') == '') {
      $object->add('all', false);
    }

    do_action('vtcore_slick_alter_filter', $object, $this);

    $this->atts = $object->extract();
  }



  public function buildObject() {
    $this->object = new VTCore_Wordpress_Element_WpTermList($this->atts);
  }
}