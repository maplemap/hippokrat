<?php
/**
 * Shortcode for building the slick filtering element
 * the filtering element must use taxonomy as the filters
 *
 *
 *   [slickthumbnail
 *     template___item="the template name for the slick items"
 *     template___empty="the template name where no slick item found"
 *     grids___columns___mobile="x"
 *     grids___columns___tablet="x"
 *     grids___columns___medium="x"
 *     grids___columns___large="x"]
 *
 *   [/slickthumbnail]
 *
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Slick_Shortcodes_SlickThumbnail
extends VTCore_Slick_Models_Shortcodes
implements VTCore_Wordpress_Interfaces_Shortcodes {

  protected $processDottedNotation = true;

  /**
   * VC 4.7 somehow wont record
   * the shortcode if it is on the first select options
   * define the defaults for those kind of select here!
   * @var array
   */
  protected $defaults = array(
    'template' => array(
      'items' => 'slick-thumb.php',
      'empty' => 'slick-thumb-empty.php',
    ),
  );

  protected function processCustomRules() {

    // Convert the bootstrap classes into vc compatible one
    $this->convertVCGrid = !get_theme_support('bootstrap');

    $object = new VTCore_Wordpress_Objects_Array($this->defaults);

    $object
      ->merge(array('slick' => $this->accessAttributes()->get('slick')))
      ->merge($this->atts)
      ->add('attributes.class.default', 'slick-carousel-thumbnail-wrapper')
      ->add('query', $this->accessQuery())
      ->add('id', $this->accessQuery()->get('vtcore_queryid'))
      ->add('data.slick-carousel-thumbnail', $object->get('id'))
      ->add('data.slick-carousel-ajax', $object->get('id'))
      ->add('data.slick-carousel', true)
      ->add('slick.fade', false)
      ->add('slick.centerMode', true)
      ->add('slick.centerPadding', '0px')
      ->add('slick.focusOnSelect', true)
      ->add('slick.variableWidth', false)
      ->add('slick.autoplay', false)
      ->add('slick.dots', false)
      ->add('slick.arrows', false)
      ->add('slick.asNavFor', '[data-slick-carousel-element="' . $object->get('id') . '"]')
      ->add('slick.asThumbPager', true)
      ->add('slick.infinite', true)
      ->add('slick.thumbPagerMove', $object->get('slick.slidesToScroll'))
      ->add('ajax', false)
      ->add('data.destination', $object->get('id'))
      ->add('data.ajax-type', 'pager')
      ->add('attributes.class.slick', 'slick-thumb')
      ->remove('slick.slidesToShow');

    do_action('vtcore_slick_alter_thumbnail', $object, $this);

    $this->atts = $object->extract();

  }

  public function buildObject() {
    $this->object = new VTCore_Wordpress_Element_WpCarousel($this->atts);
  }
}