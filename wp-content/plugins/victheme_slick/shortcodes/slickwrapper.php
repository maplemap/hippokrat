<?php
/**
 * Class extending the Shortcodes base class
 * for building the Slick Wrapper
 * 
 * Slick wrapper can only valid to hold [slickslide]
 * as its direct children.
 *
 * how to use :
 *
 * [slickwrapper
 *   slick___accessibility="true|false" #
 *   slick___autoplay="true|false" #
 *   slick___centermode="true|false" #
 *   slick___draggable="true|false" #
 *   slick___fade="true|false" #
 *   slick___pauseonhover="true|false" #
 *   slick___infinite="true|false" #
 *   slick___swipe="true|false" #
 *   slick___touchmove="true|false" #
 *   slick___vertical="true|false"  #
 *   slick___cssease="ease|ease-in|ease-out|ease-in-out" #
 *   slick___easing="swing|easeInQuad|easeOutQuad|easeInOutQuad|easeInCubic|easeOutCubic|easeInOutCubic|..." #
 *   slick___autoplayspeed="Number in miliseconds" #
 *   slick___centerpadding="xxpx" #
 *   slick___slidestoshow="number" #
 *   slick___slidestoscroll="number" #
 *   slick___speed="number in miliseconds" #
 *   slick___touchthreshold="number" #
 *   slick___variablewidth="true|false" #
 *   slick___equalheight="true|false"
 *   slick___dots="true|false"
 *   slick___arrows="true|false"
 *   slick___grids___columns___mobile="X"
 *   slick___grids___columns___tablet="X"
 *   slick___grids___columns___small="X"
 *   slick___grids___columns___large="X"
 *   ]
 *
 *    [slickinner]
 *      content with shortcodes
 *    [/slickinner]
 *
 * [/slickwrapper]
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Slick_Shortcodes_SlickWrapper
extends VTCore_Slick_Models_Shortcodes
implements VTCore_Wordpress_Interfaces_Shortcodes {

  protected $processDottedNotation = true;

  /**
   * VC 4.7 somehow wont record
   * the shortcode if it is on the first select options
   * define the defaults for those kind of select here!
   * @var array
   */
  protected $defaults = array(
    'slick' => array(
      'dots' => false,
      'arrows' => false,
      'slidesToScroll' => 1,
      'centerPadding' => '50px',
      'autoplaySpeed' => '1000',
      'cssEase' => 'ease',
      'speed' => 600,
      'touchThreshold' => 5,
      'centerMode' => false,
      'variableWidth' => false,
      'autoplay' => false,
      'pauseOnHover' => false,
      'infinite' => false,
      'accessibility' => false,
      'draggable' => false,
      'fade' => false,
      'swipe' => false,
      'touchmove' => false,
      'vertical' => false,
      'adaptiveHeight' => false,
      'equalheight' => false,
    ),
  );
  /**
   * Overriding parent method, we dont want to build
   * the actual attributes data-*.
   * @see VTCore_Wordpress_Models_Shortcodes::processData()
   */
  protected function processData() {

    // Convert the bootstrap classes into vc compatible one
    $this->convertVCGrid = !get_theme_support('bootstrap');

    $object = new VTCore_Wordpress_Objects_Array($this->defaults);

    $object
      ->merge($this->atts)
      ->add('attributes.class.slick', 'slick-wrapper-carousel')
      ->add('grids', $object->get('slick.grids'))
      ->add('convert_grids', false)
      ->add('process.query', false)
      ->add('process.filter', false)
      ->add('process.ajax', false)
      ->add('process.loop', false);

    do_action('vtcore_slick_alter_wrapper', $object, $this);

    $this->atts = $object->extract();
  }


  public function buildObject() {

    VTCore_Wordpress_Utility::loadAsset('jquery-slick');
    VTCore_Wordpress_Utility::loadAsset('slick-front');

    $this->object = new VTCore_Wordpress_Element_WpCarousel($this->atts);
    $this->object->addClass('slick-wrapper-carousel');
    $this->object->addChildren(do_shortcode($this->content));
  }
}