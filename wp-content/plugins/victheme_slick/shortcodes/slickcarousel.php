<?php
/**
 * Shortcode for building the slick carousel container
 * in the advanced slick carousel building method.
 *
 * This container will hold all the attributes related
 * to the slick element inside this container and will
 * pass the attributes globaly.
 *
 * subelement can use VTCore_Slick_Shortcodes_SlickContainer::getAttributes()->get()
 * method to access the aggregated attributes.
 *
 *
 * [slickcarousel
 *   queryargs="{json of valid WP_Query arguments}"
 *   filter___taxonomy="taxonomy name for the filter elements"
 *   slick___accessibility="true|false" #
 *   slick___autoplay="true|false" #
 *   slick___centermode="true|false" #
 *   slick___draggable="true|false" #
 *   slick___fade="true|false" #
 *   slick___pauseonhover="true|false" #
 *   slick___infinite="true|false" #
 *   slick___swipe="true|false" #
 *   slick___touchmove="true|false" #
 *   slick___vertical="true|false"  #
 *   slick___cssease="ease|ease-in|ease-out|ease-in-out" #
 *   slick___easing="swing|easeInQuad|easeOutQuad|easeInOutQuad|easeInCubic|easeOutCubic|easeInOutCubic|..." #
 *   slick___autoplayspeed="Number in miliseconds" #
 *   slick___centerpadding="xxpx" #
 *   slick___slidestoshow="number" #
 *   slick___slidestoscroll="number" #
 *   slick___speed="number in miliseconds" #
 *   slick___touchthreshold="number" #
 *   slick___variablewidth="true|false" #
 *   slick___equalheight="true|false"
 *   slick___grids___columns___mobile="X"
 *   slick___grids___columns___tablet="X"
 *   slick___grids___columns___small="X"
 *   slick___grids___columns___large="X"
 *   ]
 *
 *   [slickfilter
 *      all="text for all button"
 *      ajax___ajax-loading-text="some text for ajax notice"
 *   ]
 *
 *   [slickprev]
 *
 *   [slickquery
 *      template___item="the template name for the slick items"
 *      template___empty="the template name where no slick item found"
 *   ]
 *
 *   [slickthumbnail
 *     template___item="the template name for the slick items"
 *     template___empty="the template name where no slick item found"
 *     slick___vertical="true|false"
 *     grids___columns___mobile="x"
 *     grids___columns___tablet="x"
 *     grids___columns___medium="x"
 *     grids___columns___large="x"]
 *
 *   [/slickthumbnail]
 *
 *   [slicknext]
 *
 *   [slickpager]
 *
 * [/slickcarousel]
 *
 *
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Slick_Shortcodes_SlickCarousel
extends VTCore_Slick_Models_Shortcodes
implements VTCore_Wordpress_Interfaces_Shortcodes {

  protected $processDottedNotation = true;

  /**
   * Extending parent method.
   * @see VTCore_Wordpress_Models_Shortcodes::processCustomRules()
   */
  protected function processCustomRules() {

    // Convert the bootstrap classes into vc compatible one
    $this->convertVCGrid = !get_theme_support('bootstrap');
    $this->accessAttributes();

    $object = new VTCore_Wordpress_Objects_Array($this->atts);

    $object
      ->add('type', 'div')
      ->add('attributes.class.slick', 'slick-advanced-carousel');

    do_action('vtcore_slick_alter_carousel', $object, $this);

    $this->atts = $object->extract();
  }



  public function buildObject() {

    VTCore_Wordpress_Utility::loadAsset('jquery-slick');
    VTCore_Wordpress_Utility::loadAsset('slick-front');

    $this->object = new VTCore_Bootstrap_Element_BsElement($this->atts);

    $this->object->addChildren(do_shortcode($this->content));

    $this->resetAttributes();
    $this->resetQuery();

  }
}