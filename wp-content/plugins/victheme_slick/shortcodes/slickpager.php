<?php
/**
 * Shortcode for building the slick dots wrapper
 * and act as the javascript append target point
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Slick_Shortcodes_SlickPager
extends VTCore_Slick_Models_Shortcodes
implements VTCore_Wordpress_Interfaces_Shortcodes {

  protected $processDottedNotation = true;

  protected function processCustomRules() {

    // Convert the bootstrap classes into vc compatible one
    $this->convertVCGrid = !get_theme_support('bootstrap');

    $object = new VTCore_Wordpress_Objects_Array($this->atts);
    $object
      ->add('type', 'div')
      ->add('attributes.class.target', 'slick-pagination')
      ->add('data.slick-carousel-pagination', $this->accessQuery()->get('vtcore_queryid'));

    do_action('vtcore_slick_alter_pagination', $object, $this);

    $this->atts = $object->extract();
  }

  public function buildObject() {
    $this->object = new VTCore_Bootstrap_Element_BsElement($this->atts);
  }
}