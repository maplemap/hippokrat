<?php
/**
 * Shortcode for building the slick element and using
 * WpCarousel plus WP_Query to fetch the slick item data
 *
 *   [slickquery
 *      template___item="the template name for the slick items"
 *      template___empty="the template name where no slick item found"
 *   ]
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Slick_Shortcodes_SlickQuery
extends VTCore_Slick_Models_Shortcodes
implements VTCore_Wordpress_Interfaces_Shortcodes {

  protected $processDottedNotation = true;

  /**
   * VC 4.7 somehow wont record
   * the shortcode if it is on the first select options
   * define the defaults for those kind of select here!
   * @var array
   */
  protected $defaults = array(
    'template' => array(
      'items' => 'slick-carousel.php',
      'empty' => 'slick-empty.php',
    ),
  );

  protected function processCustomRules() {

    // Convert the bootstrap classes into vc compatible one
    $this->convertVCGrid = !get_theme_support('bootstrap');

    $object = new VTCore_Wordpress_Objects_Array($this->defaults);
    $object
      ->merge($this->atts)
      ->add('slick', $this->accessAttributes()->get('slick'))
      ->add('attributes.class.default', 'slick-carousel-element-wrapper')
      ->add('grids', $this->accessAttributes()->get('slick.grids'))
      ->add('query', $this->accessQuery())
      ->add('id', $this->accessQuery()->get('vtcore_queryid'))
      ->add('data.slick-carousel-element', $object->get('id'))
      ->add('data.slick-carousel', true)
      ->add('slick.appendDots', '[data-slick-carousel-pagination="' . $object->get('id') . '"]')
      ->add('slick.appendArrows.prev', '[data-slick-carousel-arrow-prev="' . $object->get('id') . '"]')
      ->add('slick.appendArrows.next', '[data-slick-carousel-arrow-next="' . $object->get('id') . '"]')
      ->add('slick.asNavFor', '[data-slick-carousel-thumbnail="' . $object->get('id') . '"]')
      ->add('slick.dots', true)
      ->add('slick.arrows', true)
      ->add('convert_grids', false)
      ->add('ajax', true)
      ->add('ajaxData.ajax-target', $this->accessAttributes()->get('container.target'))
      ->add('attributes.class.slick', 'slick-element')
      ->remove('slick.slidesToShow');

    $object->add('attributes.class.template', str_replace('.php', '', $object->get('template.items')));

    if ($this->accessAttributes()->get('taxonomy')) {
      $object->add('filters', explode(',', $this->accessAttributes()->get('taxonomy')));
    }

    do_action('vtcore_slick_alter_query', $object, $this);

    $this->atts = $object->extract();

  }



  public function buildObject() {
    $this->object = new VTCore_Wordpress_Element_WpCarousel($this->atts);
  }
}