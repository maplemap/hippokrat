<?php
/**
 * Class for building the [slickslide] shortcode
 * this class must be placed as direct child of
 * [slickwrapper] shortcode to be processed properly.
 *
 * // For generic content
 * [slickslide]
 *   Some content here additional shortcodes is allowed
 * [/slickslide]
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Slick_Shortcodes_SlickSlide
extends VTCore_Slick_Models_Shortcodes
implements VTCore_Wordpress_Interfaces_Shortcodes {


  public function buildObject() {
    $this->object = new VTCore_Html_Element($this->atts);
    $this->object->setType('div')->addClass('slick-items');

    $this->object->addChildren(do_shortcode($this->content));
  }
}