<?php
/*
Plugin Name: VicTheme Slick
Plugin URI: http://victheme.com
Description: Plugin for providing carousel in shortcode and widgets.
Author: jason.xie@victheme.com
Version: 1.2.2
Author URI: http://victheme.com
*/

define('VTCORE_SLICK_CORE_VERSION', '1.7.0');

// Load the translation as early as possible
load_plugin_textdomain('victheme_slick', false, 'victheme_slick/languages');

add_action('plugins_loaded', 'bootVicthemeSlick', 13);

function bootVicthemeSlick() {

  if (!(defined('VTCORE_VERSION') && version_compare(VTCORE_VERSION, VTCORE_SLICK_CORE_VERSION, '='))) {

    add_action('admin_notices', 'SlickMissingCoreNotice');

    function SlickMissingCoreNotice() {

      if (!defined('VTCORE_VERSION')) {
        $notice = __('VicTheme Slick depends on VicTheme Core Plugin which is not activated or missing, Please enable it first before VicTheme Slick can work properly.', 'victheme_slick');
      }
      elseif (version_compare(VTCORE_VERSION, VTCORE_SLICK_CORE_VERSION, '!=')) {
        $notice = sprintf(__('VicTheme Slick depends on VicTheme Core Plugin API version %s to operate properly.', 'victheme_slick'), VTCORE_SLICK_CORE_VERSION);
      }

      if (isset($notice)) {
        echo '<div class="error""><p>' . $notice . '</p></div>';
      }

    }

    return;
  }


  if (defined('WPB_VC_VERSION') && !version_compare(WPB_VC_VERSION, '4.7.0', '>=')) {
    add_action('admin_notices', 'VTCore_SlickVCTooLow');

    function VTCore_SlickVCTooLow() {
      echo
        '<div class="error""><p>' .

        __( 'Slick requires Visual Composer Plugin version 4.7.0 and above before it can function properly.', 'victheme_slick') .

        '</p></div>';
    }

    return;
  }

  define('VTCORE_SLICK_LOADED', true);
  define('VTCORE_SLICK_BASE_DIR', dirname(__FILE__));

  // Booting autoloader
  $autoloader = new VTCore_Autoloader('VTCore_Slick', dirname(__FILE__));
  $autoloader->setRemovePath('vtcore' . DIRECTORY_SEPARATOR . 'slick' . DIRECTORY_SEPARATOR);
  $autoloader->register();

  // Registering assets
  VTCore_Wordpress_Init::getFactory('assets')
    ->get('library')
    ->detect(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets', plugins_url('', __FILE__) . '/assets');

  // Registering custom templating system
  VTCore_Wordpress_Init::getFactory('template')
    ->register(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'templates', 'templates');


  // Registering actions
  VTCore_Wordpress_Init::getFactory('actions')
    ->addPrefix('VTCore_Slick_Actions_')
    ->addHooks(array(
      'widgets_init',
      'vc_backend_editor_enqueue_js_css',
      'vc_frontend_editor_enqueue_js_css',
      'vc_load_iframe_jscss'
    ))
    ->register();

  // Registering actions
  VTCore_Wordpress_Init::getFactory('filters')
    ->addPrefix('VTCore_Slick_Filters_')
    ->addHooks(array(
      'vtcore_register_shortcode_prefix',
      'vtcore_register_shortcode',
    ))
    ->register();

  // Register to visual composer via VTCore Visual Composer Factory
  if (VTCore_Wordpress_Init::getFactory('visualcomposer')) {
    VTCore_Wordpress_Init::getFactory('visualcomposer')
      ->mapShortcode(array(
        'VTCore_Slick_Composer_SlickCarousel',
        'VTCore_Slick_Composer_SlickFilter',
        'VTCore_Slick_Composer_SlickNext',
        'VTCore_Slick_Composer_SlickPager',
        'VTCore_Slick_Composer_SlickPrev',
        'VTCore_Slick_Composer_SlickQuery',
        'VTCore_Slick_Composer_SlickThumbnail',
        'VTCore_Slick_Composer_SlickWrapper',
        'VTCore_Slick_Composer_SlickSlide',
        'VTCore_Slick_Composer_SlickSimple',
      ));
  }
}