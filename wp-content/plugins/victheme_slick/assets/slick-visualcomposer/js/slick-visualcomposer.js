/**
 *
 * Integrating Advanced Carousel and its elements
 * into Visual Composer Front-end editor.
 *
 * This is not compatible with VisualComposer version
 * less than 4.7 since it uses events rather than extending
 * method!.
 *
 * @author jason.xie@victheme.com
 */
(function ($) {

  $(window)
    .on('vc_ready', function() {

      // Registering our slick global methods.
      vc.vtcore.slickCarousel = {
        getParent: function(model) {
          var Container = $(vc.frame_window.document).find('[data-model-id="' + model.get('parent_id') + '"]'), Loop;

          // Parent not visualloop carousel drill up dom further
          if (Container.data('tag') != 'slickcarousel') {
            Container = Container.closest('[data-tag="slickcarousel"]').eq(0);
          }

          Loop = vc.shortcodes.get(Container.data('model-id'));

          if (Loop) {
            model.set('loop', Loop);
            model.set('loopID', Loop.get('id'));
            delete Loop;
          }
        },
        setParams: function(model) {
          // We only concern for queryargs params
          this.getParent(model);
          if (model.get('loop')) {
            var loopparams = model.get('loop').get('params'), params = model.get('params');

            params.queryargs = loopparams.queryargs;

            _.each(loopparams, function (value, key) {
              if (vc.vtcore.strpos(key, 'taxonomy') !== false
                || vc.vtcore.strpos(key, 'slick___') !== false) {
                params[key] = value;
              }
            }, this);


            model.set('params', params);
          }

          return model.get('params');
        },
        cleanParams: function(model) {
          var params = model.get('params'), clean;

          params.queryargs && delete params.queryargs;

          _.each(params, function (value, key) {
            if (vc.vtcore.strpos(key, 'taxonomy') !== false
              || vc.vtcore.strpos(key, 'slick___') !== false) {

              delete params[key];
            }
          }, this);

          model.set('params', params);
        },
        // Only use this for carousel without container mode
        // such as for slicksimple and slickwrapper
        renderCarousel: function(model) {
          var Carousel = $(vc.frame_window.document)
            .find('[data-model-id="' + model.get('id') + '"]')
            .find('.slick-carousel').eq(0);

          if (Carousel) {
            var data = Carousel.data('settings');

            // Slick wrapper has too much problem with cloned element
            if (model.get('shortcode') == 'slickwrapper') {
              data.infinite = false;
              data.autoplay = false;
            }

            Carousel.unslick().slick(data);
          }

        },
        fixSlickItemClass: function(model) {
          model.view.$el.addClass('slick-items');
          model.view.$content.removeClass('slick-items');
        },
        updateCarousel: function(model) {
          var Frame = $(vc.frame_window.document)
            .find('[data-model-id="' + model.get('loop').get('id') + '"]');

          _.each(Frame.find('[data-slick-carousel-element]'), function(element, index) {

            var carousel = $(element),
                data = carousel.data('settings');

            data.dots = false;
            data.arrows = false;
            data.asNavFor = false;

            if (Frame.find('[data-slick-carousel-thumbnail]').length) {
              data.asNavFor = Frame.find('[data-slick-carousel-thumbnail]');
            }

            if (Frame.find('[data-slick-carousel-pagination]').length) {
              data.dots = true;
              data.appendDots = Frame.find('[data-slick-carousel-pagination]');
            }

            if (Frame.find('[data-slick-carousel-arrow-prev]').length) {
              data.arrows = true;
              data.appendArrows.prev = Frame.find('[data-slick-carousel-arrow-prev]');
            }

            if (Frame.find('[data-slick-carousel-arrow-next]').length) {
              data.arrows = true;
              data.appendArrows.next = Frame.find('[data-slick-carousel-arrow-next]');
            }

            carousel.unslick().slick(data);

          }, this);
        },
        updateThumbnail: function(model) {
          var Frame = $(vc.frame_window.document)
            .find('[data-model-id="' + model.get('loop').get('id') + '"]');

          _.each(Frame.find('[data-slick-carousel-thumbnail]'), function(element, index) {

            var carousel = $(element),
              data = carousel.data('settings');

            data.dots = false;
            data.arrows = false;

            if (Frame.find('[data-slick-carousel-element]').length) {
              data.asNavFor = Frame.find('[data-slick-carousel-element]');
            }

            carousel.unslick().slick(data);

          }, this);
        }
      }

      /**
       * Bind custom events to act on vc events.
       */
      vc.events
        .on('shortcodes:slickquery:add', function(model) {
          vc.vtcore.slickCarousel.setParams(model);
        })
        .on('shortcodes:slickthumbnail:add', function(model) {
          vc.vtcore.slickCarousel.setParams(model);
        })
        .on('shortcodes:slickfilter:add', function(model) {
          vc.vtcore.slickCarousel.setParams(model);
        })
        .on('shortcodes:slickprev:add', function(model) {
          vc.vtcore.slickCarousel.setParams(model);
        })
        .on('shortcodes:slicknext:add', function(model) {
          vc.vtcore.slickCarousel.setParams(model);
        })
        .on('shortcodes:slickpager:add', function(model) {
          vc.vtcore.slickCarousel.setParams(model);
        })
        .on('shortcodeView:ready:slickquery', function(model) {
          vc.vtcore.slickCarousel.updateCarousel(model);
          vc.vtcore.slickCarousel.updateThumbnail(model);
          vc.vtcore.fixGridClass(model, model.view.$el.children());
          vc.vtcore.slickCarousel.cleanParams(model);
        })
        .on('shortcodeView:ready:slickpager', function(model) {
          vc.vtcore.slickCarousel.updateCarousel(model);
          vc.vtcore.fixGridClass(model, model.view.$el.children());
          vc.vtcore.slickCarousel.cleanParams(model);
        })
        .on('shortcodeView:ready:slicknext', function(model) {
          vc.vtcore.slickCarousel.updateCarousel(model);
          vc.vtcore.fixGridClass(model, model.view.$el.children());
          vc.vtcore.slickCarousel.cleanParams(model);
        })
        .on('shortcodeView:ready:slickprev', function(model) {
          vc.vtcore.slickCarousel.updateCarousel(model);
          vc.vtcore.fixGridClass(model, model.view.$el.children());
          vc.vtcore.slickCarousel.cleanParams(model);
        })
        .on('shortcodeView:ready:slickthumbnail', function(model) {
          vc.vtcore.slickCarousel.updateCarousel(model);
          vc.vtcore.slickCarousel.updateThumbnail(model);
          vc.vtcore.fixGridClass(model, model.view.$el.children());
          vc.vtcore.slickCarousel.cleanParams(model);
        })
        .on('shortcodeView:ready:slickfilter', function(model) {
          vc.vtcore.fixGridClass(model, model.view.$el.children());
          vc.vtcore.slickCarousel.cleanParams(model);
        })
        .on('shortcodeView:ready:slickcarousel', function(model) {
          vc.vtcore.fixGridClass(model, model.view.$el.children());
          vc.vtcore.registerGridContainer(model, 'vtcore-slickcarousel');
          vc.vtcore.checkEmpty(model);
        })
        .on('shortcodeView:updated:slickcarousel', function(model) {
          var elements = vc.shortcodes.where({loopID: model.get('id')});
          _.each(elements, function(element) {
            vc.vtcore.slickCarousel.setParams(element);
            vc.builder.update(element);
          }, this);
        })
        .on('shortcodeView:ready:slicksimple', function(model) {
          vc.vtcore.fixGridClass(model, model.view.$el.children());
          vc.vtcore.slickCarousel.renderCarousel(model);
        })
        .on('shortcodeView:ready:slickwrapper', function(model) {
          vc.vtcore.fixGridClass(model, model.view.$el.children());
          //vc.vtcore.checkEmpty(model);
          vc.vtcore.slickCarousel.renderCarousel(model);
        })
        .on('shortcodeView:ready:slickslide', function(model) {

          model.view.$controls.find('.vc_control-btn-clone').remove();

          vc.vtcore.slickCarousel.fixSlickItemClass(model);
          vc.vtcore.fixGridClass(model, model.view.$el.children());
          vc.vtcore.checkEmpty(model);

          var parent = vc.shortcodes.get(model.get('parent_id'));
          vc.vtcore.slickCarousel.renderCarousel(parent);
        });


      /**
       * Extending the inline shortcode view to piggy back our
       * before update events. remove this when vc
       * has actual before update events.
       */
      window.InlineShortcodeViewSlickElement = window.InlineShortcodeView.extend({
        events: {
          'click > .vc_controls .vc_control-btn-delete': 'destroy',
          'click > .vc_controls .vc_control-btn-edit': 'edit',
          'mousemove': 'checkControlsPosition'
        },
        addControls: function () {
          window.InlineShortcodeViewSlickElement.__super__.addControls.call(this);
          this.$controls.find('.vc_control-btn-clone').remove();
        },
        beforeUpdate: function() {
          vc.vtcore.slickCarousel.setParams(this.model);
        }
      });

      window.InlineShortcodeView_slickfilter = window.InlineShortcodeViewSlickElement;
      window.InlineShortcodeView_slickquery = window.InlineShortcodeViewSlickElement;
      window.InlineShortcodeView_slickthumbnail = window.InlineShortcodeViewSlickElement;
      window.InlineShortcodeView_slickpager = window.InlineShortcodeViewSlickElement;
      window.InlineShortcodeView_slickprev = window.InlineShortcodeViewSlickElement;
      window.InlineShortcodeView_slicknext = window.InlineShortcodeViewSlickElement;
    });

})(window.jQuery);