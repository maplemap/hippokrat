<?php
/**
 * Template for displaying slick carousel thumbnail navigation
 * with these rules :
 * 1. Thumbnail image is taken from featured image
 *
 * This template must be invoked inside the WPCarousel loop.
 *
 * You can override this template by copying to your theme
 * /templates/slickcarousel folder or custom template folder
 * if you register the folder first using :
 *
 *
    // Registering custom templating system
    VTCore_Wordpress_Init::getFactory('template')
      ->register('Full Directory Path string here!', 'relative directory path here!');

 *
 * @author jason.xie@victheme.com
 */
?>

  <div id="slick-thumbs-<?php echo $post->ID; ?>" <?php post_class();?>>
    <figure class="post-thumb">
      <?php echo get_the_post_thumbnail((int) $post->ID, 'thumbnail'); ?>
    </figure>
  </div>

