<?php
/**
 * Template for displaying slick carousel items
 * with these rules :
 * 1. Thumbnail image is taken from featured imate
 * 2. Post title linked to the post
 * 3. Post excerpt
 * 4. Readmore button linked to the post
 *
 * This template must be invoked inside the WPCarousel loop.
 * You can override the visibility of the element by passing
 * $show array to the WPCarousel Context under the custom.show key.
 *
 * You can override this template by copying to your theme
 * /templates/slickcarousel folder or custom template folder
 * if you register the folder first using :
 *
 *
    // Registering custom templating system
    VTCore_Wordpress_Init::getFactory('template')
      ->register('Full Directory Path string here!', 'relative directory path here!');

 *
 * @author jason.xie@victheme.com
 */

  if (is_a($this, 'VTCore_Wordpress_Element_WPCarousel')) {
    $show = $this->getContext('custom.show');
  }

  if (!isset($show) || empty($show)) {
    $show = array(
      'image' => true,
      'title' => true,
      'excerpt' => true,
      'button' => true,
    );
  }
?>

  <article id="slick-items-<?php echo $post->ID; ?>" <?php post_class();?>>

    <?php if (has_post_thumbnail() && $show['image']) : ?>
      <a class="post-thumbnail"
         href="<?php esc_url(the_permalink()); ?>"
         alt="<?php echo esc_attr(get_the_title()); ?>">
        <figure class="post-thumbnail">
          <?php echo get_the_post_thumbnail((int) $post->ID, 'large'); ?>
        </figure>
      </a>
    <?php endif; ?>

    <?php if (get_the_title((int) $post->ID) && $show['title']) : ?>
      <h3 class="post-title clearboth clearfix">
        <a href="<?php echo get_permalink(); ?>"
           class="post-title-link"
           alt="<?php esc_attr($post->post_title); ?>">
          <?php the_title();  ?>
        </a>
      </h3>
    <?php endif; ?>

    <?php if (get_the_excerpt() && $show['excerpt']) : ?>
      <div class="post-excerpt clearboth clearfix">
        <?php the_excerpt(); ?>
      </div>
    <?php endif; ?>


    <?php if ($show['button']) : ?>
      <a class="post-readmore btn btn-primary clearboth"
         href="<?php the_permalink(); ?>"
         alt="<?php esc_attr($post->post_title); ?>">
        <?php echo __('Readmore', 'victheme_slick'); ?>
      </a>
    <?php endif; ?>

  </article>

