<?php
/**
 * Template for displaying slick carousel when
 * no content is available.
 *
 * This template must be invoked inside the WPCarousel loop.
 *
 * You can override this template by copying to your theme
 * /templates/slickcarousel folder or custom template folder
 * if you register the folder first using :
 *
 *
    // Registering custom templating system
    VTCore_Wordpress_Init::getFactory('template')
      ->register('Full Directory Path string here!', 'relative directory path here!');

 *
 * @author jason.xie@victheme.com
 */
?>

  <article id="slick-items-empty" class="slick-empty">
    <h3 class="post-title clearfix clearboth">
      <?php echo __('No Slides found', 'victheme_slick'); ?>
    </h3>
  </article>

