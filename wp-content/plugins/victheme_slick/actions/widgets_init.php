<?php
/**
 * Hooking into wordpress widget_init action for
 * registering maps custom widgets.
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Slick_Actions_Widgets__Init
extends VTCore_Wordpress_Models_Hook {

  public function hook() {

    $widgets = array(
      'VTCore_Slick_Widgets_Simple',
      'VTCore_Slick_Widgets_Query',
    );

    foreach ($widgets as $widget) {
      $object = new $widget;
      $object->registerWidget();
    }
  }
}