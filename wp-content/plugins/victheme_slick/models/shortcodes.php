<?php
/**
 * Extending VTCore Shortcodes Model to add
 * static query and attributes since one
 * query or attributes is going to be used
 * in multiple shortcodes with parent - child
 * relationship.
 *
 * @author jason.xie@victheme.com
 */
abstract class VTCore_Slick_Models_Shortcodes
extends VTCore_Wordpress_Models_Shortcodes {

  static protected $attributes;
  static protected $query;
  protected $queryArgs = array();

  public function resetQuery() {
    self::$query = false;
    return $this;
  }

  protected $camelcase = array(
    'slick.autoplaySpeed',
    'slick.centerMode',
    'slick.centerPadding',
    'slick.cssEase',
    'slick.pauseOnHover',
    'slick.slidesToShow',
    'slick.slidesToScroll',
    'slick.touchMove',
    'slick.touchThreshold',
    'slick.variableWidth',
    'slick.adaptiveHeight'
  );

  protected $booleans = array(
    'slick.accessibility',
    'slick.autoplay',
    'slick.arrows',
    'slick.dots',
    'slick.centerMode',
    'slick.draggable',
    'slick.fade',
    'slick.infinite',
    'slick.pauseOnHover',
    'slick.swipe',
    'slick.touchMove',
    'slick.vertical',
    'slick.variableWidth',
    'slick.adaptiveHeight',
    'slick.equalheight'
  );


  protected $int = array(
    'slick.slidesToShow',
    'slick.slidesToScroll',
    'slick.speed',
    'slick.touchTreshold',
    'slick.autoplaySpeed',
    'slick.thumbPagerMove',
  );

  protected $defaults = array(
    'slick' => array(
      'slidesToScroll' => 1,
      'centerPadding' => '50px',
      'autoplaySpeed' => '1000',
      'cssEase' => 'ease',
      'speed' => 600,
      'touchThreshold' => 5,
      'centerMode' => false,
      'variableWidth' => false,
      'autoplay' => false,
      'pauseOnHover' => false,
      'infinite' => false,
      'accessibility' => false,
      'draggable' => false,
      'fade' => false,
      'swipe' => false,
      'touchmove' => false,
      'vertical' => false,
      'adaptiveHeight' => false,
      'equalheight' => false,
    ),
  );


  public function buildQuery() {

    $args = $this->accessAttributes()->get('queryargs');

    // Try json first
    if (is_string($args)) {
      $converted = json_decode(html_entity_decode($args), true);

      if (is_array($converted)) {
        $args = $converted;
      }
    }

    // Maybe in url query format
    if (is_string($args)) {
      $converted = wp_parse_args(html_entity_decode($args));

      if (is_array($converted)) {
        $args = $converted;
      }
    }

    // Use empty array if all fails
    if (!is_array($args)) {
      $args = array();
    }

    $this->queryArgs = new VTCore_Wordpress_Objects_Array($args);
    $this->processQueryArgs();
    self::$query = new WP_Query($this->queryArgs->extract());
    return $this;
  }


  protected function processQueryArgs() {

    if ($this->queryArgs->get('query')) {
      $object = new VTCore_Wordpress_Objects_Array();
      foreach ($this->queryArgs->get('query') as $key => $data) {
        if ($key == 'id') {
          $object->add('vtcore_queryid', $data);
        }
        elseif ($key == 'taxonomy') {
          foreach ($data as $delta => $value) {
            if (is_numeric($delta)) {
              if (empty($value['terms']) || empty($value['taxonomy'])) {
                unset($data[$delta]);
              }
            }
          }

          if (count($data) > 1) {
            $object->add('tax_query', $data);
          }
        }

        elseif ($key == 'meta') {
          foreach ($data as $delta => $value) {
            if (is_numeric($delta)) {
              if (empty($value['key']) || empty($value['value'])) {
                unset($data[$delta]);
              }
            }
          }

          if (count($data) > 1) {
            $object->add('meta_query', $data);
          }
        }
        else {
          $object->merge($data);
        }
      }
      $this->queryArgs->set($object->extract());
    }

    return $this;
  }

  public function accessQuery() {
    if (empty(self::$query)) {
      $this->accessAttributes();
    }
    return self::$query;
  }

  public function resetAttributes() {
    self::$attributes = false;
    return $this;
  }

  public function buildAttributes() {
    self::$attributes = new VTCore_Wordpress_Objects_Array($this->defaults);
    self::$attributes->merge($this->atts);
    return $this;
  }

  public function accessAttributes() {

    if (empty(self::$attributes)) {
      $this->buildAttributes();
      $this->buildQuery();
    }

    return self::$attributes;
  }

}