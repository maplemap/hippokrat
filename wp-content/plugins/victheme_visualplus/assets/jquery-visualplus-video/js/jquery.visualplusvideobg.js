/**
 * 
 * VisualPlus video player.
 * 
 * This is a heavily modified script for creating
 * video element based from visualPlusVideoBG script.
 * 
 * It is only intended with use for VisualPlus plugin
 * do not include this in other project
 * 
 * @author jason.xie@victheme.com
 * @preserve Copyright 2011 Syd Lawrence ( www.sydlawrence.com );
 *
 * Licensed under MIT and GPLv2.
 *
 */

(function( $ ){

	$.fn.visualPlusVideoBG = function(options) { 
		
		if (typeof options != "object") {
		  options = {};
		}
		
		options = $.extend({}, $.fn.visualPlusVideoBG.defaults, options);

        if (options.autoplay == 'false') {
          options.autoplay = false;
        }

        if (options.scale == 'false') {
          options.scale = false;
        }

        if (options.fullscreen == 'false') {
          options.fullscreen = false;
        }

        options.loop = parseInt(options.loop);


		var container = $(this);
		
		// check if elements available otherwise it will cause issues
		if (!container.length)
			return;
		
		// container to be at least relative
		if (container.css('position') == 'static' || !container.css('position'))
			container.css('position','relative');
		
		// we need a width
		if (options.width == 0)
			options.width = container.width();
		
		// we need a height
		if (options.height == 0)
			options.height = container.height();	
		
		
		// get the video
		var video = $.fn.visualPlusVideoBG.video(options);
		
		// if we are forcing width / height
		if (options.scale) {
			// video
			video.height(options.height)
				.width(options.width);
		}
		
		container.prepend(video);

        // Container sibling needs to have position relative and higher z-index
        video.siblings().css({
          position: 'relative',
          zIndex: 1
        });

		// Free memory leaks
		container = null;
		video = null;
		options = null;
		
		return $(this).find("video")[0];
	}


	// get the formatted video element
	$.fn.visualPlusVideoBG.video = function(options) {
	
		// video container
		var $div = $('<div/>');
		$div.addClass('visualPlusVideoBG')
			.css('position',options.position)
			.css('z-index',options.zIndex)
			.css('top',0)
			.css('left',0)
			.css('height',options.height)
			.css('width',options.width)
			.css('opacity',options.opacity)
			.css('overflow','hidden');


		if (options.scale) {
		  $div      
		    .css('bottom', 0)
		    .css('right', 0)
		    .width('auto')
		    .height('auto');
		}
		
		if (options.id) {
		  $div.attr('id', options.id);
		}
		
		// video element
		var $video = $('<video/>');
		$video.css('position','absolute')
			.css('z-index',options.zIndex)
			.attr('poster',options.poster)
			.css('top',0)
			.css('bottom', 0)
      .css('right', 0)
			.css('left',0)
			.css('min-width','100%')
      .css('min-height','100%')
			.css('width','auto')
			.css('height','auto');
		
		if (options.autoplay) {
			$video.attr('autoplay',options.autoplay);
		}

		// if fullscreen
		if (options.fullscreen) {
			$video.bind('canplay',function() {
				// set the aspect ratio
				$video.aspectRatio = $video.width() / $video.height();
				$.fn.visualPlusVideoBG.setFullscreen($video);
			})

			// listen out for screenresize
			var resizeTimeout;
			$(window).resize(function() {
				clearTimeout(resizeTimeout);
				resizeTimeout = setTimeout(function() {
					$.fn.visualPlusVideoBG.setFullscreen($video);
				},100);	
			});
			$.fn.visualPlusVideoBG.setFullscreen($video);
		}
			
		
		// video standard element
		var v = $video[0];
		
		// if meant to loop
		if (options.loop) {
			loops_left = options.loop;
		
			// cant use the loop attribute as firefox doesnt support it
			$video.bind('ended', function(){
				
				// if we have some loops to throw
				if (loops_left)
					// replay that bad boy
					v.play();
				
				// if not forever
				if (loops_left !== true)
					// one less loop
					loops_left--;
  			});
		}
		
		// when can play, play
		$video.bind('canplay', function(){
			
			if (options.autoplay)
				// replay that bad boy
				v.play();
				
		});
		
		
		// if supports video
		if ($.fn.visualPlusVideoBG.supportsVideo()) {

		  	// supports webm
		    if ($.fn.visualPlusVideoBG.supportType('mp4')) {    
          // play mp4
          $video.attr('src',options.mp4);
          
          //  $video.html('<source src="'.options.mp4.'" />');
          
		    }
		  	// supports mp4
		  	else if ($.fn.visualPlusVideoBG.supportType('ogv')) {	  	
		  			
          // play ogv
          $video.attr('src',options.ogv);
		  		
		  	}
		  	// throw ogv at it then
		  	else if($.fn.visualPlusVideoBG.supportType('webm')) {
		  		
	         // play webm
          $video.attr('src',options.webm);

		  	} 	
	  	}
	  	
	  	
		
		// image for those that dont support the video	
		var $img = $('<img/>');
		$img.attr('src',options.poster)
			.css('position','absolute')
			.css('z-index',options.zIndex)
			.css('top',0)
			.css('left',0)
			.css('min-width','100%')
            .css('min-height','100%')
			.css('width','100%')
			.css('height','100%');
		


		var isMobileDevice = navigator.userAgent.match(/ipad|iphone|ipod/i) != null 
		    || screen.width <= 480;
		
		// add the image to the video
		// if supports video
		if ($.fn.visualPlusVideoBG.supportsVideo() && !isMobileDevice) {
			// add the video to the wrapper
			$div.html($video);
		}
		
		// nope - whoa old skool
		else {
			
			// add the image instead
			$div.html($img);
		}
		
		// if text replacement
		if (options.textReplacement) {
	
			// force the heights and widths
			$div.css('min-height',1).css('min-width',1);
			$video.css('min-height',1).css('min-width',1);
			$img.css('min-height',1).css('min-width',1);
			
			$div.height(options.height).width(options.width);
			$video.height(options.height).width(options.width);
			$img.height(options.height).width(options.width);	
		}
		
		if ($.fn.visualPlusVideoBG.supportsVideo()) {
			v.play();
		}
		
		// Free memory leaks
		$img = null;
		$video = null;
		
		return $div;
	}
	
	// check if suuports video
	$.fn.visualPlusVideoBG.supportsVideo = function() {
		return (document.createElement('video').canPlayType);
	}
	
	// check which type is supported
	$.fn.visualPlusVideoBG.supportType = function(str) {
		
		// if not at all supported
		if (!$.fn.visualPlusVideoBG.supportsVideo())
			return false;
		
		// create video
		var v = document.createElement('video');
		
		// check which?
		switch (str) {
			case 'webm' :
				return (v.canPlayType('video/webm; codecs="vp8, vorbis"'));
				break;
			case 'mp4' :
				return (v.canPlayType('video/mp4; codecs="avc1.42E01E, mp4a.40.2"'));
				break;
			case 'ogv' :
				return (v.canPlayType('video/ogg; codecs="theora, vorbis"'));
				break;			
		}
		// nope
		return false;	
	}
	
	
	// these are the defaults
	$.fn.visualPlusVideoBG.defaults = {
	    id: false,
        mp4:'',
        ogv:'',
        webm:'',
        poster:'',
        autoplay:true,
        loop:true,
        scale:false,
        position:"absolute",
        opacity:1,
        textReplacement:false,
        zIndex:0,
        width:0,
        height:0,
        fullscreen:false,
        imgFallback:true
	}

})(jQuery);

/**
 * Auto booting
 */
jQuery(document).ready(function($) {
  
  if (typeof visualplusVideo != 'undefined') {
    $.each(visualplusVideo, function(key, video) {
      $(video.selector + ':not(.processed)').addClass('processed').visualPlusVideoBG(video);
    });
  }
  
});