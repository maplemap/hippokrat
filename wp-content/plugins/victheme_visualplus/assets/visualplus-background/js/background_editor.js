/**
 * Visual Plus Font Params Javascript
 * 
 * Main Function :
 * 1. Create custom css
 * 2. Inject the custom css to iframe for live preview
 * 3. Load the google fonts needed
 * 4. Boot up the colorpicker
 * 5. inject data back to form
 * 
 * 
 * This needs to be injected everytime the 
 * configuration form fetched via AJAX.
 * 
 * @author jason.xie@victheme.com
 */

if (_.isUndefined(window.vc))
  var vc = {
    atts : {}
  };


(function($) {
  
  if ($('[data-background-editor=true]').length == 0) {
    return;
  }

  // Gradient parser object
  var GradientParser = {};
  
  GradientParser.parse = (function() {
    var tokens = {
      linearGradient : /^linear\-gradient/i,
      repeatingLinearGradient : /^repeating\-linear\-gradient/i,
      radialGradient : /^radial\-gradient/i,
      repeatingRadialGradient : /^repeating\-radial\-gradient/i,
      sideOrCorner : /^to (left (top|bottom)|right (top|bottom)|left|right|top|bottom)/i,
      extentKeywords : /^(closest\-side|closest\-corner|farthest\-side|farthest\-corner|contain|cover)/,
      positionKeywords : /^(left|center|right|top|bottom)/i,
      pixelValue : /^([0-9]+)px/,
      percentageValue : /^([0-9]+)\%/,
      emValue : /^([0-9]+)em/,
      angleValue : /^([0-9]+)deg/,
      startCall : /^\(/,
      endCall : /^\)/,
      comma : /^,/,
      hexColor : /^\#([0-9a-fA-F]+)/,
      literalColor : /^([a-zA-Z]+)/,
      rgbColor : /^rgb/i,
      rgbaColor : /^rgba/i,
      number : /^(([0-9]*\.[0-9]+)|([0-9]+\.?))/
    };
    var input = '';
    
    function error(msg) {
      var err = new Error(input + ': ' + msg);
      err.source = input;
      throw err;
    }
    
    function getAST() {
      var ast = matchListDefinitions();
      if (input.length > 0) {
        error('Invalid input not EOF');
      }
      return ast;
    }
    
    function matchListDefinitions() {
      return matchListing(matchDefinition);
    }
    
    function matchDefinition() {
      return matchGradient('linear-gradient', tokens.linearGradient,
          matchLinearOrientation)
          || matchGradient('repeating-linear-gradient',
              tokens.repeatingLinearGradient, matchLinearOrientation)
          || matchGradient('radial-gradient', tokens.radialGradient,
              matchListRadialOrientations)
          || matchGradient('repeating-radial-gradient',
              tokens.repeatingRadialGradient, matchListRadialOrientations);
    }

    function matchGradient(gradientType, pattern, orientationMatcher) {
      return matchCall(pattern, function(captures) {
        var orientation = orientationMatcher();
        if (orientation) {
          if (!scan(tokens.comma)) {
            error('Missing comma before color stops');
          }
        }
        return {
          type : gradientType,
          orientation : orientation,
          colorStops : matchListing(matchColorStop)
        };
      });
    }

    function matchCall(pattern, callback) {
      var captures = scan(pattern);
      if (captures) {
        if (!scan(tokens.startCall)) {
          error('Missing (');
        }
        result = callback(captures);
        if (!scan(tokens.endCall)) {
          error('Missing )');
        }
        return result;
      }
    }

    function matchLinearOrientation() {
      return matchSideOrCorner() || matchAngle();
    }

    function matchSideOrCorner() {
      return match('directional', tokens.sideOrCorner, 1);
    }

    function matchAngle() {
      return match('angular', tokens.angleValue, 1);
    }

    function matchListRadialOrientations() {
      var radialOrientations, radialOrientation = matchRadialOrientation(), lookaheadCache;
      if (radialOrientation) {
        radialOrientations = [];
        radialOrientations.push(radialOrientation);
        lookaheadCache = input;
        if (scan(tokens.comma)) {
          radialOrientation = matchRadialOrientation();
          if (radialOrientation) {
            radialOrientations.push(radialOrientation);
          } else {
            input = lookaheadCache;
          }
        }
      }
      return radialOrientations;
    }

    function matchRadialOrientation() {
      var radialType = matchCircle() || matchEllipse();
      if (radialType) {
        radialType.at = matchAtPosition();
      } else {
        var defaultPosition = matchPositioning();
        if (defaultPosition) {
          radialType = {
            type : 'default-radial',
            at : defaultPosition
          };
        }
      }
      return radialType;
    }

    function matchCircle() {
      var circle = match('shape', /^(circle)/i, 0);
      if (circle) {
        circle.style = matchLength() || matchExtentKeyword();
      }
      return circle;
    }

    function matchEllipse() {
      var ellipse = match('shape', /^(ellipse)/i, 0);
      if (ellipse) {
        ellipse.style = matchDistance() || matchExtentKeyword();
      }
      return ellipse;
    }

    function matchExtentKeyword() {
      return match('extent-keyword', tokens.extentKeywords, 1);
    }

    function matchAtPosition() {
      if (match('position', /^at/, 0)) {
        var positioning = matchPositioning();
        if (!positioning) {
          error('Missing positioning value');
        }
        return positioning;
      }
    }

    function matchPositioning() {
      var location = matchCoordinates();
      if (location.x || location.y) {
        return {
          type : 'position',
          value : location
        };
      }
    }

    function matchCoordinates() {
      return {
        x : matchDistance(),
        y : matchDistance()
      };
    }

    function matchListing(matcher) {
      var captures = matcher(), result = [];
      if (captures) {
        result.push(captures);
        while (scan(tokens.comma)) {
          captures = matcher();
          if (captures) {
            result.push(captures);
          } else {
            error('One extra comma');
          }
        }
      }
      return result;
    }

    function matchColorStop() {
      var color = matchColor();
      if (!color) {
        error('Expected color definition');
      }
      color.length = matchDistance();
      return color;
    }

    function matchColor() {
      return matchHexColor() || matchRGBAColor() || matchRGBColor()
          || matchLiteralColor();
    }

    function matchLiteralColor() {
      return match('literal', tokens.literalColor, 0);
    }

    function matchHexColor() {
      return match('hex', tokens.hexColor, 1);
    }

    function matchRGBColor() {
      return matchCall(tokens.rgbColor, function() {
        return {
          type : 'rgb',
          value : matchListing(matchNumber)
        };
      });
    }

    function matchRGBAColor() {
      return matchCall(tokens.rgbaColor, function() {
        return {
          type : 'rgba',
          value : matchListing(matchNumber)
        };
      });
    }

    function matchNumber() {
      return scan(tokens.number)[1];
    }

    function matchDistance() {
      return match('%', tokens.percentageValue, 1) || matchPositionKeyword()
          || matchLength();
    }

    function matchPositionKeyword() {
      return match('position-keyword', tokens.positionKeywords, 1);
    }

    function matchLength() {
      return match('px', tokens.pixelValue, 1)
          || match('em', tokens.emValue, 1);
    }

    function match(type, pattern, captureIndex) {
      var captures = scan(pattern);
      if (captures) {
        return {
          type : type,
          value : captures[captureIndex]
        };
      }
    }

    function scan(regexp) {
      var captures, blankCaptures;
      blankCaptures = /^[\n\r\t\s]+/.exec(input);
      if (blankCaptures) {
        consume(blankCaptures[0].length);
      }
      captures = regexp.exec(input);
      if (captures) {
        consume(captures[0].length);
      }
      return captures;
    }

    function consume(size) {
      input = input.substr(size);
    }

    return function(code) {
      input = code.toString();
      return getAST();
    };

  })();



  /**
   * Filler fallback for native trim()
   */
  if (!String.prototype.trim) {
    String.prototype.trim = function trim() {
      return this.toString().replace(/^([\s]*)|([\s]*)$/g, '');
    };
  }


  /**
   * Filler for contains
   */
  if (typeof String.prototype.contains === 'undefined') {
    String.prototype.contains = function(it) { return this.indexOf(it) != -1; };
  }


  /**
   * Connect to VC
   */
  var VisualPlusBackgroundEditor = Backbone.View.extend({

    render : function(value) {
      _.isString(value) && this.parse(value);
      return this;
    },

    parse : function(value) {
      var data_split = value.split(/\s*(\.[^\{]+)\s*\{\s*([^\}]+)\s*\}\s*/g);
      data_split[2]
          && this.parseAtts(data_split[2].replace(/\s+!important/g, ''));

      if (data_split[3] && data_split[3].indexOf('keyframes') !== -1) {
        this.parseKeyframe(data_split[3]);
      }
    },

    showMode: 'image',

    parseKeyframe : function(value) {

      var regexRules = {
          from : /\sfrom(([^{}]+){([^{};]+:[^{};]+;\s*)+})/g,
          to : /\sto(([^{}]+){([^{};]+:[^{};]+;\s*)+})/g
      };

      _.each(regexRules, function(regex, rulekey) {

        _.each(value.match(regex), function(rule) {
          rules = rule.split(/background-position:([^;]*)/);

          if (typeof rules[1] != 'undefined') {
            _.each(rules[1].split(','), function(value, key) {
              this.$el
                .find('.wp-background-picker tbody tr').eq(key)
                .find('[name="background-image[keyframe][frames]['+ rulekey.trim() + '][background][position][]"]').val(value.trim()).trigger('change');
            }, this);
          }

        }, this);

      }, this);

    },
    parseAtts : function(string) {

      var rules = string.split(';');

      _.each(rules, function(rule) {
        var value = rule.split(': ');

        switch (value[0].trim()) {

          case 'background-color' :
            this.$el.find('[name="background-image[background][color]"]').val(value[1].trim());
            this.showMode = 'image';
            break;

          case 'background-masking' :
            this.$el.find('[name="background-image[background][masking]"]').val(value[1].trim());
            this.showMode = 'image';
            break;

          case 'background-image' :
          case 'background-attachment' :
          case 'background-position' :
          case 'background-size' :
          case 'background-repeat' :

            this.updateImage(value[0].replace('background-', ''), value[1].trim());
            this.showMode = 'image';

            break;

          case 'background-parallax' :

            this.$el.find('[name="background-image[background][parallax]"]').val(value[1].trim());
            this.showMode = 'image';
            break;

          case 'animation-name' :
          case 'animation-delay' :
          case 'animation-direction' :
          case 'animation-fill-mode' :
          case 'animation-play-state' :
          case 'animation-timing-function' :
          case 'animation-duration' :
          case 'animation-iteration-count' :

            this.updateAnimation(value[0].replace('animation-', ''), value[1].trim());
            this.showMode = 'image';

            break;


          case 'background' :

            this.showMode = 'gradient';
            this.updateGradient(value[1]);

            break;

          case 'video-source-mp4' :
          case 'video-source-ogv' :
          case 'video-source-webm' :
          case 'video-poster' :
          case 'video-position' :
          case 'video-autoplay' :
          case 'video-loop' :
          case 'video-scale' :
          case 'video-width' :
          case 'video-height' :

            this.showMode = 'video';
            this.updateVideo(value[0], value[1].trim());

            break;
        }

      }, this);

      this.$el.find('[data-show="' + this.showMode + '"]').click();

    },
    updateVideo: function(key, value) {
      var target = '';

      if (key.indexOf('source') !== -1) {
        target = '[source][' + key.split('-')[2].trim() + ']';
      }
      else {
        target = '[' + key.split('-')[1].trim() + ']';
      }

      // Only update if value change
      if (this.$el.find('[name="background-video[video]' + target + '"]').val() != value) {
        this.$el.find('[name="background-video[video]' + target + '"]').val(value);

        if (key.indexOf('source') !== -1
            || target == '[poster]') {

          this.$el
            .find('[name="background-video[video]' + target + '"]')
            .attr('data-media-url', value)
            .attr('data-media-filetype', (target == '[poster]') ? 'image' : 'video' )
            .trigger('change');

        }
      }

    },
    updateGradient: function(value) {

      // Create dummy element
      var holder = document.createElement('div');
          holder.style.background = value,
          options = GradientParser.parse(holder.style.backgroundImage.replace('-moz-','').replace('-o-','').replace('-webkit-', ''));

      if (typeof options[0] != 'undefined') {
        options = options[0];

        $(holder).remove();

        this.$el.find('[name="background-gradient[gradient][type]"]').val(options.type.replace('-gradient', '').replace('repeating-', ''));

        this.$el.find('[name="background-gradient[gradient][repeat]"]').removeAttr('checked', 'checked').trigger('change');
        if (options.type.indexOf('repeating') !== -1) {
          this.$el.find('[name="background-gradient[gradient][repeat]"]').attr('checked', 'checked').trigger('change');
        }

        if (options.type.indexOf('radial') !== -1) {
          if (typeof options.orientation[0].at != 'undefined') {

            var position = '';

            if (typeof options.orientation[0].at.value.x.value != 'undefined') {
              position += options.orientation[0].at.value.x.value;
            }

            if (typeof options.orientation[0].at.value.x.type != 'undefined' && options.orientation[0].at.value.x.type != 'position-keyword') {
              position += options.orientation[0].at.value.x.type;
            }

            if (typeof options.orientation[0].at.value.y.value != 'undefined') {
              position += ' ' + options.orientation[0].at.value.y.value;
            }

            if (typeof options.orientation[0].at.value.y.type != 'undefined' && options.orientation[0].at.value.y.type != 'position-keyword') {
              position += options.orientation[0].at.value.y.type;
            }

            this.$el.find('[name="background-gradient[gradient][settings][position]"]').val(position);
          }
          this.$el.find('[name="background-gradient[gradient][settings][shape]"]').val(options.orientation[0].value);
          this.$el.find('[name="background-gradient[gradient][settings][size]"]').val(options.orientation[0].style.value);
        }

        var offset = 0;

        if (options.type.indexOf('linear') !== -1) {
          offset = -1;
        }

        _.each(options.colorStops, function (data, key) {

          // @bug linear settings makes the color stops dirty.
          if (key == 0 && options.type.indexOf('linear') !== -1) {
            if (options.type.indexOf('linear') !== -1) {
              this.$el.find('[name="background-gradient[gradient][settings][direction]"]').val(data.value.trim() + ' ' + data.length.value.trim());
            }
          }
          else {
            var color = '';
            switch (data.type) {

              case 'hsl' :
              case 'hsv' :
              case 'rgba':
              case 'rgb' :
                color = data.type + '(' + data.value.join(', ').trim() + ')';
                break;

              case 'hex' :
                color = '#' + data.value;
                break;
            }

            var delta = key + offset;

            if (this.$el.find('[name="background-gradient[gradient][colors][' + delta + '][stop]"]').length == 0) {
              this.$el.find('[data-tablemanager-type="addrow"]').click();
            }

            if (typeof data.length != 'undefined' && typeof data.length.value != 'undefined' && typeof data.length.type != 'undefined') {
              this.$el.find('[name="background-gradient[gradient][colors][' + delta + '][stop]"]').val(data.length.value.trim() + data.length.type.trim());
            }

            this.$el.find('[name="background-gradient[gradient][colors][' + delta + '][color]"]').val(color);

          }

        }, this);

        this.$el.find('[name="background-gradient[gradient][type]"]').trigger('change');
      }
    },
    updateImage : function(elkey, value) {
      elkey =  elkey.trim();

      _.each(value.split(','), function(val, key) {

        if (this.$el.find('.wp-background-picker tbody tr').eq(key).length == 0) {
          this.$el.find('.wp-background-picker [data-tablemanager-type="addrow"]').click();
        }

        var row = this.$el.find('.wp-background-picker tbody tr').eq(key),
            target = this.$el.find('.wp-background-picker tbody tr').eq(key).find('[name="background-image[background][' + elkey + '][]"]');

        if (target.length != 0) {
          target.val(val.trim()).trigger('change');
        }

        if (elkey == 'image') {
          target
            .attr('data-media-url', val.trim().replace('url(', '').replace(')', ''))
            .attr('data-media-filetype', 'image')
            .trigger('change');
        }

      }, this);

    },
    updateAnimation : function(elkey, value) {
      elkey =  elkey.trim();

      this.$el
        .find('[name="background-image[animation][' + elkey + ']"]').val(value.trim()).trigger('change');

    },
    save : function() {

      var string = '', frame = '', extraClass = '', extraCSS = '', uniqID = (+new Date);

      switch (this.$el.find('.btn-warning').data('show')) {

        case 'image' :

          // Bail on invalid rules, no image nor color specified
          if (!this.$el.find('[name="background-image[background][color]"]').val()
              && !this.$el.find('[name="background-image[background][masking]"]').val()
              && !this.$el.find('[name="background-image[background][image][]"]').val()) {
            break;
          }

          var styles = this.$el.find('.wp-background-picker-preview').attr('style').split(';'),
              processed = [];

          _.each(styles, function(val, key) {

            if (val && val.indexOf('animation') === -1) {
              // Don't add !important, firefox cant animate properly!
              var value = ' ' + val.replace(/["]/g, '').replace(/[;]/g, '').trim();

              if (typeof value != 'undefined' && value != 'none' && value != 'initial') {
                processed.push(value);
              }
            }

          }, this);

          // Background Masking
          if (this.$el.find('[name$="[masking]"]').val().length) {
            processed.push(' ' + 'background-masking: ' + this.$el.find('[name$="[masking]"]').val());
            extraCSS = ' .masking {background-color:' + this.$el.find('[name$="[masking]"]') + ';}';
          }

          // Background parallax
          if (this.$el.find('[name$="[parallax]"]').val() != 'none') {
            processed.push(' ' + 'background-parallax: ' + this.$el.find('[name$="[parallax]"]').val());
            extraClass = '.' + this.$el.find('[name$="[parallax]"]').val();
          }

          // Some browser will automatically convert the animation to shorthand syntax
          // causing all sort of problems!. parse the value manually
          var AnimationName = this.$el.find('[data-animation-type="name"]').find('[name]').val().trim();
          if (AnimationName != 'none' && AnimationName.length != 0) {
            this.$el.find('[data-animation-type]').each(function () {
              var type = $(this).data('animation-type'),
                value = $(this).find('[name]').val().trim();

              if (value != 'none'
                  && value != 'normal'
                  && value != 'running'
                  && value != 'ease') {

                var prefixes = ['-moz-', '-webkit-', '-o-', ''];

                _.each(prefixes, function (prefix) {
                  processed.push(' ' + prefix + 'animation-' + type + ': ' + value);
                }, this);

              }
            });
          }

          string += processed.join(";");

          break;

        case 'gradient' :
          var prefixes = ['-moz-', '-webkit-', '-o-', ''],
              styles = [],
              sources = this.$el
                .find('.wp-gradient-picker-preview')
                .attr('style')
                .replace('-moz-', '')
                .replace('-webkit-', '')
                .replace('-o-', '')
                .split(';');


          // bail on invalid rules, no valid gradient styles found
          if (sources.length == 0) {
            break;
          }

          _.each(sources, function(val, key) {
            if (val.length != 0) {
              _.each(prefixes, function (prefix) {
                var value = val
                  .replace(' linear-gradient', ' ' + prefix + 'linear-gradient')
                  .replace(' radial-gradient', ' ' + prefix + 'radial-gradient')
                  .replace(' repeating-linear-gradient', ' ' + prefix + 'repeating-linear-gradient')
                  .replace(' repeating-radial-gradient', ' ' + prefix + 'repeating-radial-gradient')
                  .trim();

                styles.push(' ' + value + ' !important');
              }, this);
            }
          }, this);

          string += styles.join(";");

          break;

        case 'video' :

          var video = this.$el.find('.wp-background-video'), rules = [];

          video.find('video').each(function() {
            var type = $(this).closest('[data-type="video"]').data('video-type');
            $(this).attr('src') && rules.push('video-source-' + type + ': ' + $(this).attr('src'));
          });

          // bail out on invalid rule, no video specified
          if (rules.length == 0) {
            break;
          }

          video.find('img').each(function() {
            $(this).attr('src') && rules.push('video-poster: ' + $(this).attr('src'));
          });

          video.find('.wp-background-video-options').find('input[type="text"], select').each(function() {
            $(this).val() && rules.push('video-' + $(this).attr('name').split('[').pop().replace(']', '') + ': ' + $(this).val());
          });

          string += rules.join(";");

          break;
      }

      if (string != '') {
        string = extraClass + ".visualplusbackground_" + uniqID + ' {' + string.replace(/["]/g, '') + ';}';
      }

      if (this.$el.find('.wp-background-picker-preview style').length != 0
          && this.$el.find('.btn-warning').data('show') == 'image') {
        frame = this.$el.find('.wp-background-picker-preview style').text();
      }

      string && vc.frame_window
             && vc.frame_window.vc_iframe.setCustomShortcodeCss(string);

      frame && vc.frame_window
            && vc.frame_window.vc_iframe.setCustomShortcodeCss(frame);


      if (extraCSS != '') {
        extraCSS = ".visualplusbackground_" + uniqID + extraCSS;
        vc.frame_window && vc.frame_window.vc_iframe.setCustomShortcodeCss(extraCSS);
      }

      return string + frame;
    }
  });

  /**
   * Add new param to atts types list for vc
   */
  vc.atts.background_editor = {
    parse : function(param) {

      var $field = this.content().find('[data-background-value]'), background_editor = $field
          .data('backgroundEditor'), result = background_editor.save();

      return result;
    }
  };




  /**
   * Find all fields with css_editor type and initialize.
   */
  $('[data-background-editor=true]').each(function() {
    var $editor = $(this), $field = $editor.find('[data-background-value]'), value = $field
        .val();

    // Auto booting color picker
    // @todo find a way to use native VTCore booting instead of this
    $editor.find('.bootstrap-colorpicker').each(function() {
      $(this).colorpicker({
        container: $(this).data('container') || false
      });
    });


    // Bind the button toggle
    $editor
      .on('click', '[data-show]', function() {

        $editor.find('[data-show]').removeClass('btn-warning');
        $(this).addClass('btn-warning');

        $editor.find('[data-target]').fadeOut().removeClass('btn-warning');
        $editor.find('[data-target="' + $(this).data('show') + '"]').fadeIn();
      });

    $editor.find('[data-show="image"]').click();

    // Boot the gradient element
    $editor.find('.wp-gradient-picker-controller select').trigger('change');

    // Booting the fonts script
    $field.data('backgroundEditor', new VisualPlusBackgroundEditor({
      el : $editor
    }).render(value));

  });

  // Rebinding the events
  $(document).on('tablemanager-addrow', function(events, clone) {
    setTimeout(function() {
      $(clone).find('.bootstrap-colorpicker').each(function() {
        var self = $(this);
        self.colorpicker('destroy').removeData().colorpicker({
          container: self.data('container') || false
        });
      });

      $(clone).parent().find('tr').eq(0).find('.bootstrap-colorpicker').each(function() {
        var self = $(this);
        self.colorpicker({
          container: self.data('container') || false
        });
      })
    }, 10);
    
  }); 

})(window.jQuery);