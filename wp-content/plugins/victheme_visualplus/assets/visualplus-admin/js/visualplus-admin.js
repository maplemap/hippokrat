/**
 * Hooking into VisualComposer FrontEditor
 * 
 * VisualPlus needs to invoke custom scripts on all elements
 * when Visual Composer is updating the shortcode elements.
 * 
 * @author jason.xie@victheme.com
 */
(function($) {

  $(window)
    .on('vc_ready', function() {

      /**
       * Inject additional method to the model object
       */
      vc.shortcodes.model = vc.shortcodes.model.extend({
        destroyBackgroundVideo: function() {

          var Params = this.get('params'),
              ID = 'video-for-' + this.get('id');

          // Build video background on all element
          if (Params.background && Params.background.indexOf('video-source') !== -1) {

            var Video = $(vc.frame_window.document).find('#' + ID);

            // Break memory leaks.
            if (Video.length != 0) {

              Video
                .find('video')
                .each(function() {
                  this.pause();
                  delete(this);
                  $(this).remove();
                });

              Video
                .empty()
                .remove();

              delete(Video);
            }

          }
          Params = null;
          delete(Params);

        },
        renderBackgroundVideo: function() {
          
          var Params = this.get('params');

          // Build video background on all element
          if (Params.background && Params.background.indexOf('video-source') !== -1) {

            var css = Params.background.split(/\s*(\.[^\{]+)\s*\{\s*([^\}]+)\s*\}\s*/g),
                options = {};

            _.each(css[2].split(';'), function(rule) {
              if (rule.indexOf('video') !== -1) {
                options[rule.split(/:(.+)?/, 2)[0].replace('video-', '').replace('source-', '').trim()] = rule.split(/:(.+)?/, 2)[1].trim();
              }
            }, this);

            options.id = 'video-for-' + this.get('id');

            this.destroyBackgroundVideo();

            $.isFunction($.fn.visualPlusVideoBG)
              && options
              && this.view.$el.find(css[1]).visualPlusVideoBG(options);
          }

          Params = null;
          delete(Params);

        }
      });

      vc.events
        .on('shortcodeView:ready', function(model) {
          if (typeof model.renderBackgroundVideo != 'undefined') model.renderBackgroundVideo();
        })
        .on('shortcode:destroy', function(model) {
          if (typeof model.renderBackgroundVideo != 'undefined') model.destroyBackgroundVideo();
        });

    });

})(window.jQuery);
