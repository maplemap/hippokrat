/**
 * Visual Plus Font Params Javascript
 * 
 * Main Function :
 * 1. Create custom css
 * 2. Inject the custom css to iframe for live preview
 * 3. Load the google fonts needed
 * 4. Boot up the colorpicker
 * 
 * 
 * This needs to be injected everytime the 
 * configuration form fetched via AJAX.
 * 
 * @author jason.xie@victheme.com
 */
if (_.isUndefined(window.vc))
  var vc = {
    atts : {}
  };

(function($) {

  if ($('[data-font-editor=true]').length == 0) {
    return;
  }

  /**
   * Filler fallback for native trim()
   */
  if (!String.prototype.trim) {
    String.prototype.trim = function trim() {
      return this.toString().replace(/^([\s]*)|([\s]*)$/g, '');
    };
  }
  
  
  
  /**
   * Extending vc_iframe methods for loading google fonts
   * 
   * @todo do smart method when we are capable to determine 
   *       what family is capable to load which weight and styles
   */
  if (typeof vc.frame_window != 'undefined' && typeof vc.frame_window.vc_iframe != 'undefined') {
    vc.frame_window.vc_iframe.loadGoogleFonts = function(fonts) {
  
      if (typeof fonts.family == 'undefined' 
          || fonts.family == false 
          || fonts.family == '') {
        
        return false;
      }
      
      var font = fonts.family;
      
      if (typeof fonts.weight != 'undefined' 
          && fonts.weight != false 
          && fonts.weight != '') {
        
        font += ':' + fonts.weight;
      }
      
      if (typeof fonts.style != 'undefined' 
          && fonts.style != false 
          && fonts.style != '') {
        
        font += fonts.style;
      }
      
      if (!this.fonts) {
        this.fonts = {};
      }
      
      
      if (typeof this.fonts[font] == 'undefined') {
        this.fonts[font] = fonts;
        $('<link media="all" type="text/css" href="http://fonts.googleapis.com/css?family=' + font + '&subset=latin,cyrillic-ext,cyrillic,greek-ext,greek,vietnamese,latin-ext" rel="stylesheet"/>')
          .prependTo('head');
      }
  
    };
  }

  
  
  
  /**
   * Connect to VC
   */
  var VisualPlusFontEditor = Backbone.View.extend({

    events: {
      'change .wp-fonts input': 'updateStorage',
      'change .wp-fonts select': 'updateStorage',
      'change .wp-fonts textarea': 'updateStorage'
    },

    render : function(value) {
      var value = this.$storage.val()
      _.isString(value) && this.parse(value);

      return this;
    },
    
    parse : function(value) {
      var data_split = value.split(/\s*(\.[^\{]+)\s*\{\s*([^\}]+)\s*\}\s*/g);
      data_split[2]
          && this.parseAtts(data_split[2].replace(/\s+!important/g, ''));
    },

    initialize: function () {
      this.$form = this.$el.find('.wp-fonts');
      this.$storage = this.$el.find('[data-font-value]');
    },
    
    parseAtts : function(string) {

      var rules = string.split(';'), that = this;

      _.each(rules, function(rule) {
        var value = rule.split(':');

        if (value[0] && value[1]) {
          var index = value[0].trim().replace('font-', '').replace('line-', '').replace('letter-', '').replace('text-', '');

          if (index == 'family') {
            value[1].replace(' ', '+');
          }

          if (index == 'weight') {
            value[1] = String(value[1]);
          }

          value[1] = value[1].trim();

          _.defer(function() {
            that.$el.find('[name="fonts[font][' + index + ']"]').val(value[1]);

            if (index == 'color') {
              that.$el.find('[name="fonts[font][' + index + ']"]').next().find('i').css('background-color', value[1]);
            }
          }, this);
        }

      }, this);

    },

    updateStorage: function() {
      var string = '';
      this.$form.find('[name^="fonts[font]"]').each(function() {
        var val = $(this).val(),
          index = $(this).attr('name').replace('fonts[font][', '').replace(']',''),
          rule = 'font-' + index;

        if (val && index && rule) {
          if (val.length != 0 && val != '' && val != 0) {
            if ($(this).attr('name').indexOf('family') !== -1) {
              val.replace('+', ' ');
            }

            if (index == 'color') {
              rule = 'color';
            }

            if (index == 'height') {
              rule = 'line-height';
            }

            if (index == 'shadow') {
              rule = 'text-shadow';
            }

            if (index == 'spacing') {
              rule = 'letter-spacing';
            }

            string += rule + ': ' + val + ' !important;';
          }
        }

      });

      if (string != '') {
        var selector = '.visualplus_' + (+new Date);
        string = selector + ' {' + string + '}';
      }

      this.$storage.val(string);

      return string;
    },
    
    save : function() {

      this.updateStorage();

      var string = this.$storage.val(),
          fonts = {
            family: this.$el.find('[name="fonts[font][family]"]').val() || false,
            weight: this.$el.find('[name="fonts[font][weight]"]').val() || false,
            style: this.$el.find('[name="fonts[font][style]"]').val() || false
          }

      string && vc.frame_window
             && vc.frame_window.vc_iframe.setCustomShortcodeCss(string);
      
      fonts.family 
          && vc.frame_window
          && vc.frame_window.vc_iframe.loadGoogleFonts(fonts);

      return string;
    }
  });
  
  
  
  
  
  /**
   * Add new param to atts types list for vc
   */
  vc.atts.font_editor = {
    init: function(param, $field) {
      var $container = $field.find('[data-font-value]');
      $container.data('fontEditor',   new VisualPlusFontEditor({ el : $field }).render());

      $field.find('.bootstrap-colorpicker').each(function() {
        $(this).colorpicker({
          container: $(this).data('container') || false
        });
      });
    },
    parse: function(param) {

      var $field = this.content().find('[data-font-value]'),
          font_editor = $field.data('fontEditor'),
          result = font_editor.save();

      return result;
    }
  };

})(window.jQuery);