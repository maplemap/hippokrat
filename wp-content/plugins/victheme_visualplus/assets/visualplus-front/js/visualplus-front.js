/**
 * Collection of script for formatting the visual plus
 * additional styling and features.
 * 
 * @author jason.xie@victheme.com
 */
(function($){

  var VTVisualPlus = {
    $doc: $(document),
    $win: $(window)
  };

  // Masking
  if (typeof visualplusMasking != 'undefined') {
    VTVisualPlus.masking = {
      $el: {},
      init: function() {
        var that = this;
        $.each(visualplusMasking, function(key, target) {
          var self = $(target);

          if (self.children().filter('.masking').length == 0){
            self.prepend('<div class="masking"></div>');
          }
          self.css('position', 'relative').find('.masking').eq(0).css({
            position: 'absolute',
            top: 0,
            left: 0,
            width: '100%',
            height: '100%',
            zIndex: 1
          });

          self.children().not('.masking').css({
            position: 'relative',
            zIndex: 2
          });

          that.$el[key] = self;
        });
      }
    }

    VTVisualPlus.masking.init();
  }

  // Fullscreen
  if (VTVisualPlus.$doc.find('.js-fullscreen').length) {
    VTVisualPlus.fullHeight = {
      init: function() {
        this.$el = VTVisualPlus.$doc.find('.js-fullscreen');
        return this;
      },
      destroy: function() {
        this.$el.each(function() {
          $(this).css('width', '').css('height', '');
        });
        return this;
      },
      reposition: function() {
        var that = this;
        this.$el.each(function() {
          that.$current = $(this);
          if (that.$current.height() < VTVisualPlus.$win.height()) {
            that.$current.height(VTVisualPlus.$win.height()).width(VTVisualPlus.$win.width());
          }
          else if (that.$current.height() > VTVisualPlus.$win.height()){
            that.$current.css('width', '').css('height', '');
          }
        });

        return this;
      }
    }

    VTVisualPlus.fullHeight.init().reposition();
  }

  /**
  if (VTVisualPlus.$doc.find('.js-nicescroll')) {
    VTVisualPlus.niceScroll = {
      init: function() {
        this.$el = VTVisualPlus.$doc.find('.js-nicescroll').find('.wpb_row');
        return this;
      },
      reposition: function() {
        this.$el.each(function() {
          //$(this).niceScroll();
        })
      }
    }

    VTVisualPlus.niceScroll.init().reposition();
  }
   **/

  // Vertical center
  if (VTVisualPlus.$doc.find('.js-verticalcenter').length) {
    VTVisualPlus.verticalCenter = {
      init: function() {
        this.$el = {};
        this.$parent = {};
        var that = this;
        VTVisualPlus.$doc.find('.js-verticalcenter').each(function(key, items) {
          that.$el[key] = $(this).find('.wpb_row').eq(0);
          that.$el[key].addClass('js-verticaltarget');
          that.$parent[key] = $(this);
        });

        return this;
      },
      destroy: function() {
        var that = this;
        $.each(this.$el, function(key, item) {
          $(this).css('margin-top', '').css('top', '');
        });

        return this;
      },
      reposition: function() {
        var that = this;
        $.each(this.$el, function(key, item) {
          that.$current = $(this);

          if (that.$current.height() < that.$parent[key].height()) {
            that.$current.css({
              position: 'relative',
              marginTop: (that.$parent[key].height() - that.$current.height()) / 2
            });
          }
          else {
            that.$current.css('margin-top', '').css('top', '');
          }
        });

        return this;
      }
    }

    VTVisualPlus.verticalCenter.init().reposition();
  }


  // Deck Mode
  if (VTVisualPlus.$doc.find('.js-deckmode').length) {

    VTVisualPlus.deckMode = {
      init: function() {
        this.$parent = VTVisualPlus.$doc.find('.js-deckmode').eq(0).parent();

        this.$parent.snapscroll();
      }
    }
  }

  // Fit Text
  if (VTVisualPlus.$doc.find('.wpb_text_column.fittext p').length) {

    VTVisualPlus.fitText = {
      init: function() {
        this.$el = VTVisualPlus.$doc.find('.fittext');

        // Fix VC Text failed to use fittext js
        this.fixVCText();
        return this;
      },
      fixVCText: function() {
        this.$el.filter('.wpb_text_column').removeClass('fittext').find('p').addClass('fittext');
      }
    }

    VTVisualPlus.fitText.init();
  }
  
  
  $(window)
    .on('resize.visualplus resize_end.visualplus', function() {
      setTimeout(function() {
        VTVisualPlus.fullHeight && VTVisualPlus.fullHeight.destroy().reposition();
      }, 1)

      setTimeout(function() {
        VTVisualPlus.verticalCenter && VTVisualPlus.verticalCenter.destroy().reposition();
      }, 1)
    })
    .on('load.visualplus pageready.visualplus', function() {
      VTVisualPlus.fullHeight && VTVisualPlus.fullHeight.reposition();
      VTVisualPlus.verticalCenter && VTVisualPlus.verticalCenter.reposition();
      // VTVisualPlus.niceScroll && VTVisualPlus.niceScroll.reposition();
    });

  $(document)
    .on('ready.visualplus', function() {
      // Don't call this before document ready to avoid deps not loaded yet bug.
      VTVisualPlus.deckMode && VTVisualPlus.deckMode.init();
    })
    .on('ajaxComplete.visualplus', function() {
      VTVisualPlus.fullHeight && VTVisualPlus.fullHeight.init().reposition();
      VTVisualPlus.verticalCenter && VTVisualPlus.verticalCenter.init().reposition();
      // VTVisualPlus.niceScroll && VTVisualPlus.niceScroll.init().reposition();
    })

})(jQuery);