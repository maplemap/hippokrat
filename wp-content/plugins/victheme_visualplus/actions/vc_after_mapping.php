<?php
/**
 * Hooking to vc_after_mapping action for adding
 * additional options to visual composer elements
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_VisualPlus_Actions_VC__After__Mapping
extends VTCore_Wordpress_Models_Hook {

  protected $weight = 999;

  private $grid = array(
    'vc_gallery' => array(
      'mobile' => 12,
      'tablet' => 6,
      'small' => 4,
      'large' => 3,
    ),
  );

  public function hook() {

    // Adding extra grid parameters to default VC element
    // @todo do full blown column entries and add as tab group
    foreach ($this->grid as $element => $columns) {
      foreach ($columns as $name => $size) {
        vc_add_param($element,  array(
          'type' => 'vtcore',
          'group' => __('Grids', 'victheme_visualplus'),
          'param_name' => 'columns_' . $name,
          'name' => 'columns_' . $name,
          'empty' => '',
          'core_class' => 'VTCore_Bootstrap_Form_BsSelect',
          'edit_field_class' => 'col-md-3',
          'core_context' => array(
          'text' =>  __('Columns', 'victheme_visualplus') . ' ' . ucfirst($name),
          'name' => 'columns_' . $name,
          'value' => $size,
          'input_elements' => array(
            'attributes' => array(
              'class' => array('wpb_vc_param_value', 'wpb-dropdown', 'columns_' . $name, 'vtcore_field', 'wpb-input', 'wpb-select')
            ),
          ),
            'options' => range(0, 12, 1),
          ),
        ));
      }
    }



    // Processing the separator line to add extra options
    vc_add_param('vc_separator',  array(
      'type' => 'textfield',
      'heading' => __('Line Height', 'victheme_visualplus'),
      'param_name' => 'height',
      'description' => __('Separator height in pixels.', 'victheme_visualplus'),
    ));

    vc_add_param('vc_separator',  array(
      'type' => 'textfield',
      'heading' => __('Maximum Width', 'victheme_visualplus'),
      'param_name' => 'maxwidth',
      'description' => __('line maximum width in pixels.', 'victheme_visualplus'),
    ));




    // Hooking VisualPlus font forms to visual composer
    vc_add_shortcode_param('font_editor', array($this, 'fontEditorForm'));
    vc_add_shortcode_param('background_editor', array($this, 'backgroundEditorForm'));

    // Inject all shortcodes with fonts form.
    // @todo some of the shortcode will failed to display the fonts
    //       properly due to the actual injection depends on css class
    //       and the shortcode needs to invoke vc_shortcode_css_class when building
    //       the target element css class name.
    foreach (WPBMap::getShortCodes() as $id => $shortcode) {

      // Bug fix 4.53 vc some shortcode doesn't have params array
      if (!isset($shortcode['params'])) {
        continue;
      }

      if (in_array($id, array('vc_button', 'vc_button2', 'vc_cta_button', 'vc_cta_button2'))) {
        vc_add_param($id, array(
          'type' => 'css_editor',
          'heading' => __('Css', 'victheme_visualplus'),
          'param_name' => 'css',
          'group' => __('Design options', 'victheme_visualplus')
        ));
      }

      // Only inject the font editor if shortcode has no
      // shortcode attribute called font to avoid
      // visual plus overriding default shortcode attr by mistake.
      if (!$this->checkParamsExists($shortcode['params'], 'font')) {
        vc_add_param($id,  array(
          'type' => 'font_editor',
          'param_name' => 'font',
          'group' => __('Fonts', 'victheme_visualplus'),
        ));
      }

      // Only inject the background editor if shortcode has no
      // shortcode attribute called background to avoid
      // visual plus overriding default shortcode attr by mistake.
      if (!$this->checkParamsExists($shortcode['params'], 'background')) {
        vc_add_param($id,  array(
          'type' => 'background_editor',
          'param_name' => 'background',
          'group' => __('Background', 'victheme_visualplus'),
        ));
      }

    }

  }


  /**
   * Check if params with certain name exists
   * @return boolean
   */
  public function checkParamsExists($params, $key) {
    foreach ($params as $delta => $param) {
      if (isset($param['param_name']) && $param['param_name'] == $key) {
        return true;
      }
    }

    return false;
  }

  /**
   * Wrapper for connecting VC into VTCore
   */
  public function fontEditorForm($settings, $value) {
    $settings['value'] = $value;
    $element = new VTCore_VisualPlus_Form_Fonts($settings);
    return $element->__toString();
  }


  /**
   * Wrapper for connecting VC into VTCore
   */
  public function backgroundEditorForm($settings, $value) {
    $settings['value'] = $value;
    $element = new VTCore_VisualPlus_Form_Background($settings);
    return $element->__toString();
  }

}