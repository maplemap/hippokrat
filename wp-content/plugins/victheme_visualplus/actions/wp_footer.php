<?php
/**
 * Class for hooking to wp_footer
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_VisualPlus_Actions_Wp__Footer
extends VTCore_Wordpress_Models_Hook {

  protected $weight = 99;

  public function hook() {

    if (is_singular()) {

      $id = get_the_ID();
      $css = get_post_meta($id, '_visualplus_custom_css', true);
      if (!empty($css)) {
        $css = str_replace('+', ' ', $css);
        $style = new VTCore_Html_Style();
        $style->addText($css)->render();
      }

    }
  }
}