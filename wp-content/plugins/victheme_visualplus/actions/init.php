<?php
/**
 *
 * Hooking into Wordpress init action.
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_VisualPlus_Actions_Init
extends VTCore_Wordpress_Models_Hook {

  protected $weight = 999;

  public function hook() {

    // Front end only
    if (vc_is_frontend_editor() || vc_is_page_editable()) {
      VTCore_Wordpress_Utility::loadAsset('bootstrap');
      VTCore_Wordpress_Utility::loadAsset('jquery-visualplus-video');
      VTCore_Wordpress_Utility::loadAsset('jquery-parallax');
      VTCore_Wordpress_Utility::loadAsset('visualplus-admin');
      VTCore_Wordpress_Utility::loadAsset('visualplus-front');
    }

    // Backend only
    if (vc_mode() == 'admin_page' && VTCore_Wordpress_Utility::checkCurrentPage(array('post.php', 'post-new.php'))) {
      VTCore_Wordpress_Utility::loadAsset('wp-bootstrap');
    }

    // Inject all shortcodes with fonts form.
    // @todo some of the shortcode will failed to display the fonts
    //       properly due to the actual injection depends on css class
    //       and the shortcode needs to invoke vc_shortcode_css_class when building
    //       the target element css class name.
    foreach (WPBMap::getShortCodes() as $id => $shortcode) {

      // Bug fix 4.5.3 vc some shortcode doesn't have params array
      if (!isset($shortcode['params'])) {
        continue;
      }

      // Only inject the font editor if shortcode has no
      // shortcode attribute called font to avoid
      // visual plus overriding default shortcode attr by mistake.
      if (!$this->checkParamsExists($shortcode['params'], 'font')) {
        vc_add_param($id,  array(
          'type' => 'font_editor',
          'param_name' => 'font',
          'group' => __('Fonts', 'victheme_visualplus'),
        ));
      }

      // Only inject the background editor if shortcode has no
      // shortcode attribute called background to avoid
      // visual plus overriding default shortcode attr by mistake.
      if (!$this->checkParamsExists($shortcode['params'], 'background')) {
        vc_add_param($id,  array(
          'type' => 'background_editor',
          'param_name' => 'background',
          'group' => __('Background', 'victheme_visualplus'),
        ));
      }

    }

    // Add Full screen mode for vc_row
    vc_add_param('vc_row', array(
      'type' => 'dropdown',
      'param_name' => 'fullscreen',
      'heading' => __('Enable Full Screen', 'victheme_visualplus'),
      'description' => __('When enabled vc row will take the width and height of the browser viewport', 'victheme_visualplus'),
      'value' => array(
        __('Disable', 'victheme_visualplus') => 'none',
        __('Enable', 'victheme_visualplus') => 'fullscreen-enabled',
      ),
    ));

    vc_add_param('vc_row', array(
      'type' => 'dropdown',
      'param_name' => 'verticalcenter',
      'heading' => __('Vertical Center', 'victheme_visualplus'),
      'description' => __('When enabled vc column will be vertically centered using javascript, please only use one column for this to work properly', 'victheme_visualplus'),
      'value' => array(
        __('Disable', 'victheme_visualplus') => 'none',
        __('Enable', 'victheme_visualplus') => 'verticalcenter-enabled',
      ),
      'dependency' => array(
        'element' => 'fullscreen',
        'value' => array(
          'fullscreen-enabled'
        ),
      ),
    ));

    /**
    vc_add_param('vc_row', array(
      'type' => 'dropdown',
      'param_name' => 'nicescroll',
      'heading' => __('Enable Nicescroll', 'victheme_visualplus'),
      'description' => __('When enabled vc row will show nicescroll bar when its content is overflowing', 'victheme_visualplus'),
      'value' => array(
        __('Disable', 'victheme_visualplus') => 'none',
        __('Enable', 'victheme_visualplus') => 'nicescroll-enabled',
      ),
      'dependency' => array(
        'element' => 'fullscreen',
        'value' => array(
          'fullscreen-enabled'
        ),
      ),
    ));
    **/

    vc_add_param('vc_row', array(
      'type' => 'dropdown',
      'param_name' => 'deckmode',
      'heading' => __('Enable Deck Mode', 'victheme_visualplus'),
      'description' => __('When enabled vc row be treated as scrollable deck element', 'victheme_visualplus'),
      'value' => array(
        __('Disable', 'victheme_visualplus') => 'none',
        __('Enable', 'victheme_visualplus') => 'deckmode-enabled',
      ),
      'dependency' => array(
        'element' => 'fullscreen',
        'value' => array(
          'fullscreen-enabled'
        ),
      ),
    ));

  }


  /**
   * Check if params with certain name exists
   * @return boolean
   */
  public function checkParamsExists($params, $key) {
    foreach ($params as $delta => $param) {
      if (isset($param['param_name']) && $param['param_name'] == $key) {
        return true;
      }
    }

    return false;
  }
}