<?php
/**
 *
 * Hooking into Wordpress edit_post action
 * to add custom saving function for custom css
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_VisualPlus_Actions_Edit__Post
extends VTCore_Wordpress_Models_Hook {

  public function hook($post_id = NULL) {
    $this->buildShortcodesCustomCss($post_id);
  }

  private function buildShortcodesCustomCss( $post_id ) {
    $post = get_post( $post_id );
    // $this->createShortCodes();
    $css = $this->parseShortcodesCustomCss( $post->post_content );

    if ( empty( $css ) ) {
      delete_post_meta( $post_id, '_visualplus_custom_css' );
    } else {
      update_post_meta( $post_id, '_visualplus_custom_css', $css );
    }
  }

  private function parseShortcodesCustomCss( $content ) {
    $css = '';
    if ( ! preg_match( '/\s*(\.[^\{]+)\s*\{\s*([^\}]+)\s*\}\s*/', $content ) ) return $css;
    preg_match_all( '/' . get_shortcode_regex() . '/', $content, $shortcodes );
    foreach ( $shortcodes[2] as $index => $tag ) {
      $shortcode = WPBMap::getShortCode( $tag );
      $attr_array = shortcode_parse_atts( trim( $shortcodes[3][$index] ) );

      if (!isset($shortcode['params']) || !is_array($shortcode['params'])) {
        continue;
      }

      foreach ( $shortcode['params'] as $param ) {
        if ( $param['type'] == 'font_editor' && isset( $attr_array[$param['param_name']] ) ) {
          $css .= $attr_array[$param['param_name']];
        }

        if ( $param['type'] == 'background_editor' && isset( $attr_array[$param['param_name']] ) ) {
          $css .= $attr_array[$param['param_name']];
        }
      }
    }
    foreach ( $shortcodes[5] as $shortcode_content ) {
      $css .= $this->parseShortcodesCustomCss( $shortcode_content );
    }
    return $css;
  }
}