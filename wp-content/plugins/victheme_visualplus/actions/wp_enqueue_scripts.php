<?php

/**
 * Hooking into wordpress enqueue script
 * @author jason.xie@victheme.com
 *
 */
class VTCore_VisualPlus_Actions_Wp__Enqueue__Scripts
  extends VTCore_Wordpress_Models_Hook {

  protected $post_id = FALSE;

  public function hook() {

    VTCore_Wordpress_Utility::loadAsset('visualplus-front');

    if (!$this->post_id) {
      $this->setID();
    }

    $css = get_post_meta($this->post_id, '_visualplus_custom_css', TRUE);
    $params = array();
    $maskingTarget = array();

    if (!empty($css)) {
      $matches = $this->parseCss($css);

      foreach ($matches as $key => $match) {
        if (isset($match['font-family']) && !empty($match['font-family'])) {
          foreach ($match as $key => $data) {

            if ($key == 'font-family') {
              $data = str_replace(' ', '+', $data);
              if (VTCore_Wordpress_Init::getFactory('fonts')
                ->get('library.' . $data)
              ) {
                VTCore_Wordpress_Init::getFactory('fonts')
                  ->add('registered.fonts.' . $data, $data);
              }
            }

            if ($key == 'font-weight') {
              VTCore_Wordpress_Init::getFactory('fonts')
                ->add('registered.weight.' . $data, $data);
            }

            if ($key == 'font-style') {
              VTCore_Wordpress_Init::getFactory('fonts')
                ->add('registered.styles.' . $data, $data);
            }
          }

          // Refresh google fonts url
          VTCore_Wordpress_Init::getFactory('fonts')->loadFont(TRUE);
        }

        if (isset($match['background-masking'])
            && !empty($match['background-masking'])
            && $match['background-masking'] != 'none') {

          $style = $key . ' .masking {background-color:' . $match['background-masking'] . ' !important; }';
          VTCore_Wordpress_Init::getFactory('assets')
            ->get('library')
            ->add('visualplus-front.css.visualplus-front-css.inline.masking', $style);

          $maskingTarget[] = $key;

        }

        if (isset($match['background-parallax'])
          && !empty($match['background-parallax'])
          && $match['background-parallax'] != 'none'
        ) {

          VTCore_Wordpress_Utility::loadAsset('jquery-parallax');

        }

        if (isset($match['video-source-mp4'])
          || isset($match['video-source-ogv'])
          || isset($match['video-source-webm'])
        ) {

          $param = array();

          if (isset($match['video-source-mp4'])) {
            $param['mp4'] = $match['video-source-mp4'];
          }

          if (isset($match['video-source-ogv'])) {
            $param['ogv'] = $match['video-source-ogv'];
          }

          if (isset($match['video-source-webm'])) {
            $param['webm'] = $match['video-source-webm'];
          }

          if (isset($match['video-poster'])) {
            $param['poster'] = $match['video-poster'];
          }

          if (isset($match['video-position'])) {
            $param['position'] = $match['video-position'];
          }

          if (isset($match['video-autoplay'])) {
            $param['autoplay'] = $match['video-autoplay'];
          }

          if (isset($match['video-loop'])) {
            $param['loop'] = $match['video-loop'];
          }

          if (isset($match['video-scale'])) {
            $param['scale'] = $match['video-scale'];
          }

          if (isset($match['video-width'])) {
            $param['width'] = $match['video-width'];
          }

          if (isset($match['video-height'])) {
            $param['height'] = $match['video-height'];
          }

          if (!empty($param)) {
            $param['selector'] = trim($key);
            $params[] = $param;

          }
        }
      }

      if (!empty($params)) {
        VTCore_Wordpress_Utility::loadAsset('jquery-visualplus-video');
        wp_localize_script('jquery', 'visualplusVideo', $params);
      }

      if (!empty($maskingTarget)) {
        wp_localize_script('jquery', 'visualplusMasking', $maskingTarget);
      }
    }
  }


  /**
   * Allow user to hijack post id to load the metadata from
   * @param $id
   */
  public function setID($id = FALSE) {
    if ($id) {
      $this->post_id = $id;
    }
    else {
      $this->post_id = get_the_ID();
    }

    return $this;
  }

  /**
   * Parse the string to get the font details
   */
  private function parseCss($cssString) {

    //Parsing the class or id name + the attributes
    $regex = "/([#\.][a-z0-9]*?\.?.*?)\s?\{([^\{\}]*)\}/m";
    preg_match_all($regex, $cssString, $classes, PREG_PATTERN_ORDER);

    if (sizeof($classes[1]) > 0) {

      foreach ($classes[2] as $index => $value) {
        //Parsing the attributes string
        $regex = "/([^\;]*);/m";
        preg_match_all($regex, $value, $returned_attributes, PREG_PATTERN_ORDER);

        $selector = $index;
        if (isset($classes[1][$index])) {
          $selector = $classes[1][$index];
        }

        if (is_array($returned_attributes[1])) {
          foreach ($returned_attributes[1] as $attributes) {
            if (strpos($attributes, ':') === FALSE) {
              continue;
            }
            $data = explode(':', $attributes);
            $key = array_shift($data);
            $att = implode(':', $data);
            $parsedCss[$selector][trim($key)] = trim(str_replace('!important', '', $att));
          }
        }
      }
    }

    return isset($parsedCss) ? $parsedCss : array();
  }

}