<?php
/**
 * Utility class
 *
 * Helper class for themer to use.
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_VisualPlus_Utility
extends VTCore_Utility {

  static function extractVisualCSSClass(&$class, $remove = false) {

    $output = array();

    $classes = explode(' ', $class);
    array_filter($classes);

    foreach ($classes as $key => $value) {
      if (strpos($value, 'visualplus') !== false
          || strpos($value, 'parallax-') !== false) {
        $output[] = $value;

        if ($remove) {
          unset($classes[$key]);
        }
      }
    }

    // Update the original class
    if ($remove) {
      $class = implode (' ', $classes);
    }

    return implode(' ', $output);
  }

}