Welcome to VicTheme Visual Plus.
================================

This plugin is created for adding extra missing options to
the default visual composer element.



Requirements
============

This plugin has 2 dependencies :
1. VicTheme Core Plugin
2. Visual Composer Plugin

Before the dependencies are installed and enabled, this plugin won't activate
itself to prevent site crash.


INSTALLATION
============

This plugin has no special installation instruction, just download the plugin
and its dependencies and upload them to wp-content/plugin directory then
activate this plugin and its dependencies via WordPress plugin manager page.
 

SUPPORT
=======

You can contact support@victheme.com to request support for this plugin.