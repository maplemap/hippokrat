<?php
/**
 * Base class for all the visualplus forms
 * The purpose of this base class is to inject
 * methods that is reusable in all the forms.
 *
 * The designs of the forms is influenced by
 * Visual composer unique configuration forms
 * Do not try to invoke the forms manually
 * outside the VC scope.
 *
 * @author jason.xie@victheme.com
 * @todo examine why using vtcore html object
 *       for printing style and script wont work?
 *
 */
class VTCore_VisualPlus_Form_Base
extends VTCore_Bootstrap_Element_BsElement {

  protected $assets = array(
    'bootstrap-colorpicker',
    'bootstrap-switch',
    'jquery-table-manager',
    'wp-media',
    'wp-gradientpicker',
    'wp-backgroundpicker',
    'wp-videopicker',
    'wp-fonts',
    'visualplus-admin',
    'visualplus-background',
    'visualplus-fonts',
  );

  private $cache;


  public function buildElement() {
    parent::buildElement();
    $this->loadAssets();
  }


  /**
   * Method for loading and compressing assets
   */
  protected function loadAssets() {

    // Do not cache on development mode
    if (!defined('WP_DEBUG') || (defined('WP_DEBUG') && WP_DEBUG != true)) {
      $this->cache = get_transient('vtcore_visualplus_merged_assets');
    }

    if (empty($this->cache)) {

      $this->cache = array(
        'css' =>  '',
        'js' => '',
      );

      // Make a reference to the global asset object
      foreach ($this->assets as $asset) {

        if (VTCore_Wordpress_Init::getFactory('assets')->get('library')->get($asset . '.css')) {
          $content = '';
          foreach (VTCore_Wordpress_Init::getFactory('assets')->get('library')->get($asset . '.css') as $file) {
            $this->base = trailingslashit(dirname($file['url']));
            $content .= @file_get_contents($file['path']);
          }

          $this->cache['css'] .= preg_replace_callback('/url\(\s*[\'"]?(?![a-z]+:|\/+)([^\'")]+)[\'"]?\s*\)/i', array($this, 'fixCssPath'), $content);
        }


        if (VTCore_Wordpress_Init::getFactory('assets')->get('library')->get($asset . '.js')) {
          foreach (VTCore_Wordpress_Init::getFactory('assets')->get('library')->get($asset . '.js') as $file) {
            $this->cache['js'] .= @file_get_contents($file['path']);
            if (substr($this->cache['js'], -1) != ';') {
              $this->cache['js'] .= ";\n";
            }
          }
        }
      }

      if (isset($this->cache['css']) || isset($this->cache['js'])) {
        set_transient('vtcore_visualplus_merged_assets', $this->cache, 1 * HOUR_IN_SECONDS);
      }
    }

    // @todo double check why using vtcore object wont work here?
    if (!empty($this->cache['css'])) {
      echo '<style id="visualplus-merged-css" type="text/css">' . preg_replace('/^@charset\s+[\'"](\S*?)\b[\'"];/i', '', $this->cache['css']) . '</style>';
    }

    // @todo double check why using vtcore object wont work here?
    if (!empty($this->cache['js'])) {
      echo '<script id="visualplus-merged-js" type="text/javascript">' . $this->cache['js'] . '</script>';
    }

  }

  /**
   * Method ripped from drupal for fixing css relative path
   */
  public function fixCssPath($matches, $base = NULL) {

    // Store base path for preg_replace_callback.
    if (isset($base)) {
      $this->base = $base;
    }

    // Prefix with base and remove '../' segments where possible.
    $path = $this->base . $matches[1];
    $last = '';
    while ($path != $last) {
      $last = $path;
      $path = preg_replace('`(^|/)(?!\.\./)([^/]+)/\.\./`', '$1', $path);
    }
    return 'url(' . $path . ')';
  }

}