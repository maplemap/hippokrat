<?php
/**
 * Class for building the complete set
 * for the background form.
 *
 * This cannot be used in normal conditions
 * outside the visual composer as the
 * actual form values is injected via javascript
 * and the form element name is set staticly.
 *
 *
 * @todo When VTCore has true background set, merge this
 *       and use that instead.
 *
 * @see VTCore_VisualPlus_Form_Fonts for assets loading
 * @author jason.xie@victheme.com
 *
 */
class VTCore_VisualPlus_Form_Background
extends VTCore_VisualPlus_Form_Base {

  protected $context = array(
    'type' => 'div',
    'attributes' => array(
      'class' => array(
        'row',
      ),
    ),
    'data' => array(
      'background-editor' => true,
    ),
  );

  public function buildElement() {

    VTCore_Wordpress_Utility::loadAsset('visualplus-admin-style');

    $this
      ->setType('div')
      ->Hidden(array(
        'attributes' => array(
          'value' => $this->getContext('value'),
          'name' => $this->getContext('param_name'),
          'data-background-value' => 'true',
          'class' => array(
            'wpb_vc_param_value',
            'wpb-input',
            $this->getContext('param_name')
          )
        ),
      ))
      ->BsRow(array(
        'type' => 'div',
        'attributes' => array(
          'class' => array(
            'wp-toggle-row'
          )
        )
      ))
      ->lastChild()
      ->BsElement(array(
        'type' => 'h4',
        'text' => __('Select Background Mode', 'victheme_visualplus'),
        'grids' => array(
          'columns' => array(
            'mobile' => 6,
            'small' => 6,
            'medium' => 6,
            'large' => 6,
          ),
        ),
      ))
      ->BsButtonGroup(array(
        'buttons_element' => array(
          'size' => 'md',
          'mode' => 'primary',
          'data' => array(
            'toggle' => 'show',
          ),
        ),
        'grids' => array(
          'columns' => array(
            'mobile' => 6,
            'small' => 6,
            'medium' => 6,
            'large' => 6,
          ),
        ),
      ))
      ->lastChild()
      ->addButton(array(
        'text' => __('Image', 'victheme_visualplus'),
        'data' => array(
          'show' => 'image',
        ),
      ))
      ->addButton(array(
        'text' => __('Gradient', 'victheme_visualplus'),
        'data' => array(
          'show' => 'gradient',
        ),
      ))
      ->addButton(array(
        'text' => __('Video', 'victheme_visualplus'),
        'data' => array(
          'show' => 'video',
        ),
      ));


    // Building Image background form
    $this
      ->BsElement(array(
        'type' => 'div',
        'data' => array(
          'target' => 'image'
        ),
      ))
      ->lastChild()
      ->addOverloaderPrefix('VTCore_Wordpress_Form_')
      ->WpBackground(array(
        'name' => 'background-image',
        'data' => array(
          'name' => 'background-gradient',
          'attribute' => 'background',
        ),
      ));

    // Building Video Background Form
    $this
      ->BsElement(array(
        'type' => 'div',
        'data' => array(
          'target' => 'video'
        ),
      ))
      ->lastChild()
      ->addOverloaderPrefix('VTCore_Wordpress_Form_')
      ->WpVideo(array(
        'name' => 'background-video',
        'data' => array(
          'name' => 'background-video',
          'attribute' => 'background',
        ),
      ));


    // Building Gradient Background Form
    $this
      ->BsElement(array(
        'type' => 'div',
        'data' => array(
          'target' => 'gradient'
        ),
      ))
      ->lastChild()
      ->addOverloaderPrefix('VTCore_Wordpress_Form_')
      ->WpGradient(array(
        'name' => 'background-gradient',
        'data' => array(
          'name' => 'background-gradient',
          'attribute' => 'background',
        ),
      ));

  }
}