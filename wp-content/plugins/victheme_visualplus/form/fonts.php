<?php
/**
 * Class for building the complete set
 * for the fonts form.
 *
 * This cannot be used in normal conditions
 * outside the visual composer as the
 * actual form values is injected via javascript
 * and the form element name is set staticly.
 *
 * @todo When we are ready to build layout manager
 *       use this as the base for true form fonts
 *       set and create a VTCore compatible class.
 *
 * @todo When VTCore has true fonts set, merge this
 *       and use that instead.
 *
 * @todo Add more relevant and popular google fonts
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_VisualPlus_Form_Fonts
extends VTCore_VisualPlus_Form_Base {

  protected $context = array(
    'type' => 'div',
    'data' => array(
      'font-editor' => true,
    ),
  );

  public function buildElement() {

    $this
      ->setType('div')
      ->Hidden(array(
        'attributes' => array(
          'value' => $this->getContext('value'),
          'name' => $this->getContext('param_name'),
          'data-font-value' => 'true',
          'class' => array(
            'wpb_vc_param_value',
            'wpb-input',
            $this->getContext('param_name')
          )
        ),
      ))
      ->addOverloaderPrefix('VTCore_Wordpress_Form_')
      ->WpFonts(array(
        'name' => 'fonts',
      ));

    parent::buildElement();
  }
}