<?php
/*
Plugin Name: VicTheme VisualPlus
Plugin URI: http://victheme.com/victheme-visual-plus
Description: Plugin for extending visual composer with missing components such as font selector.
Author: jason.xie@victheme.com
Version: 1.4.1
Author URI: http://victheme.com
*/

define('VTCORE_VISUALPLUS_CORE_VERSION', '1.7.0');

add_action('plugins_loaded', 'VTCore_VisualPlus_bootPlugin', 11);


/**
 * Plugin booting method
 */
function VTCore_VisualPlus_bootPlugin() {

  if (!(defined('VTCORE_VERSION') && version_compare(VTCORE_VERSION, VTCORE_VISUALPLUS_CORE_VERSION, '='))) {

    add_action('admin_notices', 'VTCore_VisualPlus_MissingCoreNotice');

    function VTCore_VisualPlus_MissingCoreNotice() {

      if (!defined('VTCORE_VERSION')) {
        $notice = __('VicTheme Visual Plus depends on VicTheme Core Plugin which is not activated or missing, Please enable it first before VicTheme Visual Plus can work properly.');
      }
      elseif (version_compare(VTCORE_VERSION, VTCORE_VISUALPLUS_CORE_VERSION, '!=')) {
        $notice = sprintf(__('VicTheme Visual Plus depends on VicTheme Core Plugin API version %s to operate properly.', 'victheme_visualplus'), VTCORE_VISUALPLUS_CORE_VERSION);
      }

      if (isset($notice)) {
        echo '<div class="error""><p>' . $notice . '</p></div>';
      }

    }

    return;
  }


  if (!defined('WPB_VC_VERSION')) {

    add_action('admin_notices', 'VTCore_VisualPlus_MissingVisualComposerNotice');

    function VTCore_VisualPlus_MissingVisualComposerNotice() {
      echo

      '<div class="error""><p>' .

      __( 'VisualPlus requires Visual Composer Plugin enabled before it can function properly.', 'victheme_visualplus') .

      '</p></div>';
    }

    return;
  }

  if (defined('WPB_VC_VERSION') && !version_compare(WPB_VC_VERSION, '4.7.0', '>=')) {
    add_action('admin_notices', 'VTCore_VisualPlusVCTooLow');

    function VTCore_VisualPlusVCTooLow() {
      echo
        '<div class="error""><p>' .

        __( 'VisualPlus requires Visual Composer Plugin version 4.7.0 and above before it can function properly.', 'victheme_visualplus') .

        '</p></div>';
    }
  }


  // Continue booting the plugin
  define('VTCORE_VISUALPLUS_BOOTSTRAP', true);
  define('VTCORE_VISUALPLUS_LOADED', true);
  define('VTCORE_VISUALPLUS_URL', plugin_dir_url(__FILE__));

  define('VTCORE_VISUALPLUS_VERSION', '1.4.1');

  // Booting Core Class
  require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'init.php');
  $init = new VTCore_VisualPlus_Init();


}