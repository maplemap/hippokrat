<?php

$output = $el_class = $bg_image = $bg_color = $bg_image_repeat = $font_color = $padding = $margin_bottom = $css = '';
extract(shortcode_atts(array(
  'el_class'        => '',
  'bg_image'        => '',
  'bg_color'        => '',
  'bg_image_repeat' => '',
  'font_color'      => '',
  'padding'         => '',
  'margin_bottom'   => '',
  'css'             => '',
  'styling'         => '',
  'schema'          => '',
  'area_class'      => '',
  'full_width' => false,
), $atts, 'vc_row'));

wp_enqueue_script( 'wpb_composer_front_js' );

$templateMode = 'inner';
if ($this->settings['base'] == 'vc_row' && defined('VTCORE_WORDPRESS_CUSTOM_TEMPLATE_FILE') && VTCORE_WORDPRESS_CUSTOM_TEMPLATE_FILE == 'page-visualcomposer.php') {
  $templateMode = 'area';
}

$el_class = $this->getExtraClass($el_class);

$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'vc_row wpb_row '. ( $this->settings('base')==='vc_row_inner' ? 'vc_inner ' : '' ) . get_row_css_class() . $el_class , $this->settings['base'], $atts );

$visual_plus = '';
if (class_exists('VTCore_VisualPlus_Utility', true)) {
  $visual_plus = VTCore_VisualPlus_Utility::extractVisualCSSClass($css_class, true);
}

$style = $this->buildStyle($bg_image, $bg_color, $bg_image_repeat, $font_color, $padding, $margin_bottom);

if ($templateMode == 'area') {

  $output .= '<div class="area vc_area clearfix '. vc_shortcode_custom_css_class($css, ' ') . ' ' . $visual_plus . ' ' . $area_class . ' ' . $styling . ' ' . $schema . '"' . $style . '><div class="masking"><div class="container-fluid">';
  $output .= '<div class="row ' . $css_class . '">';

}
else {

  $output .= '<div class="clearfix '. $css_class . vc_shortcode_custom_css_class( $css, ' ' ) . '"' . $style . '>';

}

if (!empty($full_width)) {

  $output .= '<div data-vc-full-width="true"';

  if ($full_width == 'stretch_row_content' || $full_width == 'stretch_row_content_no_spaces' ) {
    $output .= ' data-vc-stretch-content="true"';
  }

  if ($full_width == 'stretch_row_content_no_spaces') {
    $output .= ' class="vc_row-no-padding"';
  }

  $output .= '>';
}

$output .= wpb_js_remove_wpautop($content);

if (!empty($full_width)) {
  $output .= '</div><div class="vc_row-full-width"></div>';
}


if ($templateMode == 'area') {

  $output .= '</div></div></div></div>';

}
else {

  $output .= '</div>';

}

$output .= $this->endBlockComment('row');
echo $output;
