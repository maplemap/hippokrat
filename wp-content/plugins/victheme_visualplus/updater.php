<?php
/**
 * Updating VisualPlus
 *
 * @see VTCore_Wordpress_Factory_Updater
 * Class VTCore_Wordpress_Models_Updater
 */
class VTCore_VisualPlus_Updater
extends VTCore_Wordpress_Models_Updater {

  protected function update_1_3_2() {

    global $wpdb;

    // Convert Obsolete fullwidth shortcode atts into
    // new VisualComposer stretch mode
    $result = $wpdb->query(
      $wpdb->prepare(
        "UPDATE {$wpdb->posts}
         SET post_content = REPLACE(post_content, %s, %s)"
      , array('fullwidth="1"', 'full_width="stretch_row_content_no_spaces"'))
    );

    return true;
  }
}