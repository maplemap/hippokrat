<?php
/**
 * Main Class for handling the Plugin operation
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_VisualPlus_Init {

  private $autoloader;

  private $filters = array(
    'vc_shortcodes_css_class',
    'vtcore_wordpress_shortcode_attributes_alter',
    'shortcode_atts_vc_separator',
    'shortcode_atts_vc_row',
    'get_post_metadata',
  );

  private $actions = array(
    'vc_after_mapping',
    'init',
    'wp_enqueue_scripts',
    'edit_post',
    'wp_footer',
    'vtcore_updater',
  );


  /**
   * Constructing the main class and adding action to init.
   */
  public function __construct() {

    // Load autoloader
    $this->autoloader = new VTCore_Autoloader('VTCore_VisualPlus', dirname(__FILE__));
    $this->autoloader->setRemovePath('vtcore' . DIRECTORY_SEPARATOR . 'visualplus' . DIRECTORY_SEPARATOR);
    $this->autoloader->register();

    // Autoload translation for vtcore
    load_plugin_textdomain('victheme_visualplus', false, 'victheme_visualplus/languages');

    // Registering assets
    VTCore_Wordpress_Init::getFactory('assets')
      ->get('library')
      ->detect(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets', plugins_url('', __FILE__) . '/assets');

    // Registering actions
    VTCore_Wordpress_Init::getFactory('filters')
      ->addPrefix('VTCore_VisualPlus_Filters_')
      ->addHooks($this->filters)
      ->register();


    // Registering filters
    VTCore_Wordpress_Init::getFactory('actions')
      ->addPrefix('VTCore_VisualPlus_Actions_')
      ->addHooks($this->actions)
      ->register();

  }

}