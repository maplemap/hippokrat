<?php
/**
 * Hooking into wordpress get_post_meta to hijack visual plus extra css
 * that is binded to the vc grid item object
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_VisualPlus_Filters_Get__Post__Metadata
extends VTCore_Wordpress_Models_Hook {

  protected $argument = 4;

  public function hook($empty = NULL, $object_id = NULL, $meta_key = NULL, $single = NULL) {

    if ($meta_key == '_wpb_shortcodes_custom_css') {
      $parser = new VTCore_VisualPlus_Actions_Wp__Enqueue__Scripts();
      $parser->setID($object_id)->hook();

      $css = get_post_meta($object_id, '_visualplus_custom_css', true);
      if (!empty($css)) {
        VTCore_Wordpress_Init::getFactory('assets')
          ->get('library')
          ->add('visualplus-front.css.visualplus-front-css.inline.custom', str_replace('+', ' ', $css));
      }

      VTCore_Wordpress_Init::getFactory('assets')->process();
      VTCore_Wordpress_Init::getFactory('fonts')->process();
    }
  }

}