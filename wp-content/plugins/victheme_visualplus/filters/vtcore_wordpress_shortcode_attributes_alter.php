<?php
/**
 * Hooking into vtcore_wordpress_shortcode_attributes_alter filter
 * to process custom shortcode attributes rules that is not
 * implemented in VTCore but implemented in VisualComposer
 *
 * This is done this way to keep the core clean from VisualComposer
 * specific codes.
 *
 * @author jason.xie@victheme.com
 */
class VTCore_VisualPlus_Filters_VTCore__Wordpress__Shortcode__Attributes__Alter
extends VTCore_Wordpress_Models_Hook {

  public function hook($atts = NULL) {

    // Preprocessing VC custom css from the design tabs
    if (isset($atts['css']) && !empty($atts['css'])) {
      $atts['attributes']['class']['vccss'] = vc_shortcode_custom_css_class($atts['css'], ' ');
    }

    // Preprocessing VC custom css from the design tabs
    if (isset($atts['font']) && !empty($atts['font'])) {
      $atts['attributes']['class']['font_style'] = vc_shortcode_custom_css_class($atts['font'], ' ');
    }

    // Preprocessing VC custom css from the design tabs
    if (isset($atts['background']) && !empty($atts['background'])) {
      $atts['attributes']['class']['background_style'] = @array_shift(explode('{', str_replace('.', ' ', str_replace('<br />', '', $atts['background']))));
    }

    return $atts;
  }
}