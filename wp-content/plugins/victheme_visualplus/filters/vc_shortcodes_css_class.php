<?php
/**
 * Hooking into vc_shortcodes_css_class
 * to process font shortcodes and apply the css class to the
 * corresponding elements.
 *
 * @author jason.xie@victheme.com
 */
class VTCore_VisualPlus_Filters_VC__Shortcodes__Css__Class
extends VTCore_Wordpress_Models_Hook {

  protected $argument = 3;

  public function hook($css = NULL, $settings = NULL, $atts = NULL) {

    // Process the fonts custom css
    if (isset($atts['font'])) {
      $css .= ' ' . vc_shortcode_custom_css_class($atts['font']);
    }

    if (strpos($css, 'equalheightRow') !== false) {
      VTCore_Wordpress_Utility::loadAsset('jquery-equalheight');
    }

    if (strpos($css, 'fittext') !== false) {
      VTCore_Wordpress_Utility::loadAsset('jquery-fittext');
    }

    if (strpos($css, 'text-tailor') !== false) {
      VTCore_Wordpress_Utility::loadAsset('jquery-texttailor');
    }

    if (strpos($css, 'fit-container') !== false) {
      VTCore_Wordpress_Utility::loadAsset('jquery-fitcontainer');
    }

    // Process the background custom css
    if (isset($atts['background'])) {
      $css .= @array_shift(explode('{', str_replace('.', ' ', str_replace('<br />', '', $atts['background']))));
    }

    if (in_array($settings, array('vc_button', 'vc_button2', 'vc_cta_button', 'vc_cta_button2')) && isset($atts['css'])) {
      $css .= ' ' . vc_shortcode_custom_css_class($atts['css']);
    }

    return $css;
  }

}