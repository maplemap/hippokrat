<?php
/**
 * Altering the default Visual Composer row
 *
 * This is needed to inject the extra area class and autoloading
 * the required javascript to perform additional styling.
 *
 * This wont fire unless vc_row.php template is modified and
 * added 'vc_row' string to the last parameter of the shortcode_atts
 * function to invoke the hook shortcode_atts_vc_row.
 *
 * @see visualplus/templates/vc_row.php
 * @see http://codex.wordpress.org/Function_Reference/shortcode_atts
 * @author jason.xie@victheme.com
 */
class VTCore_VisualPlus_Filters_Shortcode__Atts__Vc__Row
extends VTCore_Wordpress_Models_Hook {

  protected $argument = 3;

  public function hook($out = NULL, $pair = NULL, $atts = NULL) {

    if (!isset($out['area_class'])) {
      $out['area_class'] = '';
    }

    if (isset($atts['deckmode']) && $atts['deckmode'] == 'deckmode-enabled') {
      VTCore_Wordpress_Utility::loadAsset('jquery-snapscroll');
      $out['area_class'] .= ' js-deckmode';
    }

    if (isset($atts['fullscreen']) && $atts['fullscreen'] == 'fullscreen-enabled') {
      $out['area_class'] .= ' js-fullscreen';
    }

    /**
    if (isset($atts['nicescroll']) && $atts['nicescroll'] == 'nicescroll-enabled') {
      VTCore_Wordpress_Utility::loadAsset('jquery-nicescroll');
      $out['area_class'] .= ' js-nicescroll';
    }
    **/

    if (isset($atts['verticalcenter']) && $atts['verticalcenter'] == 'verticalcenter-enabled') {
      $out['area_class'] .= ' js-verticalcenter';
    }

    return $out;
  }

}