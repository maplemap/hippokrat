<?php
/**
 * Altering the default Visual Composer separators
 *
 * This will only work if the theme implements
 * additional paramater to the vc_separator.php shortcode_atts
 * as needed
 *
 * @see visualplus/templates/vc_separator.php
 * @see http://codex.wordpress.org/Function_Reference/shortcode_atts
 * @author jason.xie@victheme.com
 */
class VTCore_VisualPlus_Filters_Shortcode__Atts__Vc__Separator
extends VTCore_Wordpress_Models_Hook {

  protected $argument = 3;

  public function hook($out = NULL, $pair = NULL, $atts = NULL) {

    $style = array();
    $out['inline_style'] = '';

    if (isset($atts['maxwidth']) && !empty($atts['maxwidth'])) {
      $style['maxwidth'] = ' max-width:' . $atts['maxwidth'];

      if (strpos($atts['maxwidth'], 'px') === false) {
        $style['maxwidth'] .= 'px';
      }

      unset($atts['maxwidth']);
    }

    if (isset($atts['height']) && !empty($atts['height'])) {
      $style['height'] = 'border-width:' . $atts['height'];

      if (strpos($atts['height'], 'px') === false) {
        $style['height'] .= 'px';
      }

      unset($atts['height']);
    }

    if (!empty($style)) {
      $out['inline_style'] = implode('; ', $style);
    }


    return $out;
  }

}