/**
 * Additional script for the department metabox in the post edit
 * 
 * @author jason.xie@victheme.com
 */
jQuery(document).ready(function($) {
  
  "use strict";
  
  $(document)
  
  // Handling the events on video field source changes.
  .on('change', '[name="department[promotional][video][videolocal]"]', function() {

    $(this).parent().parent().find('.department-video-wrapper').hide();

    if ($(this).val() == 'local') {
      $(this).parent().parent().find('[data-source="local"]').show();
    }
    
    if ($(this).val() == 'outside') {
      $(this).parent().parent().find('[data-source="other"]').show();
    }
    
  })
  .on('ajaxComplete', function() {
    
    var self = $('[name="department[promotional][video][videolocal]"]');
    
    self.parent().parent().find('.department-video-wrapper').hide();

    if (self.val() == 'local') {
      self.parent().parent().find('[data-source="local"]').show();
    }
    
    if (self.val() == 'outside') {
      self.parent().parent().find('[data-source="other"]').show();
    }
  });

  // Invoking the video fields change on first boot.
  $('[name="department[promotional][video][videolocal]"]').trigger('change');
  
});