<?php
/**
 * Class for building the Department Post Type.
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Department_PostType
  extends VTCore_Wordpress_Models_Config {

  protected $database = false;
  protected $filter = 'vtcore_department_post_type_alter';
  protected $loadFunction = false;
  protected $saveFunction = false;
  protected $deleteFunction = false;



  /**
   * Child class must extend this method
   * to register their object information
   *
   * @param array $options
   * @return VTCore_Wordpress_Config_Base
   */
  protected function register(array $options) {
    $this->options = array(
      'labels' => array(
        'name' => _x('Department', 'post type general name', 'victheme_department'),
        'singular_name' => _x('Department', 'post type singular name', 'victheme_department'),
        'add_new' => _x('Add New', 'Department', 'victheme_department'),
        'add_new_item' => __('Add New Department', 'victheme_department'),
        'edit_item' => __('Edit Department', 'victheme_department'),
        'new_item' => __('New Department', 'victheme_department'),
        'view_item' => __('View Department', 'victheme_department'),
        'search_items' => __('Search Departments', 'victheme_department'),
        'not_found' =>  __('No Department found', 'victheme_department'),
        'not_found_in_trash' => __('No department found in Trash', 'victheme_department'),
        'parent_item_colon' => ''
      ),
      'public' => true,
      'publicly_queryable' => true,
      'show_ui' => true,
      'query_var' => true,
      'rewrite' => array( 'slug' => 'department','with_front' => true),
      'capability_type' => 'page',
      'map_meta_cap' => true,
      'hierarchical' => false,
      'has_archive' => true,
      'menu_position' => 5,
      'supports' => array('title', 'editor', 'excerpt', 'thumbnail'),
      'exclude_from_search' => false,
      'register_meta_box_cb' => array($this, 'registerMetabox'),
      'menu_icon' => 'dashicons-groups',
      'delete_with_user' => true,
    );

    $this->merge($options);
    $this->filter();

    return $this;
  }

  public function registerPostType() {
    register_post_type('department', $this->options);
    return $this;
  }

  public function registerMetabox() {
    add_meta_box('department-information', __('Department Information', 'victheme_department'), array(new VTCore_Department_Form_Metabox(), 'render'), 'department','normal', 'high');
  }

}