<?php
/**
 * Main initialization class for properly
 * booting the Department plugin.
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Department_Init {

  private $autoloader;

  private $actions = array(
    'save_post',
    'delete_post',
    'the_post',
    'delete_user',
    'init',
    'vtcore_headline_add_configuration_panel',
    'vtcore_services_form_information_alter',
  );

  private $filters = array(
    'vtcore_headline_alter',
    'vtcore_headline_configuration_context_alter',
    'enter_title_here',
  );


  public function __construct() {

    // Booting autoloader
    $this->autoloader = new VTCore_Autoloader('VTCore_Department', dirname(__FILE__));
    $this->autoloader->setRemovePath('vtcore' . DIRECTORY_SEPARATOR . 'department' . DIRECTORY_SEPARATOR);
    $this->autoloader->register();

    // Autoload translation for vtcore
    load_plugin_textdomain('victheme_department', false, 'victheme_department/languages');

    // Registering assets
    VTCore_Wordpress_Init::getFactory('assets')
      ->get('library')
      ->detect(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets', plugins_url('', __FILE__) . '/assets');


    // Registering actions
    VTCore_Wordpress_Init::getFactory('filters')
      ->addPrefix('VTCore_Department_Filters_')
      ->addHooks($this->filters)
      ->register();


    // Registering filters
    VTCore_Wordpress_Init::getFactory('actions')
      ->addPrefix('VTCore_Department_Actions_')
      ->addHooks($this->actions)
      ->register();

  }

}