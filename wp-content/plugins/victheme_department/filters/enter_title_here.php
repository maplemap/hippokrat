<?php
/**
 * Hooking into enter_title_here to modify
 * the text in the post edit page.
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Department_Filters_Enter__Title__Here
extends VTCore_Wordpress_Models_Hook {

  public function hook($title = NULL) {
    $screen = get_current_screen();
    return ($screen->post_type == 'department') ? __('Enter Department Title', 'victheme_department') : $title;
  }

}