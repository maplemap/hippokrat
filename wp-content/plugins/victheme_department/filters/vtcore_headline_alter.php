<?php
/**
 * Hooking into headline_alter filter
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Department_Filters_VTCore__Headline__Alter
extends VTCore_Wordpress_Models_Hook {

  protected $argument = 2;

  public function hook($instance = NULL, $default = NULL) {

    global $template;

    if ((strpos($template, 'archive-department.php')) !== false) {
      $instance = $default['department'];
    }

    return $instance;
  }
}