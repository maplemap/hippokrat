<?php
/**
 * Class for building the video field element
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Department_Form_Promotional
extends VTCore_Bootstrap_Form_Base {

  protected $context = array(

    'type' => 'div',
    'attributes' => array(
      'class' => array(
        'row',
      ),
    ),
    'name' => 'department',
    'value' => array(
      'title' => false,
      'description' => false,
      'videolocal' => 'local',
      'video' => array(
        'youtube' => '',
        'vimeo' => '',
        'dailymotion' => '',
        'attachment_id' => '',
      ),
    ),

  );


  /**
   * Overriding the parent buildElement method
   */
  public function buildElement() {

    parent::buildElement();

    $this
      ->addChildren(new VTCore_Bootstrap_Element_BsElement(array(
        'type' => 'div',
        'grids' => array(
          'columns' => array(
            'mobile' => '12',
            'tablet' => '4',
            'small' => '4',
            'large' => '4',
          ),
        ),
      )))
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Element_BsElement(array(
        'type' => 'h4',
        'text' => __('Promotional Media', 'victheme_department'),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsDescription(array(
        'text' => __('You can use choose to use localy hosted video and upload the video to the local server or use outside source such as youtube to display the promotional video.', 'victheme_department'),
      )))
      ->getParent()
      ->addChildren(new VTCore_Bootstrap_Element_BsElement(array(
        'type' => 'div',
        'grids' => array(
          'columns' => array(
            'mobile' => '12',
            'tablet' => '8',
            'small' => '8',
            'large' => '8',
          ),
        ),
      )))
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'text' => __('Title', 'victheme_department'),
        'name' => $this->getContext('name') . '[promotional][title]',
        'value' => $this->getContext('value.title'),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsTextarea(array(
        'text' => __('Description', 'victheme_department'),
        'name' => $this->getContext('name') . '[promotional][description]',
        'value' => $this->getContext('value.description'),
        'input_elements' => array(
          'raw' => true,
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsSelect(array(
        'text' => __('Video Source', 'victheme_department'),
        'name' => $this->getContext('name') . '[promotional][video][videolocal]',
        'value' => $this->getContext('value.videolocal'),
        'options' => array(
          'local' => __('Local Source', 'victheme_department'),
          'outside' => __('Other Source', 'victheme_department'),
        )
      )))
      ->addChildren(new VTCore_Bootstrap_Element_BsElement(array(
        'type' => 'div',
        'attributes' => array(
          'class' => array(
            'department-video-wrapper'
          ),
        ),
        'data' => array(
          'source' => 'local',
        ),
      )))
      ->lastChild()
      ->addChildren(new VTCore_Wordpress_Form_WpMedia(array(
        'text' => __('Local Video', 'victheme_department'),
        'name' => $this->getContext('name') . '[promotional][video][attachment_id]',
        'value' => $this->getContext('value.video.attachment_id'),
        'data' => array(
          'type' => 'video',
          'title' => __('Select Video', 'victheme_department'),
          'button' =>__('Select File', 'victheme_department'),
          'mode' => 'id',
          'preview' => true,
          'autoplay' => false,
        ),
        'attributes' => array(
          'class' => array('department-video'),
        ),
        'buttons' => array(
          'add' => __('Select Video', 'victheme_department'),
          'remove' => __('Remove', 'victheme_department'),
        ),
      )))
      ->getParent()
      ->addChildren(new VTCore_Bootstrap_Element_BsElement(array(
        'type' => 'div',
        'attributes' => array(
          'class' => array(
            'department-video-wrapper'
          ),
        ),
        'data' => array(
          'source' => 'other',
        ),
      )))
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'text' => __('Youtube', 'victheme_department'),
        'name' => $this->getContext('name') . '[promotional][video][youtube]',
        'value' => $this->getContext('value.video.youtube'),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'text' => __('Vimeo', 'victheme_department'),
        'name' => $this->getContext('name') . '[promotional][video][vimeo]',
        'value' => $this->getContext('value.video.vimeo'),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'text' => __('Daily Motion', 'victheme_department'),
        'name' => $this->getContext('name') . '[promotional][video][dailymotion]',
        'value' => $this->getContext('value.video.dailymotion'),
      )));

  }

}