<?php
/**
 * Class for building the teasers metabox
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Department_Form_Teasers
extends VTCore_Bootstrap_Form_Base {

  protected $context = array(

    'type' => 'div',
    'attributes' => array(
      'class' => array(
        'row',
      ),
    ),
    'name' => 'department',
    'value' => array(
      'icon' => false,
      'image' => false,
      'title' => false,
      'slogan' => false,
      'description' => false,
      'color' => false,
    ),

  );


  /**
   * Overriding the parent buildElement method
   */
  public function buildElement() {

    parent::buildElement();

    $this
      ->addChildren(new VTCore_Bootstrap_Element_BsElement(array(
        'type' => 'div',
        'grids' => array(
          'columns' => array(
            'mobile' => '12',
            'tablet' => '4',
            'small' => '4',
            'large' => '4',
          ),
        ),
      )))
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Element_BsElement(array(
        'type' => 'h4',
        'text' => __('Display Teasers', 'victheme_department'),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsDescription(array(
        'text' => __('Adjust the display teasers for this department', 'victheme_department'),
      )))
      ->getParent()
      ->addChildren(new VTCore_Bootstrap_Element_BsElement(array(
        'type' => 'div',
        'grids' => array(
          'columns' => array(
            'mobile' => '12',
            'tablet' => '8',
            'small' => '8',
            'large' => '8',
          ),
        ),
      )))
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'text' => __('Title', 'victheme_department'),
        'name' => $this->getContext('name') . '[teasers][title]',
        'value' => $this->getContext('value.title'),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'text' => __('Slogan', 'victheme_department'),
        'name' => $this->getContext('name') . '[teasers][slogan]',
        'value' => $this->getContext('value.slogan'),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsTextarea(array(
        'text' => __('Description', 'victheme_department'),
        'name' => $this->getContext('name') . '[teasers][description]',
        'value' => $this->getContext('value.description'),
        'input_elements' => array(
          'raw' => true,
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsColor(array(
        'text' => __('Background Color', 'victheme_department'),
        'name' => $this->getContext('name') . '[teasers][color]',
        'value' => $this->getContext('value.color'),
        'data' => array(
          'container' => '#department-teaser-color-selector-' . $this->getMachineID(),
        ),
        'attributes' => array(
          'id' => 'department-teaser-color-selector-' . $this->getMachineID(),
        ),
      )))
      ->addChildren(new VTCore_Wordpress_Form_WpMedia(array(
        'text' => __('Image', 'victheme_department'),
        'name' => $this->getContext('name') . '[teasers][image]',
        'value' => $this->getContext('value.image'),
        'data' => array(
          'type' => 'image',
          'title' => __('Select Image', 'victheme_department'),
          'button' =>__('Select Image', 'victheme_department'),
        ),
      )))
      ->addChildren(new VTCore_Wordpress_Form_WpIconSet(array(
        'name' => $this->getContext('name') . '[teasers][icon]',
        'value' => $this->getContext('value.icon'),
        'build' => array(
          'preview' => false,
          'picker' => true,
          'sizing' => false,
          'styling' => false,
          'border' => false,
        ),
      )));

  }

}