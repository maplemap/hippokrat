<?php
/**
 * Class for building the video field element
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Department_Form_Headers
extends VTCore_Bootstrap_Form_Base {

  protected $context = array(
    'type' => 'div',
    'attributes' => array(
      'class' => array(
        'row',
      ),
    ),
    'name' => 'department',
    'value' => array(
      'image' => false,
      'slogan' => false,
      'author' => false,
      'position' => false,
    ),

  );


  private $video;

  /**
   * Overriding the parent buildElement method
   */
  public function buildElement() {

    parent::buildElement();

    $this
      ->addChildren(new VTCore_Bootstrap_Element_BsElement(array(
        'type' => 'div',
        'grids' => array(
          'columns' => array(
            'mobile' => '12',
            'tablet' => '4',
            'small' => '4',
            'large' => '4',
          ),
        ),
      )))
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Element_BsElement(array(
        'type' => 'h4',
        'text' => __('Header Display', 'victheme_department'),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsDescription(array(
        'text' => __('Configure the department header display', 'victheme_department'),
      )))
      ->getParent()
      ->addChildren(new VTCore_Bootstrap_Element_BsElement(array(
        'type' => 'div',
        'grids' => array(
          'columns' => array(
            'mobile' => '12',
            'tablet' => '8',
            'small' => '8',
            'large' => '8',
          ),
        ),
      )))
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsTextarea(array(
        'text' => __('Slogan', 'victheme_department'),
        'name' => $this->getContext('name') . '[headers][slogan]',
        'value' => $this->getContext('value.slogan'),
        'input_elements' => array(
          'raw' => true,
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()
      ->addChildren(new VTCore_Wordpress_Form_WpMedia(array(
        'text' => __('Banner Image', 'victheme_department'),
        'name' => $this->getContext('name') . '[headers][image]',
        'value' => $this->getContext('value.image'),
        'data' => array(
          'type' => 'image',
          'title' => __('Select Image', 'victheme_department'),
          'button' =>__('Select Image', 'victheme_department'),
        ),
        'grids' => array(
          'columns' => array(
            'mobile' => '12',
            'tablet' => '6',
            'small' => '6',
            'large' => '6',
          ),
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Grid_BsColumn(array(
        'grids' => array(
          'columns' => array(
            'mobile' => '12',
            'tablet' => '6',
            'small' => '6',
            'large' => '6',
          ),
        ),
      )))
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'text' => __('Author', 'victheme_department'),
        'name' => $this->getContext('name') . '[headers][author]',
        'value' => $this->getContext('value.author'),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'text' => __('Job Position', 'victheme_department'),
        'name' => $this->getContext('name') . '[headers][position]',
        'value' => $this->getContext('value.position'),
      )));

  }

}