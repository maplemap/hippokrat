<?php
/**
 * Class for building the services field element
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Department_Form_Services
extends VTCore_Bootstrap_Form_Base {

  protected $context = array(

    'type' => 'div',
    'attributes' => array(
      'class' => array(
        'row',
      ),
    ),
    'name' => 'department',
    'value' => array(
      'title' => false,
      'description' => false,
    ),

  );


  /**
   * Overriding the parent buildElement method
   */
  public function buildElement() {

    parent::buildElement();

    $this
      ->addChildren(new VTCore_Bootstrap_Element_BsElement(array(
        'type' => 'div',
        'grids' => array(
          'columns' => array(
            'mobile' => '12',
            'tablet' => '4',
            'small' => '4',
            'large' => '4',
          ),
        ),
      )))
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Element_BsElement(array(
        'type' => 'h4',
        'text' => __('Service Box', 'victheme_department'),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsDescription(array(
        'text' => __('Configure the service listing box section', 'victheme_department'),
      )))
      ->getParent()
      ->addChildren(new VTCore_Bootstrap_Element_BsElement(array(
        'type' => 'div',
        'grids' => array(
          'columns' => array(
            'mobile' => '12',
            'tablet' => '8',
            'small' => '8',
            'large' => '8',
          ),
        ),
      )))
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsText(array(
        'text' => __('Title', 'victheme_department'),
        'name' => $this->getContext('name') . '[services][title]',
        'value' => $this->getContext('value.title'),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsTextarea(array(
        'text' => __('Description', 'victheme_department'),
        'name' => $this->getContext('name') . '[services][description]',
        'value' => $this->getContext('value.description'),
        'input_elements' => array(
          'raw' => true,
        ),
      )));

  }

}