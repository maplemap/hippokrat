<?php
/**
 * Class for building the main metabox wrapper
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Department_Form_Metabox
extends VTCore_Bootstrap_Element_BsTabs {

  /**
   * Overriding parent method
   * @see VTCore_Html_Base::buildElement()
   */
  public function buildElement() {

    VTCore_Wordpress_Utility::loadAsset('wp-bootstrap');
    VTCore_Wordpress_Utility::loadAsset('wp-media');
    VTCore_Wordpress_Utility::loadAsset('department-metabox');

    $this->addContext('values', get_post_meta(get_the_id(), '_department_information', true));

    $object = new VTCore_Bootstrap_Form_Base(array(
      'type' => false,
    ));

    $object->addChildren(new VTCore_Department_Form_Director(array(
      'value' => $this->getContext('values.director'),
    )));

    $object->addChildren(new VTCore_Department_Form_Members(array(
      'value' => $this->getContext('values.members'),
    )));

    do_action('vtcore_department_form_information_alter', $object, $this);

    $this->addContext('contents.members', array(
      'title' => __('Information', 'victheme_department'),
      'content' => $object,
    ));


    $object = new VTCore_Bootstrap_Form_Base(array(
      'type' => false,
    ));

    $object->addChildren(new VTCore_Department_Form_Headers(array(
      'value' => $this->getContext('values.headers'),
    )));

    $object->addChildren(new VTCore_Department_Form_Teasers(array(
      'value' => $this->getContext('values.teasers'),
    )));

    $object->addChildren(new VTCore_Department_Form_Promotional(array(
      'value' => $this->getContext('values.promotional'),
    )));

    if (defined('VTCORE_SERVICES_LOADED')) {
      $object->addChildren(new VTCore_Department_Form_Services(array(
        'value' => $this->getContext('values.services'),
      )));
    }

    do_action('vtcore_department_form_display_alter', $object, $this);

    $this->addContext('contents.display', array(
      'title' => __('Display', 'victheme_department'),
      'content' => $object,
    ));

    $this->addContext('active', 'members');

    parent::buildElement();

  }

}