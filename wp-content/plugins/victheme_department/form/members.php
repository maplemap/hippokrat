<?php
/**
 * Class for building the members metabox
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Department_Form_Members
extends VTCore_Bootstrap_Form_Base {

  protected $context = array(

    'type' => 'div',
    'attributes' => array(
      'class' => array(
        'row',
      ),
    ),
    'name' => 'department',
    'value' => array(
      false,
    ),
  );

  private $rows = array();
  private $options = array();


  /**
   * Overriding parent method
   * @see VTCore_Html_Base::buildElement()
   */
  public function buildElement() {

    parent::buildElement();

    // Allow other plugin to define the valid user to use
    // services item
    $this->options = apply_filters('vtcore_department_valid_users', $this->options);

    // Fallback to simple user fetching
    if (empty($this->options)) {
      $users = get_users();
      $this->options = array();
      foreach ($users as $user) {
        $name = $user->get('display_name');
        if (empty($name)) {
          $name = $user->get('user_nicename');
        }
        if (empty($name)) {
          $name = $user->get('user_login');
        }
        $this->options[$user->ID] = $name;
      }
    }

    $this
      ->addChildren(new VTCore_Bootstrap_Element_BsElement(array(
        'type' => 'div',
        'grids' => array(
          'columns' => array(
            'mobile' => '12',
            'tablet' => '4',
            'small' => '4',
            'large' => '4',
          ),
        ),
      )))
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Element_BsElement(array(
        'type' => 'h4',
        'text' => __('Members', 'victheme_department'),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsDescription(array(
        'text' => __('Select user that is a member of this department', 'victheme_department'),
      )))
      ->getParent()
      ->addChildren(new VTCore_Bootstrap_Element_BsElement(array(
        'type' => 'div',
        'grids' => array(
          'columns' => array(
            'mobile' => '12',
            'tablet' => '8',
            'small' => '8',
            'large' => '8',
          ),
        ),
      )))
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsSelect(array(
        'text' => __('Available Users', 'victheme_department'),
        'description' => __('hold ctrl and click to select multiple members', 'victheme_department'),
        'multiple' => true,
        'name' => $this->getContext('name') . '[members]',
        'value' => $this->getContext('value'),
        'options' => $this->options,
      )));
  }
}