<?php
/**
 * Class for building the director metabox
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Department_Form_Director
  extends VTCore_Bootstrap_Form_Base {

  protected $context = array(

    'type' => 'div',
    'attributes' => array(
      'class' => array(
        'row',
      ),
    ),
    'name' => 'department',
    'value' => '',
  );

  private $options = array();


  /**
   * Overriding parent method
   * @see VTCore_Html_Base::buildElement()
   */
  public function buildElement() {

    parent::buildElement();

    $this->buildOptions();

    $this->options = apply_filters('vtcore_department_valid_users', $this->options);

    $this
      ->addChildren(new VTCore_Bootstrap_Element_BsElement(array(
        'type' => 'div',
        'grids' => array(
          'columns' => array(
            'mobile' => '12',
            'tablet' => '4',
            'small' => '4',
            'large' => '4',
          ),
        ),
      )))
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Element_BsElement(array(
        'type' => 'h4',
        'text' => __('Head of Department', 'victheme_department'),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsDescription(array(
        'text' => __('Choose the user acting as the head of this department', 'victheme_department'),
      )))
      ->getParent()
      ->addChildren(new VTCore_Bootstrap_Element_BsElement(array(
        'type' => 'div',
        'grids' => array(
          'columns' => array(
            'mobile' => '12',
            'tablet' => '8',
            'small' => '8',
            'large' => '8',
          ),
        ),
      )))
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsSelect(array(
        'text' => __('Select Director', 'victheme_department'),
        'value' => $this->getContext('value'),
        'name' => $this->getContext('name').  '[director]',
        'options' => $this->options,
      )));
  }


  /**
   * Building the media table rows contents array
   */
  private function buildOptions() {

    $users = get_users();
    $this->options = array(
      false => __('-- Select --', 'victheme_department'),
    );

    foreach ($users as $user) {
      $name = $user->get('display_name');
      if (empty($name)) {
        $name = $user->get('user_nicename');
      }
      if (empty($name)) {
        $name = $user->get('user_login');
      }
      $this->options[$user->ID] = $name;
    }

    return $this;
  }

}