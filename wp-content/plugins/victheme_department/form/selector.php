<?php
/**
 * Class for building the video field element
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Department_Form_Selector
extends VTCore_Bootstrap_Form_Base {

  protected $context = array(
    'type' => 'div',
    'attributes' => array(
      'class' => array(
        'row',
      ),
    ),
    'name' => 'deparment',
    'value' => array(
      'department' => false,
    ),

  );

  /**
   * Overriding the parent buildElement method
   */
  public function buildElement() {

    parent::buildElement();

    $this->options = array(
      false => __('-- Select --', 'victheme_department'),
    );

    $posts = get_posts(array(
      'post_type' => 'department',
      'post_status' => 'publish',
      'posts_per_page' => 600,
    ));

    foreach ($posts as $post) {
      $this->options[$post->ID] = $post->post_title;
    }

    $this
      ->addChildren(new VTCore_Bootstrap_Element_BsElement(array(
        'type' => 'div',
        'grids' => array(
          'columns' => array(
            'mobile' => '12',
            'tablet' => '4',
            'small' => '4',
            'large' => '4',
          ),
        ),
      )))
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Element_BsElement(array(
        'type' => 'h4',
        'text' => __('Department', 'victheme_department'),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsDescription(array(
        'text' => __('Choose which department this service belongs to', 'victheme_department'),
      )))
      ->getParent()
      ->addChildren(new VTCore_Bootstrap_Element_BsElement(array(
        'type' => 'div',
        'grids' => array(
          'columns' => array(
            'mobile' => '12',
            'tablet' => '8',
            'small' => '8',
            'large' => '8',
          ),
        ),
      )))
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsSelect(array(
        'text' => __('Available Department', 'victheme_department'),
        'name' => $this->getContext('name') . '[department]',
        'value' => $this->getContext('value.department'),
        'options' => $this->options,
      )));

  }

}