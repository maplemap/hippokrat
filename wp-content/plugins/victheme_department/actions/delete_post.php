<?php
/**
 * Hooking into wordpress delete_post action
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Department_Actions_Delete__Post
extends VTCore_Wordpress_Models_Hook {

  protected $weight = 11;
  private $post;

  public function hook($post_id = FALSE) {

    global $wpdb;
    $this->post =  $wpdb->get_row($wpdb->prepare("SELECT * FROM $wpdb->posts WHERE ID = %d", $post_id));

    if ($this->post->post_type == 'department') {
      $object = new VTCore_Department_Entity(array('post' => $this->post));
      $object->removeRelatedServicesEntry()->clearCache();
    }

    if ($this->post->post_type == 'services') {
      $object = new VTCore_Department_Entity(array('post' => $this->post));
      $object->clearCache();
    }
  }
}