<?php
/**
 * Hooking into wordpress the_post action.
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Department_Actions_The__Post
extends VTCore_Wordpress_Models_Hook {

  public function hook($post = NULL) {

    if ($post->post_type == 'department') {
      $post->department = new VTCore_Department_Entity(array('post' => $post));
    }

  }
}