<?php
/**
 * Hooking into vtcore_services for registering
 * form element to link department and services.
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Department_Actions_VTCore__Services__Form__Information__Alter
extends VTCore_Wordpress_Models_Hook {

  protected $argument = 2;

  public function hook($object = NULL, $form = NULL) {

    $object->prependChild(new VTCore_Department_Form_Selector(array(
      'name' => 'services',
      'value' => array(
        'department' => $form->getContext('values.department'),
      ),
    )));
  }
}