<?php
/**
 * Hooking into wordpress delete_post action
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Department_Actions_Delete__User
extends VTCore_Wordpress_Models_Hook {

  protected $argument = 2;
  protected $weight = 11;
  private $post;

  public function hook($user_id = FALSE, $replace = FALSE) {

    if (VTCORE_DEPARTMENT_DATA_MODE == 'user') {
      global $wpdb;

      $post_ids = $wpdb->get_col($wpdb->prepare("SELECT m.post_id FROM $wpdb->postmeta WHERE meta_key = %s AND m.meta_value = %s", 'department_head', $user_id));

      if (!empty($post_ids) && is_array($post_ids)) {
        foreach ($post_ids as $post_id) {
          $meta = get_post_meta($post_id, '_department_information');
          $meta['director'] = $replace;
          update_post_meta($post_id, '_department_information', $meta);
          update_post_meta($post_id, '_department_head', $replace);
        }
      }
    }
  }
}