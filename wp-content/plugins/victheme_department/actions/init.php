<?php
/**
 * Hooking into wordpress init action.
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Department_Actions_Init
extends VTCore_Wordpress_Models_Hook {

  public function hook() {

    // Booting Members post type
    $object = new VTCore_Department_PostType();

    // Last chance to alter the post type before it get registered
    do_action('vtcore_department_registering_posttype', $object);

    // Register the post type
    $object->registerPostType();

    // Department "home" archive template redirection
    // on custom taxonomy and department/ link
    add_rewrite_rule('department/?$', 'index.php?post_type=department', 'top');
  }
}