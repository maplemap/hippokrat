<?php
/**
 * Simple Class for loading all related post meta data
 * into the object and utilizing the dotted notation
 * for easy parsing.
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Department_Entity
extends VTCore_Wordpress_Models_Dot {

  protected $metaKey = '_department_information';
  protected $serviceKey = '_department_services';
  protected static $cache = array();


  /**
   * Overriding parent method
   * @param array $options
   */
  public function __construct($options = array()) {

    if (isset($options['post'])) {
      $options['post'] = (array) $options['post'];
    }

    $this->merge($options);

    if (!$this->get('post') && get_the_ID()) {
      $this->add('post', (array) get_post(get_the_ID()));
    }

    if (!$this->get('data') && $this->get('post')) {
      $this->add('data', get_post_meta($this->get('post.ID'), $this->metaKey, true));
    }

    // Load related services
    if (defined('VTCORE_SERVICES_LOADED')) {
      if (!isset(self::$cache['services']) && !isset(self::$cache['services'][$this->get('post.ID')])) {
        self::$cache['services'][$this->get('post.ID')] = $this->getMeta($this->serviceKey, $this->get('post.ID'));
      }

      if (!isset(self::$cache['services'][$this->get('post.ID')])) {
        self::$cache['services'][$this->get('post.ID')] = array();
      }

      $this->add('services', self::$cache['services'][$this->get('post.ID')]);
    }

    do_action('vtcore_department_entity_loading', $this);

  }


  /**
   * Method for retrieving stored wordpress meta post id
   * by defining the metakey, meta value and post status
   * @param $key
   *  string meta key
   * @param $value
   *  string meta value
   * @param string $status
   *  string post status
   * @return array
   */
  protected function getMeta($key, $value, $status = 'publish') {
    global $wpdb;
    $results = $wpdb->get_col(
      $wpdb->prepare("
        SELECT m.post_id
        FROM $wpdb->postmeta as m
        JOIN $wpdb->posts as p
        ON m.post_id = p.ID
        WHERE m.meta_key = %s
        AND m.meta_value = %s
        AND p.post_status = %s
      ", $key, $value, $status));

    foreach ($results as $key => $value) {
      $results[$key] = (int) $value;
    }

    return $results;
  }


  /**
   * Method for clearing the staticaly cached variable
   * @return $this
   */
  public function clearCache() {
    self::$cache = array();
    return $this;
  }


  /**
   * Method for removing all linked service entry to
   * this object.
   *
   * @return $this
   */
  public function removeRelatedServicesEntry() {
    foreach ($this->get('services') as $service_id) {
      delete_post_meta($service_id, '_department_services');

      $service_meta = get_post_meta($service_id, '_services_data');
      if (!is_array($service_meta)
          || !isset($service_meta['department'])) {
        continue;
      }

      $service_meta['department'] = false;
      update_post_meta($service_id, '_services_data', $service_meta);

    }

    return $this;
  }


  /**
   * Method for reseting the stored rendered markup per type
   *
   * @param $type
   *   string the rendered type to reset
   *   valid type : teasers, single, all (reset all rendered types)
   * @return $this
   */
  public function resetRendered($type = 'all') {
    if ($type == 'all') {
      $this->remove('rendered');
    }
    elseif ($this->get('rendered.' . $type)) {
      $this->remove('rendered.' . $type);
    }

    return $this;
  }


  /**
   * Method for retrieving the object teaser image and will
   * fall back to post thumbnail if teaser image is not available
   * or empty string if nothing is available to fetch
   *
   * @param $size
   *    The image size string as in wordpress registered image size or array or width and height
   * @return string
   *    empty string or the image markup
   */
  public function getTeaserImage($size = 'medium') {

    if (!$this->get('rendered.teasers.image')) {
      $image = '';
      if ($this->get('data.teasers.image')) {
        $image = wp_get_attachment_image($this->get('data.teasers.image'), $size);
      }
      elseif ($this->get('post.ID') && has_post_thumbnail($this->get('post.ID'))) {
        $image = get_the_post_thumbnail($this->get('post.ID'), $size);
      }

      $this->add('rendered.teasers.image', wp_kses_post($image));
    }

    return $this->get('rendered.teasers.image');
  }



  /**
   * Method for retrieving the object teaser icon image and will
   * fall back to post thumbnail if teaser icon image is not available
   * or empty string if nothing is available to fetch
   *
   * @param $size
   *    The image size string as in wordpress registered image size or array or width and height
   * @return string
   *    empty string or the icon image markup
   */
  public function getTeaserIcon() {

    if (!$this->get('rendered.teasers.icon')) {
      $object = new VTCore_Wordpress_Element_WpIcon($this->get('data.teasers.icon'));
      $this->add('rendered.teasers.icon', wp_kses_post($object->__toString()));
    }

    return $this->get('rendered.teasers.icon');
  }


  /**
   * Method for retrieving the object teaser title and fallback
   * to post title if no teaser title defined
   *
   * it will also process any shortcode defined in the title
   *
   * @return string
   *  empty string or teaser title markup
   */
  public function getTeaserTitle() {

    if (!$this->get('rendered.teasers.title')) {
      $title = '';
      if ($this->get('data.teasers.title')) {
        $title = $this->get('data.teasers.title');
      }
      elseif ($this->get('post.ID')) {
        $title = get_the_title($this->get('post.ID'));
      }

      $this->add('rendered.teasers.title', wp_kses_post(do_shortcode($title)));
    }

    return $this->get('rendered.teasers.title');
  }



  /**
   * Method for retrieving the object teaser slogan
   *
   * it will also process any shortcode defined in the slogan
   *
   * @return string
   *  empty string or teaser slogan markup
   */
  public function getTeaserSlogan() {

    if (!$this->get('rendered.teasers.slogan')) {
      $slogan = '';
      if ($this->get('data.teasers.slogan')) {
        $slogan = $this->get('data.teasers.slogan');
      }

      $this->add('rendered.teasers.slogan', wp_kses_post(do_shortcode($slogan)));
    }

    return $this->get('rendered.teasers.slogan');
  }



  /**
   * Method for retrieving the object teaser description and fallback
   * to post excerpt if no teaser description is defined.
   *
   * It will also process any shortcode defined in the description
   *
   * @return string
   *  empty string or teaser description markup
   */
  public function getTeaserDescription() {

    if (!$this->get('rendered.teasers.description')) {
      $description = '';
      if ($this->get('data.teasers.description')) {
        $description = $this->get('data.teasers.description');
      }
      elseif ($this->get('post.post_excerpt')) {
        if (!post_password_required($this->get('post.ID'))) {
          $description = apply_filters('get_the_excerpt', $this->get('post.post_excerpt'));
        }
        else {
          $description = __('There is no excerpt because this is a protected post.');
        }
      }

      $this->add('rendered.teasers.description', wp_kses_post(do_shortcode($description)));
    }

    return $this->get('rendered.teasers.description');
  }



  /**
   * Method for building markup for registering custom css
   * styles for each object teaser.
   *
   * @param $target
   *   Valid css selector for the teaser target
   * @return array|null|string|void
   *   empty string or the style markup for registering css style
   */
  public function getTeaserStyle($target) {

    if (!$this->get('rendered.teasers.background')) {
      if ($this->get('data.teasers.color')) {
        $css = new VTCore_CSSBuilder_Factory(array(
          $target,
        ));
        $css
          ->Background(array(
            'color' => $this->get('data.teasers.color'),
          ));

        $object = new VTCore_Html_Style();
        $object->addChildren($css->__toString());

        $this->add('rendered.teasers.background', $object->__toString());
      }
    }

    return $this->get('rendered.teasers.background');
  }



  /**
   * Method for extracting department data for single page
   *
   * @param $type
   *   array keys in dotted notation format
   *
   * @return array|null|string|void
   */
  public function getSingleData($type) {

    if (!$this->get('rendered.single.' . $type)) {
      if ($this->get('data.' . $type)) {
        $this->add('rendered.single.' . $type, wp_kses_post(do_shortcode($this->get('data.' . $type))));
      }
    }

    return $this->get('rendered.single.' . $type);
  }


  /**
   * This method will retrieve and format the department
   * single banner
   *
   * @param string $size
   * @return array|null|string|void
   */
  public function getSingleBanner($size = 'large') {

    if (!$this->get('rendered.single.banner')) {
      $image = '';
      if ($this->get('data.headers.image')) {
        $image = wp_get_attachment_image($this->get('data.headers.image'), $size);
      }

      $this->add('rendered.single.banner', wp_kses_post($image));
    }

    return $this->get('rendered.single.banner');
  }


  /**
   * This method will retrieve, select and format the correct markup
   * for department promotional video
   *
   * @return array|null|string|void
   */
  public function getSingleVideo() {

    if (!$this->get('rendered.single.video')) {

      $source = $this->get('data.promotional.video');
      $source = array_filter($source);
      $context = array();
      $markup = '';

      if (isset($source['videolocal'])) {

        if ($source['videolocal'] == 'outside') {
          if (isset($source['youtube'])) {
            $context['videos'][] = $source['youtube'];
            $context['media-element'] = TRUE;
            $context['video-type'] = 'video/youtube';
          }
          elseif ($source['vimeo']) {
            $context['videos'][] = $source['vimeo'];
            $context['media-element'] = TRUE;
            $context['video-type'] = 'video/vimeo';
          }

          // WP Needs to update mediaelement js to build 2.17.0 in order for this to work
          elseif ($source['dailymotion']) {
            $context['videos'][] = $source['video.dailymotion'];
            $context['media-element'] = TRUE;
            $context['video-type'] = 'video/dailymotion';
          }
        }
        elseif ($source['videolocal'] == 'local' && isset($source['attachment_id'])) {
          $context['attachment_id'] = $source['attachment_id'];
        }

        if (!empty($context)) {
          $object = new VTCore_Wordpress_Element_WpVideo($context);

          // MediaElement needs attribute type
          if ($source['videolocal'] == 'outside') {
            $child = $object->lastChild();
            if (is_a($child, 'VTCore_Html_Source')) {
              $child
                ->addAttribute('type', $context['video-type']);
            }
          }

          $markup = $object->__toString();
        }
      }

      $this->add('rendered.single.video', wp_kses_post($markup));
    }

    return $this->get('rendered.single.video');
  }

}