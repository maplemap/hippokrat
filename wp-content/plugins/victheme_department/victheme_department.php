<?php
/*
Plugin Name: VicTheme Department
Plugin URI: http://victheme.com
Description: Plugin suite developed creating department groups with member
Author: jason.xie@victheme.com
Version: 1.0.2
Author URI: http://victheme.com
*/

define('VTCORE_DEPARTMENT_CORE_VERSION', '1.7.0');

add_action('plugins_loaded', 'bootVicthemeDepartment', 12);

function bootVicthemeDepartment() {

  if (!(defined('VTCORE_VERSION') && version_compare(VTCORE_VERSION, VTCORE_DEPARTMENT_CORE_VERSION, '='))) {

    add_action('admin_notices', 'DepartmentMissingCoreNotice');

    function DepartmentMissingCoreNotice() {

      if (!defined('VTCORE_VERSION')) {
        $notice = __('VicTheme Department depends on VicTheme Core Plugin which is not activated or missing, Please enable it first before VicTheme Department can work properly.');
      }
      elseif (version_compare(VTCORE_VERSION, VTCORE_DEPARTMENT_CORE_VERSION, '!=')) {
        $notice = sprintf(__('VicTheme Department depends on VicTheme Core Plugin API version %s to operate properly.', 'victheme_department'), VTCORE_DEPARTMENT_CORE_VERSION);
      }

      if (isset($notice)) {
        echo '<div class="error""><p>' . $notice . '</p></div>';
      }

    }

    return;
  }

  define('VTCORE_DEPARTMENT_LOADED', true);
  define('VTCORE_DEPARTMENT_BASE_DIR', dirname(__FILE__));
  define('VTCORE_DEPARTMENT_DATA_MODE', apply_filters('vtcore_department_alter_data_mode', 'user'));

  include_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'init.php');
  new VTCore_Department_Init();
}