<?php
/**
 * Hooking into wordpress admin_menu action for
 * registering maps configuration page.
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Headline_Actions_Admin__Menu
extends VTCore_Wordpress_Models_Hook {

  public function hook() {
     add_menu_page(
        'Headline Configuration',
        'Headlline',
        'manage_options',
        'headline-main',
        array(new VTCore_Headline_Admin, 'buildPage'),
        'dashicons-align-none');
  }
}