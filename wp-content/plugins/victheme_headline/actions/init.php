<?php
/**
 * Hooking into wordpress init action 
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Headline_Actions_Init
extends VTCore_Wordpress_Models_Hook {

  public function hook() {

    // Clear headline cache
    VTCore_Headline_Utility::clearCache();

    $headline = new VTCore_Headline_Metabox_Factory();
    do_action('vtcore_headline_metabox_object_alter', $headline);
  }
}