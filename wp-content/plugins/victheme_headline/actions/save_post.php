<?php
/**
 * Hooking into wordpress save_post action
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Headline_Actions_Save__Post
extends VTCore_Wordpress_Models_Hook {

  protected $argument = 2;

  public function hook($post_id = FALSE, $post = FALSE) {

    if (isset($_POST['headline'])) {

      if (isset($_POST['headline']['items'])) {
        foreach ($_POST['headline']['items'] as $key => $data) {

          // Remove empty headline items
          if (empty($data['title']) && empty($data['text']) && empty($data['icon'])) {
            unset($_POST['headline']['items'][$key]);
          }
        }
      }

      update_post_meta($post->ID, '_headline_data', $_POST['headline']);

      // Clean ourselves.
      unset($_POST['headline']);

      // Clear asset cache
      VTCore_Wordpress_Init::getFactory('assets')
        ->mutate('prefix', 'comp-front-')
        ->clearCache()
        ->mutate('prefix', 'comp-admin-');

      // Clear headline cache
      VTCore_Headline_Utility::clearCache();
    }
  }
}