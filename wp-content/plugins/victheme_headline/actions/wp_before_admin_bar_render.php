<?php
/**
 * Class for filtering wp_before_admin_bar_render
 * and adding extra class to it.
 *
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Headline_Actions_Wp__Before__Admin__Bar__Render
extends VTCore_Wordpress_Models_Hook {

  public function hook() {
    global $wp_admin_bar;

    if (is_archive()) {
      $wp_admin_bar->add_menu(array(
        'id' => 'headline-configuration-edit',
        'title' => __('Edit Headline', 'victheme_headline'),
        'href' => admin_url() . 'admin.php?page=headline-main',
        'parent' => '',
      ));
    }

  }
}