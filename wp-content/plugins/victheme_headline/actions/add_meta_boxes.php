<?php
/**
 * Hooking into wordpress add_meta_boxes action
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Headline_Actions_Add__Meta__Boxes
extends VTCore_Wordpress_Models_Hook {

  public function hook() {
    $notShowed = apply_filters('vtcore_headline_hide_metabox', array('nav_menu_items', 'attachment', 'vc_grid_item', 'shop_order'));
    foreach (get_post_types() as $type => $name) {

      if (in_array($type, $notShowed)) {
        continue;
      }

      add_meta_box('vtcore-headline', __('Headline Manager', 'victheme_headline'), array(new VTCore_Headline_Metabox_Factory(), 'buildMetabox'), $type, 'normal', 'high');
    }
  }
}