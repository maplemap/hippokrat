<?php
/**
 * Utility singleton class
 * This class is for all the function that can be
 * used outside any of the headline classes hiearchy
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Headline_Utility
extends VTCore_Utility {

  protected static $instance = false;

  /**
   * Main function for building headline content
   * @see headline.php
   */
  static public function getHeadline() {

    // Honor vtcore global clear cache.
    if (defined('VTCORE_CLEAR_CACHE') && VTCORE_CLEAR_CACHE) {
      self::clearCache();
    }


    // There could be only one headlie per page request
    // so cache it staticaly so it is safe to call this
    // function multiple time without speed penalty.
    if (self::$instance == false) {
      $config = new VTCore_Headline_Config();

      // Load post title if available
      if (!is_singular('post')) {
        $instance = $config->get('blog');
      }

      // If archived page
      if (is_archive()) {
        $instance = $config->get('archive');
      }

      // Title for categories
      if (is_category()) {
        $instance = $config->get('category');
      }

      // Title for search
      if (is_search()) {
        $instance = $config->get('search');
      }

      if (is_tag()) {
        $instance = $config->get('tags');
      }

      if (is_author()) {
        $instance = $config->get('author');
      }

      if (is_404()) {
        $instance = $config->get('notfound');
      }

      // Detect post ID, Caution this can fetch "last looped" id instead
      // the true page id
      $pageID = get_option('page_for_posts');
      $post_id = get_the_ID();
      if (is_home() && !empty($pageID)) {
        $post_id = $pageID;
      }

      // Support for woocommerce
      if (function_exists('is_shop') && is_shop()) {
        $pageID = get_option('woocommerce_shop_page_id');
        $post_id = false;
        if (!empty($pageID)) {
          $post_id = $pageID;
        }
      }


      // Always use post meta if available
      if ($post_id) {
        $meta = get_post_meta($post_id, '_headline_data');
        if (!empty($meta)) {
          $instance = array_shift($meta);
        }
      }

      // Load post title if available
      if (is_singular()) {

        $post_title = get_the_title();

        // Fallback to default
        if (empty($instance)) {
          $instance = $config->get('blog');

          if (!empty($post_title)) {
            $instance['general']['title'] = get_the_title();
          }

          $instance['general']['subtitle'] = '';
        }

        if (!empty($post_title) && isset($instance['general']['title']) && $instance['general']['title'] == '') {
          $instance['general']['title'] = get_the_title();
        }
      }

      // Backward compatibility for new video settings
      if (isset($instance['background']['video'])
        && !empty($instance['background']['video'])
      ) {

        if (isset($instance['background']['image'])) {
          $image = $instance['background']['image'];
          if (is_numeric($instance['background']['image'])) {
            $image = wp_get_attachment_url($instance['background']['image']);
          }
          $instance['background']['video']['poster'] = $image;
        }

        if (isset($instance['background']['video']['mp4']) && is_numeric($instance['background']['video']['mp4'])) {
          $instance['background']['video']['mp4'] = wp_get_attachment_url($instance['background']['video']['mp4']);
        }

        if (isset($instance['background']['video']['ogv']) && is_numeric($instance['background']['video']['ogv'])) {
          $instance['background']['video']['ogv'] = wp_get_attachment_url($instance['background']['video']['ogv']);
        }

        if (isset($instance['background']['video']['webm']) && is_numeric($instance['background']['video']['webm'])) {
          $instance['background']['video']['webm'] = wp_get_attachment_url($instance['background']['video']['webm']);
        }

        $video = FALSE;
        foreach (array('video', 'ogv', 'webm') as $type) {
          if (isset($instance['background']['video'][$type])
              && !empty($instance['background']['video'][$type])
              && preg_match('/^.*\.(mp4|mov|ogv|ogg|webm)$/i', $instance['background']['video'][$type])) {
            $video = TRUE;
            break;
          }
        }

        if (!$video) {
          $instance['background']['video'] = array();
        }
      }

      $instance = apply_filters('vtcore_headline_alter', $instance, $config->extract(), $config);
      $filtered = self::arrayFilterEmpty((array) $instance);

      if (isset($instance['general']['enable'])) {
        $filtered['general']['enable'] = $instance['general']['enable'];
      }

      // Js need true boolean and cannot be removed as self::arrayFilterEmpty() is
      // removing these options when not available
      if (isset($instance['background']['video'])
        && !empty($instance['background']['video'])
      ) {

        foreach (array('autoplay', 'loop', 'scale', 'fullscreen') as $key) {
          if (isset($filtered['background']['video'][$key])) {
            $filtered['background']['video'][$key] = filter_var($instance['background']['video'][$key], FILTER_VALIDATE_BOOLEAN);
          }
          else {
            $filtered['background']['video'][$key] = FALSE;
          }
        }
      }

      // Bug fix : video never empty
      if (isset($video) && !$video) {
        $filtered['background']['video'] = array();
      }

      self::$instance = $filtered;
    }

    if (!isset($instance['background']['video']['mp4'])
        && !isset($instance['background']['video']['ogv'])
        && !isset($instance['background']['video']['webm'])) {

      $instance['background']['video'] = array();
    }

    // Return cached instance
    return self::$instance;
  }




  /**
   * Check if we should enable the headline or not.
   * @param unknown $instance
   */
  static public function checkEnabled($key, $instance) {
    return isset($instance[$key]['enable']) ? $instance[$key]['enable'] : false;
  }





  /**
   * Retrieve css class for headline column bootstrap classes
   */
  static public function getHeadlineColumn($instance) {

    if (!isset($instance['general']['headline_columns'])) {
      $instance['general']['headline_columns'] = array(
        'mobile' => '12',
        'tablet' => '6',
        'small' => '6',
        'large' => '6',
      );
    }

    $bootstrap = new VTCore_Bootstrap_Grid_Column(array('columns' => $instance['general']['headline_columns']));
    return $bootstrap->getClass();
  }






  /**
   * Retrieve css class for item column bootstrap classes
   */
  static public function getItemColumn($instance) {

    if (!isset($instance['general']['item_columns'])) {
      $instance['general']['item_columns'] = array(
        'mobile' => '0',
        'tablet' => '6',
        'small' => '6',
        'large' => '6',
      );
    }

    $bootstrap = new VTCore_Bootstrap_Grid_Column(array('columns' => $instance['general']['item_columns']));
    return $bootstrap->getClass();
  }






  /**
   * Retrieving css block for masking elements
   * This is not a replacement for the headline css
   * only the dynamically configured css will be printed,
   * for the rest of the css rules will still use headline.css
   * file.
   */
  static public function getMaskingCSS($instance) {

    if (!isset($instance['masking']) || empty($instance['masking'])) {
      return '';
    }

    $css = new VTCore_CSSBuilder_Factory('.headline-mask');

    if (isset($instance['masking']['image']) && is_numeric($instance['masking']['image'])) {
      $instance['masking']['image'] = wp_get_attachment_url($instance['masking']['image']);
    }

    // Remove broken gradient
    if (isset($instance['masking']['gradient'])
        && isset($instance['masking']['gradient']['type'])
        && ($instance['masking']['gradient']['type'] == false
        || empty($instance['masking']['gradient']['colors']))) {

      unset($instance['masking']['gradient']);

    }

    $css->Background($instance['masking']);

    if (!empty($instance['masking']['opacity'])) {
      $css->Abstract(array(
        'opacity' => $instance['masking']['opacity'],
      ));
    }



    return $css->__toString();
  }


  /**
   * Allow user to clear the cache;
   */
  static public function clearCache() {
    self::$instance = false;
  }



  /**
   * Retrieve background position as configured by user
   * or use center top as the default.
   */
  static public function getBackgroundPosition($instance) {
    return (isset($instance['background']['left']) ? $instance['background']['left'] : 'center')
           . ' '
           . (isset($instance['background']['top']) ? $instance['background']['top'] : 'top');

  }





  /**
   * Retrieve background position as configured by user
   * or use center top as the default.
   */
  static public function getBackgroundSize($instance) {
    return (isset($instance['background']['width']) ? $instance['background']['width'] : '100%')
        . ' '
        . (isset($instance['background']['height']) ? $instance['background']['height'] : '100%');

  }





  /**
   * Retrieving css block for image elements
   * This is not a replacement for the headline css
   * only the dynamically configured css will be printed,
   * for the rest of the css rules will still use headline.css
   * file.
   */
  static public function getBackgroundCSS($instance) {

    if (isset($instance['background']['image']) && is_numeric($instance['background']['image'])) {
      $instance['background']['image'] = wp_get_attachment_url($instance['background']['image']);
    }

    // Preprocess data to match the css factory needs
    $instance['background']['position'] = self::getBackgroundPosition($instance);
    $instance['background']['size'] = self::getBackgroundSize($instance);

    $css = new VTCore_CSSBuilder_Factory('.headline-background');
    $css->Background($instance['background']);

    return $css->__toString();
  }





  /**
   * Building fontawesome icon
   */
  static public function getItemIcon($data) {

    if (isset($data['icon_color'])) {
      $data['color'] = $data['icon_color'];
    }

    if (isset($data['icon_background'])) {
      $data['background'] = $data['icon_background'];
    }

    $icon = new VTCore_Fontawesome_faIcon($data);
    return $icon->__toString();
  }
}