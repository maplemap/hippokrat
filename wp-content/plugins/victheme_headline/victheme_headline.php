<?php
/*
Plugin Name: VicTheme Headline
Plugin URI: http://victheme.com
Description: Plugin for creating dynamic top headline with image or video
Author: jason.xie@victheme.com
Version: 1.8.7
Author URI: http://victheme.com
*/

define('VTCORE_HEADLINE_CORE_VERSION', '1.7.0');

add_action('plugins_loaded', 'bootVicthemeHeadline', 11);

function bootVicthemeHeadline() {

  if (!(defined('VTCORE_VERSION') && version_compare(VTCORE_VERSION, VTCORE_HEADLINE_CORE_VERSION, '='))) {

    add_action('admin_notices', 'MissingHeadlineCoreNotice');

    function MissingHeadlineCoreNotice() {

      if (!defined('VTCORE_VERSION')) {
        $notice = __('VicTheme Headline depends on VicTheme Core Plugin which is not activated or missing, Please enable it first before VicTheme Headline can work properly.');
      }
      elseif (version_compare(VTCORE_VERSION, VTCORE_HEADLINE_CORE_VERSION, '!=')) {
        $notice = sprintf(__('VicTheme Headline depends on VicTheme Core Plugin API version %s to operate properly.', 'victheme_headline'), VTCORE_HEADLINE_CORE_VERSION);
      }

      if (isset($notice)) {
        echo '<div class="error""><p>' . $notice . '</p></div>';
      }

    }

    return;
  }

  define('VTCORE_HEADLINE_LOADED', true);
  define('VTCORE_HEADLINE_BASE_DIR', dirname(__FILE__));

  include_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'init.php');
  new VTCore_Headline_Init();

}