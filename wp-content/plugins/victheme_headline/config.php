<?php
/**
 * Main Class for handling Headline default value
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Headline_Config
extends VTCore_Wordpress_Models_Config {

  /**
   * Child class must extend this method
   * to register their object information
   *
   * @param array $options
   * @return VTCore_Wordpress_Config_Base
   */
  protected function register(array $options) {

    // Define the database name
    $this->database = 'vtcore_headline_config';

    // Set the default options
    $this->options = array(
      'blog' => array(
        'general' => array(
          'title' => __('Blogs', 'victheme_headline'),
          'subtitle' => __('Browse latest blog entry', 'victheme_headline'),
          'enable' => true,
          'headline_columns' => array(
            'mobile' => '12',
            'tablet' => '6',
            'small' => '6',
            'large' => '6',
          ),
          'item_columns' => array(
            'mobile' => '0',
            'tablet' => '6',
            'small' => '6',
            'large' => '6',
          ),
        ),
      ),
      'archive' => array(
        'general' => array(
          'title' => __('Archive', 'victheme_headline'),
          'subtitle' => __('Displaying archived content', 'victheme_headline'),
          'enable' => true,
          'headline_columns' => array(
            'mobile' => '12',
            'tablet' => '6',
            'small' => '6',
            'large' => '6',
          ),
          'item_columns' => array(
            'mobile' => '0',
            'tablet' => '6',
            'small' => '6',
            'large' => '6',
          ),
        ),
      ),
      'category' => array(
        'general' => array(
          'title' => __('Category', 'victheme_headline'),
          'subtitle' => __('Displaying categories', 'victheme_headline'),
          'enable' => true,
          'headline_columns' => array(
            'mobile' => '12',
            'tablet' => '6',
            'small' => '6',
            'large' => '6',
          ),
          'item_columns' => array(
            'mobile' => '0',
            'tablet' => '6',
            'small' => '6',
            'large' => '6',
          ),
        ),
      ),
      'search' => array(
        'general' => array(
          'title' => __('Search', 'victheme_headline'),
          'subtitle' => __('Displaying search results', 'victheme_headline'),
          'enable' => true,
          'headline_columns' => array(
            'mobile' => '12',
            'tablet' => '6',
            'small' => '6',
            'large' => '6',
          ),
          'item_columns' => array(
            'mobile' => '0',
            'tablet' => '6',
            'small' => '6',
            'large' => '6',
          ),
        ),
      ),
      'author' => array(
        'general' => array(
          'title' => __('Author', 'victheme_headline'),
          'subtitle' => __('Displaying content made by author', 'victheme_headline'),
          'enable' => true,
          'headline_columns' => array(
            'mobile' => '12',
            'tablet' => '6',
            'small' => '6',
            'large' => '6',
          ),
          'item_columns' => array(
            'mobile' => '0',
            'tablet' => '6',
            'small' => '6',
            'large' => '6',
          ),
        ),
      ),

      'tags' => array(
        'general' => array(
          'title' => __('Tags', 'victheme_headline'),
          'subtitle' => __('Displaying tags', 'victheme_headline'),
          'enable' => true,
          'headline_columns' => array(
            'mobile' => '12',
            'tablet' => '6',
            'small' => '6',
            'large' => '6',
          ),
          'item_columns' => array(
            'mobile' => '0',
            'tablet' => '6',
            'small' => '6',
            'large' => '6',
          ),
        ),
      ),
      'notfound' => array(
        'general' => array(
          'title' => __('Error 404 Page', 'victheme_headline'),
          'subtitle' => __('Could not found the page you are looking for.', 'victheme_headline'),
          'enable' => true,
          'headline_columns' => array(
            'mobile' => '12',
            'tablet' => '6',
            'small' => '6',
            'large' => '6',
          ),
          'item_columns' => array(
            'mobile' => '0',
            'tablet' => '6',
            'small' => '6',
            'large' => '6',
          ),
        ),
      ),
    );

    // Set the hookable filter name
    $this->filter = 'vtcore_headline_configuration_context_alter';

    // Apply the hookable filter
    $this->filter();

    // Inject from database
    $this->load();

    // Merge the user supplied options
    $this->merge($options);

    return $this;
  }
}