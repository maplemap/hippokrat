<?php
/**
 * Booting up the headline related classes.
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Headline_Init {

  private $autoloader;
  private $actions = array(
    'admin_menu',
    'save_post',
    'add_meta_boxes',
    'wp_before_admin_bar_render',
  );

  private $filters = array();


  public function __construct() {

    // Booting autoloader
    $this->autoloader = new VTCore_Autoloader('VTCore_Headline', dirname(__FILE__));
    $this->autoloader->setRemovePath('vtcore' . DIRECTORY_SEPARATOR . 'headline' . DIRECTORY_SEPARATOR);
    $this->autoloader->register();

    // Autoload translation for headline
    load_plugin_textdomain('victheme_headline', false, 'victheme_headline/languages');

    // Registering assets
    VTCore_Wordpress_Init::getFactory('assets')
      ->get('library')
      ->detect(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets', plugins_url('', __FILE__) . '/assets');

    // Registering actions
    VTCore_Wordpress_Init::getFactory('filters')
      ->addPrefix('VTCore_Headline_Filters_')
      ->addHooks($this->filters)
      ->register();


    // Registering filters
    VTCore_Wordpress_Init::getFactory('actions')
      ->addPrefix('VTCore_Headline_Actions_')
      ->addHooks($this->actions)
      ->register();


    // Hooking to WPML
    if (defined('ICL_SITEPRESS_VERSION')) {
      VTCore_Wordpress_Init::getFactory('wpml')
        ->add('vtcore_headline_config', array(
          'type' => 'plugin',
          'atid' => 'victheme_headline',
          'map' => $this->buildTranslationArrayMap(),
        ));
    }
  }


  /**
   * Helper method for building the translation map for headline
   * @return array
   */
  private function buildTranslationArrayMap() {
    $config = new VTCore_Headline_Config();
    $panels = $config->extract();
    $panels = array_keys($panels);

    $mapKeys = apply_filters('vtcore_headline_translation_map', array(
      'general' => array(
        'title' => true,
        'subtitle' => true,
      ),
    ));

    $maps = array();
    foreach ($panels as $key) {
      $maps[$key] = $mapKeys;
    }

    return $maps;
  }

}