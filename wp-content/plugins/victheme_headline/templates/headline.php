<?php
/**
 * Headline templates
 *
 * Building the custom headline that can be configured via post page.
 *
 * Themes that wishes to implement the Headline element
 * must copy this template to the theme folder and
 * call it as normal template would be eg. using get_template_part()
 * or get_sidebar().
 *
 * @author jason.xie@victheme.com
 */
?>
<?php
  // Don't Proceed any further if no VicTheme Headline plugin is enabled.
  if (!defined('VTCORE_HEADLINE_LOADED')) {
    return;
  }

  // Booting headline
  $instance = VTCore_Headline_Utility::getHeadline();

  if (!isset($instance['background'])) {
    $instance['background'] = false;
  }

  // Non empty array will throw false true.
  if (isset($instance['background']['video'])) {
    $instance['background']['video'] = array_filter($instance['background']['video']);
  }

  // User disabled the headline, bailing out!.
  if (!VTCore_Headline_Utility::checkEnabled('general', $instance)) {
    return;
  }

  $inlineStyle = VTCore_Headline_Utility::getMaskingCSS($instance) . VTCore_Headline_Utility::getBackgroundCSS($instance);


  // Load VTCore Assets used by this template.
  VTCore_Wordpress_Utility::loadAsset('jquery-verticalcenter');
  VTCore_Wordpress_Utility::loadAsset('css-animation');

  // Load Headline assets needed for this template to work properly.
  VTCore_Wordpress_Utility::loadAsset('jquery-videobg');
  VTCore_Wordpress_Utility::loadAsset('headline-front');

  if (isset($instance['background']['parallax']) && $instance['background']['parallax'] != 'none') {
    VTCore_Wordpress_Utility::loadAsset('jquery-parallax');
  }

?>

<header id="headline" class="animated in startblank fadeIn vertical-center">

  <?php if (!empty($inlineStyle)) : ?>
  <style type="text/css">
    <?php echo $inlineStyle; ?>
  </style>
  <?php endif;?>


  <?php if (VTCore_Headline_Utility::checkEnabled('masking', $instance)) : ?>
    <div class="headline-mask"></div>
  <?php endif;?>



  <?php if (isset($instance['background']['video']) && !empty($instance['background']['video'])) : ?>

  <!-- Video Background -->
  <div class="headline-background"
       data-mode="video-background-ng"
       data-settings="<?php echo esc_attr(json_encode($instance['background']['video'])); ?>"></div>
  <?php endif;?>


  <?php if (!isset($instance['background']['video']) || empty($instance['background']['video'])) : ?>
  <div class="headline-background <?php if (isset($instance['background']['parallax']) && $instance['background']['parallax'] != 'none') echo $instance['background']['parallax'];?>"></div>
  <?php endif;?>

  <div class="area">
  <div class="container-fluid clearfix">
		<div class="row">
			<div class="headline-left vertical-target animated in startblank fadeInLeft <?php echo VTCore_Headline_Utility::getHeadlineColumn($instance);?>"
			     data-vertical-offset="0">

				<?php if (isset($instance['general']['title'])) : ?>
				<h1 class="headline-title"
				    <?php if (isset($instance['general']['title_color'])
				              && !empty($instance['general']['title_color'])) : ?>
				      style="color:<?php echo $instance['general']['title_color'];?>"
				    <?php endif;?>
				>

				    <?php echo $instance['general']['title'];?>

  			  <?php if (isset($instance['general']['subtitle'])) : ?>
  			  <small class="headline-subtitle"
  			     <?php if (isset($instance['general']['subtitle_color'])
  			               && !empty($instance['general']['subtitle_color'])) : ?>
  			       style="color:<?php echo $instance['general']['subtitle_color'];?>"
  			     <?php endif;?>
  			  >

  			    <?php echo $instance['general']['subtitle'];?>
  			  </small>
  			  <?php endif;?>

			  </h1>
				<?php endif;?>
			</div>

			<?php if (isset($instance['items'])) : ?>
		  <div class="headline-right vertical-target animated in startblank fadeInRight <?php echo VTCore_Headline_Utility::getItemColumn($instance);?>"
		       data-vertical-offset="0">

		    <?php foreach ($instance['items'] as $delta => $data) : ?>

              <h2
                class="headline-title inline headline-elements-<?php echo $delta; ?>">

                <?php if (isset($data['icon']) && $data['icon'] != 'none') : ?>
                  <?php echo VTCore_Headline_Utility::getItemIcon($data); ?>
                <?php endif ?>

                <?php if (isset($data['title'])) : ?>
                  <span class="headline-item-title"
                    <?php if (isset($data['title_color'])) : ?>
                      style="color: <?php echo $data['title_color']; ?>"
                    <?php endif; ?>>

		            <?php echo $data['title']; ?>
		        </span>
                <?php endif; ?>

                <?php if (isset($data['text'])) : ?>
                  <small class="headline-item-text"
                    <?php if (isset($data['text_color'])) : ?>
                      style="color: <?php echo $data['text_color']; ?>"
                    <?php endif; ?>>

                    <?php echo $data['text']; ?>
                  </small>
                <?php endif; ?>

              </h2>

            <?php endforeach; ?>
			</div>
			<?php endif;?>
    </div>
		</div>
	</div>
</header>