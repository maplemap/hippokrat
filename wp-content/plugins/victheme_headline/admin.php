<?php
/**
 * Class for handling Headline administration pages
 *
 * The Form relies on VTCore Form classes for building
 * the value based on $_POST or default value from
 * database.
 *
 * The stored data is under vtcore_headline_config using
 * WordPress options table.
 *
 * This form is heavily related to the headline metabox,
 * altering the metabox will alters this form too.
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Headline_Admin {

  private $metaboxClassPrefix = 'VTCore_Headline_Metabox_';
  private $form;
  private $header;
  private $messages;
  private $context;
  private $panels = array(
    'blog',
    'archive',
    'category',
    'search',
    'author',
    'tags',
    'notfound',
  );

  private $tabsPanels = array(
    'general',
    'background',
    'masking',
    'items',
    'columns',
  );


  /**
   * Method for adding new panel to the panel
   * registry system
   *
   * @param string $panel panel machine name
   */
  public function addPanel($panel) {
    $this->panels[] = $panel;
  }





  /**
   * Public function for building and retrieving context
   * the context will be processed and merged from database.
   */
  public function getContext() {
    $this->processContext();
    return $this->context;
  }



  /**
   * Page callbacks
   * @see add_submenu_page()
   * @see VTCore_Headline_Actions_Admin__Menu
   */
  public function buildPage() {

    if (is_admin() && isset($_GET['page']) && $_GET['page'] == 'headline-main') {
      VTCore_Wordpress_Utility::loadAsset('wp-bootstrap');
      VTCore_Wordpress_Utility::loadAsset('bootstrap-confirmation');
      VTCore_Wordpress_Utility::loadAsset('headline-admin-page');
    }

    $this->messages = new VTCore_Bootstrap_BsMessages();

    // Allow other plugin to add extra configuration panel
    do_action('vtcore_headline_add_configuration_panel', $this);

    // Reset Database
    if (isset($_POST['headlineResetSubmit'])) {
      $this->messages->setNotice(__('Configuration deleted from database and retrieving the default configuration value.', 'victheme_headline'));
      delete_option('vtcore_headline_config');

      VTCore_Wordpress_Init::getFactory('assets')
        ->mutate('prefix', 'comp-front-')
        ->clearCache()
        ->mutate('prefix', 'comp-admin-');
    }

    $this->processContext();


    $this->buildHeader();
    $this->header->render();

    $this->buildForm();


    // Grab any errors, errors is built from the loop in buildForm() method
    $errors = $this->messages->getError();


    // Save the form
    if (empty($errors) && isset($_POST['headlineSaveSubmit'])) {
      $this->messages->setNotice(__('Configuration saved to database', 'victheme_headline'));
      unset($_POST['headlineSaveSubmit']);
      update_option('vtcore_headline_config', wp_unslash($_POST));

      VTCore_Wordpress_Init::getFactory('assets')
        ->mutate('prefix', 'comp-front-')
        ->clearCache()
        ->mutate('prefix', 'comp-admin-');
    }

    // Add messages
    $this->form->prependChild($this->messages->render());

    $this->form->render();

  }



  /**
   * Build the form elements
   */
  private function buildForm() {

    // Build the form
    $this->form = new VTCore_Bootstrap_Form_BsInstance(array(
      'attributes' => array(
        'id' => 'headline-configuration-form',
        'method' => 'post',
        'action' => $_SERVER['REQUEST_URI'],
        'class' => array('container-fluid'),
        'autocomplete' => 'off',
      ),
    ));

    foreach ($this->panels as $panel) {

      if (isset($this->context[$panel])) {
        $context = $this->context[$panel];
      }

      $context['prefix'] = $panel;



      $tabs = new VTCore_Bootstrap_Element_BsTabs(array(
        'prefix' => 'headline-tabs-' . $panel,
      ));

      $panelObject = $this->form
      ->BsPanel(array(
        'text' => sprintf(__('%s Page Options', 'victheme_headline'), str_replace(array('-', '_'), ' ', ucfirst($panel))),
      ))
      ->lastChild()
      ->addContent($tabs);

      $skip = apply_filters('vtcore_headline_remove_metabox_tabs', array());


      // Build inner tabs
      foreach ($this->tabsPanels as $name) {

        $class = $this->metaboxClassPrefix . ucfirst($name);

        if (class_exists($class, true) && !in_array($name, $skip)) {
          $metabox = new $class((array) $context);
          $metabox = apply_filters('vtcore_headline_metabox_form_alter', $metabox);

          $form = new VTCore_Bootstrap_Form_BsInstance();
          $form->addChildren($metabox);

          if (!isset($_POST['headlineResetSubmit'])) {
            $form
              ->processForm()
              ->processError(true, true);
          }

          // Grab any errors
          $errors = $form->getErrors();

          // Show error
          if (!empty($errors)) {
            foreach ($errors as $error) {
              $this->messages->setError($error);
            }
          }

          $form = NULL;
          unset($form);

          $tabs->addHeader($metabox->getContext('header'));
          $tabs->addContent($metabox->__toString());
          $tabs->setDelta();

          unset($metabox);
        }

      }

      $tabs->setActiveTabs(0);


      $panelObject
        ->addContent(new VTCore_Form_Submit(array(
          'attributes' => array(
            'name' => 'headlineSaveSubmit',
            'value' => __('Save', 'victheme_headline'),
            'class' => array('btn', 'btn-primary')
          ),
        )));


      // Clean object
      unset($panelObject);
      unset($tabs);
    }



    // Build submit button
    $this->form
      ->Submit(array(
        'attributes' => array(
          'name' => 'headlineSaveSubmit',
          'value' => __('Save', 'victheme_headline'),
          'class' => array('btn', 'btn-primary')
        ),
      ))
      ->BsSubmit(array(
        'attributes' => array(
          'name' => 'headlineResetSubmit',
          'value' => __('Reset', 'victheme_headline'),
        ),
        'mode' => 'danger',
        'confirmation' => true,
        'title' => __('Are you sure?', 'victheme_headline'),
        'ok' => __('Yes', 'victheme_headline'),
        'cancel' => __('No', 'victheme_headline'),
      ));

    return $this->form;
  }





  /**
   * Build the page header
   */
  private function buildHeader() {

    $plugin_data = get_plugin_data(VTCORE_HEADLINE_BASE_DIR . DIRECTORY_SEPARATOR . 'victheme_headline.php');

    $this->header = new VTCore_Bootstrap_Grid_BsContainerFluid(array(
      'type' => 'div',
      'attributes' => array(
        'id' => 'headline-options-header',
      ),
    ));

    $this->header
      ->BsRow()
      ->lastChild()
      ->BsColumn(array(
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 12,
            'small' => 12,
            'large' => 6,
          )
        ),
      ))
      ->lastChild()
      ->addOverloaderPrefix('VTCore_Fontawesome_')
      ->faIcon(array(
        'icon' => 'edit',
        'shape' => 'circle',
        'position' => 'pull-left',
      ))
      ->BsHeader(array(
        'text' => __('Headline Configuration', 'victheme_headline'),
        'small' => sprintf(__('version %s', 'victheme_headline'), $plugin_data['Version']),
      ))
      ->getParent()
      ->BsColumn(array(
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 12,
            'small' => 12,
            'large' => 6,
          )
        ),
      ));

    return $this;
  }







  /**
   * Processing context by grabbing database and invoking filters
   */
  private function processContext() {

    $config = new VTCore_Headline_Config();
    $this->context = $config->extract();

    return $this;
  }

}