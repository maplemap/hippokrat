<?php
/**
 * Class for building Headline columns size
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Headline_Metabox_Columns
extends VTCore_Headline_Metabox_Base
implements VTCore_Form_Interface {

  protected $context = array(
    'header' => 'Columns',
    'prefix' => 'headline',
    'general' => array(
      'headline_columns' => array(
        'mobile' => '12',
        'tablet' => '6',
        'small' => '6',
        'large' => '6',
      ),
      'item_columns' => array(
        'mobile' => '0',
        'tablet' => '6',
        'small' => '6',
        'large' => '6',
      ),
    ),
  );


  /**
   * Overriding parent method
   * @see VTCore_Html_Base::buildElement()
   */
  public function buildElement() {

    $this
      ->BsRow(array(
        'group' => 'column-headline',
      ))
      ->lastChild()
      ->BsColumn(array(
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 4,
            'small' => 4,
            'large' => 4,
          ),
        ),
      ))
      ->lastChild()
      ->BsHeader(array(
        'tag' => 'h3',
        'text' => __('Headline Column Size', 'victheme_headline'),
      ))
      ->BsDescription(array(
        'text' => __('Configure the headline column size, this configuration is related to the headline items column size.
                      Total column size for a single viewport plus the items column size for the same viewport must equal
                      to 12 as bootstrap columns rules requires.', 'victheme_headline'),
      ))
      ->getParent()
      ->BsColumn(array(
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 8,
            'small' => 8,
            'large' => 8,
          ),
        ),
      ))
      ->lastChild()
      ->BsRow();

    foreach ($this->getDefault('general', 'headline_columns') as $key => $value) {
      $this
        ->lastChild()
        ->lastChild()
        ->lastChild()
        ->BsSelect(array(
          'text' => ucfirst($key),
          'name' => $this->getContext('prefix') . '[general][headline_columns][' . $key . ']',
          'options' => range(0, 12, 1),
          'value' => $value,
          'grids' => array(
            'columns' => array(
              'mobile' => '6',
              'tablet' => '6',
              'small' => '3',
              'large' => '3',
            ),
          ),
        ));
    }

    $this
      ->BsRow(array(
        'group' => 'column-items',
      ))
      ->lastChild()
      ->BsColumn(array(
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 4,
            'small' => 4,
            'large' => 4,
          ),
        ),
      ))
      ->lastChild()
      ->BsHeader(array(
        'tag' => 'h3',
        'text' => __('Items Column Size', 'victheme_headline'),
      ))
      ->BsDescription(array(
        'text' => __('Configure the items column size, this configuration is related to the headline column size.
                        Total column size for a single viewport plus the headline column size for the same viewport must equal
                        to 12 as bootstrap columns rules requires.', 'victheme_headline'),
      ))
      ->getParent()
      ->BsColumn(array(
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 8,
            'small' => 8,
            'large' => 8,
          ),
        ),
      ))
      ->lastChild()
      ->BsRow();

    foreach ($this->getDefault('general', 'item_columns') as $key => $value) {
      $this
        ->lastChild()
        ->lastChild()
        ->lastChild()
        ->BsSelect(array(
          'text' => ucfirst($key),
          'name' => $this->getContext('prefix') . '[general][item_columns][' . $key . ']',
          'options' => range(0, 12, 1),
          'value' => $value,
          'grids' => array(
            'columns' => array(
              'mobile' => '6',
              'tablet' => '6',
              'small' => '3',
              'large' => '3',
            ),
          ),
        ));
    }
  }
}