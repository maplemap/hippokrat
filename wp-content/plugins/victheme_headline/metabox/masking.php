<?php
/**
 * Class for building Headline masking tabs
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Headline_Metabox_Masking
extends VTCore_Headline_Metabox_Base
implements VTCore_Form_Interface {

  protected $context = array(
    'header' => 'Masking',
    'prefix' => 'headline',
    'masking' => array(
      'enable' => true,
      'color' => '',
      'gradient' => array(
        'type' => false,
      ),
      'opacity' => '',
    ),
  );


  /**
   * Overriding parent method
   * @see VTCore_Html_Base::buildElement()
   */
  public function buildElement() {

    $this
      ->BsRow(array(
        'group' => 'masking',
      ))
      ->lastChild()
      ->BsColumn(array(
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 4,
            'small' => 4,
            'large' => 4,
          ),
        ),
      ))
      ->lastChild()
      ->BsHeader(array(
        'tag' => 'h3',
        'text' => __('Headline Masking', 'victheme_headline'),
      ))
      ->BsDescription(array(
        'text' => __('Configure if the headline element should build a masking layer, the masking layer can consist of
                      a single flat color element or a CSS3 gradient, by specifying color one for flat color or both color
                      one and color two for gradient.', 'victheme_headline'),
      ))
      ->getParent()
      ->BsColumn(array(
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 8,
            'small' => 8,
            'large' => 8,
          ),
        ),
      ))
      ->lastChild()
      ->BsRow()
      ->lastChild()
      ->BsColumn(array(
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 12,
            'small' => 6,
            'large' => 6,
          ),
        ),
      ))
      ->lastChild()
      ->BsSelect(array(
        'name' => $this->getContext('prefix') . '[masking][enable]',
        'text' => __('Enable Masking', 'victheme_headline'),
        'value' => $this->getDefault('masking', 'enable'),
        'options' => array(
          true => __('Yes', 'victheme_headline'),
          false => __('No', 'victheme_headline'),
        ),
      ))
      ->BsRow()
      ->lastChild()
      ->addOverloaderPrefix('VTCore_Wordpress_Form_')
      ->wpMedia(array(
        'name' => $this->getContext('prefix') . '[masking][image]',
        'text' => __('Image', 'victheme_headline'),
        'value' => $this->getDefault('masking', 'image'),
        'data' => array(
          'type' => 'image',
          'title' => __('Select Image', 'victheme_headline'),
          'button' =>__('Select Image', 'victheme_headline'),
        ),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 12,
            'small' => 12,
            'large' => 12,
          ),
        ),
      ))
      ->getParent()
      ->BsRow()
      ->lastChild()
      ->BsText(array(
        'text' => __('Top Position', 'victheme_headline'),
        'name' => $this->getContext('prefix') . '[masking][top]',
        'value' => $this->getDefault('masking', 'top'),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      ))
      ->BsText(array(
        'text' => __('Left Position', 'victheme_headline'),
        'name' => $this->getContext('prefix') . '[masking][left]',
        'value' => $this->getDefault('masking', 'left'),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      ))
      ->getParent()
      ->BsRow()
      ->lastChild()
      ->BsSelect(array(
        'text' => __('Repeat', 'victheme_headline'),
        'name' => $this->getContext('prefix') . '[masking][repeat]',
        'value' => $this->getDefault('masking', 'repeat'),
        'options' => array(
          'no-repeat' => __('No Repeat', 'victheme_headline'),
          'repeat' => __('Repeat All', 'victheme_headline'),
          'repeat-x' => __('Repeat Horizontaly', 'victheme_headline'),
          'repeat-y' => __('Repeat Verticaly', 'victheme_headline'),
        ),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      ))
      ->BsColor(array(
        'text' => __('Color', 'victheme_headline'),
        'name' => $this->getContext('prefix') . '[masking][color]',
        'value' => $this->getDefault('masking', 'color'),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      ))
      ->getParent()
      ->BsRow()
      ->lastChild()
      ->BsText(array(
        'text' => __('Width', 'victheme_headline'),
        'name' => $this->getContext('prefix') . '[masking][width]',
        'value' => $this->getDefault('masking', 'width'),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      ))
      ->BsText(array(
        'text' => __('Height', 'victheme_headline'),
        'name' => $this->getContext('prefix') . '[masking][height]',
        'value' => $this->getDefault('masking', 'height'),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      ))
      ->getParent()
      ->BsRow()
      ->lastChild()
      ->BsSelect(array(
        'text' => __('Attachment', 'victheme_headline'),
        'name' => $this->getContext('prefix') . '[masking][attachment]',
        'value' => $this->getDefault('masking', 'attachment'),
        'options' => array(
          'scroll' => __('Scroll', 'victheme_headline'),
          'fixed' => __('Fixed', 'victheme_headline'),
          'local' => __('Local', 'victheme_headline'),
          'initial' => __('Initial', 'victheme_headline'),
          'inherit' => __('Inherit', 'victheme_headline'),
        ),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      ))
      ->BsText(array(
        'name' => $this->getContext('prefix') . '[masking][opacity]',
        'text' => __('Opacity', 'victheme_headline'),
        'description' => __('Valid value is from 0 to 1, example 0.5 as 50% transparent.', 'victheme_headline'),
        'value' => $this->getDefault('masking', 'opacity'),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      ))
      ->getParent()
      ->getParent()
      ->BsColumn(array(
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 12,
            'small' => 6,
            'large' => 6,
          ),
        ),
      ))
      ->lastChild()
      ->addOverloaderPrefix('VTCore_Wordpress_Form_')
      ->WpGradient(array(
        'name' => $this->getContext('prefix') . '[masking]',
        'text' => __('Gradient', 'victheme_headline'),
        'description' => __('Create gradient masking with CSS3 gradients.', 'victheme_headline'),
        'value' => $this->getDefault('masking', 'gradient'),
      ));

  }


}