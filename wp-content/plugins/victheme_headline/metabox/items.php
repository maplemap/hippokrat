<?php
/**
 * Class for building Headline general tabs
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Headline_Metabox_Items
extends VTCore_Headline_Metabox_Base
implements VTCore_Form_Interface {

  protected $context = array(
    'header' => 'Items',
    'prefix' => 'headline',
    'items' => array(
      array(
        'icon' => '',
        'border' => '',
        'flip' => '',
        'position' => '',
        'rotate' => '',
        'shape' => '',
        'size' => '',
        'spin' => '',
        'title' => '',
        'text' => '',
        'icon_color' => '',
        'title_color' => '',
        'text_color' => '',
        'icon_background' => '',
      ),
    ),
  );

  private $rows = array();


  /**
   * Overriding parent method
   * @see VTCore_Html_Base::buildElement()
   */
  public function buildElement() {

    VTCore_Wordpress_Utility::loadAsset('jquery-table-manager');

    $this->buildRows();

    $this
      ->BsRow()
      ->lastChild()
      ->BsColumn(array(
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 4,
            'small' => 4,
            'large' => 4,
          ),
        ),
      ))
      ->lastChild()
      ->BsHeader(array(
        'tag' => 'h3',
        'text' => __('Headline Items', 'victheme_headline'),
      ))
      ->BsDescription(array(
        'text' => __('Configure the text items on the Headline Element, Additional configuration for the
                      Grid columns can be found in the Headline configuration page.', 'victheme_headline'),
      ))
      ->getParent()
      ->BsColumn(array(
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 8,
            'small' => 8,
            'large' => 8,
          ),
        ),
      ))
      ->lastChild()
      ->BsElement(array(
        'type' => 'div',
        'attributes' => array(
          'class' => array('table-manager'),
        ),
      ))
      ->lastChild()
      ->Table(array(
        'headers' => array(
          '',
          __('Content', 'victheme_headline'),
          __('Color', 'victheme_headline'),
          '',
        ),
        'rows' => $this->rows,
      ))
      ->Button(array(
        'text' => __('Add New Entry', 'victheme_headline'),
        'attributes' => array(
          'data-tablemanager-type' => 'addrow',
          'class' => array('button', 'button-large', 'button-primary'),
        ),
      ));


    // Free up memory
    $this->rows = array();
    unset($this->rows);
  }



  private function buildRows() {

    $defaults = array(
      'icon' => '',
      'border' => '',
      'flip' => '',
      'position' => '',
      'rotate' => '',
      'shape' => '',
      'size' => '',
      'spin' => '',
      'title' => '',
      'text' => '',
      'icon_color' => '',
      'title_color' => '',
      'text_color' => '',
      'icon_background' => '',
    );

    foreach ($this->getContext('items') as $key => $data) {

      $data = wp_parse_args($data, $defaults);

      // Draggable Icon
      $this->rows[$key][] = array(
        'content' => new VTCore_Bootstrap_Element_BsElement(array(
          'type' => 'span',
          'attributes' => array(
            'class' => array('drag-icon'),
          ),
        )),
        'attributes' => array(
          'class' => array('drag-element'),
        ),
      );



      // Icons & content
      $this->rows[$key]['icon'] = new VTCore_Headline_Metabox_Base();
      $this->rows[$key]['icon']
        ->BsElement(array(
          'type' => false,
          'group' => 'title',
        ))
        ->lastChild()
        ->BsText(array(
          'text' => __('Title', 'victheme_headline'),
          'name' => $this->getContext('prefix') . '[items][' . $key . '][title]',
          'value' => $data['title'],
        ))
        ->BsTextarea(array(
          'text' => __('Text', 'victheme_headline'),
          'name' => $this->getContext('prefix') . '[items][' . $key . '][text]',
          'value' => $data['text'],
          'raw' => true,
        ))
        ->getParent()
        ->BsRow(array(
          'group' => 'fontawesome',
        ))
        ->lastChild()
        ->addOverloaderPrefix('VTCore_Fontawesome_Form_')
        ->faIcon(array(
          'text' => __('Icon', 'victheme_headline'),
          'name' => $this->getContext('prefix') . '[items][' . $key . '][icon]',
          'value' => $data['icon'],
          'grids' => array(
            'columns' => array(
              'mobile' => '12',
              'tablet' => '4',
              'small' => '4',
              'large' => '4',
            ),
          ),
        ))
        ->faBorder(array(
          'text' => __('Border', 'victheme_headline'),
          'name' => $this->getContext('prefix') . '[items][' . $key . '][border]',
          'value' => $data['border'],
          'grids' => array(
            'columns' => array(
              'mobile' => '12',
              'tablet' => '4',
              'small' => '4',
              'large' => '4',
            ),
          ),
        ))
        ->faFlip(array(
          'text' => __('Flip', 'victheme_headline'),
          'name' => $this->getContext('prefix') . '[items][' . $key . '][flip]',
          'value' => $data['flip'],
          'grids' => array(
            'columns' => array(
              'mobile' => '12',
              'tablet' => '4',
              'small' => '4',
              'large' => '4',
            ),
          ),
        ))
        ->getParent()
        ->BsRow(array(
          'group' => 'fontawesome',
        ))
        ->lastChild()
        ->addOverloaderPrefix('VTCore_Fontawesome_Form_')
        ->faPosition(array(
          'text' => __('Position', 'victheme_headline'),
          'name' => $this->getContext('prefix') . '[items][' . $key . '][position]',
          'value' => $data['position'],
          'grids' => array(
            'columns' => array(
              'mobile' => '12',
              'tablet' => '4',
              'small' => '4',
              'large' => '4',
            ),
          ),
        ))
        ->faRotate(array(
          'text' => __('Rotate', 'victheme_headline'),
          'name' => $this->getContext('prefix') . '[items][' . $key . '][rotate]',
          'value' => $data['rotate'],
          'grids' => array(
            'columns' => array(
              'mobile' => '12',
              'tablet' => '4',
              'small' => '4',
              'large' => '4',
            ),
          ),
        ))
        ->faShape(array(
          'text' => __('Shape', 'victheme_headline'),
          'name' => $this->getContext('prefix') . '[items][' . $key . '][shape]',
          'value' => $data['shape'],
          'grids' => array(
            'columns' => array(
              'mobile' => '12',
              'tablet' => '4',
              'small' => '4',
              'large' => '4',
            ),
          ),
        ))
        ->getParent()
        ->BsRow(array(
          'group' => 'fontawesome',
        ))
        ->lastChild()
        ->addOverloaderPrefix('VTCore_Fontawesome_Form_')
        ->faSize(array(
          'text' => __('Size', 'victheme_headline'),
          'name' => $this->getContext('prefix') . '[items][' . $key . '][size]',
          'value' => $data['size'],
          'grids' => array(
            'columns' => array(
              'mobile' => '12',
              'tablet' => '4',
              'small' => '4',
              'large' => '4',
            ),
          ),
        ))
        ->faBorder(array(
          'text' => __('Spin', 'victheme_headline'),
          'name' => $this->getContext('prefix') . '[items][' . $key . '][spin]',
          'value' => $data['spin'],
          'grids' => array(
            'columns' => array(
              'mobile' => '12',
              'tablet' => '4',
              'small' => '4',
              'large' => '4',
            ),
          ),
        ));

      $this->rows[$key]['colors'] = new VTCore_Bootstrap_Element_Base();
      $this->rows[$key]['colors']
        ->BsElement(array(
          'type' => false,
          'group' => 'text-color',
        ))
        ->lastChild()
        ->BsColor(array(
          'name' => $this->getContext('prefix') . '[items][' . $key . '][title_color]',
          'text' => __('Title Color', 'victheme_headline'),
          'value' => $data['title_color'],
        ))
        ->BsColor(array(
          'name' => $this->getContext('prefix') . '[items][' . $key . '][text_color]',
          'text' => __('Text Color', 'victheme_headline'),
          'value' => $data['text_color'],
        ))
        ->getParent()
        ->BsElement(array(
          'type' => false,
          'group' => 'fontawesome-color',
        ))
        ->lastChild()
        ->BsColor(array(
          'name' => $this->getContext('prefix') . '[items][' . $key . '][icon_color]',
          'text' => __('Icon Color', 'victheme_headline'),
          'value' => $data['icon_color'],
        ))
        ->BsColor(array(
          'name' => $this->getContext('prefix') . '[items][' . $key . '][icon_background]',
          'text' => __('Icon Background', 'victheme_headline'),
          'value' => $data['icon_background'],
        ));


      // Remove button
      $this->rows[$key][] = new VTCore_Form_Button(array(
        'text' => 'X',
        'attributes' => array(
          'data-tablemanager-type' => 'removerow',
          'class' => array('button', 'button-mini', 'form-button'),
        ),
      ));

    }
  }

}