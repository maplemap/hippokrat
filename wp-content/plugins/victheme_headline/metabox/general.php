<?php
/**
 * Class for building Headline general tabs
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Headline_Metabox_General
extends VTCore_Headline_Metabox_Base
implements VTCore_Form_Interface {

  protected $context = array(
    'header' => 'General',
    'prefix' => 'headline',
    'general' => array(
      'enable' => true,
      'title' => '',
      'headline_tag' => 'h1',
      'title_color' => '',
      'subtitle' => '',
      'subtitle_tag' => 'h2',
      'subtitle_color' => '',
    ),
  );


  /**
   * Overriding parent method
   * @see VTCore_Html_Base::buildElement()
   */
  public function buildElement() {

    $this
      ->BsRow(array(
        'group' => 'general',
      ))
      ->lastChild()
      ->BsColumn(array(
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 4,
            'small' => 4,
            'large' => 4,
          ),
        ),
      ))
      ->lastChild()
      ->BsHeader(array(
        'tag' => 'h3',
        'text' => __('General Configuration', 'victheme_headline'),
      ))
      ->BsDescription(array(
        'text' => __('Set the Title and Subtitle to be displayed on the Headline element,
                      The Column size of this element can be configured on the main Headline configuration page', 'victheme_headline'),
      ))
      ->getParent()
      ->BsColumn(array(
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 8,
            'small' => 8,
            'large' => 8,
          ),
        ),
      ))
      ->lastChild()
      ->BsSelect(array(
        'name' => $this->getContext('prefix') . '[general][enable]',
        'text' => __('Enable Headline', 'victheme_headline'),
        'value' => $this->getDefault('general', 'enable'),
        'options' => array(
          true => __('Yes', 'victheme_headline'),
          false => __('No', 'victheme_headline'),
        ),
      ));


    $this
      ->BsRow(array(
        'group' => 'title',
      ))
      ->lastChild()
      ->BsColumn(array(
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 4,
            'small' => 4,
            'large' => 4,
          ),
        ),
      ))
      ->lastChild()
      ->BsHeader(array(
        'tag' => 'h3',
        'text' => __('Headline Title', 'victheme_headline'),
      ))
      ->BsDescription(array(
        'text' => __('Configure the main headline element tag, content and text color', 'victheme_headline'),
      ))
      ->getParent()
      ->BsColumn(array(
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 8,
            'small' => 8,
            'large' => 8,
          ),
        ),
      ))
      ->lastChild()
      ->BsRow()
      ->lastChild()
      ->BsSelect(array(
        'name' => $this->getContext('prefix') . '[general][title_tag]',
        'text' => __('Tag', 'victheme_headline'),
        'value' => $this->getDefault('general', 'title_tag'),
        'options' => array(
          'h1' => __('H1', 'victheme_headline'),
          'h2' => __('H2', 'victheme_headline'),
          'h3' => __('H3', 'victheme_headline'),
          'h4' => __('H4', 'victheme_headline'),
          'h5' => __('H5', 'victheme_headline'),
          'h6' => __('H6', 'victheme_headline'),
          'p' => __('p', 'victheme_headline'),
        ),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 3,
            'small' => 3,
            'large' => 3,
          ),
        ),
      ))
      ->BsText(array(
        'name' => $this->getContext('prefix') . '[general][title]',
        'text' => __('Title', 'victheme_headline'),
        'value' => $this->getDefault('general', 'title'),
        'description' => __('The main headline title.', 'victheme_headline'),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      ))
      ->BsColor(array(
        'name' => $this->getContext('prefix') . '[general][title_color]',
        'text' => __(' Color', 'victheme_headline'),
        'value' => $this->getDefault('general', 'title_color'),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 3,
            'small' => 3,
            'large' => 3,
          ),
        ),
      ));


    $this
      ->BsRow(array(
        'group' => 'subtitle',
      ))
      ->lastChild()
      ->BsColumn(array(
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 4,
            'small' => 4,
            'large' => 4,
          ),
        ),
      ))
      ->lastChild()
      ->BsHeader(array(
        'tag' => 'h3',
        'text' => __('Headline Sub Title', 'victheme_headline'),
      ))
      ->BsDescription(array(
        'text' => __('Configure the headline sub title element tag, content and text color', 'victheme_headline'),
      ))
      ->getParent()
      ->BsColumn(array(
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 8,
            'small' => 8,
            'large' => 8,
          ),
        ),
      ))
      ->lastChild()
      ->BsRow()
      ->lastChild()
      ->BsSelect(array(
        'name' => $this->getContext('prefix') . '[general][subtitle_tag]',
        'text' => __('Subtitle Tag', 'victheme_headline'),
        'value' => $this->getDefault('general', 'subtitle_tag'),
        'options' => array(
          'h1' => __('H1', 'victheme_headline'),
          'h2' => __('H2', 'victheme_headline'),
          'h3' => __('H3', 'victheme_headline'),
          'h4' => __('H4', 'victheme_headline'),
          'h5' => __('H5', 'victheme_headline'),
          'h6' => __('H6', 'victheme_headline'),
          'p' => __('p', 'victheme_headline'),
        ),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 3,
            'small' => 3,
            'large' => 3,
          ),
        ),
      ))
      ->BsText(array(
        'name' => $this->getContext('prefix') . '[general][subtitle]',
        'text' => __('SubTitle', 'victheme_headline'),
        'value' => $this->getDefault('general', 'subtitle'),
        'description' => __('The headline subtitle underneath the main title.', 'victheme_headline'),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      ))
      ->BsColor(array(
        'name' => $this->getContext('prefix') . '[general][subtitle_color]',
        'text' => __('SubTitle Color', 'victheme_headline'),
        'value' => $this->getDefault('general', 'subtitle_color'),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 3,
            'small' => 3,
            'large' => 3,
          ),
        ),
      ));

  }


}