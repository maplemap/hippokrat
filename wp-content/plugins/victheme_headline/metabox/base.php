<?php
/**
 * Portfolio Metabox Base super class
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Headline_Metabox_Base
extends VTCore_Bootstrap_Form_Base {

  protected $overloaderPrefix = array(
    'VTCore_Wordpress_Form_',
    'VTCore_Wordpress_',
    'VTCore_Fontawesome_Form_',
    'VTCore_Fontawesome_',
    'VTCore_Bootstrap_Form_',
    'VTCore_Bootstrap_Element_',
    'VTCore_Bootstrap_Grid_',
    'VTCore_Form_',
    'VTCore_Html_',
  );


  protected function getDefault($key, $type) {
    return isset($this->context[$key][$type]) ? $this->context[$key][$type] : NULL;
  }

}