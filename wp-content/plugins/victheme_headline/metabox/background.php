<?php
/**
 * Class for building Headline background tabs
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Headline_Metabox_Background
extends VTCore_Headline_Metabox_Base
implements VTCore_Form_Interface {

  protected $context = array(
    'header' => 'Background',
    'prefix' => 'headline',
    'background' => array(
      'top' => 'top',
      'left' => 'right',
      'color' => '',
      'image' => '',
      'video' => array(
        'mp4' => '',
        'ogv' => '',
        'webm' => '',
        'autoplay' => false,
        'loop' => false,
        'scale' => true,
        'fullscreen' => true,
      ),
      'width' => '',
      'height' => '',
      'attachment' => 'scroll',
      'parallax' => 'none',
    ),
  );


  /**
   * Overriding parent method
   * @see VTCore_Html_Base::buildElement()
   */
  public function buildElement() {

    $video = $this->getDefault('background', 'video');

    $this
      ->BsRow(array(
        'group' => 'background',
      ))
      ->lastChild()
      ->BsColumn(array(
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 4,
            'small' => 4,
            'large' => 4,
          ),
        ),
      ))
      ->lastChild()
      ->BsHeader(array(
        'tag' => 'h3',
        'text' => __('Headline Background', 'victheme_headline'),
      ))
      ->BsDescription(array(
        'text' => __('Add The background image or video for the headline element. If Video is configured then the image will be served as the
                      fallback in case the client browser is incapable to display HTML5 Video. The Video Format is served in following order :
                      MP4, OGV and WEBM depending on the browser capability to display the video format.', 'victheme_headline'),
      ))
      ->getParent()
      ->BsColumn(array(
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 8,
            'small' => 8,
            'large' => 8,
          ),
        ),
      ))
      ->lastChild()
      ->BsRow()
      ->lastChild()
      ->addOverloaderPrefix('VTCore_Wordpress_Form_')
      ->wpMedia(array(
        'name' => $this->getContext('prefix') . '[background][image]',
        'text' => __('Image', 'victheme_headline'),
        'value' => $this->getDefault('background', 'image'),
        'data' => array(
          'type' => 'image',
          'title' => __('Select Image', 'victheme_headline'),
          'button' =>__('Select Image', 'victheme_headline'),
        ),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 6,
            'small' => 4,
            'large' => 3,
          ),
        ),
      ))
      ->wpMedia(array(
        'name' => $this->getContext('prefix') . '[background][video][mp4]',
        'text' => __('Video MP4 Format', 'victheme_headline'),
        'value' => $video['mp4'],
        'data' => array(
          'type' => 'video',
          'title' => __('Select MP4 Video', 'victheme_headline'),
          'button' =>__('Select Video', 'victheme_headline'),
        ),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 6,
            'small' => 4,
            'large' => 3,
          ),
        ),
      ))
      ->wpMedia(array(
        'name' => $this->getContext('prefix') . '[background][video][ogv]',
        'text' => __('Video OGV Format', 'victheme_headline'),
        'value' => $video['ogv'],
        'data' => array(
          'type' => 'video',
          'title' => __('Select OGV Video', 'victheme_headline'),
          'button' =>__('Select Video', 'victheme_headline'),
        ),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 6,
            'small' => 4,
            'large' => 3,
          ),
        ),
      ))
      ->wpMedia(array(
        'name' => $this->getContext('prefix') . '[background][video][webm]',
        'text' => __('Video WEBM Format', 'victheme_headline'),
        'value' => $video['webm'],
        'data' => array(
          'type' => 'video',
          'title' => __('Select WEBM Video', 'victheme_headline'),
          'button' =>__('Select video', 'victheme_headline'),
        ),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 6,
            'small' => 4,
            'large' => 3,
          ),
        ),
      ));

    $this
      ->BsRow(array(
        'group' => 'background-css',
      ))
      ->lastChild()
      ->BsColumn(array(
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 4,
            'small' => 4,
            'large' => 4,
          ),
        ),
      ))
      ->lastChild()
      ->BsHeader(array(
        'tag' => 'h3',
        'text' => __('Background CSS Adjustment', 'victheme_headline'),
      ))
      ->BsDescription(array(
        'text' => __('Fine tune the background CSS positioning and background color, this settings will be
                      applied to Video Element too, especially the positioning adjustment', 'victheme_headline'),
      ))
      ->getParent()
      ->BsColumn(array(
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 8,
            'small' => 8,
            'large' => 8,
          ),
        ),
      ))
      ->lastChild()
      ->BsRow()
      ->lastChild()
      ->BsText(array(
        'text' => __('Top', 'victheme_headline'),
        'name' => $this->getContext('prefix') . '[background][top]',
        'value' => $this->getDefault('background', 'top'),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 3,
            'small' => 3,
            'large' => 3,
          ),
        ),
      ))
      ->BsText(array(
        'text' => __('Left', 'victheme_headline'),
        'name' => $this->getContext('prefix') . '[background][left]',
        'value' => $this->getDefault('background', 'left'),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 3,
            'small' => 3,
            'large' => 3,
          ),
        ),
      ))
      ->BsSelect(array(
        'text' => __('Repeat', 'victheme_headline'),
        'name' => $this->getContext('prefix') . '[background][repeat]',
        'value' => $this->getDefault('background', 'repeat'),
        'options' => array(
          'no-repeat' => __('No Repeat', 'victheme_headline'),
          'repeat' => __('Repeat All', 'victheme_headline'),
          'repeat-x' => __('Repeat Horizontaly', 'victheme_headline'),
          'repeat-y' => __('Repeat Verticaly', 'victheme_headline'),
        ),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 3,
            'small' => 3,
            'large' => 3,
          ),
        ),
      ))
      ->BsColor(array(
        'text' => __('Color', 'victheme_headline'),
        'name' => $this->getContext('prefix') . '[background][color]',
        'value' => $this->getDefault('background', 'color'),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 3,
            'small' => 3,
            'large' => 3,
          ),
        ),
      ))
      ->getParent()
      ->BsRow()
      ->lastChild()
      ->BsText(array(
        'text' => __('Width', 'victheme_headline'),
        'name' => $this->getContext('prefix') . '[background][width]',
        'value' => $this->getDefault('background', 'width'),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 3,
            'small' => 3,
            'large' => 3,
          ),
        ),
      ))
      ->BsText(array(
        'text' => __('Height', 'victheme_headline'),
        'name' => $this->getContext('prefix') . '[background][height]',
        'value' => $this->getDefault('background', 'height'),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 3,
            'small' => 3,
            'large' => 3,
          ),
        ),
      ))
      ->BsSelect(array(
        'text' => __('Attachment', 'victheme_headline'),
        'name' => $this->getContext('prefix') . '[background][attachment]',
        'value' => $this->getDefault('background', 'attachment'),
        'options' => array(
          'scroll' => __('Scroll', 'victheme_headline'),
          'fixed' => __('Fixed', 'victheme_headline'),
          'local' => __('Local', 'victheme_headline'),
          'initial' => __('Initial', 'victheme_headline'),
          'inherit' => __('Inherit', 'victheme_headline'),
        ),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 3,
            'small' => 3,
            'large' => 3,
          ),
        ),
      ))
      ->BsSelect(array(
        'text' => __('Parallax Mode', 'victheme_headline'),
        'name' =>  $this->getContext('prefix') . '[background][parallax]',
        'options' => array(
          'none' => __('Disable Parallax', 'victheme_headline'),
          'parallax-vertical' => __('Vertical Parallax', 'victheme_headline'),
          'parallax-horizontal' => __('Horizontal Parallax', 'victheme_headline'),
          'parallax-diagonal' => __('Diagonal Parallax', 'victheme_headline'),
        ),
        'value' => $this->getDefault('background', 'parallax'),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 3,
            'small' => 3,
            'large' => 3,
          ),
        ),
      ));


    $this
      ->BsRow(array(
        'group' => 'video-settings',
      ))
      ->lastChild()
      ->BsColumn(array(
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 4,
            'small' => 4,
            'large' => 4,
          ),
        ),
      ))
      ->lastChild()
      ->BsHeader(array(
        'tag' => 'h3',
        'text' => __('Video Settings', 'victheme_headline'),
      ))
      ->BsDescription(array(
        'text' => __('Configure the video background settings', 'victheme_headline'),
      ))
      ->getParent()
      ->BsColumn(array(
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 8,
            'small' => 8,
            'large' => 8,
          ),
        ),
      ))
      ->lastChild()
      ->BsRow()
      ->lastChild()
      ->BsCheckbox(array(
        'text' => __('Autoplay', 'victheme_headline'),
        'description' => __('Enable the autoplay feature, most mobile browser will disable the autoplay feature by default regardless of the setting provided here.', 'victheme_headline'),
        'name' => $this->getContext('prefix') . '[background][video][autoplay]',
        'switch' => true,
        'checked' => (boolean) $video['autoplay'],
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      ))
      ->BsCheckbox(array(
        'text' => __('Loop', 'victheme_headline'),
        'name' => $this->getContext('prefix') . '[background][video][loop]',
        'description' => __('When enabled, video will be looped infinitely.', 'victheme_headline'),
        'switch' => true,
        'checked' => (boolean) $video['loop'],
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      ))
      ->BsCheckbox(array(
        'text' => __('Scale', 'victheme_headline'),
        'name' => $this->getContext('prefix') . '[background][video][scale]',
        'description' => __('Scale the video to match the headline element size.', 'victheme_headline'),
        'switch' => true,
        'checked' => (boolean) $video['scale'],
        'attributes' => array(
          'class' => array(
            'clearboth',
          ),
        ),
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      ))
      ->BsCheckbox(array(
        'text' => __('Full Screen', 'victheme_headline'),
        'name' => $this->getContext('prefix') . '[background][video][fullscreen]',
        'description' => __('Stretch the video to full screen width and height.', 'victheme_headline'),
        'switch' => true,
        'checked' => (boolean) $video['fullscreen'],
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet'=> 6,
            'small' => 6,
            'large' => 6,
          ),
        ),
      ));
  }
}