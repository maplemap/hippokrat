<?php
/**
 * Factory class for building Headline Metabox
 *
 * @author jason.xie@victheme.com
 *
 * Available hooks and filters
 * @filter vtcore_hide_headline_metabox
 *     Filtering the post type where headline metabox must be disabled
 *
 * @filter vtcore_headline_metabox_form_alter
 *     Altering the form object before it got processed and rendered
 *
 * @filter vtcore_headline_context_alter
 *     Altering the headline form context before it is passed to
 *     the form object
 */
class VTCore_Headline_Metabox_Factory {

  private $metaboxClassPrefix = 'VTCore_Headline_Metabox_';
  private $context;
  private $form;

  private $tabsPanels = array(
    'general',
    'background',
    'masking',
    'items',
    'columns',
  );


  /**
   * Build Metabox form
   */
  public function buildMetabox() {

    global $post;

    VTCore_Wordpress_Utility::loadAsset('wp-bootstrap');
    VTCore_Wordpress_Utility::loadAsset('font-awesome');
    VTCore_Wordpress_Utility::loadAsset('wp-media');
    VTCore_Wordpress_Utility::loadAsset('headline-admin');


    // Build the default contextes
    $options = get_post_meta(get_the_ID(), '_headline_data');

    $this->context = $options;

    if (!is_array($this->context)) {
      $this->context = (array) $this->context;
    }

    $this->context = array_pop($this->context);

    // Allow other plugin or theme to alter the context
    $this->context = apply_filters('vtcore_headline_context_alter', $this->context, $options);

    // Simplify things by just overloading the form
    // This is required for processForm() to work and update
    // the form field value based on $_POST value
    $this->form = new VTCore_Bootstrap_Form_BsInstance(array(
      'type' => 'div',
    ));

    $tabs = $this->form->BsTabs(array(
      'prefix' => 'headline-tabs',
    ))->lastChild();

    $skip = apply_filters('vtcore_headline_remove_metabox_tabs', array());

    foreach ($this->tabsPanels as $name) {

      $class = $this->metaboxClassPrefix . ucfirst($name);

      if (class_exists($class, true) && !in_array($name, $skip)) {

        $metabox = new $class((array) $this->context);
        $metabox = apply_filters('vtcore_headline_metabox_form_alter', $metabox);

        $form = new VTCore_Bootstrap_Form_BsInstance();
        $form->addChildren($metabox);

        // Process the form
        $form->processForm();

        $form = NULL;
        unset($form);

        $tabs->addHeader($metabox->getContext('header'));
        $tabs->addContent($metabox->__toString());
        $tabs->setDelta();
        unset($metabox);
      }
    }

    $tabs->setActiveTabs(0);
    $tabs->addClass('headline-tabs');

    // Render the form
    $this->form->render();

    // Clean object
    unset($tabs);
  }
}