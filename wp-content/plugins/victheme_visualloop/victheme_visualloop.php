<?php
/*
Plugin Name: VicTheme Visual Loop
Plugin URI: http://victheme.com
Description: Plugin for providing advanced WordPress Loop shortcode
Author: jason.xie@victheme.com
Version: 1.0.1
Author URI: http://victheme.com
*/

define('VTCORE_VISUALLOOP_CORE_VERSION', '1.7.0');

load_plugin_textdomain('victheme_visualloop', false, 'victheme_visualloop/languages');

add_action('plugins_loaded', 'bootVicthemeVisualLoop', 13);

function bootVicthemeVisualLoop() {

  if (!defined('VTCORE_VERSION') || version_compare(VTCORE_VERSION, VTCORE_VISUALLOOP_CORE_VERSION, '!=')) {

    add_action('admin_notices', 'VisualLoopMissingCoreNotice');

    function VisualLoopMissingCoreNotice() {

      if (!defined('VTCORE_VERSION')) {
        $notice = __('VicTheme VisualLoop depends on VicTheme Core Plugin which is not activated or missing, Please enable it first before VicTheme VisualLoop can work properly.', 'victheme_visualloop');
      }
      elseif (version_compare(VTCORE_VERSION, VTCORE_VISUALLOOP_CORE_VERSION, '!=')) {
        $notice = sprintf(__('VicTheme VisualLoop depends on VicTheme Core Plugin API version %s to operate properly.', 'victheme_visualloop'), VTCORE_VISUALLOOP_CORE_VERSION);
      }

      if (isset($notice)) {
        echo '<div class="error""><p>' . $notice . '</p></div>';
      }

    }

    return;
  }


  if (defined('WPB_VC_VERSION') && !version_compare(WPB_VC_VERSION, '4.7.0', '>=')) {
    add_action('admin_notices', 'VTCore_VisualLoopVCTooLow');

    function VTCore_VisualLoopVCTooLow() {
      echo
        '<div class="error""><p>' .

        __( 'VisualLoop requires Visual Composer Plugin version 4.7.0 and above before it can function properly.', 'victheme_visualloop') .

        '</p></div>';
    }

    return;
  }

  define('VTCORE_VISUALLOOP_LOADED', true);
  define('VTCORE_VISUALLOOP_BASE_DIR', dirname(__FILE__));

  // Booting autoloader
  $autoloader = new VTCore_Autoloader('VTCore_VisualLoop', dirname(__FILE__));
  $autoloader->setRemovePath('vtcore' . DIRECTORY_SEPARATOR . 'visualloop' . DIRECTORY_SEPARATOR);
  $autoloader->register();

  // Registering assets
  VTCore_Wordpress_Init::getFactory('assets')
    ->get('library')
    ->detect(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets', plugins_url('', __FILE__) . '/assets');

  // Registering custom templating system
  VTCore_Wordpress_Init::getFactory('template')
    ->register(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'templates', 'templates');


  // Registering actions
  VTCore_Wordpress_Init::getFactory('actions')
    ->addPrefix('VTCore_VisualLoop_Actions_')
    ->addHooks(array(
      'vc_backend_editor_enqueue_js_css',
      'vc_frontend_editor_enqueue_js_css',
      'vc_load_iframe_jscss'
    ))
    ->register();

  // Registering actions
  VTCore_Wordpress_Init::getFactory('filters')
    ->addPrefix('VTCore_VisualLoop_Filters_')
    ->addHooks(array(
      'vtcore_register_shortcode_prefix',
      'vtcore_register_shortcode',
    ))
    ->register();

  // Register to visual composer via VTCore Visual Composer Factory
  if (VTCore_Wordpress_Init::getFactory('visualcomposer')) {
    VTCore_Wordpress_Init::getFactory('visualcomposer')
      ->mapShortcode(array(
        'VTCore_VisualLoop_Composer_VisualLoop',
        'VTCore_VisualLoop_Composer_VisualLoopFilter',
        'VTCore_VisualLoop_Composer_VisualLoopPager',
        'VTCore_VisualLoop_Composer_VisualLoopQuery',
        'VTCore_VisualLoop_Composer_VisualLoopStamp',
      ));
  }
}