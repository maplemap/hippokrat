<?php
/**
 * Default template for visual loop items.
 * This template assumes generic post type
 * is used and can be used as the starting
 * point for creating a new template
 *
 * @author jason.xie@victheme.com
 */

  $class = array(

    // Example of custom classes that can be defined here.
    'post-teasers',
    'column',
    'item',
    'multiple',
    'clearfix',

    // This is the grid settings from the shortcode turned
    // into bootstrap / visualcomposer grid class
    $this->getContext('objects.columns')->getClass(),

    // Create a zebra class for styling purposes
    ($post->post_delta % 2) ? 'odd' : 'even',
  );

?>
<article id="post-<?php the_ID(); ?>" <?php post_class($class); ?>>

  <?php
  /**
   * Build the post thumbnail when there is a thumbnail uploaded
   * via the featured image metabox.
   */
  ?>
  <?php if (has_post_thumbnail()) : ?>
    <section class="post-image">
      <a class="post-thumbnail-link"
         href="<?php esc_url(the_permalink()); ?>"
         alt="<?php echo esc_attr(get_the_title()); ?>">
          <?php echo wp_kses_post(get_the_post_thumbnail($post->ID, 'large')); ?>
      </a>
    </section>
  <?php endif; ?>

  <section class="post-content">
    <?php
    /**
     * Build the post title
     */
    ?>
    <header>
      <h1 class="post-title text-uppercase">
        <a class="post-title" href="<?php echo esc_url(get_the_permalink()); ?>"
           title="<?php echo esc_attr(get_the_title()); ?>">
          <?php echo wp_kses_post(get_the_title()); ?>
        </a>
      </h1>
    </header>

    <?php
    /**
     * Build the post excerpt
     */
    ?>
    <div class="post-excerpt">
      <?php the_excerpt(); ?>
    </div>


    <?php
    /**
     * Build the Readmore button
     */
    ?>
    <a class="post-readmore btn btn-primary"
       href="<?php echo esc_url(get_the_permalink()); ?>"
       title="<?php echo esc_attr(get_the_title()); ?>">
      <?php echo __('View More', 'victheme_visualloop'); ?>
    </a>
  </section>
</article>
