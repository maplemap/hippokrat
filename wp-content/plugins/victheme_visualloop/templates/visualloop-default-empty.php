<?php
/**
 * Template for displaying empty content message when
 * no teaser is available to display.
 *
 * For other not found page will use the page404.php
 * template.
 *
 * @author jason.xie@victheme.com
 */
?>
<div class="column item col-xs-12 col-sm-12 col-md-12 col-lg-12">
  <div id="post-empty"
       class="clearfix">

    <?php echo __('Sorry, but the requested resource was not found on this site.', 'victheme_visualloop'); ?>

  </div>
</div>