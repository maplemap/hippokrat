<?php
/**
 * Registering visuallooppager shortcode to visualcomposer
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_VisualLoop_Composer_VisualLoopPager
extends VTCore_Wordpress_Models_VC {

  
  public function registerVC() {

    $options = array(
      'name' => __('Loop Pager', 'victheme_visualloop'),
      'description' => __('Loop pagination element', 'victheme_visualloop'),
      'base' => 'visuallooppager',
      'icon' => 'icon-visuallooppager',
      'category' => __('VisualLoop', 'victheme_visualloop'),
      'is_container' => false,
      'as_child' => array(
        'only' => 'vc_column_inner, visualloop',
      ),
      'params' => array()
    );

    $options['params'][] = array(
      'heading' => __('Ajax', 'victheme_visualloop'),
      'type' => 'dropdown',
      'param_name' => 'ajax',
      'edit_field_class' => 'vc_col-sm-3',
      'admin_label' => true,
      'value' => array(
        __('Disable', 'victheme_visualloop') => 'false',
        __('Enable', 'victheme_visualloop') => 'true',
      ),
    );

    $options['params'][] = array(
      'heading' => __('Mini Pager', 'victheme_visualloop'),
      'type' => 'dropdown',
      'param_name' => 'mini',
      'edit_field_class' => 'vc_col-sm-3',
      'admin_label' => true,
      'value' => array(
        __('Disable', 'victheme_visualloop') => 'false',
        __('Enable', 'victheme_visualloop') => 'true',
      ),
    );

    $options['params'][] = array(
      'heading' => __('Infinite Pager', 'victheme_visualloop'),
      'type' => 'dropdown',
      'param_name' => 'infinite',
      'edit_field_class' => 'vc_col-sm-3',
      'admin_label' => true,
      'value' => array(
        __('Disable', 'victheme_visualloop') => 'false',
        __('Enable', 'victheme_visualloop') => 'true',
      ),
    );

    $options['params'][] = array(
      'heading' => __('Prev and next', 'victheme_visualloop'),
      'type' => 'dropdown',
      'param_name' => 'prev_next',
      'edit_field_class' => 'vc_col-sm-3',
      'admin_label' => true,
      'value' => array(
        __('Disable', 'victheme_visualloop') => 'false',
        __('Enable', 'victheme_visualloop') => 'true',
      ),
    );


    $options['params'][] = array(
      'type' => 'textfield',
      'heading' => __('Pager End Size', 'victheme_visualloop'),
      'description' => __('Number of pagination shown before replacing with ...', 'victheme_visualloop'),
      'param_name' => 'end_size',
      'edit_field_class' => 'vc_col-xs-4 clearboth',
      'admin_label' => true,
      'value' => '4',
    );

    $options['params'][] = array(
      'type' => 'textfield',
      'heading' => __('Pager Prev Text', 'victheme_visualloop'),
      'description' => __('Text for the prev button.', 'victheme_visualloop'),
      'param_name' => 'prev_text',
      'edit_field_class' => 'vc_col-xs-4',
      'admin_label' => true,
      'value' => '&laquo',
    );

    $options['params'][] = array(
      'type' => 'textfield',
      'heading' => __('Pager Next Text', 'victheme_visualloop'),
      'description' => __('Text for the next button.', 'victheme_visualloop'),
      'param_name' => 'next_text',
      'edit_field_class' => 'vc_col-xs-4',
      'admin_label' => true,
      'value' => '&raquo',
    );
    
    $options['params'][] = array(
      'type' => 'textfield',
      'heading' => __('CSS ID', 'victheme_visualloop'),
      'param_name' => 'id',
      'admin_label' => true,
    );

    $options['params'][] = array(
      'type' => 'textfield',
      'heading' => __('CSS Class', 'victheme_visualloop'),
      'param_name' => 'class',
      'admin_label' => true,
    );

    $options['params'][] = array(
      'type' => 'dropdown',
      'heading' => __( 'CSS Animation', 'victheme_visualloop'),
      'param_name' => 'css_animation',
      'admin_label' => true,
      'value' => array(
        __('No', 'victheme_visualloop') => '',
        __('Top to bottom', 'victheme_visualloop') => 'top-to-bottom',
        __('Bottom to top', 'victheme_visualloop') => 'bottom-to-top',
        __('Left to right', 'victheme_visualloop') => 'left-to-right',
        __('Right to left', 'victheme_visualloop') => 'right-to-left',
        __('Appear from center', 'victheme_visualloop') => "appear"
      ),
      'description' => __('Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'victheme_visualloop'),
    );

    $keys = array('columns', 'offset', 'push', 'pull');
    $sizes = array('mobile', 'tablet', 'small', 'large');
    foreach ($keys as $key) {
      foreach ($sizes as $size) {

        $name =  'grids___' . $key .'___' . $size;

        $value = 0;
        if ($key == 'columns') {
          $value = 12;
        }

        $options['params'][] = array(
          'type' => 'vtcore',
          'param_name' => $name,
          'name' => $name,
          'core_class' => 'VTCore_Bootstrap_Form_BsSelect',
          'edit_field_class' => 'vc_col-xs-3',
          'heading' => ucfirst($key) . ' ' . ucfirst($size),
          'group' => __('Grids', 'victheme_visualloop'),
          'core_context' => array(
            'name' => $name,
            'value' => $value,
            'input_elements' => array(
              'attributes' => array(
                'class' => array('wpb_vc_param_value', 'wpb-dropdown', 'icon', 'vtcore_field')
              ),
            ),
            'options' => range(0, 12, 1),
          ),
        );
      }
    }

    // CSS Editor
    $options['params'][] = array(
      'type' => 'css_editor',
      'heading' => __('Css', 'victheme_visualloop'),
      'param_name' => 'css',
      'group' => __('Design options', 'victheme_visualloop'),
    );
    
    return $options;
  }
}


class WPBakeryShortCode_VisualLoopPager extends WPBakeryShortCode {}