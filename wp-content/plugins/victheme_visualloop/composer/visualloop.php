<?php
/**
 * Registering visualloopcarousel shortcode to visualcomposer
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_VisualLoop_Composer_VisualLoop
extends VTCore_Wordpress_Models_VC {

  
  public function registerVC() {

    $options = array(
      'name' => __('Advanced Loop', 'victheme_visualloop'),
      'description' => __('VisualLoop elements container', 'victheme_visualloop'),
      'base' => 'visualloop',
      'icon' => 'icon-visualloop',
      'category' => __('VisualLoop', 'victheme_visualloop'),
      'deprecated' => false,
      'is_container' => true,
      'js_view' => 'VTCoreContainer',
      'as_parent' => array(
        'except' => 'visualloop, visualloopstamp',
      ),
      'params' => array()
    );

    $options['params'][] = array(
      'type' => 'textfield',
      'heading' => __('CSS ID', 'victheme_visualloop'),
      'description' => __('Valid CSS ID for the main wrapper element.', 'victheme_visualloop'),
      'param_name' => 'id',
      'admin_label' => true,
    );

    $options['params'][] = array(
      'type' => 'textfield',
      'heading' => __('CSS Class', 'victheme_visualloop'),
      'description' => __('Valid CSS Class for the main wrapper element.', 'victheme_visualloop'),
      'param_name' => 'class',
      'admin_label' => true,
    );

    $options['params'][] = array(
      'type' => 'dropdown',
      'heading' => __( 'CSS Animation', 'victheme_visualloop'),
      'param_name' => 'css_animation',
      'admin_label' => true,
      'value' => array(
        __('No', 'victheme_visualloop') => '',
        __('Top to bottom', 'victheme_visualloop') => 'top-to-bottom',
        __('Bottom to top', 'victheme_visualloop') => 'bottom-to-top',
        __('Left to right', 'victheme_visualloop') => 'left-to-right',
        __('Right to left', 'victheme_visualloop') => 'right-to-left',
        __('Appear from center', 'victheme_visualloop') => "appear"
      ),
      'description' => __('Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'victheme_visualloop'),
    );

    $keys = array('columns', 'offset', 'push', 'pull');
    $sizes = array('mobile', 'tablet', 'small', 'large');
    foreach ($keys as $key) {
      foreach ($sizes as $size) {

        $name =  'grids___' . $key .'___' . $size;

        $value = 0;
        if ($key == 'columns') {
          $value = 12;
        }

        $options['params'][] = array(
          'type' => 'vtcore',
          'param_name' => $name,
          'name' => $name,
          'core_class' => 'VTCore_Bootstrap_Form_BsSelect',
          'edit_field_class' => 'vc_col-xs-3',
          'heading' => ucfirst($key) . ' ' . ucfirst($size),
          'group' => __('Grids', 'victheme_visualloop'),
          'core_context' => array(
            'name' => $name,
            'value' => $value,
            'input_elements' => array(
              'attributes' => array(
                'class' => array('wpb_vc_param_value', 'wpb-dropdown', 'icon', 'vtcore_field')
              ),
            ),
            'options' => range(0, 12, 1),
          ),
        );
      }
    }

    $class = '';
    if (!get_theme_support('bootstrap')) {
      $class .= 'vc_col-xs-12 no-bootstrap';
    }
    $options['params'][] = array(
      'type' => 'vt_query_form',
      'heading' => __('Query Parameter', 'victheme_visualloop'),
      'param_name' => 'queryargs',
      'value' => '',
      'edit_field_class' => $class,
      'description' => __('Post query parameter settings for retrieving carousel items', 'victheme_visualloop'),
      'group' => __('Query', 'victheme_visualloop'),
    );

    // CSS Editor
    $options['params'][] = array(
      'type' => 'css_editor',
      'heading' => __('Css', 'victheme_visualloop'),
      'param_name' => 'css',
      'group' => __('Design options', 'victheme_visualloop'),
    );

    return $options;
  }
}


class WPBakeryShortCode_VisualLoop extends WPBakeryShortCodesContainer {}