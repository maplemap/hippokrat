<?php
/**
 * Registering visualloopfilter shortcode to visualcomposer
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_VisualLoop_Composer_VisualLoopFilter
extends VTCore_Wordpress_Models_VC {

  
  public function registerVC() {

    $options = array(
      'name' => __('Loop Filter', 'victheme_visualloop'),
      'description' => __('Loop taxonomy filtering element', 'victheme_visualloop'),
      'base' => 'visualloopfilter',
      'icon' => 'icon-visualloopfilter',
      'category' => __('VisualLoop', 'victheme_visualloop'),
      'is_container' => false,
      'as_child' => array(
        'only' => 'vc_column_inner, visualloop',
      ),
      'params' => array()
    );

    $taxonomies = get_taxonomies(array('public' => true));
    $tax = array(
      __('--- Select ---', 'victheme_visualloop') => '',
    );
    foreach ($taxonomies as $taxonomy) {
      $tax[$taxonomy] = $taxonomy;
    }

    $options['params'][] = array(
      'type' => 'dropdown',
      'param_name' => 'taxonomy',
      'heading' => __('Taxonomy for filter element', 'victheme_slick'),
      'description' => __('Define the taxonomy for filtering elements', 'victheme_slick'),
      'edit_field_class' => 'vc_col-xs-4',
      'value' => $tax,
      'admin_label' => true,
    );

    $options['params'][] = array(
      'heading' => __('Enable Ajax', 'victheme_woocommerce'),
      'description' => __('The filter element will redirect to taxonomy archive page when no ajax enabled.', 'victheme_woocommerce'),
      'type' => 'dropdown',
      'param_name' => 'ajax',
      'edit_field_class' => 'vc_col-xs-4',
      'admin_label' => true,
      'value' => array(
        __('Disable', 'victheme_woocommerce') => 'false',
        __('Enable', 'victheme_woocommerce') => 'true',
      ),
    );

    $options['params'][] = array(
      'heading' => __('Enable redirect on click', 'victheme_woocommerce'),
      'description' => __('The filter element will redirect to taxonomy archive page when no ajax enabled.', 'victheme_woocommerce'),
      'type' => 'dropdown',
      'param_name' => 'redirect',
      'edit_field_class' => 'vc_col-xs-4',
      'admin_label' => true,
      'value' => array(
        __('Disable', 'victheme_woocommerce') => 'false',
        __('Enable', 'victheme_woocommerce') => 'true',
      ),
    );

    $options['params'][] = array(
      'heading' => __('Enable terms drilling', 'victheme_woocommerce'),
      'description' => __('If enabled the filter will drill down to child terms when activated', 'victheme_woocommerce'),
      'type' => 'dropdown',
      'param_name' => 'drill',
      'edit_field_class' => 'vc_col-xs-4 clearboth',
      'admin_label' => true,
      'value' => array(
        __('Disable', 'victheme_woocommerce') => 'false',
        __('Enable', 'victheme_woocommerce') => 'true',
      ),
    );

    $options['params'][] = array(
      'heading' => __('Show parent terms', 'victheme_woocommerce'),
      'description' => __('Show the parent terms when child term is active, only applicable if drilling mode is enabled', 'victheme_woocommerce'),
      'type' => 'dropdown',
      'param_name' => 'parent',
      'edit_field_class' => 'vc_col-xs-4',
      'admin_label' => true,
      'value' => array(
        __('Disable', 'victheme_woocommerce') => 'false',
        __('Enable', 'victheme_woocommerce') => 'true',
      ),
    );

    $options['params'][] = array(
      'type' => 'textfield',
      'param_name' => 'all',
      'heading' => __('Show All', 'victheme_visualloop'),
      'edit_field_class' => 'vc_col-xs-6',
      'description' => __('Text to display as the show all button, leave empty to disable', 'victheme_visualloop'),
      'value' => __('All', 'victheme_visualloop'),
      'admin_label' => true,
    );

    $options['params'][] = array(
      'type' => 'textfield',
      'param_name' => 'ajax___ajax-loading-text',
      'heading' => __('Loading Text', 'victheme_visualloop'),
      'edit_field_class' => 'vc_col-xs-6',
      'description' => __('Text to display when ajax is processing', 'victheme_visualloop'),
      'value' => __('Processing...', 'victheme_visualloop'),
      'admin_label' => true,
    );

    $options['params'][] = array(
      'type' => 'textfield',
      'heading' => __('CSS ID', 'victheme_visualloop'),
      'param_name' => 'id',
      'admin_label' => true,
    );

    $options['params'][] = array(
      'type' => 'textfield',
      'heading' => __('CSS Class', 'victheme_visualloop'),
      'param_name' => 'class',
      'admin_label' => true,
    );

    $options['params'][] = array(
      'type' => 'dropdown',
      'heading' => __( 'CSS Animation', 'victheme_visualloop'),
      'param_name' => 'css_animation',
      'admin_label' => true,
      'value' => array(
        __('No', 'victheme_visualloop') => '',
        __('Top to bottom', 'victheme_visualloop') => 'top-to-bottom',
        __('Bottom to top', 'victheme_visualloop') => 'bottom-to-top',
        __('Left to right', 'victheme_visualloop') => 'left-to-right',
        __('Right to left', 'victheme_visualloop') => 'right-to-left',
        __('Appear from center', 'victheme_visualloop') => "appear"
      ),
      'description' => __('Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'victheme_visualloop'),
    );

    $keys = array('columns', 'offset', 'push', 'pull');
    $sizes = array('mobile', 'tablet', 'small', 'large');
    foreach ($keys as $key) {
      foreach ($sizes as $size) {

        $name =  'grids___' . $key .'___' . $size;

        $value = 0;
        if ($key == 'columns') {
          $value = 12;
        }

        $options['params'][] = array(
          'type' => 'vtcore',
          'param_name' => $name,
          'name' => $name,
          'core_class' => 'VTCore_Bootstrap_Form_BsSelect',
          'edit_field_class' => 'vc_col-xs-3',
          'heading' => ucfirst($key) . ' ' . ucfirst($size),
          'group' => __('Grids', 'victheme_visualloop'),
          'core_context' => array(
            'name' => $name,
            'value' => $value,
            'input_elements' => array(
              'attributes' => array(
                'class' => array('wpb_vc_param_value', 'wpb-dropdown', 'icon', 'vtcore_field')
              ),
            ),
            'options' => range(0, 12, 1),
          ),
        );
      }
    }

    // CSS Editor
    $options['params'][] = array(
      'type' => 'css_editor',
      'heading' => __('Css', 'victheme_visualloop'),
      'param_name' => 'css',
      'group' => __('Design options', 'victheme_visualloop'),
    );

    return $options;
  }
}


class WPBakeryShortCode_VisualLoopFilter extends WPBakeryShortCode {}