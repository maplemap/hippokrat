<?php
/**
 * Registering visualloopfilter shortcode to visualcomposer
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_VisualLoop_Composer_VisualLoopStamp
extends VTCore_Wordpress_Models_VC {

  
  public function registerVC() {

    $options = array(
      'name' => __('Isotope Stamp', 'victheme_visualloop'),
      'description' => __('Stamp wrapper element', 'victheme_visualloop'),
      'base' => 'visualloopstamp',
      'icon' => 'icon-visualloopstamp',
      'category' => __('VisualLoop', 'victheme_visualloop'),
      'is_container' => true,
      'js_view' => 'VTCoreContainer',
      'as_parent' => array(
        'except' => 'visualloop, visualloopquery, visuallooppager, visualloopfilter',
      ),
      'as_child' => array(
        'only' => 'visualloopquery',
      ),
      'params' => array()
    );


    $options['params'][] = array(
      'type' => 'dropdown',
      'param_name' => 'horizontal',
      'heading' => __('Horizontal Position', 'victheme_slick'),
      'description' => __('Horizontal position for the stamp element', 'victheme_slick'),
      'edit_field_class' => 'vc_col-xs-6',
      'value' => array(
        __('Left', 'victheme_visualloop') => 'left',
        __('Center', 'victheme_visualloop') => 'center',
        __('Right', 'victheme_visualloop') => 'right',
      ),
      'admin_label' => true,
    );

    $options['params'][] = array(
      'type' => 'dropdown',
      'param_name' => 'vertical',
      'heading' => __('Vertical Position', 'victheme_slick'),
      'description' => __('Vertical position for the stamp element', 'victheme_slick'),
      'edit_field_class' => 'vc_col-xs-6',
      'value' => array(
        __('Top', 'victheme_visualloop') => 'top',
        __('Center', 'victheme_visualloop') => 'center',
        __('Bottom', 'victheme_visualloop') => 'bottom',
      ),
      'admin_label' => true,
    );

    $options['params'][] = array(
      'type' => 'textfield',
      'heading' => __('CSS ID', 'victheme_visualloop'),
      'param_name' => 'id',
      'admin_label' => true,
    );

    $options['params'][] = array(
      'type' => 'textfield',
      'heading' => __('CSS Class', 'victheme_visualloop'),
      'param_name' => 'class',
      'admin_label' => true,
    );

    $options['params'][] = array(
      'type' => 'dropdown',
      'heading' => __( 'CSS Animation', 'victheme_visualloop'),
      'param_name' => 'css_animation',
      'admin_label' => true,
      'value' => array(
        __('No', 'victheme_visualloop') => '',
        __('Top to bottom', 'victheme_visualloop') => 'top-to-bottom',
        __('Bottom to top', 'victheme_visualloop') => 'bottom-to-top',
        __('Left to right', 'victheme_visualloop') => 'left-to-right',
        __('Right to left', 'victheme_visualloop') => 'right-to-left',
        __('Appear from center', 'victheme_visualloop') => "appear"
      ),
      'description' => __('Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'victheme_visualloop'),
    );

    $keys = array('columns');
    $sizes = array('mobile', 'tablet', 'small', 'large');
    foreach ($keys as $key) {
      foreach ($sizes as $size) {

        $name =  'grids___' . $key .'___' . $size;

        $value = 0;
        if ($key == 'columns') {
          $value = 12;
        }

        $options['params'][] = array(
          'type' => 'vtcore',
          'param_name' => $name,
          'name' => $name,
          'core_class' => 'VTCore_Bootstrap_Form_BsSelect',
          'edit_field_class' => 'vc_col-xs-3',
          'heading' => ucfirst($key) . ' ' . ucfirst($size),
          'group' => __('Grids', 'victheme_visualloop'),
          'core_context' => array(
            'name' => $name,
            'value' => $value,
            'input_elements' => array(
              'attributes' => array(
                'class' => array('wpb_vc_param_value', 'wpb-dropdown', 'icon', 'vtcore_field')
              ),
            ),
            'options' => range(0, 12, 1),
          ),
        );
      }
    }

    // CSS Editor
    $options['params'][] = array(
      'type' => 'css_editor',
      'heading' => __('Css', 'victheme_visualloop'),
      'param_name' => 'css',
      'group' => __('Design options', 'victheme_visualloop'),
    );

    return $options;
  }
}


class WPBakeryShortCode_VisualLoopStamp extends WPBakeryShortCodesContainer {}