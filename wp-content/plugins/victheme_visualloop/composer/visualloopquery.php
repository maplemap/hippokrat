<?php
/**
 * Registering visualloopquery shortcode to visualcomposer
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_VisualLoop_Composer_VisualLoopQuery
extends VTCore_Wordpress_Models_VC {

  
  public function registerVC() {

    $options = array(
      'name' => __('Loop Items', 'victheme_visualloop'),
      'description' => __('Loop queried content', 'victheme_visualloop'),
      'base' => 'visualloopquery',
      'icon' => 'icon-visualloopquery',
      'category' => __('VisualLoop', 'victheme_visualloop'),
      'is_container' => true,
      'js_view' => 'VTCoreContainer',
      'as_parent' => array(
        'only' => 'visualloopstamp',
      ),
      'as_child' => array(
        'only' => 'vc_column_inner, visualloop',
      ),
      'params' => array()
    );

    $templates = array_flip(apply_filters('vtcore_visualloop_register_template_items', array(
      'visualloop-default.php' => __('Default', 'victheme_visualloop'),
    )));

    $empty = array_flip(apply_filters('vtcore_visualloop_register_template_empty', array(
      'visualloop-default-empty.php' => __('Default', 'victheme_visualloop'),
    )));

    $options['params'][] = array(
      'type' => 'dropdown',
      'param_name' => 'template___items',
      'edit_field_class' => 'vc_col-xs-4',
      'heading' => __('Item Template', 'victheme_visualloop'),
      'description' => __('Template to use for processing the query items', 'victheme_visualloop'),
      'value' => $templates,
    );

    $options['params'][] = array(
      'type' => 'dropdown',
      'param_name' => 'template___empty',
      'edit_field_class' => 'vc_col-xs-4',
      'heading' => __('Empty Template', 'victheme_visualloop'),
      'description' => __('Template to use when query found no valid items', 'victheme_visualloop'),
      'value' => $empty,
    );

    $options['params'][] = array(
      'type' => 'dropdown',
      'heading' => __('Layout Style', 'victheme_visualloop'),
      'param_name' => 'isotope___layoutmode',
      'edit_field_class' => 'vc_col-xs-4',
      'admin_label' => true,
      'value' => array(
        __('FitRows', 'victheme_visualloop') => 'fitRows',
        __('Masonry', 'victheme_visualloop') => 'masonry',
      ),
    );


    $options['params'][] = array(
      'heading' => __('Enable equalheight', 'victheme_visualloop'),
      'type' => 'dropdown',
      'param_name' => 'isotope___equalheight',
      'edit_field_class' => 'vc_col-xs-4',
      'admin_label' => true,
      'value' => array(
        __('Disable', 'victheme_visualloop') => 'false',
        __('Enable', 'victheme_visualloop') => 'true',
      ),
    );

    $options['params'][] = array(
      'heading' => __('Enable ajax', 'victheme_visualloop'),
      'type' => 'dropdown',
      'param_name' => 'ajax',
      'edit_field_class' => 'vc_col-xs-4',
      'admin_label' => true,
      'value' => array(
        __('Disable', 'victheme_visualloop') => 'false',
        __('Enable', 'victheme_visualloop') => 'true',
      ),
    );

    $options['params'][] = array(
      'type' => 'textfield',
      'heading' => __('Ajax Loading Text', 'victheme_visualloop'),
      'edit_field_class' => 'vc_col-xs-4',
      'param_name' => 'ajaxdata___ajax-loading-text',
      'value' => __('Loading....', 'victheme_visualloop'),
      'admin_label' => true,
    );

    $keys = array('columns');
    $sizes = array('mobile', 'tablet', 'small', 'large');
    foreach ($keys as $key) {
      foreach ($sizes as $size) {

        $name =  'grids___' . $key .'___' . $size;

        $value = 0;
        if ($key == 'columns') {
          $value = 12;
        }

        $options['params'][] = array(
          'type' => 'vtcore',
          'param_name' => $name,
          'name' => $name,
          'core_class' => 'VTCore_Bootstrap_Form_BsSelect',
          'edit_field_class' => 'vc_col-xs-3',
          'heading' => ucfirst($key) . ' ' . ucfirst($size),
          'core_context' => array(
            'name' => $name,
            'value' => $value,
            'input_elements' => array(
              'attributes' => array(
                'class' => array('wpb_vc_param_value', 'wpb-dropdown', 'icon', 'vtcore_field')
              ),
            ),
            'options' => range(0, 12, 1),
          ),
        );
      }
    }

    $keys = array('columns', 'offset', 'push', 'pull');
    $sizes = array('mobile', 'tablet', 'small', 'large');
    foreach ($keys as $key) {
      foreach ($sizes as $size) {

        $name =  'container___grids___' . $key .'___' . $size;

        $value = 0;
        if ($key == 'columns') {
          $value = 12;
        }

        $options['params'][] = array(
          'type' => 'vtcore',
          'param_name' => $name,
          'name' => $name,
          'core_class' => 'VTCore_Bootstrap_Form_BsSelect',
          'edit_field_class' => 'vc_col-xs-3',
          'heading' => ucfirst($key) . ' ' . ucfirst($size),
          'group' => __('Grids', 'victheme_visualloop'),
          'core_context' => array(
            'name' => $name,
            'value' => $value,
            'input_elements' => array(
              'attributes' => array(
                'class' => array('wpb_vc_param_value', 'wpb-dropdown', 'icon', 'vtcore_field')
              ),
            ),
            'options' => range(0, 12, 1),
          ),
        );
      }
    }

    $options['params'][] = array(
      'type' => 'textfield',
      'heading' => __('CSS ID', 'victheme_visualloop'),
      'param_name' => 'id',
      'admin_label' => true,
    );

    $options['params'][] = array(
      'type' => 'textfield',
      'heading' => __('CSS Class', 'victheme_visualloop'),
      'param_name' => 'class',
      'admin_label' => true,
    );

    $options['params'][] = array(
      'type' => 'dropdown',
      'heading' => __( 'CSS Animation', 'victheme_visualloop'),
      'param_name' => 'css_animation',
      'admin_label' => true,
      'value' => array(
        __('No', 'victheme_visualloop') => '',
        __('Top to bottom', 'victheme_visualloop') => 'top-to-bottom',
        __('Bottom to top', 'victheme_visualloop') => 'bottom-to-top',
        __('Left to right', 'victheme_visualloop') => 'left-to-right',
        __('Right to left', 'victheme_visualloop') => 'right-to-left',
        __('Appear from center', 'victheme_visualloop') => "appear"
      ),
      'description' => __('Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'victheme_visualloop'),
    );

    $options['params'][] = array(
      'type' => 'hidden',
      'param_name' => 'css_animation',
      'value' => '',
    );

    // CSS Editor
    $options['params'][] = array(
      'type' => 'css_editor',
      'heading' => __('Css', 'victheme_visualloop'),
      'param_name' => 'css',
      'group' => __('Design options', 'victheme_visualloop'),
    );
    
    return $options;
  }
}


class WPBakeryShortCode_VisualLoopQuery extends WPBakeryShortCodesContainer {}