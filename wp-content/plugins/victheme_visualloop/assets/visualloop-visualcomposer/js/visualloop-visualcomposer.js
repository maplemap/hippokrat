/**
 *
 * Integrating Advanced Carousel and its elements
 * into Visual Composer Front-end editor.
 *
 * This is not compatible with VisualComposer version
 * less than 4.7 since it uses events rather than extending
 * method!.
 *
 * Note to developer, the trick is just to pass the queryargs
 * params taken from the containers to each of query elements
 * before the "update" event fired, thus it will be injected
 * to the ajax parameter and then PHP will pickup the rest.
 *
 * @author jason.xie@victheme.com
 */
(function ($) {

  $(window)
    .on('vc_ready', function() {


      vc.vtcore.visualLoop = {
        getParent: function(model) {
          var Container = $(vc.frame_window.document).find('[data-model-id="' + model.get('parent_id') + '"]'), Loop;

          // Parent not visualloop carousel drill up dom further
          if (Container.data('tag') != 'visualloop') {
            Container = Container.closest('[data-tag="visualloop"]').eq(0);
          }

          Loop = vc.shortcodes.get(Container.data('model-id'));

          if (Loop) {
            model.set('loop', Loop);
            model.set('loopID', Loop.get('id'));
            delete Loop;
          }
        },
        setParams: function(model) {
          // We only concern for queryargs params
          this.getParent(model);
          if (model.get('loop')) {
            var loopparams = model.get('loop').get('params'), params = model.get('params');
            params.queryargs = loopparams.queryargs;
            model.set('params', params);
          }

          return model.get('params');
        },
        cleanParams: function(model) {
          var params = model.get('params'), clean;

          params.queryargs && delete params.queryargs;

          model.set('params', params);
        },
        isotope: {
          init: function(model) {
            var isotope = $(vc.frame_window.document).find('[data-model-id="' + model.get('id') + '"]').find('.js-isotope').eq(0);
            if (!isotope.data('isotope')) {
              isotope.isotope(isotope.data('isotope-options'));
            }
          },
          refresh: function(model) {

            if (!model.get('layouting')) {
              var isotope = $(vc.frame_window.document).find('[data-model-id="' + model.get('id') + '"]').find('.js-isotope').eq(0);

              if (isotope.find('.stamps')) {
                isotope.isotope('stamp', isotope.find('.stamps'));
              }

              model.set('layouting', true);

              setTimeout(function () {
                isotope.isotope('layout');
                model.set('layouting', false);
              }, 1000);
            }
          },
          fixOptions: function(model) {
            var isotope = $(vc.frame_window.document).find('[data-model-id="' + model.get('id') + '"]').find('.js-isotope').eq(0),
                data = isotope.data('isotope-options');

            if (data && data.layoutMode && (data.layoutMode != 'masonry' || data.layoutMode != 'packery')) {
              data.layoutMode = 'masonry';
              isotope.data('isotope-options', data).isotope(data);
            }
          },
          fixStampClass: function(model) {

            if (model.view && model.view.$content) {
              var cssClass = model.view.$content.attr('class'), classes = '', arrayClass = [];

              if (cssClass) {
                _.each(cssClass.split(' '), function(value, key) {
                  if (value.indexOf('position-') !== -1
                    || value.indexOf('stamps') !== -1) {
                    arrayClass.push(value);
                  }
                },this);

                classes = arrayClass.join(' ')
              }

              model.view.$el.addClass(classes);
              model.view.$content.removeClass(classes);
            }
          }
        }
      }


      /**
       * Bind custom events to act on vc events.
       */
      vc.events

        .on('shortcodes:visuallooppager:add', function(model, eventType) {
          vc.vtcore.visualLoop.setParams(model);
        })

        .on('shortcodes:visualloopquery:add', function(model, eventType) {
          vc.vtcore.visualLoop.setParams(model);
        })

        .on('shortcodes:visualloopfilter:add', function(model, eventType) {
          vc.vtcore.visualLoop.setParams(model);
        })

        .on('shortcodeView:ready:visualloopquery', function(model) {
          vc.vtcore.fixGridClass(model, model.view.$el.children());
          vc.vtcore.registerGridContainer(model, 'vc_element-container');
          vc.vtcore.checkEmpty(model);
          vc.vtcore.visualLoop.cleanParams(model);
          vc.vtcore.visualLoop.isotope.init(model);
        })

        .on('shortcodeView:ready:visuallooppager', function(model) {
          vc.vtcore.fixGridClass(model, model.view.$el.children());
          vc.vtcore.visualLoop.cleanParams(model);
        })

        .on('shortcodeView:ready:visualloopfilter', function(model) {
          vc.vtcore.fixGridClass(model, model.view.$el.children());
          vc.vtcore.visualLoop.cleanParams(model);
        })

        .on('shortcodeView:ready:visualloop', function(model) {
          vc.vtcore.fixGridClass(model, model.view.$el.children());
          vc.vtcore.registerGridContainer(model, 'vtcore-visualloop');
          vc.vtcore.checkEmpty(model);
        })

        .on('shortcodes:visualloopstamp:destroy', function(model, eventType) {
          vc.vtcore.visualLoop.isotope.refresh(vc.shortcodes.get(model.get('parent_id')));
        })

        .on('shortcodeView:ready:visualloopstamp', function(model, eventType) {
          var parent = vc.shortcodes.get(model.get('parent_id'));
          vc.vtcore.fixGridClass(model, model.view.$el.children());
          vc.vtcore.registerGridContainer(model, 'vc_element-container');
          vc.vtcore.visualLoop.isotope.fixOptions(parent);
          vc.vtcore.visualLoop.isotope.fixStampClass(model);
          vc.vtcore.visualLoop.isotope.refresh(parent);
          vc.vtcore.checkEmpty(model);
        })

        .on('shortcodeView:updated:visualloopstamp', function(model, eventType) {
          vc.vtcore.visualLoop.isotope.refresh(vc.shortcodes.get(model.get('parent_id')));
        })

        .on('shortcodeView:updated:visualloop', function(model) {

          var elements = vc.shortcodes.where({loopID: model.get('id')});
          _.each(elements, function(element) {
            vc.vtcore.visualLoop.setParams(element);
            _.defer(function () {
              vc.builder.update(element);
            });
          }, this);

        });


      window.InlineShortcodeView_visualloopquery = window.InlineShortcodeViewContainer.extend({
        beforeUpdate: function() {
          vc.vtcore.visualLoop.setParams(this.model);
        }
      });

      window.InlineShortcodeView_visuallooppager = window.InlineShortcodeView_visualloopfilter = window.InlineShortcodeView.extend({
        beforeUpdate: function() {
          vc.vtcore.visualLoop.setParams(this.model);
        }
      });
    });

})(window.jQuery);