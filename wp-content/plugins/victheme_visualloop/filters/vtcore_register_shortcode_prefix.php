<?php
/**
 * Hooking into vtcore_register_shortcode_prefix filter
 * to register visualcandy related shortcodes
 *
 * @author jason.xie@victheme.com
 */
class VTCore_VisualLoop_Filters_VTCore__Register__Shortcode__Prefix
extends VTCore_Wordpress_Models_Hook {

  public function hook($prefix = NULL) {

    $prefix[] = 'VTCore_VisualLoop_Shortcodes_';

    return $prefix;
  }
}