<?php
/**
 * Shortcode for building the visualloop carousel container
 * in the advanced visualloop carousel building method.
 *
 * This container will hold all the attributes related
 * to the visualloop element inside this container and will
 * pass the attributes globaly.
 *
 * subelement can use VTCore_VisualLoop_Shortcodes_VisualLoopContainer::getAttributes()->get()
 * method to access the aggregated attributes.
 *
 *
 * [visualloop
 *   queryid="the unique query id"
 *   queryargs="{json of valid WP_Query arguments}"
 *   grids___columns___mobile="X"
 *   grids___columns___tablet="X"
 *   grids___columns___small="X"
 *   grids___columns___large="X"
 *   grids___offset___mobile="X"
 *   grids___offset___tablet="X"
 *   grids___offset___small="X"
 *   grids___offset___large="X"
 *   grids___push___mobile="X"
 *   grids___push___tablet="X"
 *   grids___push___small="X"
 *   grids___push___large="X"
 *   grids___pull___mobile="X"
 *   grids___pull___tablet="X"
 *   grids___pull___small="X"
 *   grids___pull___large="X"
 *   ]
 *
 *   [visualloopfilter
 *      all="text for all button"
 *      ajax___ajax-loading-text="some text for ajax notice"
 *      taxonomy="taxonomy"
*       queryargs="{json of valid WP_Query arguments}"
 *      redirect="true"
 *      parent="true"
 *      drill="false"
 *      grids___columns___mobile="X"
 *      grids___columns___tablet="X"
 *      grids___columns___small="X"
 *      grids___columns___large="X"
 *      grids___offset___mobile="X"
 *      grids___offset___tablet="X"
 *      grids___offset___small="X"
 *      grids___offset___large="X"
 *      grids___push___mobile="X"
 *      grids___push___tablet="X"
 *      grids___push___small="X"
 *      grids___push___large="X"
 *      grids___pull___mobile="X"
 *      grids___pull___tablet="X"
 *      grids___pull___small="X"
 *      grids___pull___large="X"
 *   ]
 *
 *   [visualloopquery
 *      ajax="true|false"
 *      ajaxdata___ajax-loading-text="text for loading"
 *      template___item="the template name for the visualloop items"
 *      template___empty="the template name where no visualloop item found"
 *      isotope___layoutmode="masonry|fitRows"
 *      isotope___equalheight="true"
 *      queryargs="{json of valid WP_Query arguments}"
 *      grids___columns___mobile="X"
 *      grids___columns___tablet="X"
 *      grids___columns___small="X"
 *      grids___columns___large="X"
 *   ]
 *
 *    [visualloopsingleitems]
 *    [visualloopstamp]
 *
 *   [/visualloopquery]
 *
 *
 *   [visuallooppager
 *      queryargs="{json of valid WP_Query arguments}"
 *      mini="true"
 *      infinite="true"
 *      prev_next="true"
 *      end_size="4"
 *      prev_text="&laquo"
 *      next_text="&raquo"
 *      grids___columns___mobile="X"
 *      grids___columns___tablet="X"
 *      grids___columns___small="X"
 *      grids___columns___large="X"
 *      grids___offset___mobile="X"
 *      grids___offset___tablet="X"
 *      grids___offset___small="X"
 *      grids___offset___large="X"
 *      grids___push___mobile="X"
 *      grids___push___tablet="X"
 *      grids___push___small="X"
 *      grids___push___large="X"
 *      grids___pull___mobile="X"
 *      grids___pull___tablet="X"
 *      grids___pull___small="X"
 *      grids___pull___large="X"
 *  ]
 *
 * [/visualloopcarousel]
 *
 *
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_VisualLoop_Shortcodes_VisualLoop
extends VTCore_VisualLoop_Models_Shortcodes
implements VTCore_Wordpress_Interfaces_Shortcodes {

  protected $processDottedNotation = true;


  /**
   * Extending parent method.
   * @see VTCore_Wordpress_Models_Shortcodes::processCustomRules()
   */
  protected function processCustomRules() {

    // Convert the bootstrap classes into vc compatible one
    $this->convertVCGrid = !get_theme_support('bootstrap');
    $this->accessAttributes();

    $object = new VTCore_Wordpress_Objects_Array($this->atts);

    $object
      ->add('type', 'div')
      ->add('attributes.class.visualloop', 'visualloop-advanced-loop');

    do_action('vtcore_visualloop_alter_container', $object, $this);

    $this->atts = $object->extract();
  }



  public function buildObject() {

    VTCore_Wordpress_Utility::loadAsset('wp-ajax');
    VTCore_Wordpress_Utility::loadAsset('wp-loop');
    VTCore_Wordpress_Utility::loadAsset('visualloop-front');

    $this->object = new VTCore_Bootstrap_Element_BsElement($this->atts);

    $this->object->addChildren(do_shortcode($this->content));

    $this->resetAttributes();
    $this->resetQuery();

  }
}