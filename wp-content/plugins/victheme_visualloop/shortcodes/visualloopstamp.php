<?php
/**
 * Shortcode for building the visualloop filtering element
 * the filtering element must use taxonomy as the filters
 *
 *   [visualloopfilter
 *      horizontal="left|center|right"
 *      vertical="top|center|right"
 *      grids___columns___mobile="X"
 *      grids___columns___tablet="X"
 *      grids___columns___small="X"
 *      grids___columns___large="X"
 *   ]
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_VisualLoop_Shortcodes_VisualLoopStamp
extends VTCore_VisualLoop_Models_Shortcodes
implements VTCore_Wordpress_Interfaces_Shortcodes {

  protected $processDottedNotation = true;

  /**
   * VC 4.7 somehow wont record
   * the shortcode if it is on the first select options
   * define the defaults for those kind of select here!
   * @var array
   */
  protected $defaults = array(
    'horizontal' => 'left',
    'vertical' => 'top',
  );

  /**
   * Extending parent method.
   * @see VTCore_Wordpress_Models_Shortcodes::processCustomRules()
   */
  protected function processCustomRules() {

    // Convert the bootstrap classes into vc compatible one
    $this->convertVCGrid = !get_theme_support('bootstrap');

    $object = new VTCore_Wordpress_Objects_Array($this->defaults);
    $object
      ->merge($this->atts)
      ->add('type', 'div')
      ->add('attributes.class.position', 'position-' . $object->get('vertical') . '-' . $object->get('horizontal'))
      ->add('attributes.class.stamp', 'stamps');

    do_action('vtcore_visualloop_alter_stamps', $object, $this);

    $this->atts = $object->extract();
  }



  public function buildObject() {

    $this->object = new VTCore_Bootstrap_Element_BsElement($this->atts);
    $this->object->addChildren(do_shortcode($this->content));

  }
}