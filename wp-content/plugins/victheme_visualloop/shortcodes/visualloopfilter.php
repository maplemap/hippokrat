<?php
/**
 * Shortcode for building the visualloop filtering element
 * the filtering element must use taxonomy as the filters
 *
 *   [visualloopfilter
 *      all="text for all button"
 *      ajax___ajax-loading-text="some text for ajax notice"
 *      taxonomy="taxonomy"
 *      queryargs="{json of valid WP_Query arguments}"
 *      redirect="true"
 *      parent="true"
 *      drill="false"
 *      grids___columns___mobile="X"
 *      grids___columns___tablet="X"
 *      grids___columns___small="X"
 *      grids___columns___large="X"
 *      grids___offset___mobile="X"
 *      grids___offset___tablet="X"
 *      grids___offset___small="X"
 *      grids___offset___large="X"
 *      grids___push___mobile="X"
 *      grids___push___tablet="X"
 *      grids___push___small="X"
 *      grids___push___large="X"
 *      grids___pull___mobile="X"
 *      grids___pull___tablet="X"
 *      grids___pull___small="X"
 *      grids___pull___large="X"
 *   ]
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_VisualLoop_Shortcodes_VisualLoopFilter
extends VTCore_VisualLoop_Models_Shortcodes
implements VTCore_Wordpress_Interfaces_Shortcodes {

  protected $processDottedNotation = true;

  protected $booleans = array(
    'ajax',
    'redirect',
    'drill',
    'parent',
  );

  /**
   * VC 4.7 somehow wont record
   * the shortcode if it is on the first select options
   * define the defaults for those kind of select here!
   * @var array
   */
  protected $defaults = array(
    'ajax' => false,
    'redirect' => false,
    'drill' => false,
    'parent' => false,
  );

  /**
   * Extending parent method.
   * @see VTCore_Wordpress_Models_Shortcodes::processCustomRules()
   */
  protected function processCustomRules() {

    // Convert the bootstrap classes into vc compatible one
    $this->convertVCGrid = !get_theme_support('bootstrap');

    $object = new VTCore_Wordpress_Objects_Array($this->defaults);
    $object
      ->merge($this->atts)
      ->add('query', $this->accessQuery())
      ->add('id', $this->accessQuery()->get('vtcore_queryid'));

    do_action('vtcore_visualloop_alter_filter', $object, $this);

    $this->atts = $object->extract();
  }



  public function buildObject() {

    $this->object = new VTCore_Wordpress_Element_WpTermList($this->atts);

  }
}