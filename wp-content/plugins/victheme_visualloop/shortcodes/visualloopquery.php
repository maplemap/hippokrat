<?php
/**
 * Shortcode for building the visualloop element and using
 * WpCarousel plus WP_Query to fetch the visualloop item data
 *
 *   [visualloopquery
 *      ajax="true|false"
 *      ajaxdata___ajax-loading-text="text for loading"
 *      template___item="the template name for the visualloop items"
 *      template___empty="the template name where no visualloop item found"
 *      isotope___layoutmode="masonry|fitRows"
 *      isotope___equalheight="true"
 *      queryargs="{json of valid WP_Query arguments}"
 *      grids___columns___mobile="X"
 *      grids___columns___tablet="X"
 *      grids___columns___small="X"
 *      grids___columns___large="X"
 *   ]
 *
 *    [visualloopsingleitems]
 *    [visualloopstamp]
 *
 *   [/visualloopquery]
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_VisualLoop_Shortcodes_VisualLoopQuery
extends VTCore_VisualLoop_Models_Shortcodes
implements VTCore_Wordpress_Interfaces_Shortcodes {

  protected $processDottedNotation = true;

  protected $booleans = array(
    'isotope.equalheight',
    'ajax',
  );

  /**
   * VC 4.7 somehow wont record
   * the shortcode if it is on the first select options
   * define the defaults for those kind of select here!
   * @var array
   */
  protected $defaults = array(
    'template' => array(
      'items' => 'visualloop-default.php',
      'empty' => 'visualloop-default-empty.php',
    ),
    'isotope' => array(
      'layoutmode' => 'fitRows',
      'equalheight' => false,
    ),
    'ajax' => false,
  );

  protected function processCustomRules() {

    // Convert the bootstrap classes into vc compatible one
    $this->convertVCGrid = !get_theme_support('bootstrap');

    $object = new VTCore_Wordpress_Objects_Array($this->defaults);
    $object
      ->merge($this->atts)
      ->add('query', $this->accessQuery())
      ->add('id', $this->accessQuery()->get('vtcore_queryid'));

    // Masonry mode
    if ($object->get('isotope.layoutmode') == 'masonry') {
      $object->add('data.isotope-options',array(
        'itemSelector' => '.item',
        'stamp' => '.stamps',
        'layoutMode' => 'masonry'
      ));
    }


    // Fitrow Mode
    if ($object->get('isotope.layoutmode') == 'fitRows') {
      $object->add('data.isotope-options', array(
        'itemSelector' => '.item',
        'layoutMode' => 'fitRows',
        'stamp' => '.stamps',
        'fitRows' => array(
          'gutter' => array(
            'width' => 0,
            'height' => 0,
          ),
          'equalheight' => $object->get('isotope.equalheight'),
        ),
      ));
    }

    $object->add('attributes.class.template', str_replace('.php', '', $object->get('template.items')));

    do_action('vtcore_visualloop_alter_query', $object, $this);

    $this->atts = $object->extract();

  }


  public function buildObject() {
    $this->object = new VTCore_Wordpress_Element_WpLoop($this->atts);
    $this->object->addChildren(do_shortcode($this->content));
  }
}