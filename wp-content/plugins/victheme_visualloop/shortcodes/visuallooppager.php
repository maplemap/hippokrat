<?php
/**
 * Shortcode for building the visualloop dots wrapper
 * and act as the javascript append target point
 *
 *   [visuallooppager
 *      queryargs="{json of valid WP_Query arguments}"
 *      mini="true"
 *      infinite="true"
 *      prev_next="true"
 *      end_size="4"
 *      prev_text="&laquo"
 *      next_text="&raquo"
 *      grids___columns___mobile="X"
 *      grids___columns___tablet="X"
 *      grids___columns___small="X"
 *      grids___columns___large="X"
 *      grids___offset___mobile="X"
 *      grids___offset___tablet="X"
 *      grids___offset___small="X"
 *      grids___offset___large="X"
 *      grids___push___mobile="X"
 *      grids___push___tablet="X"
 *      grids___push___small="X"
 *      grids___push___large="X"
 *      grids___pull___mobile="X"
 *      grids___pull___tablet="X"
 *      grids___pull___small="X"
 *      grids___pull___large="X"
 *  ]
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_VisualLoop_Shortcodes_VisualLoopPager
extends VTCore_VisualLoop_Models_Shortcodes
implements VTCore_Wordpress_Interfaces_Shortcodes {

  protected $processDottedNotation = true;

  protected $booleans = array(
    'ajax',
    'mini',
    'infinite',
    'prev_next',
  );

  /**
   * VC 4.7 somehow wont record
   * the shortcode if it is on the first select options
   * define the defaults for those kind of select here!
   * @var array
   */
  protected $defaults = array(
    'ajax' => false,
    'mini' => false,
    'inifinite' => false,
    'prev_next' => false,
  );

  protected function processCustomRules() {

    // Convert the bootstrap classes into vc compatible one
    $this->convertVCGrid = !get_theme_support('bootstrap');

    $object = new VTCore_Wordpress_Objects_Array($this->defaults);
    $object
      ->merge($this->atts)
      ->add('query', $this->accessQuery())
      ->add('id', $this->accessQuery()->get('vtcore_queryid'));

    do_action('vtcore_visualloop_alter_pagination', $object, $this);

    $this->atts = $object->extract();
  }

  public function buildObject() {
    $this->object = new VTCore_Wordpress_Element_WpPager($this->atts);
  }
}