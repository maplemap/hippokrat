<?php
/**
 * Page callback class for building the
 * one click demo import configuration
 * page.
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Demo_Page_Import {

  private $form;
  private $header;
  private $messages;



  /**
   * Menu callback function
   * Don't allow ancient user without javascript enabled.
   */
  public function buildPage() {

    // REMOVE THIS!
    // d(get_option('vtcore_demo_status_' . BASE_THEME));
    // d(get_option('vtcore_demo_map_' . BASE_THEME));
    // d(get_option('vtcore_demo_lock'));


    wp_deregister_script('heartbeat');

    VTCore_Wordpress_Utility::loadAsset('wp-bootstrap');
    VTCore_Wordpress_Utility::loadAsset('bootstrap-confirmation');
    VTCore_Wordpress_Utility::loadAsset('wp-ajax');
    VTCore_Wordpress_Utility::loadAsset('demo-page');

    $this->messages = new VTCore_Bootstrap_BsMessages();

    // Build and process the form
    $this
      ->buildHeader()
      ->buildForm()
      ->processForm()
      ->processError(true, true);


    // Grab any errors
    $errors = $this->form->getErrors();

    // Puke in errors
    if (!empty($errors)) {
      foreach ($errors as $error) {
        $this->messages->setError($error);
      }
    }

    $this->form->prependChild($this->messages->render());

    // Let other alter this form
    add_action('vtcore_demo_form_alter', $this->form);

    // Spit out the HTML Markup
    $this->header->render();
    $this->form->render();

  }



  /**
   * Building the form
   */
  private function buildForm() {

    $this->form = new VTCore_Bootstrap_Form_BsInstance(array(
      'attributes' => array(
        'id' => 'theme-configuration-form',
        'method' => 'post',
        'action' => $_SERVER['REQUEST_URI'],
        'class' => array('container-fluid'),
        'autocomplete' => 'off',
      ),
    ));

    // @see all the Elements for demo apps!
    $this->form
      ->addChildren(new VTCore_Bootstrap_Element_BsAlert(array(
        'text' => __('Demo import requires browser javascript, please enable it first.', 'victheme_demo'),
        'attributes' => array(
          'class' => array(
            'vtcore-demo-js-notice',
          ),
        ),
      )))
      ->addChildren(new VTCore_Demo_Element_Frame());

    return $this->form;

  }


  /**
   * Build the page header elements
   */
  private function buildHeader() {

    $this->header = new VTCore_Bootstrap_Grid_BsContainerFluid(array(
      'type' => 'div',
      'attributes' => array(
        'id' => 'theme-options-header',
      ),
    ));

    $this->header
      ->BsRow()
      ->lastChild()
      ->BsColumn(array(
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 12,
            'small' => 12,
            'large' => 6,
          )
        ),
      ))
      ->lastChild()
      ->addOverloaderPrefix('VTCore_Fontawesome_')
      ->faIcon(array(
        'icon' => 'upload',
        'shape' => 'circle',
        'position' => 'pull-left',
      ))
      ->BsHeader(array(
        'text' => __('Demo Content', 'victheme_demo'),
      ))
      ->getParent()
      ->BsColumn(array(
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 12,
            'small' => 12,
            'large' => 6,
          )
        ),
      ));

    return $this;
  }
}