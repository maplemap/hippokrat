<?php
/**
 * Booting up the Exporter related class
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Demo_Init {

  private $autoloader;
  private $actions = array(
    'admin_menu',
  );

  private $filters = array();


  public function __construct() {

    // Booting autoloader
    $this->autoloader = new VTCore_Autoloader('VTCore_Demo', dirname(__FILE__));
    $this->autoloader->setRemovePath('vtcore' . DIRECTORY_SEPARATOR . 'demo' . DIRECTORY_SEPARATOR);
    $this->autoloader->register();

    // Registering autoloader for the theme demo content object
    $this->autoloader = new VTCore_Autoloader('VTCore_Zeus_Demo', VTCORE_ZEUS_THEME_PATH . DIRECTORY_SEPARATOR . 'demo');
    $this->autoloader->setRemovePath('vtcore' . DIRECTORY_SEPARATOR . 'zeus' . DIRECTORY_SEPARATOR . 'demo');
    $this->autoloader->register();

    // Register the special file extension .demo, this is done this way
    // to circumvent theme_check plugin cannot parse .php file larger than 1MB
    $this->autoloader = new VTCore_Autoloader('VTCore_Zeus_Demo', VTCORE_ZEUS_THEME_PATH . DIRECTORY_SEPARATOR . 'demo');
    $this->autoloader->setRemovePath('vtcore' . DIRECTORY_SEPARATOR . 'zeus' . DIRECTORY_SEPARATOR . 'demo');
    $this->autoloader->setFileExtension('.demo');
    $this->autoloader->register();

    // Registering assets
    VTCore_Wordpress_Init::getFactory('assets')->get('library')->detect(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets', plugins_url('', __FILE__) . '/assets');

    // Registering actions
    VTCore_Wordpress_Init::getFactory('filters')
      ->addPrefix('VTCore_Demo_Filters_')
      ->addHooks($this->filters)
      ->register();


    // Registering filters
    VTCore_Wordpress_Init::getFactory('actions')
      ->addPrefix('VTCore_Demo_Actions_')
      ->addHooks($this->actions)
      ->register();

    load_plugin_textdomain('victheme_demo', false, 'victheme_demo/languages');
  }

}