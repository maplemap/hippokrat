<?php
/**
 * All class that provides additional information
 * for the demo site information must extend this class.
 *
 * The demo site additional information only usable
 * if the theme has multiple demo site that is importable
 * to user.
 *
 * @author jason.xie@victheme.com
 *
 */
abstract class VTCore_Demo_Models_Config {

  protected $context = array(
    'thumbnail' => false,
    'title' => false,
    'description' => false,
  );

  public function __construct() {
    $this->register();
  }

  abstract protected function register();

  public function get($type) {
    return isset($this->context[$type]) ? $this->context[$type] : false;
  }
}