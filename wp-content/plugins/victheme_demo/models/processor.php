-<?php
/**
 * Base super abstract class that must be
 * extended by other processors sub class
 *
 * @author jason.xie@victheme.com
 *
 */
abstract class VTCore_Demo_Models_Processor {

  protected $config;
  protected $map;
  protected $statuses;
  protected $name;
  protected $site = 'Data_';
  protected $prefix = 'VTCore_Zeus_Demo_';
  protected $close = true;

  private $mapStorage;
  private $statusesStorage;
  protected $dependencies = false;

  /**
   * array of processor that must be
   * finish first before this processor
   * can run.
   * @var array
   */
  protected $processorsDependencies = array();


  public function __construct($name, $site = NULL, $type = NULL) {

    $this->mapStorage = 'vtcore_demo_map_' . BASE_THEME;
    $this->statusesStorage = 'vtcore_demo_status_' . BASE_THEME;

    $this->map = get_option($this->mapStorage, array());
    $this->statuses = get_option($this->statusesStorage, array());
    $this->name = $name;

    if (!empty($site)) {
      $this->site = ucfirst($site) . '_';
    }

    $this->prefix .= $this->site;

    $this->log = new VTCore_Demo_Entity_Logger();
  }

  abstract function importData();
  abstract function removeData();



  /**
   * Method for checking all other processors
   * that is marked as dependencies were successfully
   * imported or not
   * @return bool
   */
  public function checkProcessorDependencies() {
    $process = true;

    if (!empty($this->processorsDependencies)) {
      foreach ($this->processorsDependencies as $processor) {
        if (!$this->getImportStatus($processor)) {
          $process = FALSE;
          break;
        }
      }
    }

    return $process;
  }


  /**
   * Method for overloading config class into
   * processor class.
   */
  public function loadData() {
    $className = $this->prefix . ucfirst(str_replace('_', '__', $this->name));

    if (class_exists($className, true)) {
      $this->config = new $className();
      return true;
    }

    return false;
  }


  /**
   * Somehow we got no valid data.
   * Don't reimport this processor.
   */
  public function failedLoadingData() {
    $this->markImportedStatus();
    return $this;
  }


  /**
   * Get other processor import status;
   * @param $processor
   * @return bool
   */
  protected function getImportStatus($processor) {
    return isset($this->statuses[$processor]) ? $this->statuses[$processor] : false;
  }


  /**
   * Check if this processor has performed the importation process
   */
  protected function checkImportStatus() {
    return isset($this->statuses[$this->name]) ? $this->statuses[$this->name] : false;
  }


  /**
   * Mark the processor as imported.
   */
  protected function markImportedStatus() {
    $this->statuses[$this->name] = true;
    update_option($this->statusesStorage, $this->statuses);
    return $this;
  }


  /**
   * Unmark the processor as imported
   */
  protected function removeImportedStatus() {
    unset($this->statuses[$this->name]);
    update_option($this->statusesStorage, $this->statuses);
    return $this;
  }

  protected function deleteImportedStatus() {
    delete_option($this->statusesStorage);
    return $this;
  }


  /**
   * Storing value mapping for later use
   */
  protected function setMap($type, $oldValue, $newValue) {
    $this->map[$this->name][$type][$oldValue] = $newValue;
    $this->saveMap();
    return $this;
  }


  /**
   * Retrieving stored mapped value
   */
  protected function getMap($processor, $type, $default = false) {
    return isset($this->map[$processor][$type]) ? $this->map[$processor][$type] : $default;
  }



  /**
   * Retrieving stored mapped value
   */
  protected function getMappedValue($processor, $type, $value) {
    return isset($this->map[$processor][$type][$value]) ? $this->map[$processor][$type][$value] : false;
  }



  /**
   * Remove stored processor map data
   */
  protected function removeMapByValue($processor, $type, $value) {
    if (isset($this->map[$processor][$type][$value])) {
      unset($this->map[$processor][$type][$value]);
      $this->saveMap();
    }
    return $this;
  }


  /**
   * Remove stored processor map data
   */
  protected function removeMap($processor) {
    if (isset($this->map[$processor])) {
      unset($this->map[$processor]);
      $this->saveMap();
    }
    return $this;
  }


  /**
   * Check if the target map array empty, and
   * if it is empty remove the whole processor
   * array
   *
   * @param $processor
   * @param bool $type
   * @return bool
   */
  protected function removeMapWhenEmpty($processor, $type = false) {
    $status = false;
    if (!$type) {
      if (isset($this->map[$processor])) {
        $this->map[$processor] = array_filter($this->map[$processor]);

        if (empty($this->map[$processor])) {
          $this->removeMap($processor);
          $status = TRUE;
        }
      }
    }
    else {
      if (isset($this->map[$processor][$type]) && empty($this->map[$processor][$type])) {
        $this->removeMap($processor);
        $status = TRUE;
      }
    }

    return $status;
  }

  protected function checkMap($processor, $type, $oldvalue) {
    return isset($this->map[$processor][$type][$oldvalue]);
  }

  protected function saveMap() {
    return update_option($this->mapStorage, $this->map);
  }

  protected function deleteMap() {
    return delete_option($this->mapStorage);
  }

  protected function getConfig() {
    return $this->config;
  }

  protected function reportFailed() {
    $this->close = false;
    return $this;
  }

  protected function maybeClose() {
    return $this->close;
  }

  public function addLog($message, $type) {
    $this->getLog()->addLog(str_replace('type-', '', $type), $message);
  }

  public function getLog() {
    return $this->log;
  }

  public function getStatus() {
    return $this->checkImportStatus();
  }

  public function getDependencies() {
    return $this->dependencies;
  }


}