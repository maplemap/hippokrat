<?php
/**
 * All class that provides demo
 * data must extend this models.
 *
 * @author jason.xie@victheme.com
 *
 */
abstract class VTCore_Demo_Models_Data {

  protected $context;
  protected $table;
  protected $primary;
  protected $base;

  public function getData() {
    return $this->context;
  }

  public function getTable() {
    return $this->table;
  }

  public function getPrimary() {
    return $this->primary;
  }

  public function getBase() {
    return $this->base;
  }

}