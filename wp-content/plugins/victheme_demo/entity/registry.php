<?php
/**
 * Class VTCore_Demo_Entity_Registry
 *
 * Class for registering valid processors
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Demo_Entity_Registry
extends VTCore_Wordpress_Models_Config {

  /**
   * Don't allow child to override this.
   * @param array $options
   * @return void|VTCore_Wordpress_Config_Base
   */
  final protected function register(array $options) {

    // Don't allow user to perform alteration or
    // database saving.
    $this->database = false;
    $this->filter = 'vtcore_demo_registry_alter';
    $this->loadFunction = false;
    $this->saveFunction = false;
    $this->deleteFunction = false;

    $this->options = array(
      'demo_path' => VTCORE_ZEUS_THEME_PATH . DIRECTORY_SEPARATOR . 'demo',
      'prefix' => 'VTCore_Zeus_Demo_',
      'lock' => new VTCore_Demo_Entity_Lock(),
      'processors' => array(

        // Default WordPress Processors
        '10' => 'options',
        '20' => 'users',
        '30' => 'usermeta',
        '40' => 'terms',
        '50' => 'posts',
        '60' => 'postmeta',
        '70' => 'menu',
        '80' => 'attachment',
        '81' => 'attachment',
        '82' => 'attachment',
        '90' => 'comments',
        '100' => 'commentmeta',
        '110' => 'term_relationships',
        '120' => 'shortcodes',
        '130' => 'widgets',

        // Special case Needs to run between terms
        '39' => 'woocommerce_attribute_taxonomies',
        '41' =>'woocommerce_termmeta',

        // Plugins and extension
        // @use number above 1000 and below 90000 as the array keys.
        '1030' =>'headline',
        '1040' =>'portfolio',
        '1050' =>'agents',
        '1060' =>'property',
        '1070' =>'vcmeta',
        '1080' =>'revslider',
        '1090' =>'layerslider',
        '1100' => 'department',
        '1110' => 'services',
        '1120' => 'members',



        // Closing up
        '90000' =>'cleanup',
        '90010' => 'clearcache',
      ),
    );

    $this->filter();
    $this->merge($options);

    // Process the object
    $this->sortProcessor();
    $this->detect();
    $this->loadConfig();
    $this->filterProcessors();
    $this->fixArrayKeys();


    return $this;
  }


  public function sortProcessor() {
    $processor = array_flip($this->options['processors']);
    asort($processor, SORT_NUMERIC);
    $this->options['processors'] = array_flip($processor);
    return $this;
  }


  public function fixArrayKeys() {
    $processors = $this->get('processors');
    $processors = array_values($processors);
    $this->add('processors', $processors);
    return $this;
  }


  public function detect() {

    $directories = new DirectoryIterator($this->get('demo_path'));

    $sites = array();
    foreach ($directories as $directory) {
      if ($directory->isDir() && !$directory->isDot()) {
        $sites[] = $directory->getFilename();
      }
    }

    $this->add('sites', $sites);

    return $this;
  }


  public function loadConfig() {

    $deps = array();

    foreach ($this->get('sites') as $site) {
      $className = $this->get('prefix') . ucfirst($site) . '_Config';

      if (!class_exists($className, true)) {
        continue;
      }

      $config = new $className();

      if (!is_a($config, 'VTCore_Demo_Models_Config')) {
        continue;
      }

      // Collect all config
      $this->add('config.' . $site, $config);

      // Mark active config
      if ($this->get('lock')->lock() == $site) {
        $this->add('active', $config);
      }

      // Collect all dependencies
      foreach($config->get('dependencies') as $plugin) {
        $deps[] = $plugin;
      }

      unset($config);
    }

    $deps = array_unique($deps);
    $this->add('deps', $deps);
  }


  protected function filterProcessors() {

    foreach ($this->get('processors') as $delta => $processor) {

      $name = 'VTCore_Demo_Processor_' . ucfirst(str_replace('_', '__', $processor));

      if (class_exists($name, true)) {
        $object = new $name($processor);
        $deps = $object->getDependencies();

        if (!empty($deps) && !in_array($deps, $this->get('deps'))) {
          $this->remove('processors.' . $delta);
        }
      }


    }

    return $this;
  }

}