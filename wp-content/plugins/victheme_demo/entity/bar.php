<?php
/**
 * Class for handling the progress bar mutation
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Demo_Entity_Bar
extends VTCore_Wordpress_Models_Config {

  protected $target = '#import-bar .progress-bar';

  /**
   * Don't allow child to override this.
   * @param array $options
   * @return void|VTCore_Wordpress_Config_Base
   */
  final protected function register(array $options) {

    // Don't allow user to perform alteration or
    // database saving.
    $this->database = false;
    $this->filter = false;
    $this->loadFunction = false;
    $this->saveFunction = false;
    $this->deleteFunction = false;

    if (!isset($options['processor'])) {
      $options['processor'] = '';
    }

    $this->options = array(
      'target' => '#import-bar .progress-bar',
      'delta' => 1,
      'total' => 1,
      'text' => '',
    );

    $this->merge($options);

    $this->barCalculateValue();

    return $this;
  }


  public function barMove() {
    $this->add('render.move', array(
      'mode' => 'callback',
      'content' => 'jQuery("' . $this->get('target') . '").width("' . $this->get('value') . '%");',
    ));

    return $this;
  }


  public function barActivated() {

    $this->add('render.active', array(
      'mode' => 'callback',
      'content' => 'jQuery("#import-bar .progress").addClass("animated");',
    ));

    return $this;
  }


  public function barDeactivated() {
    $this->add('render.active', array(
      'mode' => 'callback',
      'content' => 'jQuery("#import-bar .progress").removeClass("animated");',
    ));

    return $this;
  }


  public function barText() {

    $this->add('render.text', array(
      'mode' => 'text',
      'target' => $this->get('target'). ' span',
      'content' => $this->get('text'),
    ));

    return $this;
  }


  public function barCalculateValue() {
    $this->add('value', (100 / $this->get('total')) * ($this->get('delta') + 1));
    return $this;
  }


  public function barInverseColor() {
    $this->add('render.color', array(
      'mode' => 'callback',
      'content' => 'jQuery("' . $this->get('target') . '").addClass("progress-bar-danger");',
    ));
    return $this;
  }


  public function barNormalColor() {
    $this->add('render.color', array(
      'mode' => 'callback',
      'content' => 'jQuery("' . $this->get('target') . '").removeClass("progress-bar-danger");',
    ));
    return $this;
  }

}