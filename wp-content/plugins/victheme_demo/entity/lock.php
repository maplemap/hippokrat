<?php
/**
 * Class for managing site locked demo
 * import type.
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Demo_Entity_Lock {

  protected $locked = false;

  public function __construct() {
    $this->locked = get_option('vtcore_demo_lock', false);
  }

  public function lock() {
    return $this->locked;
  }

  public function locking($lock) {
    $this->locked = $lock;
    update_option('vtcore_demo_lock', $lock);
    return $this;
  }

  public function unlock() {
    $this->locked = false;
    delete_option('vtcore_demo_lock');
    return $this;
  }

}