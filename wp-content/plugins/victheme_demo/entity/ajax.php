<?php
/**
 * Ajax Callback Class
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Demo_Entity_Ajax
extends VTCore_Wordpress_Models_Ajax {

  protected $config;
  protected $lock;
  protected $importer;
  protected $logger;
  protected $bar;


  /**
   * Main Callback function invoked by Wordpress
   * Ajax and the parent class.
   */
  public function processAjax() {

    // Build Registry
    $this->registry = new VTCore_Demo_Entity_Registry();

    // Build Config
    $this->config = new VTCore_Wordpress_Objects_Config(array(
      'processor' =>  $this->post['queue'],
      'mode' => str_replace('Demo', '', $this->post['value']),
      'type' => str_replace('plugin##', '', $this->post['queue']),
      'prefix' => 'VTCore_Demo_Processor_',
      'processors' => $this->registry->get('processors'),
      'keys' => array_flip($this->registry->get('processors')),
      'total' => count($this->registry->get('processors')),
    ));

    $this->config->add('delta', $this->config->get('keys.' . $this->config->get('processor')));

    $this->logger = new VTCore_Demo_Entity_Logger(array(
      'mode' => 'ajax',
    ));

    // Process locking
    $this->processLock();

    // Process Bar
    $this->processBar();

    // Import the content
    $this->processImport();

    // Update button status
    $this->processStatus();

    // Closing down shop
    $this->logger->renderLogs();

    foreach ($this->logger->get('render') as $delta => $log) {
      $this->addRender('action', $log);
    }

    foreach ($this->bar->get('render') as $delta => $bar) {
      $this->addRender('action', $bar);
    }

    $this->logger->reset()->delete();

    $this->maybeLock();

  }


  /**
   * Pass the saved status to apps via ajax
   */
  protected function processStatus() {
    $status = get_option('vtcore_demo_status_' . BASE_THEME, false);
    $this->addRender('action', array(
      'mode' => 'callback',
      'content' => 'jQuery(".btn-import").data("ajax-completed", ' . json_encode($status) . ');',
    ));
  }


  /**
   * Perform the import process
   * @return $this
   */
  protected function processImport() {
    $this->importer = new VTCore_Demo_Entity_Import($this->config->extract());
    $this->importer->get('object')->getLog()->renderLogs();

    foreach ($this->importer->get('object')->getLog()->get('render') as $delta => $log) {
      $this->addRender('action', $log);
    }

    if (!$this->importer->get('status') && $this->config->get('mode') == 'import') {
      // $this->logger->addLog('error', sprintf(__('Failed when trying to import %s processor', 'victheme_demo'), ucfirst(str_replace('_', ' ', $this->config->get('processor')))));
    }

    if ($this->importer->get('status') && $this->config->get('mode') == 'remove') {
      // $this->logger->addLog('error', sprintf(__('Failed when trying to remove %s processor', 'victheme_demo'), ucfirst(str_replace('_', ' ', $this->config->get('processor')))));
    }

    // Open the import button back and enable all processor when remove complete!
    if ($this->config->get('mode') == 'remove') {
      $this->config->add('maps', get_option('vtcore_demo_map_' . BASE_THEME));
      $maps = $this->config->get('maps');
      if (empty($maps)) {
        $this->addRender('action', array(
          'mode' => 'callback',
          'content' => 'jQuery(".btn-import").data("ajax-completed", false);',
        ));
      }
    }

    return $this;
  }



  /**
   * Lock or unlock the site, we will basing
   * this from the processor central maps. regardless
   * of what is the ajax mode.
   *
   * @return $this
   */
  protected function maybeLock() {
    // Lock the site
    $this->config->add('maps', get_option('vtcore_demo_map_' . BASE_THEME));
    $maps = $this->config->get('maps');

    // Open lock
    if (empty($maps)) {
      $this->lock->unlock();
      $this->addRender('action', array(
        'mode' => 'callback',
        'content' => 'jQuery("[data-toggle-preview]").attr("data-toggle", "modal");',
      ));
    }

    // Lock the site changer
    else {
      $this->addRender('action', array(
        'mode' => 'callback',
        'content' => 'jQuery("[data-toggle-preview]").attr("data-toggle", "disabled");',
      ));
    }

    return $this;
  }




  /**
   * Mutate the progress bar and pass it via Ajax Framework
   * for jQuery to pickup and mutate the DOM
   * @return $this
   */
  protected function processBar() {

    $this->bar = new VTCore_Demo_Entity_Bar($this->config->extract());
    $this->bar->add('text', sprintf(__('%s processed', 'victheme_demo'), ucfirst(str_replace('_', ' ', $this->config->get('processor')))));
    $this->bar->barActivated();
    $this->bar->barNormalColor();

    if ($this->config->get('mode') == 'remove') {
      $this->bar->barInverseColor();
    }

    $last = $this->config->get('keys');
    if (array_pop($last) == $this->config->get('delta')) {
      $this->bar->barDeactivated();
      $this->bar->add('value', 100);
      $this->bar->add('text', __('Finish processing all items', 'victheme_demo'));
    }

    $this->bar->barMove()->barText();


    return $this;
  }



  /**
   * Initial process for setting the right lock for the site.
   * @return $this
   */
  protected function processLock() {

    $this->lock = new VTCore_Demo_Entity_Lock();

    if (isset($this->post['data']['demo_site'])
      && !empty($this->post['data']['demo_site'])) {

      $this->lock->locking($this->post['data']['demo_site']);
    }

    $this->config->add('site', $this->lock->lock());

    return $this;
  }


}