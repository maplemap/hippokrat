<?php
/**
 * Class VTCore_Demo_Entity_Import
 *
 * Class for handling the importation process.
 * this is a factory type of class.
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Demo_Entity_Import
extends VTCore_Wordpress_Models_Config {


  /**
   * Don't allow child to override this.
   * @param array $options
   * @return void|VTCore_Wordpress_Config_Base
   */
  final protected function register(array $options) {

    // Don't allow user to perform alteration or
    // database saving.
    $this->database = false;
    $this->filter = false;
    $this->loadFunction = false;
    $this->saveFunction = false;
    $this->deleteFunction = false;

    $this->options = array(
      'timelimit' => 36000000,
      'memorylimit' => '256M',
    );

    $this->merge($options);

    // Increase the time limit
    set_time_limit($this->get('timelimit'));
    ini_set('memory_limit', $this->get('memorylimit'));

    $this->buildProcessor();

    if ($this->get('object')) {
      switch ($this->get('mode')) {
        case 'import' :
          $this->doImport();
          break;
        case 'remove' :
          $this->doRemove();
          break;
      }
    }
  }


  protected function buildProcessor() {

    $name = $this->get('prefix') . ucfirst(str_replace('_', '__', $this->get('processor')));
    if (class_exists($name, true)) {
      $this->add('name', $name);
      $this->add('object', new $name($this->get('processor'), $this->get('site')), $this->get('type'));
    }

    return $this;
  }


  protected function doImport() {

    if ($this->get('object')->checkProcessorDependencies()) {

      // When we got data, process the object
      if ( $this->get('object')->loadData()) {
        $this->get('object')->importData();
        $this->add('status', $this->get('object')->getStatus());
      }
      else {
        $this->get('object')->failedLoadingData();
      }
    }

    return $this;
  }


  protected function doRemove() {
    $this->get('object')->loadData();
    $this->get('object')->removeData();
    $this->add('status', $this->get('object')->getStatus());

    return $this;
  }

}