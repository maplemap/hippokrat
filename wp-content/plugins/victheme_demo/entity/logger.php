<?php
/**
 * Class VTCore_Demo_Entity_Logger
 *
 * Generic class for storing and building the logger
 * message with its markup.
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Demo_Entity_Logger
extends VTCore_Wordpress_Objects_Config {

  /**
   * Don't allow child to override this.
   * @param array $options
   * @return void|VTCore_Wordpress_Config_Base
   */
  final protected function register(array $options) {

    // Don't allow user to perform alteration or
    // database saving.
    $this->database = 'vtcore_demo_logs';
    $this->filter = false;

    $this->options = array(
      'target' => '#log-panel .panel-body',
      'mode' => 'ajax',
      'markup' => array(),
      'render' => array(),
      'logs' => array(),
    );

    $this->load();
    $this->merge($options);

    return $this;
  }


  public function addLog($type, $message) {
    $this->options['logs'][$type][] = $message;
    $this->save();
    return $this;
  }


  public function renderLogs() {

    foreach ($this->get('logs') as $type => $logs) {

      $class = 'text-success';

      switch ($type) {
        case 'danger':
        case 'errors':
        case 'error':
        case 'text-danger':
          $class = 'text-danger';
          break;

        case 'text-success':
        case 'success' :
          $class = 'text-success';
          break;
      }

      foreach ($logs as $delta => $log) {
        $this->add('markup.' . $delta, new VTCore_Bootstrap_Element_BsElement(array(
          'type' => 'div',
          'attributes' => array(
            'class' => array($class),
          ),
          'text' => $log,
          'clean' => true,
        )));
      }
    }

    switch ($this->get('mode')) {
      case 'ajax' :
        $sliced = array_chunk($this->get('markup'), 10);
        foreach ($sliced as $delta => $logs) {
          $markup = '';
          foreach ($logs as $log) {
            $markup .= $log->__toString();
          }
          $this->add('render.' . $delta, array(
            'mode' => 'append',
            'target' => $this->get('target'),
            'content' => $markup,
          ));
        }
        break;
      case 'html' :
        $markup = '';
        foreach ($this->get('markup') as $object) {
          $markup .= $object->__toString();
        }

        $this->add('render', $markup);
        break;
    }


    $this->delete();
  }


}