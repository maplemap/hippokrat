<?php
/**
 * Hooking into wordpress admin_menu action
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Demo_Actions_Admin__Menu
extends VTCore_Wordpress_Models_Hook {

  public function hook() {
    add_theme_page(
      'Theme Option',
      'Demo Import',
      'edit_theme_options',
      'demo_import',
      array(new VTCore_Demo_Page_Import(), 'buildPage')
    );
  }
}