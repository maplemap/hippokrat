/**
 * Additional javascript only for the demo import page
 * 
 * @author jason.xie@victheme.com
 */
jQuery(document).ready(function($) {

  $('.vtcore-demo-js-notice').remove();

  var VTCoreDemoDeparam = function( params, coerce ) {
    var obj = {},
      coerce_types = { 'true': !0, 'false': !1, 'null': null };

    if(typeof params !== 'string') {
      return obj;
    }
    if(typeof coerce === 'undefined') {
      coerce = true;
    }

    function safeDecodeURIComponent(component) {
      returnvalue = '';
      try {
        returnvalue = decodeURIComponent(component);
      } catch(e) {
        returnvalue = unescape(component);
      }
      return returnvalue;
    }

    // Iterate over all name=value pairs.
    params.replace( /\+/g, ' ' ).split( '&' ).forEach(function(element) {
      var param = element.split( '=' ),
        key = safeDecodeURIComponent( param[0] ),
        val,
        cur = obj,
        i = 0,

      // If key is more complex than 'foo', like 'a[]' or 'a[b][c]', split it
      // into its component parts.
        keys = key.split( '][' ),
        keys_last = keys.length - 1;

      // If the first keys part contains [ and the last ends with ], then []
      // are correctly balanced.
      if (/\[/.test(keys[0]) && /\]$/.test(keys[ keys_last])) {
        // Remove the trailing ] from the last keys part.
        keys[ keys_last ] = keys[ keys_last ].replace( /\]$/, '' );

        // Split first keys part into two parts on the [ and add them back onto
        // the beginning of the keys array.
        keys = keys.shift().split('[').concat( keys );

        keys_last = keys.length - 1;
      } else {
        // Basic 'foo' style key.
        keys_last = 0;
      }

      // Are we dealing with a name=value pair, or just a name?
      if ( param.length === 2 ) {
        val = safeDecodeURIComponent( param[1] );

        // Coerce values.
        if ( coerce ) {
          val = val && !isNaN(val)            ? +val              // number
            : val === 'undefined'             ? undefined         // undefined
            : coerce_types[val] !== undefined ? coerce_types[val] // true, false, null
            : val;                                                // string
        }

        if ( keys_last ) {
          // Complex key, build deep object structure based on a few rules:
          // * The 'cur' pointer starts at the object top-level.
          // * [] = array push (n is set to array length), [n] = array if n is
          //   numeric, otherwise object.
          // * If at the last keys part, set the value.
          // * For each keys part, if the current level is undefined create an
          //   object or array based on the type of the next keys part.
          // * Move the 'cur' pointer to the next level.
          // * Rinse & repeat.
          for ( ; i <= keys_last; i++ ) {
            key = keys[i] === '' ? cur.length : keys[i];
            cur = cur[key] = i < keys_last
              ? cur[key] || ( keys[i+1] && isNaN( keys[i+1] ) ? {} : [] )
              : val;
          }
        } else {
          // Simple key, even simpler rules, since only scalars and shallow
          // arrays are allowed.

          if ( Array.isArray( obj[key] ) ) {
            // val is already an array, so push on the next value.
            obj[key].push( val );

          } else if ( obj[key] !== undefined ) {
            // val isn't an array, but since a second value has been specified,
            // convert val into an array.
            obj[key] = [ obj[key], val ];

          } else {
            // val is a scalar.
            obj[key] = val;
          }
        }

      } else if ( key ) {
        // No value was defined, so set something meaningful.
        obj[key] = coerce
          ? undefined
          : '';
      }
    });

    return obj;
  };

  /**
   * Function to perform for the filtering buttons
   */
  function VTCoreImportDemoFilters(mode) {
    switch (mode) {
      case 'clear' :
        $('#log-panel').find('.panel-body').empty();
      break;
      
      case 'all' :
        $('#log-panel').find('.panel-body > div').show();
      break;
      
      case 'success' :
        $('#log-panel').find('.panel-body > div').hide().filter('.text-success').show();
      break;
      
      case 'errors' :
        $('#log-panel').find('.panel-body > div').hide().filter('.text-danger').show();
      break;
      
    }
  }

  Array.prototype.remove = function(x) {
    var i;
    for(i in this){
      if(this[i].toString() == x.toString()){
        this.splice(i,1)
      }
    }
  }
  
  /**
   * Bind everything to document for sane event bubbling
   */
  $(document)

    .on('ready', function() {

      if ($('select[name="demo_site"]').length && $('select[name="demo_site"]').val() == 'None') {
        $('.btn-import').attr('disabled', true);
      }

      // Store the original queue first
      $('.btn-import').data('original-queue', $('.btn-import').data('ajax-queue'))

    })

    .on('shown.bs.modal', function() {
      $(this).find('.js-isotope').isotope('layout');
    })

    .on('click.selector', '[data-toggle-selector="true"]', function(e) {

      $('[data-toggle-selector="true"]').removeClass('active');
      $(this).addClass('active');
      $('select[name="demo_site"]').val($(this).data('toggle-site')).trigger('change');

      $('[data-toggle-preview="true"]').html($(this).html());
    })

    .on('change', 'select[name="demo_site"]', function() {

      if ($(this).val()) {
        $('.btn-import').removeAttr('disabled');
      }
      else {
        $('.btn-import').attr('disabled');
      }

      // Dynamic queue based on plugin dependencies
      var option = $('option[value="' + $(this).val() + '"]', this);
      if (option.data('dependencies')) {

        var queue = [];

        $.each(option.data('dependencies'), function(key, value) {
          queue.push(value);
        });

        $.each($('.btn-import').data('original-queue'), function(key, value) {
          queue.push(value);
        });

        $('.btn-import').data('ajax-queue', queue);

      }


    })

    // Binding the click event for the filtering buttons
    .on('click', '[data-toggle="import-filter"]', function() {
      var mode = $(this).data('filter');
      
      if (mode != 'clear') {
        $('#log-panel').data('filter-mode', mode);
        $('[data-toggle="import-filter"]').removeClass('active');
        $(this).addClass('active');
      }

      VTCoreImportDemoFilters(mode);

    })

    // Filtering only unprocessed processor queues
    .on('click.btn-ajax-demo', '.btn-import', function(e) {

      var queue = $('.btn-import').data('ajax-queue');

      // Remove already completed import processor from queue
      if ($(this).data('ajax-completed')) {
        $.each($(this).data('ajax-completed'), function (key, value) {
          value && queue.remove(key);
        })
        $('.btn-import').data('ajax-queue', queue);
      }

      // Restore the queue back to the original state
      else {
        $('.btn-import').removeData('ajax-queue');
        $('select[name="demo_site"]').trigger('change');
      }

    })

    // Binding on ajax complete events
    .on('ajaxComplete', function() {
      var mode =  $('#log-panel').data('filter-mode');
      VTCoreImportDemoFilters(mode);
    })
    .on('ajaxError', function(event, XHR, Settings, Error) {

      var data = VTCoreDemoDeparam(Settings.data),
          message = 'Ajax Request Failed, Please try re-importing again';

      if (typeof data.queue != 'undefined') {
        message = 'Ajax Request for processing ' + data.queue + ' failed, please retry again.';
      }

      $('#log-panel .panel-body').append('<div class="text-danger">' + message + '</div>');
    });
});