<?php
/*
Plugin Name: VicTheme Demo
Plugin URI: http://victheme.com
Description: Plugin for importing data as the site demo
Author: jason.xie@victheme.com
Version: 1.3.4
Author URI: http://victheme.com
*/

define('VTCORE_DEMO_CORE_VERSION', '1.7.0');

add_action('plugins_loaded', 'bootVicthemeDemo', 11);
add_action('init', 'initVicthemeDemo', 21);

function bootVicthemeDemo() {

  if (!(defined('VTCORE_VERSION') && version_compare(VTCORE_VERSION, VTCORE_DEMO_CORE_VERSION, '='))) {

    add_action('admin_notices', 'VicthemeDemoMissingDemoCoreNotice');

    function VicthemeDemoMissingDemoCoreNotice() {

      if (!defined('VTCORE_VERSION')) {
        $notice = __('VicTheme Demo depends on VicTheme Core Plugin which is not activated or missing, Please enable it first before VicTheme Demo can work properly.');
      }
      elseif (version_compare(VTCORE_VERSION, VTCORE_DEMO_CORE_VERSION, '!=')) {
        $notice = sprintf(__('VicTheme Demo depends on VicTheme Core Plugin API version %s to operate properly.', 'victheme_demo'), VTCORE_DEMO_CORE_VERSION);
      }

      if (isset($notice)) {
        echo '<div class="error""><p>' . $notice . '</p></div>';
      }

    }

    return;
  }

  define('VTCORE_DEMO_LOADED', true);
  define('VTCORE_DEMO_BASE_DIR', dirname(__FILE__));

}


function initVicthemeDemo() {

  if (!defined('VTCORE_ZEUS_THEME_PATH')) {

    add_action('admin_notices', 'VicthemeDemoMissingThemeNotice');

    function VicthemeDemoMissingThemeNotice() {
      echo '<div class="error""><p>' . __('Current Theme activated is not supported for demo import.', 'victheme_demo') . '</p></div>';
    }

    return;
  }

  if (defined('VTCORE_DEMO_LOADED')) {
    include_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'init.php');
    new VTCore_Demo_Init();
  }

}