<?php
/**
 * Processor class for handling the plugin dependencies
 * This class will extract plugin zip from Zeus Plugin library
 * folder and auto activate them if the demo site demands the
 * plugin as dependencies.
 *
 * Additional method setPlugin($pluginName) must be invoked
 * to register the target plugin to be extracted and activated.
 * 
 * @author jason@victheme.com
 *
 */
class VTCore_Demo_Processor_Plugin
extends VTCore_Demo_Models_Processor {

  protected $plugin;
  protected $allPlugins = array();
  protected $installed = false;
  protected $active = false;
  protected $path = false;
  protected $upgrader;
  protected $result;
  protected $access;


  /**
   * Overriding parent method since we don't need
   * to load a config object but instead prepare
   * the plugin data for further processing
   */
  public function loadData() {

    // Check plugin status
    $this
      ->loadAllPlugin()
      ->checkInstalled()
      ->checkActive()
      ->checkFileAccess();

    return !$this->active;
  }


  /**
   * Importing all stored options into the database.
   * This method will not perform any logic for mutating the
   * options value such as changing the post id for attachment.
   *
   *
   * @see VTCore_Demo_Models_Processor::importData()
   */
  public function importData() {

    if ($this->checkMap('plugin', $this->plugin, $this->plugin)) {
      return;
    }

    // Do installation
    if (!$this->installed) {

      // Only do plugin installation if Wordpress detected
      // client server capable to do direct install method.
      if ($this->access == 'direct') {

        $this->installPlugin();

        if (is_wp_error($this->result) || empty($this->result)) {
          $this->addLog(
            sprintf(__( 'Failed to install plugin %s', 'victheme_demo'), $this->plugin), 'text-danger');
        }
        else {
          $this->addLog(
            sprintf(__('Plugin %s installed', 'victheme_demo'), $this->plugin), 'text-success');
        }
      }

      // Report to user that server is not capable to do direct install
      else {
        $this->addLog(
          sprintf(__( 'Cannot install plugin %s, No Direct install method available,
                       please use manual installation via WordPress plugin management page.', 'victheme_demo'), $this->plugin), 'text-danger');
      }
    }

    // Do activation
    if (!$this->active) {

      $this->activatePlugin();

      if (is_wp_error($this->result)) {
        $this->addLog(
          sprintf(__( 'Failed to activate plugin %s', 'victheme_demo'), $this->plugin), 'text-danger');
      }
      else {
        $this->addLog(
          sprintf(__('Plugin %s activated', 'victheme_demo'), $this->plugin), 'text-success');
      }
    }

    $this->setMap('plugin', $this->plugin, $this->plugin);
  }


  /**
   * Method for loading all plugin information to the object
   * @return $this
   */
  protected function loadAllPlugin() {

    if (!function_exists('get_plugins')) {
      require_once ABSPATH . 'wp-admin/includes/plugin.php';
    }

    $this->allPlugins = get_plugins();

    return $this;
  }


  /**
   * Method for checking if plugin installed and inject the
   * plugin file and directory path to the object.
   *
   * @return $this
   */
  protected function checkInstalled() {
    $keys = array_keys($this->allPlugins);
    foreach ($keys as $key) {
      if (strpos($key, $this->plugin) !== false) {
        $this->installed = true;
        $this->path = $key;
        break;
      }
    }

    return $this;
  }


  /**
   * Method for checking if the current plugin is activated
   * or not
   * @return $this
   */
  protected function checkActive() {
    $this->active = is_plugin_active($this->path);
    return $this;
  }


  /**
   * Method for performing the plugin extraction and installation
   * from the theme plugin directory zip file into the normal
   * Wordpress plugins directory.
   *
   * @return $this
   */
  protected function installPlugin() {

    // Reset results
    $this->result = false;

    if (!class_exists('WP_Upgrader', false)) {
      require_once ABSPATH . 'wp-admin/includes/template.php';
      require_once ABSPATH . 'wp-admin/includes/misc.php';
      require_once ABSPATH . 'wp-admin/includes/file.php';
      require_once ABSPATH . 'wp-admin/includes/class-wp-upgrader.php';
    }

    $this->upgrader = new WP_Upgrader();

    // Nuke messages
    $this->upgrader->strings['unpack_package'] = '';
    $this->upgrader->strings['installing_package'] = '';
    $this->upgrader->strings['remove_old'] = '';
    $this->upgrader->skin->set_upgrader($this->upgrader);

    // Somehow this is needed or WP_Upgrader() will complain and break!
    $res = $this->upgrader->fs_connect( array(WP_CONTENT_DIR, VTCORE_ZEUS_PLUGIN_SOURCE_PATH . DIRECTORY_SEPARATOR . $this->plugin . '.zip'));

    $this->result = $this->upgrader->install_package(array(
      'source' => $this->upgrader->unpack_package(VTCORE_ZEUS_PLUGIN_SOURCE_PATH . DIRECTORY_SEPARATOR . $this->plugin . '.zip', FALSE),
      'destination' => WP_PLUGIN_DIR,
      'clear_destination' => TRUE,
      'abort_if_destination_exists' => TRUE,
      'clear_working' => TRUE,
      'hook_extra' => array(),
    ));

    return $this;
  }


  /**
   * Method for determining server file access available
   * for unpacking and installing zip plugin
   * @return $this
   */
  protected function checkFileAccess() {
    $this->access = get_filesystem_method();
    return $this;
  }


  /**
   * Method for activating current plugin.
   * @return $this
   */
  protected function activatePlugin() {

    // Reset result
    $this->result = false;

    // Try to refresh the object for freshly installed plugin
    if (empty($this->path)) {
      $this->loadAllPlugin()->checkInstalled();
    }

    if (!empty($this->path)) {
      $this->result = activate_plugin($this->path);
    }

    return $this;
  }



  /**
   * Removing or reverting the previously imported
   * options entry
   *
   * @see VTCore_Demo_Models_Processor::removeData()
   */
  public function removeData() {
    $this->removeMapByValue('plugin', $this->plugin, $this->plugin);
    return $this;
  }


  /**
   * Method for injecting the plugin name to the object
   * @param $plugin
   */
  public function setPlugin($plugin) {
    $this->plugin = $plugin;
    return $this;
  }

}