<?php
/**
 * Processor class for handling the wp commentmeta table
 * content exported by victheme_exporter module.
 *
 * @author jason@victheme.com
 *
 */
class VTCore_Demo_Processor_Commentmeta
extends VTCore_Demo_Models_Processor {

  protected $processorsDependencies = array(
    'comments',
  );

  /**
   * Importing comment meta entry to the database
   * This must be run after the comment processor.
   *
   * @see VTCore_Demo_Models_Processor::importData()
   */
  public function importData() {

    // Stop processing if this database is marked
    // as imported previously.
    if ($this->checkImportStatus()) {
      return;
    }

    foreach ($this->config->getData() as $cid => $meta) {

      if ($this->checkMap('commentmeta', 'ID', $cid)) {
        continue;
      }

      $meta['comment_id'] = $this->getMappedValue('comments', 'ID', $meta['comment_id']);

      // Save comment
      $result = add_comment_meta($meta['comment_id'], $meta['meta_key'], $meta['meta_value']);

      if ($result !== true) {
        $this->addLog(sprintf(__( 'Failed to import comment %s - %s', 'victheme_demo'), $meta['comment_id'], $meta['meta_key']), 'text-danger');
        $this->reportFailed();
      }
      else {
        $this->addLog(sprintf(__('Comment %s - %s Imported', 'victheme_demo'), $meta['comment_id'], $meta['meta_key']), 'text-success');
        $this->setMap('ID', $cid, $result);
      }

    }

    if ($this->maybeClose()) {
      $this->markImportedStatus();
    }

  }



  /**
   * The actual metadata removal will be invoked when
   * the post is deleted. Thus this function will only
   * remove the import marking and mapping.
   *
   * @see VTCore_Demo_Models_Processor::removeData()
   */
  public function removeData() {
    $this->removeMap('commentmeta');
    $this->removeImportedStatus();
  }

}