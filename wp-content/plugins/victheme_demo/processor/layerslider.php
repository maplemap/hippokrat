<?php
/**
 * Processor class for handling the Layer Slider data
 * content made by victheme_exporter module.
 *
 * @author jason@victheme.com
 *
 */
class VTCore_Demo_Processor_LayerSlider
extends VTCore_Demo_Models_Processor {

  private $widgets;
  private $base;

  protected $dependencies = 'layerslider';
  protected $processorsDependencies = array(
    'options',
    'posts',
    'attachment',
    'widgets',
  );

  /**
   * Overriding parent method to use Post config
   * All Rev Slider data class must exist eventhough it is empty.
   *
   * @see VTCore_Demo_Models_Processor::loadData()
   */
  public function loadData() {

    $process = false;
    $className = $this->prefix . ucfirst(str_replace('_', '__', $this->name));

    if (class_exists($className, true)) {
      $this->config = new $className();
      $process = true;
    }

    $widgets = get_option('widget_layerslider_widget', array());
    if (!empty($widgets)) {
      $this->widgets = $widgets;
      $process = true;
    }

    return $process;
  }





  /**
   * Injecting the revolution slider data taken from the
   * victheme_exporter plugin into the current database
   * and also try to import the media files into the
   * upload folder as the revolution slider data structure
   * requires.
   *
   * @see VTCore_Demo_Models_Processor::importData()
   */
  public function importData() {

    // Stop processing if this database is marked
    // as imported previously.
    if ($this->checkImportStatus()) {
      return;
    }

    // Retrive new base url
    $this->base = get_option('siteurl');

    // Import css
    if (!empty($this->config)) {
      $this->importSliders();
      $this->updateShortcode();
    }

    if (!empty($this->widgets)) {
      $this->updateWidgets();
    }

    if ($this->maybeClose()) {
      $this->markImportedStatus();
    }
  }



  /**
   * Updating widgets to point to the mapped slider instances.
   */
  private function updateWidgets() {

    if ($this->checkMap('layerslider', 'widgets', 'updated')) {
      return;
    }

    foreach ($this->widgets as $id => $widget) {

      if (empty($widget)
          || !is_numeric($id)
          || !$this->checkMap('layerslider', 'sliders', $widget['id'])) {
        continue;
      }

      $this->widgets[$id]['id'] = $this->getMappedValue('layerslider', 'sliders', $widget['id']);
    }

    $result = update_option('widget_layerslider_widget', $this->widgets);

    if ($result) {
      $this->addLog(__('Layer slider widget instances updated', 'victheme_demo'), 'text-success');
      $this->setMap('widgets', 'updated', TRUE);
    }

  }






  /**
   * This method will attempt to inject the sliders into
   * the current database and try to convert and / or retrieve
   * attached background image.
   */
  private function importSliders() {

    global $wpdb;

    // Build the sliders
    foreach ($this->config->getData() as $id => $data) {

      if ($this->checkMap('layerslider', 'sliders', $id)) {
        continue;
      }

      // Id is autoincremental database field remove and get the convertion later
      unset($data['id']);

      $param = json_decode($data['data'], false);

      foreach ($param->layers as $object) {

        // Convert localy stored background
        if (!empty($object->properties->backgroundId) && is_numeric($object->properties->backgroundId)) {
          $object->properties->backgroundId = $this->getMappedValue('posts', 'ID', $object->properties->backgroundId);
          $object->properties->background = wp_get_attachment_url($object->properties->backgroundId);
        }

        // Convert localy stored thumbnail
        if (!empty($object->properties->thumbnailId) && is_numeric($object->properties->thumbnailId)) {
          $object->properties->thumbnailId = $this->getMappedValue('posts', 'ID', $object->properties->thumbnailId);
          $object->properties->thumbnail = wp_get_attachment_url($object->properties->thumbnailId);
        }

        // Process the sublayer
        foreach ($object->sublayers as $sublayer) {
          if (!empty($sublayer->imageId) && !empty($sublayer->imageId)) {
            $sublayer->imageId = $this->getMappedValue('posts', 'ID', $sublayer->imageId);
            $sublayer->image = wp_get_attachment_url($sublayer->imageId);
          }
        }
      }

      $data['data'] = json_encode($param);

      $result = $wpdb->insert($wpdb->prefix . 'layerslider', $data);

      if (!$result) {
        $this->addLog(sprintf(__('Failed to insert layer slider instance id: %s to database', 'victheme_demo'), $id), 'text-danger');
        $this->reportFailed();
      }
      else {
        $this->setMap('sliders', $id, $wpdb->insert_id);
        $this->addLog(sprintf(__('Layer slider instance id: %s imported to database', 'victheme_demo'), $id), 'text-success');
      }

    }

  }


  /**
   * Updating layer slider shortcodes
   * currently it only updates VC provided shortcodes
   */
  private function updateShortcode() {

    global $wpdb;

    foreach ($this->getMap('layerslider', 'sliders', array()) as $oldID => $newID) {

      if ($this->checkMap('layerslider', 'shortcodes', $oldID)) {
        continue;
      }


      $oldShortcode = '[layerslider_vc id="' . $oldID . '"';
      $newShortcode = '[layerslider_vc id="' . $newID . '"';

      $result = $wpdb->query($wpdb->prepare("UPDATE {$wpdb->posts} SET post_content = REPLACE(post_content, %s, %s)", array($oldShortcode, $newShortcode)));

      // No shortcode will return 0 while error will return false!
      if ($result === false) {
        $this->addLog(sprintf(__('Failed updating LayerSlider %s shortcodes', 'victheme_demo'), $oldID), 'text-danger');
        $this->reportFailed();
      }
      elseif ($result === true) {
        $this->addLog(sprintf(__('LayerSlider %s shortcodes updated', 'victheme_demo'), $oldID), 'text-success');
        $this->setMap('shortcodes', $oldID, $newID);
      }
    }

  }


  /**
   * The actual metadata removal will be invoked when
   * the post is deleted. Thus this function will only
   * remove the import marking and mapping.
   *
   * Widgets will be processed by options processor
   *
   * @see VTCore_Demo_Models_Processor::removeData()
   */
  public function removeData() {

    global $wpdb;

    foreach ($this->getMap('layerslider', 'sliders', array()) as $oldID => $newID) {
      $result = $wpdb->delete($wpdb->prefix.'layerslider', array('id' => (int) $newID), array('%d'));

      if ($result) {
        $this->addLog(sprintf(__('Layer slider CSS id: %s removed from database', 'victheme_demo'), $newID), 'text-success');
        $this->removeMapByValue('layerslider', 'sliders', $oldID);
        $this->removeMapByValue('layerslider', 'shortcodes', $oldID);
      }
      else {
        $this->addLog(sprintf(__('Failed to remove Layer slider CSS id: %s', 'victheme_demo'), $newID), 'error');
      }
    }

    if ($this->removeMapWhenEmpty('layerslider', 'sliders')) {
      $this->removeImportedStatus();
    }

  }

}