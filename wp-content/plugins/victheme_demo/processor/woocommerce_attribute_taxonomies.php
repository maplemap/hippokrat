<?php
/**
 * Processor for processing WooCommerce custom
 * product attribute system
 *
 * @authod jason.xie@victheme.com
 */
class VTCore_Demo_Processor_Woocommerce__Attribute__Taxonomies
extends VTCore_Demo_Models_Processor  {

  protected $dependencies = 'woocommerce';

  public function importData() {
    if ($this->checkImportStatus()) {
      return;
    }

    global $wpdb;

    foreach ($this->config->getData() as $aid => $meta) {
      if ($this->checkMap('woocommerce_attribute_taxonomies', 'termID', $aid)) {
        continue;
      }

      unset($meta['attribute_id']);
      $result = $wpdb->insert("{$wpdb->prefix}woocommerce_attribute_taxonomies", $meta);

      if (!empty($result)) {
        $this->setMap('termID', $aid, $wpdb->insert_id);
        $this->addLog(sprintf(__('WC Attribute %s inserted to database', 'victheme_demo'), $aid), 'text-success');

      }
      else {
        $this->addLog(sprintf(__('Failed to insert WC Attribute %s to database', 'victheme_demo'), $aid), 'text-error');
        $this->reportFailed();
      }
    }

    // Force refresh so processor terms can pickup the
    // newly injected dynamic attributes taxonomy
    // @see class-wp-post-types.php for the invocation
    // @see wp_get_attribute_taxonomies() for the transient
    delete_transient('wc_attribute_taxonomies');

    if ($this->maybeClose()) {
      $this->markImportedStatus();
    }
  }

  public function removeData() {

    global $wpdb;
    foreach ($this->getMap('woocommerce_attribute_taxonomies', 'termID', array()) as $oldID => $newID) {

      $wpdb->delete("{$wpdb->prefix}woocommerce_attribute_taxonomies", array(
        'attribute_id' => $newID,
      ));

      $this->addLog(sprintf(__('WC Attribute %s deleted from database', 'victheme_demo'), $newID), 'text-success');
      $this->removeMapByValue('woocommerce_attribute_taxonomies', 'termID', $oldID);
    }

    $this->removeMap('woocommerce_attribute_taxonomies');
    $this->removeImportedStatus();

    delete_transient('wc_attribute_taxonomies');
  }

}

