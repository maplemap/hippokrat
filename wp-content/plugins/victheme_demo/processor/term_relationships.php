<?php
/**
 * Processor class for handling the wp terms table
 * content exported by victheme_exporter module.
 *
 * @author jason@victheme.com
 *
 */
class VTCore_Demo_Processor_Term__Relationships
extends VTCore_Demo_Models_Processor {

  private $term_relationships;
  private $term_taxonomy;
  protected $processorsDependencies = array(
    'posts',
    'terms',
  );


  /**
   * Overriding parent method.
   * This is needed because we need to merge
   * terms and term_taxonomy data before we can
   * actually inject the taxonomy into the database.
   *
   * This method also remap the taxonomy terms id
   * to the actual one stored in the database.
   *
   * @see VTCore_Demo_Models_Processor::loadData()
   */
  public function loadData() {

    $this->config = array();

    $relationshipClassName = $this->prefix . 'Term__Relationships';
    $taxonomyClassName = $this->prefix . 'Term__Taxonomy';

    if (class_exists($relationshipClassName, true)
        && class_exists($taxonomyClassName, true)) {

      $this->term_relationships = new $relationshipClassName();
      $this->term_taxonomy = new $taxonomyClassName();

      $taxonomyObject = $this->term_taxonomy->getData();

      foreach ($this->term_relationships->getData() as $delta => $relationship) {

        // Invalid taxonomy
        if (!isset($taxonomyObject[$relationship['term_taxonomy_id']]['term_id'])
            || !isset($taxonomyObject[$relationship['term_taxonomy_id']]['taxonomy'])
            || !$this->checkMap('posts', 'ID', $relationship['object_id'])
            || !$this->checkMap('terms', 'term_taxonomy_id', $relationship['term_taxonomy_id'])) {

          continue;
        }

        $term_id = $taxonomyObject[$relationship['term_taxonomy_id']]['term_id'];
        $object_id = $this->getMappedValue('posts', 'ID', $relationship['object_id']);
        $id = $this->getMappedValue('terms', 'ID', $term_id);
        $taxonomy = $taxonomyObject[$relationship['term_taxonomy_id']]['taxonomy'];

        $this->config[$object_id]['taxonomy'] = $taxonomy;
        $this->config[$object_id]['ids'][] = $id;

      }
    }

    return !empty($this->config) ? true : false;
  }




  /**
   * Importing the terms relationship into database.
   *
   * @see VTCore_Demo_Models_Processor::importData()
   */
  public function importData() {

    // Stop processing if this database is marked
    // as imported previously.
    if ($this->checkImportStatus() || empty($this->config)) {
      return;
    }

    // Inject the terms to database
    foreach ($this->config as $object_id => $taxonomy) {

      // Skip if this meta already processed and mapped.
      if ($this->checkMap('term_relationships', 'ID', $object_id)) {
        continue;
      }

      foreach ($taxonomy['ids'] as $term) {
        $result = wp_add_object_terms($object_id, $term, $taxonomy['taxonomy']);

        if (is_wp_error($result)) {
          $this->addLog(sprintf(__('Failed to build relationship for object %s - %s', 'victheme_demo'), $object_id, $taxonomy['taxonomy']), 'text-danger');
          $this->reportFailed();
        }
        else {
          $this->addLog(sprintf(__('Relationship for object %s - %s updated', 'victheme_demo'), $object_id, $taxonomy['taxonomy']), 'text-success');
          $this->setMap('ID', $object_id, $taxonomy);
        }
      }
    }

    if ($this->maybeClose()) {
      $this->markImportedStatus();
    }

  }





  /**
   * Only remove the marking of importation as the actual
   * data removal is handled by taxonomy and posts removal
   * process.
   *
   * @see VTCore_Demo_Models_Processor::removeData()
   */
  public function removeData() {

    $this->removeMap('term_relationships');
    $this->removeImportedStatus();

  }

}