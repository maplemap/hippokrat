<?php
/**
 * Processor for processing members
 * entry from victheme members plugin
 *
 * @author jason@victheme.com
 *
 */
class VTCore_Demo_Processor_Members
extends VTCore_Demo_Models_Processor {

  protected $dependencies = 'victheme_members';
  protected $processorsDependencies = array(
    'posts',
    'postmeta',
    'terms',
    'attachment',
    'department',
    'services',
  );

  /**
   * Overriding parent method to use Post config
   * @see VTCore_Demo_Models_Processor::loadData()
   */
  public function loadData() {
    $postName = $this->prefix . 'Posts';

    if (class_exists($postName, TRUE)) {
      $this->posts = new $postName();
      return TRUE;
    }

    return FALSE;
  }


  /**
   * The main controller method for performing
   * the data remapping.
   *
   * This has to run after the content processor.
   *
   * @see VTCore_Demo_Models_Processor::importData()
   */
  public function importData() {

    // Stop processing if this database is marked
    // as imported previously.
    if ($this->checkImportStatus()) {
      return;
    }

    // Process post meta
    foreach ($this->posts->getData() as $id =>$post) {

      if ($post['post_type'] != 'members' || $this->checkMap('members', 'ID', $id)) {
        continue;
      }

      $newId = $this->getMappedValue('posts', 'ID', $id);
      $meta = get_post_meta((int) $newId, '_members_department', true);
      if (!empty($meta)) {
        update_post_meta((int) $newId, '_members_department', (int) $this->getMappedValue('posts', 'ID', $meta));
      }

      $this->setMap('members', 'ID', $id);
    }


    if ($this->maybeClose()) {
      $this->markImportedStatus();
    }

  }

  /**
   * Remove the marking in database
   * @see VTCore_Demo_Models_Processor::removeData()
   */
  public function removeData() {
    $this->removeMap('members');
    $this->removeImportedStatus();
  }

}