<?php

/**
 * Processor for processing Custom teaser
 * data, especially fixing the media (image and video)
 * attachment post id since it will change
 * from the original post id into the mapped
 * new post id.
 *
 * @author jason@victheme.com
 *
 */
class VTCore_Demo_Processor_Teaser
  extends VTCore_Demo_Models_Processor {

  private $templateObjects;
  protected $dependencies = 'victheme_teaser';
  protected $processorsDependencies = array(
    'posts',
    'postmeta',
    'attachment',
  );

  /**
   * Overriding parent method to use Post config
   * @see VTCore_Demo_Models_Processor::loadData()
   */
  public function loadData() {

    $metaName = $this->prefix . 'Postmeta';
    $postName = $this->prefix . 'Posts';
    $templateObjects = 'VTCore_Teaser_Templates';

    if (class_exists($metaName, TRUE)
      && class_exists($postName, TRUE)
      && class_exists($templateObjects, TRUE)
    ) {

      $this->config = new $metaName();
      $this->posts = new $postName();
      $this->templateObjects = new $templateObjects();
      return TRUE;
    }

    return FALSE;
  }


  /**
   * The main controller method for performing
   * the data remapping.
   *
   * This has to run after the content processor.
   *
   * @see VTCore_Demo_Models_Processor::importData()
   */
  public function importData() {

    // Stop processing if this database is marked
    // as imported previously.
    if ($this->checkImportStatus()) {
      return;
    }

    // Process post meta
    foreach ($this->config->getData() as $meta) {

      if ($meta['meta_key'] != '_vtcore_teaser_data' || $this->checkMap('teaser', 'ID', $meta['post_id'])) {
        continue;
      }

      $this->templateObjects->buildTemplateObject($meta['meta_value']['template']);
      foreach ($this->templateObjects->getObject()->findChildren('context', 'section') as $insertPoint) {
        foreach ($insertPoint->getContext('contents') as $contentArray) {
          if ($contentArray['mode'] == 'image' || $contentArray['mode'] == 'video') {
            $meta['meta_value'][$contentArray['id']] = $this->getMappedValue('posts', 'ID', $meta['meta_value'][$contentArray['id']]);
          }
        }
      }

      $newPostId = $this->getMappedValue('posts', 'ID', $meta['post_id']);
      $update = update_post_meta($newPostId, '_vtcore_teaser_data', $meta['meta_value']);
      if ($update) {
        $this->addLog(sprintf(__('Post - %s custom teaser data media remapped', 'victheme_demo'), $newPostId), 'text-success');
        $this->setMap('ID', $meta['post_id'], $newPostId);
      }
      else {
        $this->addLog(sprintf(__('Failed to remap Post - %s custom teaser data media', 'victheme_demo'), $newPostId), 'text-danger');
        $this->reportFailed();
      }
    }

    if ($this->maybeClose()) {
      $this->markImportedStatus();
    }
  }


  /**
   * Function for building the template layout and inject
   * the correct corresponding form element into the template
   * marker position.
   */
  private function buildTemplateForm() {

    $this->templates->buildTemplateObject($this->template);

    foreach ($this->templates->getObject()
               ->findChildren('context', 'section') as $insertPoint) {

      foreach ($insertPoint->getContext('contents') as $contentArray) {
        switch ($contentArray['mode']) {

          case 'image' :

            break;

          case 'video' :


            break;

        }
      }
    }
  }


  /**
   * Remove the marking in database
   * @see VTCore_Demo_Models_Processor::removeData()
   */
  public function removeData() {
    if ($this->checkImportStatus() == FALSE) {
      return;
    }

    $this->removeMap('teaser');
    $this->removeImportedStatus();

  }

}