<?php
/**
 * Processor class for handling the Visual Composer Post Meta
 * and repair the templates exported by victheme_exporter module.
 *
 * @author jason@victheme.com
 *
 */
class VTCore_Demo_Processor_VCMeta
extends VTCore_Demo_Models_Processor {

  protected $processorsDependencies = array(
    'posts',
    'postmeta',
    'attachment',
  );

  private $singles = array(
    'vc_single_image' => 'image',
    'vmmarker' => 'icon',
    'bsmediaobject' => 'img',
    'jqflipcontent' => 'image',
    'wptestimonial' => 'image',
    'wpcarouselimage' => 'image',
    'vpgallery' => 'postid',
    'vpproject' => 'postid',
    'google_marker' => 'icon',
    'google_maps' => 'icon',
  );


  private $multipleByComma = array(
    'vc_gallery' => 'images',
    'vc_images_carousel' => 'images',
  );


  /**
   * Don't process vc shortcode item attributes
   * for these tags, this will be used in
   * the vc meta auto process.
   * @var array
   */
  private $skipItemTag = array(
    'vc_basic_grid',
  );


  private $oldBaseUrl;
  private $baseUrl;
  private $templates;

  protected $dependencies = 'js_composer';

  /**
   * Overriding parent method to use Post config
   * @see VTCore_Demo_Models_Processor::loadData()
   */
  public function loadData() {

    $className = $this->prefix . 'Posts';
    $metaClassName = $this->prefix .'PostMeta';

    if (class_exists($className, true)
        && class_exists($metaClassName, true)) {

      $this->config = new $className();
      $this->meta = new $metaClassName();
      $this->templates = get_option('wpb_js_templates', array());
      $this->oldBaseUrl = $this->config->getBase();
      $this->baseUrl = get_option('siteurl');

      return true;
    }

    return false;
  }



  /**
   * Importing the post meta data to the database.
   *
   * @see VTCore_Demo_Models_Processor::importData()
   */
  public function importData() {

    // Stop processing if this database is marked
    // as imported previously.
    if ($this->checkImportStatus()) {
      return;
    }

    global $wpdb;

    // Processing Visual Composer Meta
    // Required as VC 4.4.x
    foreach ($this->meta->getData() as $metaid => $meta) {

      if (!isset($meta['meta_key']) || $this->checkMap('vcmeta', '_vc_post_settings', $metaid) || $meta['meta_key'] != '_vc_post_settings') {
        continue;
      }

      $meta['post_id'] = $this->getMappedValue('posts', 'ID', $meta['post_id']);

      if (isset($meta['meta_value']['vc_grid']['shortcodes'])) {
        foreach ($meta['meta_value']['vc_grid']['shortcodes'] as $key => $shortcode) {

          // Custom grid post id
          if (isset($shortcode['atts']['item'])) {
            $meta['meta_value']['vc_grid']['shortcodes'][$key]['atts']['item'] = $this->getMappedValue('posts', 'ID', $shortcode['atts']['item']);
          }
        }
      }

      if (isset($meta['meta_value']['vc_grid_id']['shortcodes'])) {
        foreach ($meta['meta_value']['vc_grid_id']['shortcodes'] as $key => $shortcode) {

          // Don't process tag marked for skip as it may break the shortcode.
          if (in_array($shortcode['tag'], $this->skipItemTag)) {
            continue;
          }

          // Custom grid post id
          if (isset($shortcode['atts']['item'])) {
            $meta['meta_value']['vc_grid_id']['shortcodes'][$key]['atts']['item'] = $this->getMappedValue('posts', 'ID', $shortcode['atts']['item']);
          }
        }
      }

      $result = update_post_meta($meta['post_id'], '_vc_post_settings', $meta['meta_value']);
      if ($result !== false) {
        $this->addLog(__('Visual Composer Post Settings updated', 'victheme_demo'), 'text-success');
      }

      $this->setMap('_vc_post_settings', $metaid, $result);

    }

    // Processing Visual Composer Template
    foreach ($this->config->getData() as $pid => $post) {

      if ($this->checkMap('vcmeta', 'ID', $pid) || $post['post_type'] != 'attachment') {
        continue;
      }

      $newPostID = $this->getMappedValue('posts', 'ID', $pid);
      $newString = '?id=' . $newPostID . ')';
      $oldString = '?id=' . $pid . ')';


      // Global nuking the background image in the post meta
      $wpdb->query( $wpdb->prepare("UPDATE {$wpdb->postmeta} SET meta_value = REPLACE(meta_value, %s, %s) WHERE meta_key='_wpb_shortcodes_custom_css'", array($oldString, $newString)));
      $wpdb->query( $wpdb->prepare("UPDATE {$wpdb->postmeta} SET meta_value = REPLACE(meta_value, %s, %s) WHERE meta_key='_visualplus_custom_css'", array($oldString, $newString)));


      // Updating visual composer template stored in option table
      foreach ($this->templates as $id => $data) {
        $this->templates[$id]['template'] = str_replace($oldString, $newString, str_replace($this->oldBaseUrl, $this->baseUrl, $data['template']));
      }

      update_option('wpb_js_templates', $this->templates);

      $message = __('Visual Composer Templates updated', 'victheme_demo');
      $this->addLog($message, 'text-success');

      $this->setMap('ID', $pid, true);

    }


    // Remap the template
    if (!$this->checkMap('vcmeta', 'remapped', 'status')) {

      foreach ($this->templates as $id => $data) {

        $content = $data['template'];

        // Process single post id replacement
        foreach ($this->singles as $tag => $atts) {

          $atts = (array) $atts;

          foreach ($atts as $att) {
            $pattern = '/\[(' . $tag . ')\b(.*?)(?:(' . $att . '="[d]"))?\]/';
            preg_match_all($pattern, $content, $matches);

            foreach ($matches[0] as $original) {
              $subpattern = '/' . $att . '=\s*"([^"]*)"/';
              preg_match_all($subpattern, $original, $values);

              foreach ($values[1] as $value) {

                $replacementID = $this->getMappedValue('posts', 'ID', $value);
                $replacement = str_replace($att . '="' . $value . '"', $att . '="' . $replacementID . '"', $original);
                $content = str_replace($original, $replacement, $content);
              }
            }
          }
        }

        // Process multiple attachment separated with comma
        foreach ($this->multiplePostComma as $tag => $atts) {
          $atts = (array) $atts;
          foreach ($atts as $att) {
            $pattern = '/\[(' . $tag . ')\b(.*?)(?:(' . $att . '="[d,]"))?\]/';
            preg_match_all($pattern, $content, $shortcodes);

            foreach ($shortcodes[0] as $shortcode) {
              $subpattern = '/' . $att . '=\s*"([^"]*)"/';
              preg_match_all($subpattern, $shortcode, $matches);

              foreach ($matches[1] as $match) {
                $replacement = explode(',', $match);
                foreach ($replacement as $key => $value) {
                  $replacement[$key] = $this->getMappedValue('posts', 'ID', trim($value));
                }

                $replacement = $att . '="' . implode(',', $replacement) . '"';
                $newcode = str_replace($att . '="' . $match . '"', $replacement, $shortcode);
                $content = str_replace($shortcode, $newcode, $content);
              }
            }
          }
        }

        $this->templates[$id]['template'] = $content;

      }

      update_option('wpb_js_templates', $this->templates);

      $message = __('Visual Composer Templates Remapped', 'victheme_demo');
      $this->addLog($message, 'text-success');

      $this->setMap('remapped', 'status', TRUE);
    }

    if ($this->maybeClose()) {
      $this->markImportedStatus();
    }

  }



  /**
   * The actual metadata removal will be invoked when
   * the post is deleted. Thus this function will only
   * remove the import marking and mapping.
   *
   * @see VTCore_Demo_Models_Processor::removeData()
   */
  public function removeData() {

    $this->removeMap('vcmeta');
    $this->removeImportedStatus();

  }

}
