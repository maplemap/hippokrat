<?php
/**
 * Processor class for handling menu
 *
 * @author jason@victheme.com
 *
 */
class VTCore_Demo_Processor_Menu
extends VTCore_Demo_Models_Processor {

  protected $processorsDependencies = array(
    'posts',
    'postmeta',
    'terms',
  );

  /**
   * Overriding parent method.
   *
   * @see VTCore_Demo_Models_Processor::loadData()
   */
  public function loadData() {

    if (class_exists($this->prefix . 'Posts')) {
      $name = $this->prefix . 'Posts';
      $this->config = new $name();
    }

    return !empty($this->config) ? true : false;
  }

  /**
   * Importing the post meta data to the database.
   *
   * @see VTCore_Demo_Models_Processor::importData()
   */
  public function importData() {

    // Stop processing if this database is marked
    // as imported previously.
    if ($this->checkImportStatus()) {
      return;
    }


    foreach ($this->config->getData() as $pid => $post) {

      if ($this->checkMap('menu', 'ID', $pid)
          || $post['post_type'] != 'nav_menu_item') {
        continue;
      }

      $newID = $this->getMappedValue('posts', 'ID', $pid);
      $menuType = get_post_meta($newID, '_menu_item_type', true);
      $menuObjectID = get_post_meta($newID, '_menu_item_object_id', true);

      switch($menuType) {
        case 'taxonomy' :
          $menuObjectID = $this->getMappedValue('terms', 'ID', $menuObjectID);
          break;

        case 'custom' :
        case 'post_type' :
          $menuObjectID = $this->getMappedValue('posts', 'ID', $menuObjectID);
          break;
      }

      $result = update_post_meta($newID, '_menu_item_object_id', $menuObjectID);

      if ($result !== false) {
        $this->addLog(sprintf(__('Menu %s Updated', 'victheme_demo'), $newID), 'text-success');
        $this->setMap('ID', $pid, $newID);
      }
      else {
        $this->addLog(sprintf(__('Failed updating Menu %s', 'victheme_demo'), $pid), 'text-danger');
        $this->reportFailed();
      }
    }

    if ($this->maybeClose()) {
      $this->markImportedStatus();
    }

  }



  /**
   * The actual metadata removal will be invoked when
   * the post is deleted. Thus this function will only
   * remove the import marking and mapping.
   *
   * Metadata removal and post removal is handled by
   * posts processor
   *
   * @see VTCore_Demo_Models_Processor::removeData()
   */
  public function removeData() {
    $this->removeMap('menu');
    $this->removeImportedStatus();
  }

}