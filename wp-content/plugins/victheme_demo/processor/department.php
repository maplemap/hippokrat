<?php
/**
 * Processor for processing department
 * entry from victheme department plugin
 *
 * @author jason@victheme.com
 *
 */
class VTCore_Demo_Processor_Department
extends VTCore_Demo_Models_Processor {

  protected $dependencies = 'victheme_department';
  protected $processorsDependencies = array(
    'posts',
    'postmeta',
    'users',
    'terms',
    'attachment',
  );

  /**
   * Overriding parent method to use Post config
   * @see VTCore_Demo_Models_Processor::loadData()
   */
  public function loadData() {
    $postName = $this->prefix . 'Posts';

    if (class_exists($postName, TRUE)) {
      $this->posts = new $postName();
      return TRUE;
    }

    return FALSE;
  }


  /**
   * The main controller method for performing
   * the data remapping.
   *
   * This has to run after the content processor.
   *
   * @see VTCore_Demo_Models_Processor::importData()
   */
  public function importData() {

    // Stop processing if this database is marked
    // as imported previously.
    if ($this->checkImportStatus()) {
      return;
    }

    // Process post meta
    foreach ($this->posts->getData() as $id =>$post) {

      if ($post['post_type'] != 'department' || $this->checkMap('department', 'ID', $id)) {
        continue;
      }

      $newId = $this->getMappedValue('posts', 'ID', $post['ID']);

      // Updating department information
      $meta = get_post_meta((int) $newId, '_department_information', true);
      $object = new VTCore_Wordpress_Objects_Array($meta);

      if ($object->get('headers.image')) {
        $object->add('headers.image', $this->getMappedValue('posts', 'ID', $object->get('headers.image')));
      }

      if ($object->get('teasers.image')) {
        $object->add('teasers.image', $this->getMappedValue('posts', 'ID', $object->get('teasers.image')));
      }

      if ($object->get('promotional.video.videolocal') == 'local'
          && $object->get('promotional.video.attachment_id')) {
        $object->add('promotional.video.attachment_id', $this->getMappedValue('posts', 'ID', $object->get('promotional.video.attachment_id')));
      }

      if ($object->get('director')) {
        $object->add('director', $this->convertDepartmentID($object->get('director')));
      }

      if ($object->get('members')) {
        foreach ((array) $object->get('members') as $delta => $value) {
          $object->add('members.' . $delta, $this->convertDepartmentID($value));
        }
      }

      update_post_meta((int) $newId, '_department_information', $object->extract());

      // Updating department head
      if ($meta = get_post_meta((int) $newId, '_department_head', true)) {
        update_post_meta((int) $newId, '_department_head', $this->convertDepartmentID($meta));
      }

      // Updating department services
      $metas = $this->getMeta('_department_services', $id, 'publish');
      foreach ($metas as $post_id) {
        update_post_meta((int) $post_id, '_department_services', (int) $newId);
      }

      $this->setMap('department', 'ID', $id);
    }


    if ($this->maybeClose()) {
      $this->markImportedStatus();
    }

  }


  /**
   * Method for retrieving stored wordpress meta post id
   * by defining the metakey, meta value and post status
   * @param $key
   *  string meta key
   * @param $value
   *  string meta value
   * @param string $status
   *  string post status
   * @return array
   */
  protected function getMeta($key, $value, $status = 'publish') {
    global $wpdb;
    $results = $wpdb->get_col(
      $wpdb->prepare("
        SELECT m.post_id
        FROM $wpdb->postmeta as m
        JOIN $wpdb->posts as p
        ON m.post_id = p.ID
        WHERE m.meta_key = %s
        AND m.meta_value = %s
        AND p.post_status = %s
      ", $key, $value, $status));

    foreach ($results as $key => $value) {
      $results[$key] = (int) $value;
    }

    return $results;
  }

  /**
   * Some of the department stored id can be either
   * user id, post id or term id based on if any
   * plugin modifies it, by default it will use
   * user.
   *
   * @param $value
   * @return bool
   */
  protected function convertDepartmentID($value) {
    switch (VTCORE_DEPARTMENT_DATA_MODE) {
      case 'post' :
        $value = $this->getMappedValue('posts', 'ID', $value);
        break;

      case 'user' :
        $value = $this->getMappedValue('users', 'ID', $value);
        break;

      case 'term' :
        $value = $this->getMappedValue('terms', 'ID', $value);
        break;
    }

    return $value;
  }


  /**
   * Remove the marking in database
   * @see VTCore_Demo_Models_Processor::removeData()
   */
  public function removeData() {
    $this->removeMap('department');
    $this->removeImportedStatus();
  }

}