<?php
/**
 * Processor class for handling the Revolution Slider data
 * content made by victheme_exporter module.
 *
 * @author jason@victheme.com
 *
 */
class VTCore_Demo_Processor_RevSlider
extends VTCore_Demo_Models_Processor {

  private $css;
  private $slides;
  private $settings;
  private $animations;
  private $widgets;
  private $base;
  private $staticSlider;


  protected $dependencies = 'revslider';
  protected $processorsDependencies = array(
    'posts',
    'widgets',
  );

  /**
   * Overriding parent method to use Post config
   * All Rev Slider data class must exist eventhough it is empty.
   *
   * @see VTCore_Demo_Models_Processor::loadData()
   */
  public function loadData() {

    $process = false;

    if (class_exists($this->prefix . 'Revslider__Sliders', true)) {
      $name = $this->prefix . 'Revslider__Sliders';
      $this->config = new $name();
      $process = true;
    }

    if (class_exists($this->prefix . 'Revslider__Css', true)) {
      $name = $this->prefix . 'Revslider__Css';
      $this->css = new $name();
      $process = true;
    }

    if (class_exists($this->prefix . 'Revslider__Slides', true)) {
      $name = $this->prefix . 'Revslider__Slides';
      $this->slides = new $name();
      $process = true;
    }

    if (class_exists($this->prefix . 'Revslider__Static__Slides', true)) {
      $name = $this->prefix . 'Revslider__Static__Slides';
      $this->staticSlider = new $name();
      $process = true;
    }

    if (class_exists($this->prefix . 'Revslider__Settings', true)) {
      $name = $this->prefix . 'Revslider__Settings';
      $this->settings = new $name();
      $process = true;
    }

    if (class_exists($this->prefix . 'Revslider__Layer__Animations', true)) {
      $name = $this->prefix . 'Revslider__Layer__Animations';
      $this->animations = new $name();
      $process = true;
    }

    $widgets = get_option('widget_rev-slider-widget', array());
    if (!empty($widgets)) {
      $this->widgets = $widgets;
      $process = true;
    }

    return $process;
  }





  /**
   * Injecting the revolution slider data taken from the
   * victheme_exporter plugin into the current database
   * and also try to import the media files into the
   * upload folder as the revolution slider data structure
   * requires.
   *
   * @see VTCore_Demo_Models_Processor::importData()
   */
  public function importData() {

    // Stop processing if this database is marked
    // as imported previously.
    if ($this->checkImportStatus())  {
      return;
    }

    global $wpdb;

    // Retrive new base url
    $this->base = get_option('siteurl');

    // Import css
    if (!empty($this->css)) {
      $this->importCSS();
    }

    // Import Sliders
    if (!empty($this->config)) {
      $this->importSliders();
    }

    if (!empty($this->animations)) {
      $this->importAnimations();
    }

    // Import Slides
    if (!empty($this->slides)) {
      $this->importSlides();
    }

    // Import Static
    if (!empty($this->staticSlider)) {
      $this->importStatic();
    }

    if (!empty($this->settings)) {
      $this->updateSettings();
    }

    if (!empty($this->widgets)) {
      $this->updateWidgets();
    }

    if ($this->maybeClose()) {
      $this->markImportedStatus();
    }

  }



  /**
   * Updating widgets to point to the mapped slider instances.
   */
  private function updateWidgets() {

    if ($this->checkMap('revslider', 'widgets', 'updated')) {
      return;
    }


    foreach ($this->widgets as $id => $widget) {

      if (empty($widget)
          || !is_numeric($id)
          || !$this->checkMap('revslider', 'sliders', $widget['rev_slider'])) {
        continue;
      }

      $this->widgets[$id]['rev_slider'] = $this->getMappedValue('revslider', 'sliders', $widget['rev_slider']);
    }

    $result = update_option('widget_rev-slider-widget', $this->widgets);

    if ($result) {
      $this->addLog(__('Revolution slider widget instances updated', 'victheme_demo'), 'text-success');
      $this->setMap('widgets', 'updated', true);
    }
    else {
      $this->addLog(__('Failed to update Revolution slider widget instances', 'victheme_demo'), 'text-danger');
      $this->reportFailed();
    }

  }



  /**
   * Importing custom animations
   * This must be run before importing the slides because
   * the slides layer need to have their link to custom animation
   * updated.
   */
  private function importAnimations() {

    global $wpdb;

    foreach ($this->animations->getData() as $id => $data) {

      if ($this->checkMap('revslider', 'animation', $id)) {
        continue;
      }

      unset($data['id']);

      $result = $wpdb->insert($wpdb->prefix .'revslider_layer_animations', $data);

      if (!$result) {
        $this->reportFailed();
        $this->addLog(sprintf(__('Failed to insert slides animation id: %s to database', 'victheme_demo'), $id), 'text-danger');
      }
      else {
        $this->setMap('animation', $id, $wpdb->insert_id);
        $this->addLog(sprintf(__('Revolution slides animation id: %s imported to database', 'victheme_demo'), $id), 'text-success');
      }

    }
  }


  /**
   * Importing Revolution static slides
   *
   * @todo need to handle the params settings better
   *       Suspecting that need to change the image
   *       path as well inside the params settings.
   */
  private function importStatic() {

    global $wpdb;

    foreach ($this->staticSlider->getData() as $id => $data) {

      if ($this->checkMap('revslider', 'static', $id)) {
        continue;
      }

      unset($data['id']);
      $data['slider_id'] = $this->getMappedValue('revslider', 'sliders', $data['slider_id']);

      $result = $wpdb->insert($wpdb->prefix .'revslider_static_slides', $data);

      if (!$result) {
        $this->addLog(sprintf(__('Failed to insert static slides id: %s to database', 'victheme_demo'), $id), 'text-danger');
        $this->reportFailed();
      }
      else {
        $this->setMap('static', $id, $wpdb->insert_id);
        $this->addLog(sprintf(__('Revolution static slides id: %s imported to database', 'victheme_demo'), $id), 'text-success');
      }
    }
  }



  /**
   * Importing Revolution slider settings.
   */
  private function updateSettings() {

    global $wpdb;

    foreach ($this->settings->getData() as $id => $data) {

      if ($this->checkMap('revslider', 'settings', $id)) {
        continue;
      }

      $olddata = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "revslider_settings WHERE ID=$id", ARRAY_A);
      if (!is_wp_error($olddata)) {
        $this->setMap('revslider', 'oldsettings', $id, $olddata);
      }

      if (is_array($data['general'])) {
        $data['general'] = serialize($data['general']);
      }

      $result = $wpdb->replace($wpdb->prefix .'revslider_settings', $data);

      if (!$result) {
        $this->addLog(sprintf(__('Failed to insert slider settings id: %s to database', 'victheme_demo'), $id), 'text-danger');
        $this->reportFailed();
      }
      else {
        $this->setMap('settings', $id, $wpdb->insert_id);
        $this->addLog(sprintf(__('Revolution slider settings id: %s imported to database', 'victheme_demo'), $id), 'text-success');
      }
    }
  }





  /**
   * Method for importing slides with its layers.
   * This will also attempt to import the images found
   * in the layers and the slide as its background.
   */
  private function importSlides() {

    global $wpdb;

    // Build the slides
    foreach ($this->slides->getData() as $id => $data) {

      if ($this->checkMap('revslider', 'slides', $id)) {
        continue;
      }

      unset($data['id']);

      $alias = $this->getMappedValue('revslider', 'alias', $data['slider_id']);
      $data['slider_id'] = $this->getMappedValue('revslider', 'sliders', $data['slider_id']);

      $param = json_decode($data['params'], true);

      if (isset($param['image'])) {
        $this->fetchFile($param['image'], array('revslider', $alias));
        $param['image'] = str_replace($this->css->base, $this->base, $param['image']);
      }

      if (isset($param['image_id']) && is_numeric($param['image_id'])) {
        $param['image_id'] = $this->getMappedValue('post', 'id', $param['image_id']);
      }

      $data['params'] = json_encode($param);

      $layers = json_decode($data['layers'], true);
      foreach ($layers as $layerKey => $layer) {

        // Update custom animation
        if (strpos('customin-', $layer['animation'] !== false)) {
          list($name, $animationid) = explode('-', $layer['animation']);

          $layer['animation'] = 'customin-' . $this->getMappedValue('revslider', 'animation', $animationid);
        }

        // Update custom end animation
        if (strpos('customout-', $layer['endanimation'] !== false)) {
          list($name, $animationid) = explode('-', $layer['endanimation']);

          $layer['animation'] = 'customout-' . $this->getMappedValue('revslider', 'animation', $animationid);
        }

        // Update layer images url.
        if (isset($layer['image_url']) && !empty($layer['image_url'])) {
          $this->fetchFile($layer['image_url'], array('revslider', $alias));
          $layers[$layerKey]['image_url'] = str_replace($this->css->base, $this->base, $layer['image_url']);
        }

        // Update image id
        if (isset($layer['image_id']) && !empty($layer['image_id'])) {
          $layers[$layerKey]['image_id'] = $this->getMappedValue('post', 'id', $layer['image_id']);
        }

      }

      $data['layers'] = json_encode($layers);

      $result = $wpdb->insert($wpdb->prefix .'revslider_slides', $data);

      if (!$result) {
        $this->addLog(sprintf(__('Failed to insert slides instance id: %s for slider id: %s to database', 'victheme_demo'), $id, $data['slider_id']), 'text-danger');
        $this->reportFailed();
      }
      else {
        $this->setMap('slides', $id, $wpdb->insert_id);
        $this->addLog(sprintf(__('Revolution slides instance id: %s for slider id: %s imported to database', 'victheme_demo'), $id, $data['slider_id']), 'text-success');
      }
    }
  }





  /**
   * This method will attempt to inject the sliders into
   * the current database and try to import any background
   * image defined in the slider data.
   */
  private function importSliders() {

    global $wpdb;

    // Build the sliders
    foreach ($this->config->getData() as $id => $data) {

      if ($this->checkMap('revslider', 'sliders', $id)) {
        continue;
      }

      unset($data['id']);

      $param = json_decode($data['params'], true);

      if (isset($param['background-image'])) {
        $this->fetchFile($param['background-image'], array('revslider', $data['alias']));

        $param['background-image'] = str_replace($this->css->base, $this->base, $param['background-image']);
      }

      $data['params'] = json_encode($param);

      $result = $wpdb->insert($wpdb->prefix . 'revslider_sliders', $data);

      if (!$result) {
        $this->addLog(sprintf(__('Failed to insert slider instance id: %s to database', 'victheme_demo'), $id), 'text-danger');
        $this->reportFailed();
      }
      else {
        $this->setMap('sliders', $id, $wpdb->insert_id);
        $this->setMap('alias', $id, $data['alias']);
        $this->addLog(sprintf(__('Revolution slider instance id: %s imported to database', 'victheme_demo'), $id), 'text-success');
      }
    }


    // Fix the slider template id param
    foreach ($this->config->getData() as $id => $data) {

      if ($this->checkMap('revslider', 'templateid', $id)) {
        continue;
      }

      $param = json_decode($data['params'], true);

      if (isset($param['slider_template_id'])) {
        $param['slider_template_id'] = $this->getMappedValue('revslider', 'sliders', $param['slider_template_id']);
        $data['params'] = json_encode($param);
        unset($data['id']);

        $result = $wpdb->update($wpdb->prefix . 'revslider_sliders', $data, array('id' => $this->getMappedValue('revslider', 'sliders', $id)));

        if (!$result) {
          $this->addLog(sprintf(__('Failed to update template id for slider instance id: %s', 'victheme_demo'), $id), 'text-danger');
          $this->reportFailed();
        }
        else {
          $this->setMap('templateid', $id, $param['slider_template_id']);
          $this->addLog(sprintf(__('Revolution slider template id updated for slide: %s', 'victheme_demo'), $id), 'text-success');
        }
      }
    }
  }





  /**
   * This method will try to import the CSS definition
   * made by revolution slider css table.
   *
   * This will try to fetch the files specified
   * in the css file and move it to revslider/css folder
   * to avoid invalid files defined by css rules.
   */
  private function importCSS() {

    global $wpdb;

    // Build the CSS data first
    foreach ($this->css->getData() as $id => $data) {

      if ($this->checkMap('revslider', 'css', $id)) {
        continue;
      }

      unset($data['id']);

      $param = json_decode($data['params'], true);
      $files = array();

      if (isset($param['background-image'])) {
        $files[] = $param['background-image'];
        $param['background-image'] = str_replace($this->css->base, $this->base, $param['background-image']);
      }

      if (isset($param['background'])) {
        $matches = preg_match('/\((.*?)\)/', $param['backgrund']);
        $files[] = $matches[1];
        $param['background'] = str_replace($this->css->base, $this->base, $param['background']);
      }

      // Retrieving files
      foreach ($files as $remote_url) {
        $this->fetchFile($remote_url, array('revslider', 'css'));
      }

      $data['params'] = json_encode($param);

      // Double check for existing entry to prevent double entry
      $oldEntry = $wpdb->get_results(
        $wpdb->prepare("SELECT * FROM " . $wpdb->prefix . "revslider_css WHERE handle = %s", $data['handle'])
        , OBJECT);

      if (!empty($oldEntry)) {
        foreach ($oldEntry as $entry) {
          $entryid = $entry->id;
          unset($entry->id);
          $this->setMap('oldcss', $id, $entry);
          $wpdb->delete($wpdb->prefix . 'revslider_css', array('id' => $entryid));
        }
      }

      $result = $wpdb->insert($wpdb->prefix . 'revslider_css', $data);

      if (!$result) {
        $message = sprintf(__('Failed to insert slider css id: %s to database', 'victheme_demo'), $id);
        $this->addLog($message, 'text-danger');
        $this->reportFailed();
      }
      else {
        $message = sprintf(__('Revolution slider CSS id: %s imported to database', 'victheme_demo'), $id);
        $this->addLog($message, 'text-success');

        $this->setMap('css', $id, $wpdb->insert_id);
      }
    }

  }





  /**
   * Method for fetching remote file and save it to wordpress
   * upload directory.
   */
  private function fetchFile($remote_url, $subdir = array()) {

    $upload = VTCore_Wordpress_Utility::uploadBits(basename($remote_url), '', $subdir);
    $headers = wp_get_http($remote_url, $upload['file']);
    $info = wp_check_filetype($upload['file']);

    if (!$headers
        || $headers['response'] != '200'
        || filesize($upload['file']) == 0
        || $info == false) {

      @unlink($upload['file']);
      $message = sprintf(__('Failed to download %s', 'victheme_demo'), $remote_url);
      $this->addLog($message, 'text-danger');
      $this->reportFailed();
    }

    else {
      $this->setMap('files', basename($remote_url), $upload['file']);
      $message = sprintf(__('File %s downloaded and saved to system', 'victheme_demo'), basename($remote_url));
      $this->addLog($message, 'text-success');
    }
  }






  /**
   * The actual metadata removal will be invoked when
   * the post is deleted. Thus this function will only
   * remove the import marking and mapping.
   *
   * @see VTCore_Demo_Models_Processor::removeData()
   */
  public function removeData() {

    global $wpdb;

    foreach ($this->getMap('revslider', 'css', array()) as $oldID => $newID) {
      $wpdb->delete($wpdb->prefix.'revslider_css', array('id' => $newID), array('%d'));
      $this->removeMapByValue('revslider', 'css', $oldID);
      $message = sprintf(__('Revolution slider CSS id: %s removed from database', 'victheme_demo'), $newID);
      $this->addLog($message, 'text-success');
    }

    foreach ($this->getMap('revslider', 'oldcss', array()) as $oldID => $data) {
      $wpdb->insert($wpdb->prefix.'revslider_css', (array) $data);
      $this->removeMapByValue('revslider', 'oldcss', $oldID);
      $message = sprintf(__('Revolution slider Old CSS id: %s restored to database', 'victheme_demo'), $oldID);
      $this->addLog($message, 'text-success');
    }

    foreach ($this->getMap('revslider', 'sliders', array()) as $oldID => $newID) {
      $result = $wpdb->delete($wpdb->prefix.'revslider_sliders', array('id' => $newID), array('%d'));

      $this->removeMapByValue('revslider', 'sliders', $oldID);
      $this->removeMapByValue('revslider', 'templateid', $oldID);
      $this->removeMapByValue('revslider', 'alias', $oldID);

      $message = sprintf(__('Revolution slider instance id: %s removed from database', 'victheme_demo'), $newID);
      $this->addLog($message, 'text-success');
    }

    foreach ($this->getMap('revslider', 'slides', array()) as $oldID => $newID) {
      $this->removeMapByValue('revslider', 'slides', $oldID);
      $wpdb->delete($wpdb->prefix.'revslider_slides', array('id' => $newID), array('%d'));

      $message = sprintf(__('Revolution slides instance id: %s removed from database', 'victheme_demo'), $newID);
      $this->addLog($message, 'text-success');
    }

    foreach ($this->getMap('revslider', 'animation', array()) as $oldID => $newID) {
      $wpdb->delete($wpdb->prefix.'revslider_layer_animations', array('id' => $newID), array('%d'));
      $this->removeMapByValue('revslider', 'animation', $oldID);
      $message = sprintf(__('Revolution custom animation id: %s removed from database', 'victheme_demo'), $newID);
      $this->addLog($message, 'text-success');
    }

    foreach ($this->getMap('revslider', 'static', array()) as $oldID => $newID) {
      $wpdb->delete($wpdb->prefix.'revslider_static_slides', array('id' => $newID), array('%d'));
      $this->removeMapByValue('revslider', 'static', $oldID);
      $message = sprintf(__('Revolution static slides id: %s removed from database', 'victheme_demo'), $newID);
      $this->addLog($message, 'text-success');
    }

    foreach ($this->getMap('revslider', 'settings', array()) as $oldID => $newID) {

      if ($this->checkMap('revslider', 'oldsettings', $oldID)) {

        $wpdb->update($wpdb->prefix . 'revslider_settings', $this->getMappedValue('revslider', 'oldsettings', $oldID), array('id' => $newID));
        $this->removeMapByValue('revslider', 'oldsettings', $oldID);
        $message = sprintf(__('Revolution settings id: %s reverted to original value', 'victheme_demo'), $newID);
        $this->addLog($message, 'text-success');

      }

      else {

        $wpdb->delete($wpdb->prefix.'revslider_settings', array('id' => $newID), array('%d'));
        $message = sprintf(__('Revolution settings id: %s removed from database', 'victheme_demo'), $newID);
        $this->addLog($message, 'text-success');
      }

      $this->removeMapByValue('revslider', 'settings', $oldID);
    }

    foreach ($this->getMap('revslider', 'files', array()) as $file => $path) {
      @unlink($path);
      $this->removeMapByValue('revslider', 'files', $file);
      $message = sprintf(__('File: %s deleted', 'victheme_demo'), $file);
      $this->addLog($message, 'text-success');
    }


    $remove = true;
    $check = array(
      'css',
      'sliders',
      'alias',
      'templateid',
      'files',
      'slides',
      'settings',
    );

    foreach ($check as $name) {
      if (isset($this->map['revslider']['css']) && !empty($this->map['revslider']['css'])) {
        $remove = false;
      }
    }

    if ($remove) {
      $this->removeMap('revslider');
      $this->removeImportedStatus();
    }

  }

}