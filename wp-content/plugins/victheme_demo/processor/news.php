<?php

/**
 * Processor for processing news post type
 * data, especially fixing the media image
 * attachment post id since it will change
 * from the original post id into the mapped
 * new post id.
 *
 * @author jason@victheme.com
 *
 */
class VTCore_Demo_Processor_News
extends VTCore_Demo_Models_Processor {

  protected $dependencies = 'victheme_news';
  protected $processorsDependencies = array(
    'posts',
    'postmeta',
    'attachment',
  );

  /**
   * Overriding parent method to use Post config
   * @see VTCore_Demo_Models_Processor::loadData()
   */
  public function loadData() {

    $metaName = $this->prefix . 'Postmeta';
    $postName = $this->prefix . 'Posts';

    if (class_exists($metaName, TRUE)
        && class_exists($postName, TRUE)) {

      $this->config = new $metaName();
      $this->posts = new $postName();

      return TRUE;
    }

    return FALSE;
  }


  /**
   * The main controller method for performing
   * the data remapping.
   *
   * This has to run after the content processor.
   *
   * @see VTCore_Demo_Models_Processor::importData()
   */
  public function importData() {

    // Stop processing if this database is marked
    // as imported previously.
    if ($this->checkImportStatus()) {
      return;
    }

    // Process post meta
    foreach ($this->config->getData() as $meta) {

      if ($meta['meta_key'] != '_news_data'
          || $this->checkMap('news', 'ID', $meta['post_id'])) {
        continue;
      }

      // Remap the media image
      $meta['meta_value']['media']['image'] = $this->getMappedValue('posts', 'ID', $meta['meta_value']['media']['image']);

      // Remap the highlight image
      $meta['meta_value']['highlight']['image'] = $this->getMappedValue('posts', 'ID', $meta['meta_value']['highlight']['image']);

      // Remap the highlight image
      $meta['meta_value']['author']['photo'] = $this->getMappedValue('posts', 'ID', $meta['meta_value']['author']['photo']);

      $newPostId = $this->getMappedValue('posts', 'ID', $meta['post_id']);
      $result = update_post_meta($newPostId, '_news_data', $meta['meta_value']);

      if ($result) {
        $this->addLog(sprintf(__('Post - %s custom news data media remapped', 'victheme_demo'), $newPostId), 'text-success');
        $this->setMap('ID', $meta['post_id'], $newPostId);
      }
      else {
        $this->addLog(sprintf(__('Failed to remap Post - %s custom news data media', 'victheme_demo'), $newPostId), 'error');
        $this->reportFailed();
      }
    }

    if ($this->maybeClose()) {
      $this->markImportedStatus();
    }
  }


  /**
   * Remove the marking in database
   *
   * post and metadata related to news will be removed
   * by the posts processor
   *
   * @see VTCore_Demo_Models_Processor::removeData()
   */
  public function removeData() {
    $this->removeMap('news');
    $this->removeImportedStatus();
  }

}