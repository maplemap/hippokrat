<?php
/**
 * Processor class for handling the wp users table
 * content exported by victheme_exporter module.
 *
 * @author jason@victheme.com
 *
 */
class VTCore_Demo_Processor_Users
extends VTCore_Demo_Models_Processor {

  /**
   * Importing user by injecting the stored userdata back into
   * current database via WordPress native api function.
   *
   * This function will never update or override user, if the database
   * already have user with the same UID then a new user will be
   * created and we will map the old UID against the new UID for later
   * use.
   *
   * @see VTCore_Demo_Models_Processor::importData()
   */
  public function importData() {

    // Stop processing if this database is marked
    // as imported previously.
    if ($this->checkImportStatus()) {
      return;
    }

    foreach ($this->config->getData() as $uid => $user) {

      // Never attempt to process root admin for
      // security reasons.
      if ($uid == 1 || $this->checkMap('users', 'ID', $uid)) {
        continue;
      }

      unset($user['ID']);
      $newid = wp_insert_user($user);

      if (is_wp_error($newid)) {
        $this->addLog(sprintf(__('Failed to import user: %s to database', 'victheme_demo'), $user['user_login']), 'text-danger');
        $this->reportFailed();
      }
      else {
        $this->setMap('ID', $uid, $newid);
        $this->addLog(sprintf(__('User: %s imported to database', 'victheme_demo'), $user['user_login']), 'text-success');
      }
    }

    if ($this->maybeClose()) {
      $this->markImportedStatus();
    }
  }





  /**
   * Removing previously imported user from database
   * This method will not process Non mapped user or
   * root (user 1) for security reason. Any posts
   * that is related to the user will be reassigned
   * to user 1.
   *
   * @see VTCore_Demo_Models_Processor::removeData()
   */
  public function removeData() {

    foreach ($this->getMap('users', 'ID', array()) as $oldID => $newID) {

      // Never attempt to process root admin for
      // security reasons.
      if ($oldID == 1 || $newID == 1) {
        continue;
      }

      $result = wp_delete_user($newID, 1);

      if ($result) {
        $this->removeMapByValue('users', 'ID', $oldID);
        $this->addLog(sprintf(__('User: %s deleted from database', 'victheme_demo'), $newID), 'text-success');
      }
    }


    if ($this->removeMapWhenEmpty('users', 'ID')) {
      $this->removeImportedStatus();
    }
  }

}