<?php

/**
 * Processor for processing headline
 * data, especially fixing the media (image and video)
 * attachment post id since it will change
 * from the original post id into the mapped
 * new post id.
 *
 * @author jason@victheme.com
 *
 */
class VTCore_Demo_Processor_Headline
extends VTCore_Demo_Models_Processor {

  protected $dependencies = 'victheme_headline';
  protected $processorsDependencies = array(
    'options',
    'posts',
    'postmeta',
    'attachment',
  );

  /**
   * Overriding parent method to use Post config
   * @see VTCore_Demo_Models_Processor::loadData()
   */
  public function loadData() {

    $className = $this->prefix . 'Postmeta';

    if (class_exists($className, TRUE)) {
      $this->config = new $className();
      return TRUE;
    }

    return FALSE;
  }


  /**
   * The main controller method for performing
   * the data remapping.
   *
   * This has to run after the content processor.
   *
   * @see VTCore_Demo_Models_Processor::importData()
   */
  public function importData() {

    // Stop processing if this database is marked
    // as imported previously.
    if ($this->checkImportStatus()) {
      return;
    }

    $baseUrl = get_option('siteurl');

    // Process post meta
    foreach ($this->config->getData() as $meta) {

      if ($meta['meta_key'] != '_headline_data'
          || $this->checkMap('headline', 'ID', $meta['post_id'])) {
        continue;
      }


      $keys = array(
        'image',
        'webm',
        'mp4',
        'ogv',
      );

      // Remap all media post id.
      foreach ($keys as $key) {

        $oldID = '';
        if ($key == 'image' && isset($meta['meta_value']['background'][$key])) {
          $oldID = $meta['meta_value']['background'][$key];

        }
        elseif (isset($meta['meta_value']['background']['video'][$key])) {
          $oldID = $meta['meta_value']['background']['video'][$key];
        }

        // Grab new mapped post id
        if (is_numeric($oldID)) {
          $newID = $this->getMappedValue('posts', 'ID', $oldID);
        }
        elseif (strpos($oldID, $this->config->getBase()) !== false) {
          $newID = str_replace($this->config->getBase(), $baseUrl, $oldID);
        }

        if ($key == 'image') {
          $meta['meta_value']['background'][$key] = $newID;
        }
        else {
          $meta['meta_value']['background']['video'][$key] = $newID;
        }
      }

      $newPostId = $this->getMappedValue('posts', 'ID', $meta['post_id']);

      $status = update_post_meta($newPostId, '_headline_data', $meta['meta_value']);
      if ($status === false) {
        $this->reportFailed();
        $message = sprintf(__('Failed to remap post - %s headline media data', 'victheme_demo'), $meta['post_id']);
        $this->addLog($message, 'text-danger');
      }

      else {
        $message = sprintf(__('Post - %s headline data media remapped', 'victheme_demo'), $newPostId);
        $this->addLog($message, 'text-success');
        $this->setMap('ID', $meta['post_id'], $newPostId);
      }
    }


    // Process options
    // The backup is already performed by Options Processor
    // No need to store another backup.

    if (!$this->checkMap('headline', 'options', 'status')) {

      $options = get_option('vtcore_headline_config', array());

      if (!empty($options)) {

        $update = FALSE;

        foreach ($options as $panel => $option) {
          $keys = array(
            'image',
            'webm',
            'mp4',
            'ogv',
          );

          foreach ($keys as $key) {
            if ($key == 'image') {
              $oldID = $option['background'][$key];
            }
            else {
              $oldID = $option['background']['video'][$key];
            }

            if (empty($mediaID)) {
              continue;
            }

            // Grab new mapped post id
            if (is_numeric($oldID)) {
              $newID = $this->getMappedValue('posts', 'ID', $oldID);
            }
            elseif (strpos($oldID, $this->config->getBase()) !== FALSE) {
              $newID = str_replace($this->config->getBase(), $baseUrl, $oldID);
            }

            if ($key == 'image') {
              $options[$panel]['background'][$key] = $newID;
            }
            else {
              $options[$panel]['background']['video'][$key] = $newID;
            }
          }
        }

        $status = update_option('vtcore_headline_config', $options);

        if ($status) {
          $message = __('Headline configuration for special page updated', 'victheme_demo');
          $this->addLog($message, 'text-success');
          $this->setMap('options', 'status', true);
        }
      }
    }

    if ($this->maybeClose()) {
      $this->markImportedStatus();
    }
  }


  /**
   * Remove the marking in database
   *
   * Options preprocessor will revert back the old vtcore_headline_config
   *
   * @see VTCore_Demo_Models_Processor::removeData()
   */
  public function removeData() {
    $this->removeMap('headline');
    $this->removeImportedStatus();
  }

}