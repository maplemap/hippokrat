<?php
/**
 * Processor class for handling the wp postmeta table
 * content exported by victheme_exporter module.
 *
 * @author jason@victheme.com
 *
 */
class VTCore_Demo_Processor_Postmeta
extends VTCore_Demo_Models_Processor {

  protected $processorsDependencies = array(
    'posts',
  );

  /**
   * Importing the post meta data to the database.
   *
   * @see VTCore_Demo_Models_Processor::importData()
   */
  public function importData() {

    if ($this->checkImportStatus()) {
      return;
    }

    $this->oldBaseUrl = $this->config->getBase();
    $this->baseUrl = get_option('siteurl');

    foreach ($this->config->getData() as $mid => $meta) {

      if ($this->checkMap('postmeta', 'ID', $mid)) {
        continue;
      }

      if ($meta['meta_key'] == '_edit_last') {

        // Try to remap the author if failed use root instead
        $meta['meta_value'] = $this->getMappedValue('users', 'ID', $meta['meta_value']);
        if (!$meta['meta_value']) {
          $meta['meta_value'] = 1;
        }

      }
      elseif ($meta['meta_key'] == '_thumbnail_id'
              || $meta['meta_key'] == '_menu_item_menu_item_parent') {

        $meta['meta_value'] = $this->getMappedValue('posts', 'ID', $meta['meta_value']);
      }

      $meta['meta_value'] = str_replace($this->oldBaseUrl, $this->baseUrl, $meta['meta_value']);

      if (is_serialized_string($meta['meta_value'])) {
        $meta['meta_value'] = VTCore_Utility::fixSerialString($meta['meta_value']);
      }

      // Save Post
      $result = add_post_meta($this->getMappedValue('posts', 'ID', $meta['post_id']) , $meta['meta_key'], $meta['meta_value']);

      if (!$result) {
        $this->reportFailed();
        $this->addLog(sprintf(__( 'Failed to import post : %s - meta : %s', 'victheme_demo'), $meta['post_id'], $meta['meta_key']), 'text-danger');
      }
      else {
        $this->addLog(sprintf(__('Post %s - meta : %s Imported', 'victheme_demo'), $meta['post_id'], $meta['meta_key']), 'text-success');
        $this->setMap('ID', $mid, $result);
      }

    }

    if ($this->maybeClose()) {
      $this->markImportedStatus();
    }

  }



  /**
   * The actual metadata removal will be invoked when
   * the post is deleted. Thus this function will only
   * remove the import marking and mapping.
   *
   * @see VTCore_Demo_Models_Processor::removeData()
   */
  public function removeData() {

    global $wpdb;
    foreach ($this->getMap('postmeta', 'ID', array()) as $oldID => $newID) {

      $meta_exists = $wpdb->get_row("SELECT * FROM $wpdb->postmeta WHERE meta_id = '" . $newID . "'", 'ARRAY_A');
      if (!$meta_exists) {
        $this->removeMapByValue('postmeta', 'ID', $oldID);
      }
      else {
        $result = $wpdb->delete($wpdb->postmeta, array('meta_id' => $newID));
        if ($result) {
          $this->addLog(sprintf(__('Post meta id: %d deleted', 'victheme_demo'), $newID), 'text-success');
          $this->removeMapByValue('postmeta', 'ID', $oldID);
        }
        else {
          $this->addLog(sprintf(__('Failed to delete Post meta id: %d', 'victheme_demo'), $newID), 'error');
        }
      }
    }

    if ($this->removeMapWhenEmpty('postmeta', 'ID')) {
      $this->removeImportedStatus();
    }

  }

}