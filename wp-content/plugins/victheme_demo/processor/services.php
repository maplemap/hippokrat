<?php
/**
 * Processor for processing services
 * entry from victheme services plugin
 *
 * @author jason@victheme.com
 *
 */
class VTCore_Demo_Processor_Services
extends VTCore_Demo_Models_Processor {

  protected $dependencies = 'victheme_services';
  protected $processorsDependencies = array(
    'posts',
    'postmeta',
    'users',
    'terms',
    'attachment',
  );

  /**
   * Overriding parent method to use Post config
   * @see VTCore_Demo_Models_Processor::loadData()
   */
  public function loadData() {
    $postName = $this->prefix . 'Posts';

    if (class_exists($postName, TRUE)) {
      $this->posts = new $postName();
      return TRUE;
    }

    return FALSE;
  }


  /**
   * The main controller method for performing
   * the data remapping.
   *
   * This has to run after the content processor.
   *
   * @see VTCore_Demo_Models_Processor::importData()
   */
  public function importData() {

    // Stop processing if this database is marked
    // as imported previously.
    if ($this->checkImportStatus()) {
      return;
    }

    // Process post meta
    foreach ($this->posts->getData() as $id =>$post) {

      if ($post['post_type'] != 'services' || $this->checkMap('services', 'ID', $id)) {
        continue;
      }

      $newId = $this->getMappedValue('posts', 'ID', $post['ID']);
      $meta = get_post_meta((int) $newId, '_services_data', true);

      $object = new VTCore_Wordpress_Objects_Array($meta);

      if ($object->get('department')) {
        $object->add('department', $this->getMappedValue('posts', 'ID', $object->get('department')));
      }

      if ($object->get('display.image')) {
        $object->add('display.image', $this->getMappedValue('posts', 'ID', $object->get('display.image')));
      }

      if ($object->get('schedule')) {
        foreach ((array) $object->get('schedule') as $sid => $schedule) {
          if (isset($schedule['users'])) {
            foreach ($schedule['users'] as $delta => $user) {
              $object->add('schedule.' . $sid . '.users.' . $delta, $this->convertDepartmentID($user));
            }
          }
        }
      }

      update_post_meta((int) $newId, '_services_data', $object->extract());


      $this->setMap('services', 'ID', $id);
    }


    if ($this->maybeClose()) {
      $this->markImportedStatus();
    }

  }


  /**
   * Some of the department stored id can be either
   * user id, post id or term id based on if any
   * plugin modifies it, by default it will use
   * user.
   *
   * @param $value
   * @return bool
   */
  protected function convertDepartmentID($value) {
    switch (VTCORE_SERVICES_DATA_MODE) {
      case 'post' :
        $value = $this->getMappedValue('posts', 'ID', $value);
        break;

      case 'user' :
        $value = $this->getMappedValue('users', 'ID', $value);
        break;

      case 'term' :
        $value = $this->getMappedValue('terms', 'ID', $value);
        break;
    }

    return $value;
  }


  /**
   * Remove the marking in database
   * @see VTCore_Demo_Models_Processor::removeData()
   */
  public function removeData() {
    $this->removeMap('services');
    $this->removeImportedStatus();
  }

}