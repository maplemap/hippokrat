<?php
/**
 * Processor class for handling the wp comments table
 * content exported by victheme_exporter module.
 *
 * @author jason@victheme.com
 *
 */
class VTCore_Demo_Processor_Comments
extends VTCore_Demo_Models_Processor {

  protected $processorsDependencies = array(
    'posts',
    'users',
  );

  /**
   * Importing Comments into the database
   *
   * @see VTCore_Demo_Models_Processor::importData()
   */
  public function importData() {

    // Stop processing if this database is marked
    // as imported previously.
    if ($this->checkImportStatus()) {
      return;
    }

    foreach ($this->config->getData() as $cid => $comment) {

      if ($this->checkMap('comments', 'ID', $cid)) {
        continue;
      }

      // Try to remap the author if failed fall to anomymous instead
      $comment['user_id'] = $this->getMappedValue('users', 'ID', $comment['user_id']);

      // Remap the comment post
      $comment['comment_post_ID'] = $this->getMappedValue('posts', 'ID', $comment['comment_post_ID']);

      // Remap the parent comment
      $comment['comment_parent'] = $this->getMappedValue('comments', 'ID', $comment['comment_parent']);

      $comment = wp_filter_comment($comment);

      // Save comment
      $result = wp_insert_comment($comment, true);

      if (is_wp_error($result)) {
        $message = sprintf(__( 'Failed to import comment %s - %s', 'victheme_demo'), $comment['comment_post_ID'], substr(esc_html($comment['comment_content']), 0, 10));
        $this->addLog($message, 'text-danger');
        $this->reportFailed();
      }
      else {
        $message = sprintf(__('Comment %s - %s Imported', 'victheme_demo'), $comment['comment_post_ID'], substr(esc_html($comment['comment_content']), 0, 10));
        $this->addLog($message, 'text-success');

        $this->setMap('ID', $cid, $result);
      }

    }

    if ($this->maybeClose()) {
      $this->markImportedStatus();
    }

  }



  /**
   * The actual metadata removal will be invoked when
   * the post is deleted. Thus this function will only
   * remove the import marking and mapping.
   *
   * @see VTCore_Demo_Models_Processor::removeData()
   */
  public function removeData() {

    global $wpdb;
    foreach ($this->getMap('comments', 'ID', array()) as $oldID => $newID) {

      $comment_exists = $wpdb->get_row("SELECT * FROM $wpdb->comments WHERE comment_ID = '" . $newID . "'", 'ARRAY_A');
      if ($comment_exists) {
        if (wp_delete_comment($newID, TRUE) === TRUE) {
          $this->addLog(sprintf(__('Comment id: %d deleted', 'victheme_demo'), $newID), 'text-success');
        }
      }

      $this->removeMapByValue('comments', 'ID', $oldID);
    }

    if ($this->removeMapWhenEmpty('comments', 'ID')) {
      $this->removeImportedStatus();
    }

  }

}