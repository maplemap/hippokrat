<?php

/**
 * Processor for processing agents
 * for fixing the agents profile related
 * taxonomy and images
 *
 * @author jason@victheme.com
 *
 */
class VTCore_Demo_Processor_Agents
extends VTCore_Demo_Models_Processor {

  protected $dependencies = 'victheme_agents';
  protected $processorsDependencies = array(
    'users',
    'usermeta',
    'terms',
    'posts',
    'attachment',
    'widgets',
  );


  /**
   * Overriding parent method to use Post config
   * @see VTCore_Demo_Models_Processor::loadData()
   */
  public function loadData() {

    $configName = $this->prefix . 'Users';

    if (class_exists($configName, TRUE)) {
      $this->config = new $configName();
      return TRUE;
    }

    return FALSE;
  }


  /**
   * The main controller method for performing
   * the data remapping.
   *
   * This has to run after the content processor.
   *
   * @see VTCore_Demo_Models_Processor::importData()
   */
  public function importData() {

    // Stop processing if this database is marked
    // as imported previously.
    if ($this->checkImportStatus()) {
      return;
    }

    $this->fields = new VTCore_Agents_Fields();
    $this->oldBaseUrl = $this->config->getBase();
    $this->baseUrl = get_option('siteurl');

    // Process post meta
    foreach ($this->config->getData() as $user) {

      // Use mapped value to reflect the new user ID as in database
      $userObject = new WP_User((int) $this->getMappedValue('users', 'ID', $user['ID']));

      if (!$userObject->has_cap('property_agents')
          || $this->checkMap('agents', 'meta', $userObject->get('ID'))) {
        continue;
      }

      $agentObject = new VTCore_Agents_Entity_Agents($userObject->get('ID'), $userObject);
      $agentObject->loadMetaData();


      if ($agentObject->getMeta('fields')) {
        foreach ($agentObject->getMeta('fields') as $fid => $storedDatas) {
          foreach ($storedDatas as $sid => $data) {

            $settings = $this->fields->getFieldValue($fid, 'storage', $sid);

            // Convert taxonomy id
            if (isset($settings['mode']) && $settings['mode'] == 'taxonomy_id') {
              foreach ($data as $delta => $term_id) {
                $data[$delta] = $this->getMappedValue('terms', 'ID', $term_id);
              }

              $agentObject->addMeta('fields.' . $fid . '.' . $sid, $data);
            }

            // Convert images
            if (isset($settings['mode']) && $settings['mode'] == 'image') {
              $is_array = is_array($data);

              $data = (array) $data;
              foreach ($data as $delta => $post_id) {
                if (is_numeric($post_id)) {
                  $data[$delta] = $this->getMappedValue('posts', 'ID', $post_id);
                }
                else {
                  $data[$delta] = str_replace($this->oldBaseUrl, $this->baseUrl, $post_id);
                }
              }

              if (!$is_array) {
                $data = array_shift($data);
              }

              $agentObject->addMeta('fields.' . $fid . '.' . $sid, $data);
            }

            // Convert headlines
            if ($fid == 'agents_headline' && $sid == 'agents_profile') {

              // Convert headline background image
              if (isset($data['background']['image'])) {
                if (is_numeric($data['background']['image'])) {
                  $data['background']['image'] = $this->getMappedValue('posts', 'ID', $data['background']['image']);
                }
                else {
                  $data['background']['image'] = str_replace($this->oldBaseUrl, $this->baseUrl, $data['background']['image']);
                }
              }

              // Convert headline background video
              if (isset($data['background']['video'])) {

                foreach ($data['background']['video'] as $delta => $video) {
                  if (is_numeric($video)) {
                    $data['background']['video'][$delta] = $this->getMappedValue('posts', 'ID', $video);
                  }
                  else {
                    $data['background']['video'][$delta] = str_replace($this->oldBaseUrl, $this->baseUrl, $video);
                  }
                }
              }

              // Convert headline masking image
              if (isset($data['masking']['image'])) {
                if (is_numeric($data['masking']['image'])) {
                  $data['masking']['image'] = $this->getMappedValue('posts', 'ID', $data['masking']['image']);
                }
                else {
                  $data['masking']['image'] = str_replace($this->oldBaseUrl, $this->baseUrl, $data['masking']['image']);
                }
              }

              $agentObject->addMeta('fields.' . $fid . '.' . $sid, $data);
            }
          }
        }
      }


      // Converting attributes
      if ($agentObject->getMeta('attributes')) {
        foreach ($agentObject->getMeta('attributes') as $aid => $terms) {
          foreach ($terms as $delta => $term_id) {
            if (is_numeric($term_id)) {
              $terms[$delta] = $this->getMappedValue('terms', 'ID', $term_id);
            }
          }

          $agentObject->addMeta('attributes.' . $aid, $terms);
        }
      }

      // Force to save and rebuild all fields and attributes
      // as some importdata only include _agent_data meta entry
      $agentObject
        ->initializeFields()
        ->initializeAttributes()
        ->saveMetaFields()
        ->saveMetaAttributes()
        ->saveMetaData();

      $message = sprintf(__('Agents - %s meta data updated', 'victheme_demo'), $userObject->get('ID'));
      $this->addLog($message, 'text-success');

      $this->setMap('meta', $userObject->get('ID'), TRUE);
    }


    // Convert configuration
    if (!$this->checkMap('agents', 'config', 'updated')) {
      $configObject = new VTCore_Agents_Config();

      // Update contact form 7
      $newId = $this->getMappedValue('posts', 'ID', $configObject->get('agents_contactform7.form_id'));
      $configObject->add('agents_contactform7.form_id', $newId);
      $configObject->save();

      $message = __('Agents - plugin configuration updated', 'victheme_demo');
      $this->addLog($message, 'text-success');

      $this->setMap('config', 'updated', TRUE);
    }


    // Widget agents list
    if (!$this->checkMap('agents', 'widget_listing', 'updated')) {

      $update = FALSE;
      $widgets = get_option('vtcore_agents_widgets_list', array());
      foreach ($widgets as $delta => $widget) {
        if (!is_numeric($delta) || empty($widget)) {
          continue;
        }

        foreach ($widgets[$delta]['agents'] as $delta => $uid) {
          if (!is_numeric($uid)) {
            continue;
          }

          $widgets[$delta]['agents'] = $this->getMappedValue('users', 'ID', $uid);

          $update = TRUE;
        }
      }

      if ($update) {
        update_option('vtcore_agents_widgets_list', $widgets);
        $message = __('Widgets Agents listing remapped', 'victheme_demo');
        $this->addLog($message, 'text-success');
      }

      $this->setMap('widget_listing', 'updated', TRUE);
    }

    $this->markImportedStatus();

  }


  /**
   * Remove the marking in database
   * @see VTCore_Demo_Models_Processor::removeData()
   */
  public function removeData() {

    $this->removeMap('agents');
    $this->removeImportedStatus();

  }

}