<?php
/**
 * Processor class for handling the wp usermeta table
 * content exported by victheme_exporter module.
 *
 * @author jason@victheme.com
 *
 */
class VTCore_Demo_Processor_UserMeta
extends VTCore_Demo_Models_Processor {

  protected $processorsDependencies = array(
    'users',
  );

  /**
   * Importing user by injecting the stored usermeta back into
   * current database via WordPress native api function.
   *
   * This function will override current saved user meta
   * eventhough the user is not mapped.
   *
   * If it needs to override data, it will save the old data
   * first so when user reverted the demo data it will restore
   * the old meta value.
   *
   * @see VTCore_Demo_Models_Processor::importData()
   */
  public function importData() {

    // Stop processing if this database is marked
    // as imported previously.
    if ($this->checkImportStatus()) {
      return;
    }

    foreach ($this->config->getData() as $mid => $meta) {

      // Skip if this meta already processed and mapped or empty meta value
      if ($this->checkMap('usermeta', 'ID', $mid)
          || empty($meta['meta_value'])) {

        continue;

      }

      // Non Mapped user
      if (!$this->checkMap('users', 'ID', $meta['user_id'])) {

        $oldmeta = get_user_meta($meta['user_id'], $meta['meta_key'], true);
        $this->setMap('usermetavalue', $mid, $oldmeta);
        $this->setMap('usermetakey', $mid, $meta['meta_key']);
        $meta['maybeuid'] = $meta['user_id'];

        // Skip if old meta is exactly the same as the new value
        // This is for performance reason by minimizing db hit and
        // WP wont update the same value anyway.
        if ($oldmeta == $meta['meta_value']) {
          continue;
        }

      }

      // Mapped user
      else {
        $meta['maybeuid'] = $this->getMappedValue('users', 'ID', $meta['user_id']);
      }


      $newid = update_user_meta((int) $meta['maybeuid'], $meta['meta_key'], $meta['meta_value']);

      if ($newid === false) {
        $this->addLog(sprintf(__('Failed to import user: %s meta data: %s to database', 'victheme_demo'), $meta['user_id'], $meta['meta_key']), 'text-danger');
        $this->reportFailed();

      }
      else {
        $this->setMap('ID', $mid, $newid);
        $this->addLog(sprintf(__('User: %s meta data: %s imported to database', 'victheme_demo'), $meta['user_id'], $meta['meta_key']), 'text-success');
      }

    }

    if ($this->maybeClose()) {
      $this->markImportedStatus();
    }
  }





  /**
   * Removing or reverting the previously imported
   * user meta datas.
   *
   * @see VTCore_Demo_Models_Processor::removeData()
   */
  public function removeData() {

    foreach ($this->getMap('usermeta', 'ID', array()) as $oldID => $newID) {

      // Restore mode
      if ($this->checkMap('usermeta', 'usermetavalue', $oldID)) {
        $oldmeta = $this->getMappedValue('usermeta', 'usermetavalue', $oldID);
        $metakey = $this->getMappedValue('usermeta', 'usermetakey', $oldID);
        update_user_meta((int) $newID, $metakey, $oldmeta);
        $this->addLog(sprintf(__('User: %s meta data: %s restored to old value', 'victheme_demo'), $oldID, $metakey), 'text-success');
      }

      // Delete mode
      else {
        $metakey = $this->getMappedValue('usermeta', 'usermetakey', $oldID);
        delete_user_meta((int) $newID, $metakey);
        $this->addLog(sprintf(__('User: %s meta data: %s deleted from database', 'victheme_demo'), $oldID, $metakey), 'text-success');
      }

      $this->removeMapByValue('usermeta', 'usermetavalue', $oldID);
      $this->removeMapByValue('usermeta', 'usermetakey', $oldID);
      $this->removeMapByValue('usermeta', 'ID', $oldID);
    }

    $this->removeMap('usermeta');
    $this->removeImportedStatus();

  }

}