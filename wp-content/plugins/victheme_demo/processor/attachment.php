<?php
/**
 * Processor class for handling the wp attachment table
 * content exported by victheme_exporter module.
 *
 * @author jason@victheme.com
 *
 */
class VTCore_Demo_Processor_Attachment
extends VTCore_Demo_Models_Processor {

  private $meta;
  private $oldBaseUrl;
  protected $processorsDependencies = array(
    'posts',
    'postmeta',
  );


  /**
   * Overriding parent method to use Post config
   * @see VTCore_Demo_Models_Processor::loadData()
   */
  public function loadData() {
    $className = $this->prefix . 'Posts';
    $metaName = $this->prefix . 'PostMeta';

    if (class_exists($className, true) && class_exists($metaName, true)) {
      $this->meta = new $metaName();
      $this->config = new $className();
      return true;
    }

    return false;
  }


  /**
   * Perform the file importation and updating the post attachment
   * This method will also perform updating attempt to all post
   * content and metadatas for changing the old file URL into the
   * new uploaded one.
   *
   * @see VTCore_Demo_Models_Processor::importData()
   */
  public function importData() {

    // Stop processing if this database is marked
    // as imported previously.
    if ($this->checkImportStatus()) {
      return;
    }

    $this->oldBaseUrl = $this->config->getBase();

    foreach ($this->config->getData() as $pid => $post) {

      if ($this->checkMap('attachment', 'ID', $pid)
          || $post['post_type'] != 'attachment') {
        continue;
      }

      // Build the file
      $remote_url = $post['guid'];
      $localFile = basename($remote_url);
      $temp = VTCore_Wordpress_Utility::getUploadDir();

      // try to use GUID to determine the correct time for folder name
      $time = str_replace(array($temp['baseurl'] . '/', '/' . $localFile), array('', ''), str_replace($this->oldBaseUrl, home_url(), $remote_url));

      $check = explode('/', $time);
      if (!isset($check[0]) || !isset($check[1]) || !is_numeric($check[0]) || !is_numeric($check[1])) {
        $time = $post['post_date'];
      }

      $localDir = wp_upload_dir($time);
      $localFile = basename($remote_url);

      // Bug fix, sometime guid only contain attachment id
      // instead of full url. Try to rebuild the url manualy.
      if (strpos($remote_url, '?attachment_id=') !== false) {

        // Check for valid meta url for the filename
        if ($meta = get_post_meta($this->getMappedValue('posts', 'ID', $pid), '_wp_attached_file', true)) {
          $remote_url = str_replace(home_url(), $this->oldBaseUrl,  $localDir['baseurl'] . '/' . $meta);
        }

        // Rebuilding the file url failed, skipping this
        else {
          $this->addLog(sprintf(__( 'Failed to download %s', 'victheme_demo'), $remote_url), 'text-danger');
          continue;
        }
      }


      // Don't upload multiple files
      if (@file_exists($localDir['path'] . '/' . $localFile)) {
        $upload = array(
          'file' => $localDir['path'] . '/' . $localFile,
          'url' => $localDir['url'] . '/' . $localFile,
          'error' => false,
        );

        $message = sprintf(__( 'Using previously imported file - %s', 'victheme_demo'), $localDir['path'] . '/' . $localFile);
        $this->addLog($message, 'text-success');

      }

      // Fetch new file
      else {

        $upload = wp_upload_bits(basename($remote_url), 0, '', $time);
        $headers = wp_get_http($remote_url, $upload['file']);
        $info = wp_check_filetype($upload['file']);

        if (!$headers
            || $headers['response'] != '200'
            || filesize($upload['file']) == 0
            || $info == false) {

          @unlink($upload['file']);
          $message = sprintf(__( 'Failed to download %s', 'victheme_demo'), $remote_url);

          $this->addLog($message, 'text-danger');

          $this->reportFailed();

          // Don't proceed any further since we dont have valid file
          continue;
        }
      }

      // Get new post
      $newpost = get_post($this->getMappedValue('posts', 'ID', $pid), ARRAY_A);
      $newpost['guid'] = $upload['url'];
      $newpost['post_parent'] = $this->getMappedValue('posts', 'ID', $post['post_parent']);

      // Save Post
      $result = wp_insert_attachment($newpost, $upload['file'], $this->getMappedValue('posts', 'ID', $post['post_parent']));

      if (empty($result)) {
        $this->addLog(sprintf(__( 'Failed to import attachment - %s', 'victheme_demo'), esc_html($post['post_title'])), 'text-danger');
        $this->reportFailed();
      }
      else {

        $this->addLog(sprintf(__('Attachment - %s Imported', 'victheme_demo'), esc_html($post['post_title'])), 'text-success');

        // Update metadata
        wp_update_attachment_metadata($result, wp_generate_attachment_metadata($result, $upload['file']));

        global $wpdb;

        // Remap urls in post_content
        $wpdb->query( $wpdb->prepare("UPDATE {$wpdb->posts} SET post_content = REPLACE(post_content, %s, %s)", array($remote_url, $upload['url'])));

        // Remap all url in post meta
        $wpdb->query( $wpdb->prepare("UPDATE {$wpdb->postmeta} SET meta_value = REPLACE(meta_value, %s, %s)", array($remote_url, $upload['url'])));

        $this->setMap('ID', $pid, $result);
      }

    }

    // Allow user to redownload if we got some error when downloading
    if ($this->maybeClose()) {
      $this->markImportedStatus();
    }

  }



  /**
   * Just remove the import marking and mapping,
   * the actual file, post entry and attachment metadata
   * will be deleted by post deletion.
   *
   * @see VTCore_Demo_Models_Processor::removeData()
   */
  public function removeData() {
    global $wpdb;
    foreach ($this->getMap('attachment', 'ID', array()) as $oldID => $newID) {
      $post_exists = $wpdb->get_row("SELECT * FROM $wpdb->posts WHERE id = '" . $newID . "'", 'ARRAY_A');
      if (!$post_exists) {
        $this->removeMapByValue('attachment', 'ID', $oldID);
      }
      else {
        if (wp_delete_attachment($newID, TRUE) !== FALSE) {
          $this->addLog(sprintf(__('Attachment id: %d deleted', 'victheme_demo'), $newID), 'text-success');
          $this->removeMapByValue('attachment', 'ID', $oldID);
        }
      }
    }

    if ($this->removeMapWhenEmpty('attachment', 'ID')) {
      $this->removeImportedStatus();
    }

  }

}