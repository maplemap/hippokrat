<?php
/**
 * Final processor for cleaning up and perform
 * final actions that is not related to database
 *
 * This must be invoked after everything else.
 *
 * @author jason@victheme.com
 *
 */
class VTCore_Demo_Processor_Cleanup
extends VTCore_Demo_Models_Processor {

  protected $processorsDependencies = array(
    'options',
    'posts',
    'terms',
  );


  /**
   * Overriding parent method, we don't need to load
   * additional data for cleanup process.
   *
   * @see VTCore_Demo_Models_Processor::loadData()
   */
  public function loadData() {
    return !$this->checkImportStatus();
  }

  /**
   * Perform cleanup service.
   *
   * @see VTCore_Demo_Models_Processor::importData()
   */
  public function importData() {

    // Stop processing if this database is marked
    // as imported previously.
    if ($this->checkImportStatus()) {
      return;
    }

    // Rebuild the frontpage page id
    if (!$this->checkMap('cleanup', 'page_on_front', 'status')) {

      $oldID = get_option('page_on_front');
      $newID = $this->getMappedValue('posts', 'ID', $oldID);

      if ($newID) {
        update_option('page_on_front', $newID);
        $message = __('Custom frontpage id updated.', 'victheme_demo');
        $this->addLog($message, 'text-success');
        $this->setMap('page_on_front', 'status', true);
      }
      else {
        $this->reportFailed();
      }
    }


    // Rebuild the blog front page id

    if (!$this->checkMap('cleanup', 'page_for_post', 'status')) {
      $oldID = get_option('page_for_post');
      $newID = $this->getMappedValue('posts', 'ID', $oldID);
      if ($newID) {
        update_option('page_for_post', $newID);
        $message = __('Custom blog front page id updated.', 'victheme_demo');
        $this->addLog($message, 'text-success');

        $this->setMap('page_for_post', 'status', true);
      }
      else {
        $this->reportFailed();
      }
    }


    // Rebuild menu locations
    if (!$this->checkMap('cleanup', 'menu', 'status')) {

      $menu_locations = get_theme_mod('nav_menu_locations', array());

      if (!empty($menu_locations)) {

        $update = true;
        foreach ($menu_locations as $key => $value) {

          if ($this->checkMap('cleanup', 'menu_location', $value) || empty($value)) {
            continue;
          }

          $newTermId = $this->getMappedValue('terms', 'ID', $value);
          if (!empty($newTermId)) {
            $menu_locations[$key] = $newTermId;
            $this->setMap('menu_location', $value, $newTermId);
          }
          else {
            $this->reportFailed();
            $update = false;
          }
        }

        if (!empty($update)) {
          set_theme_mod('nav_menu_locations', $menu_locations);
          $message = __('Menu location updated', 'victheme_demo');
          $this->addLog($message, 'text-success');

          $this->setMap('menu', 'status', TRUE);
        }
      }
    }


    // Fix WooCommerce Pages
    if (!$this->checkMap('cleanup', 'woopages', 'status')) {
      $wooPages = array(
        'woocommerce_shop_page_id' => __('Shop Page', 'victheme_demo'),
        'woocommerce_cart_page_id' => __('Cart Page', 'victheme_demo'),
        'woocommerce_checkout_page_id' => __('Checkout Page', 'victheme_demo'),
        'woocommerce_terms_page_id' => __('Terms Page', 'victheme_demo'),
        'woocommerce_myaccount_page_id' => __('My Account', 'victheme_demo'),
      );

      $update = true;
      foreach ($wooPages as $page => $title) {

        if ($this->checkMap('cleanup', 'woopages', $page)) {
          continue;
        }

        $oldID = get_option($page);

        if (!empty($oldID)) {

          $newID = $this->getMappedValue('posts', 'ID', $oldID);

          if (!empty($newID)) {
            update_option($page, $newID);
            $message = sprintf(__('WooCommerce Page : %s remapped.', 'victheme_demo'), $title);
            $this->addLog($message, 'text-success');
            $this->setMap('woopages', $page, $oldID);
          }
          else {
            $this->reportFailed();
            $update = false;
          }
        }
      }

      if ($update) {
        $this->setMap('woopages', 'status', true);
      }
    }

    if ($this->maybeClose()) {
      $this->markImportedStatus();
    }

  }



  /**
   * Nothing here.
   *
   * @see VTCore_Demo_Models_Processor::removeData()
   */
  public function removeData() {

    // Force WP to rebuild the permalinks
    global $wp_rewrite;
    $wp_rewrite->init();
    flush_rewrite_rules(true);

    $message = __('Permalinks reverted to old value.', 'victheme_demo');
    $this->addLog($message, 'text-success');

    // Schedule clear cache
    update_option('vtcore_clear_cache', true);


    // Perform new cleaning logic to trully clean posts
    foreach ($this->getMap('cleanup', 'woopages', array()) as $page => $oldID) {

      if (!empty($oldID) && $page != 'status') {
        update_option($page, $oldID);
      }

      $this->addLog(sprintf(__('WooCommerce %s reverted to old value', 'victheme_demo'), $page), 'text-success');
    }

    $this->removeMap('cleanup');
    $this->removeImportedStatus();
  }

}