<?php
/**
 * Processor class for handling the wp options table
 * content exported by victheme_exporter module.
 *
 * @author jason@victheme.com
 *
 */
class VTCore_Demo_Processor_Options
extends VTCore_Demo_Models_Processor {


  /**
   * Importing all stored options into the database.
   * This method will not perform any logic for mutating the
   * options value such as changing the post id for attachment.
   *
   *
   * @see VTCore_Demo_Models_Processor::importData()
   */
  public function importData() {

    // Stop processing if this database is marked
    // as imported previously.
    if ($this->checkImportStatus()) {
      return;
    }

    $this->oldBaseUrl = $this->config->getBase();
    $this->baseUrl = get_option('siteurl');

    foreach ($this->config->getData() as $oid => $option) {

      if ($this->checkMap('options', 'ID', $oid)) {
        continue;
      }

      // Check if we got old options and map them for restoration
      if ($oldoption = get_option($option['option_name'])) {
        $this->setMap('data', $oid, $oldoption);
      }

      // Serialize first
      if (is_array($option['option_value'])) {
        $option['option_value'] = serialize($option['option_value']);
        $option['option_value'] = str_replace($this->oldBaseUrl, $this->baseUrl, $option['option_value']);
        $option['option_value'] = VTCore_Utility::fixSerialString($option['option_value']);
        $option['option_value'] = unserialize($option['option_value']);
      }
      else {
        // Global nuke old base url in the content.
        $option['option_value'] = str_replace($this->oldBaseUrl, $this->baseUrl, $option['option_value']);
      }

      if (is_serialized_string($option['option_value'])) {
        $option['option_value'] = unserialize(VTCore_Utility::fixSerialString($option['option_value']));
      }

      delete_option($option['option_name']);

      $result = add_option($option['option_name'], $option['option_value'], '', $option['autoload']);

      if (!$result) {
        $this->addLog(sprintf(__( 'Failed to import option %s', 'victheme_demo'), $option['option_name']), 'text-danger');
        $this->reportFailed();
      }
      else {
        $this->addLog(sprintf(__('Options %s Imported', 'victheme_demo'), $option['option_name']), 'text-success');
        $this->setMap('ID', $oid, $option['option_name']);
      }
    }

    if ($this->maybeClose()) {
      $this->markImportedStatus();
    }

  }



  /**
   * Removing or reverting the previously imported
   * options entry
   *
   * @see VTCore_Demo_Models_Processor::removeData()
   */
  public function removeData() {

    foreach ($this->getMap('options', 'ID', array()) as $oid => $name) {
      delete_option($name);
      $this->addLog(sprintf(__('Option %s deleted from database.', 'victheme_demo'), $name), 'text-success');
      $this->removeMapByValue('options', 'ID', $oid);

      // Restore old options
      if ($this->checkMap('options', 'data', $oid)) {
        add_option($name, $this->getMappedValue('options', 'data', $oid));
        $this->addLog(sprintf(__('Option %s restored to original value.', 'victheme_demo'), $name), 'text-success');
        $this->removeMapByValue('options', 'data', $oid);
      }
    }

    if ($this->removeMapWhenEmpty('options', 'ID')) {
      $this->removeImportedStatus();
    }
  }
}