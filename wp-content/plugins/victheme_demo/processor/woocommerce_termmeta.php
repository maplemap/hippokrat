<?php
/**
 * Processor for processing WooCommerce custom
 * meta system
 *
 * @authod jason.xie@victheme.com
 */
class VTCore_Demo_Processor_Woocommerce__TermMeta
extends VTCore_Demo_Models_Processor  {

  protected $processorsDependencies = array(
    'terms',
  );

  protected $dependencies = 'woocommerce';

  public function importData() {
    if ($this->checkImportStatus()) {
      return;
    }

    global $wpdb;

    foreach ($this->config->getData() as $aid => $meta) {
      if ($this->checkMap('woocommerce_termmeta', 'termID', $aid)) {
        continue;
      }

      $meta['woocommerce_term_id'] = $this->getMappedValue('terms', 'ID', $meta['woocommerce_term_id']);
      unset($meta['meta_id']);
      $result = $wpdb->insert("{$wpdb->prefix}woocommerce_termmeta", $meta);

      if (!empty($result)) {
        $this->setMap('termID', $aid, $wpdb->insert_id);
        $this->addLog(sprintf(__('WC Term Meta %s inserted to database', 'victheme_demo'), $aid), 'text-success');

      }
      else {
        $this->addLog(sprintf(__('Failed to insert WC Term Meta %s to database', 'victheme_demo'), $aid), 'text-error');
        $this->reportFailed();

      }
    }

    if ($this->maybeClose()) {
      $this->markImportedStatus();
    }
  }

  public function removeData() {

    global $wpdb;
    foreach ($this->getMap('woocommerce_termmeta', 'termID', array()) as $aid => $newID) {

      $wpdb->delete("{$wpdb->prefix}woocommerce_termmeta", array(
        'meta_id' => $newID,
      ));

      $this->addLog(sprintf(__('WC Term Meta %s deleted from database', 'victheme_demo'), $newID), 'text-success');
      $this->removeMapByValue('woocommerce_termmeta', 'termID', $aid);
    }

    $this->removeMap('woocommerce_termmeta');
    $this->removeImportedStatus();
  }
}

