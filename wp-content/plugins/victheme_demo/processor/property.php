<?php

/**
 * Processor for processing agents
 * for fixing the agents profile related
 * taxonomy and images
 *
 * @author jason@victheme.com
 *
 */
class VTCore_Demo_Processor_Property
extends VTCore_Demo_Models_Processor {

  protected $dependencies = 'victheme_property';
  protected $processorsDependencies = array(
    'posts',
    'postmeta',
    'attachment',
    'terms',
    'widgets',
  );

  /**
   * Overriding parent method to use Post config
   * @see VTCore_Demo_Models_Processor::loadData()
   */
  public function loadData() {
    $postName = $this->prefix . 'Posts';

    if (class_exists($postName, TRUE)) {
      $this->posts = new $postName();
      return TRUE;
    }

    return FALSE;
  }


  /**
   * The main controller method for performing
   * the data remapping.
   *
   * This has to run after the content processor.
   *
   * @see VTCore_Demo_Models_Processor::importData()
   */
  public function importData() {

    // Stop processing if this database is marked
    // as imported previously.
    if ($this->checkImportStatus()) {
      return;
    }

    $this->fields = new VTCore_Property_Fields();
    $this->oldBaseUrl = $this->posts->getBase();
    $this->baseUrl = get_option('siteurl');

    // Process post meta
    foreach ($this->posts->getData() as $post) {

      if ($post['post_type'] != 'property' || $this->checkMap('property', 'ID', $post['ID'])) {
        continue;
      }

      $newId = $this->getMappedValue('posts', 'ID', $post['ID']);

      // Use property object to update the
      // new metadata. This is the recommended
      // way to ensure maximum compatibility with
      // the property plugin.
      $propertyObject = new VTCore_Property_Entity_Property($newId);
      $propertyObject->loadMetaData();

      // Don't process broken property
      if (!$propertyObject->getMeta('type')) {
        continue;
      }


      // Process types and its related entity
      if ($propertyObject->getMeta('type')) {

        // Update the property meta
        $propertyObject->addMeta('type', $this->getMappedValue('terms', 'ID', $propertyObject->getMeta('type')));
        $propertyObject->initializeType()->saveMetaType();
      }

      // Only process fields if it has object defined
      if ($propertyObject->getMeta('fields')) {
        foreach ($propertyObject->getMeta('fields') as $fid => $storedDatas) {
          foreach ($storedDatas as $sid => $data) {

            $settings = $this->fields->getFieldValue($fid, 'storage', $sid);

            // Convert taxonomy id
            if (isset($settings['mode']) && $settings['mode'] == 'taxonomy_id') {

              $is_array = is_array($data);
              $data = (array) $data;

              foreach ($data as $delta => $term_id) {
                $data[$delta] = $this->getMappedValue('terms', 'ID', $term_id);
              }

              if (!$is_array) {
                $data = array_shift($data);
              }

              if (!empty($data)) {
                $propertyObject->addMeta('fields.' . $fid . '.' . $sid, $data);
              }
            }

            // Convert images
            if (isset($settings['mode']) && ($settings['mode'] == 'image' || $settings['mode'] == 'video' || $settings['mode'] == 'audio')) {
              $is_array = is_array($data);

              $data = (array) $data;
              foreach ($data as $delta => $post_id) {
                if (is_numeric($post_id)) {
                  $data[$delta] = $this->getMappedValue('posts', 'ID', $post_id);
                }
                else {
                  $data[$delta] = str_replace($this->oldBaseUrl, $this->baseUrl, $post_id);
                }
              }

              if (!$is_array) {
                $data = array_shift($data);
              }

              if (!empty($data)) {
                $propertyObject->addMeta('fields.' . $fid . '.' . $sid, $data);
              }
            }
          }
        }

        $propertyObject->initializeFields()->saveMetaFields();
      }


      // Updating attributes
      $attributes = $propertyObject->getMeta('attributes');
      if (!empty($attributes)) {

        foreach ($attributes as $aid => $terms) {
          foreach ($terms as $delta => $term_id) {
            if (is_numeric($term_id)) {
              $terms[$delta] = $this->getMappedValue('terms', 'ID', $term_id);
            }
          }

          if (!empty($terms)) {
            $propertyObject->addMeta('attributes.' . $aid, $terms);
          }
        }

        $propertyObject->initializeAttributes()->saveMetaAttributes();
      }

      // Resave the meta data
      $propertyObject->saveMetaData();

      $message = sprintf(__('Property - %s meta data updated', 'victheme_demo'), $newId);
      $this->addLog($message, 'text-success');

      $this->setMap('ID', $post['ID'], $newId);
    }


    // Convert property types registry
    if (!$this->checkMap('property', 'types', 'updated')) {
      $types = get_option('vtcore_property_types', array());
      $newTypes = array();
      foreach ($types as $tid => $data) {

        if (isset($data['id'])) {
          $data['id'] = $this->getMappedValue('terms', 'ID', $data['id']);
        }

        if (isset($data['maps_icon'])) {
          $data['maps_icon'] = $this->getMappedValue('posts', 'ID', $data['maps_icon']);
        }

        $newTypes[$data['id']] = $data;
      }

      $result = update_option('vtcore_property_types', $newTypes);
      if ($result) {
        $this->addLog( __('Property - property types remapped', 'victheme_demo'), 'text-success');
        $this->setMap('types', 'updated', $types);
      }
      else {
        $this->addLog( __('Property - failed to remap property types', 'victheme_demo'), 'text-danger');
        $this->reportFailed();
      }
    }

    // Convert configuration
    if (!$this->checkMap('property', 'config', 'updated')) {
      $configObject = new VTCore_Property_Config();

      // Update contact form 7
      $newId = $this->getMappedValue('posts', 'ID', $configObject->get('property_contactform7.form_id'));
      $configObject->add('property_contactform7.form_id', $newId);

      // Update Maps icon
      $icon = $configObject->get('property_maps.data.icon');
      if (is_numeric($icon)) {
        $icon = $this->getMappedValue('posts', 'ID', $icon);
      }
      else {
        $icon = str_replace($this->oldBaseUrl, $this->baseUrl, $icon);
      }

      $configObject->add('property_maps.data.icon', $icon);
      $configObject->save();

      $message = __('Property - plugin configuration updated', 'victheme_demo');
      $this->addLog($message, 'text-success');
      $this->setMap('config', 'updated', TRUE);
    }


    // Widget property maps
    if (!$this->checkMap('property', 'widget_map', 'updated')) {

      $oldwidgets = $widgets = get_option('widget_vtcore_property_widgets_map', array());
      foreach ($widgets as $delta => $widget) {
        if (!is_numeric($delta) || empty($widget)) {
          continue;
        }

        foreach ($widget['types'] as $id => $term_id) {
          $widgets[$delta]['types'][$id] = $this->getMappedValue('terms', 'ID', $term_id);
        }

        if (isset($widget['data']['icon']) && !empty($widget['data']['icon'])) {
          if (is_numeric($widget['data']['icon'])) {
            $widgets[$delta]['data']['icon'] = $this->getMappedValue('posts', 'ID', $widget['data']['icon']);
          }
          else {
            $widgets[$delta]['data']['icon'] = str_replace($this->oldBaseUrl, $this->baseUrl, $widget['data']['icon']);
          }
        }
      }

      $result = update_option('widget_vtcore_property_widgets_map', $widgets);
      if ($result) {
        $this->addLog(__('Widgets Property Map remapped', 'victheme_demo'), 'text-success');
        $this->setMap('widget_map', 'updated', TRUE);
      }
      elseif ($oldwidgets != $widgets) {
        $this->addLog(__('Failed to remap Widgets Property Map', 'victheme_demo'), 'text-danger');
        $this->reportFailed();
      }
    }



    // Widget property search
    if (!$this->checkMap('property', 'widget_search', 'updated')) {

      $widgets = get_option('widget_vtcore_property_widgets_search', array());
      foreach ($widgets as $delta => $widget) {

        if (!is_numeric($delta) || empty($widget) || !isset($widget['types']) || empty($widget['types'])) {
          continue;
        }

        foreach ($widget['types'] as $id => $term_id) {
          $widgets[$delta]['types'][$id] = $this->getMappedValue('terms', 'ID', $term_id);
        }
      }


      $result = update_option('widget_vtcore_property_widgets_search', $widgets);
      if ($result) {
        $this->addLog(__('Widgets Property Search remapped', 'victheme_demo'), 'text-success');
        $this->setMap('widget_search', 'updated', TRUE);
      }
      else {
        $this->addLog(__('Failed to remap Widgets Property Search', 'victheme_demo'), 'text-danger');
        $this->reportFailed();
      }
    }


    // Fix shortcodes
    if (!$this->checkMap('property', 'shortcodes', 'updated')) {
      foreach ($this->posts->getData() as $pid => $post) {

        if (strpos($post['post_content'], 'types__') !== FALSE) {
          $newPostID = $this->getMappedValue('posts', 'ID', $pid);
          $newPost = get_post($newPostID, ARRAY_A);
          $oldTypes = $this->getMappedValue('property', 'types', 'updated');

          if (is_array($oldTypes)) {
            foreach ($oldTypes as $tid => $data) {
              $original = 'types__' . $data['id'] . '="' . $data['id'] . '"';
              $replacement = 'types__' . $this->getMappedValue('terms', 'ID', $data['id']) . '="' . $this->getMappedValue('terms', 'ID', $data['id']) . '"';
              $newPost['post_content'] = str_replace($original, $replacement, $newPost['post_content']);
            }
          }

          // Update content
          global $wpdb;
          $wpdb->query($wpdb->prepare("UPDATE {$wpdb->posts} SET post_content = %s WHERE ID=%s", $newPost['post_content'], $newPostID));
          $message = sprintf(__('Property %s shortcodes updated', 'victheme_demo'), $newPost['post_title']);
          $this->addLog($message, 'text-success');

        }
      }

      $this->setMap('shortcodes', 'updated', TRUE);
    }


    if ($this->maybeClose()) {
      $this->markImportedStatus();
    }

  }


  /**
   * Remove the marking in database
   * @see VTCore_Demo_Models_Processor::removeData()
   */
  public function removeData() {
    $this->removeMap('property');
    $this->removeImportedStatus();
  }

}