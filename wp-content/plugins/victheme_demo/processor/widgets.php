<?php
/**
 * Generic processor for processing various kind of widget.
 *
 * Please use the $widgetProcessor to add new specific
 * processors for different plugins and define the
 * method with name exactly the same as the one defined
 * in the $widgetProcessor.
 *
 * Inside the processor method, you can utilize the
 * $widgetName, $widgets, $update, $widget, $delta, $key
 * and $value variable to perform the alterations.
 *
 * @author jason@victheme.com
 *
 */
class VTCore_Demo_Processor_Widgets
extends VTCore_Demo_Models_Processor {

  protected $processorsDependencies = array(
    'options',
    'posts',
    'attachment',
    'terms',
  );

  private $widgetProcessor = array(
    'processor_displayWidget',
    'processor_menuWidget',
    'processor_imageWidget',
  );

  private $widgetName;
  private $widgets;
  private $update;
  private $widget;
  private $delta;
  private $key;
  private $value;


  /**
   * Overriding parent method, we don't need to load
   * additional data for cleanup process.
   *
   * @see VTCore_Demo_Models_Processor::loadData()
   */
  public function loadData() {
    $name = $this->prefix . 'Options';

    if (class_exists($name, TRUE)) {

      $this->config = new $name();
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Perform cleanup service.
   *
   * @see VTCore_Demo_Models_Processor::importData()
   */
  public function importData() {

    // Stop processing if this database is marked
    // as imported previously.
    if ($this->checkImportStatus()) {
      return;
    }


    foreach ($this->config->getData() as $oid => $result) {

      $this->widget_name = $result['option_name'];

      if ($this->checkMap('widgets', 'ID', $oid)
          || strpos($this->widget_name, 'widget_') === false) {

        continue;
      }

      $this->widgets = maybe_unserialize($result['option_value']);

      if (!is_array($this->widgets)) {
        continue;
      }

      $this->update = false;

      // Looping into single widget instance
      foreach ($this->widgets as $delta => $widget) {
        if (!is_numeric($delta)) {
          continue;
        }

        $this->widget = $widget;
        $this->delta = $delta;

        // Looping into single widget meta key
        foreach ($widget as $key => $value) {

          $this->key = $key;
          $this->value = $value;

          foreach ($this->widgetProcessor as $processor) {
            $this->{$processor}();
          }
        }
      }

      if ($this->widgets == $result['option_value']) {
        $this->update = false;
      }

      // Update this widget
      if ($this->update) {
        $result = update_option($this->widget_name, $this->widgets);
        if ($result) {
          $this->addLog(sprintf(__('Widget %s updated', 'victheme_demo'), $this->widget_name), 'text-success');
          $this->setMap('widgets', 'ID', $this->widget_name, TRUE);
        }
        else {
          $this->addLog(sprintf(__('Failed to update Widget %s', 'victheme_demo'), $this->widget_name), 'text-danger');
          $this->reportFailed();
        }
      }
    }

    if ($this->maybeClose()) {
      $this->markImportedStatus();
    }

  }



  /**
   * Display widgets plugin processor
   */
  private function processor_displayWidget() {

    // Updating display widgets stored post id
    if (strpos($this->key, 'page-') !== false) {
      $oldid = str_replace('page-', '', $this->key);
      if (is_numeric($oldid)) {
        $newid = $this->getMappedValue('posts', 'ID', $oldid);
        unset($this->widgets[$this->delta][$this->key]);
        $this->widgets[$this->delta]['page-' . $newid] = $this->value;
        $this->update = true;
      }
    }

  }

  /**
   * Navigation Custom Menu plugin processor
   */
  private function processor_menuWidget() {

    // Updating display widgets stored post id
    if (strpos($this->key, 'nav_menu') !== false
        && is_numeric($this->value)) {
      $this->widgets[$this->delta]['nav_menu'] = $this->getMappedValue('terms', 'ID', $this->value);
      $this->update = true;
    }

  }



  /**
   * Preprocessing maybe image id value
   */
  private function processor_imageWidget() {

    // Updating spImage attached id
    if (strpos($this->key, 'attachment_id') !== false
        && is_numeric($this->value)) {

      $this->widgets[$this->delta]['attachment_id'] = $this->getMappedValue('posts', 'ID', $this->value);
      $this->update = true;

    }

    // Updating slick attached id
    if (strpos($this->key, 'entries') !== false) {

      foreach ($this->value as $delta => $data) {
        if (!isset($data['image']) || !is_numeric($data['image'])) {
          continue;
        }

        $this->widgets[$this->delta]['entries'][$delta]['image'] = $this->getMappedValue('posts', 'ID', $data['image']);
        $this->update = true;
      }
    }

  }

  /**
   * Nothing here. all the actual old data restoration will
   * be handled by the options processor.
   *
   * @see VTCore_Demo_Models_Processor::removeData()
   */
  public function removeData() {
    $this->removeMap('widgets');
    $this->removeImportedStatus();

  }

}