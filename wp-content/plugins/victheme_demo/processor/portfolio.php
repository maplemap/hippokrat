<?php

/**
 * Processor for processing portfolio
 * data, especially fixing the media (image and video)
 * attachment post id since it will change
 * from the original post id into the mapped
 * new post id.
 *
 * @author jason@victheme.com
 *
 */
class VTCore_Demo_Processor_Portfolio
extends VTCore_Demo_Models_Processor {

  protected $dependencies = 'victheme_portfolio';
  protected $processorsDependencies = array(
    'posts',
    'postmeta',
    'attachment',
    'widgets',
  );

  /**
   * Overriding parent method to use Post config
   * @see VTCore_Demo_Models_Processor::loadData()
   */
  public function loadData() {

    $metaName = $this->prefix . 'Postmeta';
    $postName = $this->prefix . 'Posts';

    if (class_exists($metaName, TRUE)
      && class_exists($postName, TRUE)
      && defined('VTCORE_PORTFOLIO_LOADED')
    ) {

      $this->config = new $metaName();
      $this->posts = new $postName();

      return TRUE;
    }

    return FALSE;
  }


  /**
   * The main controller method for performing
   * the data remapping.
   *
   * This has to run after the content processor.
   *
   * @see VTCore_Demo_Models_Processor::importData()
   */
  public function importData() {

    // Stop processing if this database is marked
    // as imported previously.
    if ($this->checkImportStatus()) {
      return;
    }

    // Process post meta
    foreach ($this->config->getData() as $meta) {

      if ($meta['meta_key'] != '_portfolio_data' || $this->checkMap('portfolio', 'ID', $meta['post_id'])) {
        continue;
      }

      // Remap the thumbnail
      $meta['meta_value']['media']['thumbnail'] = $this->getMappedValue('posts', 'ID', $meta['meta_value']['media']['thumbnail']);

      foreach ($meta['meta_value']['media']['contents'] as $delta => $medias) {
        foreach ($medias as $key => $media) {
          $meta['meta_value']['media']['contents'][$delta][$key] = $this->getMappedValue('posts', 'ID', $media);
        }
      }

      $newPostId = $this->getMappedValue('posts', 'ID', $meta['post_id']);
      $result = update_post_meta($newPostId, '_portfolio_data', $meta['meta_value']);

      if ($result) {
        $this->addLog(sprintf(__('Post - %s portfolio data media remapped', 'victheme_demo'), $newPostId), 'text-success');
        $this->setMap('ID', $meta['post_id'], $newPostId);
      }
      else {
        $this->addLog(sprintf(__('Failed to remap Post - %s portfolio data media', 'victheme_demo'), $newPostId), 'error');
        $this->reportFailed();
      }

    }


    // Updating widget gallery
    if (!$this->checkMap('portfolio', 'gallery', 'updated')) {
      $oldWidgets = $widgets = get_option('widget_vtcore_portfolio_widgets_gallery', array());
      foreach ($widgets as $delta => $widget) {

        if (!is_numeric($delta) || empty($widget)) {
          continue;
        }

        $widgets[$delta]['postid'] = $this->getMappedValue('posts', 'ID', $widget['postid']);
      }

      $result = update_option('widget_vtcore_portfolio_widgets_gallery', $widgets);

      if ($result !== false) {
        $this->setMap('gallery', 'updated', TRUE);
        $this->addLog(__('Widgets portfolio gallery remapped', 'victheme_demo'), 'text-success');
      }
      elseif ($widgets != $oldWidgets) {
        $this->addLog(__('Failed to remap Widgets portfolio gallery', 'victheme_demo'), 'text-danger');
        $this->reportFailed();
      }
    }


    // Updating widget project
    if (!$this->checkMap('portfolio', 'project', 'updated')) {

      $oldWidgets  = $widgets = get_option('widget_vtcore_portfolio_widgets_project', array());
      foreach ($widgets as $delta => $widget) {
        if (!is_numeric($delta) || empty($widget)) {
          continue;
        }

        $widgets[$delta]['postid'] = $this->getMappedValue('posts', 'ID', $widget['postid']);
      }


      $result = update_option('widget_vtcore_portfolio_widgets_project', $widgets);
      if ($result !== false) {
        $this->setMap('project', 'updated', TRUE);
        $this->addLog(__('Widgets portfolio project remapped', 'victheme_demo'), 'text-success');
      }
      elseif ($widgets != $oldWidgets) {
        $this->addLog(__('Failed to remap Widgets portfolio project', 'victheme_demo'), 'text-danger');
      }
    }

    if ($this->maybeClose()) {
      $this->markImportedStatus();
    }
  }


  /**
   * Remove the marking in database
   *
   * Posts, meta and widget will be removed by post
   * processor and option processor
   *
   * @see VTCore_Demo_Models_Processor::removeData()
   */
  public function removeData() {
    $this->removeMap('portfolio');
    $this->removeImportedStatus();
  }

}