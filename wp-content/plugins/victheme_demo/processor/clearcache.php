<?php
/**
 * Final processor for just for force
 * site to perform the late cache clearing
 * called by cleanup.php
 *
 * @author jason@victheme.com
 *
 */
class VTCore_Demo_Processor_ClearCache
extends VTCore_Demo_Models_Processor {


  /**
   * Overriding parent method, we don't need to load
   * additional data for clearing cache process.
   *
   * @see VTCore_Demo_Models_Processor::loadData()
   */
  public function loadData() {
    return true;
  }

  /**
   * Perform cleanup service.
   *
   * @see VTCore_Demo_Models_Processor::importData()
   */
  public function importData() {

    // Force WP to rebuild the permalinks
    global $wp_rewrite;
    $wp_rewrite->init();
    flush_rewrite_rules(true);

    $this->addLog(__('Permalinks updated', 'victheme_demo'), 'text-success');

    // Schedule clear cache
    update_option('vtcore_clear_cache', true);

    // Just give notice as VTCore will handle the rest
    $this->addLog(__('Cache Cleared', 'victheme_demo'), 'text-success');

  }



  /**
   * Nothing here.
   *
   * @see VTCore_Demo_Models_Processor::removeData()
   */
  public function removeData() {

    // Just give notice as VTCore will handle the rest
    $this->addLog(__('Cache Cleared', 'victheme_demo'), 'text-success');
    $this->removeImportedStatus();

    // Delete all imported status when no more maps available
    if (empty($this->map)) {
      $this->deleteImportedStatus();
    }

  }

}