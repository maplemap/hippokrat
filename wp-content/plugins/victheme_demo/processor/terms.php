<?php
/**
 * Processor class for handling the wp terms table
 * content exported by victheme_exporter module.
 *
 * @author jason@victheme.com
 *
 */
class VTCore_Demo_Processor_Terms
extends VTCore_Demo_Models_Processor {

  private $terms;
  private $term_taxonomy;

  /**
   * Overriding parent method.
   * This is needed because we need to merge
   * terms and term_taxonomy data before we can
   * actually inject the taxonomy into the database.
   *
   * @see VTCore_Demo_Models_Processor::loadData()
   */
  public function loadData() {

    $termsClassName = $this->prefix . 'Terms';
    $taxonomyClassName = $this->prefix . 'Term__Taxonomy';

    if (class_exists($termsClassName, true)
        && class_exists($taxonomyClassName, true)) {

      $this->terms = new $termsClassName();
      $this->term_taxonomy = new $taxonomyClassName();

      $terms = $this->terms->getData();

      foreach ($this->term_taxonomy->getData() as $taxid => $taxonomy) {

        // Don't build broken taxonomy
        if (!isset($terms[$taxonomy['term_id']])) {
          continue;
        }

        $this->config[$taxid] = array_merge($taxonomy, $terms[$taxonomy['term_id']]);
      }

      ksort($this->config);

    }

    return !empty($this->config) ? true : false;
  }




  /**
   * Method for importing taxonomy terms into database.
   * This method will import all taxonomy terms including
   * the categories, tags, menu and more. It will also
   * perform the repairing of the parent children relationship
   * for each taxonomies.
   *
   * It will try to avoid inserting the taxonomy terms if
   * it is exists in the database or previously imported.
   *
   * @see VTCore_Demo_Models_Processor::importData()
   */
  public function importData() {

    global $wpdb;

    // Stop processing if this database is marked
    // as imported previously.
    if ($this->checkImportStatus()) {
      return;
    }

    // Inject the terms to database
    foreach ($this->config as $tid => $term) {

      // Skip if this meta already processed and mapped.
      if ($this->checkMap('terms', 'term_taxonomy_id', $tid)) {
        continue;
      }

      $this->setMap('taxonomy', $term['term_id'], $term['taxonomy']);

      // Just map the term if already exists in the database.
      if ($existingTerm = term_exists($term['slug'], $term['taxonomy'])) {
        $this->setMap('ID', $term['term_id'], (int) $existingTerm['term_id']);
        continue;
      }

      // Just create the term_taxonomy entry for same term
      // with multiple taxonomy entry
      if ($this->checkMap('terms', 'ID', $term['term_id'])) {
        $args = array(
          'term_id' => $this->getMappedValue('terms', 'ID', $term['term_id']),
          'taxonomy' => $term['taxonomy'],
          'description' => $term['description'],
          'parent' => 0,
          'count' => $term['count'],
        );

        $result = $wpdb->insert($wpdb->term_taxonomy, $args);

        if (!$result) {
          $message = sprintf(__( 'Failed to add terms_taxonomy entry for term %s - %s', 'victheme_demo'), esc_html($term['taxonomy']), esc_html($term['name']) );
          $this->addLog($message, 'text-danger');
        }
        else {
          $message = sprintf(__('Taxonomy term entry for Term %s - %s recorded', 'victheme_demo'), esc_html($term['taxonomy']), esc_html($term['name']) );
          $this->addLog($message, 'text-success');
          $this->setMap('term_taxonomy_id', $tid, (int) $wpdb->insert_id);
        }

        continue;
      }


      // @Bugfix WP 4.0 doesn't allow invalid parent id
      $term['parent'] = 0;

      if (taxonomy_exists($term['taxonomy'])) {
        $result = wp_insert_term($term['name'], $term['taxonomy'], array('slug' => $term['slug']));
      }
      else {
        $message = sprintf(__( 'Failed to import term %s - %s, Taxonomy - %s doesn\'t exists', 'victheme_demo'), esc_html($term['taxonomy']), esc_html($term['name']), esc_html($term['taxonomy']) );
        $this->addLog($message, 'text-danger');
        $this->reportFailed();
        continue;
      }

      if (is_wp_error($result)) {
        $message = sprintf(__( 'Failed to import term %s - %s', 'victheme_demo'), esc_html($term['taxonomy']), esc_html($term['name']) );
        $this->reportFailed();
        $this->addLog($message, 'text-danger');
      }
      else {
        $message = sprintf(__('Term %s - %s Imported', 'victheme_demo'), esc_html($term['taxonomy']), esc_html($term['name']) );
        $this->addLog($message, 'text-success');
        $this->setMap('ID', $term['term_id'], $result['term_id']);
        $this->setMap('term_taxonomy_id', $tid, $result['term_taxonomy_id']);
      }
    }

    // Restructure the parent - children relationship
    foreach ($this->config as $tid => $term) {

      if ($this->checkMap('terms', 'relationship', $tid)) {
        continue;
      }


      if ($this->getMappedValue('terms', 'ID', $term['parent'])
          && $this->getMappedValue('terms', 'ID', $term['term_id'])) {

        $result = wp_update_term($this->getMappedValue('terms', 'ID', $term['term_id']), $term['taxonomy'], array(
          'parent' => $this->getMappedValue('terms', 'ID', $term['parent']),
          'name' => $term['name'],
        ));

        if (is_wp_error($result)) {
          $message = sprintf(__( 'Failed to update hierarchy for term %s - %s', 'victheme_demo'), esc_html($term['taxonomy']), esc_html($term['name']) );
          $this->addLog($message, 'text-danger');
        }
        else {
          $this->setMap('relationship', $tid, $term['parent']);

          $message = sprintf(__('Term %s - %s hiearchy updated', 'victheme_demo'), esc_html($term['taxonomy']), esc_html($term['name']) );
          $this->addLog($message, 'text-success');
        }

      }
    }

    if ($this->maybeClose()) {
      $this->markImportedStatus();
    }

  }





  /**
   * Removing or reverting the previously imported
   * texonomy terms
   *
   * @see VTCore_Demo_Models_Processor::removeData()
   */
  public function removeData() {

    foreach ($this->getMap('terms', 'ID', array()) as $oldTid => $newTid) {
      $taxonomy = $this->getMappedValue('terms', 'taxonomy', $oldTid);
      $result = wp_delete_term($newTid, $taxonomy);

      if (!is_wp_error($result)) {
        $this->addLog(sprintf(__('Term %s deleted from database', 'victheme_demo'), $newTid), 'text-success');
      }
      else {
        $this->addLog(sprintf(__('Failed to delete Term %s', 'victheme_demo'), $newTid), 'error');
      }

      $this->removeMapByValue('terms', 'ID', $oldTid);
    }

    if ($this->removeMapWhenEmpty('terms', 'ID')) {
      $this->removeImportedStatus();
    }

  }

}