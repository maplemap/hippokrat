<?php
/**
 * Processor class for handling the wp posts table
 * content exported by victheme_exporter module.
 *
 * @author jason@victheme.com
 *
 */
class VTCore_Demo_Processor_Posts
extends VTCore_Demo_Models_Processor {

  private $baseUrl;
  private $oldBaseUrl;

  /**
   * Processing Post entry importation process.
   * This method will import all type of post type including
   * the Navigation, Attachment, Post, Page and custom posts type.
   *
   * @see VTCore_Demo_Models_Processor::importData()
   */
  public function importData() {

    // Stop processing if this database is marked
    // as imported previously.
    if ($this->checkImportStatus()) {
      return;
    }

    $this->oldBaseUrl = $this->config->getBase();
    $this->baseUrl = get_option('siteurl');

    foreach ($this->config->getData() as $pid => $post) {

      if ($this->checkMap('posts', 'ID', $pid)) {
        continue;
      }


      // Try to remap the author if failed use root instead
      $uid = $this->getMappedValue('users', 'ID', $post['post_author']);
      if (!$uid) {
        $uid = 1;
      }

      $post['post_author'] = $uid;

      // WP will complain error if we set this.
      // Instead rely on the mapped storage to retrieve ID.
      unset($post['ID']);

      // Remove GUID Let wordpress rebuild the GUID
      unset($post['guid']);

      // Global nuke old base url in the content.
      $post['post_content'] = str_replace($this->oldBaseUrl, $this->baseUrl, $post['post_content']);

      // Save Post
      $result = wp_insert_post($post, true);

      if (is_wp_error($result)) {
        $message = sprintf(__( 'Failed to import post %s - %s', 'victheme_demo'), $post['post_type'], esc_html($post['post_title']));
        $this->reportFailed();
        $this->addLog($message, 'text-danger');
      }
      else {
        $message = sprintf(__('Post %s - %s Imported', 'victheme_demo'), $post['post_type'], esc_html($post['post_title']));
        $this->addLog($message, 'text-success');

        $this->setMap('ID', $pid, $result);
      }

    }

    // Update post parent
    // New to handle media.php breaks on invalid parent since wp 4.0
    foreach ($this->config->getData() as $pid => $post) {

      if ($this->checkMap('posts', 'ParentID', $pid) || $post['post_parent'] == 0) {
        continue;
      }

      $newID = $this->getMappedValue('posts', 'ID', $pid);
      $postParentID = $this->getMappedValue('posts', 'ID', $post['post_parent']);
      global $wpdb;

      // Remap post parent id
      $wpdb->query( $wpdb->prepare("UPDATE {$wpdb->posts} SET post_parent=%s WHERE ID=%s", array($postParentID, $newID)));

      $message = sprintf(__('Post %s - %s Parent remapped', 'victheme_demo'), $post['post_type'], esc_html($post['post_title']));
      $this->addLog($message, 'text-success');

      $this->setMap('ParentID', $pid, $postParentID);
    }

    if ($this->maybeClose()) {
      $this->markImportedStatus();
    }

  }



  /**
   * Removing or reverting the previously imported
   * posts entry
   *
   * @see VTCore_Demo_Models_Processor::removeData()
   */
  public function removeData() {

    // Perform new cleaning logic to trully clean posts
    foreach ($this->getMap('posts', 'ID', array()) as $oldID => $newID) {
      wp_delete_post($newID, true);
      $this->addLog(sprintf(__('Post %s deleted from database', 'victheme_demo'), $newID), 'text-success');
      $this->removeMapByValue('posts', 'ID', $oldID);
    }

    if ($this->removeMapWhenEmpty('posts', 'ID')) {
      $this->removeImportedStatus();
    }

  }

}