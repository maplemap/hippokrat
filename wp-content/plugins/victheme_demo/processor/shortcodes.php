<?php
/**
 * Processor class for handling mutating Media Attachment
 * that is attached via shortcode in the post content.
 *
 * Supports :
 * Visual Composer
 * Visual Bootstrap
 * Visual Candy
 * Victheme Maps
 * Contact form 7
 * Victheme Portfolio
 * Victheme Agents
 *
 * @author jason@victheme.com
 *
 */
class VTCore_Demo_Processor_Shortcodes
extends VTCore_Demo_Models_Processor {

  protected $processorsDependencies = array(
    'posts',
    'postmeta',
    'attachment',
    'terms',
  );

  /**
   * This will replace the value in the
   * attributes with the mapped post id
   */
  private $singlePostId = array(
    'contact-form-7' => 'id',
    'vpgallery' => 'postid',
    'vpproject' => 'postid',
    'vc_single_image' => 'image',
    'vmmarker' => 'icon',
    'bsmediaobject' => 'img',
    'jqflipcontent' => 'image',
    'wptestimonial' => 'image',
    'wpcarouselimage' => 'image',
    'centerline' => 'image_attachmentid',
    'historyinner' => 'image_attachmentid',
    'vpropertyattributes' => 'postid',
    'vpropertyfields' => 'postid',
    'vpropertytype' => 'postid',
    'vpropertymap' => 'data__icon',
    'vc_basic_grid' => 'item',
  );


  /**
   * This will replace the value in the
   * attributes with the mapped term id
   */
  private $singleTermId = array(
    'vtwoocatalog' => 'terms___menu',
  );

  /**
   * This will replace the value in the
   * attributes with the mapped user id
   */
  private $singleUserId = array(
    'vagentsattributes' => 'userid',
    'vagentsfields' => 'userid',
  );



  /**
   * This will replace the attributes value
   * that has multiple post id separated by
   * commas with the new mapped post id value
   */
  private $multiplePostByComma = array(
    'vc_gallery' => 'images',
    'vc_images_carousel' => 'images',
    'playlist' => 'ids',
    'gallery' => 'ids',
    'vtwoocatalog' => array(
      'query___post__in',
      'query___post__not_in',
    ),
  );


  /**
   * This will replace the attributes value
   * that has multiple term id separated by
   * commas with the new mapped term id value
   */
  private $multipleTermByComma = array(
    'vpropertysearch' => 'types',
    'vtwoocatalog' => 'query___terms',
  );


  /**
   * This will replace the attributes value
   * that has multiple user id separated by
   * commas with the new mapped user id value
   */
  private $multipleUserByComma = array(
    'vagentlisting' => 'user',
  );

  private $dbUpdate;


  /**
   * Overriding parent method to use Post config
   * @see VTCore_Demo_Models_Processor::loadData()
   */
  public function loadData() {

    $className = $this->prefix . 'Posts';

    if (class_exists($className, true)) {
      $this->config = new $className();
      return true;
    }

    return false;
  }



  /**
   * Importing the post meta data to the database.
   *
   * @see VTCore_Demo_Models_Processor::importData()
   */
  public function importData() {

    // Stop processing if this database is marked
    // as imported previously.
    if ($this->checkImportStatus()) {
      return;
    }

    foreach ($this->config->getData() as $pid => $oldPost) {

      if ($this->checkMap('shortcodes', 'ID', $pid)
          || $oldPost['post_type'] == 'attachment'
          || $oldPost['post_type'] == 'navigation') {
        continue;
      }

      $newPostID = $this->getMappedValue('posts', 'ID', $pid);
      $newPost = get_post($newPostID, ARRAY_A);

      $this->dbUpdate = true;

      // Process single post id replacement
      foreach ($this->singlePostId as $tag => $atts) {
        $atts = (array) $atts;
        foreach ($atts as $att) {
          $pattern = '/\[(' . $tag . ')\b(.*?)(?:(' . $att . '="[d]"))?\]/';
          preg_match_all($pattern, $newPost['post_content'], $matches);

          foreach ($matches[0] as $original) {
            $subpattern = '/' . $att . '=\s*"([^"]*)"/';
            preg_match_all($subpattern, $original, $values);

            foreach ($values[1] as $value) {
              $replacementID = $this->getMappedValue('posts', 'ID', $value);
              $replacement = str_replace($att . '="' . $value . '"', $att . '="' . $replacementID . '"', $original);
              $newPost['post_content'] = str_replace($original, $replacement, $newPost['post_content']);
            }
          }
        }
      }


      // Process single term id replacement
      foreach ($this->singleTermId as $tag => $atts) {
        $atts = (array) $atts;
        foreach ($atts as $att) {
          $pattern = '/\[(' . $tag . ')\b(.*?)(?:(' . $att . '="[d]"))?\]/';
          preg_match_all($pattern, $newPost['post_content'], $matches);

          foreach ($matches[0] as $original) {
            $subpattern = '/' . $att . '=\s*"([^"]*)"/';
            preg_match_all($subpattern, $original, $values);

            foreach ($values[1] as $value) {
              $replacementID = $this->getMappedValue('terms', 'ID', $value);
              $replacement = str_replace($att . '="' . $value . '"', $att . '="' . $replacementID . '"', $original);
              $newPost['post_content'] = str_replace($original, $replacement, $newPost['post_content']);
            }
          }
        }
      }


      // Process singleuser id replacement
      foreach ($this->singleUserId as $tag => $atts) {
        $atts = (array) $atts;
        foreach ($atts as $att) {
          $pattern = '/\[(' . $tag . ')\b(.*?)(?:(' . $att . '="[d]"))?\]/';
          preg_match_all($pattern, $newPost['post_content'], $matches);

          foreach ($matches[0] as $original) {
            $subpattern = '/' . $att . '=\s*"([^"]*)"/';
            preg_match_all($subpattern, $original, $values);

            foreach ($values[1] as $value) {
              $replacementID = $this->getMappedValue('users', 'ID', $value);
              $replacement = str_replace($att . '="' . $value . '"', $att . '="' . $replacementID . '"', $original);
              $newPost['post_content'] = str_replace($original, $replacement, $newPost['post_content']);
            }
          }
        }
      }


      // Process multiple attachment separated with comma
      foreach ($this->multiplePostByComma as $tag => $atts) {
        $atts = (array) $atts;
        foreach ($atts as $att) {
          $pattern = '/\[(' . $tag . ')\b(.*?)(?:(' . $att . '="[d,]"))?\]/';
          preg_match_all($pattern, $newPost['post_content'], $shortcodes);

          foreach ($shortcodes[0] as $shortcode) {
            $subpattern = '/' . $att . '=\s*"([^"]*)"/';
            preg_match_all($subpattern, $shortcode, $matches);

            foreach ($matches[1] as $match) {
              $replacement = explode(',', $match);

              foreach ($replacement as $key => $value) {
                $replacement[$key] = $this->getMappedValue('posts', 'ID', trim($value));
              }

              $replacement = $att . '="' . implode(',', $replacement) . '"';
              $newcode = str_replace($att . '="' . $match . '"', $replacement, $shortcode);
              $newPost['post_content'] = str_replace($shortcode, $newcode, $newPost['post_content']);

            }
          }
        }
      }


      // Process multiple attachment separated with comma
      foreach ($this->multipleTermByComma as $tag => $atts) {
        $atts = (array) $atts;
        foreach($atts as $att) {
          $pattern = '/\[(' . $tag . ')\b(.*?)(?:(' . $att . '="[d,]"))?\]/';
          preg_match_all($pattern, $newPost['post_content'], $shortcodes);

          foreach ($shortcodes[0] as $shortcode) {
            $subpattern = '/' . $att . '=\s*"([^"]*)"/';
            preg_match_all($subpattern, $shortcode, $matches);

            foreach ($matches[1] as $match) {
              $replacement = explode(',', $match);
              foreach ($replacement as $key => $value) {
                $replacement[$key] = $this->getMappedValue('terms', 'ID', trim($value));
              }
              $replacement = $att . '="' . implode(',', $replacement) . '"';
              $newcode = str_replace($att . '="' . $match . '"', $replacement, $shortcode);
              $newPost['post_content'] = str_replace($shortcode, $newcode, $newPost['post_content']);
            }
          }
        }
      }

      // Process multiple user separated with comma
      foreach ($this->multipleUserByComma as $tag => $atts) {
        $atts = (array) $atts;
        foreach ($atts as $att) {
          $pattern = '/\[(' . $tag . ')\b(.*?)(?:(' . $att . '="[d,]"))?\]/';
          preg_match_all($pattern, $newPost['post_content'], $shortcodes);

          foreach ($shortcodes[0] as $shortcode) {
            $subpattern = '/' . $att . '=\s*"([^"]*)"/';
            preg_match_all($subpattern, $shortcode, $matches);

            foreach ($matches[1] as $match) {
              $replacement = explode(',', $match);

              $doReplacement = true;
              foreach ($replacement as $key => $value) {
                $replacement[$key] = $this->getMappedValue('users', 'ID', trim($value));
              }
              $replacement = $att . '="' . implode(',', $replacement) . '"';
              $newcode = str_replace($att . '="' . $match . '"', $replacement, $shortcode);
              $newPost['post_content'] = str_replace($shortcode, $newcode, $newPost['post_content']);
            }
          }
        }
      }

      // Update content
      global $wpdb;
      $result = $wpdb->query($wpdb->prepare("UPDATE {$wpdb->posts} SET post_content = %s WHERE ID=%d", $newPost['post_content'], $newPostID));

      if ($result === false) {
        $this->addLog(sprintf(__('Failed to update Post %s shortcodes', 'victheme_demo'), $newPost['post_title']), 'text-danger');
        $this->reportFailed();
      }
      else {
        $this->addLog(sprintf(__('Post %s shortcodes updated', 'victheme_demo'), $newPost['post_title']), 'text-success');
        $this->setMap('ID', $pid, TRUE);
      }
    }

    if ($this->maybeClose()) {
      $this->markImportedStatus();
    }
  }




  /**
   * The actual metadata removal will be invoked when
   * the post is deleted. Thus this function will only
   * remove the import marking and mapping.
   *
   * @see VTCore_Demo_Models_Processor::removeData()
   */
  public function removeData() {
    $this->removeMap('shortcodes');
    $this->removeImportedStatus();
  }

}