<?php
/**
 * Class VTCore_Demo_Element_Logger
 *
 * Class for building the import log panel
 */
class VTCore_Demo_Element_Logger
extends VTCore_Bootstrap_Element_BsPanel {

  public function buildElement() {

    parent::buildElement();

    $this
      ->setChildrenPointer('heading')
      ->BsElement(array(
        'type' => 'div',
        'attributes' => array(
          'class' => array(
            'btn-group',
            'pull-right',
            'hidden-no-js',
          ),
        ),
      ))
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Form_BsButton(array(
        'text' => __('Clear', 'victheme_demo'),
        'mode' => 'warning',
        'size' => 'xs',
        'data' => array(
          'filter' => 'clear',
          'toggle' => 'import-filter',
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsButton(array(
        'text' => __('All', 'victheme_demo'),
        'mode' => 'info',
        'size' => 'xs',
        'data' => array(
          'filter' => 'all',
          'toggle' => 'import-filter',
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsButton(array(
        'text' => __('Errors', 'victheme_demo'),
        'size' => 'xs',
        'mode' => 'danger',
        'data' => array(
          'filter' => 'errors',
          'toggle' => 'import-filter',
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Form_BsButton(array(
        'text' => __('Success', 'victheme_demo'),
        'mode' => 'success',
        'size' => 'xs',
        'data' => array(
          'filter' => 'success',
          'toggle' => 'import-filter',
        ),
      )));
  }

}