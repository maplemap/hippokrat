<?php
/**
 * Class VTCore_Demo_Element_Preview
 *
 * Class for building the preview box
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Demo_Element_Preview
extends VTCore_Bootstrap_Element_BsElement {

  protected $context = array(
    'type' => 'div',
    'id' => 'preview',
    'data' => array(
      'toggle-preview' => true,
      'target' => '#vtcore-demo-selector-modal',
      'ajax-group' => 'demo',
    ),
    'attributes' => array(
      'class' => array(
        'vtcore-selected-preview'
      ),
    ),
    'config' => false,
  );



  public function buildElement() {

    parent::buildElement();

    // Build the default preview
    // Used for "locked" site.
    if ($this->getContext('config')) {

      if ($this->getContext('config')->get('thumbnail')) {
        $this->addChildren(new VTCore_Wordpress_Element_WpImage(array(
          'type' => 'img',
          'attachment_id' => false,
          'attributes' => array(
            'src' => $this->getContext('config')->get('thumbnail'),
            'class' => array(
              'vtcore-demo-image',
            )
          ),
        )));
      }

      if ($this->getContext('config')->get('title')) {
        $this->addChildren(new VTCore_Html_Element(array(
          'type' => 'div',
          'text' => $this->getContext('config')->get('title'),
          'attributes' => array(
            'class' => 'vtcore-demo-title',
          ),
        )));

      }

      if ($this->getContext('config')->get('description')) {

        $this->addChildren(new VTCore_Html_Element(array(
          'type' => 'div',
          'text' => $this->getContext('config')->get('description'),
          'attributes' => array(
            'class' => 'vtcore-demo-description',
          ),
        )));
      }
    }
  }

}