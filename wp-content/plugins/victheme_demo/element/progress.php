<?php
/**
 * Class VTCore_Demo_Element_Progress
 *
 * Class for building the progress bar Panels
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Demo_Element_Progress
extends VTCore_Bootstrap_Element_BsPanel {

  public function buildElement() {

    parent::buildElement();

    $this
      ->addContent(new VTCore_Bootstrap_Element_BsProgressBar(array(
        'mintext' => __('Start', 'victheme_demo'),
        'maxtext' => __('Finished', 'victheme_demo'),
        'stripped' => true,
        'animated' => false,
        'attributes' => array(
          'id' => 'import-bar',
        ),
        'contents' => array(
          array(
            'text' => ' ',
            'width' => 0,
          ),
        ),
      )));
  }

}