<?php
/**
 * Class VTCore_Demo_Element_Modal
 *
 * Class for building modal or single site import
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Demo_Element_Modal
extends VTCore_Bootstrap_Form_Base {

  public function buildElement() {

    parent::buildElement();

    $this->addContext('processors', new VTCore_Demo_Entity_Registry());
    $this->addContext('activePlugins', get_option('active_plugins', array()));

    if (count($this->getContext('processors')->get('sites')) == 1) {
      $this->buildSingleSite();
    }
    else {
      $this->buildMultiSite();
    }

  }


  protected function buildSingleSite() {
    $this->addChildren(new VTCore_Form_Hidden(array(
      'attributes' => array(
        'name' => 'demo_site',
        'value' => $this->getContext('sites'),
      ),
    )));
  }


  private function checkPluginActive($plugin) {

    $active = false;
    foreach ($this->getContext('activePlugins') as $pluginPath) {
      if (strpos($pluginPath, $plugin) !== false) {
        $active = true;
      }
    }

    return $active;
  }



  protected function buildSiteOptions() {

    $this->options = array(
      FALSE => array(
        'text' => __('None', 'victheme_demo'),
        'attributes' => array(
          'value' => FALSE,
        ),
    ));

    foreach ($this->getContext('processors')->get('config') as $site => $config) {

      $this->options[$site] = array(
        'text' => $config->get('title'),
        'attributes' => array(
          'value' => $site,
        ),
      );

      if ($config->get('dependencies')) {

        $dependencies = array();

        foreach ($config->get('dependencies') as $plugin) {

          if ($this->checkPluginActive($plugin)) {
            continue;
          }

          $dependencies[] = 'plugin##' . $plugin;
        }

        $this->options[$site]['data']['dependencies'] = $dependencies;

        unset($config);
      }
    }

    return $this;

  }


  protected function buildMultiSite() {

    VTCore_Wordpress_Utility::loadAsset('jquery-isotope');

    $this->buildSiteOptions();

    $this
      ->addChildren(new VTCore_Bootstrap_Form_BsSelect(array(
        'text' => !$this->getContext('processors')->get('lock')->lock() ? __('Choose Demo Import Source', 'victheme_demo') : __('Active Demo Source', 'victheme_demo'),
        'name' => 'demo_site',
        'required' => true,
        'attributes' => array(
          'class' => array(
            'vtcore-demo-site-selector',
          ),
        ),
        'data' => array(
          'disable' => true,
          'ajax-group' => 'demo',
        ),
        'value' => $this->getContext('processors')->get('lock')->lock(),
        'options' => $this->options,
      )))
      ->addChildren(new VTCore_Demo_Element_Preview(array(
        'data' => array(
          'toggle' => !$this->getContext('processors')->get('lock')->lock() ? 'modal' : 'disabled',
        ),
        'config' => $this->getContext('processors')->get('active'),
      )))
      ->addChildren(new VTCore_Bootstrap_Element_BsModal(array(
        'text' => __('Select site source', 'victheme_demo'),
        'size' => 'lg',
        'attributes' => array(
          'id' => 'vtcore-demo-selector-modal',
        ),
      )));


    // Build the modal Content
    $this
      ->lastChild()
      ->addContent(new VTCore_Bootstrap_Grid_BsRow(array(
      'attributes' => array(
        'class' => array(
          'js-isotope',
        ),
      ),
      'data' => array(
        'isotope-options' => array(
          'layoutMode' => 'fitRows',
          'fitRows' => array(
            'equalheight' => true,
          ),
        ),
      ),
    )));

    foreach ($this->getContext('processors')->get('config') as $site => $config) {

      $this->lastChild()
        ->setChildrenPointer('content')
        ->lastChild()
        ->addChildren(new VTCore_Demo_Element_Site(array(
          'id' => $site,
          'attributes' => array(
            'class' => array(
              'vtcore-demo-items',
            ),
          ),
          'config' => $config,
          'data' => array(
            'toggle-selector' => TRUE,
            'toggle-site' => $site,
          ),
        )));
    }

    // Build the modal Row
    $this->lastChild()
      ->addFooter(new VTCore_Bootstrap_Form_BsButton(array(
        'text' => __('Select', 'victheme_demo'),
        'data' => array(
          'dismiss' => 'modal',
        ),
      )));

  }
}