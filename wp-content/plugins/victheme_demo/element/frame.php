<?php
/**
 * Class for building the demo apps frame
 * This is separated for easier maintenance.
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Demo_Element_Frame
extends VTCore_Bootstrap_Element_BsElement {

  public function buildElement() {

    // First row for building the progress bar
    $this
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Grid_BsColumn(array(
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 12,
            'small' => 12,
            'large' => 12,
          ),
        ),
      )))
      ->lastChild()
      ->addChildren(new VTCore_Demo_Element_Progress(array(
        'text' => __('Importing Progress', 'victheme_demo'),
        'attributes' => array(
          'class' => array(
            'hidden-no-js',
            'controller-progress'
          )
        ),
      )));


    // Second Row for the import log and buttons
    $this
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()


      // First Column for buttons
      ->addChildren(new VTCore_Bootstrap_Grid_BsColumn(array(
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 4,
            'small' => 4,
            'large' => 4,
          ),
        ),
      )))
      ->lastChild()
      ->addChildren(new VTCore_Demo_Element_Import(array(
        'text' => __('Import Demo Content', 'victheme_demo'),
        'attributes' => array(
          'class' => array(
            'controller-panels'
          )
        ),
      )))
      ->addChildren(new VTCore_Demo_Element_Remove(array(
        'text' => __('Remove Demo Content', 'victheme_demo'),
        'attributes' => array(
          'class' => array(
            'controller-panels'
          )
        ),
      )))
      ->getParent()

      // Second Column for Logger
      ->addChildren(new VTCore_Bootstrap_Grid_BsColumn(array(
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 8,
            'small' => 8,
            'large' => 8,
          ),
        ),
      )))
      ->lastChild()
      ->addChildren(new VTCore_Demo_Element_Logger(array(
        'text' => __('Import Log', 'victheme_demo'),
        'attributes' => array(
          'id' => 'log-panel',
          'class' => array(
            'log-panels'
          ),
        ),
        'heading_elements' => array(
          'attributes' => array(
            'class' => array(
              'clearfix',
            ),
          ),
        ),
      )));

  }

}