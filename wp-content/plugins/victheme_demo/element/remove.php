<?php
/**
 * Class VTCore_Demo_Element_Remove
 *
 * Class for building the remove button panels
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Demo_Element_Remove
extends VTCore_Bootstrap_Element_BsPanel {

  public function buildElement() {

    parent::buildElement();

    $this->addContext('processors', new VTCore_Demo_Entity_Registry());

    $this
      ->addContent(new VTCore_Bootstrap_Form_BsDescription(array(
        'text' => __('This will remove the previously imported demo content from database.', 'victheme_demo'),
      )))
      ->addContent(new VTCore_Bootstrap_Form_BsSubmit(array(
        'attributes' => array(
          'name' => 'removeDemo',
          'value' => __('Remove Demo Content', 'victheme_demo'),
          'class' => 'btn-ajax',
        ),
        'mode' => 'danger',
        'confirmation' => true,
        'title' => __('Are you sure?', 'victheme_demo'),
        'ok' => __('Yes', 'victheme_demo'),
        'cancel' => __('No', 'victheme_demo'),
        'data' => array(
          'ajax-mode' => 'post',
          'ajax-target' => '#theme-configuration-form',
          'ajax-loading-text' => __('Removing Data', 'victheme_demo'),
          'ajax-object' => 'VTCore_Demo_Entity_Ajax',
          'ajax-action' => 'vtcore_ajax_framework',
          'ajax-value' => 'removeDemo',
          'ajax-queue' => $this->getContext('processors')->get('processors'),
          'ajax-group' => 'demo',
          'confirmation-target' => '#import-bar',
          'ajax-retry' => 10,
        ),
      )));
  }

}