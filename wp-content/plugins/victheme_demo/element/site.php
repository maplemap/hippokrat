<?php
/**
 * Class VTCore_Demo_Element_Site
 *
 * Class for building the modal items
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Demo_Element_Site
extends VTCore_Bootstrap_Grid_BsColumn {

  protected $context = array(
    'type' => 'div',
    'attributes' => array(
      'class' => array(
        'vtcore-demo-items',
      ),
    ),
    'grids' => array(
      'columns' => array(
        'mobile' => '6',
        'tablet' => '6',
        'small' => '4',
        'large' => '4',
      ),
    ),
    'config' => false,
  );



  public function buildElement() {

    parent::buildElement();

    if ($this->getContext('config')) {
      if ($this->getContext('config')->get('thumbnail')) {
        $this->addChildren(new VTCore_Wordpress_Element_WpImage(array(
          'type' => 'img',
          'attachment_id' => FALSE,
          'attributes' => array(
            'src' => $this->getContext('config')->get('thumbnail'),
            'class' => array(
              'vtcore-demo-image',
            )
          ),
        )));
      }

      if ($this->getContext('config')->get('title')) {
        $this->addChildren(new VTCore_Html_Element(array(
          'type' => 'div',
          'text' => $this->getContext('config')->get('title'),
          'attributes' => array(
            'class' => 'vtcore-demo-title',
          ),
        )));

      }

      if ($this->getContext('config')->get('description')) {
        $this->addChildren(new VTCore_Html_Element(array(
          'type' => 'div',
          'text' => $this->getContext('config')->get('description'),
          'attributes' => array(
            'class' => 'vtcore-demo-description',
          ),
        )));
      }
    }
  }

}