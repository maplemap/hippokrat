<?php
/**
 * Class VTCore_Demo_Element_Import
 *
 * Class for building the import panels
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Demo_Element_Import
extends VTCore_Bootstrap_Element_BsPanel {

  public function buildElement() {

    parent::buildElement();

    $this->addContext('processors', new VTCore_Demo_Entity_Registry());

    $this
      ->addContent(new VTCore_Bootstrap_Form_BsDescription(array(
        'text' => __('This will import the demo content automatically and may overwrite stored
                      data in the database. Only use this on a brand new installation to avoid loss of
                      data', 'victheme_demo'),
      )))
      ->addContent(new VTCore_Wordpress_Form_WpNonce(array(
        'action' => 'vtcore-ajax-nonce-admin',
      )))
      ->addContent(new VTCore_Demo_Element_Modal())
      ->addContent(new VTCore_Bootstrap_Form_BsSubmit(array(
        'attributes' => array(
          'name' => 'importDemo',
          'value' => __('Import Demo Content', 'victheme_demo'),
          'class' => array(
            'btn-ajax',
            'btn-import',
          ),
        ),
        'mode' => 'primary',
        'confirmation' => true,
        'title' => __('Are you sure?', 'victheme_demo'),
        'ok' => __('Yes', 'victheme_demo'),
        'cancel' => __('No', 'victheme_demo'),
        'data' => array(
          'ajax-mode' => 'post',
          'ajax-target' => '#theme-configuration-form',
          'ajax-loading-text' => __('Importing Data', 'victheme_demo'),
          'ajax-object' => 'VTCore_Demo_Entity_Ajax',
          'ajax-action' => 'vtcore_ajax_framework',
          'ajax-value' => 'importDemo',
          'ajax-queue' => $this->getContext('processors')->get('processors'),
          'ajax-completed' => get_option('vtcore_demo_status_' . BASE_THEME, false),
          'ajax-group' => 'demo',
          'confirmation-target' => '#import-bar',
          'ajax-retry' => 10,
        ),
      )));
  }

}