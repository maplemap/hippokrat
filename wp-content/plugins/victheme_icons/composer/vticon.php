<?php
/**
 * Registering slickcarousel shortcode to visualcomposer
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Icons_Composer_VTIcon
extends VTCore_Wordpress_Models_VC {

  
  public function registerVC() {

    $options = array(
      'name' => __('VTIcon', 'victheme_icon'),
      'description' => __('Custom Icon from VTCore Icon Library', 'victheme_icon'),
      'base' => 'vticon',
      'icon' => 'icon-vticon',
      'category' => __( 'Content', 'js_composer' ),
      'params' => array()
    );

    $options['params'][] = array(
      'type' => 'vt_iconset_form',
      'param_name' => 'iconset',
      'value' => '',
      'group' => __('Icon', 'victheme_icon'),
    );

    $options['params'][] = array(
      'type' => 'textfield',
      'heading' => __('CSS ID', 'victheme_icon'),
      'description' => __('Valid CSS ID for the main wrapper element.', 'victheme_icon'),
      'param_name' => 'id',
      'admin_label' => true,
    );

    $options['params'][] = array(
      'type' => 'textfield',
      'heading' => __('CSS Class', 'victheme_icon'),
      'description' => __('Valid CSS Class for the main wrapper element.', 'victheme_icon'),
      'param_name' => 'class',
      'admin_label' => true,
    );

    $options['params'][] = array(
      'type' => 'dropdown',
      'heading' => __( 'CSS Animation', 'victheme_icon'),
      'param_name' => 'css_animation',
      'admin_label' => true,
      'value' => array(
        __('No', 'victheme_icon') => '',
        __('Top to bottom', 'victheme_icon') => 'top-to-bottom',
        __('Bottom to top', 'victheme_icon') => 'bottom-to-top',
        __('Left to right', 'victheme_icon') => 'left-to-right',
        __('Right to left', 'victheme_icon') => 'right-to-left',
        __('Appear from center', 'victheme_icon') => "appear"
      ),
      'description' => __('Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'victheme_icon'),
    );


    // CSS Editor
    $options['params'][] = array(
      'type' => 'css_editor',
      'heading' => __('Css', 'victheme_icon'),
      'param_name' => 'css',
      'group' => __('Design options', 'victheme_icon'),
    );

    return $options;
  }
}


class WPBakeryShortCode_VTIcon extends WPBakeryShortCode {}