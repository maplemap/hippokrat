<?php

/**
 * Page callback class for building the Victheme Icons
 * icon configuration page.
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Icons_Pages_Config
  extends VTCore_Wordpress_Models_Page {


  protected $help_panel;
  protected $config;

  protected function register() {
    $this->headerText = __('Icons Configuration', 'victheme_core');
    $this->headerIcon = 'dashboard';
    $this->saveKey = 'vtcore-save';
    $this->resetKey = 'vtcore-reset';
  }

  protected function loadAssets() {
    wp_deregister_script('heartbeat');
    VTCore_Wordpress_Utility::loadAsset('wp-bootstrap');
    VTCore_Wordpress_Utility::loadAsset('wp-page');
    VTCore_Wordpress_Utility::loadAsset('bootstrap-confirmation');
    VTCore_Wordpress_Utility::loadAsset('vticon-config');
  }

  public function renderAjax($post) {
    return FALSE;
  }

  protected function save() {

    if (empty($this->errors)
      && isset($_POST['icons'])
      && isset($_POST['icons'])
    ) {

      $data = wp_unslash($_POST['icons']);

      $this->messages->setNotice(__('Configuration Saved.', 'victheme_icons'));
      update_option('vtcore_icons_config', $data);
    }

  }

  protected function reset() {
    $this->messages->setNotice(__('Configuration resetted to default state, all icons is enabled again.', 'victheme_icons'));
    delete_option('vtcore_icons_config');
    unset($_POST['icons']);
  }


  protected function buildForm() {

    $this->config = new VTCore_Wordpress_Objects_Array(get_option('vtcore_icons_config', array()));

    $this->form = new VTCore_Bootstrap_Form_BsInstance(array(
      'attributes' => array(
        'id' => 'vtcore-icon-configuration-form',
        'method' => 'post',
        'action' => $_SERVER['REQUEST_URI'],
        'class' => array('container-fluid', 'vtcore-configuration-form-skins'),
        'autocomplete' => 'off',
      ),
    ));

    $this->form
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow())
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Grid_BsColumn(array(
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 8,
            'small' => 9,
            'large' => 9,
          ),
        ),
      )))
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Element_BsPanel(array(
        'text' => __('Icon Assets Library', 'victheme_icons'),
      )))
      ->lastChild()
      ->addContent(new VTCore_Html_Table(array(
        'headers' => array(
          new VTCore_Form_Checkbox(array('attributes' => array('class' => array('check-all')))),
          __('Icon Family', 'victheme_icons'),
          __('Total Icon', 'victheme_icons'),
        ),
        'rows' => $this->buildRows(),
        'attributes' => array(
          'class' => array(
            'table-responsive',
            'table-hover',
            'table-stripped',
          ),
        ),
      )))
      ->addContent(new VTCore_Bootstrap_Form_BsSubmit(array(
        'attributes' => array(
          'name' => $this->saveKey,
          'value' => __('Save', 'victheme_core'),
        ),
      )))
      ->addContent(new VTCore_Bootstrap_Form_BsSubmit(array(
        'attributes' => array(
          'name' => $this->resetKey,
          'value' => __('Reset', 'victheme_core'),
        ),
        'mode' => 'danger',
        'confirmation' => TRUE,
        'title' => __('Are you sure?', 'victheme_core'),
        'ok' => __('Yes', 'victheme_core'),
        'cancel' => __('No', 'victheme_core'),
      )));

    $this->help_panel = $this->form
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Grid_BsColumn(array(
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 4,
            'small' => 3,
            'large' => 3,
          ),
        ),
      )))
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Element_BsModal(array(
        //'show' => true,
        'attributes' => array(
          'id' => 'vtcore-zeus-help-modal',
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Element_BsPanel(array(
        'text' => __('Quick Help', 'medikal'),
        'attributes' => array(
          'class' => array(
            'quick-help-panels'
          )
        ),
      )))
      ->lastChild()
      ->addContent(new VTCore_Bootstrap_Element_BsElement(array(
        'type' => 'div',
      )))
      ->lastChild()
      ->addChildren(new VTCore_Fontawesome_faIcon(array(
        'position' => 'pull-left',
        'icon' => 'question-circle',
      )))
      ->addChildren(new VTCore_Html_Element(array(
        'type' => 'p',
        'text' => __('You can use this page to enable or disable an icon family from the core database, disabling unused icon family will improve the icon forms and element loading time.', 'victheme_icons'),
      )))
      ->getParent()
      ->addContent(new VTCore_Bootstrap_Element_BsElement(array(
        'type' => 'div',
      )))
      ->lastChild()
      ->addChildren(new VTCore_Fontawesome_faIcon(array(
        'position' => 'pull-left',
        'icon' => 'question-circle',
      )))
      ->addChildren(new VTCore_Html_Element(array(
        'type' => 'p',
        'raw' => true,
        'text' => __('<strong><em>FontAwesome</em></strong> and <strong><em>GlyphIcon</em></strong> icon family is used by the VicTheme core thus it cannot be disabled.', 'victheme_icons'),
      )));

  }


  /**
   * Helper function for building table rows.
   */
  protected function buildRows() {

    $rows = array();
    $total = 0;
    $enabled = 0;

    // Icon counter
    $icon = new VTCore_Wordpress_Data_Icons_Library();
    foreach ($icon->extract() as $key => $family) {

      $object = new VTCore_Wordpress_Objects_Array($family);
      $status = $this->config->get($key) !== NULL ? (boolean) $this->config->get($key) : TRUE;

      // Cannot remove fontawesome and glyphicon
      if (in_array($key, array('fontawesome', 'glyphicon'))) {
        $form = new VTCore_Html_Element();
        $form
          ->addChildren(new VTCore_Form_Hidden(array(
            'attributes' => array(
              'name' => 'icons[' . $key . ']',
              'value' => true,
            ),
          )))
          ->addChildren(new VTCore_Form_Checkbox(array(
            'attributes' => array(
              'checked' => true,
              'disabled' => true,
            ),
          )));

        $rows[$key][] = $form;
      }
      else {
        $rows[$key][] = new VTCore_Bootstrap_Form_BsCheckbox(array(
          'switch' => FALSE,
          'name' => 'icons[' . $key . ']',
          'checked' => $status,
        ));
      }

      $rows[$key][] = $object->get('text');
      $count = count(array_filter(array_unique(explode(',', str_replace(array(
        ' ',
        "\n"
      ), array('', ''), $object->get('iconset'))))));
      $rows[$key][] = number_format($count);
      $total += (int) $count;

      if ($status) {
        $enabled += (int) $count;
      }

    }

    $rows['total'][] = '';
    $rows['total'][] = '<strong>' . __('Total Icons Available', 'victheme_icons') . '</strong>';
    $rows['total'][] = '<strong>' . number_format($total) . '</strong>';

    $rows['enabled'][] = '';
    $rows['enabled'][] = '<strong>' . __('Total Icons Enabled', 'victheme_icons') . '</strong>';
    $rows['enabled'][] = '<strong>' . number_format($enabled) . '</strong>';

    return $rows;
  }
}