<?php
/**
 * Page callback class for building the Victheme Icons
 * Shortcode Generator page.
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Icons_Pages_Generator
extends VTCore_Wordpress_Models_Page {


  protected $help_panel;

  protected function register() {
    $this->headerText = __('Shortcode Generator', 'victheme_core');
    $this->headerIcon = 'paragraph';
  }

  protected function loadAssets() {
    wp_deregister_script('heartbeat');
    VTCore_Wordpress_Utility::loadAsset('wp-bootstrap');
    VTCore_Wordpress_Utility::loadAsset('wp-page');
    VTCore_Wordpress_Utility::loadAsset('wp-icons');
    VTCore_Wordpress_Utility::loadAsset('vticon-generator');
  }

  public function renderAjax($post) {
    return false;
  }

  protected function save() {
    return $this;
  }

  protected function reset() {
    return $this;
  }


  protected function buildForm() {

    $this->form = new VTCore_Bootstrap_Form_BsInstance(array(
      'attributes' => array(
        'id' => 'vtcore-shortcode-generator-form',
        'method' => 'post',
        'action' => $_SERVER['REQUEST_URI'],
        'class' => array('container-fluid', 'vtcore-configuration-form-skins'),
        'autocomplete' => 'off',
      ),
    ));

    $this->form
      ->addChildren(new VTCore_Bootstrap_Grid_BsRow());

    $this->form
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Grid_BsColumn(array(
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 8,
            'small' => 9,
            'large' => 9,
          ),
        ),
      )))
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Element_BsPanel(array(
        'text' => __('Generated Shortcode', 'victheme_icons'),
      )))
      ->lastChild()
      ->addContent(new VTCore_Bootstrap_Element_BsElement(array(
        'type' => 'div',
        'text' => __('No Shortcode Generated', 'victheme_icons'),
        'data' => array(
          'icon-shortcode' => true,
        ),
        'attributes' => array(
          'class' => array(
            'icon-shortcode',
          ),
        ),
      )))
      ->getParent()
      ->addChildren(new VTCore_Bootstrap_Element_BsPanel(array(
        'text' => __('Icon Selector', 'victheme_icons'),
      )))
      ->lastChild()
      ->addContent(new VTCore_Wordpress_Form_WpIconSet(array(
        'text' => __('Preview', 'victheme_icons'),
        'name' => 'generator',
      )))
      ->addContent(new VTCore_Bootstrap_Form_BsButton(array(
        'text' => __('Generate', 'victheme_icons'),
        'data' => array(
          'icon-generator' => true,
        ),
        'attributes' => array(
          'name' => $this->saveKey,
        ),
      )));

    $this->help_panel = $this->form
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Grid_BsColumn(array(
        'grids' => array(
          'columns' => array(
            'mobile' => 12,
            'tablet' => 4,
            'small' => 3,
            'large' => 3,
          ),
        ),
      )))
      ->lastChild()
      ->addChildren(new VTCore_Bootstrap_Element_BsModal(array(
        //'show' => true,
        'attributes' => array(
          'id' => 'vtcore-zeus-help-modal',
        ),
      )))
      ->addChildren(new VTCore_Bootstrap_Element_BsPanel(array(
        'text' => __('Quick Help', 'medikal'),
        'attributes' => array(
          'class' => array(
            'quick-help-panels'
          )
        ),
      )))
      ->lastChild()
      ->addContent(new VTCore_Bootstrap_Element_BsElement(array(
        'type' => 'div',
      )))
      ->lastChild()
      ->addChildren(new VTCore_Fontawesome_faIcon(array(
        'position' => 'pull-left',
        'icon' => 'question-circle',
      )))
      ->addChildren(new VTCore_Html_Element(array(
        'type' => 'p',
        'text' => __('You can use this page to generate valid WordPress Shortcodes for generating the VTIcons', 'victheme_icons'),
      )))
      ->getParent()
      ->addContent(new VTCore_Bootstrap_Element_BsElement(array(
        'type' => 'div',
      )))
      ->lastChild()
      ->addChildren(new VTCore_Fontawesome_faIcon(array(
        'position' => 'pull-left',
        'icon' => 'question-circle',
      )))
      ->addChildren(new VTCore_Html_Element(array(
        'type' => 'p',
        'text' => __('Use the Icon Selector Panel to build your icons and click on the generate button when you are ready to generate the shortcode', 'victheme_icons'),
      )))
      ->getParent()
      ->addContent(new VTCore_Bootstrap_Element_BsElement(array(
        'type' => 'div',
      )))
      ->lastChild()
      ->addChildren(new VTCore_Fontawesome_faIcon(array(
        'position' => 'pull-left',
        'icon' => 'question-circle',
      )))
      ->addChildren(new VTCore_Html_Element(array(
        'type' => 'p',
        'text' => __('After the shortcode is generated, you can copy and paste the shortcode text to WordPress post content or any other place the support shortcode', 'victheme_icons'),
      )));

  }

}