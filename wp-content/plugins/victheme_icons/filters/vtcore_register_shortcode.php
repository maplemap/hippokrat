<?php
/**
 * Hooking into vtcore_register_shortcode filter
 * to register visualcandy related shortcodes
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Icons_Filters_VTCore__Register__Shortcode
extends VTCore_Wordpress_Models_Hook {

  public function hook($shortcodes = NULL) {

    $shortcodes[] = 'vticon';

    return $shortcodes;
  }
}