<?php
/**
 * Class for adding the medikal icon to the icon library
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Icons_Actions_VTCore__Wordpress__Alter__Icons__Status
extends VTCore_Wordpress_Models_Hook {

  public function hook($object = NULL) {
    $config = new VTCore_Wordpress_Objects_Array(get_option('vtcore_icons_config', array()));
    foreach ($config->extract() as $family => $status) {
      if ($status !== NULL) {
        $object->add($family . '.enabled',(boolean) $status);
      }
    }
  }
}