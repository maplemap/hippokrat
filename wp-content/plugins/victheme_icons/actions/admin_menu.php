<?php
/**
 * Link to Wordpress Action admin_menu for
 * registering Core Administration menus.
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Icons_Actions_Admin__Menu
extends VTCore_Wordpress_Models_Hook {

  public function hook() {

    // Icon Configuration
    add_menu_page(
      'Icon Configuration',
      'Icon',
      'manage_options',
      'vtcore-icons',
      array(new VTCore_Icons_Pages_Config(), 'buildPage'),
      'dashicons-editor-italic');

    // Shortcode Generator
    add_submenu_page(
      'vtcore-icons',
      'Shortcode Generator',
      'Shortcode Generator',
      'manage_options',
      'vtcore-icons-shortcode-generator',
      array(new VTCore_Icons_Pages_Generator(), 'buildPage')
    );

  }
}