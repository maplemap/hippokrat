<?php
/**
 * Hooking into Visual Composer Frontend iFrame
 * before it got initialized
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Icons_Actions_Vc__Load__IFrame__JsCss
extends VTCore_Wordpress_Models_Hook {

  public function hook() {
    VTCore_Wordpress_init::getFactory('assets')
      ->get('queues')
      ->add('vticon-visualcomposer', array('footer' => true))
      ->add('wp-icons-front', array('footer' => true))
      ->remove('vticon-visualcomposer.js.vticon-visualcomposer-js');

  }
}