<?php
/**
 * Hooking into wordpress widget_init action for
 * registering custom widgets.
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Icons_Actions_Widgets__Init
extends VTCore_Wordpress_Models_Hook {

  public function hook() {

    $widgets = array(
      'VTCore_Icons_Widgets_Simple',
    );

    foreach ($widgets as $widget) {
      $object = new $widget;
      $object->registerWidget();
    }
  }
}