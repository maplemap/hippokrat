<?php
/*
Plugin Name: VicTheme Icons
Plugin URI: http://victheme.com
Description: Plugin for providing font icons.
Author: jason.xie@victheme.com
Version: 1.1.2
Author URI: http://victheme.com
*/

define('VTCORE_ICONS_CORE_VERSION', '1.7.0');


// Load the translation as early as possible
load_plugin_textdomain('victheme_icons', false, 'victheme_icons/languages');

add_action('plugins_loaded', 'bootVicthemeIcons', 13);

function bootVicthemeIcons() {

  if (!(defined('VTCORE_VERSION') && version_compare(VTCORE_VERSION, VTCORE_ICONS_CORE_VERSION, '='))) {

    add_action('admin_notices', 'IconsMissingCoreNotice');

    function IconsMissingCoreNotice() {

      if (!defined('VTCORE_VERSION')) {
        $notice = __('VicTheme Icons depends on VicTheme Core Plugin which is not activated or missing, Please enable it first before VicTheme Icons can work properly.', 'victheme_icons');
      }
      elseif (version_compare(VTCORE_VERSION, VTCORE_ICONS_CORE_VERSION, '!=')) {
        $notice = sprintf(__('VicTheme Icons depends on VicTheme Core Plugin API version %s to operate properly.', 'victheme_icons'), VTCORE_ICONS_CORE_VERSION);
      }

      if (isset($notice)) {
        echo '<div class="error""><p>' . $notice . '</p></div>';
      }

    }

    return;
  }


  if (defined('WPB_VC_VERSION') && !version_compare(WPB_VC_VERSION, '4.7.0', '>=')) {
    add_action('admin_notices', 'VTCore_IconsVCTooLow');

    function VTCore_IconsVCTooLow() {
      echo
        '<div class="error""><p>' .

        __( 'VicTheme Icons requires Visual Composer Plugin version 4.7.0 and above before it can function properly.', 'victheme_icons') .

        '</p></div>';
    }
  }

  define('VTCORE_ICONS_LOADED', true);
  define('VTCORE_ICONS_BASE_DIR', dirname(__FILE__));

  // Booting autoloader
  $autoloader = new VTCore_Autoloader('VTCore_Icons', dirname(__FILE__));
  $autoloader->setRemovePath('vtcore' . DIRECTORY_SEPARATOR . 'icons' . DIRECTORY_SEPARATOR);
  $autoloader->register();

  // Registering assets
  VTCore_Wordpress_Init::getFactory('assets')
    ->get('library')
    ->detect(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets', plugins_url('', __FILE__) . '/assets');


  // Registering actions
  VTCore_Wordpress_Init::getFactory('actions')
    ->addPrefix('VTCore_Icons_Actions_')
    ->addHooks(array(
      'widgets_init',
      'admin_menu',
      'vc_backend_editor_enqueue_js_css',
      'vc_frontend_editor_enqueue_js_css',
      'vc_load_iframe_jscss',
      'vtcore_wordpress_alter_icons_library',
      'vtcore_wordpress_alter_icons_status',
    ))
    ->register();

  // Registering actions
  VTCore_Wordpress_Init::getFactory('filters')
    ->addPrefix('VTCore_Icons_Filters_')
    ->addHooks(array(
      'vtcore_register_shortcode_prefix',
      'vtcore_register_shortcode',
    ))
    ->register();

  // Register to visual composer via VTCore Visual Composer Factory
  if (VTCore_Wordpress_Init::getFactory('visualcomposer')) {
    VTCore_Wordpress_Init::getFactory('visualcomposer')
      ->mapShortcode(array(
        'VTCore_Icons_Composer_VTIcon',
      ));
  }


}