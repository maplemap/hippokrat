<?php
/**
 * Class for building the single icon widget widget.
 *
 * @author jason.xie@victheme.com
 */
class VTCore_Icons_Widgets_Simple
extends WP_Widget {

  private $defaults = array(
    'title' => '',
    'description' => '',
  );

  private $form;


  /**
   * Registering widget as WP_Widget requirement
   */
  public function __construct() {
    parent::__construct(
        'vtcore_icons_widgets_simple',
        'Single Icon',
        array('description' => 'Widget to display a single icon.')
    );
  }




  /**
   * Registering widget
   */
  public function registerWidget() {
    return register_widget('vtcore_icons_widgets_simple');
  }





  /**
   * Extending widget
   *
   * @see WP_Widget::widget()
   */
  public function widget($args, $instance) {

    $this->instance = VTCore_Utility::arrayMergeRecursiveDistinct($instance, $this->defaults);

    echo $args['before_widget'];

    $object = new VTCore_Wordpress_Element_WpIcon($this->instance);
    $object->render();

    $title = $this->instance['title'];

    if (!empty($title)) {
      echo $args['before_title'] . $title . $args['after_title'];
    }

    if (!empty($this->instance['description'])) {
      $object = new VTCore_Html_Element(array(
        'type' => 'div',
        'text' => $this->instance['description'],
        'attributes' => array(
          'class' => array(
            'vtcore-slick-description'
          ),
        ),
        'raw' => true,
      ));

      $object->render();
    }

    echo $args['after_widget'];
  }





  /**
   * Widget configuration form
   * @see WP_Widget::form()
   */
  public function form($instance) {

    VTCore_Wordpress_Utility::loadAsset('wp-bootstrap');
    VTCore_Wordpress_Utility::loadAsset('jquery-table-manager');
    VTCore_Wordpress_Utility::loadAsset('wp-widget');

    $this->instance = VTCore_Utility::arrayMergeRecursiveDistinct($instance, $this->defaults);
    $this->buildForm()->processForm()->processError(true, false)->render();

  }



  /**
   * Form need to be in separated function for validation
   * purposes. Rebuild the form and get the error to validate.
   */
  private function buildForm() {


    $widget = new VTCore_Bootstrap_Form_BsInstance(array(
      'type' => false,
    ));

    // Title
    $widget
      ->BsText(array(
        'text' => __('Title', 'victheme_icon'),
        'name' => $this->get_field_name('title'),
        'id' => $this->get_field_id('title'),
        'value' => $this->instance['title'],
      ))
      ->BsTextarea(array(
        'text' => __('Description', 'victheme_icon'),
        'name' => $this->get_field_name('description'),
        'id' => $this->get_field_id('description'),
        'value' => $this->instance['description'],
      ))
      ->addChildren(new VTCore_Wordpress_Form_WpIconSet(array(
        'prefix' => $this->get_field_id('icons-'),
        'ajax-id' => $this->get_field_id('icons'),
        'text' => __('Icon Preview', 'victheme_icon'),
        'name' => str_replace('[##removethistext##]', '', $this->get_field_name('##removethistext##')),
        'value' => $this->instance,
      )));


    return $widget;
  }

  /**
   * Widget update function.
   * @see WP_Widget::update()
   */
  public function update($new_instance, $old_instance) {

    $this->form = $this->buildForm()->processForm()->processError();
    $errors = $this->form->getErrors();
    if (empty($errors)) {
      return wp_unslash($new_instance);
    }

    return false;
  }
}