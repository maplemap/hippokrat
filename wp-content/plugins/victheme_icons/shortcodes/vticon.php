<?php
/**
 * Building Icon shortcodes that will utilizes
 * VTCore_Wordpress_Element_WpIcon object
 *
 * [vticon
 *   id="Unique CSS ID"
 *   family="the icon family as registered in the library"
 *   icon="the actual icon class"
 *   size="the icon size"
 *   rotate="degree value to rotate the icon element"
 *   spin="spin the icon indefinetely"
 *   flip="flip the icon"
 *   position="the icon element position relative to the wrapper"
 *   border___width="the border styling for the icon effect wrapper"
 *   border___style="the border styling for the icon effect wrapper"
 *   border___color="the border styling for the icon effect wrapper"
 *   border___radius="the border styling for the icon effect wrapper"
 *   shape="the special shape for the icon effect wrapper"
 *   color="the icon text color"
 *   background="the icon effect wrapper background color"
 *   lineheight="the lineheight for the icon element"
 *   padding="the inner padding for the effect wrapper"
 *   margin="the margin for the effect wrapper"
 * /]
 *
 * @author jason.xie@victheme.com
 *
 */
class VTCore_Icons_Shortcodes_VTIcon
extends VTCore_Wordpress_Models_Shortcodes
implements VTCore_Wordpress_Interfaces_Shortcodes {

  protected $processDottedNotation = true;

  /**
   * Extending parent method.
   * @see VTCore_Wordpress_Models_Shortcodes::processCustomRules()
   */
  protected function processCustomRules() {

    $object = new VTCore_Wordpress_Objects_Array($this->atts);

    if (function_exists('vc_shortcode_custom_css_class')) {
      if ($object->get('css')) {
        $object->add('attributes.class.vccss', vc_shortcode_custom_css_class($object->get('css')));
      }

      if ($object->get('font')) {
        $object->add('attributes.class.visualplus-font', vc_shortcode_custom_css_class($object->get('font')));
      }

      if ($object->get('background')) {
        $object->add('attributes.class.visualplus-background', vc_shortcode_custom_css_class($object->get('background')));
      }
    }

    if (isset($this->atts['iconset'])) {
      $args = wp_parse_args(html_entity_decode($this->atts['iconset']));

      if (isset($args['iconset'])) {
        $object->merge($args['iconset']);
      }
    }

    $this->atts = $object->extract();

  }


  public function buildObject() {

    // Clean the atts
    $object = new VTCore_Wordpress_Objects_Array($this->atts);
    $object->remove('attributes.class.background_style');
    $this->atts = $object->extract();
    $this->object = new VTCore_Wordpress_Element_WpIcon($this->atts);
  }
}