/**
 * Integrating VTIcons to visualcomposer front editor.
 * We need to force the WpIcon element to use inline
 * styling instead of injecting custom css into head.
 *
 * @author jason.xie@victheme.com
 */
(function ($) {

  $(window)
    .on('vc_ready', function() {

      // Registering our slick global methods.
      vc.vtcore.vticon = {
        setParams: function(model) {
          var params = model.get('params');
          params.inline_style = true;
          model.set('params', params);
          return this;
        },
        cleanParams: function(model) {
          var params = model.get('params');
          params.inline_style && delete params.inline_style;
          model.set('params', params);
        }
      }

      /**
       * Bind custom events to act on vc events.
       */
      vc.events
        .on('shortcodeView:ready:vticon', function(model) {
          vc.vtcore.vticon.cleanParams(model);
        })

      /**
       * Extending the inline shortcode view to piggy back our
       * before update events. remove this when vc
       * has actual before update events.
       */
      window.InlineShortcodeView_vticon = window.InlineShortcodeView.extend({
        beforeUpdate: function() {
          vc.vtcore.vticon.setParams(this.model);
        }
      });
    });

})(window.jQuery);