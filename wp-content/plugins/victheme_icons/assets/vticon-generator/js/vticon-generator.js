/**
 *
 * Javascript for generating Icon shortcode text
 *
 * @author jason.xie@victheme.com
 */
(function ($) {

  if (typeof VTCore == 'undefined') {
    VTCore = {};
  };

  console.log(VTCore);

  VTCore.VTIconsShortcodeGenerator = function(element) {
    this.$el = $(element);
    this.$target = this.$el.find('[data-icon-shortcode]');
    this.prefix = '[vticon ';
    this.suffix = ']';
    this.generated = '';
  }

  VTCore.VTIconsShortcodeGenerator.prototype = {
    generate: function() {
      var values = this.$el.serializeArray(),
          results = [],
          that = this;

      this.generated = '';

      $.each(values, function(key, entry){

        if (typeof entry.name != 'undefined'
            && typeof entry.value != 'undefined'
            && entry.value.length != 0
            && that.customRule(entry)) {

          var name = entry.name.replace('generator[', '').replace('][', '___').replace(']', '');

          if (name == 'spin' && entry.value == '1') {
            entry.value = 'true';
          }

          results.push(name + '="' + entry.value + '"');
        }

      });

      if (results.length != 0) {
        this.generated = this.prefix + results.join(' ') + this.suffix;
      }

      return this;
    },
    customRule: function(entry) {
      var name = entry.name.replace('generator[', '').replace('][', '___').replace(']', '');
      switch (name) {
        case 'shape' :
        case 'position' :
        case 'spin' :
        case 'flip' :

          if (entry.value == '0') {
            return false;
          }
          break;
      }

      return true;
    },
    update: function() {
      this.generate();

      if (this.generated.length == 0) {
        this.generated = 'No Shortcode Generated';
      }

      this.$target.text(this.generated);
    }
  }

  $(document)
    .on('click.icon-generator', '[data-icon-generator]', function() {
      var Generator = new VTCore.VTIconsShortcodeGenerator($(this).closest('form'));
      Generator.generate().update();
    });

})(jQuery);