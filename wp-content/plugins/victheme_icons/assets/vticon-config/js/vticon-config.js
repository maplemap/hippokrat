jQuery(document).ready(function($) {
  
  $(document).on('click', '.check-all', function() {
    var state = $(this).attr('checked');
    $(this).closest('table').find('[type="checkbox"]').not(':disabled').each(function() {
      if (state == 'checked') {
        $(this).attr('checked', 'checked');
      }
      else {
        $(this).removeAttr('checked');
      }
    })
  })
  
});