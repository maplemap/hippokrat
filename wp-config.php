<?php
/**
 * Основные параметры WordPress.
 *
 * Этот файл содержит следующие параметры: настройки MySQL, префикс таблиц,
 * секретные ключи и ABSPATH. Дополнительную информацию можно найти на странице
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Кодекса. Настройки MySQL можно узнать у хостинг-провайдера.
 *
 * Этот файл используется скриптом для создания wp-config.php в процессе установки.
 * Необязательно использовать веб-интерфейс, можно скопировать этот файл
 * с именем "wp-config.php" и заполнить значения вручную.
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'hippokrat');

/** Имя пользователя MySQL */
define('DB_USER', 'admin');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', 'admin');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'pbxsM6c[9=|S+jtBQHAp9#r]W3+2FPI^%e&*1Y<#o-WY]aImoroJsL-+S8NFm_X#');
define('SECURE_AUTH_KEY',  'JplQSH)Z412(+~p!-`t7$qL~K,ZAIgU_i1I-~RDV/hq/+P~48vi#@Lo)qT_6a.8?');
define('LOGGED_IN_KEY',    '.N<^07~+iac.JT*=/eI.Llh#+6qCw=PEfy>Ef/?Ge}^?XrC96J+Uk{LJ-Az4b-Zf');
define('NONCE_KEY',        '+D?$Ala%-R-,ojhH~8R !&fM?nwSaJoK` .KsA<{5_Z;$yU<K8w z`<J6TsPBjmr');
define('AUTH_SALT',        '(2ew@{rE-B%!vn6ze*>A,UrNi+^z4{Grsv+J{nq8D0=m/ D*Td.X(!K=4v.M-xZ@');
define('SECURE_AUTH_SALT', 'jD8B?yO_jxcLi&F?ON*;.B-~?[79T3+trf9n9}:Xi0PRS?[`1koJrw-.-13rC:0h');
define('LOGGED_IN_SALT',   'P/;gf*J[|f]sHpv?oWh!5Xd-Xigo}9GuJ!g8dnYtpy@-Pj)D9DOY3 ,0w<`2dL`=');
define('NONCE_SALT',       'Rsya_ej|OMU>4,=2(D|r6V-R9d25eaDvMNI8*<^:?QyEb=|P9Rlw/|3w>FhA_OBg');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');

define('FS_METHOD', 'direct');
